/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.text;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import jumbo2.navigator.DocumentNavigator;
import jumbo2.navigator.text.filecontentmanager.FileContentManager;
import jumbo2.util.Utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class TextDocumentNavigator extends DocumentNavigator {
	private static final Logger logger = LogManager.getLogger(TextDocumentNavigator.class.getName());
	private FileContentManager fileManager = null;
	private String baseUUID ;
	private String inputFilePath = null;
	private String append = null;
	
	public TextDocumentNavigator(String inputFilePath, String topTemplatePath) throws IOException, ParserConfigurationException, SAXException{
		this(inputFilePath, topTemplatePath, null, null);
	}
	
	public TextDocumentNavigator(String inputFilePath, String topTemplatePath, String append) throws ParserConfigurationException, SAXException, IOException{
		this(inputFilePath, topTemplatePath, append, null); 
	}

	public TextDocumentNavigator(String inputFilePath, String topTemplatePath, String append, String excludedLinesRegexp) throws ParserConfigurationException, SAXException, IOException {
		super(topTemplatePath);
		this.inputFilePath = inputFilePath;
		this.append = append;
		baseUUID = Utils.buildRandomUUID();		
		fileManager = new FileContentManager(inputFilePath, baseUUID, excludedLinesRegexp);	    		
	}

	@Override
	public Document call() throws Exception {
		ElementNavigator elementNavigator = new ElementNavigator(namespaceContext, fileManager, templateDocument.getFirstChild(), resultDocument.getFirstChild(), baseUUID, "");
		elementNavigator.run();			
		if(logger.getLevel() != Level.DEBUG)
			elementNavigator.removeStartEndLineAttributes();
		else
			elementNavigator.saveUnprocessedSectionsToFile(inputFilePath + ".unprocessed.txt");			
		if(append != null && !append.equals(""))
			elementNavigator.appendTextAsXML(append);
		return null;
	}
	
	public String getInputFilePath(){
		return this.inputFilePath;
	}
		
}

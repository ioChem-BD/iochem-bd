/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.text.filecontentmanager;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import jumbo2.navigator.text.filecontentmanager.helper.LineRange;
import jumbo2.navigator.text.filecontentmanager.helper.LineProperties;
import jumbo2.navigator.text.filecontentmanager.helper.Section;
import jumbo2.navigator.text.filecontentmanager.helper.tree.Interval1D;
import jumbo2.navigator.text.filecontentmanager.helper.tree.IntervalST;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileContentManager {

	private static final Logger logger = LogManager.getLogger(FileContentManager.class.getName());
	private LineProperties[] lineProperties = null;
	private List<String> lines = null;

	private String baseUUID = null;
	private IntervalST<String> st = null;
	private HashMap<String, Interval1D> map = null;
	private int totalNumberOfLines = 0;
	
	private static Charset[] testedCharsets = {
												Charset.forName("UTF-8"), 
												Charset.forName("ISO-8859-1"), 
												Charset.forName("UTF-16"), 
												Charset.forName("Windows-1251"), 
												Charset.forName("Windows-1252"), 
												Charset.forName("GB2312"), 
												Charset.forName("EUC-KR"), 
												Charset.forName("EUC-JP"), 
												Charset.forName("GBK"), 
												Charset.forName("ISO-8859-2"), 
												Charset.forName("ISO-8859-15"), 
												Charset.forName("Windows-1250"), 
												Charset.forName("Windows-1256"), 
												Charset.forName("ISO-8859-9"), 
												Charset.forName("Big5"), 
												Charset.forName("Windows-1254"), 
												Charset.forName("Windows-874")
											};	

	public FileContentManager(String filePath, String baseUUID) {
		this(filePath, baseUUID, null);
	}

	public FileContentManager(String filePath, String baseUUID, String filterRegex) {	
		this.baseUUID = baseUUID;
		st = new IntervalST<String>();
		map = new HashMap<String, Interval1D>();
		initLineContent(filePath, filterRegex);
		initLineProperties();
		initFirstInterval();		
	}

	private void initLineContent(String filePath, String filterRegex) {
		Path path = Paths.get(filePath);
		lines = new ArrayList<String>();
		List<String> tmpLines = null;
		for(Charset charset : testedCharsets){		//Test multiple charsets
			try{
				tmpLines = Files.readAllLines(path, charset);
				filterLines(tmpLines, filterRegex);
				break;
			}catch(Exception e){
				//logger.error(e.getMessage());
			}
		}
		lines.add("");
		lines.addAll(tmpLines);
		tmpLines.clear();
		
		totalNumberOfLines = lines.size() - 1;
	}
	
	private void filterLines(List<String> lines, String filterRegex) {
		if(filterRegex == null)
			return;
		
		Pattern p = Pattern.compile(filterRegex);
		Iterator<String> itr = lines.iterator();
		while(itr.hasNext()) {
			if(p.matcher(itr.next()).matches())
				itr.remove();
		}
	}

	private void initLineProperties() {
		lineProperties = new LineProperties[totalNumberOfLines + 1]; // element in position 0 will be discarded
		for (int inx = 1; inx <= totalNumberOfLines; inx++)
			lineProperties[inx] = new LineProperties(baseUUID);
	}

	private void initFirstInterval() {
		Interval1D interval = new Interval1D(1, totalNumberOfLines);
		st.put(interval, baseUUID);
		map.put(baseUUID, interval);
		logger.debug(interval);
	}

	public LinkedList<Section> getUnProcessedSections(String parentUUID) throws IOException {
		LinkedList<Section> sections = new LinkedList<Section>();
		Section currentSection = null;
		Interval1D interval = map.get(parentUUID);
		for (int inx = interval.low; inx <= interval.high; inx++) {
			if (lineProperties[inx].getParentUUID().equals(parentUUID)) {
				if (!lineProperties[inx].isProcessed()) {
					if (currentSection == null) {
						currentSection = new Section(inx);
						sections.add(currentSection);
					}
					currentSection.appendLine(lines.get(inx));
				}
			} else
				currentSection = null;
		}
		return sections;
	}

	public LinkedList<Section> getAllUnProcessedSections(boolean appendLineNumbers) throws IOException {
		LinkedList<Section> sections = new LinkedList<Section>();
		Section currentSection = null;
		for (int inx = 1; inx <= totalNumberOfLines; inx++) {
			if (!lineProperties[inx].isProcessed()) {
				if (currentSection == null) {
					currentSection = new Section(inx);
					sections.add(currentSection);
				}
				if (appendLineNumbers) {
					currentSection.appendLine(inx + "\t\t" + lines.get(inx));
				}
				else {
					currentSection.appendLine(lines.get(inx));
				}
			} else {
				currentSection = null;
			}
		}
		return sections;
	}

	public void assignLinesToNewSection(LineRange lineEnds, String parentUUID, String indent) {
		Interval1D interval = new Interval1D(lineEnds.getStart(), lineEnds.getEnd());
		if (st.contains(interval))
			logger.info("duplicated interval : " + interval.toString());
		st.put(interval, parentUUID);
		map.put(parentUUID, interval);
		for (int inx = lineEnds.getStart(); inx <= lineEnds.getEnd(); inx++)
			lineProperties[inx].setParentUUID(parentUUID);
	}

	public void setLineAsProcessed(int lineNumber) {
		lineProperties[lineNumber].setProceesed(true);
	}

	public String getUUIDForInterval(int start, int end) {
		return st.get(st.search(new Interval1D(start, end)));
	}

}

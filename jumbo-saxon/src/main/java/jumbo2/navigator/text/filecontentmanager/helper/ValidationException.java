package jumbo2.navigator.text.filecontentmanager.helper;

public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }
    
}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.text.filecontentmanager.helper;

import java.util.ArrayList;

public class Section {
	
	private ArrayList<String> lines;
	private int offset;
	
	public Section(int offset){
		lines = new ArrayList<String>();
		this.offset = offset - 1;			//Section has offset 0 related to parent document first line 	 
	}
	
	public void appendLine(String line){
		lines.add(line);
	}
	
	public int getOffset(){
		return offset;	
	}
	
	public ArrayList<String> getLines(){
		return lines;
	}
	
}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.xslt;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.concurrent.Callable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import jumbo2.util.Utils;

public class XsltSaxonConverter implements Callable<Document> {
	private static final Logger logger = LogManager.getLogger(XsltSaxonConverter.class.getName());
	private static DocumentBuilderFactory bFactory 	= null;
	private static TransformerFactory tFactory 	= new net.sf.saxon.TransformerFactoryImpl();
	private Transformer transformer = null;
	private String inputFilePath = null;
	private String append = null;	
	
	static{
		bFactory = DocumentBuilderFactory.newInstance();
		bFactory.setXIncludeAware(true);
		bFactory.setNamespaceAware(true);
		bFactory.setIgnoringComments(true);
	}
	
	public XsltSaxonConverter(String inputFilePath, String xslFilePath) throws TransformerConfigurationException, MalformedURLException, IOException{
		this(inputFilePath, xslFilePath, null);
	}
	
	public XsltSaxonConverter(String inputFilePath, String xslFilePath, String append) throws TransformerConfigurationException, MalformedURLException, IOException{
		this.inputFilePath = inputFilePath;
		if(transformer == null){
			StreamSource xslSource = new StreamSource(Utils.buildFullUri(xslFilePath));
			transformer =  tFactory.newTemplates(xslSource).newTransformer();
		}
		this.append = append;
	}
	
	@Override
	public Document call() throws Exception{
		StreamSource xslSource = new StreamSource(new File(inputFilePath));	   
		StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
		if(append != null && !append.equals(""))
    		transformer.setParameter("append", append);		
		transformer.transform(xslSource, result);
		DocumentBuilder builder = bFactory.newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(writer.getBuffer().toString())));
		return doc;
	}

}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.converters;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import jumbo2.navigator.text.TextDocumentNavigator;

public class PlainTextToXmlConverter extends BasicConverter<Document> {
	
	private TextDocumentNavigator documentNavigator = null;
	private String append = null;
	
	public PlainTextToXmlConverter(String inputFilePath, String templateFilePath) throws IOException, ParserConfigurationException, SAXException{		
		this(inputFilePath, templateFilePath, null);		
	}
	
	public PlainTextToXmlConverter(String inputFilePath, String templateFilePath, String append) throws IOException, ParserConfigurationException, SAXException{		
		documentNavigator = new TextDocumentNavigator(inputFilePath, templateFilePath, append);		
	}
	
	public PlainTextToXmlConverter(String inputFilePath, String templateFilePath, String appendedXML, String excludedLinesRegexp) throws ParserConfigurationException, SAXException, IOException {
		documentNavigator = new TextDocumentNavigator(inputFilePath, templateFilePath, append, excludedLinesRegexp);
	}

	@Override
	public Document call() throws Exception{
		Long start = System.currentTimeMillis();
		documentNavigator.call();
		Document result = documentNavigator.getResultDocument(); 		
	    Long end = System.currentTimeMillis();
	    if(logger.getLevel() == Level.DEBUG)
	    	printResume(result, start, end);
	    return result;
	}
		
}
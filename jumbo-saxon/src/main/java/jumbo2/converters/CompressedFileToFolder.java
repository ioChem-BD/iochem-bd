package jumbo2.converters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;

public class CompressedFileToFolder extends BasicConverter<File> {

	private String inputFilePath;

	public CompressedFileToFolder(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	@Override
	public File call() {
		Path tmpFile = null;
		try {
			tmpFile = Files.createTempDirectory("iochem-bd-extraction-");			
			getFileContent(new File(inputFilePath), tmpFile.toFile());
			return tmpFile.toFile();
		} catch (Exception e) {
			if(tmpFile != null)
				deleteDir(tmpFile.toFile());			
		}	
		return null;
	}

	private void getFileContent(File zipFile, File destFolder) throws IOException {
		String mimetype = Files.probeContentType(zipFile.toPath());	
		if(mimetype == null)
			mimetype = "text/plain";
		switch (mimetype) {
			case "application/zip":				    getZipContent(zipFile, destFolder);
													break;

			case "application/gzip":
			case "application/x-compressed-tar": 	getGzipContent(zipFile, destFolder);
													break;

			case "text/plain":						
			default:								getPlainTextContent(zipFile, destFolder);
													break;
								
		}
	}
	private void getZipContent(File zip, File destFolder) throws IOException {
		Path destPath = destFolder.toPath().toAbsolutePath().normalize();
		try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zip))) {			
			ZipEntry zipEntry = zis.getNextEntry();
			while (zipEntry != null) {
				Path resolvedPath = destPath.resolve(zipEntry.getName()).normalize();
				if (!resolvedPath.startsWith(destPath)) {
					logger.warn("Skipping malicious entry: " + zipEntry.getName());
					zipEntry = zis.getNextEntry();
					continue;
				}
				
				File out = resolvedPath.toFile();
				if (zipEntry.isDirectory()) {
					if (!out.exists() && !out.mkdirs()) {
						throw new IOException("Failed to create directory " + out);
					}
				} else {
					File parent = out.getParentFile();
					if (!parent.exists() && !parent.mkdirs()) {
						throw new IOException("Failed to create directory " + parent);
					}
					if (out.exists()) {
						logger.warn("File already exists, skipping: " + out);
					} else {
						extractFile(zis, out);
					}
				}								
				zipEntry = zis.getNextEntry();
			}
		} catch (Exception e) {
			logger.error("Error extracting zip file: " + e.getMessage(), e);
			throw new IOException("Failed to extract zip file", e);
		}
	}

	private static void extractFile(ZipInputStream is, File out) throws IOException {
		byte[] buffer = new byte[2048];
		
		try(FileOutputStream fos = new FileOutputStream(out);
			BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

            int len;
            while ((len = is.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
            }						
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		} 
	}
	
    public void getGzipContent(File zip, File destFolder) throws IOException {    	
        try{
        	unTar(unGzip(zip, destFolder), destFolder);                                   
        } catch (IOException e) {
            throw e;
        }
    }

    private static File unGzip(final File inputFile, final File destFolder) throws FileNotFoundException, IOException {
        final File outputFile = new File(destFolder, inputFile.getName().substring(0, inputFile.getName().length() - 3));
        try (final GZIPInputStream in = new GZIPInputStream(new FileInputStream(inputFile));
             final FileOutputStream out = new FileOutputStream(outputFile);){
        	IOUtils.copy(in, out);
            return outputFile;
        }catch(Exception e) {
        	throw e;
        }
    }
	
	private static List<File> unTar(final File inputFile, final File outputDir) throws IOException {
		final List<File> untaredFiles = new LinkedList<>();
		Path outputPath = outputDir.toPath().toAbsolutePath().normalize();
		try (FileInputStream fis = new FileInputStream(inputFile);
			BufferedInputStream bis = new BufferedInputStream(fis)) {
			
			byte[] buffer = new byte[512];
			while (true) {
				int bytesRead = bis.read(buffer, 0, 512);
				if (bytesRead == -1) break;

				String fileName = new String(buffer, 0, 100).trim();
				if (fileName.isEmpty()) continue;
				long size = Long.parseLong(new String(buffer, 124, 12).trim(), 8);
				// Check for path traversal
				Path filePath = outputPath.resolve(fileName).normalize();
				if (!filePath.startsWith(outputPath)) {
					logger.warn("Potentially malicious entry detected: " + fileName);
					continue;
				}

				File outputFile = filePath.toFile();
				if (fileName.endsWith("/")) {
					if (!outputFile.exists() && !outputFile.mkdirs()) {
						throw new IOException("Failed to create directory " + outputFile);
					}
				} else {
					File parent = outputFile.getParentFile();
					if (!parent.exists() && !parent.mkdirs()) {
						throw new IOException("Failed to create directory " + parent);
					}
					try (FileOutputStream fos = new FileOutputStream(outputFile);
						BufferedOutputStream bos = new BufferedOutputStream(fos)) {
						long remainingSize = size;
						while (remainingSize > 0) {
							int toRead = (int) Math.min(remainingSize, buffer.length);
							int read = bis.read(buffer, 0, toRead);
							if (read == -1) break;
							bos.write(buffer, 0, read);
							remainingSize -= read;
						}
					}
				}
				untaredFiles.add(outputFile);
				long skipAmount = (512 - (size % 512)) % 512;
				bis.skip(skipAmount);
			}
		} catch (Exception e) {
			logger.error("Error processing TAR file: " + e.getMessage(), e);
			// Clean up partially extracted files
			for (File file : untaredFiles) {
				FileUtils.deleteQuietly(file);
			}
			throw new IOException("Failed to process TAR file", e);
		}

		try {
			Files.delete(inputFile.toPath());
		} catch (IOException e) {
			logger.warn("Failed to delete temporary file: " + inputFile, e);
		}
		return untaredFiles;
	}

	private void getPlainTextContent(File text, File destFolder) throws IOException {
		Files.copy(text.toPath(), Paths.get(destFolder.getCanonicalPath(), text.getName()));		
	}

    private void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
}

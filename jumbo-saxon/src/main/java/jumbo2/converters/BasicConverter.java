/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.converters;

import java.io.File;
import java.text.DecimalFormat;
import java.util.concurrent.Callable;

import jumbo2.util.Utils;
import net.sf.saxon.lib.NamespaceConstant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

public abstract class BasicConverter<T> implements Callable <T>{
	
	protected static Logger logger = LogManager.getLogger(BasicConverter.class.getName());
	
	@Override
	abstract public T call() throws Exception;
	
	static{
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		System.setProperty("javax.xml.xpath.XPathFactory:"+NamespaceConstant.OBJECT_MODEL_SAXON, "net.sf.saxon.xpath.XPathFactoryImpl");
	}
	
	public static void printResume(String filePath, Long start, Long end){
	    double seconds = (end-start)/1000.0;
		double bytes = new File(filePath).length();	    
		double kilobytes = (bytes / 1024);
		DecimalFormat kiloFormat = new DecimalFormat("#.###");		
		String kilobytesStr = kiloFormat.format(kilobytes);		
	    System.out.println("File size: " + kilobytesStr + " kb, ellapsed time : " + String.valueOf(seconds) + " s");
	}
	
	public static void printResume(Document document, Long start, Long end){
	    double seconds = (end-start)/1000.0;
	    
		double bytes = Utils.documentToString(document).getBytes().length;
		double kilobytes = (bytes / 1024);
		DecimalFormat kiloFormat = new DecimalFormat("#.###");		
		String kilobytesStr = kiloFormat.format(kilobytes);		
	    System.out.println("File size: " + kilobytesStr + " kb, ellapsed time : " + String.valueOf(seconds) + " s");			
	}
}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.util;

import org.xmlcml.cml.converters.DynamicConverter;

/**
 * Conversion service that will use jumbo-saxon libraries
 * @author malvarez
 *
 */
public class ConvertService {
	
	public static void main(String[] args) {
		String templateUrl = System.getenv("JUMBO_TEMPLATE_BASE_URL");
		if(templateUrl == null || templateUrl.equals("")) {
			System.out.println("Must define system variable JUMBO_TEMPLATE_BASE_URL pointing to the base folder structure.");
			System.exit(-1);
		}	
		
		System.setProperty("JUMBO_TEMPLATE_BASE_URL", templateUrl);
		
		if(args.length != 3) {
			System.out.println("Invalid number of arguments:");			
			System.out.println("Usage: java -jar converter.jar CONVERSION_CLASS INPUT_FILE OUTPUT_FILE ");
			System.out.println("The following parameters are mandatory:");			
			System.out.println("    CONVERSION_CLASS : Class package used for current conversion");
			System.out.println("    INPUT_FILE       : Input file to convert");
			System.out.println("    OUTPUT_FILE      : Generated output");
			System.out.println("");
			System.out.println("Example:");
			System.out.println("    java -jar converter.jar org.xmlcml.cml.converters.compchem.gaussian.log.GaussianLog2CompchemConverter hexenol.out hexenol.cml ");
			System.out.println("");
			System.exit(-1);
		}

		String conversionClass = args[0];
		String inputFilePath = args[1];
		String outputFilePath = args[2];

		try {
			DynamicConverter converter = new DynamicConverter(conversionClass, inputFilePath, outputFilePath, null);
			converter.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

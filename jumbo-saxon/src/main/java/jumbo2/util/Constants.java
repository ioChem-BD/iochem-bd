/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.util;

public class Constants {

	public static final String cmlNamespace 	= "http://www.xml-cml.org/schema";
	
	public static final String cmlPrefix = "cml";	
	public static final String cmlPrefixNamespace[] = {"xmlns:" + cmlPrefix, cmlNamespace};
	
	public static final String cmlxPrefix 	= "cmlx";
	public static final String cmlxPrefixNamespace[] = {"xmlns:" + cmlxPrefix, cmlNamespace + "/cmlx"};

	public static final String compchemPrefix = "cc";
	public static final String compchemNamespace[] =  {"xmlns:" + compchemPrefix, "http://www.xml-cml.org/dictionary/compchem/"};
	
	public static final String siUnitsPrefix = "si";
	public static final String siNamespace[] = {"xmlns:" + siUnitsPrefix, "http://www.xml-cml.org/unit/si/"};

	public static final String nonSiUnitsPrefix = "nonsi";
	public static final String nonSiUnitsNamespace[] =  {"xmlns:" + nonSiUnitsPrefix, "http://www.xml-cml.org/unit/nonSi/"};
	
	public static final String nonSi2UnitsPrefix = "nonsi2";
	public static final String nonSi2UnitsNamespace[] =  {"xmlns:" + nonSi2UnitsPrefix, "http://www.iochem-bd.org/unit/nonSi2/"};
		
	public static final String xmlSchemaPrefix = "xsd";
	public static final String xmSchemaNamespace[] =  {"xmlns:" + xmlSchemaPrefix, "http://www.w3.org/2001/XMLSchema"};
    		
	public static final String conventionPrefix = "convention";
	public static final String conventionNamespace[] =  {"xmlns:" + conventionPrefix, "http://www.xml-cml.org/convention/"};
    
	public static final String xIncludePrefix = "xi";
	public static final String xIncludeNamespace[] =  {"xmlns:" + xIncludePrefix, "http://www.w3.org/2001/XInclude"};
    

	public static final String[][] namespaces = {cmlPrefixNamespace, cmlxPrefixNamespace, compchemNamespace, siNamespace, nonSiUnitsNamespace, nonSi2UnitsNamespace, xmSchemaNamespace, conventionNamespace, xIncludeNamespace };
	
	public static final String WHITESPACES_REGEX = "\\s+"; 
	

}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import jumbo2.util.Constants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Bond {
		
	public static enum BondType { SINGLE,	DOUBLE,	TRIPLE}
	private String fromAtomId = null;
	private String toAtomId = null;
	private BondType bondType;
	
	public Bond(String fromAtomId, String toAtomId, BondType bondType){
		this.setFromAtomId(fromAtomId);
		this.setToAtomId(toAtomId);
		this.setBondType(bondType);
	}

	public String getFromAtomId() {
		return fromAtomId;
	}

	public void setFromAtomId(String fromAtomId) {
		this.fromAtomId = fromAtomId;
	}

	public String getToAtomId() {
		return toAtomId;
	}

	public void setToAtomId(String toAtomId) {
		this.toAtomId = toAtomId;
	}

	public BondType getBondType() {
		return bondType;
	}

	public void setBondType(BondType bondType) {
		this.bondType = bondType;
	}
	
	public Node buildNode(Document doc){
		Element bond = doc.createElementNS(Constants.cmlNamespace, "bond");
		bond.setAttribute("atomRefs2", fromAtomId + " " + toAtomId);
		switch(bondType){
			case SINGLE:	bond.setAttribute("order", "S");
							break;			
			case DOUBLE:	bond.setAttribute("order", "D");
							break;
			case TRIPLE:	bond.setAttribute("order", "T");
							break;
		}
		return bond;
	}
}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import java.util.Hashtable;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import jumbo2.util.Constants;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MoleculeElement {
	/*
	 * Helper class to convert Molecule class into Node element 
	 */
	
	private static final String[] hillOrderedAtoms = {"C","H","Ac","Al","Am","Sb","Ar","As","At","B","Ba","Bk","Be","Bi","Bh","Br","Cd","Ca","Cf","Ce","Cs","Cl","Cr","Co","Cu","Cm","Ds","Db","Dy","Es","Er","Eu","F","Fm","Fr","Gd","Ga","Ge","Au","Hf","Hs","He","Ho","I","In","Ir","Fe","Kr","La","Lr","Pb","Li","Lu","Mg","Mn","Mt","Md","Hg","Mo","N","Nd","Ne","Np","Ni","Nb","No","O","Os","P","Pd","Pt","Pu","Po","K","Pr","Pm","Pa","Ra","Rn","Re","Rh","Rb","Ru","Rf","Sm","Sc","Sg","Se","Si","Ag","Na","S","Sr","Ta","Tc","Te","Tb","Tl","Th","Tm","Sn","Ti","W","U","Uub","Uuh","Uuo","Uup","Uuq","Uus","Uut","Uuu","V","Xe","Yb","Y","Zn","Zr"};
	
	Molecule molecule = null;
	Document document = null;
	Element moleculeNode = null;
	XPathFactory factory = null;
	NamespaceContext namespaceContext = null;
	
	public MoleculeElement(XPathFactory factory, NamespaceContext namespaceContext, Document document, Molecule molecule){
		this.factory = factory;
		this.namespaceContext = namespaceContext;
		this.document = document;
		this.molecule = molecule;				 
	}
	
	public Node buildNode() throws Exception{
		buildMoleculeNode();
		buildAtomArrayNode();
		buildBondArrayNode();
		addFormulaAndProperties();
		removeExplicitHydrogenCounts();		
		return moleculeNode;
	}

	private Element buildMoleculeNode(){
		moleculeNode = document.createElementNS(Constants.cmlNamespace, "molecule");
		moleculeNode.setAttribute("id", molecule.getId());
		return moleculeNode;
	}
	
	private void buildAtomArrayNode(){
		Element atomArray = document.createElementNS(Constants.cmlNamespace, "atomArray");
		for(String id : molecule.getAtomsId())
			atomArray.appendChild(molecule.getAtomById(id).buildNode(document));
		if(molecule.hasZMatrix())
			moleculeNode.appendChild(molecule.getZMatrix().buildNode(document));
		moleculeNode.appendChild(atomArray);
		
	}
	
	private void buildBondArrayNode(){
		Element bondArray = document.createElementNS(Constants.cmlNamespace, "bondArray");
		for(Bond bond: molecule.getBondArray())
			bondArray.appendChild(bond.buildNode(document));		
		moleculeNode.appendChild(bondArray);		
	}
	
	private void addFormulaAndProperties() throws Exception{
		createFormula();
		addMolecularMass();
	}
	
	private void createFormula(){
		Document doc = moleculeNode.getOwnerDocument();
		Element formula = (Element)doc.createElementNS(Constants.cmlNamespace, "formula");
		Element atomArray = (Element)doc.createElementNS(Constants.cmlNamespace, "atomArray");
		formula.appendChild(atomArray);
   
        Hashtable<String,Integer> atomCount = new Hashtable<String,Integer>();
        StringBuilder hillNotation = new StringBuilder();        
        StringBuilder atomTypes   = new StringBuilder();
        StringBuilder atomCounter = new StringBuilder();
        // First we count all atoms
        for(Atom atom : molecule.getAtomArray()){
                int count;
                if(atomCount.containsKey(atom.getElementType())){
                        count = atomCount.get(atom.getElementType());
                        atomCount.put(atom.getElementType(), count +1);
                }else{
                        atomCount.put(atom.getElementType(), 1);
                }
        }

       for(String atomType: hillOrderedAtoms)
    	   if(atomCount.containsKey(atomType)){
    		   hillNotation.append(atomType + String.valueOf( atomCount.get(atomType) > 1? atomCount.get(atomType): ""));
    		   atomTypes.append(atomType + " ");
    		   atomCounter.append(String.valueOf(atomCount.get(atomType)) + " ");
    	   }
			
		formula.setAttribute("concise", hillNotation.toString().trim());
		atomArray.setAttribute("elementType", atomTypes.toString().trim());
		atomArray.setAttribute("count", atomCounter.toString().trim());
		moleculeNode.appendChild(formula);		
	}	
	
	private void addMolecularMass() throws Exception{
		double mwt = getCalculatedMolecularMass(false);
		Element property = moleculeNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "property");
		property.setAttribute("dictRef", "cml:molmass");
		Element scalar = moleculeNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");
		scalar.setAttribute("units", "unit" + ":" + "dalton");
		scalar.setTextContent(String.valueOf(mwt));
		property.appendChild(scalar);
		moleculeNode.appendChild(property);		
	}
	
    private double getCalculatedMolecularMass(boolean countHydrogens) throws Exception{
    	double total = 0.0;
    	for(Atom atom : molecule.getAtoms())
    		if(countHydrogens)
    			total += ChemicalElement.getChemicalElement(atom.getElementType()).getMass();
    		else if(!countHydrogens && !atom.getElementType().equals("H"))
    			total += ChemicalElement.getChemicalElement(atom.getElementType()).getMass();
    	return total;
    }
	
	private void removeExplicitHydrogenCounts() throws XPathExpressionException {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(".//@hydrogenCount");
		NodeList hydrogenCounts = (NodeList)expr.evaluate(moleculeNode, XPathConstants.NODESET);
		for (int i = 0; i < hydrogenCounts.getLength(); i++) {
			Attr attr = (Attr) hydrogenCounts.item(i);
			attr.getParentNode().removeChild(attr);			
		}
	}
	
	
	
}

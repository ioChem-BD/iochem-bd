/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import jumbo2.util.Constants;

public class Atom {
	private String elementType = "";
	private int atomicNumber = -1;
	private String id = "";
	private Point3d coords = null;
	private Point3d fractcoords = null;		//Optional coordinates in fractional format
	private ArrayList<Node> internalScalars = null;
	private boolean hasX = false;
	private boolean hasY = false;
	private boolean hasZ = false;
	private DecimalFormatSymbols df = DecimalFormatSymbols.getInstance(Locale.UK);
	private DecimalFormat formatter = new DecimalFormat("#0.0000####", df);
	
	public Atom(String id){
		this.id = id;
		coords = new Point3d();		
		internalScalars = new ArrayList<Node>();
	}	
	
	public Atom(Node node) throws Exception{
		Element atom = (Element) node;
		elementType = atom.getAttribute("elementType");
		atomicNumber = ChemicalElement.getChemicalElement(elementType).getAtomicNumber();
		id = atom.getAttribute("id");
		if(atom.hasAttribute("x3") || atom.hasAttribute("y3") || atom.hasAttribute("z3"))
			coords = new Point3d();
		if(atom.hasAttribute("x3")){
			coords.x = Double.valueOf(atom.getAttribute("x3"));
			hasX = true;
		}
		if(atom.hasAttribute("y3")){
			coords.y = Double.valueOf(atom.getAttribute("y3"));
			hasY = true;
		}
		if(atom.hasAttribute("z3")){
			coords.z = Double.valueOf(atom.getAttribute("z3"));
			hasZ = true;
		}
		
		if(atom.hasAttribute("xFract") || atom.hasAttribute("yFract") || atom.hasAttribute("zFract"))
			fractcoords = new Point3d();
		if(atom.hasAttribute("xFract")){
			fractcoords.x = Double.valueOf(atom.getAttribute("xFract"));
			hasX = true;
		}
		if(atom.hasAttribute("yFract")){
			fractcoords.y = Double.valueOf(atom.getAttribute("yFract"));
			hasY = true;
		}
		if(atom.hasAttribute("zFract")){
			fractcoords.z = Double.valueOf(atom.getAttribute("zFract"));
			hasZ = true;
		}
		
		internalScalars = new ArrayList<Node>();
	}

	public Atom(String elementType, String id) throws Exception{
		this.elementType = elementType;
		this.atomicNumber = ChemicalElement.getChemicalElement(elementType).getAtomicNumber();
		this.id = id;
		coords = new Point3d();
		fractcoords = null;
	}
	
	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) throws Exception {
		this.elementType = elementType;
		this.atomicNumber = ChemicalElement.getChemicalElement(elementType).getAtomicNumber();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getX3() {
		return coords.x;
	}

	public void setX3(double x3) {
		coords.x = x3;
		hasX = true;
	}

	public double getY3() {
		return coords.y;
	}

	public void setY3(double y3) {
		coords.y = y3;
		hasY = true;
	}

	public double getZ3() {
		return coords.z;
	}

	public void setZ3(double z3) {
		coords.z = z3;
		hasZ = true;
	}	
	
	public Point3d getXYZ3(){
		return coords;
	}
	
	public void setXYZ3(Point3d coords){
		this.coords = coords;
		hasX = hasY = hasZ = true;
	}
	
	public Point3d getFractionaXYZ3(){
		return fractcoords;
	}
	
	public void setFractionalXYZ3(Point3d coords){
		this.fractcoords = coords;
		hasX = hasY = hasZ = true;
	}
	
	
	public Node buildNode(Document doc){
		Element atom = doc.createElementNS(Constants.cmlNamespace, "atom");
		atom.setAttribute("elementType", elementType);
		atom.setAttribute("id", id);
		if(coords!= null){
			if(hasX)
				atom.setAttribute("x3", String.valueOf(formatter.format(coords.x)));
			if(hasY)
				atom.setAttribute("y3", String.valueOf(formatter.format(coords.y)));
			if(hasZ)
				atom.setAttribute("z3", String.valueOf(formatter.format(coords.z)));			
		}
		if(fractcoords != null){
			if(hasX)
				atom.setAttribute("xFract", String.valueOf(formatter.format(fractcoords.x)));
			if(hasY)
				atom.setAttribute("yFract", String.valueOf(formatter.format(fractcoords.y)));
			if(hasZ)
				atom.setAttribute("zFract", String.valueOf(formatter.format(fractcoords.z)));
		}
		if(internalScalars.size() > 0)
			for(Node scalar : internalScalars)
				atom.appendChild(scalar);			
		
		return atom;
	}
	
	public void addScalar(Node scalar){
		internalScalars.add(scalar);
	}

	public Integer getIndex() {
		return Integer.valueOf(this.id.replace("a", ""));
	}

	public int getAtomicNumber() {
		return atomicNumber;
	}

	public float getBondingRadius() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}

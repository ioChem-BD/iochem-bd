/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Vector;

import jumbo2.util.Constants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


public class Molecule {
		
	private Logger logger = LogManager.getLogger(Molecule.class.getName());
	private String id = null;
	private LinkedHashMap<String, Atom> atoms = null;
	private Vector<Bond> bonds = null;
	
	private ZMatrix zMatrix = null;
	
	public Molecule(String id){
		atoms = new LinkedHashMap<String, Atom>();
		bonds = new Vector<Bond>();
		this.setId(id);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void addAtom(Atom atom){
		atoms.put(atom.getId(), atom);
	}
	
	public Atom[] getAtoms(){
		Atom[] atomArray = new Atom[atoms.size()];
		return atoms.values().toArray(atomArray);
	}
	
	public Set<String> getAtomsId(){
		return atoms.keySet();
	}
	
	public Atom getAtomById(String id){
		return atoms.get(id);
	}

	public void addCartesiansTo(){
		zMatrix.addCartesiansTo(this);
	}
	
	public boolean hasZMatrix(){
		return zMatrix != null;
	}
	
	public void addZMatrixLine(Node line){
		if(zMatrix == null)
			zMatrix = new ZMatrix();
		zMatrix.addLine(line);
	}
	
	public ZMatrix getZMatrix(){
		return this.zMatrix;
	}
	
	class ZMatrix{	
		private ArrayList<Length> lengthElements = new ArrayList<Length>();
		private ArrayList<Angle> angleElements = new ArrayList<Angle>();
		private ArrayList<Torsion> torsionElements = new ArrayList<Torsion>();
		
		public ZMatrix(){
			lengthElements = new ArrayList<Length>();
			angleElements = new ArrayList<Angle>();
			torsionElements = new ArrayList<Torsion>();		
		}
		
		public void addLine(Node line){						
			if(line.getLocalName().equals("length"))
				lengthElements.add(new Length(line));			
			else if(line.getLocalName().equals("angle"))				
				angleElements.add(new Angle(line));
			else if(line.getLocalName().equals("torsion"))				
				torsionElements.add(new Torsion(line));
		}
		
		public Node buildNode(Document document){
			Node zMatrix = document.createElementNS(Constants.cmlNamespace, "zMatrix");
			for(Length l: lengthElements)	
				zMatrix.appendChild(l.buildNode(document));
			for(Angle a: angleElements)
				zMatrix.appendChild(a.buildNode(document));
			for(Torsion t: torsionElements)
				zMatrix.appendChild(t.buildNode(document));
			return zMatrix;
		}
		
		public void addCartesiansTo(Molecule molecule){			
			  if (lengthElements.size() == 0) {
				  
		      } else if (lengthElements.size() == 1) {
		            Length length = lengthElements.get(0);
		            String id = length.getAtomRefs2()[1];
		            Atom atom0 = molecule.getAtomById(id);
		            if (atom0 == null) {
		                throw new RuntimeException("Cannot find atom: "+id);
		            }
		            id = length.getAtomRefs2()[0];
		            Atom atom1 = molecule.getAtomById(id);
		            if (atom1 == null) {
		                throw new RuntimeException("Cannot find atom: "+id);
		            }
		            atom0.setXYZ3(new Point3d(0, 0, 0));
		            atom1.setXYZ3(new Point3d(length.value, 0, 0));
		        } else if (lengthElements.size() == 2) {
		            setCoordinates(angleElements.get(0), lengthElements.get(0),lengthElements.get(1), molecule);
		        } else if (angleElements.size() == 0) {
		        	logger.debug("BAD ZMATRIX");
		        	throw new RuntimeException("Bad zMatrix");
		        } else {
		            setCoordinates(angleElements.get(0), lengthElements.get(0), lengthElements.get(1), molecule);
		            for (int itors = 0; itors < torsionElements.size(); itors++) {
		                setXYZ3(lengthElements.get(itors+2),angleElements.get(itors+1),torsionElements.get(itors),molecule);
		            }
		        }
		    }

	    private void setCoordinates(Angle angle, Length length0, Length length1, Molecule molecule) {
	        int i0 = -1;
	        int i2 = -1;
	        if (atomHash(length0.getAtomRefs2()).equals(atomHash(
	                angle.getAtomRefs3()[0], angle.getAtomRefs3()[1]))) {
	            i0 = 0;
	            i2 = 2;
	        } else if (atomHash(length0.getAtomRefs2()).equals(atomHash(
	                angle.getAtomRefs3()[2], angle.getAtomRefs3()[1]))) {
	            i0 = 2;
	            i2 = 0;
	        } else {
	            throw new RuntimeException("Cannot match lengths to angle");
	        }
	        String id = angle.getAtomRefs3()[i0];
	        Atom atom0 = molecule.getAtomById(id);
	        if (atom0 == null) {
	            throw new RuntimeException("Cannot find atom: "+id);
	        }
	        id = angle.getAtomRefs3()[1];
	        Atom atom1 = molecule.getAtomById(id);
	        if (atom1 == null) {
	            throw new RuntimeException("Cannot find atom: "+id);
	        }
	        id = angle.getAtomRefs3()[i2];
	        Atom atom2 = molecule.getAtomById(id);
	        atom0.setXYZ3(new Point3d(0, 0, 0));
	        double l0 = length0.getValue();
	        atom1.setXYZ3(new Point3d((float) l0, 0, 0));
	        double l1 = length1.getValue();
	        double anglex = angle.getValue()*Math.PI / 180.;
	        double sina = Math.sin(anglex);
	        double cosa = Math.cos(anglex);
	        atom2.setXYZ3(new Point3d((float)l0 - l1*cosa, (float)l1*sina, 0.0));
	    }

	    /** set cartesian coordinates on atom from internal coordinates.
	     * all parameters must be organised in ZMatrix convention
	     * coordinates on a4 are altered. atoms a1, a2, a3, a4 must exist
	     * and a1, a2, a3 must have coordinates (XYZ3)
	     * @param length a3-a4
	     * @param angle a2-a3-a4
	     * @param torsion a1-a2-a3-a4
	     * @param molecule
	     */
	    public void setXYZ3(Length length, Angle angle, Torsion torsion, Molecule molecule) {
	        String[] atomRefs4 = torsion.getAtomRefs4();
	        Atom atom0 = molecule.getAtomById(atomRefs4[0]);
	        Atom atom1 = molecule.getAtomById(atomRefs4[1]);
	        Atom atom2 = molecule.getAtomById(atomRefs4[2]);
	        Atom atom3 = molecule.getAtomById(atomRefs4[3]);
	        Point3d p0 = atom0.getXYZ3();
	        Point3d p1 = atom1.getXYZ3();
	        Point3d p2 = atom2.getXYZ3();
	        Point3d p3 = atom3.getXYZ3();
	        if (p0 == null) {
	            throw new RuntimeException("should not be null p0 "+atom0.getId());
	        }
	        if (p1 == null) {
	            throw new RuntimeException("should not be null p1 "+atom1.getId());
	        }
	        if (p2 == null) {
	            throw new RuntimeException("should not be null p2 "+atom2.getId());
	        }
	        if (p3 != null) {
	            throw new RuntimeException("should be null p3 "+atom3.getId());
	        }
	        
	        Vector3d v01 = new Vector3d(p0);
	        v01.sub(p1);
	        Vector3d v12 = new Vector3d(p2);
	        v12.sub(p1);
	        
	        Vector3d cross1 = new Vector3d();
	        cross1.cross(v01, v12);
	        cross1.normalize();
	       		        
	        Vector3d cross2 = new Vector3d();
	        cross2.cross(v12, cross1);
	        cross2.normalize();
	       
	        double tangle = torsion.getValue()*Math.PI / 180.;
	        
	        Vector3d cross1a  = new Vector3d(cross1);
	        cross1a.scale(Math.sin(-tangle));
	        
	        Vector3d cross2a = new Vector3d(cross2);
	        cross2a.scale(Math.cos(tangle));
	        
	        Vector3d cross3 = new Vector3d(cross1a);
	        cross3.add(cross2a);
	        cross3.normalize();
	        
	        Vector3d v12a = new Vector3d(v12);
	        v12a.normalize();
	        
	        double len = length.getValue();
	        double ang = angle.getValue()*Math.PI / 180.;
	        
	        
	        p3 = new Point3d(p2);
	        cross3.scale(len * Math.sin(ang));		        
	        p3.add(cross3);		        		    
	        v12a.scale(len * Math.cos(ang));		        
	        p3.sub(v12a);
	        atom3.setXYZ3(p3);
	    }
		     
	    public String atomHash(String[] atomRefs2) {
			return (atomRefs2 == null) ? null
					: atomHash(atomRefs2[0], atomRefs2[1]);
		}
	    
	    public String atomHash(String atomId1, String atomId2) {
	    	if (atomId1 == null || atomId2 == null) {
	    		return null;
	    	}
	    	if (atomId1 == atomId2) {
	    		return null;
	    	}
	    	if (atomId1.compareTo(atomId2) < 0) {
	    		String temp = atomId2;
	    		atomId2 = atomId1;
	    		atomId1 = temp;
	    	}
	    	return atomId1 + "__" + atomId2;
	    }

	}

	public boolean isEmpty() {
		return atoms.size() == 0;		
	}

	public int getAtomsSize() {
		return atoms.size();		
	}

	public Iterator<Atom> getAtomIterator() {
		return atoms.values().iterator();
	}
	
	public Atom[] getAtomArray(){
		return atoms.values().toArray(new Atom[atoms.values().size()]);
		
	}

	public void addBondArray(Integer[] firstAtomIds, Integer[] secondAtomIds, Bond.BondType[] bondTypes) {
		for(int inx = 0; inx < firstAtomIds.length; inx++){
			String fromAtomId = "a" + firstAtomIds[inx];
			String toAtomId = "a" + secondAtomIds[inx];
			Bond.BondType bondType = bondTypes[inx];
			Bond bond = new Bond(fromAtomId, toAtomId, bondType);
			bonds.add(bond);
		}		
	}
	
	public Iterator<Bond> getBondIterator() {
		return bonds.iterator();
	}
	
	public Bond[] getBondArray(){
		return bonds.toArray(new Bond[bonds.size()]);
	}
	
}



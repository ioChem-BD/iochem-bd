/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;


public class ConnectionHelper{ 

	private static final Logger log = LogManager.getLogger(ConnectionHelper.class.getName());
	
    public final static float BOND_TOLERANCE = 0.35f;		// >= 0.45f will generate too much extra bonds  
    public final static float MIN_BOND_DISTANCE = 0.4f;
    public final static float MIN_BOND_DISTANCE2 = MIN_BOND_DISTANCE * MIN_BOND_DISTANCE;
    
    public static void connectTheDots(Molecule mol){
		List<Atom> atoms = Arrays.asList(mol.getAtoms());
    	Vector<String> connections = new Vector<>();    	
	    double d2;	    
	    double maxBondingRadius = Double.MIN_VALUE;
	    
	    KDTree kd = new KDTree(Arrays.asList(mol.getAtoms()));
		
		Iterator<Atom> ait = mol.getAtomIterator();
	    while(ait.hasNext()){
	    	Atom a = (Atom)ait.next();			
			double fvalue = ChemicalElement.getCovalentRad(a.getAtomicNumber());
	        if(fvalue > maxBondingRadius)
	        	maxBondingRadius = fvalue;
	    }

	    
	    ArrayList<Integer> firstList = new ArrayList<>();
	    ArrayList<Integer> secondList = new ArrayList<>();
	    ArrayList<Bond.BondType> orderList = new ArrayList<>();
	    ait = mol.getAtomIterator();
	    while(ait.hasNext()){
	    	Atom a = ait.next();
	    	double myBondingRadius = ChemicalElement.correctedBondRad(a.getAtomicNumber(), 3);
	    	double searchRadius = myBondingRadius + maxBondingRadius + BOND_TOLERANCE;
	    	//List<Atom> nearestAtoms = kd.nearestNeighbors(mol.getAtomArray(), a, searchRadius);
			List<Atom> nearestAtoms = kd.searchNearestAtoms(a, searchRadius);
			nearestAtoms.remove(a);	// remove itself
			for(Atom b : nearestAtoms){				
				log.debug(a.getId() + " is near " + b.getId());
				double bBondingRadius = ChemicalElement.correctedBondRad(b.getAtomicNumber(), 3);
				d2 = Math.pow(a.getX3() - b.getX3(),2) + Math.pow(a.getY3() - b.getY3(),2) + Math.pow(a.getZ3() - b.getZ3(),2);
				short order = getBondOrder((float)myBondingRadius,(float)bBondingRadius ,(float)d2, MIN_BOND_DISTANCE2,BOND_TOLERANCE);
				if (order > 0) {
					if(connections.contains(a.getId() + "_" + b.getId()))
						continue;	                    
					if (ChemicalElement.isHydrogen(a) && ChemicalElement.isHydrogen(b))
						continue;	
					if(a.getId().equals(b.getId()))
						continue;
					firstList.add(a.getIndex());
					secondList.add(b.getIndex());
					orderList.add(Bond.BondType.SINGLE);					
					connections.add(a.getId() + "_" + b.getId());
					connections.add(b.getId() + "_" + a.getId());
					}                    						
			}
	    }
	    Integer[] firstAtomIds = firstList.toArray(new Integer[firstList.size()]);
	    Integer[] secondAtomIds = secondList.toArray(new Integer[secondList.size()]);
	    Bond.BondType[] bondTypes = orderList.toArray(new Bond.BondType[orderList.size()]);
	    mol.addBondArray(firstAtomIds, secondAtomIds, bondTypes);	
    }
    
    private static short getBondOrder(float bondingRadiusA, float bondingRadiusB, float distance2, float minBondDistance2, float bondTolerance) {
    	if (bondingRadiusA == 0 || bondingRadiusB == 0 || distance2 < minBondDistance2)
    		return 0;
    	float maxAcceptable = bondingRadiusA + bondingRadiusB + bondTolerance;
    	float maxAcceptable2 = maxAcceptable * maxAcceptable;
    	return (distance2 > maxAcceptable2 ? (short) 0 : (short) 1);
    }

	// private IAtomContainer toAtomContainer(Molecule mol) {
	// 	IAtomContainer atomContainer = new org.openscience.cdk.AtomContainer();
	// 	for(Atom a : mol.getAtoms()) {
	// 		org.openscience.cdk.interfaces.IAtom atom = new org.openscience.cdk.Atom(a.getElementType());			
	// 		atom.setPoint3d(new javax.vecmath.Point3d(a.getX3(), a.getY3(), a.getZ3()));
	// 		if(atom.getFractionalPoint3d() != null)
	// 			atom.setFractionalPoint3d(new javax.vecmath.Point3d(atom.getFractionalPoint3d().x, atom.getFractionalPoint3d().y, atom.getFractionalPoint3d().z));
	// 		atomContainer.addAtom(atom);
	// 	}
	// }
}



class KDTree {
	HashMap<Atom, Node> atomNodeMap = new HashMap<>();

    private static class Node {
        Atom atom;
        Node left;
        Node right;

        Node(Atom atom) {
            this.atom = atom;
            left = null;
            right = null;
        }
    }

    private Node root;

    public KDTree(List<Atom> atoms) {
        root = buildTree(atoms, 0);
    }

    private Node buildTree(List<Atom> atoms, int depth) {
        if (atoms.isEmpty())
            return null;

        int axis = depth % 3; // Assuming 3D points

        atoms.sort((a, b) -> Double.compare(getCoordinate(a, axis), getCoordinate(b, axis)));

        int medianIndex = atoms.size() / 2;
        Node medianNode = new Node(atoms.get(medianIndex));
		atomNodeMap.put(atoms.get(medianIndex), medianNode);

        List<Atom> leftAtoms = new ArrayList<>(atoms.subList(0, medianIndex));
        List<Atom> rightAtoms = new ArrayList<>(atoms.subList(medianIndex + 1, atoms.size()));

        medianNode.left = buildTree(leftAtoms, depth + 1);
        medianNode.right = buildTree(rightAtoms, depth + 1);

        return medianNode;
	}

	public List<Atom> searchNearestAtoms(Atom targetAtom, double radius) {
        List<Atom> nearestAtoms = new ArrayList<>();
        searchNearestAtoms(root, targetAtom, radius, 0, nearestAtoms);
        return nearestAtoms;
    }

    private void searchNearestAtoms(Node node, Atom targetAtom, double radius, int depth, List<Atom> nearestAtoms) {
        if (node == null)
            return;

        double distance = distance(node.atom, targetAtom);
        if (distance <= radius)
            nearestAtoms.add(node.atom);

        int axis = depth % 3;
        double axisDistance = getCoordinate(targetAtom, axis) - getCoordinate(node.atom, axis);

        if (axisDistance < 0) {
            searchNearestAtoms(node.left, targetAtom, radius, depth + 1, nearestAtoms);
            if (Math.abs(axisDistance) <= radius)
                searchNearestAtoms(node.right, targetAtom, radius, depth + 1, nearestAtoms);
        } else {
            searchNearestAtoms(node.right, targetAtom, radius, depth + 1, nearestAtoms);
            if (axisDistance <= radius)
                searchNearestAtoms(node.left, targetAtom, radius, depth + 1, nearestAtoms);
        }
    }

    private double distance(Atom a1, Atom a2) {
        double dx = a1.getX3() - a2.getX3();
        double dy = a1.getY3() - a2.getY3();
        double dz = a1.getZ3() - a2.getZ3();
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    private double getCoordinate(Atom atom, int axis) {
        switch (axis) {
            case 0:
                return atom.getX3();
            case 1:
                return atom.getY3();
            case 2:
                return atom.getZ3();
            default:
                throw new IllegalArgumentException("Invalid axis index");
        }
    }
}

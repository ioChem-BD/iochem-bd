/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

public class Point3d {
	public double x = 0.0d;
	public double y = 0.0d;
	public double z = 0.0d;

	public Point3d() {
		this.x = 0.0d;
		this.y = 0.0d;
		this.z = 0.0d;	
	}
	
	public Point3d(Double x, Double y, Double z) {
		this.x = x; 
		this.y = y;
		this.z = z;		
	}

	public Point3d(double x, int y, int z) {
		this.x = x; 
		this.y = y;
		this.z = z;
	}

	public Point3d(Point3d p2) {
		this.x = p2.x; 
		this.y = p2.y;
		this.z = p2.z;
	}

	public final void add(Vector3d v1){ 
        this.x += v1.x;
        this.y += v1.y;
        this.z += v1.z;
    }
	 
	public final void sub(Vector3d v1){ 
	    this.x -= v1.x;
	    this.y -= v1.y;
	    this.z -= v1.z;
	}
	
}

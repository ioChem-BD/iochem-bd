/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jumbo2.matchers.record.RecordField;
import jumbo2.matchers.record.RecordRegexProcessor;
import jumbo2.matchers.record.RecordField.DataType;
import jumbo2.navigator.text.filecontentmanager.FileContentManager;
import jumbo2.navigator.text.filecontentmanager.helper.Section;
import jumbo2.util.Constants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class RecordMatcher {
	Logger logger = LogManager.getLogger(RecordMatcher.class.getName());
	
	private String id = null;
	private boolean makeArray = false;
	private String repeat = "1";
	private String innerText = null;
	private boolean isEmptyRecord = true;
	private RecordRegexProcessor recordRegexProcessor = null;
	
	public void processAttribute(Node attribute, String indentation){
		if(attribute.getNodeName().equals("id"))			
			id = attribute.getNodeValue();		
		else if(attribute.getNodeName().equals("makeArray"))
			makeArray = attribute.getNodeValue().toUpperCase().equals("TRUE")?true:false;					
		else if(attribute.getNodeName().startsWith("repeat"))
			repeat = attribute.getNodeValue();
		else 
			logger.warn("Record: Not coded attribute action : " + attribute.getNodeName() + " = " + attribute.getNodeValue() + "; ");		
		logger.debug(indentation + attribute.getNodeName()  + " : " + attribute.getNodeValue());			
	}
	
	public void processInnerText(String innerText, String indentation) throws Exception{
		this.innerText = innerText;	
		if(!this.innerText.equals("") && this.innerText != null){
			isEmptyRecord = false;
			recordRegexProcessor = new RecordRegexProcessor(innerText, indentation);					
			logger.debug(indentation + "Inner Regex : " + recordRegexProcessor.getFinalRegex());
		}			
	}
	
	public void readLines(FileContentManager fileManager, Node resultNode, String parentUUID, String indent) throws Exception{
		Section section = null;
		try{
			section = fileManager.getUnProcessedSections(parentUUID).get(0);
		}catch(Exception e){
			logger.debug("Record didn't match reading parentUUID child:" + parentUUID );
			return;
		}
		if(isEmptyRecord){	//There will be only one section holding this record, it's container TODO: Check if an inner <template> that leaves blanks spaces on upper side can break this process = SURELY!!!
			resultNode.appendChild(buildEmptyList(resultNode));
			for(int inx = 0; inx < section.getLines().size(); inx++){//TODO:check real line number calculation
				fileManager.setLineAsProcessed(section.getOffset() + inx + 1); 				
				if(!repeat.equals("*"))
						if(Integer.valueOf(repeat) == inx + 1)
							break;
			}
		}else{
			Pattern p = Pattern.compile(recordRegexProcessor.getFinalRegex());
			ArrayList<Matcher> matchersArray = new ArrayList<Matcher>();
			for(int inx = 0; inx < section.getLines().size(); inx++){
				Matcher matcher = p.matcher(section.getLines().get(inx));				
				if(matcher.matches()){
					fileManager.setLineAsProcessed(section.getOffset()+ inx + 1);
					matchersArray.add(matcher);
					if(repeat.equals("1"))										
						break;			//(makeArray=false or undefined) and (repeat=1 or undefined) and 1 match reached		
					else						
						if(!repeat.equals("*"))
							if(Integer.valueOf(repeat) == inx + 1)																			
								break;	//makeArray=true repeat=X and X matches reached												
				}else{
					if(!repeat.equals("*"))																								//repeat=X and no enough matches
						logger.debug("Record reading failed for record :" + innerText + " and line : " + section.getLines().get(inx) +" on parentUUID :" + parentUUID);//throw new Exception("Record reading failed for record :" + innerText + " and line : " + section.getLines().get(inx) +" on parentUUID :" + parentUUID);
					else						
						break; //repeat=* and no more matches
					
					
				}
			}
			if(matchersArray.size()> 0)		
				resultNode.appendChild(buildListFromMatchers(resultNode, recordRegexProcessor, matchersArray, makeArray, repeat, indent));	//makeArray=true repeat=* and reached end of section
		}
	}

	private Node buildEmptyList(Node parentNode){
		Element list = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");
		list.setAttributeNS(Constants.cmlxPrefixNamespace[1], "cmlx:templateRef", id==null?"missingID":id);
		Element subList = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");
		list.appendChild(subList);
		return list;
	}
	
	private Node buildListFromMatchers(Node parentNode, RecordRegexProcessor recordRegexProcessor, ArrayList<Matcher> matchersArray, boolean makeArray, String repeat, String indent) throws DOMException, Exception{
		Element list = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");
		list.setAttributeNS(Constants.cmlxPrefixNamespace[1], "cmlx:templateRef", id==null?"missingID":id);
		LinkedHashMap<String,RecordField> fields = recordRegexProcessor.getQNameAndRecordFields();
		
		if(makeArray)
			list.setAttributeNS(Constants.cmlxPrefixNamespace[1], "cmlx:lineCount" , String.valueOf(matchersArray.size()));

		if(matchersArray.size() == 1){
			Element subList = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace,  "list");
			if(!makeArray && fields.size() > 1)		//Special case of Jumbo-converters (maybe bug) when building CML records with multiple fields and makeArray = false
				list.appendChild(subList);
			for(String qname : fields.keySet()){	
				RecordField recordField = fields.get(qname);							
				if(!makeArray){
					if(fields.size() > 1){
						if(recordField.isArray())
							subList.appendChild(buildArrayNode(parentNode, recordField, matchersArray.get(0)));	
						else
							subList.appendChild(buildScalarNode(parentNode, recordField, matchersArray.get(0)));											
					}else{
						if(recordField.isArray())
							list.appendChild(buildArrayNode(parentNode, recordField, matchersArray.get(0)));	
						else
							list.appendChild(buildScalarNode(parentNode, recordField, matchersArray.get(0)));
					}
				}else{
					
					list.appendChild(buildArrayNode(parentNode, recordField, matchersArray.get(0)));	
				}
			}
		}else{
			if(makeArray){			
				for(String qname : fields.keySet()){	
					RecordField recordField = fields.get(qname);
					ArrayList<String> namedGroups = recordField.getNamedGroups();
					ArrayList<String> values = new ArrayList<String>();
					for(Matcher matcher : matchersArray)
						for(String namedGroup : namedGroups)
							if(matcher.group(namedGroup)!=null)
								values.add(matcher.group(namedGroup));
					list.appendChild(buildArrayNode(parentNode, recordField, values));	
				}
			}else{				
				for(Matcher matcher : matchersArray){
					Element subList = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace,  "list");
					if(!makeArray && fields.size() > 1)		//Special case of Jumbo-converters (maybe bug) when building CML records with multiple fields and makeArray = false
						list.appendChild(subList);
					for(String qname : fields.keySet()){	
						RecordField recordField = fields.get(qname);
						if(fields.size() > 1){
							if(recordField.getNamedGroups().size() == 1)
								subList.appendChild(buildScalarNode(parentNode, recordField, matcher));
							else							
								subList.appendChild(buildArrayNode(parentNode, recordField, matcher));													
						}else{
							if(recordField.getNamedGroups().size() == 1)
								list.appendChild(buildScalarNode(parentNode, recordField, matcher));
							else							
								list.appendChild(buildArrayNode(parentNode, recordField, matcher));
						}
							
					}
				}
			}
		}	
		return list;		
	}
		
	private Node buildScalarNode(Node parentNode, RecordField recordField, Matcher matcher) throws DOMException, Exception{
		Element scalar = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");
		if(recordField.getQName() != null)		
			scalar.setAttribute("dictRef", recordField.getQName());
		scalar.setAttribute("dataType", getDataTypeString(recordField.getDataType()));
		String value = normalizeFields(matcher.group(recordField.getNamedGroups().get(0)).trim(), recordField.getDataType()); 				
		scalar.setTextContent(value);
		return scalar;
	}
		
	private Node buildArrayNode(Node parentNode, RecordField recordField, Matcher matcher) throws Exception{
		Element array = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "array");
		if(recordField.getQName() != null)
			array.setAttribute("dictRef", recordField.getQName());
		StringBuilder sb = new StringBuilder();
		int counter = 0;
		for(String namedGroup : recordField.getNamedGroups()){
			if(matcher.group(namedGroup) != null){
				sb.append(normalizeFields(matcher.group(namedGroup).trim(), recordField.getDataType())    + " ");
				counter++;				
			}else
				break;				
		}
		array.setAttribute("dataType", getDataTypeString(recordField.getDataType()));
		array.setAttribute("size", String.valueOf(counter));
		array.setTextContent(sb.toString().trim());
		return array;		
	}
	
	private Node buildArrayNode(Node parentNode, RecordField recordField, ArrayList<String> values) throws DOMException, Exception{
		Element array = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "array");
		array.setAttribute("size", String.valueOf(values.size()));		
		array.setAttribute("dataType", getDataTypeString(recordField.getDataType()));
		if(recordField.getQName() != null)
			array.setAttribute("dictRef", recordField.getQName());
		StringBuilder sb = new StringBuilder();
		char delimiter = ' ';
		if(getDataTypeString(recordField.getDataType()).equals("xsd:string"))
			delimiter = findValidDelimiter(values);
		if(delimiter != ' ')
			array.setAttribute("delimiter", String.valueOf(delimiter));
		sb.append(delimiter);
		for(String value : values)
			sb.append(normalizeFields(value.trim(), recordField.getDataType()) + delimiter);			
		array.setTextContent(sb.toString().trim());		
		return array;
	}
	
	public String getDataTypeString(DataType dataType) throws Exception{
		switch(dataType){
		case BOOLEAN:   return "xsd:boolean";
		case INTEGER: 	return "xsd:integer";
		case FLOAT	:					
		case SCIENTIFIC:return "xsd:double";
		case STRING :	return "xsd:string";					
		case WORD	:	return "xsd:string";
		default		: 	throw new Exception("Unmanaged element dataType");			
		}
	}
	
	public char findValidDelimiter(ArrayList<String> values){
		String delimiters = " |#_"; 
		for(int inx = 0; inx < delimiters.length(); inx++){
			char delimiter = delimiters.charAt(inx);
			boolean delimiterFoundInsideValue = false;
			for(String value: values)
				if(value.trim().contains(String.valueOf(delimiter))){
					delimiterFoundInsideValue  = true;
					break;
			}
			if(!delimiterFoundInsideValue)
				return delimiter;
		}		
		return ' ';
	}
	
	private String normalizeFields(String value, DataType dataType){
		switch(dataType){
			case BOOLEAN:  Pattern p = Pattern.compile("(?i)(true|1|T|yes)");
						   Matcher m = p.matcher(value.trim());
						   if(m.matches())
							   return "true";
						   else
							   return "false";				
			case STRING:
			case WORD:			
			case INTEGER:		
							return value;
			case SCIENTIFIC:
							value = value.replaceAll("[dD]", "e");							
							return value;
			default:
							return value;
		}
	}
	
}

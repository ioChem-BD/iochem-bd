/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import jumbo2.chemistry.Atom;
import jumbo2.chemistry.ChemicalElement;
import jumbo2.chemistry.ConnectionHelper;
import jumbo2.chemistry.Molecule;
import jumbo2.chemistry.MoleculeElement;
import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CreateMolecule extends TransformMatcher {
	
	public CreateMolecule(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		if(attributes.containsKey("mode")){					//New version of createMolecule transform
			if(attributes.get("mode").equals("multiple")){
				XPathExpression expr = xpath.compile(attributes.get("xpath"));
				NodeList resultNodes = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);				
				for(int inx = 0; inx < resultNodes.getLength(); inx++){
					Element parent = (Element)resultNodes.item(inx);
					NodeList result = parent.getElementsByTagName("array");
					if(result.getLength() > 0){			
						Element element = (Element)result.item(0);						
						Element dummy = element.getOwnerDocument().createElement("dummy");					
						if (!element.getNodeName().equals("array")) {
							throw new RuntimeException(
							"Molecule requires list of arrays, but found: "+element.getClass().getName()+";"+element.getLocalName());
						}
						
						int natoms = Integer.parseInt(element.getAttribute("size"));
						Molecule molecule = new Molecule(attributes.get("id"));
						parent.insertBefore(dummy ,element);
						Atom[] atomArray = createAtoms(natoms);		
						for(Atom atom: atomArray)
							molecule.addAtom(atom);			
						try {
							addArraysMultiple(atomArray, result, natoms);				
						} catch (Exception e) {
							parent.removeChild(dummy);
							logger.error("cannot create molecule: "+e);
							return;
						}
						addElementTypes(parentNode.getOwnerDocument(), molecule, atomArray);
						
						if(!attributes.containsKey("connect") || attributes.get("connect").equals("yes"))
						    ConnectionHelper.connectTheDots(molecule);
									
						MoleculeElement moleculeElement = new MoleculeElement(factory, namespaceContext, parentNode.getOwnerDocument(), molecule); 
						Node moleculeNode =moleculeElement.buildNode(); 			
						parent.replaceChild(moleculeNode, dummy);
					}
				}
			}
		}else{		//Old version of createMolecule transform
			XPathExpression expr = xpath.compile(attributes.get("xpath"));		
			NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);		
			if(result.getLength() > 0){			
				Element element = (Element)result.item(0);
				Element parent = (Element) element.getParentNode();
				Element dummy = element.getOwnerDocument().createElement("dummy");					
				if (!element.getNodeName().equals("array")) {
					throw new RuntimeException(
					"Molecule requires list of arrays, but found: "+element.getClass().getName()+";"+element.getLocalName());
				}
				
				int natoms = Integer.parseInt(element.getAttribute("size"));
				Molecule molecule = new Molecule(attributes.get("id"));
				parent.insertBefore(dummy ,element);
				Atom[] atomArray = createAtoms(natoms);		
				for(Atom atom: atomArray)
					molecule.addAtom(atom);			
				try {
					addArrays(atomArray, result, natoms);				
				} catch (Exception e) {
					parent.removeChild(dummy);
					logger.error("cannot create molecule: "+e);
					return;
				}
				addElementTypes(parentNode.getOwnerDocument(), molecule, atomArray);
				if(!attributes.containsKey("connect") || attributes.get("connect").equals("yes"))
				    ConnectionHelper.connectTheDots(molecule);
							
				MoleculeElement moleculeElement = new MoleculeElement(factory, namespaceContext, parentNode.getOwnerDocument(), molecule); 
				Node moleculeNode =moleculeElement.buildNode(); 			
				parent.replaceChild(moleculeNode, dummy);
			}	
		}
		logger.debug(indent + "transform createMolecule :" + attributes.get("xpath"));		
	}

	private Atom[] createAtoms(int nAtoms){
		Atom[] atomArray = new Atom[nAtoms];
		for (int iatom = 0; iatom < nAtoms; iatom++) {
			String id = "a"+(iatom+1);
			Atom newAtom = new Atom(id);
			atomArray[iatom] = newAtom;
		}
		return atomArray;		
	}	
	
	
	private void addArraysMultiple(Atom[] atoms, NodeList nodeList, int natoms) {
		int numNodes = nodeList.getLength();
		for(int inx = 0; inx < numNodes; inx++){
			Node node = nodeList.item(0);			
			if(!(node.getNodeName().equals("array"))) {
				logger.debug("molecule only operates on arrays");
				throw new RuntimeException("molecule only operates on arrays");
			}
			Element array = (Element) node;
			if(Integer.valueOf(array.getAttribute("size")) != natoms) {
				throw new RuntimeException("addArrays (" + inx + ")out of sync with atoms: " + array.getAttribute("size") + " != " + natoms);
			}
			processDictRef(atoms, array);
			array.getParentNode().removeChild(array);			
		}
	}
	
	private void addArrays(Atom[] atoms, NodeList nodeList, int natoms) {		
		for(int inx = 0; inx < nodeList.getLength(); inx++){
			Node node = nodeList.item(inx);			
			if(!(node.getNodeName().equals("array"))) {
				logger.debug("molecule only operates on arrays");
				throw new RuntimeException("molecule only operates on arrays");
			}
			Element array = (Element) node;
			if(Integer.valueOf(array.getAttribute("size")) != natoms) {
				throw new RuntimeException("addArrays (" + inx + ")out of sync with atoms: " + array.getAttribute("size") + " != " + natoms);
			}
			processDictRef(atoms, array);
			array.getParentNode().removeChild(array);			
		}
	}

	
	private void processDictRef(Atom[] atoms, Element array){
		String dictRefAttribute = array.getAttribute("dictRef");
		if (dictRefAttribute == null) 
			throw new RuntimeException("Array must have dictRef");
		if(dictRefAttribute.contains(":"))
			dictRefAttribute = dictRefAttribute.split(":")[1];
		Pattern p = Pattern.compile("x3|y3|z3|id|elementType|label|atomTypeRef");
		Matcher m = p.matcher(dictRefAttribute);
		if (m.matches())
			addScalar(atoms, array, dictRefAttribute);
		else 
			addCMLAttribute(atoms, array, dictRefAttribute);
	}

	private void addScalar(Atom[] atoms, Element array, String localName) {
		String[] values = getStrings(array);
		double[] doubles = getDoubles(array);
		int[] ints = getInts(array);
		try {
		for (int i = 0; i < atoms.length; i++) {
			Atom atom = atoms[i];
			if (localName.equals("x3")) {
				atom.setX3(doubles[i]);
			} else if (localName.equals("y3")) {
				atom.setY3(doubles[i]);
			} else if (localName.equals("z3")) {
				atom.setZ3(doubles[i]);
			} else if (localName.equals("id")) {
				addId(array, values, ints, i, atom);
			} else if (localName.equals("elementType")) {
				addElementType(array, values, doubles, ints, i, atom);
			}
		}
		} catch (Exception e) {
			logger.error( "" + localName + doubles);
			throw new RuntimeException("problem in adding Scalar doubles: " + e.getLocalizedMessage(), e);
		}
	}
	
	private void addCMLAttribute(Atom[] atoms, Element array, String localName) {
		String[] values = getStrings(array);
		double[] doubles = getDoubles(array);
		int[] ints = getInts(array);
		String dictRef = array.getAttribute("dictRef");
		// we need a getScalar method
		for (int i = 0; i < Integer.parseInt(array.getAttribute("size")); i++) {
			Element scalar = array.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");			
			String dataType = array.getAttribute("dataType");
			if (dataType.equals("xsd:string")) 
				scalar.setTextContent(values[i]);
			else if (dataType.equals("xsd:double")) 
				scalar.setTextContent(String.valueOf(doubles[i]));
			else if (dataType.equals("xsd:integer")) 				
				scalar.setTextContent(String.valueOf(ints[i]));			
			scalar.setAttribute("dictRef", dictRef);
			scalar.setAttribute("dataType", dataType);
			atoms[i].addScalar(scalar);
		}
	}

	private void addId(Element array, String[] values, int[] ints, int i, Atom atom) {
		String id = null;
		if (array.getAttribute("dataType").equals("xsd:string")) 
			id = values[i];
		else if (array.getAttribute("dataType").equals("xsd:integer"))
			id = "a"+ints[i];
		
		if (id == null) 
			throw new RuntimeException("Cannot make id");
		atom.setId(id);
	}

	private void addElementType(Element array, String[] values, double[] doubles, int[] ints, int i, Atom atom) throws Exception {
		String elem = null;
		ChemicalElement chemicalElement = null;
		if (array.getAttribute("dataType").equals("xsd:string")) {
			elem = values[i];
			if (elem.length() > 2) {
				elem = elem.substring(0,2);
			}
			if (elem.length() == 2) {
				if (Character.isLetter(elem.charAt(1))) {
					elem = elem.substring(0, 1).toUpperCase()+elem.substring(1,2).toLowerCase();
				} else {
					elem = elem.substring(0, 1);
				}
			}
			if (elem.length() == 1) elem = elem.substring(0, 1).toUpperCase();
			chemicalElement = ChemicalElement.getChemicalElement(elem);
			if (chemicalElement == null) {
				throw new RuntimeException("Unknown element: "+values[i]);
			}
		} else if (array.getAttribute("dataType").equals("xsd:integer")){
			chemicalElement = ChemicalElement.getElement(ints[i]);
		} else if (array.getAttribute("dataType").equals("xsd:double")){
			chemicalElement = ChemicalElement.getElement((int)doubles[i]);
		}
		if (chemicalElement == null) {
			throw new RuntimeException("Cannot make chemical element");
		}
		atom.setElementType(chemicalElement.getSymbol());
	}
		
	public String[] getStrings(Element array) {
		String[] ss = null;
		if (array.getAttribute("dataType").equals("xsd:string")) 
			ss = getSplitContent(array);		
		return ss;
	}
	
	private double[] getDoubles(Element array){
		double[] dd = null;
		String dataType = array.getAttribute("dataType");
		if (dataType != null && dataType.equals("xsd:double")) {
			String[] ss = getSplitContent(array);
			dd = new double[ss.length];
			for (int i = 0; i < dd.length; i++) {
				try {
					dd[i] = Double.parseDouble(ss[i]);
				} catch (Exception e) {
					throw new RuntimeException("Bad double : " + ss[i] + "at position " + i, e);
				}
			}
		}
		return dd;
	}
		
	private int[] getInts(Element array){
		int[] ii = null;
		String dataType = array.getAttribute("dataType");
		if (dataType.equals("xsd:integer")) {
			String[] ss = getSplitContent(array);
			ii = new int[ss.length];
			for (int i = 0; i < ii.length; i++) {
				try {
					ii[i] = new Integer(ss[i]).intValue();
				} catch (NumberFormatException nfe) {
					throw new RuntimeException("Bad int (" + ss[i] + ") at position: " + i);
				}
			}
		}
		return ii;
	}
	
	private String[] getSplitContent(Element array) throws RuntimeException {
		String[] ss = new String[0];
		String content = array.getTextContent();
		if (content != null) {
			content = content.trim();
			String delimiter = array.hasAttribute("delimiter")? array.getAttribute("delimiter"): " ";
			ss = getSplitContent(content, delimiter);
		}
		if(array.hasAttribute("delimiter") && array.getAttribute("delimiter").matches(Constants.WHITESPACES_REGEX))
			array.removeAttribute("delimiter");
		return ss;
	}
	
	public String[] getSplitContent(String content, String delimiter) {
	        String[] ss = new String[0];
		    content = content.trim();
		    if (content.length() > 0) 
		        ss = content.split("\\" +delimiter);		    	
		    return ss;
	}

	private void addElementTypes(Document doc, Molecule molecule, Atom[] atoms) throws Exception {
		for (Atom atom : atoms) {
			String elementType = atom.getElementType();
			if (elementType != null) {
				ChemicalElement chemicalElement = ChemicalElement.getChemicalElement(elementType);
				Element scalar = doc.createElementNS(Constants.cmlNamespace, "scalar");
				scalar.setTextContent(String.valueOf(chemicalElement.getAtomicNumber()));
				scalar.setAttribute("dictRef", "cc:atomicNumber");			
				scalar.setAttribute("dataType", "xsd:integer");
				atom.addScalar(scalar);
			}
		}
	}
	
}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateMatrix33 extends TransformMatcher {

	public CreateMatrix33(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);
			expr = xpath.compile(attributes.get("from"));		
			NodeList nodes = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
			if(nodes.getLength() == 3*3 && nodes.item(0).getNodeName().equals("scalar"))
				addScalarsToMatrix(3, 3, node, nodes);
			else if(nodes.getLength() == 3 && nodes.item(0).getNodeName().equals("array"))
				addArraysToMatrix(3,3,nodes);
			else if(nodes.getLength() == 0){
			}else{
				logger.error("Cannot create matrix33 found " + nodes.getLength() +" nodes of type: "+nodes.item(0).getNodeName());
				//throw new RuntimeException("Cannot create matrix33 found " + nodes.getLength() +" nodes of type: "+nodes.item(0).getNodeName());		
			}			
		}
		logger.debug(indent + "transform createMatrix33 :" + attributes.get("xpath"));
	}
	
	private void addScalarsToMatrix(int rows, int cols, Node parentNode, NodeList nodes) {
		int size = rows*cols;
		double[] dd = new double[size];
		Node node0 = null;
		for (int i = 0; i < size; i++) {
			Element nodex = (Element)nodes.item(i);
			if (!(nodex.getNodeName().equals("scalar"))) {
				throw new RuntimeException("Expected scalar in 'from', found: "+ nodex.getNodeName());
			}			
			dd[i] = Double.parseDouble(nodex.getTextContent());
			if (i == 0) {
				node0 = nodex;
			} else {
				nodex.getParentNode().removeChild(nodex);
			}
		}
		Node matrix = buildMatrix(rows, cols, dd, node0);		
		node0.getParentNode().replaceChild(matrix, node0);
	}

	private void addArraysToMatrix(int rows, int cols, NodeList nodes) {
		int size = rows*cols;
		double[] dd = new double[size];
		Element array0 = (Element)nodes.item(0);
		if (Integer.parseInt(array0.getAttribute("size")) != cols) {
			throw new RuntimeException("Unexpected array size: "+ array0.getAttribute("size"));
		}
		for (int inx = 0; inx < rows; inx++) {
			Element nodex = (Element)nodes.item(inx);
			if (!nodex.getNodeName().equals("array")) 
				throw new RuntimeException("Expected array in 'from', found: "+nodex.getClass());						
			if (Integer.valueOf(nodex.getAttribute("size")) != cols || !(nodex.getAttribute("dataType").equals("xsd:double"))) 
				throw new RuntimeException("createMatrix requires arrays of "+cols+" doubles but had "+ nodex.getAttribute("size") +" in row "+ (inx + 1));
			String values[] = nodex.getTextContent().trim().split(Constants.WHITESPACES_REGEX);
			for(int inx2 = 0; inx2 < values.length; inx2++)
				dd[inx * cols + inx2] = Double.valueOf(values[inx2]);
			if(inx > 0)
				nodex.getParentNode().removeChild(nodex);
		}
		Node matrix = buildMatrix(rows, cols, dd, array0);
		array0.getParentNode().replaceChild(matrix, array0);
	}
	
	private Node buildMatrix(int rows, int cols, double[] dd, Node node) {
		StringBuilder sb = new StringBuilder();		
		for (int i = 0; i < dd.length; i++) 
			sb.append(String.valueOf(dd[i]) + " ");
		Element matrix = node.getOwnerDocument().createElementNS(Constants.cmlNamespace, "matrix");
		matrix.setAttribute("rows", String.valueOf(rows));
		matrix.setAttribute("cols", String.valueOf(cols));
		matrix.setAttribute("dictRef", attributes.get("dictRef"));	//TODO: This transform doesn't set cmlx:templateRef attribute as jumbo-converters do 
		matrix.setAttribute("dataType", ((Element)node).getAttribute("dataType"));
		matrix.setTextContent(sb.toString().trim());
		return matrix;
	}
	
}

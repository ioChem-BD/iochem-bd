package jumbo2.matchers.transform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.navigator.text.filecontentmanager.helper.ValidationException;

public class Verify extends TransformMatcher {

	public Verify(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}

    @Override
    public void transformNode(Node parentNode, String indent) throws Exception {
        XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		Boolean result = (Boolean)expr.evaluate(parentNode, XPathConstants.BOOLEAN);
        if(result){
            String errorMessage = (String)attributes.get("value");
            throw new ValidationException(errorMessage);
        }
    }
}

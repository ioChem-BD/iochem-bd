/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import jumbo2.matchers.TransformMatcher;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Copy extends TransformMatcher {

	public Copy(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		if(attributes.containsKey("multiple") && attributes.get("multiple").equals("true")){
			XPathExpression expr = xpath.compile(attributes.get("xpath"));
			NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
			for(int inx = 0; inx < result.getLength(); inx++){
				Element node = (Element)result.item(inx);						
				expr = xpath.compile(attributes.get("to"));			
				NodeList toNodes = (NodeList)expr.evaluate(node, XPathConstants.NODESET);				
				if(toNodes == null)
					continue;
				for(int inx2 = 0; inx2 < toNodes.getLength(); inx2++){
					Element toNode = (Element)toNodes.item(inx2);
					Element clonedNode = (Element) node.cloneNode(true);
					clonedNode.setAttribute("id", toNode.hasAttribute("id")? toNode.getAttribute("id") + "." + String.valueOf(inx):"copy." + String.valueOf(inx));
					toNode.appendChild(clonedNode);	
				}
			}	
		}else{
			XPathExpression expr = xpath.compile(attributes.get("xpath"));
			NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
			for(int inx = 0; inx < result.getLength(); inx++){
				Element node = (Element)result.item(inx);
				Element clonedNode = (Element) node.cloneNode(true);			
				expr = xpath.compile(attributes.get("to"));			
				Element toNode = (Element)expr.evaluate(node, XPathConstants.NODE);
				if(toNode == null)
					continue;
				clonedNode.setAttribute("id", toNode.hasAttribute("id")? toNode.getAttribute("id") + "." + String.valueOf(inx):"copy." + String.valueOf(inx));
				toNode.appendChild(clonedNode);
			}	
		}
		logger.debug(indent + "transform copy :" + attributes.get("xpath"));
	}
}

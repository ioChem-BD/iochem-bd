/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AddAttribute extends TransformMatcher {

	public AddAttribute(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		String[] attnames = attributes.get("name").split(":");		
		String prefix = null;
		String uri = null;
		Integer fromPos = attributes.containsKey("fromPosition") ? new Integer(attributes.get("fromPosition")) : null;
		if (attnames.length == 2) {
			prefix = attnames[0];
			if (prefix.equals(Constants.cmlxPrefix)) {
				uri = Constants.cmlxPrefixNamespace[1];
			}
		}
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Element element = (Element)result.item(inx);
			NodeList fromNodeList = queryUsingNamespaces(element, attributes.get("from"));
			for (int i = 0; i < fromNodeList.getLength(); i++) {
				if (fromPos == null || fromPos == i+1) {
					Element fromElement = (Element) fromNodeList.item(i);
					String valuex = (String) getValue(fromElement, attributes.get("value"));
					if (valuex != null) {
						if (uri != null) {
							fromElement.setAttributeNS(uri, attributes.get("name"), valuex);
						} else {
							fromElement.setAttribute(attributes.get("name"), valuex);
						}
					}
				}
			}
		}
	}
	
}

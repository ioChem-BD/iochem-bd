/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;

public class MoveRelative extends TransformMatcher {

	public MoveRelative (String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		Integer pos = (attributes.containsKey("position")) ? new Integer(attributes.get("position")) - 1 : null;
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);
			if (node instanceof Element) {
				Element fromElement = (Element)node;
				expr = xpath.compile(attributes.get("to"));
				NodeList toNodes = (NodeList) expr.evaluate(fromElement, XPathConstants.NODESET);
				if(toNodes.getLength() == 1){
					Element toElement = (Element)toNodes.item(0);
					if(pos == null || pos == toElement.getChildNodes().getLength()){						
						node.getParentNode().removeChild(node);
						toElement.appendChild(node);
					}else if(pos >= 0 && pos < toElement.getChildNodes().getLength()){
						node.getParentNode().removeChild(node);
						toElement.insertBefore(node, toElement.getChildNodes().item(pos));
					}else{
						throw new RuntimeException("Cannot move ");
					}
				}				
			}
		}
		logger.debug(indent + "transform moveRelative :" + attributes.get("xpath"));
	}

}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateMatrix extends TransformMatcher {

	public CreateMatrix(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		NodeList base = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);		
		for(int inx = 0; inx < base.getLength(); inx++){
			Node node = (Node)base.item(inx);
			XPathExpression expr2 = xpath.compile(attributes.get("from"));
			NodeList arrays = (NodeList)expr2.evaluate(node, XPathConstants.NODESET);
			if (arrays.getLength() > 0) {
				if (arrays.item(0).getNodeName().equals("scalar")){
					throw new RuntimeException("Please create matrix from arrays, not scalars");
				} else if(arrays.item(0).getNodeName().equals("array")){
					buildMatrix(node, arrays.getLength(), Integer.valueOf(((Element)arrays.item(0)).getAttribute("size")), arrays);			
				} else {
					throw new RuntimeException("Cannot create matrix found "+ arrays.getLength() +" nodes of type: "+ arrays.item(0).getNodeName());
				}
			}

		}
		logger.debug(indent + "transform createMatrix :" + attributes.get("xpath") + " " + attributes.get("from"));
	}

	private void buildMatrix(Node baseNode, int rows, int cols, NodeList nodes) {
		StringBuilder matrixValues = new StringBuilder();
		
		Element array0 = (Element)nodes.item(0);		
		if (Integer.valueOf(array0.getAttribute("size")) != cols) {
			throw new RuntimeException("Unexpected array size: "+array0.getAttribute("size"));
		}
		matrixValues.append(array0.getTextContent() + " ");
		for (int i = 1; i < rows; i++) {
			Element nodex = (Element)nodes.item(i);
			String dataType = nodex.getAttribute("dataType");
			int size = Integer.valueOf(nodex.getAttribute("size"));			
			
			if (!(nodex.getNodeName().equals("array"))) 
				throw new RuntimeException("Expected array in 'from', found: "+nodex.getClass());						
			if (size != cols || !dataType.equals("xsd:double") && !dataType.equals("xsd:integer"))
				throw new RuntimeException("createMatrix requires arrays of "+cols+" doubles but had "+ size +" in row "+i);
			matrixValues.append(nodex.getTextContent() + " ");
			nodex.getParentNode().removeChild(nodex);			
		}
		
		Element matrix = baseNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "matrix");
		matrix.setAttribute("rows", String.valueOf(rows));
		matrix.setAttribute("cols", String.valueOf(cols));
		matrix.setAttribute("dictRef", attributes.get("dictRef"));
		matrix.setAttribute("dataType", array0.getAttribute("dataType"));
		matrix.setTextContent(matrixValues.toString().trim());
		array0.getParentNode().replaceChild(matrix, array0);
	}
	
}

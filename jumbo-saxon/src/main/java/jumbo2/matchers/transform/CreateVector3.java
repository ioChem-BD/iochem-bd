/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateVector3 extends TransformMatcher {

		public CreateVector3(String[] requiredAttributes){
			super();
			this.requiredAttributes = requiredAttributes;
		}
		
		@Override
		public void transformNode(Node parentNode, String indent) throws Exception {
			XPath xpath = factory.newXPath();
			xpath.setNamespaceContext(namespaceContext);
			XPathExpression expr = xpath.compile(attributes.get("xpath"));
			NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
			for(int inx = 0; inx < result.getLength(); inx++){
				Node node = (Node)result.item(inx);
				expr = xpath.compile(attributes.get("from"));								
				NodeList nodes = (NodeList)expr.evaluate(node, XPathConstants.NODESET);			
				double[] dd = new double[3];
				if(nodes.getLength() == 3){
					for(int inx2 = 0; inx2 < nodes.getLength(); inx2++){
						dd[inx2] = Double.parseDouble(nodes.item(inx2).getTextContent());
						if(inx2 > 0)
							nodes.item(inx2).getParentNode().removeChild(nodes.item(inx2));
					}
					Element vector3 = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "vector3");
					vector3.setAttribute("dictRef", attributes.get("dictRef"));
					
					StringBuilder values = new StringBuilder();
					for(double d: dd)
						values.append(String.valueOf(d) + " ");
					vector3.setTextContent(values.toString().trim());
					nodes.item(0).getParentNode().replaceChild(vector3, nodes.item(0));
				}else if(nodes.getLength() == 0){
					
				}else
					throw new RuntimeException("No vector3 can be made");
							
			}
			logger.debug(indent + "transform createVector3 :" + attributes.get("xpath"));		
		}
				
	}

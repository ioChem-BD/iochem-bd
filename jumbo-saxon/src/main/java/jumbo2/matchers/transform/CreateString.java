/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateString extends TransformMatcher {

	public CreateString(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		NodeList nodeList = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		
		if(nodeList.getLength() == 0){
			logger.error("createString points to no path");
//			String value = expr.evaluate(parentNode);
//			Node text = nodeList.item(0);
//			Element scalar = text.getOwnerDocument().createElementNS(PlainTextToCML.cmlNamespace, "scalar");
//			scalar.setTextContent(text.getTextContent());			
//			if (attributes.get("id") != null) 
//				scalar.setAttribute("id", attributes.get("id"));			
//			text.getParentNode().replaceChild(scalar, text);			
			
		}else if (nodeList.getLength() > 0 && ((Element)nodeList.item(0)).getNodeName().equals("scalar")) {
			StringBuilder sb = new StringBuilder();
			for (int inx = 0; inx < nodeList.getLength(); inx++) {
				Node node = nodeList.item(inx);
				sb.append(node.getTextContent());
			}
			((Element)nodeList.item(0)).setTextContent(sb.toString());
			for (int inx2 = 1; inx2 < nodeList.getLength(); inx2++) 
				nodeList.item(inx2).getParentNode().removeChild(nodeList.item(inx2));		
		} else {
			for (int inx = 0; inx < nodeList.getLength(); inx++) {
				Element node = (Element)nodeList.item(inx); 
				if (node.getNodeName().equals("array")) {
					Element scalar = node.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");
					scalar.setTextContent(node.getTextContent());
					scalar.setAttribute("dictRef", node.getAttribute("dictRef"));					
					node.getParentNode().replaceChild(scalar, node);
				}
			}
		}
		
		logger.debug(indent + "transform createString :" + attributes.get("xpath"));
	}

}

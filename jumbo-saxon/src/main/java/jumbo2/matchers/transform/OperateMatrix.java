/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import jumbo2.matchers.TransformMatcher;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class OperateMatrix extends TransformMatcher {

	public OperateMatrix(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);		
		Pattern p = Pattern.compile("\\s*operator\\s*=\\s*(\\S+)\\s*");
		Matcher m = p.matcher(attributes.get("args"));				
		if(m.matches()){			
			String operand = m.group(1);						
			for(int inx = 0; inx < result.getLength(); inx++){		
				Element node = (Element)result.item(inx);
				int rows = Integer.valueOf(node.getAttribute("rows"));
				int cols = Integer.valueOf(node.getAttribute("cols"));
				
				switch(operand){
					case "transpose":	Matrix matrix = new Matrix(rows,cols,node.getTextContent());
										node.setAttribute("rows", String.valueOf(cols));
										node.setAttribute("cols", String.valueOf(rows));
										node.setTextContent(matrix.getTransposedValues());
										break;
				}
			}
		}			
		logger.debug(indent + "transform operateScalar :" + attributes.get("xpath") + " " + attributes.get("args"));
	}
}

class Matrix{
	private int rows;
	private int cols;

	private String data[][]; 
	
	public Matrix(int rows, int cols, String values){
		this.rows = rows;
		this.cols = cols;
		data = new String[rows][cols];
		String array[] = values.trim().split("[ \t\n\f\r]+");
		for(int inx = 0; inx < rows; inx++)
			for(int inx2 = 0; inx2 < cols; inx2++)
				data[inx][inx2] = array[inx2 + inx * cols];		
	}	

	public String getTransposedValues(){
		StringBuilder sb = new StringBuilder();
		String[][] trasposed = new String[cols][rows];
		for(int inx = 0; inx < rows; inx++)
			for(int inx2 = 0; inx2 < cols; inx2++)
				trasposed[inx2][inx] = data[inx][inx2];
		
		for(int inx = 0; inx < cols; inx++)
			for(int inx2 = 0; inx2 < rows; inx2++)
				sb.append(trasposed[inx][inx2] + " ");
		
		return sb.toString().trim();
	}
	
}
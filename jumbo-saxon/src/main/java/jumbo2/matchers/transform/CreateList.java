/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateList extends TransformMatcher {

	public CreateList(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Element node = (Element)result.item(inx);
			if(node.getLocalName().equals("module")){
				Node list = buildList(parentNode.getOwnerDocument(), node);
				node.getParentNode().replaceChild(list, node);
			}
		}
		logger.debug(indent + "transform createList :" + attributes.get("xpath"));
	}
	
	private Node buildList(Document document, Element module){
		Element list = document.createElementNS(Constants.cmlNamespace, "list");
		for(int inx = 0; inx < module.getAttributes().getLength(); inx++ ){
			Node attribute = module.getAttributes().item(inx);
			if(attribute.getNamespaceURI() != null && !attribute.getNamespaceURI().equals(Constants.cmlNamespace))
				list.setAttributeNS(attribute.getNamespaceURI(), attribute.getNodeName(), attribute.getNodeValue());
			else
				list.setAttribute(attribute.getNodeName(), attribute.getNodeValue());
		}
		for(int inx = 0; inx < module.getChildNodes().getLength(); inx++)
			list.appendChild(module.getChildNodes().item(inx).cloneNode(true));
		return list;
	}
}

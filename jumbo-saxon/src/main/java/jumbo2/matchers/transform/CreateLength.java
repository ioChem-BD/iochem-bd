/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CreateLength extends TransformMatcher {

	public CreateLength (String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Element node = (Element)result.item(inx);
			String atomRefs[] = createAtomRefs(node, attributes.get("atomRefs"), 2);
			if(atomRefs != null){
				atomRefs[0] = getValue(node, atomRefs[0]);
				atomRefs[1] = getValue(node, atomRefs[1]);		
				String value = getValue(node, attributes.get("value"));
				node.appendChild(buildLength(node.getOwnerDocument(), atomRefs, value));				
			}
		}
		logger.debug(indent + "transform createLength :" + attributes.get("xpath"));
	}
	
	private Node buildLength(Document document, String[] atomRefs, String length){
		Element node = document.createElementNS(Constants.cmlNamespace, "length");
		if(!atomRefs[0].startsWith("a"))
			atomRefs[0] = "a" + atomRefs[0];
		if(!atomRefs[1].startsWith("a"))
			atomRefs[1] = "a" + atomRefs[1];
		node.setAttribute("atomRefs2", atomRefs[0] + " " + atomRefs[1]);
		node.setTextContent(length);
		return node;
	}

}

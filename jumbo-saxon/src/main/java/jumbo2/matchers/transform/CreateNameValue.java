/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateNameValue extends TransformMatcher {

	public CreateNameValue(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
	
		String regex = attributes.get("regex");
		
		Pattern pattern = null;
		if (regex == null){ 
			if(!attributes.containsKey("name") || !attributes.containsKey("value"))
				throw new Exception("name and value fields required if regex not defined");
		
		}else 
			pattern = Pattern.compile(regex);
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for (int inx = 0; inx < result.getLength(); inx++) {
			Element element = (Element)result.item(inx);			
			if (pattern != null) {
				String value = element.getTextContent();
				Matcher matcher = pattern.matcher(value);
				if (matcher.matches() && matcher.groupCount() == 2) {
					String nam = matcher.group(1);
					String val = matcher.group(2);
					Element scalar = element.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");
					scalar.setAttribute("dataType", "xsd:string");
					scalar.setTextContent(val);				
					scalar.setAttribute("dictRef","x:" + nam);					
					element.getParentNode().replaceChild(scalar, element);
				}
			} else {
				expr = xpath.compile(attributes.get("name"));				
				NodeList nameNodes = (NodeList)expr.evaluate(element, XPathConstants.NODESET);
				expr = xpath.compile(attributes.get("value"));
				NodeList valueNodes = (NodeList)expr.evaluate(element, XPathConstants.NODESET);
				if (nameNodes.getLength() == valueNodes.getLength()) {
					for (int inx2 = 0; inx2 < nameNodes.getLength(); inx2++) {
						Element nameScalar = (Element)nameNodes.item(inx2);						
						Element valueScalar = (Element)valueNodes.item(inx2);
						createNameValue(nameScalar, valueScalar);
					}
				}
			}
		}
		
		logger.debug(indent + "transform createNameValue :" + attributes.get("xpath"));
	}
	
	private void createNameValue(Element nameScalar, Element valueScalar){		
		String nameValue = nameScalar.getTextContent().trim();
		Attr nameDictRef = nameScalar.getAttributeNode("dictRef");
		if(nameDictRef.getValue().contains(":")){
			String prefix = nameDictRef.getValue().split(":")[0];
			nameScalar.setAttribute("dictRef",prefix + ":" + nameValue);
		}else{
			nameScalar.setAttribute("dictRef",nameValue);
		}
		
		String valueValue = valueScalar.getTextContent().trim();
		valueScalar.getParentNode().removeChild(valueScalar);		
		nameScalar.setTextContent(valueValue);
		
	}
	
	
}

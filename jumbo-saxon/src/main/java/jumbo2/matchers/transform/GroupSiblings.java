/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;

public class GroupSiblings extends TransformMatcher {

	public GroupSiblings(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		
		NodeList nodeList = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		Node parent = null;
		for(int inx = 0; inx < nodeList.getLength(); inx++){
			Node node = (Node)nodeList.item(inx);
			Node parent0 = node.getParentNode();
			if (parent0 == null) {
				throw new RuntimeException("Siblings must have parents");
			}
			if (parent == null) {
				parent = parent0;
			} else if (!parent.equals(parent0)) {
				throw new RuntimeException("Can only group siblings (i.e. with same parent");
			}

		}
		logger.debug(indent + "transform groupSiblings :" + attributes.get("xpath"));
		
		if (parent != null && nodeList.getLength() > 1) {
			int start = indexOf(parent, nodeList.item(0));
			int end = indexOf(parent, nodeList.item(nodeList.getLength()-1));
			List<Node> newNodeList = new ArrayList<Node>();
			for (int i = 0; i < parent.getChildNodes().getLength(); i++) {
				newNodeList.add(parent.getChildNodes().item(i));
			}
			int currentNode = 0;
			Element groupParent = null;
			for (int i = start; i < end; i++) {
				Node child = newNodeList.get(i);
				if (child instanceof Element) {
					if (child.equals(nodeList.item(currentNode))) {
						currentNode++;
						if (currentNode >= nodeList.getLength()) {
							break;
						}
						groupParent = (Element)child;
					} else {
						child.getParentNode().removeChild(child);
						groupParent.appendChild(child);
					}
				}
			}
			
		}
	}

	private int indexOf(Node parent, Node child){
		int position = -1;
		for(int inx = 0; inx < parent.getChildNodes().getLength(); inx++){
			if(parent.getChildNodes().item(inx).equals(child))
				position = inx;
		}	
		return position;
	}

}

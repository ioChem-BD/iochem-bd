/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.MutableDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateDate extends TransformMatcher {
	
	private static final String ALPHA_ZONE_PATTERN = "EEE MMM  d HH:mm:ss ZZZ yyyy";
    private static final DateTimeFormatter ALPHA_ZONE_FORMATTER = DateTimeFormat.forPattern(ALPHA_ZONE_PATTERN);
    
	private static final String ALPHA_PATTERN1 = "EEE MMM  d HH:mm:ss yyyy";
	private static final String ALPHA_PATTERN2 = "EEE MMM dd HH:mm:ss yyyy";
    private static final DateTimeFormatter ALPHA_FORMATTER1 = DateTimeFormat.forPattern(ALPHA_PATTERN1);
    private static final DateTimeFormatter ALPHA_FORMATTER2 = DateTimeFormat.forPattern(ALPHA_PATTERN2);
    
	
    private static final String DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZZ";
    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormat.forPattern(DATETIME_PATTERN);
	
	private static final String ZULU_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z";
	private static final DateTimeFormatter ZULU_FORMATTER = DateTimeFormat.forPattern(ZULU_DATETIME_PATTERN);
	
	
	public CreateDate(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		String format = attributes.get("format");
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);			
			String val = node.getTextContent();
			try {
				Object dateTimeDuration = null;
				if (format.equals("DHMS")) {
					dateTimeDuration = createDMHS(val);
				} else if (format != null) {
					dateTimeDuration = parseDate(val, format);
				} else {
					dateTimeDuration = parseDate(val);
				}										
				// create new scalar and replace
				Node date = createDate(parentNode.getOwnerDocument(), dateTimeDuration, node);				
				node.getParentNode().replaceChild(date, node);
			} catch (Exception e) {
				logger.warn(indent + "Cannot parse/set date/duration: "+val+ " (format='"+format+"'); "+e);
			}		
		}
		logger.debug(indent + "transform X :" + attributes.get("xpath"));
	}

	private Duration createDMHS(String val) {
		Pattern ELAPSED = Pattern.compile("\\s*(\\d+)\\s*days\\s*(\\d+)\\s*hours\\s*(\\d+)\\s*minutes\\s*([\\d\\.]+)\\s*seconds\\.?\\s*");
		Matcher matcher = ELAPSED.matcher(val);
		Duration duration = null;
		if (matcher.matches()) {
			double secs = new Integer(matcher.group(1))*86400 +
			    new Integer(matcher.group(2))*3600 +
			    new Integer(matcher.group(3))*60 +
			    new Double(matcher.group(4));
			long millis = (long)(1000*secs);
			duration = new Duration(millis);
		}
		return duration;
	}
	
	private DateTime parseDate(String value, String format){
		DateTimeParser customParser = DateTimeFormat.forPattern(format).getParser();
		DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(customParser).toFormatter();		
		DateTime date = formatter.parseDateTime(value);
		return date; 
	}
	
	private DateTime parseDate(String s){
	 if (s.endsWith("Z")) {
            MutableDateTime dateTime = ZULU_FORMATTER.parseMutableDateTime(s);
            dateTime.setZone(DateTimeZone.UTC);
            return dateTime.toDateTime();
        } else if (Character.isLetter(s.charAt(0))){
        	DateTime dateTime = parseQuietly(ALPHA_FORMATTER2, s);
            if (dateTime == null) {
                dateTime = parseQuietly(ALPHA_FORMATTER1, s);
            }
            return dateTime;
        } else {
            return DATETIME_FORMATTER.parseDateTime(s);
        }		
	}
	
    public static DateTime parseQuietly(DateTimeFormatter formatter, String s) {
    	DateTime dateTime = null;
    	if (formatter != null) {
    		try {
    			dateTime = formatter.parseDateTime(s);
    		} catch (Exception e) {    		
    		}
    	}
    	return dateTime;
	}

    public Node createDate(Document document, Object dateTime, Node originalNode){
    	Element date = document.createElementNS(Constants.cmlNamespace, "scalar");
    	if(((Element)originalNode).hasAttribute("dictRef"))
    		date.setAttribute("dictRef", ((Element)originalNode).getAttribute("dictRef")); 			
		if(dateTime instanceof DateTime){		    
			date.setTextContent(((DateTime)dateTime).toLocalDateTime().toString());
			date.setAttribute("dataType", "xsd:date");
		}else if(dateTime instanceof Duration){
			date.setTextContent(((Duration)dateTime).toString());
			date.setAttribute("dataType", "xsd:string");			
		}
		return date;    	
    }
	
}

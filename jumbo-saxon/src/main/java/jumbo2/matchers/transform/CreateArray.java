/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateArray extends TransformMatcher {


	public CreateArray(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		XPathExpression expr2 = xpath.compile(attributes.get("from"));
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);
			NodeList scalarNodes = (NodeList)expr2.evaluate(node, XPathConstants.NODESET);  
			if (scalarNodes.getLength() > 0) {
				Element scalar = (Element)scalarNodes.item(0);
				String dataTypex = attributes.containsKey("dataType") ? attributes.get("dataType") : scalar.getAttribute("dataType");
				String dictRefx = attributes.containsKey("dictRef") ? attributes.get("dictRef") : scalar.getAttribute("dictRef");
				String[] values = null;
				if (scalarNodes.getLength() == 1) {
					String value = scalarNodes.item(0).getTextContent();	// splits scalar 
					if (value != null && value.trim().length() > 0){
						String splitterx = attributes.containsKey("splitter") ? attributes.get("splitter"): Constants.WHITESPACES_REGEX;
						values = value.split(splitterx);	
					}
				} else if (scalarNodes.getLength() > 0) {					
					values = new String[scalarNodes.getLength()];				// collects all values into scalars
					for (int j = 0; j < scalarNodes.getLength(); j++) 
						values[j] = scalarNodes.item(j).getTextContent();
				}
				if (values != null && values.length > 0){
					String delimiter = attributes.get("delimiter") == null ? " " : attributes.get("delimiter");
					Node array = createArray(parentNode, dataTypex, dictRefx, values, delimiter);
					Node parent = scalar.getParentNode();
					parent.replaceChild(array, scalar);
					for (int j = 1; j < scalarNodes.getLength(); j++){
						parent = scalarNodes.item(j).getParentNode(); 
						parent.removeChild(scalarNodes.item(j));
					}
				}				
			}
		}
		logger.debug(indent + "transform createArray :" + attributes.get("xpath"));
	}
	
	private Node createArray(Node parentNode, String dataTypex, String dictRefx, String[] values, String delimiter){
		StringBuilder sb = new StringBuilder();
		Element array = (Element)parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "array");
		if(!dataTypex.equals("")) 
			array.setAttribute("dataType", dataTypex);
		if(!dictRefx.equals(""))
			array.setAttribute("dictRef", dictRefx);
		if(!delimiter.equals(" "))
			array.setAttribute("delimiter", delimiter);
		array.setAttribute("size", String.valueOf(values.length));
		for(String value : values)
			sb.append(value + delimiter);
		if(sb.toString().endsWith(delimiter))
			array.setTextContent(sb.toString().substring(0,sb.toString().length()-delimiter.length()));
		else
			array.setTextContent(sb.toString().trim());
		return array;
	}
}

/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.converters.PlainTextToXmlConverter;
import jumbo2.matchers.TransformMatcher;

public class AddChild extends TransformMatcher {

	public AddChild(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Element node = (Element)result.item(inx);
			Element childNode = createElement(node.getOwnerDocument(), attributes.get("elementName"));			
			if(attributes.containsKey("id"))
				childNode.setAttribute("id", attributes.get("id"));
			if(attributes.containsKey("dictRef"))
				childNode.setAttribute("dictRef", attributes.get("dictRef"));
			if(attributes.containsKey("value"))
				childNode.setTextContent(getValue(node, attributes.get("value")));
			if(attributes.containsKey("position")){
				Integer position = attributes.containsKey("position")? Integer.parseInt(attributes.get("position")) : null;
				if(position == null || position > node.getChildNodes().getLength() -1)
					node.appendChild(childNode);
				else
					node.insertBefore(childNode, node.getChildNodes().item(position));
			}else
				node.appendChild(childNode);
		}
		logger.debug(indent + "transform addChild : " + attributes.get("xpath"));
	}
	
}

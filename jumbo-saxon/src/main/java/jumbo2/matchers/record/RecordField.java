/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RecordField {

	Logger logger = LogManager.getLogger(RecordField.class.getName());	
	public static enum DataType  {BOOLEAN, STRING, WORD, INTEGER, FLOAT, SCIENTIFIC};
	private static HashMap<DataType, String>regexForDataType;
	private static Pattern p = null;
	private static final int MAX_ENDRANGE = 50;
	
	private int startRange = -1;
	private int endRange = -1;
	private DataType dataType = null;
	private int totalSpace = 0;
	private int integerPositions = 0;
	private int decimalPositions = 0;
	private String namespace = "";
	private String fieldName = "";	
	private int number = -1;
	private ArrayList<String> namedGroups;
	
	static {
		p = Pattern.compile(RecordRegexProcessor.FIELD_REGEX);
		regexForDataType = new HashMap<DataType, String>();
		regexForDataType.put(DataType.BOOLEAN, "\\\\s*(True|False|TRUE|FALSE|true|false|0|1|T|F|YES|NO|Yes|No|yes|no)\\\\s*");
		regexForDataType.put(DataType.STRING, "\\.*?");			//Not greedy capturing template
		regexForDataType.put(DataType.WORD, "\\\\s*\\\\S+\\\\s*");				
		regexForDataType.put(DataType.INTEGER, "\\\\s*[+-]?[0-9]+\\\\s*");
		regexForDataType.put(DataType.FLOAT, "\\\\s*[+-]?([0-9]+(\\\\.[0-9]*)?|\\\\.[0-9]+)\\\\s*");  //Optional sign, integer and fraction part. 
		regexForDataType.put(DataType.SCIENTIFIC, "\\\\s*[+-]?([0-9]+(\\\\.[0-9]*)?|\\\\.[0-9]+)([eEdD][-+]?[0-9]+)?\\\\s*"); //Optional sign, integer , fraction and exponent.
	}
	
	public RecordField(int number ,String line) throws Exception{
		this.number = number;
		namedGroups = new ArrayList<String>();
		Matcher groups = p.matcher(line);
		if(groups.find()){
			if(groups.group(2) != null && !groups.group(2).equals(""))
				startRange = Integer.parseInt(groups.group(2));
			if(groups.group(4) != null && !groups.group(4).equals("")){ 				
				endRange = Integer.parseInt(groups.group(4));
				if(endRange > MAX_ENDRANGE)
					throw new Exception("Record exceeded max endRange value, line :" + line);
			}
			if(groups.group(7) != null && !groups.group(7).equals(""))
				totalSpace = Integer.parseInt(groups.group(7));
			if(groups.group(8) != null && !groups.group(8).equals("")){
				decimalPositions = Integer.parseInt(groups.group(8));
				integerPositions = totalSpace - decimalPositions - 1; 
			}
			if(groups.group(10) != null && !groups.group(10).equals(""))
				namespace = groups.group(10);
			if(groups.group(11) != null && !groups.group(11).equals(""))				
				fieldName = groups.group(11);		
			
			String dataTypeStr = groups.group(5);
			if(dataTypeStr.equals("B"))
				this.dataType = DataType.BOOLEAN;
			else if(dataTypeStr.equals("A"))
				this.dataType = DataType.WORD;
			else if(dataTypeStr.equals("I"))
				this.dataType = DataType.INTEGER;
			else if(dataTypeStr.equals("F"))
				this.dataType = DataType.FLOAT;
			else if(dataTypeStr.equals("E"))
				this.dataType = DataType.SCIENTIFIC;
			else if(dataTypeStr.equals("X"))
				this.dataType = DataType.STRING;			
		}else{
			throw new Exception("Invalid record definition found while parsing line:" + line);
		}		
	}
	
	public DataType getDataType(){
		return dataType;
	}
	
	public String getNamespace(){
		return namespace;
	}
	
	public String getFieldName(){
		return fieldName;
	}
	
	public String getQName(){
		if(namespace.equals("") && fieldName.equals(""))
			return null;
		return namespace + ":" + fieldName;
	}
	
	public ArrayList<String> getNamedGroups(){
		return namedGroups;
	}
	
	public String toRegexExpression(String indentation) throws Exception{
		StringBuilder sb = new StringBuilder();
		int groupCounter = 1;
		if(startRange != -1){					//We must build as many different named templates as user defined range, this is done because multiple group capture only returns last match, refer to http://stackoverflow.com/questions/5018487/regular-expression-with-variable-number-of-groups
			if(endRange == -1){					//Undefined endRange is equals as : startRange = 1 , endRange = starRange 
				endRange = startRange;
				startRange = 1;
				for(int inx = 1; inx <= endRange; inx++){
					namedGroups.add("f" + number + "s" + groupCounter);
					sb.append("(?<f" + number + "s" + groupCounter++ + "" + ">" + buildFieldRegex(dataType,totalSpace, integerPositions, decimalPositions) + ")" );
				}
			}else{
				for(int inx = 1; inx <= endRange; inx++){
					namedGroups.add("f" + number + "s" + groupCounter);
					sb.append("(?<f" + number + "s" + groupCounter++ + "" + ">" +buildFieldRegex(dataType,totalSpace, integerPositions, decimalPositions) + ")" +(inx<= startRange?"":"?"));   // {1_3F} Defines one fixed group () and two optionals ()?   {3_7F} Defines three fixed groups () and four optionals ()?
				}
			}						
		}else{
			namedGroups.add("f" + number + "s" + groupCounter);
			sb.append("(?<f" + number + "s" + groupCounter++ + "" + ">" + buildFieldRegex(dataType,totalSpace, integerPositions, decimalPositions) + ")");
		}				
		return sb.toString();
	}
	
	
	private String buildFieldRegex(DataType dataType, int totalSpace, int integerPositions, int decimalPositions) throws Exception{
		if(totalSpace == 0)
			return regexForDataType.get(dataType);
		else{
			return buildDelimitedPositionRegex(dataType, totalSpace, integerPositions, decimalPositions);
		}	
	}
	
	
	
	private String buildDelimitedPositionRegex(DataType datatype, int totalSpace, int integerPositions, int decimalPositions) throws Exception{
		if(datatype == DataType.INTEGER)
			return "[\\\\+\\\\-0-9\\\\s]{" + totalSpace + "}";
		else if(datatype == DataType.FLOAT)
			return "[\\\\+\\\\-0-9\\\\s]{" + integerPositions + "}\\\\.[0-9\\\\s]{" + decimalPositions + "}";
		else if(datatype == DataType.WORD)		    
		    return "[\\\\s*\\\\S+\\\\s*]{" + totalSpace + "}";		    
		else 
			throw new Exception("Unsuported record field expression. It's defined with boundaries for INTEGER, FLOAT or WORD type.");
	}
	
	
	public int numberOfGroups(){
		if(startRange != -1)					
			if(endRange != -1)			
				return endRange - startRange + 1;
			else
				return startRange;
		else
			return 1;				
	}
	
	public boolean isArray(){
		return (startRange > 1 || endRange > 1);
	}
	
}

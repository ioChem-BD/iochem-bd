/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import jumbo2.matchers.template.TemplatePattern;
import jumbo2.navigator.text.filecontentmanager.FileContentManager;
import jumbo2.navigator.text.filecontentmanager.helper.LineRange;
import jumbo2.navigator.text.filecontentmanager.helper.Section;
import jumbo2.util.Constants;
import jumbo2.util.Utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TemplateMatcher {
	Logger logger = LogManager.getLogger(TemplateMatcher.class.getName());	
	
	private String id = null;
	private String name = null;
	private ArrayList<TemplatePattern> startPatterns = null;	
	private ArrayList<TemplatePattern> endPatterns = null;
	private boolean hasEofEndPattern = false;
	private int offset = 0;
	private int endOffset = -1;
	private int repeat = 1;
    private int repeatCounter = 1;
    private String keep = "all";

	private boolean containsMultilinePatterns = false;
	
	public TemplateMatcher(){
		startPatterns = new ArrayList<TemplatePattern>();
		endPatterns = new ArrayList<TemplatePattern>();		
	}
	
	public void processAttribute(Node attribute, String indentation){
		if(attribute.getNodeName().equals("id"))			
			id = attribute.getNodeValue();		
		else if(attribute.getNodeName().equals("name"))
			name = attribute.getNodeValue();					
		else if(attribute.getNodeName().startsWith("pattern"))
			addStartPattern(attribute.getNodeValue(), indentation);
		else if(attribute.getNodeName().startsWith("endPattern"))
			addEndPattern(attribute.getNodeValue(), indentation);
		else if(attribute.getNodeName().equals("offset"))
			setOffset(Integer.parseInt(attribute.getNodeValue()));
		else if(attribute.getNodeName().equals("endOffset"))
			setEndOffset(Integer.parseInt(attribute.getNodeValue()));	
		else if(attribute.getNodeName().equals("repeat"))
			setRepeat(attribute.getNodeValue());
		else if(attribute.getNodeName().equals("keep"))
			setKeep(attribute.getNodeValue());
		else
			if(!attribute.getNodeName().equals("xml:base") && !attribute.getNodeName().equals("xmlns:xi"))	//Avoid warn on template references
				logger.debug(indentation + "  " + "Not coded attribute action : " + attribute.getNodeName() + " = " + attribute.getNodeValue() + "; ");		
		logger.debug(indentation + "  " + attribute.getNodeName() + " : " + attribute.getNodeValue());
	}
	
	private void addStartPattern(String pattern, String indent){
		startPatterns.add(new TemplatePattern(pattern));		
		if(pattern.contains("$"))
			containsMultilinePatterns = true;
	}
	
	private void addEndPattern(String pattern, String indent){
		if(pattern.equals("~")){
			hasEofEndPattern = true;
			return;
		}
		endPatterns.add(new TemplatePattern(pattern));
		if(pattern.contains("$"))
			containsMultilinePatterns = true;
	}
	
	private void setOffset(int offset){		
		this.offset += offset;
	}
	
	private void setEndOffset(int endOffset){		
		this.endOffset += endOffset;
	}
	
	private void setRepeat(String repeat){
		this.repeat = repeat.equals("*")?Integer.MAX_VALUE:Integer.parseInt(repeat);
	}
	
	private void setKeep(String keep){		
		this.keep = keep.toLowerCase();
	}
	
	public String getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean containsMultiplePatterns(){
		return containsMultilinePatterns;
	}
	
	public ArrayList<LineRange> getMatches(FileContentManager calculationLines, String parentUUID, String indent) throws IOException{
		if(startPatterns.size() == 0){
			logger.warn("Pattern " + this.getId() + " does not have start pattern defined, assigning .* by default");
			addStartPattern(".*", indent);
		}
		if(endPatterns.size() == 0 && !hasEofEndPattern){
			logger.warn("Pattern " + this.getId() + " does not have end pattern defined, assigning ~ by default");
			addEndPattern("~", indent);
		}
		
		ArrayList<LineRange> result = new ArrayList<LineRange>(); //Matching results
		logger.debug(indent + "  " + "-==Matches found==-");
		for(Section section: calculationLines.getUnProcessedSections(parentUUID)){
			ArrayList<String> textLines = section.getLines(); 
			String line;
			ListIterator<String> iter = textLines.listIterator();
		    int lineCounter = 0;
		    int startLineCounter = -1;
		    while(iter.hasNext()){
		    	line = iter.next();
		    	lineCounter++;
		    	for(TemplatePattern startPattern : startPatterns){
		    		if(repeatCounter > repeat)
						break;	   
		    		if(startPattern.matches(line,iter)){	    			
		    			startLineCounter = lineCounter;
		    			logger.info(indent + "  " + "TemplatePattern \""+ id  +"\" start match : " + line);    
		    			boolean foundEndMatch = false;
		    			while(iter.hasNext()){
		    				if(foundEndMatch == true) 
		    					break;	    			
		    			    line = iter.next();
		    			    lineCounter++;	    			    
		    			    for(TemplatePattern endPattern : endPatterns)	    			    	
 		    			    	if(endPattern.matches(line, iter)){
		    			    		logger.info(indent + "  " + "TemplatePattern " + id + " end match : " + line);
		    			    		result.add(new LineRange(section.getOffset() + startLineCounter + offset, section.getOffset() + lineCounter + endOffset));
		    			    		foundEndMatch = true;
		    			    		lineCounter += endOffset;
		    			    		adjustLineToEndOffset(iter, endOffset);		    			    		
		    			    		repeatCounter++;	    			    	
		    						break;	    	
		    			    	}	    			  
		    			}	    		
		    			if(!foundEndMatch && hasEofEndPattern){
		    				logger.info(indent + "  " + "TemplatePattern eof end match : " + line);		    						    			
		    				if(startLineCounter == lineCounter)
		    					result.add(new LineRange(section.getOffset() + startLineCounter + offset, section.getOffset() + lineCounter + 1 + endOffset)); //In the case there's a eof match of a single line section
		    				else{
		    					//if(hasEofEndPattern && endPatterns.size() == 0 && !hasEndOffset) 		//If endPattern="~" and not defined endOffset, we'll capture all lines and final one too		    				 							    			
		    					result.add(new LineRange(section.getOffset() + startLineCounter + offset, section.getOffset() + lineCounter));
		    				}
		    			}
		    		}		    			    	
		    	}
		    }		
		}		
		logger.debug(indent + "  " + "===================");		
	    return filterByKeepField(result);
	}
	
	public ArrayList<LineRange> filterByKeepField(ArrayList<LineRange> lineEnds){		
		if(keep.equals("first") && lineEnds.size() >= 1){
			ArrayList<LineRange> result = new ArrayList<LineRange>();
			result.add(lineEnds.get(0));
			return result;
		}else if(keep.equals("last") && lineEnds.size() >=1){
			ArrayList<LineRange> result = new ArrayList<LineRange>();
			result.add(lineEnds.get(lineEnds.size()-1));
			return result;
		}else if(keep.equals("firstandlast") && lineEnds.size() >=1){
			ArrayList<LineRange> result = new ArrayList<LineRange>();
			result.add(lineEnds.get(0));
			if(lineEnds.size() > 1)
				result.add(lineEnds.get(lineEnds.size()-1));
			return result;
		}else
			return lineEnds;	//return all
	}
	
	public Node buildNode(Document resultDocument, LineRange lineEnds){
		String nodeUUID = Utils.buildRandomUUID();
		Element element = resultDocument.createElementNS(Constants.cmlNamespace, "module");
		element.setUserData("UUID", nodeUUID, null);
		if(id != null)
			element.setAttributeNS(Constants.cmlxPrefixNamespace[1], "cmlx:templateRef", id);
		element.setAttributeNS(Constants.cmlxPrefixNamespace[1], "cmlx:lineCount", String.valueOf(lineEnds.size()));
		element.setAttribute("startLine", String.valueOf(lineEnds.getStart()));
		element.setAttribute("endLine", String.valueOf(lineEnds.getEnd()));
		return element;
	}
	
	/**
	 * This function is used to move iterator to the point where endOffset value set 
	 * @param iter
	 * @param previousCounter
	 */
	private void adjustLineToEndOffset(ListIterator<String> iter, int endOffset){
		if(endOffset < 0)			
			for(int inx = 0; inx > endOffset; inx--)
				iter.previous();		
		else
			for(int inx = 0; inx < endOffset; inx++)
				iter.next();		
	}
	
}

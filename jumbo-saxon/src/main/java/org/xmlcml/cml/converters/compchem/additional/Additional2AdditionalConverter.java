/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.additional;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xmlcml.cml.converters.compchem.CompChemConverter;

public class Additional2AdditionalConverter extends CompChemConverter {
	
	public Logger logger = LogManager.getLogger(Additional2AdditionalConverter.class.getName());
	
	@Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.TEXT;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws IOException {
		Path sourcePath = FileSystems.getDefault().getPath(inputFilePath);
		Path destPath = FileSystems.getDefault().getPath(outputFilePath);		
		Files.copy(sourcePath, destPath);		
	}

	@Override
	public String getRegistryInputType() {
		return "noformat_additional";
	}

	@Override
	public String getRegistryOutputType() {
		return "noformat_additional";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert Additional file to same file";
	}

}

package org.xmlcml.cml.converters.compchem.qespresso.phonon;

import org.w3c.dom.Document;
import org.xmlcml.cml.converters.compchem.CompChemConverter;
import org.xmlcml.cml.converters.compchem.CompChemConverter.Type;

import jumbo2.converters.PlainTextToXmlConverter;
import jumbo2.util.Utils;


public class QEspressoPhononInput2XMLConverter extends CompChemConverter {
	
	private static final String SOURCETOXML_TEMPLATE_RESOURCE 	= "org/xmlcml/cml/converters/compchem/qespresso/phonon/input/templates/topTemplate.xml";

	@Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.XML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
		PlainTextToXmlConverter txtToCompChem = new PlainTextToXmlConverter(inputFilePath, SOURCETOXML_TEMPLATE_RESOURCE, appendedXML);
		Document result = txtToCompChem.call();
		Utils.outputResultDocument(result, outputFilePath, false);
	}

	@Override
	public String getRegistryInputType() {
		return "qespresso_phonon_input";
	}

	@Override
	public String getRegistryOutputType() {
		return "qespresso_phonon_input_xml";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert QuantumEspresso phonon input file to XML";
	}

}

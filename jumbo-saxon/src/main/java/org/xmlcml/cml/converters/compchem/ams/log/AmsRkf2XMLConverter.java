/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.ams.log;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.xmlcml.cml.converters.compchem.CompChemConverter;

import jumbo2.util.XMLFileManager;

public class AmsRkf2XMLConverter extends CompChemConverter {

    @Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.XML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
	    String rkfReaderPath = System.getProperty("RKF2CML_FILE_PATH");
	    if(rkfReaderPath == null || !new File(rkfReaderPath).exists())
	        throw new Exception("Could not locate rkf2cml tool.");
	   
	    XMLFileManager xml = new XMLFileManager("http://www.xml-cml.org/schema", null, appendedXML);
	    String amsRkfPath = xml.getStringFromQuery("//module/text()");	    
	    if(amsRkfPath == null || !new File(amsRkfPath).exists())
	        throw new Exception("Could not locate ams.rkf file on provided path.");     
	   
        ArrayList<String> commandList = new ArrayList<String>();
        String[] commands;                     
        commandList.add(rkfReaderPath);
        commandList.add(amsRkfPath);
        commandList.add(inputFilePath);
        commandList.add(outputFilePath);        
        commands = new String[commandList.size()];
        commandList.toArray(commands);
        try{
            Process child = Runtime.getRuntime().exec(commands);
            InputStream is = child.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String aux = br.readLine();                         
            while (aux != null)
                aux = br.readLine();
        }catch (Exception e){
            throw new Exception("Error converting rkf files to CML." + e.getMessage());
        }        
        if(!new File(outputFilePath).exists())
            throw new Exception("Error converting rkf files to CML, conversion file not generated.");
	}

	@Override
	public String getRegistryInputType() {
		return "adf_rkf";
	}

	@Override
	public String getRegistryOutputType() {
		return "adf_rkf_compchem";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert both ams.rkf and adf.rkf files to CML, using rkf2cml tool";
	}

}

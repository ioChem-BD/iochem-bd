/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.qespresso.pdos;

import jumbo2.converters.PlainTextToXmlConverter;
import jumbo2.util.Utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;
import org.xmlcml.cml.converters.compchem.CompChemConverter;

public class QEspressoPDOS2XMLConverter extends CompChemConverter {
	
	private static final Pattern PDOSFILENAME_PATTERN = Pattern.compile(".*#(\\d+)\\((\\w+)\\).*#(\\d+)\\((\\w+)\\)"); 
	private static final String SOURCETOXML_TEMPLATE_RESOURCE 	= "org/xmlcml/cml/converters/compchem/qespresso/pdos/templates/topTemplate.xml";
	
	private static final String NAME_TEMPLATE =    "<list id='orbital'>" 
											    + "   <atom elementType='%s' id='a%d' />"
												+ "   <scalar dataType='xsd:string' dictRef='qex:orbitalname'>%s</scalar>"
												+ "   <scalar dataType='xsd:string' dictRef='qex:orbitalid'>%d</scalar>"
												+ "</list>";
														
			

	@Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.XML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
		appendedXML = getNameInformation(inputFilePath);
		PlainTextToXmlConverter txtToCompChem = new PlainTextToXmlConverter(inputFilePath, SOURCETOXML_TEMPLATE_RESOURCE, appendedXML);
		Document result = txtToCompChem.call();
		
		
		Utils.outputResultDocument(result, outputFilePath, false);
	}

	private String getNameInformation(String inputFilePath) {				
		Matcher matcher = PDOSFILENAME_PATTERN.matcher(new File(inputFilePath).getName());
		if(matcher.matches()) {
			return String.format(NAME_TEMPLATE, matcher.group(2), Integer.parseInt(matcher.group(1)), matcher.group(4), Integer.parseInt(matcher.group(3)));
		}
		return "";
	}

	@Override
	public String getRegistryInputType() {
		return "qespresso_pdos";
	}

	@Override
	public String getRegistryOutputType() {
		return "qespresso_pdos_xml";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert QuantumEspresso PDOS file to XML";
	}

}

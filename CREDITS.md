# Credits

Copyright 2011-2023.

Institutions involved in ioChem-BD development:
 * [Institute of Chemical Research of Catalonia](https://www.iciq.es/) (ICIQ)
 * [Universitat Rovira i Virgili](https://www.urv.cat/) (URV) 
 * [Institució Catalana de Recerca i Estudis Avançats](https://www.icrea.cat/) (ICREA)
 * [Universitat Autònoma de Barcelona](https://www.uab.cat/) (UAB)

### Scientific committee:
  * Carles Bo (ICIQ - URV)
  * Josep Maria Poblet (URV)
  * Núria Lopez (ICIQ)
  * Coen de Graaf (URV - ICREA)
  * Feliu Maseras (URV - UAB)

### Scientific contributors:
  * Max Garcia
  * Neyvis Nalmora
  * Maria Besora
  * Joan Gonzalez
  * Ana Mateo
  * Enric Petrus
  * Diego Garay
  * Jordi Buils

### Development:
  * Main developer: Moisés Álvarez (ICIQ-URV) (2011 - now)
  * Developer: Marc Gruber (ICIQ) (2023 -now)
  * SCIPIO (later Create module) developer: Joan Iglesias (ICIQ) (2011-2013)   
 
### System administrators:
  * Martin Gumbau (ICIQ)
  * Jose Carlos Ortiz (URV)

### Contributors
  * Bro-Jørgensen, William (University of Copenhagen)

# ioChem-BD third party license summary

The following is a list of all Java third-party dependencies used inside the ioChem-BD project, grouped by their license type. You can review each one on [SPDX webpage](https://spdx.org/licenses/).

The full text for each listed license can be found in *./third-party* folder.

## License list


###  3-Clause BSD License, BSD-3-Clause:

Name | id | version | url
---- | --- | --------- | ----
Protocol Buffers [Core] | com.google.protobuf:protobuf-java | 3.12.4 | https://developers.google.com/protocol-buffers/protobuf-java/

###  AGPL-3.0-or-later:

Name | id | version | url
---- | --- | --------- | ----
ioChem-BD Create shell project | cat.iciq.iochembd.create:shell | 3.1.1 | https://gitlab.com/ioChem-BD/iochem-bd/-/tree/master/createshell
jumbo-saxon | jumbo:jumbo-saxon | 3.1.1 | https://gitlab.com/ioChem-BD/iochem-bd/-/tree/master/jumbo-saxon

###  Apache-2.0:

Name | id | version | url
---- | --- | --------- | ----
Abdera Client | org.apache.abdera:abdera-client | 1.1.1 | http://abdera.apache.org/abdera-client
Abdera Core | org.apache.abdera:abdera-core | 1.1.1 | http://abdera.apache.org/abdera-core
Abdera Parser | org.apache.abdera:abdera-parser | 1.1.1 | http://abdera.apache.org/abdera-parser
Activation | org.apache.geronimo.specs:geronimo-activation_1.0.2_spec | 1.1 | http://geronimo.apache.org/geronimo-activation_1.0.2_spec
Activation 1.1 | org.apache.geronimo.specs:geronimo-activation_1.1_spec | 1.0.2 | http://geronimo.apache.org/specs/geronimo-activation_1.1_spec
aljava | xyz.avarel:aljava | 0.0.3 | no url defined
Ant-Contrib Tasks | ant-contrib:ant-contrib | 1.0b3 | http://ant-contrib.sourceforge.net
ant-launcher | org.apache.ant:ant-launcher | 1.7.0 | http://ant.apache.org/ant-launcher/
Apache Commons CLI | commons-cli:commons-cli | 1.5.0 | https://commons.apache.org/proper/commons-cli/
Apache Commons Codec | commons-codec:commons-codec | 1.11 | http://commons.apache.org/proper/commons-codec/
Apache Commons Codec | commons-codec:commons-codec | 1.16.1 | https://commons.apache.org/proper/commons-codec/
Apache Commons Codec | commons-codec:commons-codec | 1.9 | http://commons.apache.org/proper/commons-codec/
Apache Commons Collections | commons-collections:commons-collections | 3.2.2 | http://commons.apache.org/collections/
Apache Commons Compress | org.apache.commons:commons-compress | 1.26.1 | https://commons.apache.org/proper/commons-compress/
Apache Commons Compress | org.apache.commons:commons-compress | 1.7 | http://commons.apache.org/proper/commons-compress/
Apache Commons CSV | org.apache.commons:commons-csv | 1.9.0 | https://commons.apache.org/proper/commons-csv/
Apache Commons FileUpload | commons-fileupload:commons-fileupload | 1.5 | https://commons.apache.org/proper/commons-fileupload/
Apache Commons IO | commons-io:commons-io | 2.11.0 | https://commons.apache.org/proper/commons-io/
Apache Commons IO | commons-io:commons-io | 2.15.1 | https://commons.apache.org/proper/commons-io/
Apache Commons Lang | org.apache.commons:commons-lang3 | 3.14.0 | https://commons.apache.org/proper/commons-lang/
Apache Commons Logging | commons-logging:commons-logging | 1.2 | http://commons.apache.org/proper/commons-logging/
Apache Commons Logging | commons-logging:commons-logging | 1.3.0 | https://commons.apache.org/proper/commons-logging/
Apache Commons Text | org.apache.commons:commons-text | 1.10.0 | https://commons.apache.org/proper/commons-text
Apache FontBox | org.apache.pdfbox:fontbox | 1.8.7 | http://pdfbox.apache.org/
Apache FontBox | org.apache.pdfbox:fontbox | 2.0.27 | http://pdfbox.apache.org/
Apache FOP All-In-One | org.apache.xmlgraphics:fop | 2.9 | http://xmlgraphics.apache.org/fop/fop/
Apache FOP Core | org.apache.xmlgraphics:fop-core | 2.9 | http://xmlgraphics.apache.org/fop/fop-core/
Apache FOP Events | org.apache.xmlgraphics:fop-events | 2.9 | http://xmlgraphics.apache.org/fop/fop-events/
Apache FOP Utilities | org.apache.xmlgraphics:fop-util | 2.9 | http://xmlgraphics.apache.org/fop/fop-util/
Apache Hadoop Annotations | org.apache.hadoop:hadoop-annotations | 2.2.0 | no url defined
Apache Hadoop Auth | org.apache.hadoop:hadoop-auth | 2.2.0 | no url defined
Apache Hadoop Common | org.apache.hadoop:hadoop-common | 2.2.0 | no url defined
Apache Hadoop HDFS | org.apache.hadoop:hadoop-hdfs | 2.2.0 | no url defined
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.3.5 | http://hc.apache.org/httpcomponents-client
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.5.13 | http://hc.apache.org/httpcomponents-client
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.5.14 | http://hc.apache.org/httpcomponents-client-ga
Apache HttpClient Cache | org.apache.httpcomponents:httpclient-cache | 4.2.6 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Cache | org.apache.httpcomponents:httpclient-cache | 4.5.13 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Mime | org.apache.httpcomponents:httpmime | 4.3.1 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Mime | org.apache.httpcomponents:httpmime | 4.5.13 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Mime | org.apache.httpcomponents:httpmime | 4.5.14 | http://hc.apache.org/httpcomponents-client-ga
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.3.2 | http://hc.apache.org/httpcomponents-core-ga
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.4.13 | http://hc.apache.org/httpcomponents-core-ga
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.4.16 | http://hc.apache.org/httpcomponents-core-ga
Apache JAMES Mime4j (Core) | org.apache.james:apache-mime4j-core | 0.7.2 | http://james.apache.org/mime4j/apache-mime4j-core
Apache JAMES Mime4j (DOM) | org.apache.james:apache-mime4j-dom | 0.7.2 | http://james.apache.org/mime4j/apache-mime4j-dom
Apache JAMES Mime4j | org.apache.james:apache-mime4j | 0.6 | http://james.apache.org/mime4j
Apache JempBox | org.apache.pdfbox:jempbox | 1.8.7 | http://www.apache.org/pdfbox-parent/jempbox/
Apache Jena - ARQ (SPARQL 1.1 Query Engine) | org.apache.jena:jena-arq | 2.12.0 | http://jena.apache.org/jena-arq/
Apache Jena - ARQ (SPARQL 1.1 Query Engine) | org.apache.jena:jena-arq | 4.5.0 | https://jena.apache.org/jena-arq/
Apache Jena - Base Common Environment | org.apache.jena:jena-base | 4.5.0 | https://jena.apache.org/jena-base/
Apache Jena - Core | org.apache.jena:jena-core | 2.12.0 | http://jena.apache.org/jena-core/
Apache Jena - Core | org.apache.jena:jena-core | 4.5.0 | https://jena.apache.org/jena-core/
Apache Jena - IRI | org.apache.jena:jena-iri | 1.1.0 | http://jena.apache.org/jena-iri/
Apache Jena - IRI | org.apache.jena:jena-iri | 4.5.0 | https://jena.apache.org/jena-iri/
Apache Jena - Libraries POM | org.apache.jena:apache-jena-libs | 2.12.0 | http://jena.apache.org/apache-jena-libs/
Apache Jena - Shadowed external libraries | org.apache.jena:jena-shaded-guava | 4.5.0 | https://jena.apache.org/jena-shaded-guava/
Apache Jena - TDB (Native Triple Store) | org.apache.jena:jena-tdb | 1.1.0 | http://jena.apache.org/jena-tdb/
Apache Log4j 1.x Compatibility API | org.apache.logging.log4j:log4j-1.2-api | 2.17.1 | https://logging.apache.org/log4j/2.x/log4j-1.2-api/
Apache Log4j 1.x Compatibility API | org.apache.logging.log4j:log4j-1.2-api | 2.23.0 | https://logging.apache.org/log4j/2.x/log4j/log4j-1.2-api/
Apache Log4j API | org.apache.logging.log4j:log4j-api | 2.17.1 | https://logging.apache.org/log4j/2.x/log4j-api/
Apache Log4j API | org.apache.logging.log4j:log4j-api | 2.23.0 | https://logging.apache.org/log4j/2.x/log4j/log4j-api/
Apache Log4j Commons Logging Bridge | org.apache.logging.log4j:log4j-jcl | 2.23.0 | https://logging.apache.org/log4j/2.x/log4j/log4j-jcl/
Apache Log4j Core | org.apache.logging.log4j:log4j-core | 2.17.1 | https://logging.apache.org/log4j/2.x/log4j-core/
Apache Log4j Core | org.apache.logging.log4j:log4j-core | 2.23.0 | https://logging.apache.org/log4j/2.x/log4j/log4j-core/
Apache Log4j SLF4J Binding | org.apache.logging.log4j:log4j-slf4j-impl | 2.17.1 | https://logging.apache.org/log4j/2.x/log4j-slf4j-impl/
Apache Log4j SLF4J Binding | org.apache.logging.log4j:log4j-slf4j-impl | 2.23.0 | https://logging.apache.org/log4j/2.x/log4j/log4j-slf4j-impl/
Apache Log4j Web | org.apache.logging.log4j:log4j-web | 2.23.0 | https://logging.apache.org/log4j/2.x/log4j/log4j-web/
Apache PDFBox | org.apache.pdfbox:pdfbox | 1.8.7 | http://www.apache.org/pdfbox-parent/pdfbox/
Apache POI | org.apache.poi:poi | 3.6 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-ooxml | 3.6 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-ooxml-schemas | 3.10.1 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-ooxml-schemas | 3.6 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-scratchpad | 3.6 | http://poi.apache.org/
Apache Solr Analysis Extras | org.apache.solr:solr-analysis-extras | 4.10.2 | http://lucene.apache.org/solr-parent/solr-analysis-extras
Apache Solr Content Extraction Library | org.apache.solr:solr-cell | 4.10.2 | http://lucene.apache.org/solr-parent/solr-cell
Apache Solr Core | org.apache.solr:solr-core | 4.10.2 | http://lucene.apache.org/solr-parent/solr-core
Apache Solr Search Server | org.apache.solr:solr | 4.10.2 | http://lucene.apache.org/solr-parent/solr
Apache Solr Solrj | org.apache.solr:solr-solrj | 4.10.2 | http://lucene.apache.org/solr-parent/solr-solrj
Apache Thrift | org.apache.thrift:libthrift | 0.16.0 | http://thrift.apache.org
Apache Tika core | org.apache.tika:tika-core | 1.5 | http://tika.apache.org/
Apache Tika parsers | org.apache.tika:tika-parsers | 1.5 | http://tika.apache.org/
Apache Tika plugin for Ogg, Vorbis and FLAC | org.gagravarr:vorbis-java-tika | 0.1 | https://github.com/Gagravarr/VorbisJava
Apache Tika XMP | org.apache.tika:tika-xmp | 1.5 | http://tika.apache.org/
Apache XML Graphics Commons | org.apache.xmlgraphics:xmlgraphics-commons | 2.9 | http://xmlgraphics.apache.org/commons/
Apereo CAS Client for Java - Core | org.apereo.cas.client:cas-client-core | 4.0.4 | https://www.apereo.org/cas/cas-client-core
Apereo CAS Client for Java - SAML Protocol Support | org.apereo.cas.client:cas-client-support-saml | 4.0.4 | https://www.apereo.org/cas/cas-client-support-saml
attoparser | org.attoparser:attoparser | 2.0.7.RELEASE | https://www.attoparser.org
Avalon Framework API | org.apache.avalon.framework:avalon-framework-api | 4.3.1 | http://www.apache.org/excalibur/avalon-framework/avalon-framework-api/
Avalon Framework Implementation | org.apache.avalon.framework:avalon-framework-impl | 4.3.1 | http://www.apache.org/excalibur/avalon-framework/avalon-framework-impl/
Axiom API | org.apache.ws.commons.axiom:axiom-api | 1.2.10 | http://ws.apache.org/axiom/axiom-api/
Axiom Impl | org.apache.ws.commons.axiom:axiom-impl | 1.2.10 | http://ws.apache.org/axiom/axiom-impl/
BeanShell | org.apache-extras.beanshell:bsh | 2.0b6 | https://github.com/beanshell/beanshell/
Boilerpipe -- Boilerplate Removal and Fulltext Extraction from HTML pages | de.l3s.boilerpipe:boilerpipe | 1.1.0 | http://code.google.com/p/boilerpipe/
bucket4j-core | com.bucket4j:bucket4j-core | 8.10.1 | http://github.com/bucket4j/bucket4j/bucket4j-core
builder-commons | com.lyncode:builder-commons | 1.0.2 | http://nexus.sonatype.org/oss-repository-hosting.html/builder-commons
Byte Buddy (without dependencies) | net.bytebuddy:byte-buddy | 1.12.18 | https://bytebuddy.net/byte-buddy
ClassMate | com.fasterxml:classmate | 1.5.1 | https://github.com/FasterXML/java-classmate
Closure Compiler | com.google.javascript:closure-compiler | v20170626 | https://developers.google.com/closure/compiler/
Closure Compiler Externs | com.google.javascript:closure-compiler-externs | v20170626 | https://github.com/google/closure-compiler/closure-compiler-externs/
CML Euclid | org.blueobelisk:euclid | 2.5 | https://github.com/BlueObelisk/euclid
CMLXOM | org.blueobelisk:cmlxom | 4.5 | https://github.com/BlueObelisk/cmlxom
Code Generation Library | cglib:cglib | 3.1 | http://cglib.sourceforge.net/
Commons BeanUtils | commons-beanutils:commons-beanutils | 1.8.3 | http://commons.apache.org/beanutils/
Commons CLI | commons-cli:commons-cli | 1.2 | http://commons.apache.org/cli/
Commons Configuration | commons-configuration:commons-configuration | 1.6 | http://commons.apache.org/${pom.artifactId.substring(8)}/
Commons Configuration | commons-configuration:commons-configuration | 1.8 | http://commons.apache.org/configuration/
Commons DBCP | commons-dbcp:commons-dbcp | 1.4 | http://commons.apache.org/dbcp/
Commons Lang | commons-lang:commons-lang | 2.6 | http://commons.apache.org/lang/
Commons Lang | org.apache.commons:commons-lang3 | 3.1 | http://commons.apache.org/lang/
Commons Logging | commons-logging:commons-logging | 1.1.1 | http://commons.apache.org/logging
Commons Math | org.apache.commons:commons-math | 2.2 | http://commons.apache.org/math/
Commons Pool | commons-pool:commons-pool | 1.4 | http://commons.apache.org/pool/
Commons Validator | commons-validator:commons-validator | 1.4.0 | http://commons.apache.org/validator/
concurrent-jena | com.hp.hpl.jena:concurrent-jena | 1.3.2 | no url defined
ConcurrentLinkedHashMap | com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru | 1.2 | http://code.google.com/p/concurrentlinkedhashmap
Digester | commons-digester:commons-digester | 1.8 | http://jakarta.apache.org/commons/digester/
Ehcache | org.ehcache:ehcache | 3.9.0 | http://ehcache.org
Ehcache Core | net.sf.ehcache:ehcache-core | 1.7.2 | http://ehcache.sf.net/ehcache-core
elasticsearch | org.elasticsearch:elasticsearch | 1.6.1 | http://nexus.sonatype.org/oss-repository-hosting.html/elasticsearch
error-prone annotations | com.google.errorprone:error_prone_annotations | 2.0.18 | http://nexus.sonatype.org/oss-repository-hosting.html/error_prone_parent/error_prone_annotations
error-prone annotations | com.google.errorprone:error_prone_annotations | 2.3.4 | http://nexus.sonatype.org/oss-repository-hosting.html/error_prone_parent/error_prone_annotations
Evo Inflector | org.atteo:evo-inflector | 1.0.1 | http://atteo.org/static/evo-inflector
fastinfoset | com.sun.xml.fastinfoset:FastInfoset | 1.2.15 | http://fi.java.net
FindBugs-jsr305 | com.google.code.findbugs:jsr305 | 3.0.0 | http://findbugs.sourceforge.net/
FindBugs-jsr305 | com.google.code.findbugs:jsr305 | 3.0.2 | http://findbugs.sourceforge.net/
flyway-core | org.flywaydb:flyway-core | 3.0 | http://flywaydb.org/flyway-core
FORESITE :: Object Reuse and Exchange library | com.googlecode.foresite-toolkit:foresite | 0.9 | http://www.openarchives.org/ore
Google Analytics API v3-rev103-1.19.0 | com.google.apis:google-api-services-analytics | v3-rev103-1.19.0 | http://nexus.sonatype.org/oss-repository-hosting.html/google-api-services-analytics
Google APIs Client Library for Java | com.google.api-client:google-api-client | 1.19.0 | http://code.google.com/p/google-api-java-client/google-api-client/
Google HTTP Client Library for Java | com.google.http-client:google-http-client | 1.19.0 | http://code.google.com/p/google-http-java-client/google-http-client/
Google OAuth Client Library for Java | com.google.oauth-client:google-oauth-client | 1.19.0 | http://code.google.com/p/google-oauth-java-client/google-oauth-client/
Gson | com.google.code.gson:gson | 2.2.1 | http://code.google.com/p/google-gson/
Gson | com.google.code.gson:gson | 2.7 | https://github.com/google/gson/gson
Gson | com.google.code.gson:gson | 2.9.0 | https://github.com/google/gson/gson
Guava: Google Core Libraries for Java | com.google.guava:guava | 13.0 | http://code.google.com/p/guava-libraries/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 14.0.1 | http://code.google.com/p/guava-libraries/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 18.0 | http://code.google.com/p/guava-libraries/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 20.0 | https://github.com/google/guava/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 30.1-jre | https://github.com/google/guava/guava
Guava: Google Core Libraries for Java | com.google.guava:guava-jdk5 | 13.0 | http://code.google.com/p/guava-libraries/guava-jdk5
Guava InternalFutureFailureAccess and InternalFutures | com.google.guava:failureaccess | 1.0.1 | https://github.com/google/guava/failureaccess
Guava ListenableFuture only | com.google.guava:listenablefuture | 9999.0-empty-to-avoid-conflict-with-guava | https://github.com/google/guava/listenablefuture
HikariCP | com.zaxxer:HikariCP | 3.2.0 | https://github.com/brettwooldridge/HikariCP
HPPC Collections | com.carrotsearch:hppc | 0.5.2 | http://labs.carrotsearch.com/hppc.html/hppc
HttpClient | commons-httpclient:commons-httpclient | 3.1 | http://jakarta.apache.org/httpcomponents/httpclient-3.x/
I18N Libraries | org.apache.abdera:abdera-i18n | 1.1.1 | http://abdera.apache.org
ISO Parser | com.googlecode.mp4parser:isoparser | 1.0-RC-1 | http://code.google.com/p/mp4parser/
J2ObjC Annotations | com.google.j2objc:j2objc-annotations | 1.3 | https://github.com/google/j2objc/
Jackson 2 extensions to the Google HTTP Client Library for Java. | com.google.http-client:google-http-client-jackson2 | 1.19.0 | http://code.google.com/p/google-http-java-client/google-http-client-jackson2/
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.10.0 | http://github.com/FasterXML/jackson
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.13.1 | http://github.com/FasterXML/jackson
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.15.2 | https://github.com/FasterXML/jackson
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.16.2 | https://github.com/FasterXML/jackson
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.10.0 | https://github.com/FasterXML/jackson-core
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.13.1 | https://github.com/FasterXML/jackson-core
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.15.2 | https://github.com/FasterXML/jackson-core
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.16.2 | https://github.com/FasterXML/jackson-core
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.10.0 | http://github.com/FasterXML/jackson
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.13.1 | http://github.com/FasterXML/jackson
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.15.2 | https://github.com/FasterXML/jackson
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.16.2 | https://github.com/FasterXML/jackson
Jackson module: Old JAXB Annotations (javax.xml.bind) | com.fasterxml.jackson.module:jackson-module-jaxb-annotations | 2.15.2 | https://github.com/FasterXML/jackson-modules-base
Jakarta Bean Validation API | jakarta.validation:jakarta.validation-api | 2.0.2 | https://beanvalidation.org
Jasig CAS Client for Java - Core | org.jasig.cas.client:cas-client-core | 3.6.1 | https://www.apereo.org/cas/cas-client-core
Java 6 (and higher) extensions to the Google OAuth Client Library for Java. | com.google.oauth-client:google-oauth-client-java6 | 1.35.0 | https://github.com/googleapis/google-oauth-java-client/google-oauth-client-java6
Java Annotation Indexer | org.jboss:jandex | 2.4.2.Final | http://www.jboss.org/jandex
JavaMail 1.4 | org.apache.geronimo.specs:geronimo-javamail_1.4_spec | 1.6 | http://geronimo.apache.org/maven/specs/geronimo-javamail_1.4_spec/1.6
javax.inject | javax.inject:javax.inject | 1 | http://code.google.com/p/atinject/
jaxen | jaxen:jaxen | 1.1.1 | http://jaxen.codehaus.org/
JBoss Logging 3 | org.jboss.logging:jboss-logging | 3.4.3.Final | http://www.jboss.org
JCL 1.2 implemented over SLF4J | org.slf4j:jcl-over-slf4j | 1.7.36 | http://www.slf4j.org
JCL 1.2 implemented over SLF4J | org.slf4j:jcl-over-slf4j | 2.0.12 | http://www.slf4j.org
jcommander | com.beust:jcommander | 1.78 | https://jcommander.org
jdbm | jdbm:jdbm | 1.0 | no url defined
jdom | jdom:jdom | 1.0 | no url defined
Jetty Server | org.mortbay.jetty:jetty | 6.1.14 | http://jetty.mortbay.org/project/modules/jetty
Jetty Servlet Tester | org.mortbay.jetty:jetty-servlet-tester | 6.1.14 | http://jetty.mortbay.org/project/jetty-servlet-tester
Jetty Utilities | org.mortbay.jetty:jetty-util | 6.1.14 | http://jetty.mortbay.org/project/jetty-util
JEval | net.sourceforge.jeval:jeval | 0.9.4 | http://jeval.sourceforge.net
JHeaps | org.jheaps:jheaps | 0.14 | http://www.jheaps.org
jMimeMagic | net.sf.jmimemagic:jmimemagic | 0.1.5 | http://github.com/arimus/jmimemagic/
Joda time | joda-time:joda-time | 2.2 | http://joda-time.sourceforge.net
Joda-Time | joda-time:joda-time | 2.3 | http://www.joda.org/joda-time/
Joda-Time | joda-time:joda-time | 2.5 | http://www.joda.org/joda-time/
jsinterop-annotations | com.google.jsinterop:jsinterop-annotations | 1.0.0 | http://www.gwtproject.org/jsinterop-annotations/
json-jena | com.hp.hpl.jena:json-jena | 1.0 | no url defined
JUnitParams | pl.pragmatists:JUnitParams | 1.0.2 | http://junitparams.googlecode.com
Lucene codecs | org.apache.lucene:lucene-codecs | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-codecs
Lucene Common Analyzers | org.apache.lucene:lucene-analyzers-common | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-common
Lucene Common Analyzers | org.apache.lucene:lucene-analyzers-common | 5.5.5 | http://lucene.apache.org/lucene-parent/lucene-analyzers-common
Lucene Core | org.apache.lucene:lucene-core | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-core
Lucene Core | org.apache.lucene:lucene-core | 5.5.5 | http://lucene.apache.org/lucene-parent/lucene-core
Lucene Expressions | org.apache.lucene:lucene-expressions | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-expressions
Lucene Facets | org.apache.lucene:lucene-facet | 5.5.5 | http://lucene.apache.org/lucene-parent/lucene-facet
Lucene Grouping | org.apache.lucene:lucene-grouping | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-grouping
Lucene Grouping | org.apache.lucene:lucene-grouping | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-grouping
Lucene Highlighter | org.apache.lucene:lucene-highlighter | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-highlighter
Lucene Highlighter | org.apache.lucene:lucene-highlighter | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-highlighter
Lucene ICU Analysis Components | org.apache.lucene:lucene-analyzers-icu | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-icu
Lucene Join | org.apache.lucene:lucene-join | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-join
Lucene Join | org.apache.lucene:lucene-join | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-join
Lucene Kuromoji Japanese Morphological Analyzer | org.apache.lucene:lucene-analyzers-kuromoji | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-kuromoji
Lucene Memory | org.apache.lucene:lucene-memory | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-memory
Lucene Memory | org.apache.lucene:lucene-memory | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-memory
Lucene Miscellaneous | org.apache.lucene:lucene-misc | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-misc
Lucene Miscellaneous | org.apache.lucene:lucene-misc | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-misc
Lucene Miscellaneous | org.apache.lucene:lucene-misc | 5.5.5 | http://lucene.apache.org/lucene-parent/lucene-misc
Lucene Morfologik Polish Lemmatizer | org.apache.lucene:lucene-analyzers-morfologik | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-morfologik
Lucene Phonetic Filters | org.apache.lucene:lucene-analyzers-phonetic | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-phonetic
Lucene Queries | org.apache.lucene:lucene-queries | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-queries
Lucene Queries | org.apache.lucene:lucene-queries | 5.5.5 | http://lucene.apache.org/lucene-parent/lucene-queries
Lucene QueryParsers | org.apache.lucene:lucene-queryparser | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-queryparser
Lucene QueryParsers | org.apache.lucene:lucene-queryparser | 5.5.5 | http://lucene.apache.org/lucene-parent/lucene-queryparser
Lucene Sandbox | org.apache.lucene:lucene-sandbox | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-sandbox
Lucene Smart Chinese Analyzer | org.apache.lucene:lucene-analyzers-smartcn | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-smartcn
Lucene Spatial | org.apache.lucene:lucene-spatial | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-spatial
Lucene Spatial | org.apache.lucene:lucene-spatial | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-spatial
Lucene Stempel Analyzer | org.apache.lucene:lucene-analyzers-stempel | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-stempel
Lucene Suggest | org.apache.lucene:lucene-suggest | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-suggest
Lucene Suggest | org.apache.lucene:lucene-suggest | 4.10.4 | http://lucene.apache.org/lucene-parent/lucene-suggest
metadata-extractor | com.drewnoakes:metadata-extractor | 2.6.2 | http://code.google.com/p/metadata-extractor/
Nimbus JOSE+JWT | com.nimbusds:nimbus-jose-jwt | 9.40 | https://bitbucket.org/connect2id/nimbus-jose-jwt
Noggit | org.noggit:noggit | 0.5 | http://noggit.org
oai4j | se.kb:oai4j | 0.6b1 | http://oai4j-client.sourceforge.net/
OAuth 2.0 verification code receiver for Google OAuth Client Library for Java. | com.google.oauth-client:google-oauth-client-jetty | 1.35.0 | https://github.com/googleapis/google-oauth-java-client/google-oauth-client-jetty
Ogg and Vorbis for Java, Core | org.gagravarr:vorbis-java-core | 0.1 | https://github.com/Gagravarr/VorbisJava
OGNL - Object Graph Navigation Library | ognl:ognl | 3.3.4 | https://github.com/jkuhnert/ognl/
opencsv | net.sf.opencsv:opencsv | 2.3 | http://opencsv.sf.net
org.apache.tools.ant | org.apache.ant:ant | 1.7.0 | http://ant.apache.org/ant/
org.apache.xmlgraphics:batik-anim | org.apache.xmlgraphics:batik-anim | 1.17 | http://xmlgraphics.apache.org/batik/batik-anim/
org.apache.xmlgraphics:batik-awt-util | org.apache.xmlgraphics:batik-awt-util | 1.17 | http://xmlgraphics.apache.org/batik/batik-awt-util/
org.apache.xmlgraphics:batik-bridge | org.apache.xmlgraphics:batik-bridge | 1.17 | http://xmlgraphics.apache.org/batik/batik-bridge/
org.apache.xmlgraphics:batik-codec | org.apache.xmlgraphics:batik-codec | 1.17 | http://xmlgraphics.apache.org/batik/batik-codec/
org.apache.xmlgraphics:batik-constants | org.apache.xmlgraphics:batik-constants | 1.17 | http://xmlgraphics.apache.org/batik/batik-constants/
org.apache.xmlgraphics:batik-css | org.apache.xmlgraphics:batik-css | 1.17 | http://xmlgraphics.apache.org/batik/batik-css/
org.apache.xmlgraphics:batik-dom | org.apache.xmlgraphics:batik-dom | 1.17 | http://xmlgraphics.apache.org/batik/batik-dom/
org.apache.xmlgraphics:batik-ext | org.apache.xmlgraphics:batik-ext | 1.17 | http://xmlgraphics.apache.org/batik/batik-ext/
org.apache.xmlgraphics:batik-extension | org.apache.xmlgraphics:batik-extension | 1.17 | http://xmlgraphics.apache.org/batik/batik-extension/
org.apache.xmlgraphics:batik-gvt | org.apache.xmlgraphics:batik-gvt | 1.17 | http://xmlgraphics.apache.org/batik/batik-gvt/
org.apache.xmlgraphics:batik-i18n | org.apache.xmlgraphics:batik-i18n | 1.17 | http://xmlgraphics.apache.org/batik/batik-i18n/
org.apache.xmlgraphics:batik-parser | org.apache.xmlgraphics:batik-parser | 1.17 | http://xmlgraphics.apache.org/batik/batik-parser/
org.apache.xmlgraphics:batik-script | org.apache.xmlgraphics:batik-script | 1.17 | http://xmlgraphics.apache.org/batik/batik-script/
org.apache.xmlgraphics:batik-shared-resources | org.apache.xmlgraphics:batik-shared-resources | 1.17 | http://xmlgraphics.apache.org/batik/batik-shared-resources/
org.apache.xmlgraphics:batik-svg-dom | org.apache.xmlgraphics:batik-svg-dom | 1.17 | http://xmlgraphics.apache.org/batik/batik-svg-dom/
org.apache.xmlgraphics:batik-svggen | org.apache.xmlgraphics:batik-svggen | 1.17 | http://xmlgraphics.apache.org/batik/batik-svggen/
org.apache.xmlgraphics:batik-transcoder | org.apache.xmlgraphics:batik-transcoder | 1.17 | http://xmlgraphics.apache.org/batik/batik-transcoder/
org.apache.xmlgraphics:batik-util | org.apache.xmlgraphics:batik-util | 1.17 | http://xmlgraphics.apache.org/batik/batik-util/
org.apache.xmlgraphics:batik-xml | org.apache.xmlgraphics:batik-xml | 1.17 | http://xmlgraphics.apache.org/batik/batik-xml/
oro | oro:oro | 2.0.8 | no url defined
pac4j: Java web security for CAS | org.pac4j:pac4j-cas | 6.0.2 | https://github.com/pac4j/pac4j/pac4j-cas
pac4j core | org.pac4j:pac4j-core | 6.0.2 | https://github.com/pac4j/pac4j/pac4j-core
pac4j implementation for JavaEE | org.pac4j:javaee-pac4j | 8.0.0 | https://github.com/pac4j/jee-pac4j/javaee-pac4j
pac4j JavaEE components | org.pac4j:pac4j-javaee | 6.0.1 | https://github.com/pac4j/pac4j/pac4j-javaee
QDox | com.thoughtworks.qdox:qdox | 1.12 | http://qdox.codehaus.org
reload4j | ch.qos.reload4j:reload4j | 1.2.22 | https://reload4j.qos.ch
ROME, RSS and atOM utilitiEs for Java | rome:rome | 0.9 | https://rome.dev.java.net/
ROME, RSS and atOM utilitiEs for Java | rome:rome | 1.0 | https://rome.dev.java.net/
Rome A9 OpenSearch | rome:opensearch | 0.1 | http://wiki.java.net/bin/view/Javawsxml/OpenSearch
rome-modules | org.rometools:rome-modules | 1.0 | http://www.rometools.org
SnakeYAML | org.yaml:snakeyaml | 1.12 | http://www.snakeyaml.org
Spatial4J | com.spatial4j:spatial4j | 0.4.1 | https://github.com/spatial4j/spatial4j
Spring AOP | org.springframework:spring-aop | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Beans | org.springframework:spring-beans | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Commons Logging Bridge | org.springframework:spring-jcl | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Context | org.springframework:spring-context | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Context Support | org.springframework:spring-context-support | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Core | org.springframework:spring-core | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Expression Language (SpEL) | org.springframework:spring-expression | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Framework: Mock | org.springframework:spring-mock | 2.0.8 | http://www.springframework.org
Spring TestContext Framework | org.springframework:spring-test | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Web | org.springframework:spring-web | 5.3.32 | https://github.com/spring-projects/spring-framework
Spring Web MVC | org.springframework:spring-webmvc | 5.3.32 | https://github.com/spring-projects/spring-framework
standard | taglibs:standard | 1.1.2 | no url defined
StAX API | stax:stax-api | 1.0.1 | http://stax.codehaus.org/
stax-api | stax:stax-api | 1.0 | no url defined
Streaming API for XML (STAX API 1.0) | org.apache.geronimo.specs:geronimo-stax-api_1.0_spec | 1.0.1 | http://geronimo.apache.org/specs/geronimo-stax-api_1.0_spec
Streaming API for XML (STAX API 1.0) | org.apache.geronimo.specs:geronimo-stax-api_1.0_spec | 1.0 | http://geronimo.apache.org/specs/geronimo-stax-api_1.0_spec
SWORD v2 :: Client Library | org.swordapp:sword2-client | 0.9.3 | http://www.swordapp.org/
SWORD v2 :: Common Server Library | org.swordapp:sword2-server | 1.0 | http://www.swordapp.org/
TagSoup | org.ccil.cowan.tagsoup:tagsoup | 1.2.1 | http://home.ccil.org/~cowan/XML/tagsoup/
Test Support | com.lyncode:test-support | 1.0.3 | http://nexus.sonatype.org/oss-repository-hosting.html/test-support
The ZK EL Library | org.zkoss.common:zel | 8.5.0 | http://www.zkoss.org/common
thymeleaf | org.thymeleaf:thymeleaf | 3.1.2.RELEASE | http://www.thymeleaf.org/thymeleaf-lib/thymeleaf
thymeleaf-spring5 | org.thymeleaf:thymeleaf-spring5 | 3.1.2.RELEASE | http://www.thymeleaf.org/thymeleaf-lib/thymeleaf-spring5
Titanium JSON-LD 1.1 | com.apicatalog:titanium-json-ld | 1.2.0 | https://github.com/filip26/titanium-json-ld
twitter4j-core | org.twitter4j:twitter4j-core | 4.0.6 | http://twitter4j.org/
unbescape | org.unbescape:unbescape | 1.1.6.RELEASE | http://www.unbescape.org
Woodstox | org.codehaus.woodstox:wstx-asl | 3.2.0 | http://woodstox.codehaus.org
Woodstox | org.codehaus.woodstox:wstx-asl | 3.2.6 | http://woodstox.codehaus.org
Woodstox | org.codehaus.woodstox:wstx-asl | 3.2.7 | http://woodstox.codehaus.org
xalan | xalan:xalan | 2.7.0 | no url defined
xalan | xalan:xalan | 2.7.3 | no url defined
Xerces2 Java Parser | xerces:xercesImpl | 2.8.0 | http://xerces.apache.org/xerces2-j
Xerces2 Java Parser | xerces:xercesImpl | 2.8.1 | http://xerces.apache.org/xerces2-j/
xml-apis | xml-apis:xml-apis | 1.3.02 | http://xml.apache.org/commons/#external
XmlBeans | org.apache.xmlbeans:xmlbeans | 2.3.0 | http://xmlbeans.apache.org
XmlBeans | org.apache.xmlbeans:xmlbeans | 2.6.0 | http://xmlbeans.apache.org
XML Commons External Components XML APIs | xml-apis:xml-apis | 1.0.b2 | http://xml.apache.org/commons/#external
XML Commons External Components XML APIs Extensions | xml-apis:xml-apis-ext | 1.3.04 | http://xml.apache.org/commons/components/external/
xml-matchers | org.xmlmatchers:xml-matchers | 0.10 | http://code.google.com/p/xml-matchers/
xmlParserAPIs | xerces:xmlParserAPIs | 2.0.2 | no url defined
xmlParserAPIs | xerces:xmlParserAPIs | 2.6.2 | no url defined
xmlParserAPIs | xml-apis:xmlParserAPIs | 2.0.2 | no url defined
XOAI : OAI-PMH Java Toolkit | com.lyncode:xoai | 3.2.9 | http://www.lyncode.com
zookeeper | org.apache.zookeeper:zookeeper | 3.4.6 | no url defined

###  Apache-2.0, BSD-4-Clause, EPL-1.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
databene ContiPerf | org.databene:contiperf | 2.2.0 | http://databene.org/contiperf

###  Apache-2.0, CDDL-1.0, EPL-1.0, LGPL-2.1-or-later, LGPL-3.0:

Name | id | version | url
---- | --- | --------- | ----
Restlet Core - API and Engine | org.restlet.jee:org.restlet | 2.1.1 | http://www.restlet.org/org.restlet
Restlet Extension - Servlet | org.restlet.jee:org.restlet.ext.servlet | 2.1.1 | http://www.restlet.org/org.restlet.ext.servlet

###  Apache-2.0, LGPL 2:

Name | id | version | url
---- | --- | --------- | ----
FreeHEP Graphics2D | org.freehep:freehep-graphics2d | 2.4 | http://freehep.github.com/freehep-vectorgraphics/freehep-graphics2d/
FreeHEP GraphicsBase | org.freehep:freehep-graphicsbase | 2.4 | http://freehep.github.com/freehep-vectorgraphics/freehep-graphicsbase/
FreeHEP GraphicsIO | org.freehep:freehep-graphicsio | 2.4 | http://freehep.github.com/freehep-vectorgraphics/freehep-graphicsio/
FreeHEP GraphicsIO Tests | org.freehep:freehep-graphicsio-tests | 2.4 | http://freehep.github.com/freehep-vectorgraphics/freehep-graphicsio-tests/
FreeHEP IO | org.freehep:freehep-io | 2.2.2 | http://freehep.github.com/freehep-io
FreeHEP PDF Driver | org.freehep:freehep-graphicsio-pdf | 2.4 | http://freehep.github.com/freehep-vectorgraphics/freehep-graphicsio-pdf/
FreeHEP PS Driver | org.freehep:freehep-graphicsio-ps | 2.4 | http://freehep.github.com/freehep-vectorgraphics/freehep-graphicsio-ps/

###  Apache-2.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
Java Native Access | net.java.dev.jna:jna | 5.10.0 | https://github.com/java-native-access/jna

###  Apache-2.0, LGPL-2.1-or-later, MPL-1.1:

Name | id | version | url
---- | --- | --------- | ----
Javassist | org.javassist:javassist | 3.18.2-GA | http://www.javassist.org/
Javassist | org.javassist:javassist | 3.29.0-GA | http://www.javassist.org/
Javassist | org.javassist:javassist | 3.29.2-GA | http://www.javassist.org/

###  Apache-2.0, SAX-PD, W3C:

Name | id | version | url
---- | --- | --------- | ----
XML Commons External Components XML APIs | xml-apis:xml-apis | 1.4.01 | http://xml.apache.org/commons/components/external/

###  Apache License, 2.0, BSD 2-Clause, EDL 1.0, EPL 2.0, GPL-2.0-with-classpath-exception, jQuery license, MIT, Modified BSD, Public Domain, W3C license:

Name | id | version | url
---- | --- | --------- | ----
jersey-container-servlet | org.glassfish.jersey.containers:jersey-container-servlet | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-container-servlet
jersey-container-servlet-core | org.glassfish.jersey.containers:jersey-container-servlet-core | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-container-servlet-core
jersey-core-client | org.glassfish.jersey.core:jersey-client | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/jersey-client
jersey-ext-entity-filtering | org.glassfish.jersey.ext:jersey-entity-filtering | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-entity-filtering
jersey-inject-hk2 | org.glassfish.jersey.inject:jersey-hk2 | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-hk2
jersey-media-jaxb | org.glassfish.jersey.media:jersey-media-jaxb | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-media-jaxb
jersey-spring5 | org.glassfish.jersey.ext:jersey-spring5 | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-spring5

###  Apache License, 2.0, EPL 2.0, Modified BSD, The GNU General Public License (GPL), Version 2, With Classpath Exception:

Name | id | version | url
---- | --- | --------- | ----
jersey-core-server | org.glassfish.jersey.core:jersey-server | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/jersey-server

###  Apache License, 2.0, EPL 2.0, Public Domain, The GNU General Public License (GPL), Version 2, With Classpath Exception:

Name | id | version | url
---- | --- | --------- | ----
jersey-core-common | org.glassfish.jersey.core:jersey-common | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/jersey-common

###  Apache License, 2.0, EPL 2.0, The GNU General Public License (GPL), Version 2, With Classpath Exception:

Name | id | version | url
---- | --- | --------- | ----
jersey-media-json-jackson | org.glassfish.jersey.media:jersey-media-json-jackson | 2.41 | https://projects.eclipse.org/projects/ee4j.jersey/project/jersey-media-json-jackson

###  Bouncy Castle:

Name | id | version | url
---- | --- | --------- | ----
Bouncy Castle ASN.1 Extension and Utility APIs | org.bouncycastle:bcutil-jdk15on | 1.70 | https://www.bouncycastle.org/java.html
Bouncy Castle CMS and S/MIME API | org.bouncycastle:bcmail-jdk15 | 1.45 | http://www.bouncycastle.org/java.html
Bouncy Castle PKIX, CMS, EAC, TSP, PKCS, OCSP, CMP, and CRMF APIs | org.bouncycastle:bcpkix-jdk15on | 1.60 | http://www.bouncycastle.org/java.html
Bouncy Castle PKIX, CMS, EAC, TSP, PKCS, OCSP, CMP, and CRMF APIs | org.bouncycastle:bcpkix-jdk15on | 1.70 | https://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15 | 1.45 | http://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15on | 1.60 | http://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15on | 1.70 | https://www.bouncycastle.org/java.html
Bouncy Castle S/MIME API | org.bouncycastle:bcmail-jdk15on | 1.60 | http://www.bouncycastle.org/java.html

###  BSD-2-Clause:

Name | id | version | url
---- | --- | --------- | ----
beam-core | uk.ac.ebi.beam:beam-core | 1.3.5 | http://www.github.com/johnmay/beam/beam-core/
beam-func | uk.ac.ebi.beam:beam-func | 1.3.5 | http://www.github.com/johnmay/beam/beam-func/
PostgreSQL JDBC Driver | org.postgresql:postgresql | 42.7.2 | https://jdbc.postgresql.org

###  BSD-3-Clause:

Name | id | version | url
---- | --- | --------- | ----
ANTLR 4 Runtime | org.antlr:antlr4-runtime | 4.12.0 | https://www.antlr.org/antlr4-runtime/
Apache Solr Webapp | org.dspace:dspace-solr | 5.1 | https://github.com/dspace/DSpace/dspace-solr
Biblio Transformation Engine :: Core | gr.ekt.bte:bte-core | 0.9.3.5 | http://github.com/EKT/Biblio-Transformation-Engine/bte-core
Biblio Transformation Engine :: Input/Output | gr.ekt.bte:bte-io | 0.9.3.5 | http://github.com/EKT/Biblio-Transformation-Engine/bte-io
cdm-core | edu.ucar:cdm-core | 5.4.2 | no url defined
coverity-escapers | com.coverity.security:coverity-escapers | 1.1.1 | http://coverity.com/security
DSpace JSP-UI | org.dspace:dspace-jspui | 5.1 | https://github.com/dspace/DSpace/dspace-jspui
DSpace Kernel :: Additions and Local Customizations | org.dspace.modules:additions | 5.1 | https://github.com/dspace/DSpace/modules/additions
DSpace Kernel :: API and Implementation | org.dspace:dspace-api | 5.1 | https://github.com/dspace/DSpace/dspace-api
DSpace OAI-PMH | org.dspace:dspace-oai | 5.1 | https://github.com/dspace/DSpace/dspace-oai
DSpace RDF | org.dspace:dspace-rdf | 5.1 | https://github.com/dspace/DSpace/dspace-rdf
DSpace REST :: API and Implementation | org.dspace:dspace-rest | 5.1 | http://demo.dspace.org
DSpace Services Framework :: API and Implementation | org.dspace:dspace-services | 5.1 | https://github.com/dspace/DSpace/dspace-services
DSpace SWORD | org.dspace:dspace-sword | 5.1 | https://github.com/dspace/DSpace/dspace-sword
DSpace SWORD v2 | org.dspace:dspace-swordv2 | 5.1 | https://github.com/dspace/DSpace/dspace-swordv2
Hamcrest All | org.hamcrest:hamcrest-all | 1.3 | https://github.com/hamcrest/JavaHamcrest/hamcrest-all
Hamcrest Core | org.hamcrest:hamcrest-core | 1.3 | https://github.com/hamcrest/JavaHamcrest/hamcrest-core
httpservices | edu.ucar:httpservices | 5.4.2 | no url defined
JBibTeX | org.jbibtex:jbibtex | 1.0.10 | http://www.jbibtex.org
JSONLD Java :: Core | com.github.jsonld-java:jsonld-java | 0.13.4 | http://github.com/jsonld-java/jsonld-java/jsonld-java/
JSONLD Java :: Core | com.github.jsonld-java:jsonld-java | 0.5.0 | http://github.com/jsonld-java/jsonld-java/jsonld-java/
Protocol Buffer Java API | com.google.protobuf:protobuf-java | 2.5.0 | http://code.google.com/p/protobuf
udunits | edu.ucar:udunits | 5.4.2 | no url defined

###  BSD-4-Clause:

Name | id | version | url
---- | --- | --------- | ----
ANTLR 3 Runtime | org.antlr:antlr-runtime | 3.5 | http://www.antlr.org
AntLR Parser Generator | antlr:antlr | 2.7.7 | http://www.antlr.org/
ARQ | com.hp.hpl.jena:arq | 2.2 | http://jena.sourceforge.net/ARQ/
ARQ Extra | com.hp.hpl.jena:arq-extra | 2.2 | http://jena.sourceforge.net/ARQ/
asm-analysis | org.ow2.asm:asm-analysis | 7.1 | http://asm.ow2.org/
ASM Commons | org.ow2.asm:asm-commons | 4.1 | http://asm.objectweb.org/asm-commons/
ASM Core | org.ow2.asm:asm | 4.1 | http://asm.objectweb.org/asm/
ASM Core | org.ow2.asm:asm | 4.2 | http://asm.objectweb.org/asm/
asm-tree | org.ow2.asm:asm-tree | 7.1 | http://asm.ow2.org/
asm-util | org.ow2.asm:asm-util | 7.1 | http://asm.ow2.org/
Automaton | dk.brics.automaton:automaton | 1.11-8 | http://www.brics.dk/automaton/
dnsjava | dnsjava:dnsjava | 2.1.1 | http://www.dnsjava.org
dom4j | dom4j:dom4j | 1.6.1 | http://dom4j.org
DSpace I18N :: Language Packs | org.dspace:dspace-api-lang | 5.0.7 | http://nexus.sonatype.org/oss-repository-hosting.html/dspace-api-lang
handle | org.dspace:handle | 6.2 | no url defined
IRI | com.hp.hpl.jena:iri | 0.5 | http://jena.sourceforge.net/
jargon | org.dspace:jargon | 1.4.25 | no url defined
jaxen | jaxen:jaxen | 1.1 | http://jaxen.codehaus.org/
Jena | com.hp.hpl.jena:jena | 2.5.5 | http://jena.sourceforge.net/
Jena | com.hp.hpl.jena:jena | 2.6.4 | http://www.openjena.org/
Jena IRI | com.hp.hpl.jena:iri | 0.8 | http://jena.sf.net/iri
Jena Tests | com.hp.hpl.jena:jenatest | 2.5.5 | http://jena.sourceforge.net/
mets | org.dspace:mets | 1.5.2 | no url defined
Morfologik FSA | org.carrot2:morfologik-fsa | 1.7.1 | http://morfologik.blogspot.com/morfologik-fsa/
Morfologik Stemming APIs | org.carrot2:morfologik-stemming | 1.7.1 | http://morfologik.blogspot.com/morfologik-stemming/
Morfologik Stemming Dictionary for Polish | org.carrot2:morfologik-polish | 1.7.1 | http://morfologik.blogspot.com/morfologik-polish/
oclc-harvester2 | org.dspace:oclc-harvester2 | 0.1.12 | no url defined
XMLUnit for Java | xmlunit:xmlunit | 1.1 | http://xmlunit.sourceforge.net/
XMLUnit for Java | xmlunit:xmlunit | 1.3 | http://xmlunit.sourceforge.net/
XMP Library for Java | com.adobe.xmp:xmpcore | 5.1.2 | http://www.adobe.com/devnet/xmp.html

###  CDDL+GPL License:

Name | id | version | url
---- | --- | --------- | ----
JAXB Runtime | org.glassfish.jaxb:jaxb-runtime | 2.3.1 | http://jaxb.java.net/jaxb-runtime-parent/jaxb-runtime
TXW2 Runtime | org.glassfish.jaxb:txw2 | 2.3.1 | http://jaxb.java.net/jaxb-txw-parent/txw2

###  CDDL-1.0:

Name | id | version | url
---- | --- | --------- | ----
JavaBeans Activation Framework (JAF) | javax.activation:activation | 1.1 | http://java.sun.com/products/javabeans/jaf/index.jsp
jsp-api | javax.servlet:jsp-api | 2.0 | no url defined
jstl | javax.servlet:jstl | 1.1.2 | no url defined
servlet-api | javax.servlet:servlet-api | 2.5 | no url defined
Servlet Specification 2.5 API | org.mortbay.jetty:servlet-api-2.5 | 6.1.14 | http://jetty.mortbay.org/project/modules/servlet-api-2.5

###  CDDL-1.0, GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
JavaMail API (compat) | javax.mail:mail | 1.4.7 | http://kenai.com/projects/javamail/mail
Java Transaction API | org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec | 1.1.1.Final | http://www.jboss.org/jboss-transaction-api_1.2_spec

###  CDDL-1.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
JHighlight | com.uwyn:jhighlight | 1.0 | https://jhighlight.dev.java.net/

###  CDDL-1.0 + GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
JavaBeans Activation Framework API jar | javax.activation:javax.activation-api | 1.2.0 | http://java.net/all/javax.activation-api/
Java Servlet API | javax.servlet:javax.servlet-api | 3.0.1 | http://servlet-spec.java.net
Java Servlet API | javax.servlet:javax.servlet-api | 4.0.1 | https://javaee.github.io/servlet-spec/
javax.annotation API | javax.annotation:javax.annotation-api | 1.3.2 | http://jcp.org/en/jsr/detail?id=250

###  CDDL 1.1, GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
istack common utility code runtime | com.sun.istack:istack-commons-runtime | 3.0.7 | http://java.net/istack-commons/istack-commons-runtime/
jaxb-api | javax.xml.bind:jaxb-api | 2.3.1 | https://github.com/javaee/jaxb-spec/jaxb-api
JAXB Reference Implementation | com.sun.xml.bind:jaxb-impl | 2.2.5 | http://jaxb.java.net/

###  CDDL-1.1 + GPL-2.0:

Name | id | version | url
---- | --- | --------- | ----
Extended StAX API | org.jvnet.staxex:stax-ex | 1.8 | http://stax-ex.java.net/

###  CPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
JUnit | junit:junit | 4.11 | http://junit.org

###  Eclipse Distribution License - v 1.0:

Name | id | version | url
---- | --- | --------- | ----
istack common utility code runtime | com.sun.istack:istack-commons-runtime | 3.0.11 | https://projects.eclipse.org/projects/ee4j/istack-commons/istack-commons-runtime
Jakarta XML Binding API | jakarta.xml.bind:jakarta.xml.bind-api | 2.3.3 | https://github.com/eclipse-ee4j/jaxb-api/jakarta.xml.bind-api
JAXB Runtime | org.glassfish.jaxb:jaxb-runtime | 2.3.3 | https://eclipse-ee4j.github.io/jaxb-ri/jaxb-runtime-parent/jaxb-runtime
TXW2 Runtime | org.glassfish.jaxb:txw2 | 2.3.3 | https://eclipse-ee4j.github.io/jaxb-ri/jaxb-txw-parent/txw2

###  Eclipse Public License 2.0, GNU General Public License, version 2 with the GNU Classpath Exception:

Name | id | version | url
---- | --- | --------- | ----
JSON-P Default Provider | org.glassfish:jakarta.json | 2.0.1 | https://github.com/eclipse-ee4j/jsonp

###  EDL 1.0:

Name | id | version | url
---- | --- | --------- | ----
Jakarta Activation | com.sun.activation:jakarta.activation | 1.2.2 | https://github.com/eclipse-ee4j/jaf/jakarta.activation
Jakarta Activation API jar | jakarta.activation:jakarta.activation-api | 1.2.2 | https://github.com/eclipse-ee4j/jaf/jakarta.activation-api

###  EPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
AspectJ runtime | org.aspectj:aspectjrt | 1.6.11 | http://www.aspectj.org
javax.persistence-api | javax.persistence:javax.persistence-api | 2.2 | https://github.com/javaee/jpa-spec
org.apache.xml.resolver_1.2.0.v201005080400.jar | org.eclipse.birt.runtime.3_7_1:org.apache.xml.resolver | 1.2.0 | http://www.eclipse.org/projects/project.php?id=birt
org.apache.xml.serializer_2.7.1.v201005080400.jar | org.eclipse.birt.runtime.3_7_1:org.apache.xml.serializer | 2.7.1 | http://www.eclipse.org/projects/project.php?id=birt

###  EPL 2.0, GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
aopalliance version 1.0 repackaged as a module | org.glassfish.hk2.external:aopalliance-repackaged | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/external/aopalliance-repackaged
Class Model for Hk2 | org.glassfish.hk2:class-model | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/class-model
HK2 API module | org.glassfish.hk2:hk2-api | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/hk2-api
HK2 core module | org.glassfish.hk2:hk2-core | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/hk2-core
HK2 Implementation Utilities | org.glassfish.hk2:hk2-utils | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/hk2-utils
HK2 module of HK2 itself | org.glassfish.hk2:hk2 | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/hk2
HK2 Spring Bridge | org.glassfish.hk2:spring-bridge | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/spring-bridge
jakarta.ws.rs-api | jakarta.ws.rs:jakarta.ws.rs-api | 2.1.6 | https://github.com/eclipse-ee4j/jaxrs-api
Jakarta Annotations API | jakarta.annotation:jakarta.annotation-api | 1.3.5 | https://projects.eclipse.org/projects/ee4j.ca
javax.inject:1 as OSGi bundle | org.glassfish.hk2.external:jakarta.inject | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/external/jakarta.inject
OSGi resource locator | org.glassfish.hk2:osgi-resource-locator | 1.0.3 | https://projects.eclipse.org/projects/ee4j/osgi-resource-locator
Run Level Service | org.glassfish.hk2:hk2-runlevel | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/hk2-runlevel
ServiceLocator Default Implementation | org.glassfish.hk2:hk2-locator | 2.6.1 | https://github.com/eclipse-ee4j/glassfish-hk2/hk2-locator

###  EPL-2.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
JGraphT - Core | org.jgrapht:jgrapht-core | 1.5.2 | http://www.jgrapht.org/jgrapht-core
JGraphT - I/O | org.jgrapht:jgrapht-io | 1.5.2 | http://www.jgrapht.org/jgrapht-io

###  GNU Library General Public License v2.1 or later, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
Hibernate Commons Annotations | org.hibernate.common:hibernate-commons-annotations | 5.1.2.Final | http://hibernate.org
Hibernate ORM - hibernate-core | org.hibernate:hibernate-core | 5.6.15.Final | https://hibernate.org/orm
Hibernate ORM - hibernate-hikaricp | org.hibernate:hibernate-hikaricp | 5.6.15.Final | https://hibernate.org/orm
Hibernate ORM - hibernate-jcache | org.hibernate:hibernate-jcache | 5.6.15.Final | https://hibernate.org/orm

###  Go License:

Name | id | version | url
---- | --- | --------- | ----
RE2/J | com.google.re2j:re2j | 1.3 | http://github.com/google/re2j

###  GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
vecmath | javax.vecmath:vecmath | 1.5.2 | https://java.net/projects/vecmath/

###  ICU:

Name | id | version | url
---- | --- | --------- | ----
ICU4J | com.ibm.icu:icu4j | 51.1 | http://icu-project.org/
ICU4J | com.ibm.icu:icu4j | 53.1 | http://icu-project.org/

###  JSR-000107 JCACHE 2.9 Public Review - Updated Specification
                License:

Name | id | version | url
---- | --- | --------- | ----
JSR107 API and SPI | javax.cache:cache-api | 1.0.0 | https://github.com/jsr107/jsr107spec

###  LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
cdk-atomtype | org.openscience.cdk:cdk-atomtype | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-atomtype/
cdk-builder3d | org.openscience.cdk:cdk-builder3d | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-builder3d/
cdk-builder3dtools | org.openscience.cdk:cdk-builder3dtools | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-builder3dtools/
cdk-bundle | org.openscience.cdk:cdk-bundle | 2.9 | http://sourceforge.net/projects/cdk/cdk-bundle/
cdk-charges | org.openscience.cdk:cdk-charges | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-charges/
cdk-cip | org.openscience.cdk:cdk-cip | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-cip/
cdk-control | org.openscience.cdk:cdk-control | 2.9 | http://sourceforge.net/projects/cdk/cdk-misc/cdk-control/
cdk-core | org.openscience.cdk:cdk-core | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-core/
cdk-ctab | org.openscience.cdk:cdk-ctab | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-ctab/
cdk-data | org.openscience.cdk:cdk-data | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-data/
cdk-datadebug | org.openscience.cdk:cdk-datadebug | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-datadebug/
cdk-depict | org.openscience.cdk:cdk-depict | 2.9 | http://sourceforge.net/projects/cdk/cdk-app/cdk-depict/
cdk-dict | org.openscience.cdk:cdk-dict | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-dict/
cdk-diff | org.openscience.cdk:cdk-diff | 2.9 | http://sourceforge.net/projects/cdk/cdk-misc/cdk-diff/
cdk-extra | org.openscience.cdk:cdk-extra | 2.9 | http://sourceforge.net/projects/cdk/cdk-misc/cdk-extra/
cdk-fingerprint | org.openscience.cdk:cdk-fingerprint | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-fingerprint/
cdk-forcefield | org.openscience.cdk:cdk-forcefield | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-forcefield/
cdk-formula | org.openscience.cdk:cdk-formula | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-formula/
cdk-fragment | org.openscience.cdk:cdk-fragment | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-fragment/
cdk-group | org.openscience.cdk:cdk-group | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-group/
cdk-hash | org.openscience.cdk:cdk-hash | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-hash/
cdk-inchi | org.openscience.cdk:cdk-inchi | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-inchi/
cdk-interfaces | org.openscience.cdk:cdk-interfaces | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-interfaces/
cdk-io | org.openscience.cdk:cdk-io | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-io/
cdk-ioformats | org.openscience.cdk:cdk-ioformats | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-ioformats/
cdk-ionpot | org.openscience.cdk:cdk-ionpot | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-ionpot/
cdk-iordf | org.openscience.cdk:cdk-iordf | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-iordf/
cdk-isomorphism | org.openscience.cdk:cdk-isomorphism | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-isomorphism/
cdk-jniinchi-support | org.openscience.cdk:cdk-jniinchi-support | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-jniinchi-support/
cdk-legacy | org.openscience.cdk:cdk-legacy | 2.9 | http://sourceforge.net/projects/cdk/cdk-legacy/
cdk-libiocml | org.openscience.cdk:cdk-libiocml | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-libiocml/
cdk-libiomd | org.openscience.cdk:cdk-libiomd | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-libiomd/
cdk-model | org.openscience.cdk:cdk-model | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-model/
cdk-pcore | org.openscience.cdk:cdk-pcore | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-pcore/
cdk-pdb | org.openscience.cdk:cdk-pdb | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-pdb/
cdk-pdbcml | org.openscience.cdk:cdk-pdbcml | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-pdbcml/
cdk-qm | org.openscience.cdk:cdk-qm | 2.9 | http://sourceforge.net/projects/cdk/cdk-misc/cdk-qm/
cdk-qsar | org.openscience.cdk:cdk-qsar | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsar/
cdk-qsaratomic | org.openscience.cdk:cdk-qsaratomic | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsaratomic/
cdk-qsarbond | org.openscience.cdk:cdk-qsarbond | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsarbond/
cdk-qsarcml | org.openscience.cdk:cdk-qsarcml | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsarcml/
cdk-qsarionpot | org.openscience.cdk:cdk-qsarionpot | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsarionpot/
cdk-qsarmolecular | org.openscience.cdk:cdk-qsarmolecular | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsarmolecular/
cdk-qsarprotein | org.openscience.cdk:cdk-qsarprotein | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-qsarprotein/
cdk-reaction | org.openscience.cdk:cdk-reaction | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-reaction/
cdk-render | org.openscience.cdk:cdk-render | 2.9 | http://sourceforge.net/projects/cdk/cdk-display/cdk-render/
cdk-renderawt | org.openscience.cdk:cdk-renderawt | 2.9 | http://sourceforge.net/projects/cdk/cdk-display/cdk-renderawt/
cdk-renderbasic | org.openscience.cdk:cdk-renderbasic | 2.9 | http://sourceforge.net/projects/cdk/cdk-display/cdk-renderbasic/
cdk-renderextra | org.openscience.cdk:cdk-renderextra | 2.9 | http://sourceforge.net/projects/cdk/cdk-display/cdk-renderextra/
cdk-sdg | org.openscience.cdk:cdk-sdg | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-sdg/
cdk-signature | org.openscience.cdk:cdk-signature | 2.9 | http://sourceforge.net/projects/cdk/cdk-descriptor/cdk-signature/
cdk-silent | org.openscience.cdk:cdk-silent | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-silent/
cdk-smarts | org.openscience.cdk:cdk-smarts | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-smarts/
cdk-smiles | org.openscience.cdk:cdk-smiles | 2.9 | http://sourceforge.net/projects/cdk/cdk-storage/cdk-smiles/
cdk-standard | org.openscience.cdk:cdk-standard | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-standard/
cdk-structgen | org.openscience.cdk:cdk-structgen | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-structgen/
cdk-tautomer | org.openscience.cdk:cdk-tautomer | 2.9 | http://sourceforge.net/projects/cdk/cdk-tool/cdk-tautomer/
cdk-valencycheck | org.openscience.cdk:cdk-valencycheck | 2.9 | http://sourceforge.net/projects/cdk/cdk-base/cdk-valencycheck/
DSpace TM-Extractors Dependency | org.dspace.dependencies:dspace-tm-extractors | 1.0.1 | http://projects.dspace.org/dspace-pom/dspace-tm-extractors
FindBugs-Annotations | com.google.code.findbugs:annotations | 3.0.0 | http://findbugs.sourceforge.net/
FindBugs-Native-Annotations | com.google.code.findbugs:findbugs-annotations | 3.0.1 | http://findbugs.sourceforge.net/
Hibernate Search Engine | org.hibernate:hibernate-search-engine | 5.11.12.Final | http://search.hibernate.org/hibernate-search-engine
Hibernate Search ORM | org.hibernate:hibernate-search-orm | 5.11.12.Final | http://search.hibernate.org/hibernate-search-orm
im4java | org.im4java:im4java | 1.4.0 | http://sourceforge.net/projects/im4java/
JGraphT | jgrapht:jgrapht | 0.6.0 | http://jgrapht.sourceforge.net
JNA InChI API | io.github.dan2097:jna-inchi-api | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-api
JNA InChI Core | io.github.dan2097:jna-inchi-core | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-core
JNA InChI Darwin AArch64 | io.github.dan2097:jna-inchi-darwin-aarch64 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-darwin-aarch64
JNA InChI Darwin x86-64 | io.github.dan2097:jna-inchi-darwin-x86-64 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-darwin-x86-64
JNA InChI Linux AArch64 | io.github.dan2097:jna-inchi-linux-aarch64 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-linux-aarch64
JNA InChI Linux ARM | io.github.dan2097:jna-inchi-linux-arm | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-linux-arm
JNA InChI Linux x86 | io.github.dan2097:jna-inchi-linux-x86 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-linux-x86
JNA InChI Linux x86-64 | io.github.dan2097:jna-inchi-linux-x86-64 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-linux-x86-64
JNA InChI Win32 x86 | io.github.dan2097:jna-inchi-win32-x86 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-win32-x86
JNA InChI Win32 x86-64 | io.github.dan2097:jna-inchi-win32-x86-64 | 1.2 | https://github.com/dan2097/jna-inchi/jna-inchi-win32-x86-64
MaxMind GeoIP API | com.maxmind.geoip:geoip-api | 1.2.11 | https://github.com/maxmind/geoip-api-java
Mets package | cat.iciq.mets:mets | 1.0 | no url defined
xom | xom:xom | 1.1 | http://www.xom.nu
XOM | xom:xom | 1.2.5 | http://xom.nu
XOM | xom:xom | 1.3.9 | https://xom.nu

###  LGPL-3.0:

Name | id | version | url
---- | --- | --------- | ----
The ZKoss Common Library | org.zkoss.common:zcommon | 8.5.0 | http://www.zkoss.org/common
The ZKoss Web Library | org.zkoss.common:zweb | 8.5.0 | http://www.zkoss.org/web
ZK Bind | org.zkoss.zk:zkbind | 8.5.0 | http://www.zkoss.org/zkbind
ZK Kernel | org.zkoss.zk:zk | 8.5.0 | http://www.zkoss.org/zk
ZK Plus Utilities | org.zkoss.zk:zkplus | 8.5.0 | http://www.zkoss.org/zkplus
ZK XHTML Components | org.zkoss.zk:zhtml | 8.5.0 | http://www.zkoss.org/zhtml
ZK XUL Components | org.zkoss.zk:zul | 8.5.0 | http://www.zkoss.org/zul

###  MIT:

Name | id | version | url
---- | --- | --------- | ----
apfloat | org.apfloat:apfloat | 1.10.1 | http://www.apfloat.org
args4j | args4j:args4j | 2.33 | http://args4j.kohsuke.org/args4j/
Checker Qual | org.checkerframework:checker-qual | 3.42.0 | https://checkerframework.org/
Checker Qual | org.checkerframework:checker-qual | 3.5.0 | https://checkerframework.org
dexx | com.github.andrewoma.dexx:collection | 0.7 | https://github.com/andrewoma/dexx
jsoup | org.jsoup:jsoup | 1.6.1 | http://jsoup.org/
JUL to SLF4J bridge | org.slf4j:jul-to-slf4j | 2.0.12 | http://www.slf4j.org
Main | org.jmockit:jmockit | 1.10 | http://www.jmockit.org
Mockito | org.mockito:mockito-all | 1.9.5 | http://www.mockito.org
Mockito | org.mockito:mockito-core | 1.9.5 | http://www.mockito.org
Objenesis | org.objenesis:objenesis | 1.0 | http://objenesis.googlecode.com/svn/docs/index.html
OpenCloud | org.mcavallo:opencloud | 0.3 | http://opencloud.mcavallo.org/
Project Lombok | org.projectlombok:lombok | 1.18.32 | https://projectlombok.org
Signatures | com.github.gilleain.signatures:signatures | 1.1 | https://github.com/gilleain/signatures
SLF4J API Module | org.slf4j:slf4j-api | 1.7.25 | http://www.slf4j.org
SLF4J API Module | org.slf4j:slf4j-api | 1.7.28 | http://www.slf4j.org
SLF4J API Module | org.slf4j:slf4j-api | 1.7.36 | http://www.slf4j.org
SLF4J API Module | org.slf4j:slf4j-api | 2.0.12 | http://www.slf4j.org
SLF4J Reload4j Provider | org.slf4j:slf4j-reload4j | 2.0.12 | http://reload4j.qos.ch
zsoup | org.zkoss:zsoup | 1.8.2.5 | http://www.zkoss.org

###  MPL-1.1:

Name | id | version | url
---- | --- | --------- | ----
juniversalchardet | com.googlecode.juniversalchardet:juniversalchardet | 1.0.3 | http://juniversalchardet.googlecode.com/

###  MPL-2.0:

Name | id | version | url
---- | --- | --------- | ----
Saxon-HE | net.sf.saxon:Saxon-HE | 9.9.1-8 | http://www.saxonica.com/

###  MPL-2.0 + EPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
H2 Database Engine | com.h2database:h2 | 1.4.180 | http://www.h2database.com

###  Public Domain:

Name | id | version | url
---- | --- | --------- | ----
JAMA | gov.nist.math:jama | 1.0.3 | http://math.nist.gov/javanumerics/jama/
XZ for Java | org.tukaani:xz | 1.4 | http://tukaani.org/xz/java.html

###  Public Domain, The New BSD License:

Name | id | version | url
---- | --- | --------- | ----
Reflections | org.reflections:reflections | 0.9.11 | http://github.com/ronmamo/reflections

###  Similar to Apache License but with the acknowledgment clause removed:

Name | id | version | url
---- | --- | --------- | ----
JDOM | org.jdom:jdom2 | 2.0.6 | http://www.jdom.org

###  Unknown license:

Name | id | version | url
---- | --- | --------- | ----
serializer | xalan:serializer | 2.7.3 | no url defined


/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.dspace.app.util.TokenManager;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.authorize.ResourcePolicy;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Collection;
import org.dspace.content.DSpaceObject;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.eperson.EPerson;
import org.dspace.eperson.Group;
import org.dspace.handle.HandleManager;
import org.dspace.embargo.CustomEmbargoSetter;

public class CollectionReviewServlet extends DSpaceServlet{
	
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(CollectionReviewServlet.class);
	
	private static Pattern pathPattern = Pattern.compile("\\/.*\\/.*\\/[0-9a-fA-F]{24}");

    protected void doDSGet(Context context, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, AuthorizeException {   
    	String handle = validateAccess(request);
    	DSpaceObject dso = HandleManager.resolveToObject(context, handle);    	
    	Collection collection = (Collection) dso;
    	
    	int itemsToReview = countEmbargoedElements(context, collection);
    	if(itemsToReview > 0 && context.getCurrentUser() == null){    		
        	//Create user and group
        	EPerson reviewer = getOrCreateReviewersUser(context, collection.getHandle());
        	Group reviewersGroup = getOrCreateReviewersGroup(context);
        	associateReviewerToGroup(reviewer, reviewersGroup);
        	//Create new access policies
        	createAccessPolicies(context, collection, reviewer, reviewersGroup);        	
        	context.setCurrentUser(reviewer);
        	HttpSession session = request.getSession();
        	request.setAttribute("is.admin", false);
            request.setAttribute("dspace.current.user", reviewer);
            session.setAttribute("dspace.current.user.id", Integer.valueOf(reviewer.getID()));
            session.setAttribute("dspace.current.remote.addr", request.getRemoteAddr());
            request.setAttribute("dspace.context", context);        
    	}
    	
    	JSPManager.showJSP(request, response, "/handle/" + collection.getHandle());
    }
    
    private String validateAccess(HttpServletRequest request) throws AuthorizeException{
        String path = request.getPathInfo();
        String handle = null;
        String key = null;
        
        if (path != null && pathPattern.matcher(path).matches())
        {
        	// substring(1) is to remove initial '/'
            path = path.substring(1);
            try
            {
                // Extract the Handle
                int firstSlash = path.indexOf('/');
                int secondSlash = path.indexOf('/', firstSlash + 1);
                // We have extra path info
                handle = path.substring(0, secondSlash);
                key = path.substring(secondSlash + 1);
            }
            catch (NumberFormatException nfe)
            {
            	throw new AuthorizeException("Malformed URL request.");
            }
        } else{
        	throw new AuthorizeException("Malformed URL request.");
        }
        
        //Check request validity
        
        String salt = ConfigurationManager.getProperty("module.communication.secret");
        String secret = getBrowseBaseUrl(request) + "#" + handle + "#" + "review-collection";
        String calculatedKey = TokenManager.encode(salt, secret);
        calculatedKey = calculatedKey.substring(calculatedKey.length() -24 ,calculatedKey.length()); //Keep last part, enough characters to be safe 8x10^28 combinations
        if (!key.equals(calculatedKey))
        {
        	throw new AuthorizeException("Not authorized request for collection review.");
        }     
        return handle;
    }
    
    
    private int countEmbargoedElements(Context context, Collection collection) throws AuthorizeException{
    	int embargoedElements = 0;
    	try{
        	ItemIterator iterator = collection.getItems();
        	while(iterator.hasNext()){
        		Item item = iterator.next();    		
        		List<ResourcePolicy> policies = AuthorizeManager.getPoliciesActionFilter(context, item, 0);        		
        		for(ResourcePolicy policy : policies)
        			if(policy.getGroupID() == Group.ANONYMOUS_ID && isDateValid(policy))
        				embargoedElements++;
        	} 
        	context.commit();
    	}catch(Exception e){    		
    		log.error("Error counting embargoed elements on collection with handle" + collection.getHandle());     		
    		throw new AuthorizeException("Not authorized request for collection review.");
    	}    	
    	return embargoedElements;
    }
    
    private EPerson getOrCreateReviewersUser(Context context, String handle){    	
    	EPerson reviewer = null;
    	handle = handle.replace("/", "_"); 
    	try{
    		String username = "Reviewer_" + handle;    	    		
    		reviewer = EPerson.findByUsername(context, username);
        	if(reviewer == null){
            	context.turnOffAuthorisationSystem();
            	reviewer = EPerson.create(context);            	
            	reviewer.setNetid("reviewer_" + handle +"@iochem-bd.org");            	
            	reviewer.setCanLogIn(false);
            	reviewer.setEmail("reviewer_" + handle +"@iochem-bd.org");
            	reviewer.setFirstName("Reviewer");
            	reviewer.setLastName("Collection " + handle);
            	reviewer.setUsername(username);
            	reviewer.update();
            	context.commit();
        	}
    	}catch(Exception e){
    		log.error("Error creating Reviewer user on servlet." + e.getMessage());
    	}finally{
    		if(context.ignoreAuthorization())
    			context.restoreAuthSystemState();
    	}
    	return reviewer;
    }
    
    private Group getOrCreateReviewersGroup(Context context){
    	Group reviewGroup = null;
    	try{
    		reviewGroup = Group.findByName(context, "Reviewers");
    		if(reviewGroup == null){
    			context.turnOffAuthorisationSystem();
    			reviewGroup = Group.create(context);
    			reviewGroup.setName("Reviewers");
    			reviewGroup.update();
    			context.commit();
    		}    		
    	}catch(Exception e){
    		log.error("Error creating Reviewers group on servlet." + e.getMessage());
    	}finally{
    		if(context.ignoreAuthorization())
    			context.restoreAuthSystemState();
    	}
    	return reviewGroup;    	
    }
    
    private void associateReviewerToGroup(EPerson reviewer, Group reviewersGroup){
    	try{
    		reviewersGroup.addMember(reviewer);
        	reviewersGroup.update();
    	}catch(Exception e){
    		log.error("Error asigning reviewer user to group on servlet." + e.getMessage());
    	}    	
    }
    
    
    private void createAccessPolicies(Context context, Collection collection, EPerson reviewer, Group reviewersGroup) throws AuthorizeException{    	
    	
    	try{
        	ItemIterator iterator = collection.getItems();
        	while(iterator.hasNext()){
        		Item item = iterator.next();    		
        		List<ResourcePolicy> policies = AuthorizeManager.getPoliciesActionFilter(context, item, 0);
        		ResourcePolicy readAnonymousPolicy = null;
        		ResourcePolicy readReviewersPolicy = null;
        		for(ResourcePolicy policy : policies)
        			if(policy.getGroupID() == Group.ANONYMOUS_ID && isDateValid(policy))        				
        				readAnonymousPolicy = policy;        			        				
        			else if(policy.getEPersonID() == reviewer.getID())
        				readReviewersPolicy = policy;	
        		if(readAnonymousPolicy == null)			// Not embargoed item 
        			continue;    	
        		if(readReviewersPolicy == null){
        			ResourcePolicy  rp = AuthorizeManager.createOrModifyPolicy(null, context, "Item review", reviewersGroup.getID() , reviewer, null, Constants.READ, "Reviewer access to item :" + item.getHandle() + " on collection " + collection.getHandle(), item);
        			rp.update();
        			for(Bundle bundle: item.getBundles()){    		
        	            if (!bundle.getName().matches(CustomEmbargoSetter.EXCLUDED_BUNDLES_FROM_EMBARGO_REGEX)){    	
        		    		for(Bitstream bitstream : bundle.getBitstreams()){        		    			
        		    			ResourcePolicy rpBitsreams = AuthorizeManager.createOrModifyPolicy(null, context, "Item review", reviewersGroup.getID(), reviewer, null, Constants.READ,  "Reviewer access to bitstream of item :" + item.getHandle() + " on collection " + collection.getHandle(), bitstream);
        		    			rpBitsreams.update();
        		    		}
        	            }
        	    	}
        			
        		}                	
        	} 
        	context.commit();
    	}catch(Exception e){    		
    		log.error("Error during setupAccessPolicy to review item on collection with handle" + collection.getHandle());     		
    		throw new AuthorizeException("Not authorized request for collection review.");
    	}
    }

    private boolean isDateValid(ResourcePolicy policy){
    	Date startDate = policy.getStartDate();
    	if(startDate == null)
    		return false;
    	if(startDate.after(new Date()))
    		return true;
    	else
    		return false;
    }
    
    private String getBrowseBaseUrl(HttpServletRequest request){
    	StringBuilder sb = new StringBuilder();
    	sb.append(request.getScheme());
    	sb.append("://");
    	sb.append(request.getServerName());
    	if(request.getServerPort() != 443 && request.getServerPort() != 80)
    		sb.append( ":" + request.getServerPort());
    	sb.append(request.getContextPath());
    	return sb.toString();
    }
}

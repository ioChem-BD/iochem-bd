/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dspace.app.util.TokenManager;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.authorize.ResourcePolicy;
import org.dspace.content.Collection;
import org.dspace.content.Community;
import org.dspace.content.DCDate;
import org.dspace.content.DSpaceObject;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.Metadatum;
import org.dspace.content.Site;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.embargo.EmbargoManager;
import org.dspace.eperson.EPerson;
import org.dspace.eperson.Group;
import org.dspace.event.Event;
import org.dspace.handle.HandleManager;
import org.dspace.sync.SyncEventConsumer;
import org.dspace.twitter.TwitterEventConsumer;

public class CollectionEditServlet extends DSpaceServlet{

	private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(CollectionEditServlet.class);
	
	private static final SyncEventConsumer eventConsumer = new SyncEventConsumer();
	private static final TwitterEventConsumer teventConsumer = new TwitterEventConsumer();
	
	private static String paperTitle = "Original title: ";
	private static String paperDoi = "DOI: ";
	private static String paperJournal = "Journal: ";
	
	public final static String REVIEW_COLLECTION_ENDPOINT = "/review-collection";
	public final static String REVIEW_COLLECTION_POSTFIX = "#review-collection";
	public final static String TITLE_REGEX = "(" + paperTitle +  "|" + paperDoi +".*|" + paperJournal +".*)";	
	public final static String DOI_REGEX = "(" + paperTitle +"((?!" + paperDoi + ").)*|" + paperDoi + "|"+ paperJournal + ".*)";	
	public final static String JOURNAL_REGEX = "(" + paperTitle +"((?!" + paperJournal +").)*|"+ paperDoi +"((?!" + paperJournal +").)*|" + paperJournal +")";
	
	private static final String DOI_URI_PREFIX = "http://dx.doi.org/";

	private static Pattern pathPattern = Pattern.compile("\\/.*\\/.*\\/[0-9a-fA-F]{24}");

    static{
    	try {    		
			eventConsumer.initialize();
			teventConsumer.initialize();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
    }
	

	
	
    protected void doDSGet(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {    	
    	String handle = validateAccess(request);        
        DSpaceObject dso = HandleManager.resolveToObject(context, handle);    	
    	Collection collection = (Collection) dso;
    	request.setAttribute("review.link", getReviewPath(request, handle));    	
    	request.setAttribute("eperson", context.getCurrentUser());
    	request.setAttribute("dspace.collection", collection);
        setEmbargoedItemsCount(request, collection);
		setPaperMetadata(request, collection);    	
        
        Community[] parents = collection.getCommunities();
        request.setAttribute("dspace.community", parents[0]);
        
        request.setAttribute("dspace.communities", getParents(parents[0],true));
        // Forward to collection home page
        request.setAttribute("collection", collection);        
        request.setAttribute("community", parents[0]);
        request.setAttribute("referral.url", request.getRequestURL().toString());
        JSPManager.showJSP(request, response, "/collection-edit.jsp");    	
    }	
	
    
    
    private String validateAccess(HttpServletRequest request) throws AuthorizeException{
        String path = request.getPathInfo();
        String handle = null;
        String key = null;
        
        if (path != null && pathPattern.matcher(path).matches())
        {
        	// substring(1) is to remove initial '/'
            path = path.substring(1);
            try
            {
                // Extract the Handle
                int firstSlash = path.indexOf('/');
                int secondSlash = path.indexOf('/', firstSlash + 1);
                // We have extra path info
                handle = path.substring(0, secondSlash);
                key = path.substring(secondSlash + 1);
            }
            catch (NumberFormatException nfe)
            {
            	throw new AuthorizeException("Malformed URL request.");
            }
        } else{
        	throw new AuthorizeException("Malformed URL request.");
        }
        
        //Check request validity
        String salt = ConfigurationManager.getProperty("module.communication.secret");
        String secret = getBrowseBaseUrl(request) + "#" + handle + "#" + "edit-collection";
        String calculatedKey = TokenManager.encode(salt, secret);
        calculatedKey = calculatedKey.substring(calculatedKey.length() -24 ,calculatedKey.length()); //Keep last part, enough characters to be safe 8x10^28 combinations
        if (!key.equals(calculatedKey))
        {
        	throw new AuthorizeException("Not authorized request for collection edit.");
        }     
        return handle;
    }
   
    protected void doDSPost(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {
    	String 	handle = validateAccess(request);
    	DSpaceObject dso = HandleManager.resolveToObject(context, handle);    	
    	Collection collection = (Collection) dso;
    	
    	String action = request.getParameter("action");
    	
    	switch(action){
    		case "setProperties" :	setProperties(context, collection, request, response);
    								break;
    		case "liftEmbargo"	 :	liftEmbargo(context, collection, request, response);
    								break;
    	}
    }
    
    private void setProperties(Context context, Collection collection, HttpServletRequest request, HttpServletResponse reponse) throws AuthorizeException{
    	String itemTitle = request.getParameter("item.title");
    	String itemDescription = request.getParameter("item.description");
    	String itemIntro = request.getParameter("item.intro");
    	String paperTitle = request.getParameter("paper.title");
    	String paperJournal = request.getParameter("paper.journal");
    	String paperDoi = request.getParameter("paper.doi");
    	
    	collection.setMetadata("name", itemTitle);
    	collection.setMetadata("short_description", itemDescription);
    	collection.setMetadata("introductory_text", itemIntro);
    	
    	if(!paperTitle.equals(""))
    		paperTitle = "Original title: " + paperTitle;
    	if(!paperDoi.equals(""))
    		paperDoi = "DOI: " + paperDoi;
    	if(!paperJournal.equals(""))
    		paperJournal = "Journal: " + paperJournal;
    	
    	String value = (paperTitle + "   " + paperDoi + "   " + paperJournal).trim();    	
    	try{
    		collection.update();
    		ItemIterator iterator = collection.getItems();
        	while(iterator.hasNext()){
        		Item item = iterator.next();
        		item.clearMetadata("dc", "relation", null,  Item.ANY);
        		item.clearMetadata("dc", "relation", "uri",  Item.ANY);
        		
        		if(!value.equals(""))
        			item.addMetadata("dc","relation", null, Item.ANY, value);
        		if(!paperDoi.equals(""))
        			item.addMetadata("dc","relation", "uri", Item.ANY, DOI_URI_PREFIX + request.getParameter("paper.doi"));
        		item.updateMetadata();        		        		        	
        	}        	
        	context.commit();
        	Event updateMetadataEvent = new Event(Event.MODIFY_METADATA, Constants.SITE, Site.SITE_ID, Constants.COLLECTION, collection.getID(), value, collection.getIdentifiers(context));
        	teventConsumer.consume(context, updateMetadataEvent);
        	eventConsumer.consume(context, updateMetadataEvent);
    	}catch(Exception e){    		           
    		throw new AuthorizeException("Exception raised while updating item metadata.");
    	}finally{
    		
    	}
    	
    }
    
    private void liftEmbargo(Context context, Collection collection, HttpServletRequest request, HttpServletResponse reponse) throws AuthorizeException{
    	try{
    		ItemIterator iterator = collection.getItems();
    		EPerson reviewer = null;
        	while(iterator.hasNext()){
        		Item item = iterator.next();        		
        		if(EmbargoManager.getEmbargoTermsAsDate(context, item) != null){		//Check for embargoed items, then lift embargo
        			if(reviewer == null)
        				reviewer = getReviewerUser(context, item);
        			EmbargoManager.liftEmbargo(context, item);        			        		
        		}
        	}        	
        	if(reviewer != null)
        		deleteReviewer(context, reviewer);
        	
        	Event liftEmbargoEvent = new Event(Event.ADD, Constants.SITE, Site.SITE_ID, Constants.ITEM, collection.getID(), null , collection.getIdentifiers(context));
        	teventConsumer.consume(context, liftEmbargoEvent);
        	eventConsumer.consume(context, liftEmbargoEvent);        	
    	}catch(Exception e){
    		throw new AuthorizeException("Exception raised while updating item metadata.");
    	}
    }    
    
    private EPerson getReviewerUser(Context context, Item item){    	    	
    	EPerson reviewer = null;     	
		try {
			Group group = Group.findByName(context, "Reviewers");
			if(group == null)
				return null;
			List<ResourcePolicy>  policies = AuthorizeManager.getPoliciesActionFilter(context, item, 0);
			for(ResourcePolicy policy: policies){
				if(policy.getGroupID() == group.getID()){
					reviewer = policy.getEPerson();
					break;
				}
			}			
		} catch (SQLException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return reviewer;
    }
    
    private void deleteReviewer(Context context, EPerson reviewer){    	
    	try {    	
    		context.turnOffAuthorisationSystem();
			reviewer.delete();
			context.commit();
		} catch (Exception e){
			log.error(e.getMessage());			
		}finally{
    		if(context.ignoreAuthorization())
    			context.restoreAuthSystemState();
    	}
    }
    
    private void setEmbargoedItemsCount(HttpServletRequest request, Collection collection) throws AuthorizeException{
    	String lift = ConfigurationManager.getProperty("embargo.field.lift");		
        String lift_schema = getSchemaOf(lift);
        String lift_element = getElementOf(lift);
        String lift_qualifier = getQualifierOf(lift);        
        request.setAttribute("embargoed.items", countEmbargoedItems(collection, lift_schema, lift_element, lift_qualifier));        
    }
    
    
    private void setPaperMetadata(HttpServletRequest request, Collection collection){
        String paperMetadata = getPaperMetadata(collection);       
    	String title = paperMetadata.replaceAll(TITLE_REGEX,"").trim();
    	String doi = paperMetadata.replaceAll(DOI_REGEX,"").trim();
    	String journal = paperMetadata.replaceAll(JOURNAL_REGEX, "").trim();
    	request.setAttribute("paper.title", title);
    	request.setAttribute("paper.doi", doi);
    	request.setAttribute("paper.journal", journal);
    }

	private int countEmbargoedItems(Collection collection, String lift_schema, String lift_element, String lift_qualifier) throws AuthorizeException{
		DCDate liftDate = null;;
		Date now = new Date();        
        int embargoedItems = 0;
        try{
        	ItemIterator iterator = collection.getAllItems();
            while(iterator.hasNext()){
            	Item item = iterator.next();
            	liftDate = recoverEmbargoDate(item, lift_schema, lift_element, lift_qualifier);
            	if(liftDate != null && liftDate.toDate().after(now))
            		embargoedItems++;            	            
            }
        }catch(SQLException e){
        	e.printStackTrace();
        	throw new AuthorizeException("Invalid request for collection edit, bad item information.");
        }
        return embargoedItems;		
	}
	
	private String getPaperMetadata(Collection collection){
		//User can publish items in different stages and dates, so paper metadata can subtly vary  (letter case, etc) we will use most repeated one
		HashMap<String, Integer>paperMetadataCount = new HashMap<String,Integer>();
		try {
			ItemIterator itemIter = collection.getAllItems();
			while(itemIter.hasNext()){
				Item item = itemIter.next();
				List<Metadatum> relations = item.getMetadata("dc","relation", null, Item.ANY, Item.ANY);
				Iterator<Metadatum> relationIter = relations.iterator();
				while(relationIter.hasNext()){
					Metadatum relation = relationIter.next();
					String value = relation.value.trim();
					if(value == null || value.equals(""))
						continue;
					if(paperMetadataCount.containsKey(value))
						paperMetadataCount.put(value, paperMetadataCount.get(value)+1);
					else
						paperMetadataCount.put(value, 1);
				}				
			}
			
			if(paperMetadataCount.size() == 0)
				return "";
			return sortByValues(paperMetadataCount).keySet().iterator().next();
		} catch (SQLException e) {		
		    e.printStackTrace();
		    return "";
		}
	}	
	
    private DCDate recoverEmbargoDate(DSpaceObject object, String lift_schema, String lift_element, String lift_qualifier) {
        DCDate liftDate = null;
        Metadatum lift[] = object.getMetadata(lift_schema, lift_element, lift_qualifier, Item.ANY);
        if (lift.length > 0)        
            liftDate = new DCDate(lift[0].value);        
        return liftDate;
    }
 
    // return the schema part of "schema.element.qualifier" metadata field spec
    private String getSchemaOf(String field)
    {
        String sa[] = field.split("\\.", 3);
        return sa[0];
    }

    // return the element part of "schema.element.qualifier" metadata field spec, if any
    private String getElementOf(String field)
    {
        String sa[] = field.split("\\.", 3);
        return sa.length > 1 ? sa[1] : null;
    }

    // return the qualifier part of "schema.element.qualifier" metadata field spec, if any
    private String getQualifierOf(String field)
    {
        String sa[] = field.split("\\.", 3);
        return sa.length > 2 ? sa[2] : null;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static LinkedHashMap<String, Integer> sortByValues(HashMap<String, Integer> map) {    	
		List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
             public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                   .compareTo(((Map.Entry) (o2)).getValue());
             }
        });     
        LinkedHashMap<String, Integer> sortedHashMap = new LinkedHashMap<String, Integer>();
        for (Iterator it = list.iterator(); it.hasNext();) {
               Map.Entry<String,Integer> entry = (Map.Entry<String,Integer>) it.next();
               sortedHashMap.put(entry.getKey(), entry.getValue());
        } 
        return sortedHashMap;
   }


    /**
     * Utility method to produce a list of parent communities for a given
     * community, ending with the passed community, if include is true. If
     * commmunity is top-level, the array will be empty, or contain only the
     * passed community, if include is true. The array is ordered highest level
     * to lowest
     */
    private Community[] getParents(Community c, boolean include)
            throws SQLException
    {
        // Find all the "parent" communities for the community
        Community[] parents = c.getAllParents();

        // put into an array in reverse order
        int revLength = include ? (parents.length + 1) : parents.length;
        Community[] reversedParents = new Community[revLength];
        int index = parents.length - 1;

        for (int i = 0; i < parents.length; i++)
        {
            reversedParents[i] = parents[index - i];
        }

        if (include)
        {
            reversedParents[revLength - 1] = c;
        }

        return reversedParents;
    }
	
    private String getReviewPath(HttpServletRequest request, String handle){    	    	
    	String salt = ConfigurationManager.getProperty("module.communication.secret"); 
    	String secret = getBrowseBaseUrl(request) + "#" + handle + REVIEW_COLLECTION_POSTFIX;
    	
        String reviewKey = TokenManager.encode(salt, secret);
        reviewKey = reviewKey.substring(reviewKey.length() -24 ,reviewKey.length()); //Keep last part, enough characters to be safe 8x10^28 combinations
    	
    	StringBuilder sb = new StringBuilder();
    	sb.append(getBrowseBaseUrl(request));
    	sb.append(REVIEW_COLLECTION_ENDPOINT);
    	sb.append("/" + handle);
    	sb.append("/" + reviewKey);
    	return sb.toString();    	
    }
    
    private String getBrowseBaseUrl(HttpServletRequest request){
    	StringBuilder sb = new StringBuilder();
    	sb.append(request.getScheme());
    	sb.append("://");
    	sb.append(request.getServerName());
    	if(request.getServerPort() != 443 && request.getServerPort() != 80)
    		sb.append( ":" + request.getServerPort());
    	sb.append(request.getContextPath());
    	return sb.toString();
    }

}

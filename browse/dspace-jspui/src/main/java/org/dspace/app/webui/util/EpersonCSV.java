/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.util;

import java.sql.SQLException;
import java.text.Normalizer;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Community;
import org.dspace.core.Context;
import org.dspace.eperson.EPerson;
import org.dspace.eperson.Group;

public class EpersonCSV {
	public static final Logger log = LogManager.getLogger(EpersonCSV.class.getName());
	
	public static Group createGroup(Context context, String email, String groupname) throws Exception{		
		Group group = Group.findByName(context, groupname);
		if(group == null){
			group = Group.create(context);
			group.setName(groupname);
			group.update();			
		}
		return group;		
	}
	
	public static String buildUsername(Context context, String firstName, String lastName) throws Exception{
		String lastNameWord  = "";
		String firstLetter = normalizeString(String.valueOf(firstName.charAt(0)).toLowerCase());		
		lastName = normalizeString(lastName);
		String[] lastNameWords = lastName.split("[\\s\\.]+");
		if(lastNameWords.length > 1 && lastNameWords[0].length() < 3 && lastNameWords[1].length() > 3)
			lastNameWord = lastNameWords[1].replaceAll("[^A-Za-z0-9]", "").toLowerCase();
		else
			lastNameWord = lastNameWords[0].replaceAll("[^A-Za-z0-9]", "").toLowerCase();
		String username = firstLetter.concat(lastNameWord);
		if(!existsUsername(context, username))
			return username;
		for(int inx = 2; inx < 1000; inx++){
			if(!existsUsername(context, username + inx))
				return username + inx;
		}
		throw new Exception("Can't generate username");
	}

	public static void createCommunityFromUserGroup(Context context, Group group) throws SQLException, AuthorizeException{
		Community[] communities = Community.findAll(context);		
		boolean existsGroupCommunity = false; 
		for(Community community : communities){
			if(community.getName().equals(group.getName())){
				existsGroupCommunity = true;
				addGroupUsersToCommunity(community, group);
			}
		}		
		if(!existsGroupCommunity){
			Community community = createCommunityFromGroup(context, group);
			addGroupUsersToCommunity(community, group);			
		}
	}

	private static void addGroupUsersToCommunity(Community community, Group group){
		try {
			Group admins = community.getAdministrators();
			if(admins == null){						
				admins = community.createAdministrators();
				admins.setName("COMMUNITY_" + group.getID() + "_ADMIN");
		        community.update();		        		   			
			}
			for(EPerson eperson : group.getMembers())
	        	admins.addMember(eperson);
			
			admins.update();			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	private static Community createCommunityFromGroup(Context context, Group group) throws SQLException, AuthorizeException{
		Community community = Community.create(null, context);
		community.setMetadata("name", group.getName());
		community.update();
		return community;
	}	
	
	private static String normalizeString(String value){
		String nfdNormalizedString = Normalizer.normalize(value, Normalizer.Form.NFD); 
	    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
	    return pattern.matcher(nfdNormalizedString).replaceAll("");
	}
	
	private static boolean existsUsername(Context context, String username) throws SQLException{
		return EPerson.findByUsername(context, username) != null;		
	}

}

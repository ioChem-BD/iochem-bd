/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 *
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */
package org.dspace.app.webui.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.dspace.app.util.TokenManager;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.app.webui.util.UIUtil;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.DSpaceObject;
import org.dspace.content.Item;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.LogManager;
import org.dspace.core.Utils;
import org.dspace.event.Event;
import org.dspace.handle.HandleManager;
import org.dspace.sync.SyncEventConsumer;
import org.dspace.usage.UsageEvent;
import org.dspace.utils.DSpace;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Servlet for retrieving bitstreams. The bits are simply piped to the user. If
 * there is an <code>If-Modified-Since</code> header, only a 304 status code
 * is returned if the containing item has not been modified since that date.
 * <P>
 * <code>/bitstream/handle/sequence_id/filename</code>
 * 
 * @author Robert Tansley
 * @version $Revision$
 */
public class BitstreamServlet extends DSpaceServlet
{
    /** log4j category */
    private static Logger log = Logger.getLogger(BitstreamServlet.class);

    /**
     * Threshold on Bitstream size before content-disposition will be set.
     */
    private int threshold;

    private static final SyncEventConsumer eventConsumer = new SyncEventConsumer();
    private static String communicationSecret;
    
    /**
     * Threshold on Bitstream size that is stored in tape format, needs to be recovered first prior download.
     */
    private long deferred;

    /**
     * Is deferred service under maintenance, it true can't retrieve large files 
     */
    private boolean maintenance;    
    
    /**
     * Verify url for reCAPTCHA v3 service 
     */
    private String recaptchaVerifyUrl;
    
    /**
     * Site key used to configure recaptcha functionality, if enabled
     */
    private String recaptchaKey;

    /**
     * Site secret key used to verify recaptcha user generated tokens, if recaptcha is enabled
     */
    private String recaptchaSecretKey;    
    
    static {
        try {           
            eventConsumer.initialize();
            communicationSecret = ConfigurationManager.getProperty("external.communication.secret");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
    
    @Override
	public void init(ServletConfig arg0) throws ServletException {

		super.init(arg0);
		threshold = ConfigurationManager
				.getIntProperty("webui.content_disposition_threshold");
		deferred = ConfigurationManager
                .getLongProperty("webui.deferred_content_disposition_threshold", -1);
		maintenance = ConfigurationManager
		        .getBooleanProperty("webui.deferred_under_maintenance", false);
		recaptchaKey = ConfigurationManager
                .getProperty("webui.deferred_recaptchav3_site_key");		
		recaptchaSecretKey = ConfigurationManager
                .getProperty("webui.deferred_recaptchav3_secret_key");		
		recaptchaVerifyUrl = ConfigurationManager
                .getProperty("webui.deferred_recaptchav3_verify_url");		
	}

    @Override
	protected void doDSGet(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {
    	Item item = null;
    	Bitstream bitstream = null;

        // Get the ID from the URL
        String idString = request.getPathInfo();
        String handle = "";
        String sequenceText = "";
        String filename = null;
        int sequenceID;

        if (idString == null)
        {
            idString = "";
        }
        
        // Parse 'handle' and 'sequence' (bitstream seq. number) out
        // of remaining URL path, which is typically of the format:
        // {handle}/{sequence}/{bitstream-name}
        // But since the bitstream name MAY have any number of "/"s in
        // it, and the handle is guaranteed to have one slash, we
        // scan from the start to pick out handle and sequence:

        // Remove leading slash if any:
        if (idString.startsWith("/"))
        {
            idString = idString.substring(1);
        }

        // skip first slash within handle
        int slashIndex = idString.indexOf('/');
        if (slashIndex != -1)
        {
            slashIndex = idString.indexOf('/', slashIndex + 1);
            if (slashIndex != -1)
            {
                handle = idString.substring(0, slashIndex);
                int slash2 = idString.indexOf('/', slashIndex + 1);
                if (slash2 != -1)
                {
                    sequenceText = idString.substring(slashIndex+1,slash2);
                    filename = idString.substring(slash2+1);
                }
            }
        }

        try
        {
            sequenceID = Integer.parseInt(sequenceText);
        }
        catch (NumberFormatException nfe)
        {
            sequenceID = -1;
        }
        
        // Now try and retrieve the item
        DSpaceObject dso = HandleManager.resolveToObject(context, handle);
        
        // Make sure we have valid item and sequence number
        if (dso != null && dso.getType() == Constants.ITEM && sequenceID >= 0)
        {
            item = (Item) dso;
        
            if (item.isWithdrawn())
            {
                log.info(LogManager.getHeader(context, "view_bitstream",
                        "handle=" + handle + ",withdrawn=true"));
                JSPManager.showJSP(request, response, "/tombstone.jsp");
                return;
            }

            AuthorizeManager.checkItemIsNotEmbargoed(context, item);
            
            boolean found = false;

            Bundle[] bundles = item.getBundles();

            for (int i = 0; (i < bundles.length) && !found; i++)
            {
                Bitstream[] bitstreams = bundles[i].getBitstreams();

                for (int k = 0; (k < bitstreams.length) && !found; k++)
                {
                    if (sequenceID == bitstreams[k].getSequenceID())
                    {
                        bitstream = bitstreams[k];
                        found = true;
                    }
                }
            }
        }

        if (bitstream == null || filename == null
                || !filename.equals(bitstream.getName()))
        {
            // No bitstream found or filename was wrong -- ID invalid
            log.info(LogManager.getHeader(context, "invalid_id", "path="
                    + idString));
            JSPManager.showInvalidIDError(request, response, idString,
                    Constants.BITSTREAM);

            return;
        }
        
        if(deferred != -1 && bitstream.getSize() >= deferred) 
        {               
            if(maintenance) {
                JSPManager.showJSP(request, response, "/error/maintenance.jsp"); 
                return;
            }else if(!isValidFileRetrievalRequest(request, bitstream.getID())) {
                request.setAttribute("status", "display_form");
                request.setAttribute("file.size.gb", deferred / 1048576.0);
                request.setAttribute("item.name", dso.getName());
                request.setAttribute("item.handle", dso.getHandle());
                request.setAttribute("bitstream.name", bitstream.getName());
                request.setAttribute("bitstream.size", Math.round(bitstream.getSize() / 1048576.0));
                request.setAttribute("bitstream.id", bitstream.getID());
                request.setAttribute("bitstream.url", request.getRequestURL());
                request.setAttribute("recaptcha.site.key", recaptchaKey);                
                JSPManager.showJSP(request, response, "/deferred.jsp");
                return;  
            }          
        }

        log.info(LogManager.getHeader(context, "view_bitstream",
                "bitstream_id=" + bitstream.getID()));
        
        //new UsageEvent().fire(request, context, AbstractUsageEvent.VIEW,
		//		Constants.BITSTREAM, bitstream.getID());

        new DSpace().getEventService().fireEvent(
        		new UsageEvent(
        				UsageEvent.Action.VIEW, 
        				request, 
        				context, 
        				bitstream));
        
        // Modification date
        // Only use last-modified if this is an anonymous access
        // - caching content that may be generated under authorisation
        //   is a security problem
        if (context.getCurrentUser() == null)
        {
            // TODO: Currently the date of the item, since we don't have dates
            // for files
            response.setDateHeader("Last-Modified", item.getLastModified()
                    .getTime());

            // Check for if-modified-since header
            long modSince = request.getDateHeader("If-Modified-Since");

            if (modSince != -1 && item.getLastModified().getTime() < modSince)
            {
                // Item has not been modified since requested date,
                // hence bitstream has not; return 304
                response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                return;
            }
        }
        
        // Pipe the bits
        InputStream is = bitstream.retrieve();
     
		// Set the response MIME type
        response.setContentType(bitstream.getFormat().getMIMEType());

        response.setContentType("application/force-download");
        // Response length
        response.setHeader("Content-Length", String
                .valueOf(bitstream.getSize()));

		if(threshold != -1 && bitstream.getSize() >= threshold)
		{
			UIUtil.setBitstreamDisposition(bitstream.getName(), request, response);
		}

        //DO NOT REMOVE IT - WE NEED TO FREE DB CONNECTION TO AVOID CONNECTION POOL EXHAUSTION FOR BIG FILES AND SLOW DOWNLOADS
        context.complete();

        Utils.bufferedCopy(is, response.getOutputStream());
        is.close();
        response.getOutputStream().flush();
    }

    
    private boolean isValidFileRetrievalRequest(HttpServletRequest request, int id) {
        try {
            Long requestMillis = Long.valueOf(request.getParameter("date"));
            String requestHash = request.getParameter("hash");
            if(requestMillis == null || requestHash == null)
                return false;
            String calculatedHash = TokenManager.encode(requestMillis + "#" + String.valueOf(id) , communicationSecret).substring(0,10);
            if(!requestHash.equals(calculatedHash))
                return false;                                   
            Long currentMillis = System.currentTimeMillis();
            if(currentMillis < requestMillis || currentMillis > requestMillis + (24*60*60*1000)  ) //If request is more than 24h old, file is deferred again, need to request it again. 
                return false;                        
        }catch(Exception e) {
            log.error(e.getMessage());
            return false;
        }        
        return true;
    }
    
    protected void doDSPost(Context c,
            HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, AuthorizeException
    {        
        if(isValidUserRequest(request)) {
            try {
                String email = request.getParameter("email");            
                int bitstreamId = Integer.valueOf(request.getParameter("bitstream"));
                String[] identifiers = {request.getParameter("bitstream"), request.getParameter("url")};
                eventConsumer.consume(c, new Event(Event.RETRIEVE, Constants.BITSTREAM, bitstreamId, email, identifiers));
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            request.setAttribute("status", "success");         
            JSPManager.showJSP(request, response, "/deferred.jsp");
            return;
        } else {
            request.setAttribute("status", "error");           
            JSPManager.showJSP(request, response, "/deferred.jsp");
            return;
        }
    }

    private boolean isValidUserRequest(HttpServletRequest request)  {
        if(recaptchaVerifyUrl == null || recaptchaVerifyUrl.isEmpty()) 
            return false;   
     
        try(CloseableHttpClient client = HttpClientBuilder.create().build()) {
            HttpPost post = new HttpPost(recaptchaVerifyUrl);            
            List <NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("secret", recaptchaSecretKey));
            parameters.add(new BasicNameValuePair("response", request.getParameter("g-recaptcha-response")));
            post.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
            
            CloseableHttpResponse response = client.execute(post);
            int code = response.getStatusLine().getStatusCode();
            if (code == HttpStatus.SC_OK) {
                String json = EntityUtils.toString(response.getEntity());
                Map<String, Object> userData = new Gson().fromJson(json, new TypeToken<HashMap<String, Object>>() {}.getType());  
                return (Boolean)userData.get("success") && (Double)userData.get("score") >= 0.5;  // Threshold taken from reCAPTCHA documentation
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return false;
    }


}

/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 *
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */
package org.dspace.app.webui.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dspace.app.webui.util.Authenticate;
import org.dspace.authorize.AuthorizeException;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Context;
import org.dspace.core.LogManager;
import org.dspace.core.Constants;

/**
 * Servlet that logs out any current user if invoked.
 * 
 * @author Robert Tansley
 * @version $Revision$
 */
public class LogoutServlet extends DSpaceServlet
{
    /** log4j logger */
    private static Logger log = Logger.getLogger(LogoutServlet.class);

    protected void doDSGet(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {            
        log.info(LogManager.getHeader(context, "logout", ""));        
        Authenticate.loggedOut(context, request);

    	StringBuffer location=new StringBuffer();
    	String type = request.getParameter("type");

    	if(type == null || !type.equals("web")) {
    	location.append(ConfigurationManager.getProperty("authentication-cas","cas.logout.url"));
    	location.append("?service=" + request.getScheme()  +"://");
    	location.append(request.getServerName());
    	location.append(":");
    	location.append(request.getServerPort());
    	location.append(request.getContextPath());        	
    	}else {
    		location.append(ConfigurationManager.getProperty("create.url").replaceAll("/zul/main.zul", Constants.LOGOUT_CREATE_ENDPOINT));
    	}
    	response.sendRedirect(location.toString());        
    }
}

/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.servlet;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Item;
import org.dspace.content.Metadatum;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.LogManager;
import org.dspace.handle.HandleManager;
import org.dspace.usage.UsageEvent;
import org.dspace.utils.DSpace;

public class CML2HTMLServlet extends DSpaceServlet
{
	private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(CML2HTMLServlet.class);

    /**
     * Default maximum number of path elements to strip when testing if a
     * bitstream called "foo.html" should be served when "xxx/yyy/zzz/foo.html"
     * is requested.
     */
    private int maxDepthGuess;
    private static final String JCAMPDX_URL = "/cml2jcamp?id=";
    private static final String XYZ_URL     = "/cml2xyz?id=";
    private static final String MOLDEN_URL     = "/molden/";
    private static TransformerFactory tFactory 			= null; 
	private static HashMap<String,Templates> templates  = null;
	
	static {
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
		templates = new HashMap<String,Templates>();
	}
    
    
    /**
     * Create an CML2HTML Servlet
     */
    public CML2HTMLServlet()
    {
        super();

        if (ConfigurationManager.getProperty("webui.html.max-depth-guess") != null)
        {
            maxDepthGuess = ConfigurationManager
                    .getIntProperty("webui.html.max-depth-guess");
        }
        else
        {
            maxDepthGuess = 3;
        }
    }
    
    // Return bitstream whose name matches the target bitstream-name
    // bsName, or null if there is no match.  Match must be exact.
    // NOTE: This does not detect duplicate bitstream names, just returns first.
    private static Bitstream getItemBitstreamByName(Item item, String bsName)
                            throws SQLException
    {
        Bundle[] bundles = item.getBundles();

        for (int i = 0; i < bundles.length; i++)
        {
            Bitstream[] bitstreams = bundles[i].getBitstreams();

            for (int k = 0; k < bitstreams.length; k++)
            {
                if (bsName.equals(bitstreams[k].getName()))
                {
                    return bitstreams[k];
                }
            }
        }
        return null;
    }

    // On the surface it doesn't make much sense for this servlet to
    // handle POST requests, but in practice some HTML pages which
    // are actually JSP get called on with a POST, so it's needed.
    protected void doDSPost(Context context, HttpServletRequest request,
                HttpServletResponse response)
        throws ServletException, IOException, SQLException, AuthorizeException
    {
        doDSGet(context, request, response);
    }

    protected void doDSGet(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {
        Item item = null;
        Bitstream bitstream = null;
        String idString = request.getPathInfo();
        String filenameNoPath = null;
        String fullpath = null;
        String handle = null;
        // Parse URL
        if (idString != null)
        {
            // Remove leading slash
            if (idString.startsWith("/"))
            {
                idString = idString.substring(1);
            }

            // Get handle and full file path
            int slashIndex = idString.indexOf('/');
            if (slashIndex != -1)
            {
                slashIndex = idString.indexOf('/', slashIndex + 1);
                if (slashIndex != -1)
                {
                    handle = idString.substring(0, slashIndex);
                    fullpath = URLDecoder.decode(idString
                            .substring(slashIndex + 1),
                            Constants.DEFAULT_ENCODING);

                    // Get filename with no path
                    slashIndex = fullpath.indexOf('/');
                    if (slashIndex != -1)
                    {
                        String[] pathComponents = fullpath.split("/");
                        if (pathComponents.length <= maxDepthGuess + 1)
                        {
                            filenameNoPath = pathComponents[pathComponents.length - 1];
                        }
                    }
                }
            }
        }

        if (handle != null && fullpath != null)
        {
            // Find the item
            try
            {
                /*
                 * If the original item doesn't have a Handle yet (because it's
                 * in the workflow) what we actually have is a fake Handle in
                 * the form: db-id/1234 where 1234 is the database ID of the
                 * item.
                 */
                if (handle.startsWith("db-id"))
                {
                    String dbIDString = handle
                            .substring(handle.indexOf('/') + 1);
                    int dbID = Integer.parseInt(dbIDString);
                    item = Item.find(context, dbID);
                }
                else
                {
                    item = (Item) HandleManager
                            .resolveToObject(context, handle);
                }
            }
            catch (NumberFormatException nfe)
            {
                // Invalid ID - this will be dealt with below
            }
        }

        if (item != null)
        {
        	AuthorizeManager.checkItemIsNotEmbargoed(context, item);
        	
            // Try to find bitstream with exactly matching name + path
            bitstream = getItemBitstreamByName(item, fullpath);
            
            if (bitstream == null && filenameNoPath != null)
            {
                // No match with the full path, but we can try again with
                // only the filename
                bitstream = getItemBitstreamByName(item, filenameNoPath);
            }
                        
            // Did we get a bitstream and has program defined?
            Metadatum[] program = item.getMetadata("cml", "program", "name", Item.ANY);
            if (bitstream != null && program != null)
            {
                log.info(LogManager.getHeader(context, "view_CML2HTML", "handle=" + handle + ",bitstream_id=" + bitstream.getID()));            
                new DSpace().getEventService().fireEvent(new UsageEvent(UsageEvent.Action.VIEW, request, context, bitstream));
                try
                {
                    String programName = program[0].value.toUpperCase();    
                    if(!templates.containsKey(programName)){
                        String xsltPath = getServletContext().getRealPath("/xslt/cml2html" + programName + ".xsl");
                        StreamSource xslSource =  new StreamSource(new File(xsltPath));                 
                        templates.put(programName, tFactory.newTemplates(xslSource));
                    }            
                    Transformer transformer = templates.get(programName).newTransformer();
                    // Set the response MIME type            
                    response.setContentType("text/html");
                    StreamSource xml = new StreamSource(bitstream.retrieve());            
                    StreamResult result = new StreamResult(response.getOutputStream());   
                    Metadatum[] titles    = item.getMetadata("dc","title", Item.ANY, Item.ANY);
                    Metadatum[] authors  = item.getMetadata("dc", "contributor", Item.ANY,  Item.ANY);
                    StringBuilder allTitles  = new StringBuilder(); 
                    StringBuilder allAuthors = new StringBuilder();
                    for(Metadatum title:titles)
                        allTitles.append(title.value + ": ");                
                    for(Metadatum author:authors)
                        allAuthors.append(author.value + ": ");             
                    URL reconstructedURL = new URL(request.getScheme(),request.getServerName(),request.getServerPort(),request.getContextPath());                
                    transformer.setParameter("webrootpath", reconstructedURL.toString());
                    transformer.setParameter("jcampdxurl", reconstructedURL.toString() + JCAMPDX_URL + item.getID());
                    transformer.setParameter("xyzurl", reconstructedURL.toString() + XYZ_URL + item.getID());
                    transformer.setParameter("browseurl", reconstructedURL.toString()  + "/handle/" + handle);
                    if(hasOrbitals(item))
                        transformer.setParameter("moldenurl", reconstructedURL.toString()  + MOLDEN_URL + handle);
                    if(allTitles.length() > 0)
                        transformer.setParameter("title", allTitles.delete(allTitles.length() - 2, allTitles.length()));
                    if(allAuthors.length() > 0)
                        transformer.setParameter("author", allAuthors.delete(allAuthors.length() - 2, allAuthors.length()));
                    transformer.transform(xml, result);                
                    response.setHeader("Content-Length", String.valueOf(bitstream.getSize()));               
                    response.getOutputStream().flush();                
                }
                catch (TransformerConfigurationException e)
                {
                    log.info(LogManager.getHeader(context, "view_CML2HTML","invalid_bitstream_id=" + idString + " error converting cml file to html."));
                    log.error(e.getMessage());
                    JSPManager.showInternalError(request, response);
                }
                catch (TransformerException e)
                {
                    log.info(LogManager.getHeader(context, "view_CML2HTML","invalid_bitstream_id=" + idString + " error converting cml file to html."));
                    log.error(e.getMessage());
                    JSPManager.showInternalError(request, response);               
                }  
            }
            else
            {
                // No bitstream - we got an invalid ID
                log.info(LogManager.getHeader(context, "view_CML2HTML",
                        "invalid_bitstream_id=" + idString));

                JSPManager.showInvalidIDError(request, response, idString,
                        Constants.BITSTREAM);
            }             
        }
    }

	private boolean hasOrbitals(Item item) {
		Bundle[] bunds;
		try {
			bunds = item.getBundles("ORIGINAL");
			if (bunds[0] != null){
				Bitstream[] bits = bunds[0].getBitstreams();
				for (int i = 0; (i < bits.length); i++) 
					if (bits[i].getFormat().getMIMEType().equals("chemical/x-molden"))
	                    return  true;
			}
		} catch (SQLException e) {
			log.error("Error retrieving item bundles for item " + item.getID());
			log.error(e.getMessage());		
		}		
		return false;
	}

}

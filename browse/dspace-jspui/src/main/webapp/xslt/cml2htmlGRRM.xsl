<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:g="http://www.iochem-bd.org/dictionary/gaussian/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk g helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> May 15, 2023</xd:p>
            <xd:p><xd:b>Author:</xd:b>Diego Garay Ruiz</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
        </xd:desc>       
    </xd:doc>
    
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>    
    <xsl:param name="browseurl"/>
    <xsl:param name="xyzurl"/>

    <xsl:variable name="calcType" select="//module[@id='initialization']//parameter[@dictRef='cc:jobtype']/scalar"/>
    
    <!-- Environment module -->
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    <!-- Initializacion module -->
    <xsl:variable name="solvation" select="(//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='solvation'])[1]"/>
    <xsl:variable name="initialMolecule" select="(//module[@dictRef='cc:initialization' and child::molecule])[last()]//molecule"/>
    <xsl:variable name="functional" select="distinct-values(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:method']/scalar)"/>
    <xsl:variable name="parameters" select="//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='parameters']"/>
    <xsl:variable name="symmetry" select="distinct-values(//module[@dictRef='cc:finalization']//scalar[@dictRef='cc:pointgroup'])"/>   

    <!-- Geometry -->
    <xsl:variable name="finalMolecule" select="((//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule)[1]"/>
    <!-- Thermochemistry -->
    <xsl:variable name="thermochemistryProperty" select="//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']"/>
    <xsl:variable name="temperature" select="($thermochemistryProperty[1]//scalar[@dictRef='cc:temp'])[1]"/>
    <xsl:variable name="pressure" select="($thermochemistryProperty[1]//scalar[@dictRef='cc:press'])[1]"/>
    <!-- Finalization module -->
    <xsl:variable name="charge" select="if(exists((//module[@id='finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:charge'])[last()])) then
                                            (//module[@id='finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:charge'])[last()] 
                                        else 
                                            //module[@id='initialization']/parameterList/parameter[@dictRef='cc:charge']/scalar"/>

    <xsl:variable name="multiplicity" select="//module[@id='initialization']/parameterList/parameter[@dictRef='cc:multiplicity']/scalar"/>

    <xsl:template match="/">        
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>

                <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"></script>
                <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>

                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                       
                            <xsl:variable name="molecule" select="
                                if(exists($finalMolecule)) 
                                    then $finalMolecule
                                else
                                    $initialMolecule
                                "/>                                                             
                            <div>
                                <xsl:call-template name="atomicCoordinates">
                                    <xsl:with-param name="molecule"     select="$molecule"/>
                                </xsl:call-template>                                        
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>MOLECULAR INFO</h3>                            
                            <xsl:call-template name="chargemultiplicity"/>
                        </div>
                    </div>
                    <!-- Results -->
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">
                            <xsl:for-each select="//cml:module[@dictRef='cc:job']">                                
                                <xsl:variable name="scfConverged" select="exists(.//module[@id='calculation']//module[@cmlx:templateRef='scf']/scalar[@dictRef='cc:scfConverged'])"/>                                
                                <div id="module-{generate-id(.)}">                                
                                   <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small>
                                       <xsl:if test="exists($scfConverged)">
                                           <small> SCF Converged</small>
                                       </xsl:if>                                               
                                   </h3>
                                   <xsl:call-template name="energySection"/>
                                   <xsl:call-template name="s2"/>
                                   <xsl:call-template name="frequencies"/>
                                   <xsl:call-template name="thermochemistry"/>
                                   <xsl:call-template name="irc"/>
                                   <br/>
                                </div>
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>                            
                        </div>
                    </div> 
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                         
                        $(document).ready(function() {    
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                        
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                            $.unblockUI();                             
                        });
                                               
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                                                
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
            </body>
        </html>
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>                               
            <tr>
                <td>Calculation type:</td>
                <td>
                    <xsl:value-of select="$calcType"/>
                    <xsl:choose>
                        <xsl:when test="exists($solvation)"><xsl:text>  (Solvation)</xsl:text></xsl:when>
                        <xsl:otherwise><xsl:text>  (Phase gas)</xsl:text></xsl:otherwise>
                    </xsl:choose>                                       
                </td>
            </tr>
            <tr>
                <td>Method(s):</td>
                <td>DFT<xsl:if test="exists($functional)">
                    <xsl:text> ( </xsl:text>
                    <xsl:for-each select="$functional">
                        <xsl:value-of select="concat(.,' ')"/>
                    </xsl:for-each>
                    <xsl:text> )</xsl:text>                    
                </xsl:if></td>                
            </tr>
            <xsl:if test="exists($symmetry)">
                <tr>
                    <td>Symmetry : </td>
                    <td>
                        <xsl:for-each select="$symmetry">
                            <xsl:value-of select="."/><xsl:text> </xsl:text>
                        </xsl:for-each>
                    </td>
                </tr>
            </xsl:if>            
            <xsl:if test="exists($thermochemistryProperty)">                                  
                <tr>
                    <td>                                           
                        <xsl:text>Temperature</xsl:text>
                    </td>                                   
                    <td>                                            
                        <xsl:value-of select="$temperature/text()"></xsl:value-of>   
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($temperature/@units)"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <xsl:text>Pressure</xsl:text>
                    </td>
                    <td>
                        <xsl:value-of select="$pressure/text()"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($pressure/@units)"></xsl:value-of>
                    </td>
                </tr>
            </xsl:if>
        </table>        
    </xsl:template>

    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">        
        <xsl:param name="molecule"/>
        <xsl:param name="fragmentFiles"/>   
        <xsl:variable name="collapseAccordion" select="if(count($molecule/cml:atomArray/cml:atom) > 10) then '' else 'in'"/>
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                        Atomic coordinates [&#8491;] 
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <tr>
                                        <td><xsl:value-of select="$title"/></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                        <xsl:variable name="id" select="@id"/>
                                        <tr>
                                            <td><xsl:value-of select="$elementType"/></td>
                                            <td><xsl:value-of select="format-number(@x3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@y3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@z3,'#0.000000')"/></td>
                                        </tr>
                                    </xsl:for-each>                                       
                                </tbody>                            
                            </table>                              
                            <script type="text/javascript">                                                                  
                                $(document).ready(function() {
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                });
                                function getXYZ(){
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons( table, {
                                    buttons: [ {
                                        extend: 'copyHtml5',
                                        text: 'XYZ'
                                        }]
                                    });
                                    table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));                                    
                                    $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                }     
                            </script>  
                            
                            
                            
                            <table id="atomicCoordinates"></table>
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                    $('table#atomicCoordinates').dataTable( {
                                    "aaData": [
                                    /* Reduced data set */
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">
                                        <xsl:variable name="elementType" select="@elementType"/>
                                        <xsl:variable name="id" select="@id"/>                      
                                        [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.0000')"/>","<xsl:value-of select="format-number(@y3,'#0.0000')"/>","<xsl:value-of select="format-number(@z3,'#0.0000')"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                    </xsl:for-each>    
                                    ],
                                    "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" },
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });   
                                });                                       
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>

    <!-- Charge / Multiplicity section -->
    <xsl:template name="chargemultiplicity">
        <div class="row bottom-buffer">
            <div class="col-md-2 col-sm-6">
                <table class="display" id="chargemultiplicity">
                    <tbody>
                        <tr>
                            <td>Charge: </td>
                            <td class="right"><xsl:value-of select="$charge"/></td>                            
                        </tr>
                        <xsl:if test="$multiplicity != ''">
                            <tr>
                                <td>Multiplicity: </td>
                                <td class="right"><xsl:value-of select="$multiplicity"/></td>
                            </tr>
                        </xsl:if>                        
                     
                    </tbody>
                </table>        
            </div>
        </div>
    </xsl:template>
 
    <!-- Thermochemistry -->
    <xsl:template name="thermochemistry">
        <xsl:variable name="thermochemistry" select="./module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermo']"/>        
        <xsl:if test="exists($thermochemistry)">
            <xsl:variable name="total.thermalCorr" select="sum($thermochemistry/scalar[starts-with(@cmlx:templateRef,'thermal-')])"/>
            <xsl:variable name="total.entropy" select="sum($thermochemistry/scalar[starts-with(@cmlx:templateRef,'entropy-')])"/>
            <xsl:variable name="elecEnergy" select="$thermochemistry/scalar[@dictRef='cc:energy']"/>
            <xsl:variable name="total.thermal" select="$total.thermalCorr + $elecEnergy"/>
            <div class="panel panel-default">
                <div class="panel-heading"  data-toggle="collapse" data-target="div#thermochemistry-{generate-id($thermochemistry)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Thermochemistry                           
                    </h4>
                </div>
                <div id="thermochemistry-{generate-id($thermochemistry)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                      <div class="row bottom-buffer">
                          <div class="col-md-12 col-sm-12">     
                              <h3>General</h3>
                              <table id="thermochemistryT1-{generate-id($thermochemistry)}">
                                  <thead>
                                      <tr>
                                          <th>Magnitude</th>
                                          <th>Value</th>                     
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>Electronic energy (<xsl:value-of select="helper:printUnitSymbol($elecEnergy/@units)"></xsl:value-of>) </td>
                                          <td><xsl:value-of select="$elecEnergy"/></td>
                                      </tr>
                                      <tr>
                                          <td>Total thermal energy (<xsl:value-of select="helper:printUnitSymbol($elecEnergy/@units)"></xsl:value-of>)</td>
                                          <td><xsl:value-of select="$total.thermal"/></td>
                                      </tr>
                                      <tr>
                                          <td>Total enthalpy (<xsl:value-of select="helper:printUnitSymbol($elecEnergy/@units)"></xsl:value-of>)</td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:zpe.sumelectthermalent']"/></td>
                                      </tr>     
                                      <tr>
                                          <td>Gibbs free energy (<xsl:value-of select="helper:printUnitSymbol($elecEnergy/@units)"></xsl:value-of>) </td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:zpe.sumelectthermalfe']"/></td>
                                      </tr>
                                  </tbody>              
                              </table>
                              <h3>Components</h3>
                              <table id="thermochemistryT2-{generate-id($thermochemistry)}">
                                  <thead>
                                      <tr>
                                          <th> </th>
                                          <th>Total</th>
                                          <th>Translational</th>      
                                          <th>Rotational</th> 
                                          <th>Vibrational</th> 
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>Thermal (<xsl:value-of select="helper:printUnitSymbol($thermochemistry/scalar[@dictRef='cc:ethermo.trans']/@units)"></xsl:value-of>)</td>
                                          <td><xsl:value-of select="$total.thermalCorr"/></td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:ethermo.trans']"/></td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:ethermo.rot']"/></td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:ethermo.vib']"/></td>
                                      </tr>
                                      <tr>
                                          <td>Entropy (<xsl:value-of select="helper:printUnitSymbol($thermochemistry/scalar[@dictRef='cc:s.trans']/@units)"></xsl:value-of>)</td>
                                          <td><xsl:value-of select="$total.entropy"/></td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:s.trans']"/></td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:s.rot']"/></td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:s.vib']"/></td>
                                      </tr>
                                      <tr>
                                          <td>ZPE (<xsl:value-of select="helper:printUnitSymbol($thermochemistry/scalar[@dictRef='cc:zpe.correction']/@units)"></xsl:value-of>)</td>
                                          <td><xsl:value-of select="$thermochemistry/scalar[@dictRef='cc:zpe.correction']"/></td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                      </tr>

                                  </tbody>              
                              </table>
                              <script type="text/javascript">
                                  $(document).ready(function() {
                                  $('table#thermochemistryT1-<xsl:value-of select="generate-id($thermochemistry)"/>').dataTable({
                                          "bFilter": false,
                                          "bPaginate": false,
                                          "bSort": false,
                                          "bInfo": false
                                      });
                                   $('table#thermochemistryT2-<xsl:value-of select="generate-id($thermochemistry)"/>').dataTable({
                                  "bFilter": false,
                                  "bPaginate": false,
                                  "bSort": false,
                                  "bInfo": false
                                  });
                                  } );
                              </script>
                          </div>
                       </div>
                     </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Electronic energy -->
    <xsl:template name="energySection">
        
        <xsl:variable name="finalEnergy" select="(//module[@id='finalization']/module[@dictRef='energy.info'])[last()]/scalar[@dictRef='cc:energy']"/>
        <xsl:if test="exists($finalEnergy)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#energy-{generate-id($finalEnergy)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Electronic Energy                         
                    </h4>
                </div>
                <div id="energy-{generate-id($finalEnergy)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table id="energyT-{generate-id($finalEnergy)}">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Value</th>
                                            <th>Units</th>    
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Electronic Energy</td>
                                            <td><xsl:value-of select="$finalEnergy"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($finalEnergy/@units)"/></td>
                                        </tr>         
                                    </tbody>                    
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#energyT-<xsl:value-of select="generate-id($finalEnergy)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>        
    </xsl:template>

    <!-- Squared S -->
    <xsl:template name="s2">
        <xsl:variable name="s2" select="(.//module[@id='finalization']/module[@dictRef='energy.info']/scalar[@dictRef='cc:s2'])"/>
        <xsl:if test="exists($s2)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#s2-{generate-id($s2)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        S**2
                    </h4>
                </div>
                <div id="s2-{generate-id($s2)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-sm-12">
                                <table class="display" >
                                    <thead>
                                        <tr>
                                            <th>Total S2 (S squared)</th>
                                        </tr>  
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><xsl:value-of select="format-number($s2/text(),'#0.000000')"/></td>
                                        </tr>                           
                                    </tbody>                    
                                </table>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if> 
    </xsl:template>
        
    <!-- Frequencies section -->    
    <xsl:template name="frequencies">
        <xsl:variable name="frequencies" select=".//module[@id='finalization']/propertyList/property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations']"/>       
        <xsl:if test="exists($frequencies)">
            <xsl:variable name="frequency" select="tokenize($frequencies/array[@dictRef='cc:frequency'],'\s+')"/>
            <div id="vibrationPanel" class="panel panel-default" >
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Frequencies 
                    </h4>
                </div>
                <div id="frequencies" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-12">                                                                                             
                                <script type="text/javascript">
                                    delete Jmol._tracker;
                                    Jmol._local = true;
                                    jmolApplet0 = "jmolApplet0";                                
                                    var jmolInfo = {
                                        width: 560,
                                        height: 400,
                                        debug: false,
                                        color: "0xF0F0F0",
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",                                        
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        readyFunction: vibrationsLoaded,
                                        animframecallback: "modelchanged",
                                        allowjavascript: true,
                                        script: "background white; vibration off; vectors off; sync on;"                                       
                                    }                                       
                                                                        
                                    $('div#frequencies').on('shown.bs.collapse', function () {
                                        if($("div#jmolApplet").children().length == 0){       
                                        use="HTML5";                                       
                                          
                                        jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                                        $("div#jmolApplet").html(Jmol.getAppletHtml(jmolApplet0));
                                        Jmol.script(jmolApplet0,"load <xsl:value-of select="$xyzurl"/>&#38;mode=grrm");                                                                         
                                        vibrationsLoading();
                                    }                                      
                                    });
                                    
                                    
                                    function vibrationsLoading(){                                       
                                    $('div#frequencies').block({ 
                                    message: '<h3>Loading spectrum</h3>', 
                                    css: { border: '3px solid #a00' } 
                                    });                                                                                    
                                    }
                                    
                                    function vibrationsLoaded(){
                                    $('div#frequencies').unblock();                                                           
                                    }             
                                    
                                </script>                                
                                <div id="irSpectrumDiv">
                                    <div id="jsvApplet" style="display:inline; float:left">                                                                         
                                    </div>
                                    <div id="jmolApplet" style="display:inline; float:left">
                                    </div>                                                                                                                               
                                </div>                                
                                <div id="frequencyComboboxDiv" style="display:block">
                                    Selected frequency : 
                                    <script type="text/javascript">
                                        function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                                            vibrationcboIndex = $("select[name='jmolMenu0'] option:selected").index();
                                            if(vibrationcboIndex != objwhat + 1)
                                                $("select[name='jmolMenu0']").val(objwhat + 2);                                                                                      
                                        }                    
                                        
                                        Jmol.jmolMenu(jmolApplet0,[                    
                                        ["vibration off", ".... select ...."],
                                        ["model 1;vibration off", "No vibration",true],
                                        <xsl:for-each select="1 to count($frequency)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            ["model <xsl:value-of select="$outerIndex+1"/>;vibration on" ,"<xsl:value-of select="$frequency[$outerIndex]"/>"]<xsl:if test="$outerIndex != count($frequency)"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>                    
                                        ]);                        
                                    </script>                    
                                </div>
                                <script type="text/javascript">
                                    $("#irSpectrumDiv" ).prepend( $("#frequencyComboboxDiv") );
                                </script>                                                              
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- IRC section -->
    <xsl:template name="irc">
        <xsl:if test="matches($calcType, 'IRC')">
            <xsl:variable name="forward" select="//cml:module[@cmlx:templateRef='irc-path' and child::cml:scalar[@dictRef='g:ircDirection' and matches(text(), 'FORWARD')]]" />
            <xsl:variable name="backward" select="//cml:module[@cmlx:templateRef='irc-path' and child::cml:scalar[@dictRef='g:ircDirection' and matches(text(), 'BACKWARD')]]" />
            <xsl:variable name="ts" select="(//cml:module[@dictRef='cc:finalization']/cml:molecule)[last()]" />
            
            <xsl:variable name="backwardEnergies" select="$backward/cml:module[@cmlx:templateRef='irc-iteration']/cml:module[@cmlx:templateRef='irc-step']/cml:scalar[@dictRef='cc:energy']" />
            <xsl:variable name="backwardSteps" select="$backward/cml:module[@cmlx:templateRef='irc-iteration']/cml:module[@cmlx:templateRef='irc-step']/cml:scalar[@dictRef='g:ircStep']" />
            <xsl:variable name="forwardEnergies" select="$forward/cml:module[@cmlx:templateRef='irc-iteration']/cml:module[@cmlx:templateRef='irc-step']/cml:scalar[@dictRef='cc:energy']" />
            <xsl:variable name="forwardSteps" select="$forward/cml:module[@cmlx:templateRef='irc-iteration']/cml:module[@cmlx:templateRef='irc-step']/cml:scalar[@dictRef='g:ircStep']" />
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#ircEnergiesCollapse" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        IRC path energies                        
                    </h4>
                </div>
                <div id="ircEnergiesCollapse" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-7 col-md-6 col-sm-12">
                                <div id="ircEnergiesChart"></div>
                                <p class="text-center">Please click on each step to load its geometry.</p>
                                <script>
                                    const xValues = [<xsl:for-each select="1 to count($backwardSteps)">'<xsl:variable name="outerIndex" select="."/><xsl:value-of select="format-number(number($backwardSteps[count($backwardSteps) - $outerIndex +1]), '-00')"/>',</xsl:for-each>
                                                    '00',
                                                    <xsl:for-each select="1 to count($forwardSteps)">'<xsl:variable name="outerIndex" select="."/><xsl:value-of select="format-number(number($forwardSteps[$outerIndex]), '00')"/>',</xsl:for-each>];
                                    
                                    const yValues = [<xsl:for-each select="1 to count($backwardEnergies)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$backwardEnergies[count($backwardSteps) - $outerIndex +1]"/>,</xsl:for-each>
                                                    <xsl:value-of select="(//cml:module[@dictRef='cc:finalization']//cml:module[@dictRef='energy.info']/cml:scalar[@dictRef='cc:energy'])[last()]" />,
                                                    <xsl:for-each select="1 to count($forwardEnergies)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$forwardEnergies[$outerIndex]"/>,</xsl:for-each>];
                                    
                                    
                                    var extraInfo = new Array();
                                    
                                    xValues.forEach(function(element,index) { extraInfo.push(index+1)}); 
                                    
                                    $('div#ircEnergiesCollapse').on('shown.bs.collapse', function () {                                    
                                        generateEnergiesChart();
                                        loadJSmolIrc();
                                    })

                                    function generateEnergiesChart(){
                                        var data = getEnergyData();
                                        var layout = {
                                            title: 'Energy per step',
                                            hovermode: 'x',
                                            yaxis: {
                                                title: 'E (Eh)',
                                                showgrid: false,                                                
                                                nticks: 10
                                            },
                                            xaxis: {
                                                title: '# step',
                                                type: 'category',                                                
                                            },
                                        };
                                        
                                        Plotly.react('ircEnergiesChart', data, layout, {responsive: true, showSendToCloud: true});
                                        
                                        document.getElementById('ircEnergiesChart').on('plotly_click', function (eventData) {
                                            const pointIndex = eventData.points[0].pointIndex;                                            
                                            const clickedStep = extraInfo[pointIndex];                                            
                                            ircChanged(clickedStep);
                                        });
                                        
                                    }
                                                  
                                    function getEnergyData() {
                                        var data = new Array();
                                        var serie = {
                                            type: 'scatter',
                                            mode: 'lines+markers',
                                            line: { width: 1 },
                                            x: xValues,
                                            y: yValues,                                            
                                        };                                        
                                        data.push(serie);
                                        return data;
                                    }
                                </script>
                            </div>
                            <div class="col-lg-5 col-md-6 col-sm-12">
                                <div id="ircJSmolDiv"></div>
                                <script type="text/javascript">  
                                    delete Jmol._tracker;
                                    Jmol._local = true;
                                    
                                    var jmolAppletIrc = null;
                                    
                                    jmolIrc_isReady = function(applet) {                               
                                        resizeIrcApplet();
                                    }
                                    
                                    var JmolConfigInfoIrc = {                                       
                                        debug: false,
                                        color: "0xFFFFFF",
                                        addSelectionOptions: false,		
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                        readyFunction: jmolIrc_isReady,
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        allowJavaScript: true,
                                        console: "none"
                                    };
                                    
                                    function loadJSmolIrc(){                                        
                                        jmolAppletIrc = Jmol.getApplet("jmolAppletIrc", JmolConfigInfoIrc);
                                        $("#ircJSmolDiv").html(Jmol.getAppletHtml(jmolAppletIrc));                                                                        
                                        Jmol.script(jmolAppletIrc,"load <xsl:value-of select="$xyzurl"/>&#38;mode=grrm_irc; set platformSpeed 2;");                                                                         
                                    }
                                    
                                    function resizeIrcApplet(){
                                        var width = $("div#ircJSmolDiv").width() - 50;
                                        var height = $("div#ircEnergiesChart").height() - 50; 
                                        Jmol.resizeApplet(jmolAppletIrc, [width,height]);                                                                                                             
                                    }
                                    
                                    window.onresize = function () {
                                        resizeIrcApplet();
                                    };
                                    
                                    function ircChanged(model){
                                        Jmol.script(jmolAppletIrc, "model " + model);
                                    }                                    
                                </script>                                                               
                            </div>                               
                        </div>                            
                    </div>
                </div>
            </div>
        </xsl:if>        
    </xsl:template>

    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAPCAMAAABEF7i9AAAABGdBTUEAANbY1E9YMgAAAJZQTFRF////7u7u3d3dys7KzMzMyMzIxsrGxcbFur+6u7u7s7iyq7GqqqqqmZmZlJmTj5CPiIiIh4eHhoaGgICAfYJ9d3d3cnZxZ2tnZmZmW15bVVVVS0xLREREQ0NDQkJCQUJBOz07OTs5MzMzMTMxLjAuJygnJCUjIiIiISEhICAgGRkZERERDxAPDg4ODQ4NDQ0NDQ0MAAAADbeuvgAAAMNJREFUeNqtk9sSgiAQQJekslaNLpgZ3exiZWr7/z/XEI6N00spO/DCMocDywJZDiCwGhqIOpYkqiWVB4jOo7WfAQa5qA9RZ0R33hG4v36sOa0Q++tuwEIAT0kxRSkHdUR0Ju88gN5k5j/ABY0gjUHKjPkeyALRHZq8GfCvYUic6arEib6zzBHHg9rwd78vQxVlTPogy6ZhCyCWAryMIqYo6cHm1HjDVsDDzXKVg+e0Bm4vFv4hhjSreLt7106x3cuW4wXCCZ5As6hO5AAAAABJRU5ErkJggg==" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAPCAMAAABEF7i9AAAABGdBTUEAANbY1E9YMgAAAJZQTFRF////7u7u3d3dys7KzMzMyMzIxsrGxcbFur+6u7u7s7iyq7GqqqqqmZmZlJmTj5CPiIiIh4eHhoaGgICAfYJ9d3d3cnZxZ2tnZmZmW15bVVVVS0xLREREQ0NDQkJCQUJBOz07OTs5MzMzMTMxLjAuJygnJCUjIiIiISEhICAgGRkZERERDxAPDg4ODQ4NDQ0NDQ0MAAAADbeuvgAAAOJJREFUeNqtk21PAjEQhKdyKjpwFrGIWN8QOPE8cP7/n/PD9YolmhivmzRpJ9un090WyhwQsoYgkCRvZYPkm5IcfPzbXwssGxsP8WtyOO0JfH47uC50R57e9wPuLIpK3nhVBfwrObiSJAAS1A7FKXAAhDYcAW90gWoB52ozHsHtyOF5yEebFfK71W9CB5ypMLLAYgkAriEvz6LD4KPb2+FTAT869PPauDHcPnWo7zdE6vBIiDXcW4xqzY3X8gQPq6SGKfBvNeTLNnOXy89JBD5uMrxDznQdeE/vfX9K7r+cOb4AY2+UGwcd6o0AAAAASUVORK5CYII=" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
        
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
	
</xsl:stylesheet>

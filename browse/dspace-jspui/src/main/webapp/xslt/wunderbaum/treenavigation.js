function initNavigationTree() {
    let dropCounter = 0;
    let sourceNodeType = null;
    $("#tree").html("");
    tree = new mar10.Wunderbaum({
        element: document.getElementById("tree"),
        iconMap: {
            error: "fas fa-exclamation-triangle",
            loading: "fas fa-hourglass-start",
            noData: "fas fa-exclamation-triangle",
            expanderExpanded: "fas fa-caret-down",
            expanderCollapsed: "fas fa-caret-right",
            expanderLazy: "fas fa-caret-right wb-helper-lazy-expander",
            checkChecked: "far fa-check-square",
            checkUnchecked: "far fa-square",
            checkUnknown: "fas fa-square",
            radioChecked: "far fa-dot-circle",
            radioUnchecked: "far fa-circle",
            radioUnknown: "fas fa-circle",
            folder: "fas fa-atom",
            folderOpen: "fas fa-atom",
            folderLazy: "fas fa-sync",
            doc: "far fa-file",
        },
        types: {
            PRO: { icon: "far fa-folder", classes: "" }
        },
        columns: [{ id: "*", title: "Name", width: 5.0 },
        { id: "type", title: "Type", width: 2.0 },
        { id: "description", title: "Desc.", width: 5.0 },
        { id: "creationDate", title: "Creation Date", width: 3.0 },
        { id: "handle", title: "Handle", width: 2.5 },
        { id: "published", title: "Pub", width: "30px" },
        { id: "edit", title: "Edit", width: "30px" },
        { id: "key", title: "Key", width: "0px" }
        ],
        filter: {
            connectInput: "input#filterNavigationQuery",
            mode: "dim",
        },
        source: baseUrl + "/create/userCommands/navigation",
        navigationModeOption: "row",
        selectMode: "multiple",
        checkbox: false,
        autoActivate: true,
        keyboard: false,
        preventSameParent: false,
        preventForeignNodes: true,
        multiSource: true,
        debugLevel: 0,
        init: (e) => {
            e.tree.setFocus();
            $('#itemSelectionCount').text(0);
        },
        load: (e) => {
            setupRowHoverListener();
            $($("div.wb-row:first-child span.wb-col-title")[0]).html("Name <span class='small'>(Ctrl+click to select)</span>");
            setupNavigationTreeContextMenu();
            setupNavigationTreeContextMenuActions();
            fireEventFromClient("onClearBusy")
        },
        click: (e) => {            
            if (e.info.canonicalName === 'Control+click' && ($(e.event.target).hasClass('wb-icon') || $(e.event.target).hasClass('wb-title') || $(e.event.target).hasClass('wb-col'))) {                    
                document.body.style.cursor = "wait"; 
                setTimeout(() => {
                    multipleSelection(e.node);
                },0);
            } else if (e.info.canonicalName === 'click' && ($(e.event.target).hasClass('wb-icon') || $(e.event.target).hasClass('wb-title') || $(e.event.target).hasClass('wb-col'))) {
                document.body.style.cursor = "wait"; 
                setTimeout(() => {
                    singleSelection(e.node);
                },0);
            }
        },
        dnd: {
            dragStart: (e) => {
                if (tree.getSelectedNodes().length === 0)
                    return false;
                if (!tree.getSelectedNodes().includes(e.node))
                    return false;
                if (selectionContainsPublishedElements())
                    return false;
                sourceNodeType = getSelectionNodeType();
                dropCounter = 0;
                return true;
            },
            dragEnter: (e) => {
                if (sourceNodeType === 'PRO' && e.node.type === 'PRO') {
                    return ["before", "after", "over"];
                } else if (sourceNodeType === 'CALC') {
                    return e.node.type === 'PRO' ? ["over"] : ["before", "after"];
                } else {
                    return e.node.type === 'PRO' ? ["over"] : [];
                }
            },
            drop: (e) => {
                if (dropCounter !== 0)
                    return;
                dropCounter++;

                let selection = getSelectedNodesId();
                if (e.region === 'before' || e.region === 'after') {
                    if (e.node.getParent().isRootNode())
                        selection.target = -1;
                    else
                        selection.target = e.node.getParent().data.id;
                } else {
                    selection.target = e.node.data.id;
                }
                if (sourceNodeType === 'CALC' && e.node.type !== 'PRO') {
                    selection.target = e.node.data.id;
                    fireCustomEventFromClient("$treeWindow", "onTreeNodesMoveToCalculation", JSON.stringify(selection));
                } else {
                    fireCustomEventFromClient("$treeWindow", "onTreeNodesMoveToProject", JSON.stringify(selection));
                }
            },
        },
        select: (e) => {
            let totalSelected = e.tree.getSelectedNodes().length;
            $('#itemSelectionCount').text(totalSelected);

            clearTimeout(selectionTimeout);
            selectionTimeout = setTimeout(function () {
                if (totalSelected !== 1) {
                    let selection = getSelectedNodesId();
                    selection.target = "";
                    fireCustomEventFromClient("$treeWindow", "onMultipleSelection", JSON.stringify(selection));
                }                
            }, 500);

        },
        render: (e) => {
            const node = e.node;
            for (const col of Object.values(e.renderColInfosById)) {
                switch (col.id) {
                    case "type": col.elem.textContent = node.type;
                        break;

                    case "handle": if (node.data.handle !== "") {
                        let url = browseUrl + '/handle/' + node.data.handle;
                        col.elem.innerHTML = "<a href=" + url + " target='_blank' rel='noopener noreferrer'>" + node.data.handle + "</a>";
                    }
                        break;

                    case "published": if (node.data.published === true) {
                        col.elem.innerHTML = "<i class='fas fa-check'></i>";
                    }
                        break;

                    case "edit": if (node.data.edit !== null && node.data.edit !== "" && (node.getParent().data.handle !== node.data.handle )) {
                        let url = browseUrl + '/edit-collection/' + node.data.handle + "/" + node.data.edit;
                        col.elem.innerHTML = "<a href='" + url + "' target='_blank' rel='noopener noreferrer'><i class='far fa-edit'></i></a>";
                    }
                        break;
                    case "key": col.elem.textContent = node.key;
                        break;

                    default: col.elem.textContent = node.data[col.id];
                        break;
                }
            }         
        }
    });

    tree.sortChildren(nodeTitleSorter, true);
    $('#filterNavigationQuery').val('');
    tree.filterNodes('', {});

    $('#filterNavigationClearBtn').click(function () {
        $('#filterNavigationQuery').val('');
        tree.filterNodes('', {});
    });

}

function nodeTitleSorter(a, b) {
    if (a.type === 'PRO' && b.type !== 'PRO') {
        return -1;
    } else if (a.type !== 'PRO' && b.type === 'PRO') {
        return 1;
    } else {
        return a.data.elementOrder - b.data.elementOrder;
    }
}

function singleSelection(node) {
    tree.runWithDeferredUpdate(() => {
        tree.visit((node) => {
            node.setSelected(false);
        });
        node.setActive();
        node.setSelected(true);
        fireCustomEventFromClient("$treeWindow", node.type === 'PRO' ? "onProjectSelected" : "onCalculationSelected", node.data.id);
    });
    document.body.style.cursor = "default";
}

function multipleSelection(node) {
    let isSelected = node.isSelected();
    tree.runWithDeferredUpdate(() => {
        node.visit((node) => {
            node.setSelected(isSelected);
        });
        //If the element is unselected, must unselect all parent project, otherwise will be orfan elements
        if (!isSelected) {
            unSelectParentProjects(node.getParent());
        }
    });
    document.body.style.cursor = "default";
}

function clearNavigationSelection() {
    tree.runWithDeferredUpdate(() => {
        tree.visit((node) => {
            node.setSelected(false);
        });
    });
}

function unSelectParentProjects(node) {
    if (node.isSelected() !== true)
        return;
    node.setSelected(false);
    unSelectParentProjects(node.getParent());
}

function selectionContainsPublishedElements() {
    for (const node of tree.getSelectedNodes()) {
        if (node.data.published === true) {
            return true;
        }
    }
    return false;
}

function setupRowHoverListener() {
    
    let targetDiv = $("div.treeParentDiv");
    const targetPosition = targetDiv.offset();
    const targetWidth = targetDiv.outerWidth();
    const targetHeight = targetDiv.outerHeight();

    // Calculate the position for the floating div (lower right corner)
    const floatingDivX = targetPosition.left + targetWidth - 200;
    const floatingDivY = targetPosition.top + targetHeight - 200;
    $("div.thumbnailPopupDiv").css({ top: floatingDivY, left: floatingDivX });


    $(document).on('mouseenter', '.wb-row', function (e) {
        let row = $(e.currentTarget);
        thumbnailTimeout = setTimeout(function () {
            let key = $(row).find("span.wb-col:last-child").text();
            let node = tree.findKey(key);
            if (node !== null && node.type !== 'PRO') {
                $("div.thumbnailPopupDiv").html("<img style='width:200px;height:200px' src='../innerServices/getfile?id=" + node.data.id + "&file=thumbnail.jpeg' alt='No image available'></image>");
                $("div.thumbnailPopupDiv").show();
            }
        }, 500);
    });

    $(document).on('mouseleave', '.wb-row', function () {
        clearTimeout(thumbnailTimeout);
        $("div.thumbnailPopupDiv").hide();
    });
}


function getSelectedNodesId() {
    let selectedProjects = [];
    let selectedCalculations = [];

    tree.getSelectedNodes().forEach(
        e => {
            if (e.type === 'PRO' && e.getParent().selected === false)
                selectedProjects.push(e.data.id);
            else if (e.type !== 'PRO' && e.getParent().selected !== true)
                selectedCalculations.push(e.data.id);
        });
    return { projects: selectedProjects, calculations: selectedCalculations };
}

function getSelectionNodeType() {
    let hasProjects = false;
    let hasCalculations = false;

    tree.getSelectedNodes().forEach(
        e => {
            if (e.type === 'PRO') hasProjects = true;
            if (e.type !== 'PRO' && !e.getParent().isSelected()) hasCalculations = true;
        });
    return hasProjects && hasCalculations ? 'MIXED' : hasProjects ? 'PRO' : 'CALC';

}

function searchFromHere() {
    if (tree.getSelectedNodes().length === 0)
        return;
    else if (tree.getSelectedNodes().length === 1) {
        if (tree.getSelectedNodes()[0].type === 'PRO')
            fireCustomEventFromClient("$treeWindow", "onSearchFromHere", tree.getSelectedNodes()[0].data.id);
        else
            fireCustomEventFromClient("$treeWindow", "onSearchFromHere", tree.getSelectedNodes()[0].getParent().data.id);
    }
}

function publishSelection() {
    fireCustomEventFromClient("$treeWindow", "onPublishSelection", JSON.stringify(getSelectedNodesId()));
}


/** Setup tree context menu */

function setupNavigationTreeContextMenu() {
    // Add event listener to show the custom context menu on right-click
    $('.wb-node-list').on('contextmenu', function (e) { showCustomContextMenu(e); });
    // Add event listener to hide the custom context menu when clicking outside it
    document.addEventListener('click', hideCustomContextMenu);
}

function setupNavigationTreeContextMenuActions() {
    $(".tree-nav-report").remove();
    $("#customContextMenu").append("<n:h6 class=\"dropdown-header tree-nav-report\" href=\"#\">Generate report</n:h6>");
    reportTypes.reports.forEach((e) => {
        $("#customContextMenu").append("<a class='dropdown-item tree-nav-report' href='#' onclick='createReportFromSelection(" + e.id + ")'>" + e.name + "</a>");
    });
}

function selectNavigationNodeChildren() {

    if (tree.getSelectedNodes().length === 1 && tree.getSelectedNodes()[0].type === 'PRO') {
        tree.runWithDeferredUpdate(() => {
            tree.getSelectedNodes()[0].visit((node) => {
                node.setSelected(true);
            });
        });
    }
}


function createReportFromSelection(reportId) {
    let selection = getSelectedNodesId();
    selection.target = reportId;
    fireCustomEventFromClient("$treeWindow", "onCreateReportFromSelection", JSON.stringify(selection));
}

// Function to show the custom context menu at the cursor position
function showCustomContextMenu(event) {
    event.preventDefault();

    if (tree.getSelectedNodes().length === 0)
        return;
    if (tree.getSelectedNodes().length === 1 && tree.getSelectedNodes()[0].type === 'PRO') {
        $('.tree-nav-proj').show();
        $('.dropdown-divider.report-section').show();
        if(!hasAncestorPublished(tree.getSelectedNodes()[0])){
            let publishLabel = tree.getSelectedNodes()[0].data.published === true ? "Publish (Append to existing)" : "Publish";
            $('.tree-nav-publish-label').text(publishLabel);
            $('.tree-nav-publish').show();
        }else{
            $('.tree-nav-publish').hide();
        } 
    } else {
        $('.dropdown-divider.report-section').hide();
        $('.tree-nav-proj').hide();
        $('.tree-nav-publish').hide();
    }
    // Set position based on cursor position
    const x = event.clientX;
    const y = event.clientY - 60;

    const customContextMenu = document.getElementById('customContextMenu');
    customContextMenu.style.display = 'block';
    const windowHeight = window.innerHeight;
    const menuHeight = customContextMenu.offsetHeight;
    const adjustedY = (y + menuHeight > windowHeight) ? y - menuHeight : y;

    customContextMenu.style.left = x + 'px';
    customContextMenu.style.top = adjustedY + 'px';

}

function hasAncestorPublished(node) {
    if (node.getParent().isRootNode())
        return false;
    else    {
        if (node.getParent().data.published === true)
            return true;
        else
            return hasAncestorPublished(node.getParent());
    }
}


// Function to hide the custom context menu
function hideCustomContextMenu() {
    const customContextMenu = document.getElementById('customContextMenu');
    customContextMenu.style.display = 'none';
}

async function expandNavigationActiveNode() {
    await tree.getActiveNode().expandAll();
    tree.getActiveNode().setExpanded(true)
}

async function collapseNavigationActiveNode() {
    tree.runWithDeferredUpdate(() => {
        collapseChildren(tree.getSelectedNodes()[0]);
        tree.getActiveNode().setExpanded(false)
    });
}

function collapseChildren(node) {
    node.children.forEach(child => {
        child.setExpanded(false);
        collapseChildren(child);
    });
}

// Navigation tree functions
function addProject(parentId, obj) {
    if (parentId === -1)
        tree.root.addChildren(obj);
    else
        getProjectNodeById(parentId).addChildren(obj);
    tree.sortChildren(nodeTitleSorter, true);
}

function addCalculation(parentId, obj) {
    if (getCalculationNodeById(obj.id) !== null)
        return;
    let node = getProjectNodeById(parentId);
    node.addChildren(obj);
    tree.sortChildren(nodeTitleSorter, true);
}

function updateProject(parentId, projectId, obj) {
    let node = getProjectNodeById(projectId);
    node.title = obj.title;
    node.data.description = obj.description;
    //Move project if needed
    //If parentId is different from node.getParent().data.id, then the project has to be moved to new parentId
    if (parentId === -1) {
        if (!node.getParent().isRootNode()) {
            reduceProjectOrder(node, projectId, obj);
            node.moveTo(tree.root);
        }
    } else if (parentId !== node.getParent().data.id) {
        reduceProjectOrder(node, projectId, obj);
        node.moveTo(getProjectNodeById(parentId));
    }
    tree.sortChildren(nodeTitleSorter, true);
}

function reduceProjectOrder(node, projectId, obj) {
    node.getParent().children.forEach(child => {
        if (child.type !== 'PRO')
            return;
        if (child.data.id != projectId && child.data.elementOrder >= obj.elementOrder) {
            child.data.elementOrder--;
        }
    });
}

function deleteCalculation(calcId) {
    let node = getCalculationNodeById(calcId);
    if (node === null)
        return;
    if (node.isSelected())
        singleSelection(node.getParent());
    node.remove();
}

function deleteProject(projectId) {
    let node = getProjectNodeById(projectId);
    if (node === null)
        return;
    node.setSelected(false);
    tree.runWithDeferredUpdate(() => {
        node.visit((node) => {
            if (node.isSelected()) {
                node.setSelected(false);
            }
        });
    });
    node.remove();
}

function updateCalculation(parentId, calcId, obj) {
    let node = getCalculationNodeById(calcId);
    if (node === null)
        return;
    if (parentId !== node.getParent().data.id) {      // Reduce old parent following calculations order
        reduceCalculationOrder(node, calcId, obj);
        node.moveTo(getProjectNodeById(parentId));
    }
    //Update node
    node.title = obj.title;
    node.data.description = obj.description;
    tree.sortChildren(nodeTitleSorter, true);
}

function reduceCalculationOrder(node, calcId, obj) {
    node.getParent().children.forEach(child => {
        if (child.type === 'PRO')
            return;
        if (child.data.id != calcId && child.data.elementOrder >= obj.elementOrder) {
            child.data.elementOrder--;
        }
    });
}

function updateCalculationOrder(calcId, oldPath, newPath, oldOrder, newOrder) {
    let node = getCalculationNodeById(calcId);
    node.data.elementOrder = newOrder;
    if (oldPath !== newPath) {
        increaseCalculationOrder(node, calcId, newOrder); // Moved element to new project, increase order of all following calculations 
    } else {
        updateCalculationOrderInRange(node, calcId, oldOrder, newOrder);   // Moved element to same parent, update order of all calculations in range
    }
    tree.sortChildren(nodeTitleSorter, true);
}

function updateProjectOrder(projectId, oldPath, newPath, oldOrder, newOrder) {
    let node = getProjectNodeById(projectId);
    node.data.elementOrder = newOrder;
    if (oldPath !== newPath) {
        increaseProjectOrder(node, projectId, newOrder); // Moved element to new project, increase order of all following projects
    } else {
        updateProjectOrderInRange(node, projectId, oldOrder, newOrder); // Moved element to same parent, update order of all projects in range
    }
    tree.sortChildren(nodeTitleSorter, true);
}

function increaseProjectOrder(node, projectId, order) {
    node.getParent().children.forEach(child => {
        if (child.type === 'PRO' && child.data.id != projectId && child.data.elementOrder >= order) {
            child.data.elementOrder++;
        }
    });
}

function updateProjectOrderInRange(node, projectId, oldOrder, newOrder) {
    node.getParent().children.forEach(child => {
        if (child.type !== 'PRO')
            return;
        const isSameProjectId = child.data.id !== projectId;
        const isWithinOrderRange = child.data.elementOrder >= Math.min(oldOrder, newOrder) && child.data.elementOrder <= Math.max(oldOrder, newOrder);

        if (isSameProjectId && isWithinOrderRange) {
            child.data.elementOrder += oldOrder < newOrder ? -1 : 1;
        }
    });
}

function increaseCalculationOrder(node, calcId, order) {
    node.getParent().children.forEach(child => {
        if (child.type !== 'PRO' && child.data.id != calcId && child.data.elementOrder >= order) {
            child.data.elementOrder++;
        }
    });
}

function updateCalculationOrderInRange(node, calcId, oldOrder, newOrder) {
    node.getParent().children.forEach(child => {
        if (child.type === 'PRO')
            return;
        const isSameCalcId = child.data.id !== calcId;
        const isWithinOrderRange = child.data.elementOrder >= Math.min(oldOrder, newOrder) && child.data.elementOrder <= Math.max(oldOrder, newOrder);

        if (isSameCalcId && isWithinOrderRange) {
            child.data.elementOrder += oldOrder < newOrder ? -1 : 1;
        }
    });
}

function getProjectNodeById(id) {
    let node = tree.findAll((n) => {
        return n.data.id === id && n.type === 'PRO';
    });
    if (node.length === 0)
        return null;
    else
        return node[0];
}

function getCalculationNodeById(id) {
    let node = tree.findAll((n) => {
        return n.data.id === id && n.type !== 'PRO';
    });
    if (node.length === 0)
        return null;
    else
        return node[0];
}

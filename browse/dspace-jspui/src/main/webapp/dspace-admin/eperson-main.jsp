<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:


    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>

<%--
  - main page for eperson admin
  -
  - Attributes:
  -   no_eperson_selected - if a user tries to edit or delete an EPerson without
  -                         first selecting one
  -   reset_password - if a user tries to reset password of an EPerson and the email with token is
  -                    send successfull 
  -
  - Returns:
  -   submit_add    - admin wants to add an eperson
  -   submit_browse - admin wants to browse epeople
  -
  --%>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"
    prefix="fmt" %>

<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.eperson.Group"   %>
<%@ page import="org.dspace.core.Context"%>
<%@ page import="org.dspace.app.webui.util.UIUtil"%>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%!
	public boolean hasUserGroups(Context context) {		
		try{			
			Group[] groups = Group.findAll(context, Group.NAME);
			for(Group group : groups) {
				if(group.getName().startsWith("COLLECTION") || 
					group.getName().startsWith("COMMUNITY") || 
					group.getName().equals("Anonymous")  || 
					group.getName().equals("Administrator") ||
					group.getName().equals("Reviewers"))
					continue;
				else
					return true;
			}
			return false;
		}catch(Exception e){
			return true;
		}
	}
%>

<%	
    boolean noEPersonSelected = (request.getAttribute("no_eperson_selected") != null);
    boolean resetPassword = (request.getAttribute("reset_password") != null);
    boolean loginAs = ConfigurationManager.getBooleanProperty("webui.user.assumelogin", false);
    boolean existsUserGroups = hasUserGroups(UIUtil.obtainContext(request));
%>

<dspace:layout style="submission" titlekey="jsp.dspace-admin.eperson-main.title"
               navbar="admin"
               locbar="link"
               parenttitlekey="jsp.administer"
               parentlink="/dspace-admin">

    <%-- <h1>Administer EPeople</h1> --%>
    <h1><fmt:message key="jsp.dspace-admin.eperson-main.heading"/>
    <dspace:popup page="<%= LocaleSupport.getLocalizedMessage(pageContext, \"help.site-admin\") + \"epeople\"%>"><fmt:message key="jsp.help"/></dspace:popup>
    </h1>
  
    <%-- <h3>Choose an action:</h3> --%>
    <h3><fmt:message key="jsp.dspace-admin.eperson-main.choose"/></h3>
  
  
  

<% if (noEPersonSelected)
	{ %><p class="alert alert-warning">
	     <fmt:message key="jsp.dspace-admin.eperson-main.noepersonselected"/>
	   </p>
<%  } %>
<% if (resetPassword)
	{ %><p class="alert alert-success">
	     <fmt:message key="jsp.dspace-admin.eperson-main.ResetPassword.success_notice"/>
	   </p>
<%  }  
    if (!existsUserGroups) 
    { %><p class="alert alert-warning">
	     <fmt:message key="jsp.dspace-admin.eperson-main.noepersongroups"/>
	   </p>
    	    
<%  } %>  
    <form name="epersongroup" method="post" action="">             
			<div class="row">
            <%-- <input type="submit" name="submit_add" value="Add EPerson..."> --%>
            	<div class="col-md-6 col-md-offset-3 col-sm-12">         
            		<div class="row">    
            			<input class="btn btn-success col-md-6" type="submit" name="submit_add" value="<fmt:message key="jsp.dspace-admin.eperson-main.add"/>"  <%=(!existsUserGroups)? "disabled": ""%> />
            			<input class="btn btn-success col-md-6" type="submit" name="submit_csv_add" value="<fmt:message key="jsp.dspace-admin.eperson-main.addbulk"/>" />
            		</div>
            	</div>
			</div>
			<br/>

	        <fmt:message key="jsp.dspace-admin.eperson-main.or"/>
			
            <div class="row">
	            <div class="col-md-6">
	            <dspace:selecteperson multiple="false" />
	            </div>
            
            <%-- then&nbsp;<input type="submit" name="submit_edit" value="Edit..." onclick="javascript:finishEPerson();"> --%>
			<div class="col-md-2">
						<fmt:message key="jsp.dspace-admin.eperson-main.then"/>
			</div>
			<div class="col-md-4">
			<input type="submit" class="btn btn-default col-md-4" name="submit_edit" value="<fmt:message key="jsp.dspace-admin.general.edit"/>" onclick="javascript:finishEPerson();"/>
						
            <% if(loginAs) { %>&nbsp;<input type="submit" class="btn btn-default col-md-4" name="submit_login_as" value="<fmt:message key="jsp.dspace-admin.eperson-main.LoginAs.submit"/>" onclick="javascript:finishEPerson();"/> <% } %>
            
            <%-- <input type="submit" name="submit_delete" value="Delete..." onclick="javascript:finishEPerson();"> --%>
            <input type="submit" class="btn btn-danger col-md-4" name="submit_delete" value="<fmt:message key="jsp.dspace-admin.general.delete-w-confirm"/>" onclick="javascript:finishEPerson();"/>
            
            </div>
            </div>
    </form>
 
</dspace:layout>

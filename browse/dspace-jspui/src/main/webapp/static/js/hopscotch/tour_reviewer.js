 // Define the tour!


$(function(){
	
    var tour = {
      id: "hello-hopscotch",
      steps: [       
        {
          title: "Welcome to ioChem-BD",
          content: "You are now accessing to the restricted content of an embargoed collection.",
          target: document.querySelector("div .well"),
          placement: "bottom"
        },
        {
            title: "Usage recommendations",
            content: "You can view collection stored items by clicking on its name.",
            target: document.querySelector("#browsetable > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(3)"),
            placement: "bottom"
        },        
        {
            title: "Unrelated content access",
            content: "If you click on Author links you will browse all current author's published content. Which is out of the scope of this restricted collection",
            target: $('th:contains("Author")')[0],
            placement: "bottom"
        },      
        {
            title: "Logout after use",
            content: "Once all content has been reviewed, please close current session clicking on 'Logout' option inside the navigation bar.",
            target: document.querySelector("span.glyphicon-user"),
            placement: "left"
        }
      ],
      onEnd: function() {
    	  setCookie("review_tour", "toured");

      },
      onClose: function() {
    	  setCookie("review_tour", "toured");

    	  }
    };
	// Initialize tour if it's the user's first time
	if (!getCookie("review_tour")) {
	    hopscotch.startTour(tour);
	}
});

function setCookie(key, value) {
	var expires = new Date();
		expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
		document.cookie = key + '=' + value + ';path=/' + ';expires=' + expires.toUTCString();
};

function getCookie(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
};

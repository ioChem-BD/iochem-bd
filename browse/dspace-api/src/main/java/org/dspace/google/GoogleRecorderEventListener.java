/**
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */

package org.dspace.google;

import org.dspace.services.model.Event;
import org.dspace.usage.AbstractUsageEventListener;

/**
 * Deactivated Google Analytics download file event due to conflic with GDPR detection.
 * To record file downloads, consent cookie should be retrieved at servlet level.
 * @author malvarez
 *
 */

public class GoogleRecorderEventListener extends AbstractUsageEventListener {

    
    public GoogleRecorderEventListener() {

    }

    public void receiveEvent(Event event) {
        return;
    }
}

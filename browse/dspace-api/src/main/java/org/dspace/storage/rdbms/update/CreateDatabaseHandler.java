package org.dspace.storage.rdbms.update;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.dspace.core.ConfigurationManager;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to handle the Create module database connection. 
 * It will be used in conjunction with the Flyway versioning system to sync databases to the latest version.
 * 
 * @author malvarez
 *
 */

public class CreateDatabaseHandler {

	/** logging category */
    private static final Logger log = LoggerFactory.getLogger(CreateDatabaseHandler.class);
	
	public static final String DDBB = "java:/comp/env/jdbc/create";
	public static final String NO_DDBB = "Server cannot locate database";	
    
	public static final InitialContext cxt = initContext();
	
	private static InitialContext initContext() {	
		try {
			InitialContext cxt = new InitialContext();
			cxt.addToEnvironment(DDBB, setupDataSource());
			return cxt;
		} catch (NamingException | FileNotFoundException e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	private CreateDatabaseHandler() {
		 throw new IllegalStateException("Utility class");
	 }
	 
	 public static Connection getCreateConnection() throws SQLException, NamingException {	
		Connection conn = null;
		DataSource ds = (DataSource) cxt.getEnvironment().get(DDBB);
		if ( ds == null ) 
			throw new SQLException(NO_DDBB);		
		conn =  ds.getConnection();
		return conn;		
	}	
	
	public static void closeConnection(Connection conn){
		try {
			if(conn != null)
				conn.close();
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
	}
	
	private static DataSource setupDataSource() throws FileNotFoundException {
		Properties props = loadCreateProperties();
		PGSimpleDataSource source = new PGSimpleDataSource();
		source.setUrl(buildUrl(props));				
		source.setUser(props.getProperty("create.database.user"));
		source.setPassword(props.getProperty("create.database.pwd"));
		return source;
	}

	private static Properties loadCreateProperties() throws FileNotFoundException {
		String propertiesFilePath = ConfigurationManager.getProperty("dspace.dir").replace("/browse", "/create/resources.properties");
		try (FileInputStream fis = new FileInputStream(propertiesFilePath)) {
			Properties props = new Properties();			
			props.load(fis);
			return props;
		} catch (Exception e) {
			log.error("Error raised loading Create module resources.properties file located on : {}" , propertiesFilePath);
			log.error(e.getMessage());
			throw new FileNotFoundException();
		}
	}	
	
	private static String buildUrl(Properties props) {
		StringBuilder url = new StringBuilder();
		url.append("jdbc:postgresql://");
		url.append(props.getProperty("create.database.host").trim());
		url.append(":");
		url.append(props.getProperty("create.database.port").trim());
		url.append("/");
		url.append(props.getProperty("create.database.name").trim());
		
		String extras = props.getProperty("database.extra.parameters");
	    if(extras != null && !extras.isEmpty())
	    	url.append(extras);		
	    return url.toString();
	}	
}

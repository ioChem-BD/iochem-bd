/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.dspace.app.util.TokenManager;
import org.dspace.core.ConfigurationManager;
import org.dspace.servicemanager.DSpaceKernelImpl;
import org.dspace.servicemanager.DSpaceKernelInit;
import org.dspace.services.ConfigurationService;

public class CommunicationSecretManager {
	
    /** The service manager kernel */
    private static transient DSpaceKernelImpl kernelImpl;
	
	public static void main(String[] argv) throws Exception {
        CommandLineParser parser = new PosixParser();
        Options options = new Options();
        options.addOption("r", "regenerate", false, "regenerate communication secrets (will update all reviewer and edit collection links)");
        options.addOption("p", "print", false, "Print external communication secret.");        
        options.addOption("h", "help", false, "help");
        CommandLine line = parser.parse(options, argv);

        if (line.hasOption('h') || (!line.hasOption('r') && (!line.hasOption('p')) )) {
            displayHelp(options);
            System.exit(0);
        }
       
    	initKernel();
        if (line.hasOption('r'))         
        	regenerateSecrets();
        else if(line.hasOption('p'))        	
        	printSecrets(); 
        destroyKernel();        
	}

	private static void displayHelp(Options options) {
		HelpFormatter myhelp = new HelpFormatter();
        myhelp.printHelp("CommunicationSecretManager\n", options);
        System.out.println("\nregenerate: CommunicationSecretManager -r");
        System.out.println("print:       CommunicationSecretManager -p");
	}
	
	private static void initKernel() {
		try {
            kernelImpl = DSpaceKernelInit.getKernel(null);
            if (!kernelImpl.isRunning()) {
                kernelImpl.start(ConfigurationManager.getProperty("dspace.dir"));
            }
        } catch (Exception e) {
            // Failed to start so destroy it and log and throw an exception
            try {
                kernelImpl.destroy();
            }
            catch (Exception e1) {
                // Nothing to do
            }
            String message = "Failure during task init: " + e.getMessage();
            System.err.println(message + ":" + e);
            throw new IllegalStateException(message, e);
        }
	}
	
	private static void destroyKernel() {
		if (kernelImpl != null) {
			kernelImpl.destroy();
			kernelImpl = null;
		}
	}

	private static void regenerateSecrets() throws FileNotFoundException, IOException, ConfigurationException {	    	   
	    //Read previous secret
	    String oldExternalSecret = getExternalSecret();	    
	    //Create new secrets
	    String moduleSecret = TokenManager.buildRandomString();
	    String externalSecret = TokenManager.buildRandomString();
	    
	    updateSecrets(moduleSecret, externalSecret);	    	    
	    printRegeneratedSecrets(oldExternalSecret, externalSecret);	    	
	}
	
	private static String getExternalSecret() {
		ConfigurationService config = kernelImpl.getConfigurationService();
		String secret = config.getProperty("external.communication.secret");
		return (secret == null) ? "" : secret;		
	}

	private static void updateSecrets(String moduleSecret, String externalSecret) throws ConfigurationException {
		ConfigurationService config = kernelImpl.getConfigurationService();
		// Add secrets to Browse configuration file
		String pathToBrowseConfig = Paths.get(config.getProperty("dspace.dir"), "config" ,"dspace.cfg").toString();
		updateConfiguration(pathToBrowseConfig, moduleSecret, externalSecret);
		// Add secrets to Create configuration file		        
        String pathToCreateConfig = Paths.get(ConfigurationManager.getProperty("dspace.dir"), "..", "create", "resources.properties").toString();
        updateConfiguration(pathToCreateConfig, moduleSecret, externalSecret);		
	}

	private static void updateConfiguration(String path, String moduleSecret, String externalSecret) throws ConfigurationException {	
		PropertiesConfiguration properties = new PropertiesConfiguration(path);
		properties.setProperty("module.communication.secret", moduleSecret);
		properties.setProperty("external.communication.secret", externalSecret);
		properties.save();
	}

	private static void printRegeneratedSecrets(String oldSecret, String newSecret) throws FileNotFoundException, IOException {
		System.out.println("Communication secrets have been regenerated.");
		System.out.println("If your institution requests DOIs and is synchronized with ioChem-BD Find at www.iochem-bd.org");
		System.out.println();
		System.out.println("Please send following text to contact@iochem-bd.org");
		System.out.println();
		System.out.println("Institution: " + getInstitutionName());
		System.out.println("Url:         " + ConfigurationManager.getProperty("dspace.baseUrl"));
		System.out.println();
		if(oldSecret != null) 
			System.out.println("Old communication secret:  " + oldSecret);
		System.out.println("New communication secret:  " + newSecret);
		System.out.println();
		System.out.println("Please reboot ioChem-BD web service in order to apply the new secrets.");
		
	}
	
	private static void printSecrets() throws FileNotFoundException, IOException {
		System.out.println("Listing current communication secrets:");		
		System.out.println("If current instance is synchronized with ioChem-BD Find at www.iochem-bd.org and it fails to do so, ");
		System.out.println("please send following text to contact@iochem-bd.org");
		System.out.println();
		System.out.println("Institution:  " + getInstitutionName());
		System.out.println("Url:          " + ConfigurationManager.getProperty("dspace.baseUrl"));
		System.out.println();
		System.out.println("Current communication secret: " + getExternalSecret());
	}
	
	private static String getInstitutionName() throws FileNotFoundException, IOException {
		Properties createProperties = new Properties();		
        String createPropsFilePath = Paths.get(ConfigurationManager.getProperty("dspace.dir"), "..", "create", "resources.properties").toString();
        File propertiesFile = new File(createPropsFilePath);
        createProperties.load(new FileInputStream(propertiesFile));
        return createProperties.getProperty("mets.institution.name");
	}
}

/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.twitter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Email.InputStreamDataSource;
import org.dspace.services.EmailService;
import org.dspace.utils.DSpace;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public class Tweet {

    /** The content of the message */
    private String content;

        /** The arguments to fill out */
    private List<Object> arguments;
   
    /** The character set this message will be sent in */
    private String charset;

    private static final Logger log = Logger.getLogger(Tweet.class);

	
    /**
     * Get the template for a tweet message. 
     *
     * @param tweetFile
     *            full name for the tweettemplate, for example "/dspace/config/tweet/addCollectionWithDOI".
     *
     * @return the tweet object, with the content filled out from
     *         the template
     *
     * @throws IOException
     *             if the template couldn't be found, or there was some other
     *             error reading the template
     */
    public static Tweet getTweet(String tweetFile)
            throws IOException
    {
        String charset = null;
        StringBuilder contentBuffer = new StringBuilder();
        InputStream is = null;
        InputStreamReader ir = null;
        BufferedReader reader = null;
        try
        {
            is = new FileInputStream(tweetFile);
            ir = new InputStreamReader(is, "UTF-8");
            reader = new BufferedReader(ir);
            boolean more = true;
            while (more)
            {
                String line = reader.readLine();
                if (line == null)
                {
                    more = false;
                }                
                else if (line.toLowerCase().startsWith("charset:"))
                {
                    charset = line.substring(8).trim();
                }
                else if (!line.startsWith("#"))
                {
                    contentBuffer.append(line);
                    contentBuffer.append("\n");
                }
            }
        } finally
        {
            if (reader != null)
            {
                try {
                    reader.close();
                } catch (IOException ioe)
                {
                }
            }
            if (ir != null)
            {
                try {
                    ir.close();
                } catch (IOException ioe)
                {
                }
            }
            if (is != null)
            {
                try {
                    is.close();
                } catch (IOException ioe)
                {
                }
            }
        }
        Tweet tweet = new Tweet();        
        tweet.setContent(contentBuffer.toString());
        if (charset != null)
        {
            tweet.setCharset(charset);
        }
        return tweet;
    }

    
    /**
     * Set the content of the tweet. Setting this "resets" the tweet
     * formatting -<code>addArgument</code> will start.
     * @param cnt
     *            the content of the message
     */
    public void setContent(String cnt)
    {
        content = cnt;
        arguments = new ArrayList<Object>();
    }

    /**
     * Fill out the next argument in the template
     *
     * @param arg
     *            the value for the next argument
     */
    public void addArgument(Object arg)
    {
        arguments.add(arg);
    }
	
    public void setCharset(String cs)
    {
        charset = cs;
    }
	
    /**
     * "Reset" the message. Clears the arguments and recipients, but leaves the
     * subject and content intact.
     */
    public void reset()
    {
        arguments = new ArrayList<Object>(50);
        charset = null;
    }
    
    
    public void send(Twitter twitter) throws MessagingException, IOException, TwitterException
    {    	
        // If no character set specified, attempt to retrieve a default
        if (charset == null)
        {
            charset = ConfigurationManager.getProperty("mail.charset");
        }
        // Format the mail message
        Object[] args = arguments.toArray();
        String fullMessage = MessageFormat.format(content, args);    	
        twitter.updateStatus(fullMessage);
    }
    
	
}

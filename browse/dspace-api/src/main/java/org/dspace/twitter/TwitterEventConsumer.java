/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.twitter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.dspace.content.Collection;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.Metadatum;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.I18nUtil;
import org.dspace.event.Consumer;
import org.dspace.event.Event;
import org.dspace.handle.HandleManager;

import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterEventConsumer implements Consumer {
	
	private static Logger log = Logger.getLogger(TwitterEventConsumer.class);
	
	private static final String DOI_PREFIX="doi.org";
	
	private static final String TWITTER_OAUTH_CONSUMER_KEY = "twitter.consumer.key";
	private static final String TWITTER_OAUTH_CONSUMER_SECRET = "twitter.consumer.secret";
	private static final String TWITTER_OAUTH_ACCESS_TOKEN = "twitter.access.token";
	private static final String TWITTER_OAUTH_ACCESS_TOKEN_SECRET = "twitter.access.token.secret";
	
    private static final String PAPER_TITLE = "Original title:";
    private static final String PAPER_DOI = "DOI:";
    private static final String PAPER_JOURNAL = "Journal:";

    private static final String PUBLISHING_METADATA_REGEX = ".*(" + PAPER_TITLE + "|"+ PAPER_DOI + "|" + PAPER_JOURNAL + ").*";
    	
	
	private static TwitterFactory tf = null;
	
	@Override
	public void initialize() throws Exception {
		if(tf != null)
			return;
		
		if(ConfigurationManager.getProperty(TWITTER_OAUTH_CONSUMER_KEY) == null 
				|| ConfigurationManager.getProperty(TWITTER_OAUTH_CONSUMER_SECRET) == null
				|| ConfigurationManager.getProperty(TWITTER_OAUTH_ACCESS_TOKEN) == null
				|| ConfigurationManager.getProperty(TWITTER_OAUTH_ACCESS_TOKEN_SECRET) == null) {			
			log.info("TwitterEventConsumer not configured, will be disabled");
			tf = null;
			return;
		}
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true);
		cb.setOAuthConsumerKey(ConfigurationManager.getProperty(TWITTER_OAUTH_CONSUMER_KEY));
		cb.setOAuthConsumerSecret(ConfigurationManager.getProperty(TWITTER_OAUTH_CONSUMER_SECRET));
		cb.setOAuthAccessToken(ConfigurationManager.getProperty(TWITTER_OAUTH_ACCESS_TOKEN));
		cb.setOAuthAccessTokenSecret(ConfigurationManager.getProperty(TWITTER_OAUTH_ACCESS_TOKEN_SECRET));
		tf = new TwitterFactory(cb.build());		
	}
	
	@Override
	public void consume(Context ctx, Event event) throws Exception {
		try {
			if(tf != null) {
				if(isAddCollection(event)){
					//sendTweet("addcollection", ctx, event);
				}else if(isUnembargoCollectionContent(event)) {
					sendTweet("collectionUnembargoed", ctx, event);				
				}
			}	
		}catch(Exception e) {
			log.error(e.getMessage());
		}		
	}

	private boolean isAddCollection(Event event){
		if(event.getEventType() == Event.ADD &&
		   event.getSubjectType() == Constants.SITE &&
		   event.getObjectType() == Constants.COLLECTION)
			return true;					
		return false;
	}
	
	private boolean isUnembargoCollectionContent(Event event){
		if(event.getEventType() == Event.ADD &&
		   event.getSubjectType() == Constants.SITE &&		   				
		   event.getObjectType() == Constants.ITEM)
			return true;					
		return false;		
	}
	
	private void sendTweet(String template, Context ctx, Event event) throws Exception{
		Tweet tweet = buildCollectionTweet(template, ctx, event.getObjectID());
		if(tweet != null)
			tweet.send(tf.getInstance());
	}

	private Tweet buildCollectionTweet(String template, Context ctx, int collectionId) throws IOException {
		Tweet tweet = Tweet.getTweet(I18nUtil.getTweetFilename(Locale.getDefault(), template));
		Collection collection;		
		String doi = null;
		String author = null;
		String article = null;
		try {
			collection = Collection.find(ctx, collectionId);
			String name = collection.getName();
			String handle = HandleManager.resolveToURL(ctx, collection.getHandle());
			collection.getMetadata(Item.ANY, Item.ANY, Item.ANY, Item.ANY);
			for(Metadatum metadatum : collection.getMetadata("dc", "identifier", "uri", Item.ANY))
				if(metadatum.value.contains(DOI_PREFIX))
					doi = metadatum.value;
			author = getAuthor(ctx, collection);
			article = getArticleInfo(ctx, collection);
			tweet.addArgument(name);
			tweet.addArgument(handle);
			tweet.addArgument(author != null? author: "");
			tweet.addArgument(doi!= null? doi: "");
			tweet.addArgument(article != null? article: "");
		} catch (SQLException e) {
			log.error(e.getMessage());
			return null;
		}
		return tweet;
	}
	
	private String getAuthor(Context ctx, Collection collection) {
		try {
			ItemIterator iter = collection.getItems(1, 0);
			if(iter.hasNext()) {
				return iter.next().getMetadata("dc.contributor.author");
			}	
		}catch(Exception e) {		
		}
		return null;
	}

	private String getArticleInfo(Context ctx, Collection collection) {
		try {
			ItemIterator iter = collection.getItems(1, 0);
			if(iter.hasNext()) {
				String field = iter.next().getMetadata("dc.relation");
				if(field.matches(PUBLISHING_METADATA_REGEX))
					return field;
			}	
		}catch(Exception e) {		
		}
		return null;
	}

	
	@Override
	public void end(Context ctx) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finish(Context ctx) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	

}

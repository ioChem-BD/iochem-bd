/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.content.crosswalk;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.DSpaceObject;
import org.dspace.content.Item;
import org.dspace.core.Context;
import org.jdom.Element;

public class CMLCrosswalk implements IngestionCrosswalk
{
	
	/**
     * This procedure process all metadata associated with CML schema
     * We also have generated all cml elements and qualified elements
     * inside DSpace's metadatafieldregistry table (using admin UI), so
     * there will be no translation, only direct assignation to metadata
     * field values.  
     * @author Moises Alvarez
	 * @version $Revision: 1.0 $
	 */
    private static Logger log = Logger.getLogger(CMLCrosswalk.class);
    
    @Override
    public void ingest(Context context, DSpaceObject dso, List<Element> metadata)
            throws CrosswalkException, IOException, SQLException,
            AuthorizeException
    {  
    	Item item = (Item)dso;   
        //Save all cml metadata values 
        for (Element field : metadata){
            String schema = field.getNamespacePrefix();
            //First of all we'll check that we're not uploading an output that already exists           
            if(!field.getChildren().isEmpty()){
                Iterator<Element> itChildren = field.getChildren().iterator();
                while(itChildren.hasNext()){
                    Element subfield = itChildren.next();
                    item.addMetadata(schema, field.getName(),subfield.getName(), "en", subfield.getText());
                } 
            }
            else
                item.addMetadata(schema, field.getName(),null, "en", field.getText());
        }
    }
       
    @Override
    public void ingest(Context context, DSpaceObject dso, Element root)
            throws CrosswalkException, IOException, SQLException,
            AuthorizeException
    {
        ingest(context, dso, root.getChildren());
    }
    

}

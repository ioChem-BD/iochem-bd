/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.ctask.fix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.Metadatum;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class V5_0_2017_10_09__IO_0003 {
	
    private static final Logger log = LoggerFactory.getLogger(V5_0_2017_10_09__IO_0003.class);
	
    private static Pattern formulaNoMiddleOnes = Pattern.compile("([A-Za-z]+)1([A-Za-z]+)");
	private static Pattern formulaNoEndingOne = Pattern.compile("([A-Za-z]+)1$");

	public static void main(String[] args) throws FileNotFoundException, IOException, SQLException {
		fixStoichiometry();
		regenerateOAI();
	}

	/*
	 * Fix stoichiometry related functions 
	 */
	private static int fixStoichiometry() throws SQLException{
		Context context = new Context();
		ItemIterator items = null;
		int processedItems = 0;		
		context.turnOffAuthorisationSystem();
		try {			
			items = Item.findAllUnfiltered(context); 			
			while(items.hasNext())
			{				
				Item item = items.next();
				System.out.println(processedItems + " : Fix stoichiometry processing item with handle " + item.getHandle());
				Metadatum[] metadatas = item.getMetadata("cml", "formula", "generic", Item.ANY);
				for(Metadatum metadata : metadatas){
					String formula = metadata.value;
					formula = formula.replaceAll("\\s+", "");					
					Metadatum updatedMetadata = metadata.copy();
			
					Matcher m1 = formulaNoMiddleOnes.matcher(formula);
					if(m1.find()){
						formula = m1.replaceAll("$1$2");			
					}
					
					Matcher m2 = formulaNoEndingOne.matcher(formula);
					if(m2.find()){
						formula = m2.replaceAll("$1");
					}
					
					updatedMetadata.value = formula;					
					item.replaceMetadataValue(metadata, updatedMetadata);	
					item.update();
				}
				processedItems++;
				if(processedItems % 1000 == 0){
					System.out.println(V5_0_2017_10_09__IO_0003.class.toString() + ": Processed " + processedItems +" items");
					context.commit();
				}
			}
			context.commit();
		}catch(Exception e){
			System.out.println(e.getMessage());
			context.abort();
		} finally {
			context.restoreAuthSystemState();
			if (items != null){
              items.close();
			}		
		}
		return processedItems;
	}

	private static int regenerateOAI() throws FileNotFoundException, IOException{
		System.out.println("Regenerating OAI indexes");
		String pathToDspaceBin = ConfigurationManager.getProperty("dspace.dir") + File.separatorChar + "bin" + File.separatorChar + "dspace";
		String pathToBinFolder =  ConfigurationManager.getProperty("dspace.dir") + File.separatorChar + "bin";
		runShellCommand(new String[]{pathToDspaceBin, "oai", "clean-cache"}, pathToBinFolder);
		runShellCommand(new String[]{pathToDspaceBin, "oai", "import", "-c", "-o"}, pathToBinFolder);
		return 0;				
	}

	private static void runShellCommand(String args[], String folder)  {	      
		try{
		    Process child = Runtime.getRuntime().exec(args, null, new File(folder));
		    InputStream is = child.getInputStream();
		    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    String aux = br.readLine();			                 
			while (aux != null) {			                  
			    aux = br.readLine();
			}
		}
		catch (Exception e)
		{
			System.out.println("Error executing command." + args  + e.getMessage());
		}   			
	}

}

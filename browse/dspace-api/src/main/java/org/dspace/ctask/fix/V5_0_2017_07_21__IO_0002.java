/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.ctask.fix;

import java.sql.SQLException;

import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.authorize.ResourcePolicy;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.DCDate;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.MetadataValue;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.embargo.EmbargoManager;
import org.dspace.eperson.Group;
import org.dspace.license.CreativeCommons;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class V5_0_2017_07_21__IO_0002  {
		
    private static final Logger log = LoggerFactory.getLogger(V5_0_2017_07_21__IO_0002.class);

    public static final String EXCLUDED_BUNDLES_FROM_EMBARGO_REGEX = "(" + Constants.LICENSE_BUNDLE_NAME  + "|" + Constants.METADATA_BUNDLE_NAME + "|" + CreativeCommons.CC_BUNDLE_NAME + "|TEXT|THUMBNAIL)";        
    
    
	public static void main(String[] args)  {
		try {
			fixEmbargoRules();
		} catch (SQLException e) {
			System.out.println("Error applying patch, cause:" + e.getMessage());
		}
	}
	
	
	private static void fixEmbargoRules() throws SQLException{
		Context context = new Context();
		ItemIterator items = null;
		context.turnOffAuthorisationSystem();		
		try{
			items = Item.findAllUnfiltered(context); 			
			while(items.hasNext()){		
				boolean hasEmbargo = false;
				Item item = items.next();
				DCDate liftDate = EmbargoManager.getEmbargoTermsAsDate(context, item);
				if(liftDate == null)
					continue;
				for(ResourcePolicy rp : AuthorizeManager.getPolicies(context, item)){
					if(rp.getStartDate() != null && rp.getEndDate() == null){
						hasEmbargo = true;
						setEmbargoOnBitstreams(context, item, liftDate);
			        	System.out.println("Extending embargo rules at bitstream level on item with handle:" + item.getHandle());
					}															
				}
				//Clean buggy embargo lift
		        if(liftDate != null && !hasEmbargo){
		        	item.clearMetadata("dc", "embargo", "terms", Item.ANY);
		        	item.update();
		        }
			}			
			context.commit();
		}catch(Exception e){
			System.out.println(e.getMessage());
			context.abort();
		} finally {
			context.restoreAuthSystemState();
			if (items != null){
              items.close();
			}		
		}
		
		
	}
	
	

    private static void setEmbargoOnBitstreams(Context context, Item item, DCDate liftDate) throws SQLException, AuthorizeException{    	
    	for(Bundle bundle: item.getBundles()){    		
            if (!bundle.getName().matches(EXCLUDED_BUNDLES_FROM_EMBARGO_REGEX)){    	
	    		for(Bitstream bitstream : bundle.getBitstreams()){
	    			ResourcePolicy rp = null;
	    			rp=AuthorizeManager.createOrModifyPolicy(null, context, "Embargoed element", Group.ANONYMOUS_ID, null, liftDate.toDate(), org.dspace.core.Constants.READ, "Embargoed element under review by external parties", bitstream);
	    			rp.update();
	    		}
            }
    	}
    }    
    
    
	
	

}

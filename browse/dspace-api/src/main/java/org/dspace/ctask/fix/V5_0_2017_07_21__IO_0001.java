/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.ctask.fix;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.io.FilenameUtils;
import org.dspace.app.launcher.ScriptLauncher;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.Metadatum;
import org.dspace.content.service.ItemService;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Context;
import org.dspace.servicemanager.DSpaceKernelImpl;
import org.dspace.servicemanager.DSpaceKernelInit;
import org.flywaydb.core.api.migration.MigrationChecksumProvider;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class V5_0_2017_07_21__IO_0001  {
	
	private static Templates fixTemplate = null;
    private static final String SELECT_VASP_CALCULATIONS_ON_CREATE = "select id from calculations where type_id=3";
    private static final String FIX_VASP_MAGNETIZATION_CML = "vasp_fix_magnetization_IO_0002.xsl";
	
	@Autowired(required = true)
	protected ItemService itemService;

    /** logging category */
    private static final Logger log = LoggerFactory.getLogger(V5_0_2017_07_21__IO_0001.class);

    private static Pattern formulaNoMiddleOnes = Pattern.compile("([A-Za-z]+)1([A-Za-z]+)");
	private static Pattern formulaNoEndingOne = Pattern.compile("([A-Za-z]+)1$");
    
    /* The checksum to report for this migration (when successful) */
    private int checksum = -1;

	public static void main(String[] args) throws SQLException, FileNotFoundException, IOException, AuthorizeException, TransformerException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		//First process: fix stoichiometry field
		fixStoichiometry();
		regenerateOAI();
		//Second part: fix VASP calculations
		loadFixTemplate();	
		fixVaspMagnetizationOnCreate();
		fixVaspMagnetizationOnBrowse();						
	}
	
	
	
	
	/*
	 * Fix stoichiometry related functions 
	 */
	

	private static int fixStoichiometry() throws SQLException{
		Context context = new Context();
		ItemIterator items = null;
		int processedItems = 0;		
		context.turnOffAuthorisationSystem();
		try {			
			items = Item.findAllUnfiltered(context); 			
			while(items.hasNext())
			{				
				Item item = items.next();
				System.out.println(processedItems + " : Fix stoichiometry processing item with handle " + item.getHandle());
				Metadatum[] metadatas = item.getMetadata("cml", "formula", "generic", Item.ANY);
				for(Metadatum metadata : metadatas){
					String formula = metadata.value;
					formula = formula.replaceAll("\\s+", "");					
					Metadatum updatedMetadata = metadata.copy();
			
					Matcher m1 = formulaNoMiddleOnes.matcher(formula);
					if(m1.find()){
						formula = m1.replaceAll("$1$2");			
					}
					
					Matcher m2 = formulaNoEndingOne.matcher(formula);
					if(m2.find()){
						formula = m2.replaceAll("$1");
					}
					
					updatedMetadata.value = formula;					
					item.replaceMetadataValue(metadata, updatedMetadata);	
					item.update();
				}
				processedItems++;
			}
			context.commit();
		}catch(Exception e){
			System.out.println(e.getMessage());
			context.abort();
		} finally {
			context.restoreAuthSystemState();
			if (items != null){
              items.close();
			}		
		}
		return processedItems;

	}
	
	private static int regenerateOAI() throws FileNotFoundException, IOException{
		System.out.println("Regenerating OAI indexes");
		String pathToDspaceBin = ConfigurationManager.getProperty("dspace.dir") + File.separatorChar + "bin" + File.separatorChar + "dspace";
		String pathToBinFolder =  ConfigurationManager.getProperty("dspace.dir") + File.separatorChar + "bin";
		runShellCommand(new String[]{pathToDspaceBin, "oai", "clean-cache"}, pathToBinFolder);
		runShellCommand(new String[]{pathToDspaceBin, "oai", "import", "-c", "-o"}, pathToBinFolder);
		return 0;				
	}
	
	private static void runShellCommand(String args[], String folder)  {	      
		try{
		    Process child = Runtime.getRuntime().exec(args, null, new File(folder));
		    InputStream is = child.getInputStream();
		    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    String aux = br.readLine();			                 
			while (aux != null) {			                  
			    aux = br.readLine();
			}
		}
		catch (Exception e)
		{
			System.out.println("Error executing command." + args  + e.getMessage());
		}   			
	}
	
	/*
	 * Fix VASP calculations related functions 
	 */
	
	private static void loadFixTemplate(){
    	try {
    	    TransformerFactory tFactory = new net.sf.saxon.TransformerFactoryImpl();   		
    		fixTemplate = tFactory.newTemplates(new StreamSource(org.dspace.storage.rdbms.migration.V5_0_2017_07_21__IO_0001.class.getResourceAsStream(FIX_VASP_MAGNETIZATION_CML)));    		
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	}
	
	private static int fixVaspMagnetizationOnCreate() throws FileNotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, SQLException, TransformerException{
		String createAssetstore = ConfigurationManager.getProperty("dspace.dir") + "/../create/assetstore/";
		Connection connection = getCreateConnection();
		PreparedStatement ps = connection.prepareStatement(SELECT_VASP_CALCULATIONS_ON_CREATE);
		ResultSet ids = ps.executeQuery();
		int inx = 0;
		while(ids.next()){
			int id = ids.getInt(1);			
			fixCreateCalculationById(createAssetstore, id);
			inx++;
		}
		return inx;		
	}
	
	private static void fixCreateCalculationById(String assetstore, int id) throws TransformerException, IOException{		
		String outputFile = assetstore + id + File.separatorChar + "output.cml";
		FileInputStream fis = new FileInputStream(new File(outputFile));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();		
		fixVaspCalculation(fis, baos);
		baos.flush();
		FileOutputStream fos = new FileOutputStream(new File(outputFile));
		fos.write(baos.toByteArray());
		fos.flush();
		fos.close();
		baos.flush();
		System.out.println("Fixed Create VASP calculation with id : " + id);
	}
	
	private static void fixVaspCalculation(InputStream is, OutputStream os) throws TransformerException, IOException{		
		Transformer transformer = fixTemplate.newTransformer();
		StreamSource source = new StreamSource(is);
		StreamResult result = new StreamResult(os);
		transformer.transform(source, result);	
		is.close();
		os.flush();
		os.close();
	}
	
	private static int fixVaspMagnetizationOnBrowse() throws SQLException, AuthorizeException, IOException, TransformerException{
		Context context = new Context();			
		context.turnOffAuthorisationSystem();
		int inx = 0;
		try {
			ItemIterator iterator = Item.findByMetadataField(context, "cml", "program", "name", "vasp");			
			while(iterator.hasNext()){
				Item item = iterator.next();
				Bitstream bitstream = getItemBitstreamByName(item, "output.cml");			
				InputStream is = bitstream.retrieve();		
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				fixVaspCalculation(is, baos);
				buildNewBitstream(item, bitstream, baos);
				System.out.println("Fixed Browse VASP calculation with handle : " + item.getHandle());
				inx++;
			}
			context.commit();
		}catch(Exception e){
			System.out.println(e.getMessage());
			context.abort();
		} finally {
			context.restoreAuthSystemState();		
		}
		return inx;
	}
	
	private static void buildNewBitstream(Item item, Bitstream originalBitstream, ByteArrayOutputStream baos) throws SQLException, AuthorizeException, IOException {
		for(Bundle bundle : item.getBundles()){
			if(bundle.getName().equals("ORIGINAL")){
				Bitstream newBitstream = bundle.createBitstream(new ByteArrayInputStream(baos.toByteArray()));
				newBitstream.setName(originalBitstream.getName());
				newBitstream.setDescription(originalBitstream.getDescription());
				newBitstream.setSequenceID(originalBitstream.getSequenceID());
				newBitstream.setUserFormatDescription(originalBitstream.getUserFormatDescription());
				newBitstream.setFormat(originalBitstream.getFormat());
				for(Metadatum metadata : originalBitstream.getMetadata(Item.ANY, Item.ANY, Item.ANY, Item.ANY, Item.ANY))
					newBitstream.addMetadata(metadata.schema, metadata.element, metadata.qualifier, metadata.language, metadata.value);
				newBitstream.update();
				bundle.addBitstream(newBitstream);
				bundle.removeBitstream(originalBitstream);
				bundle.update();
			}
		}
		item.update();
	}

	private static Bitstream getItemBitstreamByName(Item item, String bsName) throws SQLException {
		Bundle[] bundles = item.getBundles();
        for (int i = 0; i < bundles.length; i++) {
            Bitstream[] bitstreams = bundles[i].getBitstreams();
            for (int k = 0; k < bitstreams.length; k++) {
                if (bsName.equals(bitstreams[k].getName())) {
                    return bitstreams[k];
                }
            }
        }
        return null;
    }
	
    protected static Connection getCreateConnection() throws FileNotFoundException, IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{    	     
    	Properties createProperties = new Properties();
    	String createPropsFilePath = ConfigurationManager.getProperty("dspace.dir") + "/../webapps/create/WEB-INF/classes/resources.properties";
    	createProperties.load(new FileInputStream(new File(createPropsFilePath)));
    	
    	String host = createProperties.getProperty("create.database.host").trim();    	
    	String port = createProperties.getProperty("create.database.port").trim();
    	String dbname = createProperties.getProperty("create.database.name").trim();
    	
    	String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbname;
    	
    	Class driverClass = Class.forName(ConfigurationManager.getProperty("db.driver"));    	
        Driver basicDriver = (Driver) driverClass.newInstance();
        DriverManager.registerDriver(basicDriver);

        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
                url,
                ConfigurationManager.getProperty("db.username"),
                ConfigurationManager.getProperty("db.password"));
    	        
        return connectionFactory.createConnection();
    	
    }
}

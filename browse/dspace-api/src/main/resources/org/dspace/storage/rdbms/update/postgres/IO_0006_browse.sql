-- Append new mimetypes

SELECT setval('bitstreamformatregistry_seq', max(bitstream_format_id)) FROM bitstreamformatregistry;
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-lammps-input', 'LAMMPS input', 'LAMMPS input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-lammps-datafile', 'LAMMPS data file', 'LAMMPS data file with the cell and atom definitions', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-lammps-trajectory', 'LAMMPS trajectory', 'LAMMPS trajectory files, compressed or not.', 1, false);
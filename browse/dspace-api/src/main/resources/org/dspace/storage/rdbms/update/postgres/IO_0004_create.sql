-- Insert Amber type 
SELECT setval('calculation_types_id_seq', max(id)) FROM calculation_types;
INSERT INTO calculation_types VALUES (nextval('calculation_types_id_seq'), '2021-11-16 11:45:00.000000', 'AMB', 'Amber', 'Amber Type', '/html/xsltmd/amber.xsl');

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '.*.in', '-', 'Amber - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_input', 'amber_input', 'chemical/x-amber-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-o', '.*.log', '-', 'Amber - output file', 'org.xmlcml.cml.converters.compchem.amber.log.AmberLog2CompchemConverter', 'amber_log', 'amber_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-p', '.*.prmtop', '-', 'Amber - topology file', 'org.xmlcml.cml.converters.compchem.amber.topology.AmberTopology2XMLConverter', 'amber_topology', 'amber_topology_xml', 'chemical/x-cml', 'append', 'Topology file .prmtop(3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-p', '.*.prmtop', '-', 'Amber - topology file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_topology', 'amber_topology', 'chemical/x-amber-topology', 'input', 'Topology file .prmtop(3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-r', '.*.ncrst', '-', 'Amber - NetCDF Restart', 'org.xmlcml.cml.converters.compchem.amber.trajectory.AmberTrajectory2XMLConverter', 'amber_trajectory', 'amber_trajectory_xml', 'chemical/x-cml', 'append', 'NetCDF restart file .ncrst(4*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-t', '.*.nc', '-', 'Amber - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_trajectory', 'amber_trajectory', 'chemical/x-amber-trajectory', 'input', 'NetCDF trajectory file .nc (5*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-a', '.*.*', '-', 'Amber - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(6)', 'repeatable');

SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-amber-input', 'amber_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-amber-input', 'amber_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlAMBER.xsl -extension .html');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-amber-topology', 'amber_topology', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-amber-topology', 'amber_topology', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-amber-trajectory', 'amber_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

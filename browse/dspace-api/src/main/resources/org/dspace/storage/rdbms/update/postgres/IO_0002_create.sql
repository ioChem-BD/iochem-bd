UPDATE actions SET parameters = '-template  html/xslt/cml2htmlQUANTUMESPRESSO.xsl -extension .html' where parameters = '-template  html/xslt/cml2htmlQESPRESSO.xsl -extension .html';
SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-as', '.*.dat|*.*', '-', 'QuantumEspresso - absorption spectra file', 'org.xmlcml.cml.converters.compchem.qespresso.spectra.QEspressoSpectra2XMLConverter', 'qespresso_spectra', 'qespresso_spectra_xml', 'chemical/x-cml', 'append', 'Absorption spectra data file(4)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-as', '.*.dat|*.*', '-', 'QuantumEspresso - absorption spectra file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_spectra', 'qespresso_spectra', 'chemical/x-qespresso-spectra', 'input', 'Absorption spectra data file(4)', NULL);
SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-spectra', 'qespresso_spectra', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-spectra', 'qespresso_spectra', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');


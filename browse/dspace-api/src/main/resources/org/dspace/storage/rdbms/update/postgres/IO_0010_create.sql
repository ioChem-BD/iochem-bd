-- Fix possible missing QuantumEspresso spectra file upload definition

UPDATE actions SET parameters = '-template  html/xslt/cml2htmlQUANTUMESPRESSO.xsl -extension .html' where parameters = '-template  html/xslt/cml2htmlQESPRESSO.xsl -extension .html';

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;

DROP INDEX IF EXISTS idx_t_id_a;
DROP INDEX IF EXISTS idx_t_id_b;

INSERT INTO cutting_area_definitions
	VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-as', '.*.dat|*.*', '-', 'QuantumEspresso - absorption spectra file', 'org.xmlcml.cml.converters.compchem.qespresso.spectra.QEspressoSpectra2XMLConverter', 'qespresso_spectra', 'qespresso_spectra_xml', 'chemical/x-cml', 'append', 'Absorption spectra data file(4)', NULL, NULL, NULL);
		
		
INSERT INTO cutting_area_definitions 
	VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-as', '.*.dat|*.*', '-', 'QuantumEspresso - absorption spectra file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_spectra', 'qespresso_spectra', 'chemical/x-qespresso-spectra', 'input', 'Absorption spectra data file(4)', NULL, NULL, NULL);		
		

SELECT setval('actions_id_seq', max(id)) FROM actions;

INSERT INTO actions
	VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-spectra', 'qespresso_spectra', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');

INSERT INTO actions
	VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-spectra', 'qespresso_spectra', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
		
	


-- Increase mimetype column size on cutting_area_definitions and actions tables
ALTER TABLE cutting_area_definitions ALTER COLUMN mimetype TYPE character varying(64);
ALTER TABLE actions ALTER COLUMN mimetype TYPE character varying(64);

-- Update QuantumEspresso with bands and pdos data capture 
UPDATE cutting_area_definitions SET behaviour = 'repeatable' WHERE id IN 
        (SELECT id FROM cutting_area_definitions WHERE calculation_type_id IN
            (SELECT id FROM calculation_types WHERE abr = 'QEX') AND use = 'output');
            
SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types WHERE abr = 'QEX'), true, '-b', '*.out|*.*', '-', 'QuantumEspresso - bands file', 'org.xmlcml.cml.converters.compchem.qespresso.bands.QEspressoBands2XMLConverter', 'qespresso_bands', 'qespresso_bands_xml', 'chemical/x-cml', 'append', 'Bands file (5)', 'repeatable', NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types WHERE abr = 'QEX'), true, '-b', '*.out|*.*', '-', 'QuantumEspresso - bands file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_bands', 'qespresso_bands', 'chemical/x-qespresso-bands', 'input', 'Bands file (5)', 'repeatable', NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-dos', '.*.pdos|*.*', '-', 'QuantumEspresso - pdos file', 'org.xmlcml.cml.converters.compchem.qespresso.pdos.QEspressoPDOS2XMLConverter', 'qespresso_pdos', 'qespresso_pdos_xml', 'chemical/x-cml', 'append', 'Projected DOS data file(6)', 'repeatable', NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-dos', '.*.pdos|*.*', '-', 'QuantumEspresso - pdos file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_pdos', 'qespresso_pdos', 'chemical/x-qespresso-pdos', 'input', 'Projected DOS data file(6)', 'repeatable', NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-pi', '.*.pwi|*.*', '-', 'QuantumEspresso - phonom input file', 'org.xmlcml.cml.converters.compchem.qespresso.phonon.QEspressoPhononInput2XMLConverter', 'qespresso_phonon_input', 'qespresso_phonon_input_xml', 'chemical/x-cml', 'append', 'Phonon input file(7)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-pi', '.*.pwi|*.*', '-', 'QuantumEspresso - phonom input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_phonon_input', 'qespresso_phonon_input', 'chemical/x-qespresso-phonon-input', 'input', 'Phonon input file(7)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-po', '.*.freq|*.*', '-', 'QuantumEspresso - phonom output file', 'org.xmlcml.cml.converters.compchem.qespresso.phonon.QEspressoPhononOutput2XMLConverter', 'qespresso_phonon_output', 'qespresso_phonon_output_xml', 'chemical/x-cml', 'append', 'Phonon data file(8)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), (SELECT id FROM calculation_types where abr = 'QEX'), true, '-po', '.*.freq|*.*', '-', 'QuantumEspresso - phonom output file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_phonon_output', 'qespresso_phonon_output', 'chemical/x-qespresso-phonon-output', 'input', 'Phonon data file(8)', NULL, NULL, NULL);

INSERT INTO actions	VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-bands', 'qespresso_bands', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-bands', 'qespresso_bands', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions	VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-pdos', 'qespresso_pdos', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-pdos', 'qespresso_pdos', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions	VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-phonon-input', 'qespresso_phonon_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-phonon-input', 'qespresso_phonon_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions	VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-phonon-output', 'qespresso_phonon_output', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-qespresso-phonon-output', 'qespresso_phonon_output', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');


DELETE from cutting_area_definitions a
	USING  cutting_area_definitions b
	WHERE a.id > b.id 
	AND a.calculation_type_id = b.calculation_type_id 
	AND a.description = b.description 
	AND a.use = b.use;
	
DELETE from actions a
	USING  actions b
	WHERE a.id > b.id 
	AND a.jumbo_format = b.jumbo_format 
	AND a.action = b.action 
	AND a.mimetype = b.mimetype;
	



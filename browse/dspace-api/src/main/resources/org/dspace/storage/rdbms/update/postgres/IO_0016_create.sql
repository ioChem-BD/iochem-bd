-- Add unique restriction on Report calculation entries.
ALTER TABLE IF EXISTS public.report_calculations ADD CONSTRAINT unique_report_calculation UNIQUE (report_id, calc_id);
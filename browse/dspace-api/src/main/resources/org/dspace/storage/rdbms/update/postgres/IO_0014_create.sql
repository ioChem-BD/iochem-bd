-- Insert new Reaction Graph report type
SELECT setval('report_type_id_seq', max(id)) FROM report_type;
INSERT INTO report_type(name, output_type, associated_xslt, associated_zul, enabled) VALUES ('Reaction Graph','', '/html/reports/ReportReactionGraph/reactionGraph.xsl', 'reportReactionGraph.zul', true);

-- Table: Molecular fingerprints for each calculation
CREATE TABLE public.molecular_fingerprints
(
    id bigint NOT NULL,
    calculation_id bigint NOT NULL,
    feature integer[] NOT NULL,
    connectivity_feature integer[] NOT NULL,
    atom_type text[] COLLATE pg_catalog."default" NOT NULL,
    canonical_smiles character varying COLLATE pg_catalog."default" NOT NULL,
    connectivity_smiles character varying COLLATE pg_catalog."default" NOT NULL,
    inchi character varying COLLATE pg_catalog."default" NOT NULL,
    inchi_key character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT molecular_fingerprints_pkey PRIMARY KEY (id),
    CONSTRAINT molecular_fingerprints_calc_id_fk FOREIGN KEY (calculation_id)
       REFERENCES calculations (id) MATCH SIMPLE
       ON UPDATE CASCADE
       ON DELETE CASCADE
);

ALTER TABLE public.molecular_fingerprints
    OWNER to iochembd;
    
-- Index: calc_id_molecular_fingerprint_index
CREATE INDEX calc_id_molecular_fingerprint_index
    ON public.molecular_fingerprints USING btree
    (calculation_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: id_molecular_fingerprint_index
CREATE INDEX id_molecular_fingerprint_index
    ON public.molecular_fingerprints USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: inchi_key_molecular_fingerprint_index
CREATE INDEX inchi_key_molecular_fingerprint_index
    ON public.molecular_fingerprints USING btree
	(inchi_key ASC NULLS LAST)
    TABLESPACE pg_default;

ALTER TABLE projects ALTER COLUMN element_order TYPE INT; 
ALTER TABLE calculations ALTER COLUMN element_order TYPE INT;

ALTER TABLE public.areas_file_ref
    DROP CONSTRAINT areas_file_ref_calculation_id_fkey, -- Drop the existing constraint
    ADD CONSTRAINT areas_file_ref_calculation_id_fkey
    FOREIGN KEY (calculation_id)
    REFERENCES public.calculations (id)
    MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

-- Update report names
UPDATE report_type SET name = 'Reaction Energy Profile' WHERE name = 'Reaction energy profile';
UPDATE report_type SET name = 'Supporting Information' WHERE name = 'Supporting information';


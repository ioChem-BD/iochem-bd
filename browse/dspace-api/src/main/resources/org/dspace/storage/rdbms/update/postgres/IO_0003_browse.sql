-- Append new mimetypes

SELECT setval('bitstreamformatregistry_seq', max(bitstream_format_id)) FROM bitstreamformatregistry;
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-gronor-input', 'GronOR input', 'GronOR input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-gromacs-input', 'GROMACS input', 'GROMACS input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-gromacs-trajectory', 'GROMACS trajectory', 'GROMACS binary trajectory file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-gromos87', 'Gromos87 geometry', 'Geometry file in Gromos87 format', 1, false);
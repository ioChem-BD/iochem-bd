-- Insert GRRM type 
SELECT setval('calculation_types_id_seq', max(id)) FROM calculation_types;
INSERT INTO calculation_types VALUES (nextval('calculation_types_id_seq'), '2023-05-16 12:37:33.854403', 'GRRM', 'GRRM', 'GRRM Type', '/html/xsltmd/grrm.xsl');

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO public.cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '*.*', '-', 'GRRM - input file', 'org.xmlcml.cml.converters.compchem.grrm.input.GrrmInput2XMLConverter', 'grrm_input', 'grrm_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '*.*', '-', 'GRRM - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'grrm_input', 'grrm_input', 'chemical/x-grrm-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), false, '-a', '*.*', '-', 'GRRM - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', ' ', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-o', '*.*', '-', 'GRRM - output file', 'org.xmlcml.cml.converters.compchem.grrm.log.GrrmLog2CompchemConverter', 'grrm_output', 'grrm_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);

SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO public.actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGRRM.xsl -extension .html');
INSERT INTO public.actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (nextval('actions_id_seq'), 'chemical/x-grrm-input', 'grrm_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (nextval('actions_id_seq'), 'chemical/x-grrm-input', 'grrm_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

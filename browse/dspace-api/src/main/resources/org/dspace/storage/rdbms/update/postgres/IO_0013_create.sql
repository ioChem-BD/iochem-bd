-- Updates to fit the new Hibernate library structure 
CREATE SEQUENCE public.calculations_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER SEQUENCE public.calculations_id_seq OWNER TO iochembd;

SELECT setval('projects_id_seq', max(id)) FROM projects;
SELECT setval('calculations_id_seq', max(id)) FROM calculations;

-- Alter the column type
DROP INDEX id_action_index;
ALTER TABLE actions ALTER COLUMN id TYPE int;
CREATE INDEX id_action_index ON actions (id);



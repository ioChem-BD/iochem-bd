-- Insert Amber type 

-- Update labels to set space for the new file type
UPDATE cutting_area_definitions SET label = 'Final restart file .ncrst(5*)' where label = 'NetCDF restart file .ncrst(4*)';
UPDATE cutting_area_definitions SET label = 'Trajectory file .nc (6*)' where label = 'NetCDF trajectory file .nc (5*)';
UPDATE cutting_area_definitions SET label = 'Additional file(7)' where label = 'Additional file(6)' and calculation_type_id in (SELECT id from calculation_types where abr = 'AMB');

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions(id, calculation_type_id, default_type_sel, abbreviation, file_name, url, description, jumbo_converter_class, jumbo_converter_in_type, jumbo_converter_out_type, mimetype, use, label, behaviour) SELECT nextval('cutting_area_definitions_id_seq'), id, true, '-ir', '.*.inpcrd', '-', 'Amber - Input coordinates or initial .ncrst file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Input coords .inpcrd .ncrst(4*)', NULL  from calculation_types where abr = 'AMB';
INSERT INTO cutting_area_definitions(id, calculation_type_id, default_type_sel, abbreviation, file_name, url, description, jumbo_converter_class, jumbo_converter_in_type, jumbo_converter_out_type, mimetype, use, label, behaviour) SELECT nextval('cutting_area_definitions_id_seq'), id, true, '-r', '.*.ncrst', '-', 'Amber - NetCDF Restart', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_trajectory', 'amber_trajectory', 'chemical/x-amber-coordinates', 'additional', 'Final restart file .ncrst(5*)', NULL from calculation_types where abr = 'AMB';
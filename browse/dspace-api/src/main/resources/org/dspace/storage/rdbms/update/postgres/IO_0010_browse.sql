-- Fix missing mimetypes due to bad patch IO_0002_browse.sql

SELECT setval('bitstreamformatregistry_seq', max(bitstream_format_id)) FROM bitstreamformatregistry;


DROP INDEX IF EXISTS idx_t_id_c;


INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-qespresso-bands', 'QuantumEspresso bands', 'QuantumEspresso bands file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-qespresso-pdos', 'QuantumEspresso pdos', 'QuantumEspresso pdos file', 1, false);

INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-qespresso-phonon-input', 'QuantumEspresso phonon input', 'QuantumEspresso phonon input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-qespresso-phonon-output', 'QuantumEspresso phonon data', 'QuantumEspresso phonon data file', 1, false);
	

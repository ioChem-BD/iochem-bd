package org.dspace.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.dspace.eperson.EPerson;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


@Path("/users")
public class UsersResource extends Resource{
    
    @GET
    @Produces({MediaType.APPLICATION_XML})
    public Response getPlatformUsers(@Context HttpHeaders headers) throws ParserConfigurationException {
        org.dspace.core.Context context = null;
        try {
            context = Resource.createContext(Resource.getUser(headers));
            EPerson ePerson = context.getCurrentUser();
            if(ePerson != null) {                                
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = builder.newDocument();
                Element root = doc.createElement("users");
                doc.appendChild(root);   
                //Retrieve users
                EPerson[] epersons = EPerson.findAll(context, EPerson.ID);
                for(int inx = 0; inx < epersons.length; inx++) {
                    EPerson eperson = epersons[inx];
                    if(eperson.getUsername() != null && !eperson.getUsername().toUpperCase().startsWith("REVIEWER")){
                        Element user = doc.createElement("user");                    
                        user.setAttribute("username", eperson.getUsername());
                        user.setAttribute("id", String.valueOf(eperson.getID()));
                        root.appendChild(user);
                    }
                }
                context.complete();
                return Response.ok(doc).build();
            }
        }catch (Exception e){
            processException("Someting went wrong while retrieving platform users list. Exception. Message: "+ e.getMessage() + e,context);
            return Response.serverError().build();
        }finally{
            processFinally(context);
        }
        return Response.serverError().build();
    }  
}

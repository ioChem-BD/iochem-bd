package org.dspace.rest;

import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.ws.rs.core.Context;
import org.dspace.eperson.EPerson;
import org.dspace.eperson.Group;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


@Path("/usergroups")
public class UserGroupsResource extends Resource{
    
    private static final Pattern BLOCKED_GROUPS = Pattern.compile("(COMMUNITY_|COLLECTION_|ANONYMOUS|ADMINISTRATOR|REVIEWERS).*");  
    
    @GET
    @Produces({MediaType.APPLICATION_XML})
    public Response getPlatformUsers(@Context HttpHeaders headers) throws ParserConfigurationException {
        org.dspace.core.Context context = null;
        try {
            context = Resource.createContext(Resource.getUser(headers));
            EPerson ePerson = context.getCurrentUser();
            if(ePerson != null) {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = builder.newDocument();
                Element root = doc.createElement("groups");
                doc.appendChild(root);              
                //Retrieve user groups
                Group[] groups = Group.findAll(context, Group.NAME);
                for(int inx = 0; inx < groups.length; inx++) {
                    Group group = groups[inx];
                    String groupName = group.getName();
                    if(!BLOCKED_GROUPS.matcher(groupName.toUpperCase()).matches()) {
                        Element user = doc.createElement("group");
                        user.setAttribute("name", groupName);
                        user.setAttribute("id", String.valueOf(group.getID()));
                        root.appendChild(user);
                    }
                }
                context.complete();                
                return Response.ok(doc).build();
            }
        }catch (Exception e){
            processException("Someting went wrong while retrieving platform users list. Exception. Message: "+ e.getMessage() + e,context);
            return Response.serverError().build();        
        }finally{
            processFinally(context);  
        }
        return Response.serverError().build();
    }
}

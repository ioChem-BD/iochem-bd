#!/bin/bash
#################################################################################
## ioChem-BD certificate upgrade tool                                          ##
## Copy private key, public certificate and optionally its parent CA certs     ##
## inside the $newCertificateFolder and with the defined filenames             ##
#################################################################################

#Root folder where ioChem-BD is installed
BASE_PATH=$PWD/../..

#Please, leave the rest of variables unchanged
HAS_ERROR=0
HAS_BUNDLE_FILE=false

#Constants
IOCHEM_BD_ALIAS=iochem-bd
KEYSTORE_PASSWORD=changeit
KEYTOOL_PATH=keytool

#certificate files to be modified
certificateFolder=$BASE_PATH/ssl
keystoreFile=$certificateFolder/keystore
cacertsFile=$certificateFolder/truststore

#User provided certificate files
newCertificateFolder=$certificateFolder/new
privateKeystore=$newCertificateFolder/keystore.jks
certificateFile=$newCertificateFolder/certificate.crt
certificateKeyFile=$newCertificateFolder/certificate.key
certificateBundleFile=$newCertificateFolder/additional.bundle
pkcs12File=$newCertificateFolder/certificate.p12


#Presentation variables
#Code taken from https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
RED='\033[1;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
REDBOX="[${RED}KO${NC}]"
GREENBOX="[${GREEN}Ok${NC}]"


safe_command() 
{
	echo -e "$\t$1" 
	eval $1
	if [ $? -ne 0 ]; then
		echo -e "$REDBOX Command failed\n" 
		HAS_ERROR=1
	else
		echo -e "$GREENBOX Command succeeded\n" 
	fi
}

abort()
{
    echo >&2 '
        ***************
        *** ABORTED ***
        ***************
    '
    echo "An error occurred. Exiting..." >&2
    exit 1
}

check_commands_existence() 
{
	if [ ! -f $KEYTOOL_PATH ]; then
		echo -e "$REDBOX Could not locate keytool command, missing PATH?"
		HAS_ERROR=1		
    else
    	echo -e "$GREENBOX Found keytool command"
	fi
	
	if ! [ -x "$(command -v openssl)" ]; then
		echo -e "$REDBOX openssl git is not installed." >&2
		HAS_ERROR=1
    else
    	echo -e "$GREENBOX Found openssl command"
	fi

}

check_certificate_files() 
{
	# public certificate and its private key are required, addintional 'bundle' public certificates file is optional
	if [ ! -f $certificateFile ]; then
		echo -e "$REDBOX Can't find "$certificateFile" file"
		HAS_ERROR=1
    else 
        echo -e "$GREENBOX Found "$certificateFile" file"

        echo -e "Checking certificate file is in PEM format"
        safe_command "openssl x509 -in $certificateFile -text -noout > /dev/null 2>&1"
	fi
   
	if [ ! -f $certificateKeyFile ]; then
		echo -e "$REDBOX Can't find "$certificateKeyFile" file"
		HAS_ERROR=1
    else
        echo -e "$GREENBOX Found "$certificateKeyFile" file"

        echo -e "Checking private key is in PEM format"
        safe_command 'grep -m 1 "BEGIN PRIVATE KEY" $certificateKeyFile'
	fi
	if [ -f $certificateBundleFile ]; then
        echo -e "$GREENBOX Found "$certificateBundleFile" file"        
        HAS_BUNDLE=true
        
        echo -e "Checking certificate bundle file is in PEM format"
        safe_command "openssl x509 -in $certificateBundleFile -text -noout > /dev/null 2>&1"

	fi
}

backup_old_certificate_files() 
{
	suffix="_"$(date +'%y%m%d%H%M%S')

	echo -e "Backup old keystore to: " $keystoreFile$suffix
	safe_command "cp $keystoreFile $keystoreFile$suffix"
	
    echo -e "Backup old cacert keystore to: " $cacertsFile$suffix
	safe_command "cp $cacertsFile $cacertsFile$suffix"
}

build_new_keystore() 
{
    echo -e "Removing previous temporal files:"
    safe_command "rm -f $pkcs12File"
    safe_command "rm -f $privateKeystore"
    
    echo -e "Creating intermediate pkcs12 file:"    
    if $HAS_BUNDLE; then
        safe_command "openssl pkcs12 -export -in $certificateFile -inkey $certificateKeyFile -out $pkcs12File -name $IOCHEM_BD_ALIAS -chain -CAfile $certificateBundleFile -caname root -passin pass:$KEYSTORE_PASSWORD -passout pass:$KEYSTORE_PASSWORD"    
    else
        safe_command "openssl pkcs12 -export -in $certificateFile -inkey $certificateKeyFile -out $pkcs12File -name $IOCHEM_BD_ALIAS -passin pass:$KEYSTORE_PASSWORD -passout pass:$KEYSTORE_PASSWORD"
    fi
    
    echo -e "Generating keystore from pkcs12 file:"
    safe_command "$KEYTOOL_PATH -importkeystore -deststorepass $KEYSTORE_PASSWORD -destkeypass $KEYSTORE_PASSWORD -destkeystore $privateKeystore -srckeystore $pkcs12File -srcstoretype PKCS12 -srcstorepass $KEYSTORE_PASSWORD -alias $IOCHEM_BD_ALIAS"
}

replace_certificates() 
{
    
    safe_command "rm -f $keystoreFile"
    safe_command "cp -f $privateKeystore $keystoreFile"
    
}

replace_javajdk_certificates() {
    $KEYTOOL_PATH -delete -alias $IOCHEM_BD_ALIAS -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD
    #Delete previous intermediate certs, if any
    $KEYTOOL_PATH -delete -alias iochem-cert-1 -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD > /dev/null 2>&1
    $KEYTOOL_PATH -delete -alias iochem-cert-2 -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD > /dev/null 2>&1
    $KEYTOOL_PATH -delete -alias iochem-cert-3 -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD > /dev/null 2>&1
    $KEYTOOL_PATH -delete -alias iochem-cert-4 -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD > /dev/null 2>&1
    $KEYTOOL_PATH -delete -alias iochem-cert-5 -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD > /dev/null 2>&1

    if $HAS_BUNDLE; then
      echo "Processing additional certificate bundle" 
      cd  $newCertificateFolder 
      awk 'BEGIN {c=0;} /BEGIN CERT/{c++} { print > "cert." c ".pem"}' < $certificateBundleFile
      additionalFiles=$(ls -1 cert.*.pem | sort -r)
      inx=1
      for i in $additionalFiles
      do
        safe_command "$KEYTOOL_PATH -importcert -alias iochem-cert-$inx -file $newCertificateFolder/$i -keystore $cacertsFile -storepass $KEYSTORE_PASSWORD -noprompt"
        let "inx++"
      done
    fi
    safe_command "$KEYTOOL_PATH  -importcert -alias $IOCHEM_BD_ALIAS -file $certificateFile -storepass $KEYSTORE_PASSWORD -keystore $cacertsFile -noprompt"
}

halt_on_error() {
    if [ $HAS_ERROR -ne 0 ]; then
        echo -e "Error(s) appeared during setup, please review and correct them."
        echo -e "No need to replace ioChem-BD certificates, original ones were not replaced."
        abort
    fi
}


###################
## Patch process ##
###################

clear
echo -e "Certificate update process start."

echo -e "(${YELLOW}1/3${NC}) Verifying requirements."
check_commands_existence
check_certificate_files
halt_on_error
backup_old_certificate_files
halt_on_error

echo -e "\n\n(${YELLOW}2/3${NC}) Generating keystore."
build_new_keystore
halt_on_error

echo -e "\n\n(${YELLOW}3/3${NC}) Replacing certificates with new ones."
replace_certificates
replace_javajdk_certificates

if [ $HAS_ERROR -ne 0 ]; then
    echo -e "Error(s) appeared during setup, please review and correct them."
    echo -e "Please replace following certificates for its backups, the original ones were replaced:"
    echo -e "\t - $keystoreFile"    
    echo -e "\t - $cacertsFile"
    abort
fi
echo 
echo -e "Certificate update process ended successfully. Please start ioChem-BD service (as iochem-bd user)."


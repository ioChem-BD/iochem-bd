CATALINA_PID="$CATALINA_BASE/tomcat.pid"

export JAVA_HOME=$(dirname $(dirname $(readlink -f $(which java))))
# Module encapsulation for Java 9 and later, for maintaining compatibility with older codes that rely on internal JDK APIs
export JAVA_OPTS="$JAVA_OPTS --add-exports java.xml/com.sun.org.apache.xerces.internal.util=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED"


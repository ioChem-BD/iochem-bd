#!/bin/bash
echo 'Starting ioChem-BD installer' & 
PID=$!
#Capture base folder
export IOCHEMBD_DIR="$( cd "$( dirname "$0" )" && pwd )"
#Launch installer passing current folder
java -DIOCHEMBD_DIR=$IOCHEMBD_DIR -Dlog4j.configurationFile=$IOCHEMBD_DIR/init-script/log4j2.xml -jar $IOCHEMBD_DIR/installer.jar -d $IOCHEMBD_DIR "$@"

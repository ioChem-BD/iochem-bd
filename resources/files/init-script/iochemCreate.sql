--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 15.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- Name: energy_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE public.energy_type AS ENUM (
    'potential',
    'free',
    'zero point corrected',
    'enthalpy'
);


ALTER TYPE public.energy_type OWNER TO iochembd;

--
-- Name: param_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE public.param_type AS ENUM (
    'string',
    'integer',
    'float'
);


ALTER TYPE public.param_type OWNER TO iochembd;

--
-- Name: project_state_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE public.project_state_type AS ENUM (
    'created',
    'modified',
    'certified',
    'published'
);


ALTER TYPE public.project_state_type OWNER TO iochembd;

--
-- Name: rep_units_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE public.rep_units_type AS ENUM (
    'kJ*mol-1',
    'kcal*mol-1',
    'eV'
);


ALTER TYPE public.rep_units_type OWNER TO iochembd;

--
-- Name: res_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE public.res_type AS ENUM (
    'project',
    'calculation'
);


ALTER TYPE public.res_type OWNER TO iochembd;

--
-- Name: roles_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE public.roles_type AS ENUM (
    'admin',
    'data-architect',
    'leader',
    'user',
    'guest'
);


ALTER TYPE public.roles_type OWNER TO iochembd;

SET default_tablespace = '';


--
-- Name: actions; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.actions (
    id integer NOT NULL,
    mimetype character varying(64) NOT NULL,
    jumbo_format character varying(50) NOT NULL,
    action character varying(100) NOT NULL,
    parameters character varying(100) NOT NULL
);


ALTER TABLE public.actions OWNER TO iochembd;

--
-- Name: actions_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.actions_id_seq OWNER TO iochembd;

--
-- Name: actions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.actions_id_seq OWNED BY public.actions.id;


--
-- Name: areas_file_ref; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.areas_file_ref (
    id integer NOT NULL,
    calculation_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    file character varying(256) NOT NULL
);


ALTER TABLE public.areas_file_ref OWNER TO iochembd;

--
-- Name: areas_file_ref_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.areas_file_ref_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.areas_file_ref_id_seq OWNER TO iochembd;

--
-- Name: areas_file_ref_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.areas_file_ref_id_seq OWNED BY public.areas_file_ref.id;


--
-- Name: calculation_types; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.calculation_types (
    id integer NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    abr character varying(4) NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(512) NOT NULL,
    metadata_template character varying(512) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.calculation_types OWNER TO iochembd;

--
-- Name: COLUMN calculation_types.metadata_template; Type: COMMENT; Schema: public; Owner: iochembd
--

COMMENT ON COLUMN public.calculation_types.metadata_template IS 'This field contains XSLT template name in charge of extracting metadata from CML files. Such files must come from current row format.';


--
-- Name: calculation_types_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.calculation_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calculation_types_id_seq OWNER TO iochembd;

--
-- Name: calculation_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.calculation_types_id_seq OWNED BY public.calculation_types.id;


--
-- Name: calculations; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.calculations (
    id bigint NOT NULL,
    type_id integer NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    name character varying(64) NOT NULL,
    path character varying(512) NOT NULL,
    description character varying(304) NOT NULL,
    mets_xml text,
    published boolean DEFAULT false NOT NULL,
    handle character varying(256) DEFAULT ''::character varying NOT NULL,
    published_name character varying(256) DEFAULT ''::character varying NOT NULL,
    publication_time timestamp(6) without time zone,
    element_order integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.calculations OWNER TO iochembd;

--
-- Name: calculations_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.calculations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calculations_id_seq OWNER TO iochembd;

--
-- Name: cutting_area_definitions; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.cutting_area_definitions (
    id integer NOT NULL,
    calculation_type_id integer NOT NULL,
    default_type_sel boolean NOT NULL,
    abbreviation character varying(8) NOT NULL,
    file_name character varying(64) NOT NULL,
    url character varying(256) NOT NULL,
    description character varying(512) NOT NULL,
    jumbo_converter_class character varying(512) NOT NULL,
    jumbo_converter_in_type character varying(32) NOT NULL,
    jumbo_converter_out_type character varying(32) NOT NULL,
    mimetype character varying(64) NOT NULL,
    use character varying(32) NOT NULL,
    label character varying(32) NOT NULL,
    behaviour character varying(256),
    requires character varying,
    renameto character varying
);


ALTER TABLE public.cutting_area_definitions OWNER TO iochembd;

--
-- Name: cutting_area_definitions_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.cutting_area_definitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cutting_area_definitions_id_seq OWNER TO iochembd;

--
-- Name: cutting_area_definitions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.cutting_area_definitions_id_seq OWNED BY public.cutting_area_definitions.id;


--
-- Name: dirs_id; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.dirs_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dirs_id OWNER TO iochembd;

--
-- Name: molecular_fingerprints; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.molecular_fingerprints (
    id bigint NOT NULL,
    calculation_id bigint NOT NULL,
    feature integer[] NOT NULL,
    connectivity_feature integer[] NOT NULL,
    atom_type text[] NOT NULL,
    canonical_smiles character varying NOT NULL,
    connectivity_smiles character varying NOT NULL,
    inchi character varying NOT NULL,
    inchi_key character varying NOT NULL
);


ALTER TABLE public.molecular_fingerprints OWNER TO iochembd;

--
-- Name: projects; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.projects (
    permissions bit(6) DEFAULT '111000'::"bit" NOT NULL,
    owner_user_id integer NOT NULL,
    owner_group_id integer NOT NULL,
    id bigint NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    concept_group character varying(64) NOT NULL,
    name character varying(64) NOT NULL,
    path character varying(512) NOT NULL,
    description character varying(304) NOT NULL,
    modification_time timestamp without time zone,
    certification_time timestamp without time zone,
    published boolean DEFAULT false NOT NULL,
    handle character varying(256) DEFAULT ''::character varying NOT NULL,
    published_name character varying(256) DEFAULT ''::character varying NOT NULL,
    publication_time timestamp(6) without time zone,
    state public.project_state_type DEFAULT 'created'::public.project_state_type NOT NULL,
    element_order integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.projects OWNER TO iochembd;

--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO iochembd;

--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;


--
-- Name: report_calculations; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.report_calculations (
    id bigint NOT NULL,
    report_id integer NOT NULL,
    calc_order integer NOT NULL,
    title character varying(128) NOT NULL,
    calc_id bigint NOT NULL
);


ALTER TABLE public.report_calculations OWNER TO iochembd;

--
-- Name: report_calculations_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.report_calculations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_calculations_id_seq OWNER TO iochembd;

--
-- Name: report_calculations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.report_calculations_id_seq OWNED BY public.report_calculations.id;


--
-- Name: report_type; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.report_type (
    id integer NOT NULL,
    name character varying(64) DEFAULT ''::bpchar NOT NULL,
    output_type character varying(256) DEFAULT ''::bpchar NOT NULL,
    associated_xslt character varying(1024) DEFAULT ''::bpchar NOT NULL,
    associated_zul character varying(1024) DEFAULT ''::bpchar NOT NULL,
    enabled boolean DEFAULT true NOT NULL
);


ALTER TABLE public.report_type OWNER TO iochembd;

--
-- Name: report_type_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.report_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.report_type_id_seq OWNER TO iochembd;

--
-- Name: report_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.report_type_id_seq OWNED BY public.report_type.id;


--
-- Name: reports; Type: TABLE; Schema: public; Owner: iochembd
--

CREATE TABLE public.reports (
    id integer NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    owner_id integer NOT NULL,
    name character varying(64) NOT NULL,
    title character varying(256) NOT NULL,
    description character varying(512) NOT NULL,
    configuration text DEFAULT ''::text,
    type integer DEFAULT 0 NOT NULL,
    published boolean DEFAULT false NOT NULL
);


ALTER TABLE public.reports OWNER TO iochembd;

--
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.reports_id_seq OWNER TO iochembd;

--
-- Name: reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE public.reports_id_seq OWNED BY public.reports.id;


--
-- Name: actions id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.actions ALTER COLUMN id SET DEFAULT nextval('public.actions_id_seq'::regclass);


--
-- Name: areas_file_ref id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.areas_file_ref ALTER COLUMN id SET DEFAULT nextval('public.areas_file_ref_id_seq'::regclass);


--
-- Name: calculation_types id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.calculation_types ALTER COLUMN id SET DEFAULT nextval('public.calculation_types_id_seq'::regclass);


--
-- Name: cutting_area_definitions id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.cutting_area_definitions ALTER COLUMN id SET DEFAULT nextval('public.cutting_area_definitions_id_seq'::regclass);


--
-- Name: projects id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);


--
-- Name: report_calculations id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_calculations ALTER COLUMN id SET DEFAULT nextval('public.report_calculations_id_seq'::regclass);


--
-- Name: report_type id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_type ALTER COLUMN id SET DEFAULT nextval('public.report_type_id_seq'::regclass);


--
-- Name: reports id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.reports ALTER COLUMN id SET DEFAULT nextval('public.reports_id_seq'::regclass);


--
-- Data for Name: actions; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO public.actions VALUES (1, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (2, 'chemical/x-gaussian-input', 'gaussian_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (3, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (4, 'chemical/x-gaussian-input', 'gaussian_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (5, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGAUSSIAN.xsl -extension .html');
INSERT INTO public.actions VALUES (6, 'chemical/x-cml', 'gaussian_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (7, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (8, 'chemical/x-adf-input', 'adf_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (9, 'chemical/x-adf-input', 'adf_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (10, 'chemical/x-cml', 'adf_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (11, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (12, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (13, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlADF.xsl -extension .html');
INSERT INTO public.actions VALUES (14, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (15, 'chemical/x-vasp-incar', 'vasp_incar', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (16, 'chemical/x-vasp-incar', 'vasp_incar', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (17, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (18, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (19, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (20, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlVASP.xsl -extension .html');
INSERT INTO public.actions VALUES (21, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (22, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (23, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (24, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlTURBOMOLE.xsl -extension .html');
INSERT INTO public.actions VALUES (25, 'chemical/x-turbomole-control', 'turbomole_control', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (26, 'chemical/x-turbomole-control', 'turbomole_control', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (27, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (28, 'chemical/x-orca-input', 'orca_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (29, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (30, 'chemical/x-orca-input', 'orca_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (31, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlORCA.xsl -extension .html');
INSERT INTO public.actions VALUES (32, 'chemical/x-cml', 'orca_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (33, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (34, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (35, 'chemical/x-molcas-input', 'molcas_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (36, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (37, 'chemical/x-molcas-input', 'molcas_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (38, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlMOLCAS.xsl -extension .html');
INSERT INTO public.actions VALUES (39, 'chemical/x-cml', 'molcas_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (40, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (41, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (42, 'chemical/x-qespresso-input', 'qespresso_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (43, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (44, 'chemical/x-qespresso-input', 'qespresso_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (45, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlQUANTUMESPRESSO.xsl -extension .html');
INSERT INTO public.actions VALUES (46, 'chemical/x-cml', 'qespresso_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (47, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (48, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (49, 'chemical/x-mopac-input', 'mopac_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (50, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (51, 'chemical/x-mopac-input', 'mopac_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (52, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlMOPAC.xsl -extension .html');
INSERT INTO public.actions VALUES (53, 'chemical/x-cml', 'mopac_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (54, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (55, 'chemical/x-gronor-input', 'gronor_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (56, 'chemical/x-gronor-input', 'gronor_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (57, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (58, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (59, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (60, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGRONOR.xsl -extension .html');
INSERT INTO public.actions VALUES (61, 'chemical/x-gromacs-input', 'gromacs_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (62, 'chemical/x-gromacs-input', 'gromacs_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (63, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (64, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (65, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (66, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGROMACS.xsl -extension .html');
INSERT INTO public.actions VALUES (67, 'chemical/x-gromos87', 'gromacs_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (68, 'chemical/x-gromos87', 'gromacs_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (69, 'chemical/x-gromacs-trajectory', 'gromacs_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (70, 'chemical/x-amber-input', 'amber_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (71, 'chemical/x-amber-input', 'amber_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (72, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (73, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (74, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (75, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlAMBER.xsl -extension .html');
INSERT INTO public.actions VALUES (76, 'chemical/x-amber-topology', 'amber_topology', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (77, 'chemical/x-amber-topology', 'amber_topology', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (78, 'chemical/x-amber-trajectory', 'amber_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (79, 'chemical/x-lammps-input', 'lammps_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (80, 'chemical/x-lammps-input', 'lammps_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (81, 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (82, 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (83, 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (84, 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlLAMMPS.xsl -extension .html');
INSERT INTO public.actions VALUES (85, 'chemical/chemical/x-lammps-trajectory', 'lammps_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (86, 'chemical/x-castep-input', 'castep_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (87, 'chemical/x-castep-input', 'castep_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (88, 'chemical/x-castep-cell', 'castep_cell', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (89, 'chemical/x-castep-cell', 'castep_cell', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (90, 'chemical/x-castep-geometry', 'castep_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (91, 'chemical/x-castep-geometry', 'castep_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (92, 'chemical/x-castep-output', 'castep_log', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (93, 'chemical/x-castep-output', 'castep_log', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (94, 'chemical/x-castep-xcd', 'castep_graph', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (95, 'chemical/x-cml', 'castep_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (96, 'chemical/x-cml', 'castep_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (97, 'chemical/x-cml', 'castep_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (98, 'chemical/x-cml', 'castep_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlCASTEP.xsl -extension .html');
INSERT INTO public.actions VALUES (99, 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (100, 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (101, 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (102, 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlAMS.xsl -extension .html');
INSERT INTO public.actions VALUES (103, 'chemical/x-qespresso-spectra', 'qespresso_spectra', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (104, 'chemical/x-qespresso-spectra', 'qespresso_spectra', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (105, 'chemical/x-qespresso-bands', 'qespresso_bands', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (106, 'chemical/x-qespresso-bands', 'qespresso_bands', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (107, 'chemical/x-qespresso-pdos', 'qespresso_pdos', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (108, 'chemical/x-qespresso-pdos', 'qespresso_pdos', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (109, 'chemical/x-qespresso-phonon-input', 'qespresso_phonon_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (110, 'chemical/x-qespresso-phonon-input', 'qespresso_phonon_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (111, 'chemical/x-qespresso-phonon-output', 'qespresso_phonon_output', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (112, 'chemical/x-qespresso-phonon-output', 'qespresso_phonon_output', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (113, 'chemical/x-siesta-input', 'siesta_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (114, 'chemical/x-siesta-input', 'siesta_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (115, 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (116, 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (117, 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (118, 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlSIESTA.xsl -extension .html');
INSERT INTO public.actions VALUES (119, 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGRRM.xsl -extension .html');
INSERT INTO public.actions VALUES (120, 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (121, 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO public.actions VALUES (122, 'chemical/x-cml', 'grrm_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO public.actions VALUES (123, 'chemical/x-grrm-input', 'grrm_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO public.actions VALUES (124, 'chemical/x-grrm-input', 'grrm_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');


--
-- Data for Name: areas_file_ref; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: calculation_types; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO public.calculation_types VALUES (1, '2010-10-11 20:46:10.24976', 'GAU', 'Gaussian', 'Gaussian Type', '/html/xsltmd/gaussian.xsl');
INSERT INTO public.calculation_types VALUES (2, '2010-10-11 20:46:10.24976', 'ADF', 'ADF', 'ADF Type', '/html/xsltmd/adf.xsl');
INSERT INTO public.calculation_types VALUES (3, '2010-10-11 20:46:10.24976', 'VSP', 'Vasp', 'VASP Type', '/html/xsltmd/vasp.xsl');
INSERT INTO public.calculation_types VALUES (4, '2014-12-15 18:09:24', 'TML', 'Turbomole', 'Turbomole Type', '/html/xsltmd/turbomole.xsl');
INSERT INTO public.calculation_types VALUES (5, '2015-11-02 16:36:01.147714', 'ORC', 'Orca', 'Orca Type', '/html/xsltmd/orca.xsl');
INSERT INTO public.calculation_types VALUES (6, '2016-07-05 08:15:00', 'MOL', 'Molcas', 'Molcas Type', '/html/xsltmd/molcas.xsl');
INSERT INTO public.calculation_types VALUES (7, '2018-02-07 18:07:05.936654', 'QEX', 'QuantumEspresso', 'QuantumEspresso Type', '/html/xsltmd/qespresso.xsl');
INSERT INTO public.calculation_types VALUES (8, '2019-03-06 08:00:00', 'MOP', 'Mopac', 'Mopac Type', '/html/xsltmd/mopac.xsl');
INSERT INTO public.calculation_types VALUES (9, '2021-04-23 12:00:00', 'GRO', 'GronOR', 'GronOR Type', '/html/xsltmd/gronor.xsl');
INSERT INTO public.calculation_types VALUES (10, '2021-09-03 12:00:00', 'GRM', 'GROMACS', 'GROMACS Type', '/html/xsltmd/gromacs.xsl');
INSERT INTO public.calculation_types VALUES (11, '2021-11-16 11:45:00', 'AMB', 'Amber', 'Amber Type', '/html/xsltmd/amber.xsl');
INSERT INTO public.calculation_types VALUES (12, '2022-05-06 13:34:00', 'LAM', 'LAMMPS', 'LAMMPS Type', '/html/xsltmd/lammps.xsl');
INSERT INTO public.calculation_types VALUES (13, '2022-07-22 17:40:00', 'CAS', 'CASTEP', 'CASTEP Type', '/html/xsltmd/castep.xsl');
INSERT INTO public.calculation_types VALUES (14, '2022-10-27 11:14:00', 'AMS', 'AMS', 'AMS with ADF Type', '/html/xsltmd/adf.xsl');
INSERT INTO public.calculation_types VALUES (15, '2022-12-24 11:45:00', 'SIE', 'SIESTA', 'SIESTA Type', '/html/xsltmd/siesta.xsl');
INSERT INTO public.calculation_types VALUES (16, '2023-05-16 12:37:33.854403', 'GRRM', 'GRRM', 'GRRM Type', '/html/xsltmd/grrm.xsl');


--
-- Data for Name: calculations; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: cutting_area_definitions; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO public.cutting_area_definitions VALUES (1, 1, true, '-o', '.*.out|.*.log', '-', 'Gaussian - output file', 'org.xmlcml.cml.converters.compchem.gaussian.log.GaussianLog2CompchemConverter', 'gaussian_log', 'gaussian_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (2, 1, true, '-i', '.*.in|.*.com', '-', 'Gaussian - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gaussian_input', 'gaussian_input', 'chemical/x-gaussian-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (3, 1, false, '-a', '*.*', '-', 'Gaussian - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (4, 2, true, '-o', '.*.out', '-', 'Adf - output file', 'org.xmlcml.cml.converters.compchem.adf.log.AdfLog2CompchemConverter', 'adf_log', 'adf_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (5, 2, true, '-i', '.*.in', '-', 'Adf - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'adf_input', 'adf_input', 'chemical/x-adf-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (6, 2, false, '-a', '*.*', '-', 'Adf - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (7, 3, false, '-a', '*.*', '-', 'VASP - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file/s(5)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (8, 3, false, '-dc', 'DOSCAR', '-', 'VASP - DOSCAR file ', 'org.xmlcml.cml.converters.compchem.vasp.doscar.Doscar2XMLConverter', 'vasp_doscar', 'vasp_doscar_xml', 'chemical/x-cml', 'append', 'DOSCAR file(3)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (9, 3, true, '-o', 'OUTCAR', '-', 'VASP - OUTCAR file', 'org.xmlcml.cml.converters.compchem.vasp.outcar.VaspOutcar2CompchemConverter', 'vasp_outcar', 'vasp_outcar_compchem', 'chemical/x-cml', 'output', 'OUTCAR file (2*)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (10, 3, false, '-i', 'INCAR', '-', 'VASP - INCAR file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'vasp_incar', 'vasp_incar', 'chemical/x-vasp-incar', 'input', 'INCAR file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (11, 4, false, '-ob', 'basis', '-', 'Turbomole - basis file', 'org.xmlcml.cml.converters.compchem.turbomole.basis.TurbomoleBasis2XMLConverter', 'turbomole_basis', 'turbomole_basis_xml', 'chemical/x-cml', 'append', 'Basis file(3)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (12, 4, false, '-oc', 'coords', '-', 'Turbomole - coord file', 'org.xmlcml.cml.converters.compchem.turbomole.coord.TurbomoleCoord2XMLConverter', 'turbomole_coord', 'turbomole_coord_xml', 'chemical/x-cml', 'append', 'Coord file(4)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (13, 4, false, '-o', 'job.last', '-', 'Turbomole - job.last file', 'org.xmlcml.cml.converters.compchem.turbomole.log.TurbomoleLog2CompchemConverter', 'turbomole_log', 'turbomole_log_compchem', 'chemical/x-cml', 'output', 'job.last(5*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (14, 4, false, '-a', '*.*', '-', 'Turbomole - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file/s(6)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (15, 4, true, '-i', 'control', '-', 'Turbomole - control file', 'org.xmlcml.cml.converters.compchem.turbomole.control.TurbomoleControl2XMLConverter', 'turbomole_control', 'turbomole_control_xml', 'chemical/x-cml', 'append', 'Control file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (16, 4, true, '-i', 'control', '-', 'Turbomole - control file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'turbomole_control', 'turbomole_control', 'chemical/x-turbomole-control', 'input', 'Control file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (17, 4, false, '-oe', 'energy', '-', 'Turbomole - energy file', 'org.xmlcml.cml.converters.compchem.turbomole.energy.TurbomoleEnergy2XMLConverter', 'turbomole_energy', 'turbomole_energy_xml', 'chemical/x-cml', 'append', 'Energy file(2)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (18, 3, false, '-kp', 'KPOINTS', '-', 'VASP - KPOINTS file', 'org.xmlcml.cml.converters.compchem.vasp.kpoints.VaspKpoint2XMLConverter', 'vasp_kpoints', 'vasp_kpoints_xml', 'chemical/x-cml', 'append', 'KPOINTS(4)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (19, 3, false, '-i', 'INCAR', '-', 'VASP - INCAR file', 'org.xmlcml.cml.converters.compchem.vasp.incar.VaspIncar2XMLConverter', 'vasp_incar', 'vasp_incar_xml', 'chemical/x-cml', 'append', 'INCAR file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (20, 5, true, '-i', '*.inp', '-', 'Orca - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'orca_input', 'orca_input', 'chemical/x-orca-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (21, 5, true, '-o', '*.out', '-', 'Orca - output file', 'org.xmlcml.cml.converters.compchem.orca.log.OrcaLog2CompchemConverter', 'orca_log', 'orca_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (22, 5, false, '-a', '*.*', '-', 'Orca - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (23, 6, true, '-i', '*.input', '-', 'Molcas - input file', 'org.xmlcml.cml.converters.compchem.molcas.input.MolcasInput2XMLConverter', 'molcas_input', 'molcas_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (24, 6, true, '-i', '*.input', '-', 'Molcas - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'molcas_input', 'molcas_input', 'chemical/x-molcas-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (25, 6, true, '-o', '*.output', '-', 'Molcas - output file', 'org.xmlcml.cml.converters.compchem.molcas.log.MolcasLog2CompchemConverter', 'molcas_log', 'molcas_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (26, 6, false, '-a', '*.*', '-', 'Molcas - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (27, 7, true, '-i', '.*.in|.*.com', '-', 'QuantumEspresso - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_input', 'qespresso_input', 'chemical/x-qespresso-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (29, 7, false, '-a', '*.*', '-', 'QuantumEspresso - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (30, 7, true, '-i', '.*.in|.*.com', '-', 'QuantumEspresso - input file', 'org.xmlcml.cml.converters.compchem.qespresso.input.QEspressoInput2XMLConverter', 'qespresso_input', 'qespresso_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (31, 8, true, '-i', '.*.in|.*.mop', '-', 'Mopac - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'mopac_input', 'mopac_input', 'chemical/x-mopac-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (32, 8, true, '-o', '.*.out|.*.log', '-', 'Mopac - output file', 'org.xmlcml.cml.converters.compchem.mopac.log.MopacLog2CompchemConverter', 'mopac_log', 'mopac_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (33, 8, false, '-a', '*.*', '-', 'Mopac - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (34, 9, true, '-i', '.*.in', '-', 'GronOR - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gronor_input', 'gronor_input', 'chemical/x-gronor-input', 'input', 'Input file(1)', '', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (35, 9, true, '-o', '.*.xml|.*.cml', '-', 'GronOR - output file', 'org.xmlcml.cml.converters.compchem.gronor.xml.GronorXML2CompchemConverter', 'gronor_xml', 'gronor_xml_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (36, 9, true, '-a', '.*.*', '-', 'GronOR - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (37, 10, true, '-i', '.*.in', '-', 'GROMACS - input file', 'org.xmlcml.cml.converters.compchem.gromacs.input.GromacsInput2XMLConverter', 'gromacs_input', 'gromacs_input_xml', 'chemical/x-cml', 'append', 'Input file .mdp(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (38, 10, true, '-i', '.*.in', '-', 'GROMACS - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_input', 'gromacs_input', 'chemical/x-gromacs-input', 'input', 'Input file .mdp(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (39, 10, true, '-o', '.*.log', '-', 'GROMACS - output file', 'org.xmlcml.cml.converters.compchem.gromacs.log.GromacsLog2CompchemConverter', 'gromacs_log', 'gromacs_log_compchem', 'chemical/x-cml', 'output', 'Output file .log(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (40, 10, true, '-oc', '.*.gro', '-', 'GROMACS - geometry file', 'org.xmlcml.cml.converters.compchem.gromacs.geometry.GromacsGeometry2XMLConverter', 'gromacs_geometry', 'gromacs_geometry_xml', 'chemical/x-cml', 'append', 'Geometry file .gro(3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (41, 10, true, '-oc', '.*.gro', '-', 'GROMACS - geometry file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_geometry', 'gromacs_geometry', 'chemical/x-gromos87', 'input', 'Geometry file .gro (3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (42, 10, true, '-t', '.*.xtc', '-', 'GROMACS - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_trajectory', 'gromacs_trajectory', 'chemical/x-gromacs-trajectory', 'input', 'Trajectory file .xtc (4*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (43, 10, true, '-a', '.*.*', '-', 'GROMACS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (44, 11, true, '-i', '.*.in', '-', 'Amber - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_input', 'amber_input', 'chemical/x-amber-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (45, 11, true, '-o', '.*.log', '-', 'Amber - output file', 'org.xmlcml.cml.converters.compchem.amber.log.AmberLog2CompchemConverter', 'amber_log', 'amber_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (46, 11, true, '-p', '.*.prmtop', '-', 'Amber - topology file', 'org.xmlcml.cml.converters.compchem.amber.topology.AmberTopology2XMLConverter', 'amber_topology', 'amber_topology_xml', 'chemical/x-cml', 'append', 'Topology file .prmtop(3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (47, 11, true, '-p', '.*.prmtop', '-', 'Amber - topology file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_topology', 'amber_topology', 'chemical/x-amber-topology', 'input', 'Topology file .prmtop(3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (48, 11, true, '-ir', '.*.inpcrd', '-', 'Amber - Input coordinates or initial .ncrst file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Input coords .inpcrd .ncrst(4*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (49, 11, true, '-r', '.*.ncrst', '-', 'Amber - NetCDF Restart', 'org.xmlcml.cml.converters.compchem.amber.trajectory.AmberTrajectory2XMLConverter', 'amber_trajectory', 'amber_trajectory_xml', 'chemical/x-cml', 'append', 'Final restart file .ncrst(5*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (50, 11, true, '-r', '.*.ncrst', '-', 'Amber - NetCDF Restart', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_trajectory', 'amber_trajectory', 'chemical/x-amber-coordinates', 'additional', 'Final restart file .ncrst(5*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (51, 11, true, '-t', '.*.nc', '-', 'Amber - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_trajectory', 'amber_trajectory', 'chemical/x-amber-trajectory', 'input', 'Trajectory file .nc (6*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (52, 11, true, '-a', '.*.*', '-', 'Amber - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(7)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (53, 12, true, '-i', '.in.file', '-', 'LAMMPS - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'lammps_input', 'lammps_input', 'chemical/x-lammps-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (54, 12, true, '-p', '.*.dat', '-', 'LAMMPS - data_file', 'org.xmlcml.cml.converters.compchem.lammps.data.LammpsData2XMLConverter', 'lammps_data', 'lammps_data_xml', 'chemical/x-cml', 'append', 'Data file for read_data(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (55, 12, true, '-p', '.*.dat', '-', 'LAMMPS - data file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'lammps_data', 'lammps_data', 'chemical/x-lammps-datafile', 'input', 'Data file for read_data(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (56, 12, true, '-o', 'log.lammps', '-', 'LAMMPS - output file', 'org.xmlcml.cml.converters.compchem.lammps.log.LammpsLog2CompchemConverter', 'lammps_log', 'lammps_log_compchem', 'chemical/x-cml', 'output', 'Output file log.lammps(3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (57, 12, true, '-t', '.*.tar.gz|.*.zip', '-', 'LAMMPS - Trajectory file', 'org.xmlcml.cml.converters.compchem.lammps.trajectory.LammpsTrajectory2CIFConverter', 'lammps_trajectory', 'lammps_trajectory_cif', 'chemical/x-cif', 'input', 'Trajectory file (4*)', NULL, 'lammps_data_xml', 'multistep.cif');
INSERT INTO public.cutting_area_definitions VALUES (58, 12, true, '-t', '.*.tar.gz|.*.zip', '-', 'LAMMPS - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'lammps_trajectory', 'lammps_trajectory', 'chemical/x-lammps-trajectory', 'additional', 'Trajectory file (4*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (59, 12, true, '-a', '.*.*', '-', 'LAMMPS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (60, 13, true, '-i', '.param', '-', 'CASTEP - param file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'castep_input', 'castep_input', 'chemical/x-castep-input', 'input', 'Input file .param(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (61, 13, true, '-oc', '.*.cell', '-', 'CASTEP - cell file', 'org.xmlcml.cml.converters.compchem.castep.cell.CastepCell2XMLConverter', 'castep_cell', 'castep_cell_xml', 'chemical/x-cml', 'append', 'Structure file .cell(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (62, 13, true, '-oc', '.*.cell', '-', 'CASTEP - cell file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'castep_cell', 'castep_cell', 'chemical/x-castep-cell', 'input', 'Structure file .cell(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (63, 13, true, '-o', '.*.castep', '-', 'CASTEP - output file', 'org.xmlcml.cml.converters.compchem.castep.log.CastepLog2CompchemConverter', 'castep_log', 'castep_log_compchem', 'chemical/x-cml', 'output', 'Output file .castep(3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (64, 13, true, '-o', '.*.castep', '-', 'CASTEP - output file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'castep_log', 'castep_log', 'chemical/x-castep-output', 'additional', 'Output file .castep(3*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (65, 13, true, '-xcd', '.*.xcd', '-', 'CASTEP - xcd graph file', 'org.xmlcml.cml.converters.compchem.castep.graph.CastepGraph2XMLConverter', 'castep_graph', 'castep_graph_xml', 'chemical/x-cml', 'append', 'Chart file .xcd(4)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (66, 13, true, '-xcd', '.*.xcd', '-', 'CASTEP - xcd graph file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'castep_graph', 'castep_graph', 'chemical/x-castep-xcd', 'additional', 'Chart file .xcd(4)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (67, 13, true, '-og', '.*.geom', '-', 'CASTEP - geometry file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'castep_geometry', 'castep_geometry', 'chemical/x-castep-geometry', 'additional', 'Geometry optimization .geom(5)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (68, 13, true, '-og', '.*.geom', '-', 'CASTEP - geometry file', 'org.xmlcml.cml.converters.compchem.castep.geom.CastepGeom2XMLConverter', 'castep_geometry', 'castep_geometry_xml', 'chemical/x-cml', 'append', 'Geometry optimization .geom(5)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (69, 13, true, '-a', '.*.*', '-', 'CASTEP - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(6)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (70, 12, true, '-i', '.in.file', '-', 'LAMMPS - input file', 'org.xmlcml.cml.converters.compchem.lammps.input.LammpsInput2XMLConverter', 'lammps_input', 'lammps_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (71, 14, true, '-i', '.in', '-', 'AMS - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'ams_input', 'ams_input', 'chemical/x-ams-input', 'input', 'Input file (1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (72, 14, true, '-rkf1', 'ams.rkf', '-', 'AMS - ams.rkf file', 'org.xmlcml.cml.converters.compchem.ams.log.AmsRkf2XMLPathConverter', 'ams_rkf', 'ams_rkf_xml', 'application/xml', 'append', 'ams.rkf file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (73, 14, true, '-rkf2', 'adf.rkf', '-', 'AMS - ads.rkf file', 'org.xmlcml.cml.converters.compchem.ams.log.AmsRkf2XMLConverter', 'ads_rkf', 'ads_rkf_xml', 'chemical/x-cml', 'append', 'adf.rkf file(3*)', NULL, 'ams_rkf_xml', NULL);
INSERT INTO public.cutting_area_definitions VALUES (74, 14, true, '-o', '.out', '-', 'AMS - output file', 'org.xmlcml.cml.converters.compchem.ams.log.AmsLog2CompchemConverter', 'ams_log', 'ams_log_compchem', 'chemical/x-cml', 'output', 'Output .out file (4*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (75, 14, true, '-a', '.*.*', '-', 'AMS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (76, 7, true, '-as', '.*.dat|*.*', '-', 'QuantumEspresso - absorption spectra file', 'org.xmlcml.cml.converters.compchem.qespresso.spectra.QEspressoSpectra2XMLConverter', 'qespresso_spectra', 'qespresso_spectra_xml', 'chemical/x-cml', 'append', 'Absorption spectra data file(4)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (77, 7, true, '-as', '.*.dat|*.*', '-', 'QuantumEspresso - absorption spectra file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_spectra', 'qespresso_spectra', 'chemical/x-qespresso-spectra', 'input', 'Absorption spectra data file(4)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (28, 7, true, '-o', '.*.out|.*.log', '-', 'QuantumEspresso - output file', 'org.xmlcml.cml.converters.compchem.qespresso.log.QEspressoLog2CompchemConverter', 'qespresso_log', 'qespresso_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (78, 7, true, '-b', '*.out|*.*', '-', 'QuantumEspresso - bands file', 'org.xmlcml.cml.converters.compchem.qespresso.bands.QEspressoBands2XMLConverter', 'qespresso_bands', 'qespresso_bands_xml', 'chemical/x-cml', 'append', 'Bands file (5)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (79, 7, true, '-b', '*.out|*.*', '-', 'QuantumEspresso - bands file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_bands', 'qespresso_bands', 'chemical/x-qespresso-bands', 'input', 'Bands file (5)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (80, 7, true, '-dos', '.*.pdos|*.*', '-', 'QuantumEspresso - pdos file', 'org.xmlcml.cml.converters.compchem.qespresso.pdos.QEspressoPDOS2XMLConverter', 'qespresso_pdos', 'qespresso_pdos_xml', 'chemical/x-cml', 'append', 'Projected DOS data file(6)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (81, 7, true, '-dos', '.*.pdos|*.*', '-', 'QuantumEspresso - pdos file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_pdos', 'qespresso_pdos', 'chemical/x-qespresso-pdos', 'input', 'Projected DOS data file(6)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (82, 7, true, '-pi', '.*.pwi|*.*', '-', 'QuantumEspresso - phonom input file', 'org.xmlcml.cml.converters.compchem.qespresso.phonon.QEspressoPhononInput2XMLConverter', 'qespresso_phonon_input', 'qespresso_phonon_input_xml', 'chemical/x-cml', 'append', 'Phonon input file(7)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (83, 7, true, '-pi', '.*.pwi|*.*', '-', 'QuantumEspresso - phonom input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_phonon_input', 'qespresso_phonon_input', 'chemical/x-qespresso-phonon-input', 'input', 'Phonon input file(7)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (84, 7, true, '-po', '.*.freq|*.*', '-', 'QuantumEspresso - phonom output file', 'org.xmlcml.cml.converters.compchem.qespresso.phonon.QEspressoPhononOutput2XMLConverter', 'qespresso_phonon_output', 'qespresso_phonon_output_xml', 'chemical/x-cml', 'append', 'Phonon data file(8)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (85, 7, true, '-po', '.*.freq|*.*', '-', 'QuantumEspresso - phonom output file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_phonon_output', 'qespresso_phonon_output', 'chemical/x-qespresso-phonon-output', 'input', 'Phonon data file(8)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (86, 15, true, '-i', '.*.in', '-', 'SIESTA - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'siesta_input', 'siesta_input', 'chemical/x-siesta-input', 'input', 'Input files(1*)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (87, 15, true, '-o', '.xml|.*', '-', 'SIESTA - output file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'siesta_log_compchem', 'siesta_log_compchem', 'chemical/x-cml', 'output', 'Output XML file(2*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (88, 15, true, '-a', '.*|.*', '-', 'SIESTA - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (89, 16, true, '-i', '*.*', '-', 'GRRM - input file', 'org.xmlcml.cml.converters.compchem.grrm.input.GrrmInput2XMLConverter', 'grrm_input', 'grrm_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (90, 16, true, '-i', '*.*', '-', 'GRRM - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'grrm_input', 'grrm_input', 'chemical/x-grrm-input', 'input', 'Input file(1*)', NULL, NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (91, 16, false, '-a', '*.*', '-', 'GRRM - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', ' ', 'additional', 'Additional file(3)', 'repeatable', NULL, NULL);
INSERT INTO public.cutting_area_definitions VALUES (92, 16, true, '-o', '*.*', '-', 'GRRM - output file', 'org.xmlcml.cml.converters.compchem.grrm.log.GrrmLog2CompchemConverter', 'grrm_output', 'grrm_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL, NULL, NULL);


--
-- Data for Name: molecular_fingerprints; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: report_calculations; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: report_type; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO public.report_type VALUES (2, 'Spin state energies for transition metal complexes', 'PDF|RTF', '/html/reports/ReportTurbomoleSpinStateEnergies2/dataExtraction.xsl|/html/reports/ReportTurbomoleSpinStateEnergies2/spinStateEnergies.xsl', 'reportTurbomoleSpinStateEnergies2.zul', false);
INSERT INTO public.report_type VALUES (4, 'Reaction Graph', '', '/html/reports/ReportReactionGraph/reactionGraph.xsl', 'reportReactionGraph.zul', true);
INSERT INTO public.report_type VALUES (3, 'Reaction Energy Profile', 'CHART', '/html/reports/ReportEnergyReactionProfile/getAvailableEnergies.xsl|/html/reports/ReportEnergyReactionProfile/getTempPressure.xsl|/html/reports/ReportEnergyReactionProfile/dataExtraction.xsl|/html/reports/ReportEnergyReactionProfile/chartGeneration.xsl', 'reportEnergyReactionProfile.zul', true);
INSERT INTO public.report_type VALUES (1, 'Supporting Information', 'PDF|RTF', '/html/reports/ReportSupportingInformation/getAvailableEnergies.xsl|/html/reports/ReportSupportingInformation/dataExtraction.xsl|/html/reports/ReportSupportingInformation/supportingInformation.xsl', 'reportSupportingInformation.zul', true);


--
-- Data for Name: reports; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: actions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.actions_id_seq', 124, true);


--
-- Name: areas_file_ref_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.areas_file_ref_id_seq', 1, true);


--
-- Name: calculation_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.calculation_types_id_seq', 16, true);


--
-- Name: calculations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.calculations_id_seq', 1, false);


--
-- Name: cutting_area_definitions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.cutting_area_definitions_id_seq', 92, true);


--
-- Name: dirs_id; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.dirs_id', 1, true);


--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.projects_id_seq', 1, true);


--
-- Name: report_calculations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.report_calculations_id_seq', 1, true);


--
-- Name: report_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.report_type_id_seq', 4, true);


--
-- Name: reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('public.reports_id_seq', 1, true);


--
-- Name: actions actions_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.actions
    ADD CONSTRAINT actions_id_key UNIQUE (id);


--
-- Name: actions actions_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.actions
    ADD CONSTRAINT actions_pkey PRIMARY KEY (id);


--
-- Name: areas_file_ref areas_file_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.areas_file_ref
    ADD CONSTRAINT areas_file_ref_pkey PRIMARY KEY (id);


--
-- Name: calculation_types calculation_types_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.calculation_types
    ADD CONSTRAINT calculation_types_pkey PRIMARY KEY (id);


--
-- Name: report_calculations calculations_for_reporting_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_calculations
    ADD CONSTRAINT calculations_for_reporting_pkey PRIMARY KEY (id);


--
-- Name: calculations calculations_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.calculations
    ADD CONSTRAINT calculations_id_key UNIQUE (id);


--
-- Name: calculations calculations_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.calculations
    ADD CONSTRAINT calculations_pkey PRIMARY KEY (name, path);


--
-- Name: cutting_area_definitions cutting_area_definitions_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.cutting_area_definitions
    ADD CONSTRAINT cutting_area_definitions_pkey PRIMARY KEY (id);


--
-- Name: reports id_unique; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT id_unique UNIQUE (id);


--
-- Name: molecular_fingerprints molecular_fingerprints_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.molecular_fingerprints
    ADD CONSTRAINT molecular_fingerprints_pkey PRIMARY KEY (id);


--
-- Name: projects projects_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_id_key UNIQUE (id);


--
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (name, path);


--
-- Name: report_calculations report_calculations_id; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_calculations
    ADD CONSTRAINT report_calculations_id UNIQUE (id);


--
-- Name: reports report_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: report_type report_type_key; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_type
    ADD CONSTRAINT report_type_key UNIQUE (id);


--
-- Name: report_type report_type_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_type
    ADD CONSTRAINT report_type_pkey PRIMARY KEY (id);


--
-- Name: calc_id_molecular_fingerprint_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX calc_id_molecular_fingerprint_index ON public.molecular_fingerprints USING btree (calculation_id);


--
-- Name: calc_order_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX calc_order_calc_rep_index ON public.report_calculations USING btree (calc_order);


--
-- Name: certification_time_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX certification_time_pro_index ON public.projects USING btree (certification_time);


--
-- Name: concept_group_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX concept_group_pro_index ON public.projects USING btree (concept_group);


--
-- Name: cre_time_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX cre_time_rep_index ON public.reports USING btree (creation_time);


--
-- Name: creation_time_cal_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX creation_time_cal_index ON public.calculations USING btree (creation_time);


--
-- Name: creation_time_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX creation_time_pro_index ON public.projects USING btree (creation_time);


--
-- Name: desc_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX desc_rep_index ON public.reports USING btree (description);


--
-- Name: description_cal_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX description_cal_index ON public.calculations USING btree (description);


--
-- Name: description_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX description_pro_index ON public.projects USING btree (description);


--
-- Name: fki_report_calculations_calc_id_fk; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX fki_report_calculations_calc_id_fk ON public.report_calculations USING btree (calc_id);


--
-- Name: id_action_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_action_index ON public.actions USING btree (id);


--
-- Name: id_cal_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_cal_index ON public.calculations USING btree (id);


--
-- Name: id_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_calc_rep_index ON public.report_calculations USING btree (id);


--
-- Name: id_molecular_fingerprint_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_molecular_fingerprint_index ON public.molecular_fingerprints USING btree (id);


--
-- Name: id_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_pro_index ON public.projects USING btree (id);


--
-- Name: id_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_rep_index ON public.reports USING btree (id);


--
-- Name: id_report_type_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX id_report_type_index ON public.report_type USING btree (id);


--
-- Name: inchi_key_molecular_fingerprint_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX inchi_key_molecular_fingerprint_index ON public.molecular_fingerprints USING btree (inchi_key);


--
-- Name: modification_time_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX modification_time_pro_index ON public.projects USING btree (modification_time);


--
-- Name: name_cal_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX name_cal_index ON public.calculations USING btree (name);


--
-- Name: name_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX name_pro_index ON public.projects USING btree (name);


--
-- Name: name_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX name_rep_index ON public.reports USING btree (name);


--
-- Name: owner_group_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX owner_group_pro_index ON public.projects USING btree (owner_group_id);


--
-- Name: owner_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX owner_rep_index ON public.reports USING btree (owner_id);


--
-- Name: owner_user_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX owner_user_pro_index ON public.projects USING btree (owner_user_id);


--
-- Name: path_cal_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX path_cal_index ON public.calculations USING btree (path);


--
-- Name: path_name_cal_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX path_name_cal_index ON public.calculations USING btree (path, name);


--
-- Name: path_name_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX path_name_pro_index ON public.projects USING btree (path, name);


--
-- Name: path_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX path_pro_index ON public.projects USING btree (path);


--
-- Name: perm_pro_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX perm_pro_index ON public.projects USING btree (permissions);


--
-- Name: rep_id_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX rep_id_calc_rep_index ON public.report_calculations USING btree (report_id);


--
-- Name: title_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX title_calc_rep_index ON public.report_calculations USING btree (title);


--
-- Name: title_rep_index; Type: INDEX; Schema: public; Owner: iochembd
--

CREATE INDEX title_rep_index ON public.reports USING btree (title);


--
-- Name: areas_file_ref areas_file_ref_calculation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.areas_file_ref
    ADD CONSTRAINT areas_file_ref_calculation_id_fkey FOREIGN KEY (calculation_id) REFERENCES public.calculations(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: calculations calculations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.calculations
    ADD CONSTRAINT calculations_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.calculation_types(id);


--
-- Name: cutting_area_definitions cutting_area_definitions_calculation_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.cutting_area_definitions
    ADD CONSTRAINT cutting_area_definitions_calculation_type_id_fkey FOREIGN KEY (calculation_type_id) REFERENCES public.calculation_types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: molecular_fingerprints molecular_fingerprints_calc_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.molecular_fingerprints
    ADD CONSTRAINT molecular_fingerprints_calc_id_fk FOREIGN KEY (calculation_id) REFERENCES public.calculations(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report_calculations report_calculations_calc_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_calculations
    ADD CONSTRAINT report_calculations_calc_id_fk FOREIGN KEY (calc_id) REFERENCES public.calculations(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report_calculations report_calculations_report_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.report_calculations
    ADD CONSTRAINT report_calculations_report_id_fkey FOREIGN KEY (report_id) REFERENCES public.reports(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reports report_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT report_type_id_fk FOREIGN KEY (type) REFERENCES public.report_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: pg_database_owner
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


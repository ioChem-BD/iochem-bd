#!/bin/bash

# This script should be run after installing ioChem-BD software to allow it run on the https port (443)
# It is not necessary its execution in case the software is installed on ports > 1024 (not privileged)
#
# Requirements:
#   - A Java 17 runtime should be installed in the machine 
#   - It must be run as root user
#   - setcap package should be installed
#

# Check if the script is run by the root user
if [ "$(id -u)" -ne 0 ]; then
  echo "This script must be run as root"
  exit 1
fi

# Determine the Java home by finding the path of the default Java command
JAVA_CMD=$(readlink -f $(which java))
if [ -z "$JAVA_CMD" ]; then
  echo "Unable to find the Java command in the system."
  exit 1
fi

# Assuming standard installation structure to find JAVA_HOME from JAVA_CMD
JAVA_HOME=$(dirname $(dirname "$JAVA_CMD"))

# Validate JAVA_HOME just in case the path resolution did not work correctly
if [ ! -d "$JAVA_HOME" ] || [ ! -x "$JAVA_HOME/bin/java" ]; then
  echo "Failed to determine JAVA_HOME or JAVA_HOME does not point to a valid Java runtime."
  exit 1
fi

# Check Java version is 17
JAVA_VERSION=$("$JAVA_HOME/bin/java" -version 2>&1 | awk -F '"' '/version/ {print $2}' | awk -F'.' '{print $1}')
if [ "$JAVA_VERSION" -ne 17 ]; then
  echo "Java version is not 17. Detected version is $JAVA_VERSION."
  exit 1
fi

# Set the required capabilities on the java executable
if ! setcap cap_net_bind_service+ep "$JAVA_HOME/bin/java"; then
  echo "Failed to set capabilities on the Java executable."
  exit 1
fi

# Create or overwrite java-libjli.conf and run ldconfig
if ! echo "$JAVA_HOME/lib" > /etc/ld.so.conf.d/java-libjli.conf; then
  echo "Failed to write to /etc/ld.so.conf.d/java-libjli.conf."
  exit 1
fi

if ! ldconfig; then
  echo "ldconfig command failed."
  exit 1
fi

echo "Configuration completed successfully."


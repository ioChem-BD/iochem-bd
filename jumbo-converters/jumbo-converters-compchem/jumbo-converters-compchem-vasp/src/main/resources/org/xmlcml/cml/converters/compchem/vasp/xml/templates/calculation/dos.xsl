<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <!--<xs:include href="../helper.xsl"/>-->
                
    <xsl:template match="./dos" mode="calculation" >        
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="dos">
                  
            </comment>
        </xsl:if>  
        
        <module id="dos" cmlx:templateRef="dos">
            <list id="totals">
                <xsl:for-each select="./total/array/set/set">                   
                    <xsl:variable name="energy">
                            <xsl:for-each select="./r">
                                <xsl:value-of select="(tokenize(helper:trim(.),'\s+'))[1]"/><xsl:text> </xsl:text>                                                                
                            </xsl:for-each>
                    </xsl:variable>
                    <xsl:variable name="total">
                        <xsl:for-each select="./r">
                            <xsl:value-of select="(tokenize(helper:trim(.),'\s+'))[2]"/><xsl:text> </xsl:text>                                                                
                        </xsl:for-each>
                    </xsl:variable>
            	    <list id="{./@comment}">
            	        <array id="energy" datatType="xsd:double" size="{helper:countNumberOfValues($energy)}"><xsl:copy-of select="helper:trim($energy)"/></array>
            	        <array id="total"  datatType="xsd:double" size="{helper:countNumberOfValues($total)}"><xsl:copy-of select="helper:trim($total)"/></array>        	                    	        
            	    </list>        	            	    
            	</xsl:for-each>
            </list>
            <list id="partials">
                <xsl:variable name="fieldNumber" select="count(./partial/array/field)"/>
                <xsl:copy-of select="helper:arrayFromStringNodes('v:dosFields', 'xsd:string',./partial/array/field)"></xsl:copy-of>
                <xsl:for-each select="./partial/array/set/set">                    
                    <list>
                        <scalar id="count" dataType="xsd:integer"><xsl:value-of select="(tokenize(./@comment,'\s+'))[2]"/></scalar>
                        <xsl:for-each select="./set">
                            <list>
                                <xsl:choose>
                                    <xsl:when test="compare(./@comment,'spin 1') = 0"><scalar id="spin" datatType="xsd:string">alpha</scalar></xsl:when>
                                    <xsl:when test="compare(./@comment,'spin 2') = 0"><scalar id="spin" datatType="xsd:string">beta</scalar></xsl:when>
                                </xsl:choose>                            
                                <xsl:copy-of select="helper:matrixFromStringNodes('v:dos','xsd:double',./r, count(./r), $fieldNumber )"/>                                                           
                            </list>
                        </xsl:for-each>
                    </list>                    
                </xsl:for-each>
                
            </list>
        <!-- 
            <xsl:variable name="generation" select="./generation"/>
            <xsl:if test="exists($generation)">                            
                <module id="generation" cmlx:templateRef="generation">
                    <scalar dataType="xsd:string" dictRef="v:generation"><xsl:value-of select="$generation/@param"/></scalar>                    
                    <xsl:copy-of select="helper:arrayFromStringNodes ('v:usershift','xsd:double',$generation/v[@name='usershift'])"/>
                    <xsl:copy-of select="helper:arrayFromStringNodes ('v:divisions','xsd:double',$generation/v[@name='divisions'])"/>
                    <xsl:copy-of select="helper:matrixFromStringNodes('v:genvects','xsd:double',$generation/v[matches(@name,'genvec.*')], -1 , 3)"/>
                    <xsl:copy-of select="helper:arrayFromStringNodes ('v:shift','xsd:double',$generation/v[@name='shift'])"/>                   
                </module>
                <xsl:copy-of select="helper:matrixFromStringNodes('v:kpointlist','xsd:double',./varray[@name='kpointlist'], -1 , 3)"/>
            </xsl:if>  
            -->                                  
        </module>
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="kpoints">
                <module id="dos" cmlx:templateRef="dos">
                               
                </module>
            </comment>
        </xsl:if>  

    </xsl:template>
    
</xsl:stylesheet>
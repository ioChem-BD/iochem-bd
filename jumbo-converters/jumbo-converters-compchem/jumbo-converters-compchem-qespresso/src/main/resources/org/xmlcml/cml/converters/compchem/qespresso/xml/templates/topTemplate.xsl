<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"   
    xmlns="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <xsl:param name="append"/>
    <xsl:include href="helper.xsl"/>   
    
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="isVerbose"/>
    <xsl:template match="/">
        <module cmlx:templateRef="qespresso" xmlns="http://www.xml-cml.org/schema">
        	<module cmlx:templateRef="job"> 
	            <xsl:apply-templates select="Root/*">
	                <xsl:with-param  name="isVerbose" select="$isVerbose" tunnel="yes"/>                
	            </xsl:apply-templates>                        	           	   
            </module>
            <xsl:if test="$append != ''">
                <module cmlx:templateRef="externalModules">                 
                    <xsl:value-of select="$append" disable-output-escaping="yes"/>                    
                </module>
            </xsl:if>            
        </module>
    </xsl:template>
    
    <xsl:include href="header.xsl"/>
    <xsl:include href="cell.xsl"/>
    <xsl:include href="ions.xsl"/>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>  
    
        
</xsl:stylesheet>
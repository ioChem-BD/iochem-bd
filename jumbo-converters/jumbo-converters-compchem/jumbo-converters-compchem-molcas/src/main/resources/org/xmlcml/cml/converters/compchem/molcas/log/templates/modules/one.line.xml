<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<templateList>


	<template id="functional" name="Functional header" pattern=".*iterations:\sEnergy\sand\sconvergence\sstatistics.*" endPattern=".*">
		<comment class="example.input" id="functional">
	                          RASSCF iterations: Energy and convergence statistics	
		</comment>
	
		<record>\s*(UHF)?{A,cc:functional}iterations:\sEnergy\sand\sconvergence\sstatistics.*</record>
		<transform process="pullup" xpath=".//cml:scalar"/>
		<transform process="delete" xpath=".//cml:module"/>
		<transform process="delete" xpath=".//cml:list"/>
		
		<comment class="example.output" id="functional">
	         <module cmlx:templateRef="functional" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	            <scalar dataType="xsd:string" dictRef="cc:functional">RASSCF</scalar>
	         </module>	
		</comment>	
	</template>
	
	
	
	<template id="stopdate" name="Module stop date" pattern="\s*\-\-\-\sStop\sModule.*" endPattern=".*" endPattern2="~">
		<comment class="example.input" id="stopdate">
--- Stop Module:  seward at Fri Mar 23 12:08:18 2012 /rc=0 ---		
		</comment>
		
		<record>\s*\-\-\-\sStop\sModule.*at{X,cc:jobdatetime.end}/.*</record>		
		<transform process="pullup" xpath=".//cml:scalar"/>
		<transform process="delete" xpath=".//cml:module"/>
		<transform process="delete" xpath=".//cml:list"/>	
		
		<comment class="example.output" id="functional">
	         <module cmlx:templateRef="stopdate" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	            <scalar dataType="xsd:string" dictRef="cc:jobdatetime.end">Fri Mar 23 12:08:18 2012</scalar>
	         </module>		
		</comment>
	</template>
	
	
	<template id="molcharge" name="Molecular charge" pattern="\s*Total\smolecular\scharge.*" endPattern=".*">
		<comment class="example.input" id="molcharge">
      		Total molecular charge    2.00	 
	
		</comment>
			
		<record>\s*Total\smolecular\scharge{F,m:charge}</record>
		<transform process="pullup" xpath=".//cml:scalar"/>
		<transform process="delete" xpath=".//cml:list" /> 			
						
		<comment  class="example.output" id="molcharge">
			<module cmlx:templateRef="molcharge" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	            <scalar dataType="xsd:double" dictRef="m:charge">2.00</scalar>
	        </module>
		</comment>
	</template>
	<template id="molcharge" name="Molecular charge 2" pattern="\s*Total\s*charge=.*" endPattern=".*">
		<comment class="example.input" id="molcharge">
              Total            charge=   -4.000000
		</comment>
			
		<record>\s*Total\s*charge={F,m:charge}</record>
		<transform process="pullup" xpath=".//cml:scalar"/>
		<transform process="delete" xpath=".//cml:list" /> 			
			
		<comment  class="example.output" id="molcharge">
			<module cmlx:templateRef="molcharge" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	            <scalar dataType="xsd:double" dictRef="m:charge">-4.000000</scalar>
	        </module>
		</comment>
	</template>
	
	


	<template id="finalstateener" name="Final state energies" pattern="\s*Final\sstate\senergy.*" endPattern=".*a\.u\.\s*$\s*" endPattern2=".*[0-9]\s*$\s*" repeat="*" endOffset="1">
		<comment class="example.input" id="finalstateener">
      Final state energy(ies):
      ------------------------

      root number  1 E =     -11393.90670314 a.u.
      root number  2 E =     -11393.82368248 a.u.
      root number  3 E =     -11393.76352244 a.u.
      
		</comment>
		<comment class="example.input" id="finalstateener2">
      Final state energy(ies):
      ------------------------

::    RASSCF root number  1 Total energy =       -749.24816103
	
		</comment>
		
		<record repeat="3"/>
		<record>.*={F,m:finalstateenergy}(a\.u\.)?</record>
		<transform process="move" xpath=".//cml:scalar" to="."/>
		<transform process="delete" xpath=".//cml:list" />
		<transform process="addUnits" xpath=".//cml:scalar" value="nonsi:hartree"/>		
		
		<comment class="example.output" id="finalstateener">
			<module cmlx:templateRef="finalstateener" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
			    <scalar dataType="xsd:double" dictRef="m:finalstateenergy" units="nonsi:hartree">-11393.90670314</scalar>            	
         	</module>
		</comment>

		<comment class="example.output" id="finalstateener2">
	         <module cmlx:templateRef="finalstateener" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	         	<scalar dataType="xsd:double" dictRef="cc:finalenergy" units="nonsi:hartree">-749.24816103</scalar>
	         </module>
		</comment>
	</template>
	
	
	<template id="ccsdt" name="CCSD(T) energy section" pattern="\s*Final\sResults\s:.*" endPattern="\s*CCSD\(T\)\scorr.*" endOffset="1">
		<comment class="example.input" id="ccsdt">
 Final Results : 

 (T)     corr. = -0.035740391008
 CCSD(T) corr. = -0.900634357896
		</comment>

		<record repeat="2"/>
		<record>\s*\(T\)\s*corr\.\s*={F,m:tcorrenergy}</record>
		<record>\s*CCSD\(T\)\s*corr\.\s*={F,m:ccsdtcorrenergy}</record>
		<transform process="pullup" xpath=".//cml:scalar"/>
		<transform process="delete" xpath=".//cml:list"/>
		
		<comment class="example.output" id="ccsdt">
			<module cmlx:templateRef="ccsdt"  xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
			   <scalar dataType="xsd:double" dictRef="m:tcorrenergy">-0.035740391008</scalar>
			   <scalar dataType="xsd:double" dictRef="m:ccsdtcorrenergy">-0.900634357896</scalar>
			</module>
		</comment>		
	</template>  
</templateList>
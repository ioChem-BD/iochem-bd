<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<template id="cosmo" name="Cosmo section" pattern="\s*-{10,}$\s*COSMO\sINITIALIZATION.*" endPattern="\s*-{10,}$\s*\w.*" repeat="*">

<comment class="example.input" id="cosmo">
--------------------
COSMO INITIALIZATION
--------------------

Epsilon                                      ...   80.4000
Refractive Index                             ...    1.3300
Potential Evaluation                         ...  ANALYTIC
Technical COSMO parameters:
  rsolv                                      ...    1.3000
  routf                                      ...    0.8500
  disex                                      ...   10.0000
  nppa                                       ... 1082   
  nspa                                       ...   92   
Radii:
 Radius for O  used is    3.2503 Bohr (=   1.7200 Ang.)
 Radius for C  used is    3.7795 Bohr (=   2.0000 Ang.)
 Radius for H  used is    2.4566 Bohr (=   1.3000 Ang.)
Initializing COSMO package            ... done
Setting COSMO radii                   ... done
Setting atoms and making cavity       ...  USING 1082 GRID
done
Cholesky decomposition of A           ... done
--------------
</comment>

	<templateList>
		<template pattern="\s*Technical\sCOSMO\sparameters.*" endPattern="\s*Radii.*" endPattern2="\w.*" endPattern3="~" repeat="*">
			<record/>
			<record id="technical" repeat="*">{X,cc:parameter}\.\.\.{X,cc:value}</record>
		</template>
		<template pattern="\s*Radii:\s*" endPattern="\s*(?!.*Radius).+" endPattern2="~" repeat="*">
			<record/>
			<record id="radii" repeat="*" makeArray="true">\s*Radius\sfor{A,cc:elementType}used\sis.*\(={F,o:radius}.*</record>
		</template>
		<template id="parameter" pattern=".*\.\.\..*" endPattern=".*" endPattern2="~" repeat="*">
			<record>{X,cc:parameter}\.\.\.{X,cc:value}</record>
		</template>
	</templateList>
	
	<transform process="move" xpath=".//cml:module[@cmlx:templateRef='parameter']/cml:list/cml:list"  to="." />
 	<transform process="pullup" xpath=".//cml:list[@cmlx:templateRef='radii']"/>
 	<transform process="pullup" xpath=".//cml:list[@cmlx:templateRef='technical']"/>
 	
 	<transform process="delete" xpath=".//cml:list[count(*)=0]" />
 	<transform process="delete" xpath=".//cml:module[count(*)=0]" />
 	
 	<transform process="delete" xpath=".//cml:module" />
 	<transform process="addUnits" xpath=".//cml:array[@dictRef='o:radius']" value="nonsi:angstrom" />

	<transform process="addChild" elementName="cml:list" xpath="." dictRef="parameters"/>
	<transform process="addAttribute" xpath=".//cml:list[@dictRef='parameters']" name="cmlx:templateRef" value="parameters" />
 	<transform process="move" xpath="./cml:list[not(@cmlx:templateRef)]" to="./cml:list[@cmlx:templateRef='parameters']" />

	<comment class="example.output" id="cosmo">
		<module cmlx:templateRef="cosmo" dictRef="cc:userDefinedModule" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
		   <list cmlx:templateRef="technical">
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">rsolv</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">1.3000</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">routf</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">0.8500</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">disex</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">10.0000</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">nppa</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">1082</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">nspa</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">92</scalar>
		      </list>
		   </list>
		   <list cmlx:templateRef="radii">
		      <array dataType="xsd:string" dictRef="cc:elementType" size="3">O C H</array>
		      <array dataType="xsd:double" dictRef="o:radius" size="3" units="nonsi:angstrom">1.7200 2.0000 1.3000</array>
		   </list>
		   <list cmlx:templateRef="parameters" dictRef="parameters">
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Epsilon</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">80.4000</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Refractive Index</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">1.3300</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Potential Evaluation</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">ANALYTIC</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Initializing COSMO package</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">done</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Setting COSMO radii</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">done</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Setting atoms and making cavity</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">USING 1082 GRID</scalar>
		      </list>
		      <list>
		         <scalar dataType="xsd:string" dictRef="cc:parameter">Cholesky decomposition of A</scalar>
		         <scalar dataType="xsd:string" dictRef="cc:value">done</scalar>
		      </list>
		   </list>
		</module>
	</comment>	
 	
</template>


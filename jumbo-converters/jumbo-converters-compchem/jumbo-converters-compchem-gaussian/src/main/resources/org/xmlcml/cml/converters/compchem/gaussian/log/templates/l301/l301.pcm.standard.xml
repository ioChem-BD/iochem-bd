<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<template id="l301.pcm.standard" pattern="\sPolarizable\sContinuum\sModel\s+\(PCM\).*" endPattern="\s*\-+.*" name="Polarizable continuum model - standard" endOffset="1" repeat="*">
<comment class="example.inputx" id="l301.pcm.standard1">
 Polarizable Continuum Model (PCM)
 =================================
 Model                : PCM.
 Atomic radii         : UA0 (Simple United Atom Topological Model).
 Polarization charges : Total charges.
 Charge compensation  : None.
 Solution method      : Matrix inversion.
 Cavity               : GePol (RMin=0.200 OFac=0.890).
                        Default sphere list used, NSphG=   54.
                        Tesserae with average area of 0.200 Ang**2.
 Solvent              : Acetone, Eps     =  20.700000
                               Eps(inf)=   1.841000
                               RSolv   =   2.380000 Ang.
 ------------------------------------------------------------------------------
</comment>

<comment class="example.inputx" id="l301.pcm.standard2">
 Polarizable Continuum Model (PCM)
 =================================
 Model                : PCM.
 Atomic radii         : UA0 (Simple United Atom Topological Model).
 Polarization charges : Total charges.
 Charge compensation  : None.
 Solution method      : Matrix inversion.
 Cavity               : GePol (RMin=0.200 OFac=0.890).
                        Default sphere list used, NSphG=   45.
                        Tesserae with average area of 0.200 Ang**2.
 Solvent              : non-standard Water,
                        Eps                           =  37.800000
                        Eps(infinity)                 =   1.776000
                        d(Eps)/dT                     =  -0.356200 K**-1
                        Molar volume                  =  18.070000 Ang**3
                        Numeral density               =   0.033480 Ang**-3
                        Absolute temperature          = 298.150000 K
                        Thermal expansion coefficient =   0.000257 K**-1
                        RSolv                         =   2.642000 Ang.
 ------------------------------------------------------------------------------
</comment>


<comment class="example.inputx" id="l301.pcm.standard3">
 Polarizable Continuum Model (PCM)
 =================================
 Model                : PCM.
 Atomic radii         : SMD-Coulomb.
 Polarization charges : Total charges.
 Charge compensation  : None.
 Solution method      : Matrix inversion.
 Cavity type          : VdW (van der Waals Surface) (Alpha=1.000).
 Cavity algorithm     : GePol (No added spheres)
                        Default sphere list used, NSphG=    2.
                        Lebedev-Laikov grids with approx.  5.0 points / Ang**2.
                        Smoothing algorithm: Karplus/York (Gamma=1.0000).
                        Polarization charges: spherical gaussians, with
                                              point-specific exponents (IZeta= 3).
                        Self-potential: point-specific (ISelfS= 7).
                        Self-field    : sphere-specific E.n sum rule (ISelfD= 2).
 1st derivatives      : Analytical E(r).r(x)/FMM algorithm (CHGder, D1EAlg=3).
                        Cavity 1st derivative terms included.
 Solvent              : Water, Eps=  78.355300 Eps(inf)=   1.777849
 ------------------------------------------------------------------------------
</comment>


<comment class="example.inputx" id="l301.pcm.standard4">
 Polarizable Continuum Model (PCM)
 =================================
 Model                : C-PCM.
 Atomic radii         : UFF (Universal Force Field).
 Polarization charges : Total charges.
 Charge compensation  : None.
 Solution method      : Matrix inversion.
 Cavity type          : Scaled VdW (van der Waals Surface) (Alpha=1.100).
 Cavity algorithm     : GePol (No added spheres)
                        Default sphere list used, NSphG=    2.
                        Lebedev-Laikov grids with approx.  5.0 points / Ang**2.
                        Smoothing algorithm: Karplus/York (Gamma=1.0000).
                        Polarization charges: spherical gaussians, with
                                              point-specific exponents (IZeta= 3).
                        Self-potential: point-specific (ISelfS= 7).
                        Self-field    : sphere-specific E.n sum rule (ISelfD= 2).
 1st derivatives      : Analytical E(r).r(x)/FMM algorithm (CHGder, D1EAlg=3).
                        Cavity 1st derivative terms included.
 Solvent              : Water, Eps=  78.355300 Eps(inf)=   1.777849
 ------------------------------------------------------------------------------
</comment>

<comment class="example.inputx" id="l301.pcm.standard5">
 Polarizable Continuum Model (PCM)
 =================================
 Model                : PCM.
 Atomic radii         : UFF (Universal Force Field).
 Polarization charges : Total charges.
 Charge compensation  : None.
 Solution method      : Matrix inversion.
 Cavity type          : Scaled VdW (van der Waals Surface) (Alpha=1.100).
 Cavity algorithm     : GePol (No added spheres)
                        Default sphere list used, NSphG=    2.
                        Lebedev-Laikov grids with approx.  5.0 points / Ang**2.
                        Smoothing algorithm: Karplus/York (Gamma=1.0000).
                        Polarization charges: spherical gaussians, with
                                              point-specific exponents (IZeta= 3).
                        Self-potential: point-specific (ISelfS= 7).
                        Self-field    : sphere-specific E.n sum rule (ISelfD= 2).
 1st derivatives      : Analytical E(r).r(x)/FMM algorithm (CHGder, D1EAlg=3).
                        Cavity 1st derivative terms included.
 Solvent              : Water, Eps=  78.355300 Eps(inf)=   1.777849
 ------------------------------------------------------------------------------
</comment>




	<record repeat="2"/>
	<record id="model">\sModel\s+\:\s*{X,g:model}\..*</record>
	<record id="atomicradii">\sAtomic\sradii\s+\:\s*{A,g:atomicradii}\.*\s*(\(.*\))*\.*\s*</record>
	<record id="polarcharges">\sPolarization\scharges\s+\:\s*{X,g:polarcharges}\.\s*</record>
	<record id="chargecompensation">\sCharge\scompensation\s+\:\s*{X,g:chargecompensation}\.\s*</record>
	<record id="solutionmethod">\sSolution\smethod\s+\:\s*{X,g:solutionmethod}\.\s*</record>
	<templateList>
		<template pattern="\s+Solvent.*Eps.*Eps.*" endPattern=".*" endOffset="0" repeat="*">
			<record id="solventeps">\s+Solvent\s+\:{X,g:solvent},\s+Eps\s*\=\s*{F,g:eps}\s*Eps\(inf\)\=\s*{F,g:epsinfinity}\s*</record>				
		</template>
		<template pattern="\s+Solvent\s+\:\s+.*Eps.*" endPattern=".*" endOffset="0">
			<record id="solventeps">\s+Solvent\s+\:\s+{X,g:solvent},\s+Eps\s+\=\s+{F,g:eps}</record>
		</template>	
		<template pattern="^\s+Solvent((?!Eps).)*$" endPattern=".*" endOffset="0">
			<record id="solvent">\s+Solvent\s*\:\s*{X,g:solvent}\s*,.*</record>		
		</template>
		<template pattern="\s+Eps\s*\=.*" endPattern=".*" endOffset="0">
			<record id="eps">\s+Eps\s*\={F,g:eps}</record>		
		</template>
		<template pattern="\s+Eps\(inf.*" endPattern=".*" endOffset="0">				
			<record id="epsinfinity">.*\={F,g:epsinfinity}</record>
		</template>				
		<template pattern="^(?!(\s*Solvent)|(\s*Eps)).*" endPattern=".*" endPattern2="~" repeat="*" endOffset="0">
			<record/>
		</template>							 											
	</templateList>
	
	<transform process="pullup" xpath="./cml:module/cml:list/cml:list/cml:scalar"/>
	<transform process="pullup" xpath="./cml:module/cml:list/cml:scalar" repeat="2"/>
	<transform process="pullup" xpath="./cml:list/cml:scalar" repeat="1"/>
	<transform process="delete" xpath="./descendant-or-self::cml:list[count(*)=0]"/>	
	<transform process="delete" xpath="./descendant-or-self::cml:list[count(*)=0]"/>
	<transform process="delete" xpath="./descendant-or-self::cml:module[count(*)=0]"/>
	
	<transform process="delete" xpath="./cml:module[@cmlx:templateRef='NULL_ID']"/>

	
	
	<comment class="example.output" id="l301.pcm.standard1">
		<module cmlx:lineCount="14" cmlx:templateRef="l301.pcm.standard" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	         <scalar dataType="xsd:string" dictRef="g:model">PCM</scalar>
	         <scalar dataType="xsd:string" dictRef="g:atomicradii">UA0</scalar>
	         <scalar dataType="xsd:string" dictRef="g:polarcharges">Total charges</scalar>
	         <scalar dataType="xsd:string" dictRef="g:chargecompensation">None</scalar>
	         <scalar dataType="xsd:string" dictRef="g:solutionmethod">Matrix inversion</scalar>
	         <scalar dataType="xsd:string" dictRef="g:solvent">Acetone</scalar>
	         <scalar dataType="xsd:double" dictRef="g:eps">20.7</scalar>
	         <scalar dataType="xsd:double" dictRef="g:epsinfinity">1.841</scalar>
		</module>	
	</comment>	
	
	<comment class="example.output" id="l301.pcm.standard2">
		<module cmlx:lineCount="20" cmlx:templateRef="l301.pcm.standard" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	        <scalar dataType="xsd:string" dictRef="g:model">PCM</scalar>
	        <scalar dataType="xsd:string" dictRef="g:atomicradii">UA0</scalar>
	        <scalar dataType="xsd:string" dictRef="g:polarcharges">Total charges</scalar>
	        <scalar dataType="xsd:string" dictRef="g:chargecompensation">None</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solutionmethod">Matrix inversion</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solvent">non-standard Water</scalar>
	        <scalar dataType="xsd:double" dictRef="g:eps">37.8</scalar>
	        <scalar dataType="xsd:double" dictRef="g:epsinfinity">1.776</scalar>
		</module>	
	</comment>
	
	<comment class="example.output" id="l301.pcm.standard3">
		<module cmlx:lineCount="20" cmlx:templateRef="l301.pcm.standard" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	        <scalar dataType="xsd:string" dictRef="g:model">PCM</scalar>
	        <scalar dataType="xsd:string" dictRef="g:atomicradii">SMD-Coulomb.</scalar>
	        <scalar dataType="xsd:string" dictRef="g:polarcharges">Total charges</scalar>
	        <scalar dataType="xsd:string" dictRef="g:chargecompensation">None</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solutionmethod">Matrix inversion</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solvent">Water</scalar>
	        <scalar dataType="xsd:double" dictRef="g:eps">78.3553</scalar>
	        <scalar dataType="xsd:double" dictRef="g:epsinfinity">1.777849</scalar>
		</module>
			
	</comment>
	
	<comment class="example.output" id="l301.pcm.standard4">
		<module cmlx:lineCount="20" cmlx:templateRef="l301.pcm.standard" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">		    
	        <scalar dataType="xsd:string" dictRef="g:model">C-PCM</scalar>
	        <scalar dataType="xsd:string" dictRef="g:atomicradii">UFF</scalar>
	        <scalar dataType="xsd:string" dictRef="g:polarcharges">Total charges</scalar>
	        <scalar dataType="xsd:string" dictRef="g:chargecompensation">None</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solutionmethod">Matrix inversion</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solvent">Water</scalar>
	        <scalar dataType="xsd:double" dictRef="g:eps">78.3553</scalar>
	        <scalar dataType="xsd:double" dictRef="g:epsinfinity">1.777849</scalar>
		</module>	
	</comment>
	
	<comment class="example.output" id="l301.pcm.standard5">		
		<module cmlx:lineCount="20" cmlx:templateRef="l301.pcm.standard" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	        <scalar dataType="xsd:string" dictRef="g:model">PCM</scalar>
	        <scalar dataType="xsd:string" dictRef="g:atomicradii">UFF</scalar>
	        <scalar dataType="xsd:string" dictRef="g:polarcharges">Total charges</scalar>
	        <scalar dataType="xsd:string" dictRef="g:chargecompensation">None</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solutionmethod">Matrix inversion</scalar>
	        <scalar dataType="xsd:string" dictRef="g:solvent">Water</scalar>
	        <scalar dataType="xsd:double" dictRef="g:eps">78.3553</scalar>
	        <scalar dataType="xsd:double" dictRef="g:epsinfinity">1.777849</scalar>
		</module>	
	</comment>		              
</template>
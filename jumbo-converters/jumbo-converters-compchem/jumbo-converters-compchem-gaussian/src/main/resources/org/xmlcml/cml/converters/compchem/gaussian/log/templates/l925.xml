<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<template id="l925" name="Excited State Electron Transfer (EET) model"
  pattern="\s+\(Enter.*l925.exe\).*" pattern2="\s*Electronic\sCoupling\sfor\sExcitation\sEnergy\sTranfer.*"   
  endPattern="\s+Leave\sLink\s+925.*" endPattern2="\s*Solvent\sscreening.*$\s*$\s*((?!Frag).)*" 
  endOffset="1" xmlns:xi="http://www.w3.org/2001/XInclude">

  <comment class="example.input" id="l925"> 
(Enter /prod/apps/gaussian/g16b1/l925.exe)
 ===============================================================================

                Electronic Coupling for Excitation Energy Tranfer

 ===============================================================================
 ...
 
 Frag=  2 State=  1 (w=  2.0665 eV) &lt;=&gt; Frag=  1 State=  1 (w=  2.0804 eV)

   delta-w                   = -0.013942398 eV
   Coulomb                   =  0.011157674 eV
   Exact-exchange            =  0.000011525 eV
   Exchange-correlation      =  0.000010188 eV
   w-avg*Overlap             = -0.000101217 eV (w-avg=  2.0734 eV, Ovlp=-0.488162D-04)
   Explicit solvent-mediated = -0.004833838 eV
   Total coupling            =  0.006244332 eV
   Solvent screening         =  0.563660992

...
 Frag=  2 State=  6 (w=  3.6275 eV) &lt;=&gt; Frag=  1 State=  4 (w=  3.4316 eV)

   delta-w                   =  0.195884381 eV
   Coulomb                   = -0.010913268 eV
   Exact-exchange            =  0.000010434 eV
   Exchange-correlation      =  0.000007568 eV
   w-avg*Overlap             =  0.000074981 eV (w-avg=  3.5295 eV, Ovlp= 0.212440D-04)
   Explicit solvent-mediated =  0.004456485 eV
   Total coupling            = -0.006363800 eV
   Solvent screening         =  0.588136054

 Frag=  2 State=  6 (w=  3.6275 eV) &lt;=&gt; Frag=  1 State=  5 (w=  3.4728 eV)

   delta-w                   =  0.154702017 eV
   Coulomb                   = -0.007584035 eV
   Exact-exchange            =  0.000001175 eV
   Exchange-correlation      = -0.000000136 eV
   w-avg*Overlap             =  0.000064985 eV (w-avg=  3.5501 eV, Ovlp= 0.183051D-04)
   Explicit solvent-mediated =  0.003225158 eV
   Total coupling            = -0.004292852 eV
   Solvent screening         =  0.571009070

 Frag=  2 State=  6 (w=  3.6275 eV) &lt;=&gt; Frag=  1 State=  6 (w=  3.5565 eV)

   delta-w                   =  0.070920227 eV
   Coulomb                   =  0.000420163 eV
   Exact-exchange            =  0.000001654 eV
   Exchange-correlation      =  0.000000121 eV
   w-avg*Overlap             = -0.000003315 eV (w-avg=  3.5920 eV, Ovlp=-0.922768D-06)
   Explicit solvent-mediated = -0.000163157 eV
   Total coupling            =  0.000255466 eV
   Solvent screening         =  0.610253206
 
 Leave Link  925 at Fri Jan  8 20:59:16 2021, MaxMem=  1572864000 cpu:           36670.9 elap:            4667.3
  </comment>

	<templateList>
		<template id="ecoupling" pattern="\s*Frag=.*State=.*" endPattern="\s*Solvent\sscreening.*" endOffset="1" repeat="*">
			<record>\s*Frag={I,g:fragmentA}State={I,g:stateA}\(w={F,g:wA}eV\).*Frag={I,g:fragmentB}State={I,g:stateB}\(w={F,g:wB}eV\)</record>
			<record repeat="7" />
			<record>\s*Total\scoupling\s*={F,g:coupling}eV</record>		
		</template>		
	</templateList>

	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:fragmentA']" />
	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:stateA']" />
	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:wA']" />
	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:fragmentB']" />
	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:stateB']" />
	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:wB']" />
	<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='g:coupling']" />	
	<transform process="addUnits" xpath=".//cml:array[@dictRef='g:wA']" value="nonsi:electronvolt" />
	<transform process="addUnits" xpath=".//cml:array[@dictRef='g:wB']" value="nonsi:electronvolt" />
	<transform process="addUnits" xpath=".//cml:array[@dictRef='g:coupling']" value="nonsi:electronvolt" />
	
	<transform process="pullup" xpath=".//cml:list/cml:list/cml:array" repeat="2" />
	<transform process="pullup" xpath=".//cml:list/cml:array" />
		
	<transform process="delete" xpath=".//cml:list" />
	<transform process="delete" xpath=".//cml:module[@cmlx:templateRef='ecoupling' and count(*) = 0]" />
	
	
  	<comment class="example.output" id="l925">
		<module cmlx:templateRef="l925" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">      
			<module cmlx:templateRef="ecoupling">
			   <array dataType="xsd:integer" dictRef="g:fragmentA" size="36">2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2</array>
			   <array dataType="xsd:integer" dictRef="g:stateA" size="36">1 1 1 1 1 1 2 2 2 2 2 2 3 3 3 3 3 3 4 4 4 4 4 4 5 5 5 5 5 5 6 6 6 6 6 6</array>
			   <array dataType="xsd:double" dictRef="g:wA" units="nonsi:electronvolt" size="36">2.0665 2.0665 2.0665 2.0665 2.0665 2.0665 2.4099 2.4099 2.4099 2.4099 2.4099 2.4099 3.2359 3.2359 3.2359 3.2359 3.2359 3.2359 3.4128 3.4128 3.4128 3.4128 3.4128 3.4128 3.4582 3.4582 3.4582 3.4582 3.4582 3.4582 3.6275 3.6275 3.6275 3.6275 3.6275 3.6275</array>
			   <array dataType="xsd:integer" dictRef="g:fragmentB" size="36">1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1</array>
			   <array dataType="xsd:integer" dictRef="g:stateB" size="36">1 2 3 4 5 6 1 2 3 4 5 6 1 2 3 4 5 6 1 2 3 4 5 6 1 2 3 4 5 6 1 2 3 4 5 6</array>
			   <array dataType="xsd:double" dictRef="g:wB" units="nonsi:electronvolt" size="36">2.0804 2.3918 3.2653 3.4316 3.4728 3.5565 2.0804 2.3918 3.2653 3.4316 3.4728 3.5565 2.0804 2.3918 3.2653 3.4316 3.4728 3.5565 2.0804 2.3918 3.2653 3.4316 3.4728 3.5565 2.0804 2.3918 3.2653 3.4316 3.4728 3.5565 2.0804 2.3918 3.2653 3.4316 3.4728 3.5565</array>
			   <array dataType="xsd:double" dictRef="g:coupling" units="nonsi:electronvolt" size="36">0.006244332 -0.004300899 -0.005109001 -0.012362113 -0.005331146 0.001686512 -0.002427520 0.003171644 0.008520093 0.006203144 0.001016240 -0.001295363 -0.007851016 -0.004324292 -0.030453475 0.011951745 0.018745528 0.002907796 -0.010538382 0.010292411 0.018925171 0.024595170 0.008664806 -0.003482754 0.007543389 -0.001341289 0.000414214 -0.011650240 -0.007756489 0.000445679 0.003983123 -0.000769178 0.000281153 -0.006363800 -0.004292852 0.000255466</array>
			</module>
		</module>
	</comment>
</template>
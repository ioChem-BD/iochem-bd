<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ca="http://www.iochem-bd.org/dictionary/castep/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 11, 2022</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>
    </xd:doc>

    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:variable name='blankspace'><xsl:text> </xsl:text></xsl:variable>

    <xsl:template match="/">
        <module id="castep.chart">
            <module id="chart">
                <scalar dataType="xsd:string" dictRef="cc:title"><xsl:value-of select="/XCD/CHART_2D/CARTESIAN_2D_PLOT/TITLE/@Text"/></scalar>
                <list id="series">
                    <xsl:for-each select="/XCD/CHART_2D/DATA_2D/SERIES_2D">
                        <xsl:variable name="index" select="position()"/>
                        <list name="{./@Name}">
                            <scalar dictRef="x:linestyle"><xsl:value-of select="helper:toPlotlyStyle((//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_GRAPH_2D_STYLE/@LineStyle)[$index])"/></scalar>
                            <scalar dictRef="x:linecolor">rgb(<xsl:value-of select="(//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_GRAPH_2D_STYLE/@LineColor)[$index]"/>)</scalar>
                            <array dataType="xsd:double" dictRef="cc:x2" size="{./@NumPoints}"><xsl:for-each select="./POINT_2D/@XY"><xsl:value-of select="substring-before(.,',')"/><xsl:if test="position() != last()"><xsl:value-of select="$blankspace"/></xsl:if></xsl:for-each></array>
                            <array dataType="xsd:double" dictRef="cc:y2" size="{./@NumPoints}"><xsl:for-each select="./POINT_2D/@XY"><xsl:value-of select="substring-after(.,',')"/><xsl:if test="position() != last()"><xsl:value-of select="$blankspace"/></xsl:if></xsl:for-each></array>
                        </list>
                    </xsl:for-each>
                </list>
                <list id="axis">
                    <list id="x">
                        <scalar dataType="xsd:string" dictRef="cc:title"><xsl:value-of select="//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_AXES_2D/AXIS_X/@Title"/></scalar>
                        <xsl:if test="exists(//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_AXES_2D/AXIS_X/TICK_LABELS)">
                            <list id="ticks">
                                <xsl:for-each select="//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_AXES_2D/AXIS_X/TICK_LABELS/TICK_LABEL">
                                    <list id="tick">
                                        <scalar dataType="xsd:string" dictRef="cc:title"><xsl:value-of select="./@Text"/></scalar>
                                        <scalar dataType="xsd:double" dictRef="cc:x2"><xsl:value-of select="./@Position"/></scalar>
                                    </list>
                                </xsl:for-each>
                            </list>                                                        
                        </xsl:if>
                    </list>
                    <list id="y">
                        <scalar dataType="xsd:string" dictRef="cc:title"><xsl:value-of select="//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_AXES_2D/AXIS_Y/@Title"/></scalar>
                        <xsl:if test="exists(//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_AXES_2D/AXIS_Y/TICK_LABELS)">
                            <list id="ticks">
                                <xsl:for-each select="//XCD/CHART_2D/CARTESIAN_2D_PLOT/CARTESIAN_AXES_2D/AXIS_Y/TICK_LABELS/TICK_LABEL">
                                    <list id="tick">
                                        <scalar dataType="xsd:string" dictRef="cc:title"><xsl:value-of select="./@Text"/></scalar>
                                        <scalar dataType="xsd:double" dictRef="cc:y2"><xsl:value-of select="./@Position"/></scalar>
                                    </list>
                                </xsl:for-each>
                            </list>                                                        
                        </xsl:if>
                    </list>                    
                </list>
            </module>
        </module>
    </xsl:template>
    
    <xsl:function name="helper:toPlotlyStyle">
        <xsl:param name="style"/>        
        <xsl:choose>
            <xsl:when test="matches($style,'Solid')">solid</xsl:when>
            <xsl:when test="matches($style,'ShortDash')">dot</xsl:when>
            <xsl:when test="matches($style,'Dash')">dash</xsl:when>
            <xsl:when test="matches($style,'LongDashShortDash')">dashdot</xsl:when>            
            <xsl:otherwise>
                <xsl:text></xsl:text>  
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:function>
</xsl:stylesheet>
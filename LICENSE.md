# Licenses inside ioChem-BD

ioChem-BD project is released under the **GNU AFFERO GENERAL PUBLIC LICENSE, Version 3**, its full text is in the COPYING file at the root of this project and in [this webpage](https://www.gnu.org/licenses/agpl-3.0.html).

![APGLv3 logo](https://www.gnu.org/graphics/agplv3-with-text-162x68.png "GNU AFFERO GENERAL PUBLIC LICENSE, Version 3")

## Third party licenses

ioChem-BD uses third party libraries that are distributed under their own terms, see [*THIRD-PARTY.txt*](license/THIRD-PARTY.txt) for a detailed list grouped by license type. The full text of each license can be found inside *license/third-party* folder.

There are some other libraries and frameworks referenced in the HTML pages of ioChem-BD that the aforementioned third-party file doesn't cover. You can review their license in the following table:

| **Package** | **Version** |  **License** |
|--|--|--|
| [ZK CE](https://www.zkoss.org) | 8.5.1 | [LGPL version 3](http://www.gnu.org/licenses/lgpl-3.0.html) | 
| [Bootstrap](https://getbootstrap.com/) | 4.1.3 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [bootstrap-table](https://github.com/wenzhixin/bootstrap-table) | 1.11.0 |  [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [Popper](https://popper.js.org/) |  | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [jQuery](https://jquery.org) | 3.3.1 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [jQuery UI](http://jqueryui.com) | 1.12.1 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [jQuery blockUI plugin](http://malsup.com/jquery/block/) | 2.70.0-2014.11.23 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [Resumable](http://github.com/23/resumable.js) |  | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [Datatables](https://datatables.net) | 1.10.12 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [script.aculo.us](http://script.aculo.us) | 1.8.2 | [MIT License](http://www.opensource.org/licenses/mit-license.html) | 
| [JSmol](http://wiki.jmol.org/index.php/JSmol) | 14.29.25 | [LGPL](http://www.gnu.org/licenses/lgpl.html) |
| [Plotly.js](https://plot.ly/) | 1.49.0 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [Font Awesome](https://fontawesome.com) |  5.3.1 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [FXSL xslt Functional Programming Library](https://sourceforge.net/projects/fxsl)| 1.2 | [Mozilla Public License 1.1 (MPL 1.1)](https://www.mozilla.org/en-US/MPL/1.1/) |
| [Highslide](http://highslide.com/) | 5.0.0 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |
| [hopscotch](http://linkedin.github.io/hopscotch/) | 0.2.5 | [Apache License v2.0](http://www.apache.org/licenses/LICENSE-2.0) |
| [InChI library](https://www.inchi-trust.org/) | 1.05 | [LGPL version 3](http://www.gnu.org/licenses/lgpl-3.0.html) | 
| [D3.js](https://d3.js) | 5.15.0 | [BSD License](https://opensource.org/licenses/BSD-3-Clause) |
| [d3-graphviz plugin](https://github.com/magjac/d3-graphviz) | 3.1.0 | [BSD License](https://opensource.org/licenses/BSD-3-Clause) |
| [ACE editor](https://ace.c9.io/) | 1.4.11 | [BSD License](https://opensource.org/licenses/BSD-3-Clause) |
| [split.js](https://split.js.org/) | 1.5.9 | [MIT License](http://www.opensource.org/licenses/mit-license.html) |


## Third party software

ioChem-BD also relies on the following software libraries and frameworks to run and operate: 
   * [Java JDK and JRE](https://www.oracle.com/technetwork/java/javase/archive-139210.html)
   * [Apache Tomcat](https://tomcat.apache.org/whoweare.html)
   * [PostgreSQL database](https://www.postgresql.org/about/)
   * [InCHI library](https://www.inchi-trust.org/)
   * [gcc package](https://gcc.gnu.org/about.html)   

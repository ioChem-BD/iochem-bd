/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import edu.harvard.hul.ois.mets.Agent;
import edu.harvard.hul.ois.mets.AmdSec;
import edu.harvard.hul.ois.mets.Checksumtype;
import edu.harvard.hul.ois.mets.Div;
import edu.harvard.hul.ois.mets.DmdSec;
import edu.harvard.hul.ois.mets.FLocat;
import edu.harvard.hul.ois.mets.File;
import edu.harvard.hul.ois.mets.FileGrp;
import edu.harvard.hul.ois.mets.FileSec;
import edu.harvard.hul.ois.mets.Fptr;
import edu.harvard.hul.ois.mets.Loctype;
import edu.harvard.hul.ois.mets.MdRef;
import edu.harvard.hul.ois.mets.MdWrap;
import edu.harvard.hul.ois.mets.Mdtype;
import edu.harvard.hul.ois.mets.Mets;
import edu.harvard.hul.ois.mets.MetsHdr;
import edu.harvard.hul.ois.mets.Name;
import edu.harvard.hul.ois.mets.RightsMD;
import edu.harvard.hul.ois.mets.Role;
import edu.harvard.hul.ois.mets.StructMap;
import edu.harvard.hul.ois.mets.TechMD;
import edu.harvard.hul.ois.mets.Type;
import edu.harvard.hul.ois.mets.XmlData;
import edu.harvard.hul.ois.mets.helper.Any;
import edu.harvard.hul.ois.mets.helper.MdSec;
import edu.harvard.hul.ois.mets.helper.MetsException;
import edu.harvard.hul.ois.mets.helper.MetsIDElement;
import edu.harvard.hul.ois.mets.helper.MetsReader;
import edu.harvard.hul.ois.mets.helper.MetsValidator;
import edu.harvard.hul.ois.mets.helper.MetsWriter;
import edu.harvard.hul.ois.mets.helper.PCData;
import edu.harvard.hul.ois.mets.helper.parser.Attribute;
import edu.harvard.hul.ois.mets.helper.parser.Attributes;

public class MetsFileHandler {
	
	public static final String METS_NAMESPACE	= "http://www.loc.gov/METS/";
	public static final String METS_PROFILE		= "DSpace METS SIP Profile 1.0";
	//Additional namespaces
	private static final String[] METS_ADDITIONAL_NAMESPACE_DC		= {"dc",		"http://dublincore.org/documents/dcmi-namespace"};
	private static final String[] METS_ADDITIONAL_NAMESPACE_JUMBO	= {"jumbo",		"https://bitbucket.org/wwmm/jumbo-converters"};
	private static final String[] METS_ADDITIONAL_NAMESPACE_CML		= {"cml",		"http://www.xml-cml.org/schema"};
	private static final String[] METS_ADDITIONAL_NAMESPACE_CMLX 	= {"cmlx",		"http://www.xml-cml.org/schemax"};
	private static final String[] METS_ADDITIONAL_NAMESPACE_XLINK 	= {"xlink",		"http://www.w3.org/1999/xlink"};
	private static final String[] METS_ADDITIONAL_NAMESPACE_XSI 	= {"xsi",		"http://www.w3.org/2001/XMLSchema-instance"};
	private static final String[] METS_ADDITIONAL_NAMESPACE_DC_TERMS= {"dcterms",	"http://purl.org/dc/terms"};
	
	public static final  String[][] METS_ADDITIONAL_NAMESPACES	= {METS_ADDITIONAL_NAMESPACE_DC, METS_ADDITIONAL_NAMESPACE_XSI, METS_ADDITIONAL_NAMESPACE_XLINK,METS_ADDITIONAL_NAMESPACE_JUMBO, METS_ADDITIONAL_NAMESPACE_CML, METS_ADDITIONAL_NAMESPACE_CMLX,  METS_ADDITIONAL_NAMESPACE_DC_TERMS};
	
	//Predefined custom values
	public static final String METSHDR_RECORDSTATUS_STORED 		= "STORED";
	public static final String METSHDR_RECORDSTATUS_PUBLISHED 	= "PUBLISHED";	
	
	public static final String DMDSEC_STATUS 				= "current";
	public static final String DMDSEC_MDWRAP_LABEL_DC		= "Dublin Core Schema Metadata";	
	public static final String DMDSEC_MDWRAP_LABEL_CML		= "CML Related metadata";
	public static final String DMDSEC_MDWRAP_LABEL_DC_TERMS	= "DIM Metadata";
	public static final String DMDSEC_MDWRAP_OTHERTYPE_CML	= "CML";
	
	public static final String AMDSEC_MDWRAP_OTHERMDTYPE_CCTEXT    = "CreativeCommonsText";
	public static final String AMDSEC_MDWRAP_OTHERMDTYPE_CCRDF     = "CreativeCommonsRDF";
	public static final String AMDSEC_MDWRAP_OTHERMDTYPE_DDLICENSE = "DSpaceDepositLicense";
	
	public static final String STRUCTMAP_TYPE				= "physical";
	public static final String STRUCTMAP_LABEL 				= "structure";	
	public static final String STRUCTMAP_DIV_LABEL			= "calculation entity";
	
	public static final String BUNDLE_ORIGINAL		= "ORIGINAL";
	public static final String BUNDLE_THUMBNAILS	= "THUMBNAIL";
	public static final String BUNDLE_GEOMETRY	    = "GEOMETRY";
	public static final String BUNDLE_TEXT    		= "TEXT";
	public static final String BUNDLE_SWORD    		= "SWORD";
	public static final String BUNDLE_LICENSE 		= "LICENSE";
	public static final String BUNDLE_CC_LICENSE  	= "CC-LICENSE";
	
	private static String ccLicenseRdfFilename = "license.xml";
	
	private Mets    mets	  	    = null;
	private AmdSec  amdSecJumbo 	= null;
	private AmdSec  amdSecRights	= null;
	private FileSec fileSec			= null;
	private XmlData jumboXmlData 	= null;				//New file related elements
	private Div     calcDiv 		= null; 
	
	private HashMap<String, FileGrp> fileGrpByBundle = null;
	private HashMap<String, DmdSec>  dmdSecByLabel	= null;
	
	public static final HashMap<String, String> dmdSecLabelByNamespace = new HashMap<String, String>() {
        {
            put(METS_ADDITIONAL_NAMESPACE_DC[0],  DMDSEC_MDWRAP_LABEL_DC);
            put(METS_ADDITIONAL_NAMESPACE_CML[0], DMDSEC_MDWRAP_LABEL_CML);
            put(METS_ADDITIONAL_NAMESPACE_DC_TERMS[0], DMDSEC_MDWRAP_LABEL_DC_TERMS);
        };
    };
	
	private String  calculationID 	= 	null; 
	private boolean hasAssignedID	= 	false;
			
	/**
	 * This constructor works loading content from existing METS compliant string
	 * @param file
	 * @throws IOException
	 */
	public MetsFileHandler(String fileContent) throws IOException{	
 		try {
 			mets = Mets.reader(new MetsReader(new ByteArrayInputStream(fileContent.getBytes("UTF-8"))));
 			associateHelperStructures();
		} catch (UnsupportedEncodingException e) {	
			throw new IOException(e.getCause());
		} catch (MetsException e) {	
			throw new IOException(e.getCause());
		}
	}
	
	public MetsFileHandler(byte[] fileContent) throws IOException{		
		try {
			mets = Mets.reader(new MetsReader(new ByteArrayInputStream(fileContent)));
		} catch (MetsException e) {
			throw new IOException(e.getCause());			
		}
	}
	
	/**
	 * When we build mets object from file or from string, we need to fill helper objects
	 */
	@SuppressWarnings("unchecked")
	private void associateHelperStructures(){
		fileGrpByBundle = new HashMap<String, FileGrp>();
		dmdSecByLabel	= new HashMap<String, DmdSec> ();
		
		calculationID = mets.getID().replace("_metsHdr", "");
		hasAssignedID = !calculationID.equals("cNOT_ASSIGNED");
		Iterator<MetsIDElement> it = mets.getContent().iterator();
		while(it.hasNext()){
			MetsIDElement section = it.next(); 
			if(section.getClass().equals(AmdSec.class)){
				AmdSec amdSec = (AmdSec)section;
				Iterator <MdSec> it2 = amdSec.getContent().iterator();
				while(it2.hasNext()){
					MdSec mdSec = it2.next();
					if(mdSec.getClass().equals(TechMD.class)){
						Iterator<MetsIDElement> it3 = mdSec.getContent().iterator();
						while(it3.hasNext()){
							MetsIDElement metsIDElement = it3.next(); 
							if(metsIDElement.getClass().equals(MdWrap.class)){
								MdWrap mdWrap = (MdWrap)metsIDElement;
								if(mdWrap.getOTHERMDTYPE()!= null && mdWrap.getOTHERMDTYPE().equals("jumbo")){
									amdSecJumbo = amdSec;			// 	jumbo information is in /mets/amdSec/TechMd/mdWrap/xmlData
									jumboXmlData = (XmlData)mdWrap.getContent().get(0);
								}else{
									amdSecRights = amdSec;
								}
							}
						}
					}
				}
			}
			else if(section.getClass().equals(DmdSec.class)){
				DmdSec dmdSec = (DmdSec) section;
				MdWrap mdWrap = (MdWrap) dmdSec.getContent().get(0);
				dmdSecByLabel.put(mdWrap.getLABEL(), dmdSec);
			}else if(section.getClass().equals(FileSec.class)){
				fileSec = (FileSec)section;
				Iterator<FileGrp> it2 = section.getContent().iterator();
				while(it2.hasNext()){
					FileGrp fileGrp = it2.next();
					fileGrpByBundle.put(fileGrp.getUSE(), fileGrp);
				}
			}else if(section.getClass().equals(StructMap.class)){
				calcDiv = (Div) section.getContent().get(0);
			}
		}		
	}
	
	/**
	 * This constructor is used when there's no CREATE calculation 
	 */
	public MetsFileHandler(){	
		this(-1);		
	}
	
	/**
	 * We'll build a skeleton of a default mets file, later it will filled with 
	 * information coming from conversion methods 
	 * @param calcID This is CREATE internal database ID that points to our calculation
	 */	
	@SuppressWarnings("unchecked")
	public MetsFileHandler(int calcID){
		String companyName = System.getProperty("INSTITUTION_NAME") == null ? "www.iochem-bd.org" : System.getProperty("INSTITUTION_NAME");
		fileGrpByBundle = new HashMap<String, FileGrp>();
		dmdSecByLabel	= new HashMap<String, DmdSec> ();
		
		if(calcID == -1)
		{
			calculationID = "cNOT_ASSIGNED"; //Text to replace when we've a valid calculation ID
			hasAssignedID = false;
		}
		else
		{
			calculationID = "c" + String.valueOf(calcID);
			hasAssignedID = true;
		}
		//Build mets root node
		mets	= new Mets();
		mets.setID(calculationID);
		mets.setOBJID(calculationID);
		mets.setPROFILE(METS_PROFILE);
		mets.setLABEL("ioChem-BD quantum chemistry calculation object. Please refer to " + companyName + " for further information");
		mets.setTYPE("dataset");
		for(String[] namespace : METS_ADDITIONAL_NAMESPACES){
			mets.setSchema(namespace[0], namespace[1]);
		}		
		buildHdr(companyName);	
		buildAmd();
		buildFileSec();
		buildStructMap();		
	}

	@SuppressWarnings("unchecked")
	private void buildHdr(String companyName){
		//Build metsHdr element
		MetsHdr metsHdr = new MetsHdr();
		metsHdr.setID(calculationID + "_metsHdr");
		metsHdr.setCREATEDATE(new Date());
		metsHdr.setRECORDSTATUS(METSHDR_RECORDSTATUS_STORED);		
		
		Agent agent = new Agent();
		agent.setROLE(Role.CREATOR);
		agent.setTYPE(Type.ORGANIZATION);
		Name name = new Name();
		name.getContent().add(new PCData(companyName));
		agent.getContent().add(name);		
		metsHdr.getContent().add(agent);
		
		agent = new Agent();
		agent.setROLE(Role.IPOWNER);
		agent.setTYPE(Type.ORGANIZATION);
		name = new Name();
		name.getContent().add(new PCData(companyName));
		agent.getContent().add(name);
		metsHdr.getContent().add(agent);		
		mets.getContent().add(metsHdr);
		
	}
	
	@SuppressWarnings("unchecked")
	private void buildAmd(){
		//Build amdSec element
		int inxSec	= 1;
		int inxTech = 1;
		amdSecJumbo = new AmdSec();
		amdSecJumbo.setID(calculationID + "_amdSec" + inxSec);
		TechMD techMD = new TechMD();
		techMD.setID(amdSecJumbo.getID() + "_techMD" + inxTech);
		techMD.setCREATED(new Date());
		MdWrap mdWrap = new MdWrap();
		mdWrap.setMIMETYPE("text/xml");
		mdWrap.setMDTYPE(Mdtype.OTHER);
		mdWrap.setOTHERMDTYPE("jumbo");		
		XmlData xmlData = new XmlData();	
		mdWrap.getContent().add(xmlData);
		techMD.getContent().add(mdWrap);		
		amdSecJumbo.getContent().add(techMD);	//Disabled section
		mets.getContent().add(amdSecJumbo);
		jumboXmlData = xmlData;
	}
	
	@SuppressWarnings("unchecked")
	private void buildFileSec(){
		//Build  fileSec element
		fileSec = new FileSec();
		fileSec.setID(calculationID + "_fileSec" + "1");
		fileSec.getContent().add(buildFileGrp(BUNDLE_ORIGINAL));
		fileSec.getContent().add(buildFileGrp(BUNDLE_THUMBNAILS));
		fileSec.getContent().add(buildFileGrp(BUNDLE_GEOMETRY));
		mets.getContent().add(fileSec);	
	}
	
	private FileGrp buildFileGrp(String bundle){
		FileGrp fileGrp = new FileGrp();
		fileGrp.setID(fileSec.getID() + "_fileGrp" + fileGrpByBundle.size() + 1);
		fileGrp.setUSE(bundle);
		fileGrpByBundle.put(bundle, fileGrp);
		return fileGrp;
	}
	
	private FileGrp getFileGrpByUse(String bundle){
		return fileGrpByBundle.get(bundle);
	}
	
	private String getDmdSecIds(){
		StringBuilder ids = new StringBuilder();
		for(String id : dmdSecByLabel.keySet())
			ids.append(dmdSecByLabel.get(id).getID()).append(" ");
		return ids.toString().trim();
	}
	
	@SuppressWarnings("unchecked")
	private void buildStructMap(){
		//Build structMap
		int inxStruct 	= 1;
		int inxDiv 		= 1;
		StructMap structMap = new StructMap();
		structMap.setID(calculationID + "_structMap" + inxStruct);
		structMap.setTYPE(STRUCTMAP_TYPE);
		structMap.setLABEL(STRUCTMAP_LABEL);
		Div div = new Div();
		div.setID(structMap.getID() + "_div" + inxDiv);
		div.setDMDID(getDmdSecIds());
		div.setLABEL(STRUCTMAP_DIV_LABEL);
		structMap.getContent().add(div);
		mets.getContent().add(structMap);		
		calcDiv = div;
	}
	
	/**
	 * Multiple parameters needed to generate a new file entry in our mets structure.
	 * They will affect //amdSec/techMD     //fileSec/fileGrp/file   and    //structMap/div
	 * @param program
	 * @param input_format
	 * @param output_format
	 * @param jumbo_class
	 * @param mimetype
	 * @param use
	 * @param xlink_href
	 */
	@SuppressWarnings("unchecked")
	public void addFile(String input_format, String output_format, String jumbo_class ,String mimetype, String md5sum, String use ,String xlink_href, String divLabel, double size){
		FileGrp calcFileGrp = null;
		if(mimetype.equals("chemical/x-cml") && use.equals("geometry"))
				calcFileGrp = getFileGrpByUse(BUNDLE_GEOMETRY);								
		else if(mimetype.equals("image/jpeg") && input_format == null)
			calcFileGrp = getFileGrpByUse(BUNDLE_THUMBNAILS);
		else			
			calcFileGrp = getFileGrpByUse(BUNDLE_ORIGINAL);
		
		int inxFile 	= calcFileGrp.getContent().size() + 1;
		int orderLabel 	= calcDiv.getContent().size() + 1;
		//Add File-FLocat element
		File file = new File();
		file.setID(calcFileGrp.getID() + "_file" + inxFile);
		file.setMIMETYPE(mimetype);
		file.setCHECKSUM(md5sum);
		file.setCHECKSUMTYPE(Checksumtype.MD5);
		file.setUSE(use);
		file.setSIZE((long) size);
		FLocat flocat = new FLocat();
		flocat.setLOCTYPE(Loctype.URL);
		flocat.setXlinkHref(xlink_href);
		file.getContent().add(flocat);
		calcFileGrp.getContent().add(file);
		if(input_format != null && output_format != null){
			//Add structmap div	
			int inxFptr = 1;
			Div div = new Div();
			div.setLABEL(divLabel);
			div.setID(calcDiv.getID() + "_div" + orderLabel) ;
			div.setORDERLABEL(String.valueOf(orderLabel));
			Fptr fptr = new Fptr();
			fptr.setID(div.getID() + "_fptr" + inxFptr);
			fptr.setFILEID(file.getID());
			div.getContent().add(fptr);
			calcDiv.getContent().add(div);
			//Add jumbo metadata
			Attributes jumboAttributes = new Attributes();		
			jumboAttributes.add(new Attribute("FILEID", file.getID(), '"'));
			jumboAttributes.add(new Attribute("jumbo:input", input_format, '"'));
			jumboAttributes.add(new Attribute("jumbo:output",output_format,'"'));
			jumboAttributes.add(new Attribute("jumbo:class", jumbo_class,  '"'));
			Any jumboAny = new Any("jumbo:file",jumboAttributes);
			jumboXmlData.getContent().add(jumboAny);
		}
	}
		
	/**
	 * Check if current objects has a valid calculation ID assigned
	 * @return 
	 */
	public boolean hasAssignedID(){
		return hasAssignedID;
	}
	
	/**
	 * Calling this function will set correct calculation id on ID , OBJID and FILEID attributes 
	 * @param calculationID
	 * @throws IOException 
	 */
	public void setCalcID(long calculationID) throws IOException {
		if(this.hasAssignedID())
			return;
		this.calculationID = "c" + calculationID;
		String calculationStr = this.toString().replace("cNOT_ASSIGNED",  this.calculationID);
		try {
			mets = Mets.reader(new MetsReader(new ByteArrayInputStream(calculationStr.getBytes("utf-8"))));
		} catch (MetsException e) {
			throw new IOException(e.getCause());
		}
		this.hasAssignedID = true;
	}

	/**
	 * This function gets a Creative Commons License and attach it as license administrative metadata 
	 * @param licenseText
	 */
	@SuppressWarnings("unchecked")
	public void attachLicenseFile(String fileName, String md5sum, int size){
		int inxSec = 2;
		amdSecRights = new AmdSec();
		amdSecRights.setID(calculationID + "_amdSec" + inxSec);
		RightsMD rightsMd = new RightsMD();
		rightsMd.setID(amdSecRights.getID() + "_rightsMd1");
		MdRef mdRef = new MdRef();
		mdRef.setLOCTYPE(Loctype.URL);
		mdRef.setXlinkHref(fileName);
		mdRef.setMDTYPE(Mdtype.OTHER);
		mdRef.setOTHERMDTYPE(AMDSEC_MDWRAP_OTHERMDTYPE_CCRDF);
		mdRef.setMIMETYPE("text/xml");
		rightsMd.getContent().add(mdRef);
		calcDiv.setADMID(amdSecRights.getID());
		amdSecRights.getContent().add(rightsMd);
		//Append amdSec after previous existing amdSec's
		Iterator it = mets.getContent().iterator();
		int inx = 0;
		while(it.hasNext()){
			MetsIDElement metsIDElement = (MetsIDElement)it.next();
			if(metsIDElement.getClass().equals(AmdSec.class)){		
				mets.getContent().add(inx,amdSecRights);
				break;
			}
			inx++;
		}		
		//Now build license bundle 
		fileSec.getContent().add(buildFileGrp(BUNDLE_CC_LICENSE));
		FileGrp licenseFileGrp = getFileGrpByUse(BUNDLE_CC_LICENSE);
		//Add File-FLocat element
		File file = new File();
		file.setID(licenseFileGrp.getID() + "_file1");
		file.setMIMETYPE("text/xml");
		file.setCHECKSUM(md5sum);
		file.setCHECKSUMTYPE(Checksumtype.MD5);
		file.setUSE(BUNDLE_CC_LICENSE);
		file.setSIZE((long) size);
		FLocat flocat = new FLocat();
		flocat.setLOCTYPE(Loctype.URL);
		flocat.setXlinkHref(ccLicenseRdfFilename);
		file.getContent().add(flocat);
		licenseFileGrp.getContent().add(file);		
	}

	@SuppressWarnings("unchecked")
	public String getOutputFileName(){
		for(FileGrp fileGrp :(List<FileGrp>)fileSec.getContent())
			if(fileGrp.getUSE().equals(BUNDLE_ORIGINAL))
				for(File file : (List<File>)fileGrp.getContent()){
					if(file.getUSE().equals("output") && file.getMIMETYPE().equals("chemical/x-cml"))						
						return ((FLocat)file.getContent().get(0)).getXlinkHref();	
				}					
		return null;
	}

	@SuppressWarnings("unchecked")
	public boolean containsMoldenFile(){
		for(FileGrp fileGrp :(List<FileGrp>)fileSec.getContent())
			if(fileGrp.getUSE().equals(BUNDLE_ORIGINAL))
				for(File file : (List<File>)fileGrp.getContent()){
					if(file.getUSE().equals("additional") && file.getMIMETYPE().equals("chemical/x-molden"))						
						return true;	
				}					
		return false;
	}

	/**
	 * Check if current mets file is well formed and is valid against METS schema
	 * @return
	 */
 	public boolean isValid(){
		try {
			mets.validate(new MetsValidator());
		} catch (MetsException e) {
			return false;
		}
		return true;
	}

 	/**
 	 * Converts mets file object into it's string representation
 	 */
 	public String toString(){ 		
 	    ByteArrayOutputStream baos = new ByteArrayOutputStream();  	    
 		try {
 			MetsWriter metsWriter = new MetsWriter(baos, "UTF-8");
			mets.write(metsWriter);
			return new String(baos.toByteArray(), "UTF-8");
		} catch (Exception e) {
			return null;
		} 		
 	}

 	/**
 	 * Saves current mets object into file 
 	 * @param filePath File path were file will be saved 
 	 * @throws IOException 
 	 */
 	public void saveToFile(String filePath) throws IOException{
 		try {
			mets.write(new MetsWriter(new FileOutputStream(new java.io.File(filePath))));
		} catch (MetsException e) {
			throw new IOException(e.getCause());
		}
 	}

    public void insertMetadataCsvFields(String fields){
    	Scanner csvFields = new Scanner(fields);
    	while(csvFields.hasNext()){
    		try{
        		String field = new String(csvFields.nextLine());
        		
        		if(field.trim().equals("")) continue;
        		String element = field.split("[\\t]+")[0];
        		String value   = field.split("[\\t]+")[1];
        		appendMetadataField(element,value);    			
    		}catch(Exception e){
    			
    		}
    	}
    	csvFields.close();    	    	
    }
    
	public void appendMetadataField(String qualifiedElement, String value){
		Any cmlAny = null;
		String namespace = qualifiedElement.split("[:]",2)[0];
		String element   = qualifiedElement.split("[:]",2)[1];
		boolean isNested = element.contains(".");
		if(!isNested){
			cmlAny = new Any(qualifiedElement, value);	
		}else{
			if(namespace.equals(METS_ADDITIONAL_NAMESPACE_DC[0]) || namespace.equals(METS_ADDITIONAL_NAMESPACE_DC_TERMS[0])){
				String parentElement = element.split("[.]")[0];
				String childElement  = element.split("[.]")[1];
				Attributes metadataAttributes = new Attributes();		
				metadataAttributes.add(new Attribute("type", childElement, '"'));								
				cmlAny = new Any(namespace + ":" + parentElement, metadataAttributes, value);				
			}else{
				String parentElement = element.split("[.]")[0];
				String childElement  = element.split("[.]")[1];
				cmlAny = new Any(namespace + ":" + parentElement);
				Any cmlAny2 = new Any(namespace + ":" + childElement,value);
				cmlAny.getContent().add(cmlAny2);	
			}
		}				
		
		if(!dmdSecByLabel.containsKey(dmdSecLabelByNamespace.get(namespace))){
			buildDmdSec(namespace);
		}
		DmdSec dmdSec    = dmdSecByLabel.get(dmdSecLabelByNamespace.get(namespace));		
		MdWrap mdWrap = (MdWrap)dmdSec.getContent().get(0);							
		XmlData xmlData = (XmlData)mdWrap.getContent().get(0);
		xmlData.getContent().add(cmlAny);
	}		  
	
    private void buildDmdSec(String namespace){
    	if(namespace.equals(METS_ADDITIONAL_NAMESPACE_DC[0]))
    		buildDmdDc(1);
    	else if(namespace.equals(METS_ADDITIONAL_NAMESPACE_CML[0]))
    		buildDmdCml(1);
    	else if(namespace.equals(METS_ADDITIONAL_NAMESPACE_DC_TERMS[0]))
    		buildDmdDcTerms(1);    	
    }
	
	@SuppressWarnings("unchecked")
	private void buildDmdDc(int inxGroup){
		//Build DC dmdSec element 
		DmdSec dmdSec = new DmdSec();
		dmdSec.setID(calculationID + "_dmdSec" + dmdSecByLabel.size()+1);
		dmdSec.setGROUPID(calculationID + "_dmdSec" + "_group" + inxGroup);		//By now all dmd sections are in the same group
		dmdSec.setSTATUS(DMDSEC_STATUS);
		dmdSec.setCREATED(new Date());
		
		MdWrap mdWrap = new MdWrap();
		mdWrap.setLABEL(DMDSEC_MDWRAP_LABEL_DC);
		mdWrap.setMDTYPE(Mdtype.DC);
		mdWrap.getContent().add(new XmlData());	
		
		dmdSec.getContent().add(mdWrap);
		mets.getContent().add(dmdSec);
		dmdSecByLabel.put(DMDSEC_MDWRAP_LABEL_DC, dmdSec);
		calcDiv.setDMDID(getDmdSecIds());
	}
	
	@SuppressWarnings("unchecked")
	private void buildDmdCml(int inxGroup){
		DmdSec dmdSec = new DmdSec();
		dmdSec.setID(calculationID + "_dmdSec" + dmdSecByLabel.size()+1);
		dmdSec.setGROUPID(dmdSec.getID() + "_group" + inxGroup);
		dmdSec.setSTATUS(DMDSEC_STATUS);
		dmdSec.setCREATED(new Date());
		
		MdWrap mdWrap = new MdWrap();
		mdWrap.setLABEL(DMDSEC_MDWRAP_LABEL_CML);
		mdWrap.setMDTYPE(Mdtype.OTHER);
		mdWrap.setOTHERMDTYPE(DMDSEC_MDWRAP_OTHERTYPE_CML);
		mdWrap.getContent().add(new XmlData());
				
		dmdSec.getContent().add(mdWrap);
		mets.getContent().add(dmdSec);
		dmdSecByLabel.put(DMDSEC_MDWRAP_LABEL_CML, dmdSec);
		calcDiv.setDMDID(getDmdSecIds());
	}

	@SuppressWarnings("unchecked")
	private void buildDmdDcTerms(int inxGroup){
		DmdSec dmdSec = new DmdSec();
		dmdSec.setID(calculationID + "_dmdSec" + dmdSecByLabel.size()+1);
		dmdSec.setGROUPID(dmdSec.getID() + "_group" + inxGroup);
		dmdSec.setSTATUS(DMDSEC_STATUS);
		dmdSec.setCREATED(new Date());
		
		MdWrap mdWrap = new MdWrap();
		mdWrap.setLABEL(DMDSEC_MDWRAP_LABEL_DC_TERMS);
		mdWrap.setMDTYPE(Mdtype.DC);
		mdWrap.getContent().add(new XmlData());							
		dmdSec.getContent().add(mdWrap);
		mets.getContent().add(dmdSec);
		dmdSecByLabel.put(DMDSEC_MDWRAP_LABEL_DC_TERMS, dmdSec);
		calcDiv.setDMDID(getDmdSecIds());
	}
	
	
	
}

/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.exceptions.InvalidParameterException;

public class MCalc extends Command implements ICommand {

    public MCalc(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Relative or absolute calculation path (mandatory)."));
        options.addOption(new Option("nn", true, "New name of the calculation (optional)."));
        options.addOption(new Option("d", true, "Description of the calculation (optional)."));
        options.addOption(new Option("np", true, "New parent project, absolute path (optional)."));
        mandatoryOptions = new String[] { "n" };
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {
        String newPath = getNewPath("Missing calculation path/name", null);
        String newDesc = cmd.getOptionValue("d");
        String newName = cmd.getOptionValue("nn");
        String newParentPath = cmd.getOptionValue("np");
        // Clear symbols and blank spaces
        if (newName != null) {
            newName = normalizeField(newName);
            if (newName.isEmpty())
                throw new InvalidParameterException("Error modifying the calculation: Invalid calculation name.");
        }

        List<NameValuePair> httpParam = new ArrayList<>();
        httpParam.add(new BasicNameValuePair("newPath", newPath));
        if (newDesc != null)
            httpParam.add(new BasicNameValuePair("newDesc", newDesc));
        if (newName != null)
            httpParam.add(new BasicNameValuePair("newName", newName));
        if (newParentPath != null)
            httpParam.add(new BasicNameValuePair("newParentPath", newParentPath));
        sendPostRequest(httpClient, targetHost, url + "userCommands/mcalc", httpParam);
    }

}

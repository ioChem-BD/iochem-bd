package cat.iciq.tcg.labbook.shell.exceptions;

public class NotAllowedException extends Exception {

    private static final long serialVersionUID = 1L;

    public NotAllowedException(String error)
    {
        super(error);
    }
}

/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.utils;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

/**
 * This class manages paths. Returning for example if the path is absolute or not.<br>
 * And other functionalities that can be added.
 *
 */
public class Paths {
	public static final String PATH_DELIMITER = "/";
	/**
	 * If there is a PATH_DELIMITER at the beginning of the path is absolute.
	 * @param path
	 * @return
	 */
	public static boolean isAbsolute(String path){
		return path.startsWith(PATH_DELIMITER);
	}
	
	/**
	 * If there is no PATH_DELIMITER, then it's a single name.
	 * @param path
	 * @return
	 */
	public static boolean isAName(String path){
		return !path.contains(PATH_DELIMITER);
	}
	
	public static String getTail(String path)	{
		int tailStart = 0;
		
		tailStart = path.lastIndexOf(PATH_DELIMITER);
		if (tailStart != -1) return path.substring(tailStart+1);
		else 				 return path.substring(0);
	}

	public static String getParent(String path) {
		return  path.substring(0,path.lastIndexOf(PATH_DELIMITER));
	}
	
	public static String getRelativePath(String path) throws BrowseCredentialsException{
		String basePath = "/db/" + ShellClient.getUserAttributes().get("user_path") + PATH_DELIMITER;
		return path.replace(basePath, "");
	}
	
	public static boolean isDescendant(String parent, String child){
		//Replace parent path and check if result is an absolute path
		String substractedPath = child.replaceAll("^" + parent, "");		
		return substractedPath.length() < child.length() && isAbsolute(substractedPath);	
	}
	
	public static String getFullPath(String path, String name){
		path = path.replace(PATH_DELIMITER + "$","");	//Normalize ends
		name = name.replace("^" + PATH_DELIMITER, ""); 	//Normalize starts
		return path + PATH_DELIMITER + name; 
	}

	/**
	 * Calculate the depth of the path from source to target.
	 * @param source path from where we start counting to the target path.
	 * @param target path we want to calculate the depth to.
	 * @return the depth of the path, or -1 if the target is not a descendant of the source.
	 */
	public static int pathDepth(String source, String target) {        
        source = source.replaceAll("/+$", "");
        target = target.replaceAll("/+$", "");

		if(source.equals(target))
			return 0;
		else if(!isDescendant(source, target) || source.length() > target.length())
			return -1;
		
        source = source.replaceAll("^/+", "");
        target = target.replaceAll("^/+", "");                 
        String[] sourceDirs = source.split("/");
        String[] targetDirs = target.split("/");
        int i;
        for(i = 0; i < sourceDirs.length && i < targetDirs.length; i++) {
            if(!sourceDirs[i].equals(targetDirs[i]))
                break;
        }

        return targetDirs.length - i;
	}
	 
}

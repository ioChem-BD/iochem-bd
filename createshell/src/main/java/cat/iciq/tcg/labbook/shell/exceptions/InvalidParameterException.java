package cat.iciq.tcg.labbook.shell.exceptions;

public class InvalidParameterException extends Exception{

    private static final long serialVersionUID = 1L;

    public InvalidParameterException(){
        super("Provided parameters are not valid.");
    }
    
    public InvalidParameterException(String message){
        super(message);
    }
}

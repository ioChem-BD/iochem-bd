/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.utils.Paths;

public class ViewCalc extends Command implements ICommand {

    private static final int BUFFER_SIZE = 10240;

    public ViewCalc(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Relative or absolute calculation path (mandatory)."));
        options.addOption(new Option("f", false,
                "Download single calculation file, all if not specified. Requires -dcp parameter (optional)."));
        options.addOption(
                new Option("dcp", true, "Folder where to store the files. Requires -f parameter (optional)."));
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {
        List<NameValuePair> httpParam = new ArrayList<>();                
        String newPath = getNewPath("Missing calculation path/name", null);
        String fileOption = null;
        if (cmd.hasOption("f")) {
            if(!cmd.hasOption("dcp"))
                throw new MissingOptionException("Missing mandatory parameter -dcp");
            if(!cmd.hasOption("n")) {
                if(cmd.getArgs().length > 1)
                    fileOption = cmd.getArgs()[1];
                else 
                    fileOption = "dummy";
            }else {
                if(cmd.getArgs().length == 1)
                    fileOption = cmd.getArgs()[0];
                else
                    fileOption = "dummy";
            }
            httpParam.add(new BasicNameValuePair("full", cmd.getOptionValue("f", fileOption)));    
        }
        httpParam.add(new BasicNameValuePair("newPath", newPath));
           
        String jsonString = sendPostRequest(httpClient, targetHost, url + "userCommands/viewcalc", httpParam);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        EntityDTO entityDTO = mapper.readValue(jsonString, EntityDTO.class);
        out.print(entityDTO.print());

        // Download files, if requested
        if (cmd.hasOption("f")) {
            List<String> files = (List<String>) entityDTO.getExtraData();
            for (String file : files) {
                String filename = Paths.getTail(file);
                if ("dummy".equals(fileOption) || filename.equals(fileOption)) {
                    httpParam = new ArrayList<>();
                    httpParam.add(new BasicNameValuePair("newPath", newPath));
                    httpParam.add(new BasicNameValuePair("calcFile", file));
                    downloadCalculationFile(httpClient, targetHost, url + "userCommands/getareafile", httpParam,
                            cmd.getOptionValue("dcp"), Paths.getTail(file));
                }
            }
        }
    }

    protected boolean downloadCalculationFile(CloseableHttpClient httpClient, HttpHost targetHost, String method,
            List<NameValuePair> parameters, String dir, String calcFile) throws IOException {
        HttpPost httpPost = new HttpPost(method);
        httpPost.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));
        HttpResponse response = httpClient.execute(targetHost, httpPost);

        HttpEntity entity = response.getEntity();
        byte[] buffer = null;
        int read = 0;

        try (InputStream instream = entity.getContent();
                BufferedInputStream reader = new BufferedInputStream(instream);
                FileOutputStream file = new FileOutputStream(dir + GeneralConstants.PARENT_SEPARATOR + calcFile);
                BufferedOutputStream writer = new BufferedOutputStream(file);) {

            buffer = new byte[BUFFER_SIZE];
            read = reader.read(buffer, 0, BUFFER_SIZE);
            while (-1 != read) {
                writer.write(buffer, 0, read);
                read = reader.read(buffer, 0, BUFFER_SIZE);
            }
            return false;
        }
    }

}

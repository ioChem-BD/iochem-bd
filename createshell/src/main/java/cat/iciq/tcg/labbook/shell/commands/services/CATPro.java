/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;

public class CATPro extends Command implements ICommand {

    public CATPro(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Relative or absolute project path (mandatory)."));        
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out) throws Exception {
        String newPath = getNewPath("Missing project path.", null);
        List<NameValuePair> httpParam = new ArrayList<>();
        httpParam.add(new BasicNameValuePair("newPath", newPath));

        try {
            String jsonString = sendPostRequest(httpClient, targetHost, url + "userCommands/catpro", httpParam);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            EntityDTO entityDTO = mapper.readValue(jsonString, EntityDTO.class);
            out.print(entityDTO.print());            
        } catch (Exception e) {            
            log.error(e.getMessage());
            throw e;
        }
    }
}

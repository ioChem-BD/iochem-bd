/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.main;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.services.*;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.tests.utils.ByteArrayResponseHandler;
import cat.iciq.tcg.labbook.shell.utils.HttpUtils;

public class ShellClient {

    private static final Logger log = LogManager.getLogger(ShellClient.class.getName());

    // client parameters:
    private static final String SECURE_SERVER_SCHEME = "https";
    private static final int SHELL_SESSION_TIMEOUT_MILLIS = 60 * 60 * 1000; // One hour timeout
    // Session timeout daemon
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> closerHandle = null;
    // CAS Server URI variables
    private String casServerPath = null;
    private String casServerTicketPath = null;
    private String casServerLogoutPath = null;
    private String casRegisteredService = null;
    private String ticketGrantingTicket = null;
    // CREATE Server URI variables
    private String createServerLogin = null;
    private String createServerApp = null;

    private HttpHost targetHost = null;
    private HttpHost casHttpHost = null;

    private PrintStream out;
    private static String path;
    private HashMap<String, ICommand> commands;
    private static Map<String, String> userAttributesMap;

    private ResponseHandler<byte[]> byteResponseHandler;
    private CloseableHttpClient httpClient;

    public ShellClient(Properties prop, String tgtPath, String keystorePath, String keystorePassword) throws Exception {
        try {
            loadProperties(prop);
            initHttpClient(keystorePath, keystorePassword, tgtPath);
            defineAvailableCommands();
            loadUserInformation();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    public String getAppName() {
        if (createServerApp.equals(""))
            return "/";
        else
            return "/" + createServerApp + "/";
    }

    public static String getPath() {
        return path;
    }

    public static void setPath(String newPath) {
        path = newPath;
    }

    public static boolean isBasePath() {
        try {
            return path.equals(GeneralConstants.DB_ROOT + ShellClient.getUserAttributes().get("user_path"));
        } catch (Exception e) {
            return false;
        }
    }

    public void setOut(PrintStream p) {
        this.out = p;
    }

    private String getCommandName(String commandLine) {
        if (commandLine.indexOf(" ") != -1)
            return commandLine.substring(0, commandLine.indexOf(" "));
        else
            return commandLine;
    }

    private String getParameters(String commandLine) {
        if (commandLine.contains(" ")) {
            return commandLine.substring(commandLine.indexOf(" ") + 1);
        } else {
            return "";
        }
    }

    private void readTicketGrantingTicket(String tgtPath) throws IOException {
        try {
            ticketGrantingTicket = FileUtils.readFileToString(new File(tgtPath));
        } catch (IOException e) {
            throw e;
        }
    }

    private void loadProperties(Properties prop) throws NumberFormatException {
        String casServerHostname = prop.getProperty("cas.server.hostname");
        String casServerPort = prop.getProperty("cas.server.port");
        String casServerApp = prop.getProperty("cas.server.app");
        casServerPath = "/" + casServerApp;
        casServerTicketPath = casServerPath + "/v1/tickets";
        casServerLogoutPath = casServerPath + "/logout";

        String createServerHostname = prop.getProperty("create.server.hostname");
        String createServerPort = prop.getProperty("create.server.port");
        createServerPort = createServerPort.equals("") ? "" : createServerPort;
        String createServerPortDelimiter = createServerPort.equals("") ? "" : ":";
        createServerApp = prop.getProperty("create.server.app");

        String createServerPath = "/" + createServerApp;
        createServerLogin = createServerPath + "/callback?client_name=CasClient";
        casRegisteredService = SECURE_SERVER_SCHEME + "://" + createServerHostname + createServerPortDelimiter
                + createServerPort + createServerPath + "/callback?client_name=CasClient";

        casHttpHost = new HttpHost(casServerHostname, casServerPort.equals("") ? -1 : Integer.valueOf(casServerPort),
                SECURE_SERVER_SCHEME);
        targetHost = new HttpHost(createServerHostname,
                createServerPort.equals("") ? -1 : Integer.valueOf(createServerPort), SECURE_SERVER_SCHEME);

        System.setProperty("JUMBO_TEMPLATE_BASE_URL", prop.getProperty("jumbo.template.base.url"));
        System.setProperty("INSTITUTION_NAME", prop.getProperty("mets.institution.name"));
    }

    public void execCommand(String message) {
        String commandName = getCommandName(message);
        String parameters = getParameters(message);
        //SecurityUtils.getSubject().getSession().touch(); // Refresh Shiro session timeout
        resetTimeoutDaemon(); // Refresh Session timeout daemon (handles shell client shutdown)
        if (!(commands.containsKey(commandName))) {
            out.println("Command not found.");
            return;
        }
        try {
            ICommand comm = commands.get(commandName);
            comm.parseCommands(tokenize(parameters));
            if (comm.isHelpRequested()) {
                comm.printHelp(out);
                return;
            }
            if (commandName.equals("loadcalc"))
                out.println("Processing calculation, please wait.");
            comm.execute(httpClient, targetHost, getAppName(), out);
        } catch (Exception e) {
            out.println(e.getMessage());
        }
    }

    public void prompt() {
        out.print("rep-shell>> ");
        out.flush();
    }

    public void close() throws ClientProtocolException, IOException {
        logoutCAS();
        httpClient.getConnectionManager().shutdown();
        if (closerHandle != null)
            closerHandle.cancel(true); // End timeout daemon
        scheduler.shutdown();
    }

    private void defineAvailableCommands() {
        commands = new HashMap<>();
        commands.put("cpro", new CPro(
                "Creates new project in current path. If name or description parameters contains blank spaces, they must be enclosed in double quotes."));
        commands.put("cdpro", new CDPro("Changes path by navigating to parent / child project or an absolute path."));
        commands.put("lspro", new LSPro("Displays content of current path: projects and calculations."));
        commands.put("mpro", new MPro(
                "Modifies the selected project properties, name, description or even moves it to another project (as a nested project)."));
        commands.put("dpro", new DPro("Deletes a project by defining its path, all child projects and calculations will also be removed from Create."));
        commands.put("pwdpro", new PWDPro("Print current path (similar to pwd)"));
        commands.put("catpro", new CATPro("Displays project information."));
        commands.put("findpro", new FindPro("Find project by it’s name, description or path (regex allowed)"));
        commands.put("loadcalc", new LoadCalculation("Internal load calculation tool, not for usage on production, use format-specific load scripts"));
        commands.put("viewcalc", new ViewCalc("This comands displays the most relevant information about a calculation and retrieves its files."));
        commands.put("dcalc", new DCalc("Removes a calculation from a project."));
        commands.put("mcalc", new MCalc("Modifies the selected calculation properties, name, description or even moves it to another project."));
        commands.put("getxyz", new GetXYZ("Retrieve calculation geometry."));
        commands.put("userinfo", new UserInfo("Display user information."));
    }

    protected void initHttpClient(String keystorePath, String keystorePassword, String tgtPath) throws Exception {
        initConnectionVariables(keystorePath, keystorePassword);
        readTicketGrantingTicket(tgtPath);
        loginCAS();        
        resetTimeoutDaemon();
    }

    private void initConnectionVariables(String keystorePath, String keystorePassword)
            throws GeneralSecurityException, IOException {
        httpClient = HttpUtils.buildAllowAllHttpClient(keystorePath, keystorePassword);
        byteResponseHandler = new ByteArrayResponseHandler();
    }

    public InputStream getUrlInputStream(URL url, SSLContext ctx) throws Exception {
        URLConnection conn = url.openConnection();
        if (conn instanceof HttpsURLConnection && ctx != null) {
            ((HttpsURLConnection) conn).setSSLSocketFactory(ctx.getSocketFactory());
        }
        InputStream is = conn.getInputStream();
        int bytesRead, bufsz = Math.max(is.available(), 4096);
        ByteArrayOutputStream os = new ByteArrayOutputStream(bufsz);
        byte[] buffer = new byte[bufsz];
        while ((bytesRead = is.read(buffer)) > 0)
            os.write(buffer, 0, bytesRead);
        byte[] content = os.toByteArray();
        os.close();
        is.close();
        return new ByteArrayInputStream(content);
    }

    private void loginCAS()
            throws ClientProtocolException, IOException, BrowseCredentialsException, KeyManagementException {
        HttpGet get = new HttpGet(createServerLogin + "&ticket=" + generateServiceTicket());
        byte[] response = httpClient.execute(casHttpHost, get, byteResponseHandler);
        log.info(new String(response));
    }

    private void logoutCAS() throws ClientProtocolException, IOException {
        HttpPost post = new HttpPost(casServerLogoutPath);
        byte[] response = httpClient.execute(casHttpHost, post, byteResponseHandler);
        log.info(new String(response));
    }

    private String generateServiceTicket() throws KeyManagementException, UnsupportedEncodingException {
        String serviceTicket = getServiceTicket(casHttpHost, casServerTicketPath, ticketGrantingTicket,
                casRegisteredService);
        if (serviceTicket == null)
            throw new KeyManagementException("Unable to get Service Ticket (ST) from CAS login server.");
        return serviceTicket;
    }

    private String getServiceTicket(HttpHost casHost, String ticketPath, String ticketGrantingTicket, String service)
            throws UnsupportedEncodingException {
        if (ticketGrantingTicket == null)
            return null;
        HttpPost post = new HttpPost(ticketPath + "/" + ticketGrantingTicket);

        List<NameValuePair> parameters = new ArrayList<NameValuePair>();
        parameters.add(new BasicNameValuePair("service", service));
        post.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));

        try {
            byte[] response = httpClient.execute(casHost, post, byteResponseHandler);
            return new String(response);

        } catch (final IOException e) {
            log.warn(e.getMessage());
        } finally {
            post.releaseConnection();
        }

        return null;
    }

    private void loadUserInformation() throws BrowseCredentialsException {
        execCommand("userinfo");
        buildUserPath();
    }

    private void buildUserPath() throws BrowseCredentialsException {
        path = GeneralConstants.DB_ROOT + ShellClient.getUserAttributes().get("user_path");
        log.info("Currently loaded, " + path);
    }

    private void resetTimeoutDaemon() {
        if (closerHandle != null)
            closerHandle.cancel(true); // If it exists we'll cancel it
        ExitByTimeoutDaemon logout = new ExitByTimeoutDaemon(httpClient);
        closerHandle = scheduler.schedule(logout, SHELL_SESSION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
    }

    private class ExitByTimeoutDaemon implements Runnable {
        CloseableHttpClient httpClient = null;

        public ExitByTimeoutDaemon(CloseableHttpClient httpClient) {
            this.httpClient = httpClient;
        }

        @Override
        public void run() {
            try {
                HttpPost post = new HttpPost(casServerLogoutPath);
                byte[] response = httpClient.execute(casHttpHost, post, new ByteArrayResponseHandler());
                log.info("Session expired due to inactivity. " + new String(response));
            } catch (ClientProtocolException e) {
                log.error(e.getMessage());
            } catch (IOException e) {
                log.error(e.getMessage());
            } finally {
                System.exit(0);
            }
        }
    }

    public static String[] tokenize(String input) {
        List<String> tokens = new ArrayList<>();
        StringBuilder currentToken = new StringBuilder();
        boolean insideQuotes = false;

        for (char c : input.toCharArray()) {
            if (c == '"') {
                insideQuotes = !insideQuotes;
                currentToken.append(c);
            } else if (c == ' ' && !insideQuotes) {
                if (currentToken.length() > 0) {
                    tokens.add(currentToken.toString());
                    currentToken = new StringBuilder();
                }
            } else {
                currentToken.append(c);
            }
        }
        if (currentToken.length() > 0) {
            tokens.add(currentToken.toString().replace("\"", ""));
        }
        return tokens.toArray(new String[tokens.size()]);
    }

    public static void setUserAttributes(Map<String, String> attributes){
        userAttributesMap = attributes;
    }

    public static Map<String, String> getUserAttributes(){
        return userAttributesMap;
    }
}

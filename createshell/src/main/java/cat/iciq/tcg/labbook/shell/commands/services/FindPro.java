/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class FindPro extends Command implements ICommand{
	
    public FindPro(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Text to find in the name field of project (optional)."));
        options.addOption(new Option("d", true, "Text to find in the description field of project (optional)."));
        options.addOption(new Option("p", true, "Text to find in the path field of project (optional)."));
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {
        if(cmd.getOptions().length == 0)
            throw new MissingOptionException("Must define a search term");
        
        List<NameValuePair> httpParam = new ArrayList<>();        
        if(cmd.hasOption("n"))
            httpParam.add(new BasicNameValuePair("nameSearch",     cmd.getOptionValue("n"))   );
        if(cmd.hasOption("d"))
            httpParam.add(new BasicNameValuePair("descSearch",      cmd.getOptionValue("d"))   );
        if(cmd.hasOption("p"))
            httpParam.add(new BasicNameValuePair("pathSearch",      cmd.getOptionValue("p"))   );        
        httpParam.add(new BasicNameValuePair("path",            ShellClient.getPath() )   );        
           
        
        try {
            String jsonString = sendPostRequest(httpClient, targetHost, url + "userCommands/findpro", httpParam);            
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);            
            TypeReference<List<String[]>> typeReference = new TypeReference<List<String[]>>() {};
            List<String[]> entityList = mapper.readValue(jsonString, typeReference);
            for(String[] values: entityList)
                out.print(String.format("%-20s%-20s%n", values[0], values[1]));               
            out.println();
        } catch (Exception e) {            
            log.error(e.getMessage());
            throw e;
        }
        
        
        
        
    }  
}

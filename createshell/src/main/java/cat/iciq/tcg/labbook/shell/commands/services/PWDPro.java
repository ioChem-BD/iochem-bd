/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;

import org.apache.http.HttpHost;
import org.apache.http.impl.client.CloseableHttpClient;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class PWDPro extends Command implements ICommand{

    public PWDPro(String header) {
        super(header);   
    }

    @Override
    public void addOptions() {
        // No options defined        
    }
    
    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {
        out.println(ShellClient.getPath());        
    }




}

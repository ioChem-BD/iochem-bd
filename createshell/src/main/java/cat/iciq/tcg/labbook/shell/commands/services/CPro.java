/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class CPro extends Command implements ICommand{

    public CPro(String header) {
        super(header);
    }
    
    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Name of the project (mandatory)."));
        options.addOption(new Option("d", true, "Detailed Description of the project (mandatory)."));
        options.addOption(new Option("cg", true, "Concept Group of the project (optional)"));
        mandatoryOptions = new String[] { "n", "d" };        
    }
    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out) throws Exception {
        String name = normalizeField(cmd.getOptionValue("n"));
        String description = cmd.getOptionValue("d");
        String cg = cmd.getOptionValue("cg", "PRO");

        List<NameValuePair> httpParam = new ArrayList<>();
        httpParam.add(new BasicNameValuePair("name", name));
        httpParam.add(new BasicNameValuePair("description", description));
        httpParam.add(new BasicNameValuePair("cg", cg));
        httpParam.add(new BasicNameValuePair("path", ShellClient.getPath()));
        
        sendPostRequest(httpClient, targetHost, url + "userCommands/cpro", httpParam);
    }
}

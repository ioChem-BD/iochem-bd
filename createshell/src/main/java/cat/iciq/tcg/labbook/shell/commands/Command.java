/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.NotAllowedException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public abstract class Command implements ICommand {

    protected static final Logger log = LogManager.getLogger(Command.class.getName());

    protected String commandName;
    protected String commandHeader;
    protected String[] mandatoryOptions = null;
    protected Options options = new Options();

    protected CommandLineParser parser = new DefaultParser();
    protected CommandLine cmd;

    protected Command(String header) {
        commandName = this.getClass().getSimpleName().toLowerCase();
        commandHeader = header;
        options.addOption("h", false, "Show command help");
        addOptions();
    }

    public void parseCommands(String[] commands) throws Exception {
        cmd = null;
        try {
            parser = new DefaultParser();
            cmd = parser.parse(options, commands);
            if (mandatoryOptions != null)
                for (String mandatoryOption : mandatoryOptions)
                    if (!cmd.hasOption(mandatoryOption) && !cmd.hasOption("h"))
                        throw new MissingOptionException(String.format("Missing option -%s", mandatoryOption));
        } catch (UnrecognizedOptionException | MissingArgumentException e) {
            throw e;
        } catch (Exception e1) {
            throw e1;
        }
    }

    public boolean isHelpRequested() {
        return cmd.hasOption("h");
    }

    @Override
    public void printHelp(PrintStream out) {
        try (StringWriter stringWriter = new StringWriter(); PrintWriter printWriter = new PrintWriter(stringWriter);) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.setOptionComparator(null);
            formatter.printHelp(printWriter, 80, commandName, commandHeader, options, 0, 0, "");
            out.print(stringWriter.toString());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    protected String getNewPath(String missingExceptionMessage, String defaultValue)
            throws IOException, MissingArgumentException {
        String newPath = cmd.getOptionValue("n");
        if (newPath == null) {
            if (cmd.getArgs().length == 0) {
                if (defaultValue == null)
                    throw new MissingArgumentException(missingExceptionMessage);
                else
                    newPath = defaultValue;
            } else
                newPath = cmd.getArgs()[0];
        }

        if (!newPath.startsWith(GeneralConstants.PARENT_SEPARATOR))
            newPath = ShellClient.getPath() + GeneralConstants.PARENT_SEPARATOR + newPath;

        if (newPath.contains(GeneralConstants.PARENT_PROJECT)) {
            File file = new File(newPath);
            newPath = file.getCanonicalPath();
        }
        return newPath;
    }

    protected String sendPostRequest(CloseableHttpClient httpClient, HttpHost targetHost, String url,
            List<NameValuePair> parameters) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new UrlEncodedFormEntity(parameters, StandardCharsets.UTF_8));
        return sendPostRequest(httpClient, targetHost, httpPost);
    }

    protected String sendPostRequest(CloseableHttpClient httpClient, HttpHost targetHost, String url,
            HttpEntity reqEntity) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(reqEntity);
        return sendPostRequest(httpClient, targetHost, httpPost);
    }

    private String sendPostRequest(CloseableHttpClient httpClient, HttpHost targetHost, HttpPost httpPost)
            throws Exception {
        try (CloseableHttpResponse response = httpClient.execute(targetHost, httpPost)) {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200)
                return convertInputStreamToString(response.getEntity().getContent());
            else if (statusCode == 403)
                throw new NotAllowedException(getContent(response.getEntity()));
            else if (statusCode == 500)
                throw new NotAllowedException(getContent(response.getEntity()));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return null;
    }

    private String getContent(HttpEntity entity) {
        if (entity != null && (entity.getContentType().getValue().startsWith("text/plain")
                || entity.getContentType().getValue().startsWith("text/html"))) {
            try {
                return EntityUtils.toString(entity);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return "Operation not allowed.";
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
    }

    public static String normalizeField(String value) {
        String symbolPattern = "[\\.\\[\\~#%&*{}/:<>¿?|\\\'\\\"\\]\\$\\^\\\\]+";
        return value.replaceAll(symbolPattern, "_").replaceAll("[ ]+", "_");
    }
}

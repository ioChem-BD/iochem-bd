package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.main.ShellClient;


public class UserInfo extends Command implements ICommand {

    public UserInfo(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        // No options needed for this command
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out) throws Exception {
         List<NameValuePair> httpParam = new ArrayList<>();
        try {
            String jsonString = sendPostRequest(httpClient, targetHost, url + "userCommands/userinfo", httpParam);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);            
            ShellClient.setUserAttributes(mapper.readValue(jsonString, HashMap.class));
        } catch (Exception e) {            
            log.error("Error obtaining user info", e);            
            throw e;
        }
    }
}

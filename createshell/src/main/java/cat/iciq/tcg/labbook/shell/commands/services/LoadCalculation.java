/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.cli.Option;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.datatype.CalculationTypeFileDTO;
import cat.iciq.tcg.labbook.shell.exceptions.InvalidParameterException;
import cat.iciq.tcg.labbook.shell.exceptions.NotAllowedException;
import cat.iciq.tcg.labbook.shell.extraction.CalExtractionJumbo;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class LoadCalculation extends Command implements ICommand {

    private static final String BAD_PATH_ERROR = "Can't upload calculations on the base path, please create a project or move to a project first.";
    private static final String IO_PARAMETER_ERROR = "If the -i parameter is specified, it's mandatory to specify the -o parameter.";
    
    private String[] notJumboParameters = { "n", "d", "dbp", "dcp", "autopublish" };

    public LoadCalculation(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Name of the calculation in the data base.(mandatory)"));
        options.addOption(new Option("d", true, "Description of the calculation in the data base.(mandatory)"));
        options.addOption(new Option("i", true, "Input file (mandatory)"));
        options.addOption(new Option("o", true,
                "Output file (vasprun.xml on VASP, job.last on Turbomole, log.lammps on LAMMPS. .castep on CASTEP).(mandatory)"));
        options.addOption(new Option("a", true,
                "Additional file.(optional), for multiple additional files, concatenate all names with #"));
        options.addOption(new Option("dc", true, "Vasp DOSCAR file.(optional)"));
        options.addOption(new Option("kp", true, "Vasp KPOINTS file.(optional)"));
        options.addOption(new Option("oe", true, "Turbomole energy file.(optional)"));
        options.addOption(new Option("oc", true, "Coordinate, CASTEP cell file.(optional)"));
        options.addOption(new Option("ob", true, "Turbomole basis file.(optional)"));
        options.addOption(new Option("og", true, "CASTEP .geom file.(optional)"));
        options.addOption(new Option("as", true, "QuantumEspresso absorption spectra data file(optional)"));
        options.addOption(new Option("b", true, "QuantumEspresso band file(optional)"));
        options.addOption(new Option("pi", true, "QuantumEspresso phonon input file(optional)"));
        options.addOption(new Option("po", true, "QuantumEspresso phonon output file(optional)"));
        options.addOption(new Option("dos", true, "QuantumEspresso PDOS files.(optional)"));
        options.addOption(new Option("xcd", true, "CASTEP .xcd graph file(optional)"));
        options.addOption(new Option("t", true, "LAMMPS/GROMACS/Amber trajectory file.(mandatory)"));
        options.addOption(new Option("ir", true, "Amber Input coordinates or initial .ncrst file.(mandatory)"));
        options.addOption(new Option("p", true, "LAMMPS/Amber parameter/topology file.(mandatory)"));
        options.addOption(new Option("r", true, "Amber final restart file .ncrst.(mandatory)"));
        options.addOption(new Option("rkf1", true, "AMS ams.rkf binary file.(mandatory)"));
        options.addOption(new Option("rkf2", true, "AMS adf.rkf binary file.(mandatory)"));
        options.addOption(new Option("cml", true, "AMS .rkf derived cml file.(mandatory)"));
        options.addOption(new Option("dcp", true, "Absolute path in the desktop computer.(optional)"));
        mandatoryOptions = new String[] { "n", "d" };
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out) throws Exception {
        if (ShellClient.isBasePath())
            throw new NotAllowedException(BAD_PATH_ERROR);

        if (cmd.hasOption("i") && !cmd.hasOption("o"))
            throw new InvalidParameterException(IO_PARAMETER_ERROR);

        List<CalculationTypeFileDTO> calDefinitions = getCalculationTypeFiles(httpClient, targetHost, url);

        String calculationType = null;
        String sourceDirectory = cmd.getOptionValue("dcp", System.getenv().get("PWD")); // pwd system provided variable.
        Map<String, String> files = getSingleFileOptions();
        String temporalDirectory = createTemporalFolder();
        CalExtractionJumbo calExt = new CalExtractionJumbo(calDefinitions, files, sourceDirectory, temporalDirectory);
        try {
            calculationType = calExt.call().get("type");            
        } catch (Exception e) {
            FileUtils.deleteQuietly(new File(temporalDirectory));
            throw new Exception(String.format("Error during file conversion: %s", e.getMessage()));
        }

        String name = normalizeField(cmd.getOptionValue("n"));
        String description = cmd.getOptionValue("d");

        try {
            out.println(storeFiles(httpClient, targetHost, url, calExt.getExtractionDirectory(), calculationType, name, description, calExt.getExtractionMethod()));
        }catch(Exception e) {
            log.error(e.getMessage());
            throw e;
        }finally {
            FileUtils.deleteQuietly(new File(temporalDirectory));
        }                       
    }

    private List<CalculationTypeFileDTO> getCalculationTypeFiles(CloseableHttpClient httpClient, HttpHost targetHost, String url) throws Exception {        
        try {
            String jsonString = sendPostRequest(httpClient, targetHost, url + "systemUserCommands/getcaltypes", new ArrayList<>());
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TypeReference<List<CalculationTypeFileDTO>> typeReference = new TypeReference<List<CalculationTypeFileDTO>>() {    };
            return mapper.readValue(jsonString, typeReference);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }    
    }

    protected Map<String, String> getSingleFileOptions() {
        Map<String, String> toReturn = new HashMap<>();        
        for(Option option : cmd.getOptions())
            if (!Arrays.asList(notJumboParameters).contains(option.getOpt()))
                toReturn.put("-" + option.getOpt(), option.getValue());        
        return toReturn;
    }

    private String createTemporalFolder() throws IOException {
        String uuid = UUID.randomUUID().toString();
        File systemTemporalDirectory = new File(System.getProperty("java.io.tmpdir"));
        File temporalExtractionDirectory = new File(
                systemTemporalDirectory.getCanonicalPath() + File.separatorChar + "jumbo_saxon_" + uuid);
        if (!temporalExtractionDirectory.mkdir())
            throw new IOException(
                    "Could not create temporal extraction folder. " + temporalExtractionDirectory.getCanonicalPath());
        return temporalExtractionDirectory.getCanonicalPath();
    }

    private Long storeFiles(CloseableHttpClient httpClient, HttpHost targetHost, String url, File extractionDirectory, String type, String name, String description, String extractionMethod) throws IOException {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create().setCharset(StandardCharsets.UTF_8);      
        try {        
            addParameters(builder, type, name, description, extractionMethod);
            for (File file : extractionDirectory.listFiles())
                builder.addPart(file.getName(), new FileBody(file));            
            String data = sendPostRequest(httpClient, targetHost, url + "userCommands/loadcalc", builder.build());
            return Long.parseLong(data);
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        } finally {
            FileUtils.deleteQuietly(extractionDirectory);
        }
    }

    private void addParameters(MultipartEntityBuilder builder, String type, String name, String description, String extractionMethod) throws UnsupportedEncodingException {
        builder.addPart("sys_user", new StringBody(ShellClient.getUserAttributes().get("user_path"), ContentType.create("text/plain", StandardCharsets.UTF_8)));
        builder.addPart("name", new StringBody(name, ContentType.create("text/plain", StandardCharsets.UTF_8)));
        builder.addPart("description", new StringBody(description, ContentType.create("text/plain", StandardCharsets.UTF_8)));
        builder.addPart("type", new StringBody(type, ContentType.create("text/plain", StandardCharsets.UTF_8)));
        builder.addPart("path", new StringBody(ShellClient.getPath(), ContentType.create("text/plain", StandardCharsets.UTF_8)));
        builder.addPart("method", new StringBody(extractionMethod, ContentType.create("text/plain", StandardCharsets.UTF_8)));
    }

}

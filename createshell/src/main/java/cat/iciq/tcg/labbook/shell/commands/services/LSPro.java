/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO.DisplayFormat;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class LSPro extends Command implements ICommand {

    public LSPro(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Relative or absolute path project (optional)."));
        options.addOption(new Option("f", false, "Display long format in listing (optional)"));
        options.addOption(new Option("o", true,
                "Order by (optional). Possible values:n,o,g,t,c,s\t n->name, o->owner, g->group,t->time,c->concept,s->state\tIf used, -f option is activated automatically"));
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {
        DisplayFormat format = cmd.hasOption("f") ? DisplayFormat.LONG : DisplayFormat.SHORT;
        String newPath = getNewPath(null, ShellClient.getPath());

        List<NameValuePair> httpParam = new ArrayList<>();
        httpParam.add(new BasicNameValuePair("newPath", newPath));

        if (cmd.hasOption("o")) {
            format = DisplayFormat.LONG;
            String orderBy = cmd.getOptionValue("o").toLowerCase();
            if (orderBy.length() != 1)
                throw new Exception(
                        "Error listing project: Only one option is allowed to be specified using -o option. Type lspro -h for help.");
            else if (!orderBy.matches("[nogtcs]"))
                throw new Exception(
                        "Error listing project: The option specified at -o it's not valid. Type lspro -h for help.");
            httpParam.add(new BasicNameValuePair("orderby", orderBy));
        }

        try {
            String jsonString = sendPostRequest(httpClient, targetHost, url + "userCommands/lspro", httpParam);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TypeReference<List<EntityDTO>> typeReference = new TypeReference<List<EntityDTO>>() {};
            List<EntityDTO> entityList = mapper.readValue(jsonString, typeReference);

            out.print(EntityDTO.printTabularHeader(format));
            for (EntityDTO entity : entityList)
                out.print(entity.printTabular(format));
            out.println();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}

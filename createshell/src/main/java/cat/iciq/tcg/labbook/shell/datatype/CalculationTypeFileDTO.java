package cat.iciq.tcg.labbook.shell.datatype;

public class CalculationTypeFileDTO {
    private int id;
    private String calculationTypeName;
    private boolean defaultTypeSelection;
    private String abbreviation;
    private String fileName;
    private String url;
    private String description;
    private String jumboConverterClass;
    private String jumboConverterInType;
    private String jumboConverterOutType;
    private String mimetype;
    private String use;
    private String label;
    private String behaviour;
    private String requires;
    private String renameTo;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getCalculationTypeName() {
        return calculationTypeName;
    }
    public void setCalculationTypeName(String calculationTypeName) {
        this.calculationTypeName = calculationTypeName;
    }
    public boolean isDefaultTypeSelection() {
        return defaultTypeSelection;
    }
    public void setDefaultTypeSelection(boolean defaultTypeSelection) {
        this.defaultTypeSelection = defaultTypeSelection;
    }
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getJumboConverterClass() {
        return jumboConverterClass;
    }
    public void setJumboConverterClass(String jumboConverterClass) {
        this.jumboConverterClass = jumboConverterClass;
    }
    public String getJumboConverterInType() {
        return jumboConverterInType;
    }
    public void setJumboConverterInType(String jumboConverterInType) {
        this.jumboConverterInType = jumboConverterInType;
    }
    public String getJumboConverterOutType() {
        return jumboConverterOutType;
    }
    public void setJumboConverterOutType(String jumboConverterOutType) {
        this.jumboConverterOutType = jumboConverterOutType;
    }
    public String getMimetype() {
        return mimetype;
    }
    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }
    public String getUse() {
        return use;
    }
    public void setUse(String use) {
        this.use = use;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getBehaviour() {
        return behaviour;
    }
    public void setBehaviour(String behaviour) {
        this.behaviour = behaviour;
    }
    public String getRequires() {
        return requires;
    }
    public void setRequires(String requires) {
        this.requires = requires;
    }
    public String getRenameTo() {
        return renameTo;
    }
    public void setRenameTo(String renameTo) {
        this.renameTo = renameTo;
    }

}
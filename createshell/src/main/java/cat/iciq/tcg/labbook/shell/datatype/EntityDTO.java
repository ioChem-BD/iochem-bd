package cat.iciq.tcg.labbook.shell.datatype;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Formatter;

public class EntityDTO {
    
    public enum DisplayFormat {SHORT, LONG};
    
    private Long id;
    private String type;
    private String path;
    private String name;
    private String description;
    private String permissions;
    private Timestamp creationDate;
    private String owner;
    private String group;
    private String conceptGroup;
    private String state;
    private String handle;
    private Boolean published;
    private String publishedName;
    private Timestamp publicationDate;
    private Object extraData;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public String getPermissions() {
        return permissions;
    }
    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Timestamp getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    public String getOwner() {
        return owner;
    }
    public void setOwner(String owner) {
        this.owner = owner;
    }
    public String getGroup() {
        return group;
    }
    public void setGroup(String group) {
        this.group = group;
    } 
    public String getConceptGroup() {
        return conceptGroup;
    }
    public void setConceptGroup(String conceptGroup) {
        this.conceptGroup = conceptGroup;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getHandle() {
        return handle;
    }
    public void setHandle(String handle) {
        this.handle = handle;
    }
    public Boolean getPublished() {
        return published;
    }
    public void setPublished(Boolean published) {
        this.published = published;
    }
    public String getPublishedName() {
        return publishedName;
    }
    public void setPublishedName(String publishedName) {
        this.publishedName = publishedName;
    }
    public Timestamp getPublicationDate() {
        return publicationDate;
    }
    public void setPublicationDate(Timestamp publicationDate) {
        this.publicationDate = publicationDate;
    }
    public Object getExtraData() {
        return extraData;
    }
    public void setExtraData(Object extraData) {
        this.extraData = extraData;
    }
    
    public String print() {
        try(Formatter fmt = new Formatter()) {
            fmt.format("%15s: %2d%n", "Id", getId());
            fmt.format("%15s: %-15s%n", "Path", getPath());
            fmt.format("%15s: %-15s%n", "Name", getName());
            fmt.format("%15s: %-65s%n", "Description", getDescription());
            fmt.format("%15s: %-15s%n", "Permissions", getPermissions());
            fmt.format("%15s: %-15s%n", "Owner", getOwner());
            fmt.format("%15s: %-15s%n", "Group", getGroup());        
            fmt.format("%15s: %-15s%n", "Concept group", getConceptGroup());
            fmt.format("%15s: %-25s%n", "Creation time", new SimpleDateFormat("yyyy/MM/dd HH.mm.ss").format(getCreationDate()));        
            fmt.format("%15s: %-15s%n", "State", getState());
            fmt.format("%15s: %-15s%n", "Published", getPublished());
            fmt.format("%15s: %-15s%n", "Handle", getHandle());
            fmt.format("%15s: %-65s%n", "Published name", getPublishedName());            
            return fmt.toString();
        }       
    }
    
    public static String printTabularHeader(DisplayFormat format) {
        StringBuilder sb = new StringBuilder();
        if(DisplayFormat.LONG.equals(format)) {                       
            try(Formatter fmt = new Formatter()) {
                fmt.format("%3s ", "");
                fmt.format("%-9s ", "Id");
                fmt.format("%-14s ", "Name");
                fmt.format("%-15s ", "Description");
                fmt.format("%-6s ", "Perms.");
                fmt.format("%-6s ", "Owner");
                fmt.format("%-6s ", "Group");        
                fmt.format("%-4s ", "CG");        
                fmt.format("%-8s %n", "State");
                sb.append(fmt.toString());
            }
            try(Formatter fmt = new Formatter()) {
                fmt.format("%3s+", "");
                fmt.format("%9s+", "");
                fmt.format("%14s+", "");
                fmt.format("%15s+", "");
                fmt.format("%6s+", "");
                fmt.format("%6s+", "");
                fmt.format("%6s+", "");        
                fmt.format("%4s+", "");        
                fmt.format("%8s+%n", "");
                sb.append(fmt.toString().replace(" ", "-"));
            }            
        }
        return sb.toString();
    }
 
    public String printTabular(DisplayFormat format) {
        try(Formatter fmt = new Formatter()) {
            if(DisplayFormat.SHORT.equals(format)) {
                if(getType().equals("PRO"))
                    fmt.format("{%s}  ", getName());
                else
                    fmt.format("%s  ", getName());
            }else {                  
                fmt.format("%-3s|", truncateWithEllipsis(getType(), 3));
                fmt.format("%-9d|", getId());
                fmt.format("%-14s|", truncateWithEllipsis(getName(), 14));
                fmt.format("%-15s|", truncateWithEllipsis(getDescription(),15));
                fmt.format("%-6s|", truncateWithEllipsis(getPermissions(), 6));
                fmt.format("%-6s|", truncateWithEllipsis(getOwner(),6));
                fmt.format("%-6s|", truncateWithEllipsis(getGroup(), 6));        
                fmt.format("%-4s|", truncateWithEllipsis(getConceptGroup(), 4));        
                fmt.format("%-8s|%n", truncateWithEllipsis(getState(), 8));
            }
            return fmt.toString();    
        }        
    }
    private static String truncateWithEllipsis(String input, int maxLength) {
        if (input == null || input.length() <= maxLength) {
            return input;
        } else {
            return input.substring(0, maxLength - 1) + "…";
        }
    }
}

/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.InvalidParameterException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;
import cat.iciq.tcg.labbook.shell.utils.Paths;

public class MPro extends Command implements ICommand {

    public MPro(String header) {
        super(header);
    }

    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Relative or absolute project path (mandatory)."));
        options.addOption(new Option("d", true, "Description of the project (optional)."));
        options.addOption(new Option("p", true, "Permissions of the project. Ex: rwr--- (optional)."));
        options.addOption(new Option("g", true, "Group owner of the project (optional)."));
        options.addOption(new Option("cg", true, "Concept Group of the project (optional)."));
        options.addOption(new Option("nn", true, "New Name of the project (optional)."));
        options.addOption(new Option("np", true, "New Parent project, absolute path (optional)."));
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {
        String newPath = getNewPath("Missing project name/path.", null);
        String newGroup = cmd.getOptionValue("g");
        String newDesc = cmd.getOptionValue("d");
        String newCG = cmd.getOptionValue("cg");
        String newPerm = cmd.getOptionValue("p");
        String newName = cmd.getOptionValue("nn");
        String newParentPath = cmd.getOptionValue("np");

        if (newName != null) {
            newName = normalizeField(newName);
            if(newName.isEmpty())
                throw new InvalidParameterException("Error modifying the project: Invalid project name.");
        }

        List<NameValuePair> httpParam = new ArrayList<>();
        httpParam.add(new BasicNameValuePair("newPath", newPath));

        if (newGroup != null)
            httpParam.add(new BasicNameValuePair("newGroup", newGroup));
        if (newDesc != null)
            httpParam.add(new BasicNameValuePair("newDesc", newDesc));
        if (newCG != null)
            httpParam.add(new BasicNameValuePair("newCG", newCG));
        if (newPerm != null)
            httpParam.add(new BasicNameValuePair("newPerm", newPerm));
        if (newName != null) 
            httpParam.add(new BasicNameValuePair("newName", newName));
        if (newParentPath != null)
            httpParam.add(new BasicNameValuePair("newParentPath", newParentPath));
        sendPostRequest(httpClient, targetHost, url + "userCommands/mpro", httpParam);
    }
}

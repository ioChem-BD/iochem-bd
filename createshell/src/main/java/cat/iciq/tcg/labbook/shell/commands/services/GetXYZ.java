/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.commons.cli.Option;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;

public class GetXYZ extends Command implements ICommand {
    
    public GetXYZ(String header) {
        super(header);
    }
    
    @Override
    public void addOptions() {
        options.addOption(new Option("n", true, "Calculation number ID (mandatory)."));
        options.addOption(new Option("o", true, "Output to file instead of stdout (optional)."));
        mandatoryOptions = new String[] { "n" };   
    }

    @Override
    public void execute(CloseableHttpClient httpClient, HttpHost targetHost, String url, PrintStream out)
            throws Exception {                
        
        String calcId = cmd.getOptionValue("n");
        String fileName = cmd.getOptionValue("o");
        
        HttpGet httpGet = new HttpGet(String.format("%sinnerServices/xyz?id=%s", url, calcId));
        String geometry = httpClient.execute(targetHost, httpGet, new BasicResponseHandler());
        
        if(geometry == null)
             throw new IOException(String.format("Error retrieving geometry: Could no retrieve geometry for calculation with ID %s" ,calcId));       
        
        if(fileName != null)
            saveToFile(fileName, geometry);
        else
            out.print(geometry);
    }

	private void saveToFile(String filename, String geometry) throws IOException{
		File file = new File(filename);
		try (FileOutputStream fop = new FileOutputStream(file)) {			
			byte[] contentInBytes = geometry.getBytes();
			fop.write(contentInBytes);
			fop.flush();			
		} catch (IOException e) {
			throw new IOException("Could not store information into file.");
		}			
	}
}

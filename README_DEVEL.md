# ioChem-BD development

To set up a development environment on ioChem-BD platform please refer [this document](https://gitlab.com/ioChem-BD/iochem-bd/-/wikis/Development/Setup-Eclipse-workspace).   
To remote debug an existing ioChem-BD instance for spotting bugs refer [this document](https://gitlab.com/ioChem-BD/iochem-bd/-/wikis/Development/Remote-debug-an-ioChem-BD-server).



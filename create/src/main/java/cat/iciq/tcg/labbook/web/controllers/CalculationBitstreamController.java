package cat.iciq.tcg.labbook.web.controllers;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;

public class CalculationBitstreamController extends HibernateService {
    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
       
        String fileName = params.get(PARAMETER_FILE_ID);
        Calculation calculation = getCalculation(params);
        if (!PermissionService.hasPermissionOnCalculation(calculation, Permissions.READ)) {            
            new ErrorResponse(HttpServletResponse.SC_FORBIDDEN,"Unauthorized",  "You don't have permission to access this calculation.").sendErrorResponse(response);
            return;
        }

        XMLFileManager xmlFileManager;
        try {
            xmlFileManager = new XMLFileManager(MetsFileHandler.METS_NAMESPACE,
                    MetsFileHandler.METS_ADDITIONAL_NAMESPACES, calculation.getMetsXml());
            String mimeType = xmlFileManager.getSingleAttributeValueQuery(XpathQueries.GET_MIMETYPE_FROM_FILENAME.replace("?", fileName));
            File requestedFile = new File(FileService.getCalculationPath(calculation.getId(), fileName));
            sendBitstream(response, requestedFile, mimeType);
        } catch ( Exception e) {
            returnKO(response, userName, fileName);
        }
    }

    protected Calculation getCalculation(Map<String, String> params) {
        String searchType = params.get(PARAMETER_SEARCHTYPE);
        String path = params.get(PARAMETER_PATH);
        String id = params.get(PARAMETER_ID);

        if (searchType == null || searchType.equalsIgnoreCase("id")) {
            return CalculationService.getById(Long.parseLong(id));
        }
        if (searchType.equalsIgnoreCase("path")) {
            return CalculationService.getByPath(path);
        }
        return null;
    }
}
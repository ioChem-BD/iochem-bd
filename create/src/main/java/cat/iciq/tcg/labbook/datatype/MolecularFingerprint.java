package cat.iciq.tcg.labbook.datatype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.Store;

import cat.iciq.tcg.labbook.datatype.helper.fingerprint.AtomTypeArrayBridge;
import cat.iciq.tcg.labbook.datatype.helper.fingerprint.FeatureArrayBridge;

@Indexed
@Entity
@Table(name="molecular_fingerprints", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
public class MolecularFingerprint {
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)        
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @NumericField
    private long id;
    
    @ManyToOne
    @JoinColumn(name = "calculation_id", foreignKey = @ForeignKey(name = "molecular_fingerprints_calc_id_fk"), nullable = false)
//    @Field(index=Index.YES, analyze=Analyze.NO, store=Store.YES)
    @IndexedEmbedded(includeEmbeddedObjectId = true)
    private Calculation calculation;

    @Column(name = "feature", columnDefinition = "int[]", nullable = false)
    @Type(type = "cat.iciq.tcg.labbook.datatype.helper.fingerprint.Feature")
    @Field(name = "feature", analyze = Analyze.NO, store = Store.YES)
    @FieldBridge(impl = FeatureArrayBridge.class)
    @NumericField
    private Integer[] feature;
    
    @Column(name = "connectivity_feature", columnDefinition = "int[]", nullable = false)
    @Type(type = "cat.iciq.tcg.labbook.datatype.helper.fingerprint.Feature")
    @Field(name = "connectivity_feature", analyze = Analyze.NO, store = Store.YES)
    @FieldBridge(impl = FeatureArrayBridge.class)
    @NumericField
    private Integer[] connectivityFeature;
    
    @Column(name = "atom_type", columnDefinition = "text[]", nullable = false)
    @Type(type = "cat.iciq.tcg.labbook.datatype.helper.fingerprint.AtomType")
    @Field(name = "atom_type", analyze = Analyze.NO, store = Store.YES)
    @FieldBridge(impl = AtomTypeArrayBridge.class)
    private String[] atomTypes;

    @Column(name = "canonical_smiles", nullable = false)    
    @Field(name = "canonical_smiles", analyze = Analyze.NO, store = Store.YES)
    private String smiles = "";

    @Column(name = "connectivity_smiles", nullable = false)
    private String connectivitySmiles = "";
    
    @Column(name = "inchi", nullable = false)    
    @Field(name = "inchi", analyze = Analyze.NO, store = Store.YES)
    private String inchi = "";
    
    @Column(name = "inchi_key", nullable = false)    
    @Field(name = "inchi_key", analyze = Analyze.NO, store = Store.YES)
    private String inchiKey = "";
  
    public MolecularFingerprint() {
        this.setFeature(new Integer[0]);
        this.setConnectivityFeature(new Integer[0]);
        this.setAtomTypes(new String[0]);               
    }
    
    public Calculation getCalculation() {
        return calculation;
    }

    public void setCalculation(Calculation calculation) {
        this.calculation = calculation;
    }

    public Integer[] getFeature() {
        return feature;
    }

    public void setFeature(Integer[] feature) {
        this.feature = feature;
    }

    public Integer[] getConnectivityFeature() {
        return connectivityFeature;
    }

    public void setConnectivityFeature(Integer[] connectivityFeature) {
        this.connectivityFeature = connectivityFeature;
    }

    public long getId() {
        return id;
    }

    public String[] getAtomTypes() {
        return atomTypes;
    }

    public void setAtomTypes(String[] atomTypes) {
        this.atomTypes = atomTypes;
    }    
   
    public String getSmiles() {
        return smiles;
    }

    public void setSmiles(String smiles) {
        if (smiles.length() > 32766) {
            this.smiles = "";
        } else {
            this.smiles = smiles;    
        }        
    }

    public String getConnectivitySmiles() {
        return connectivitySmiles;
    }

    public void setConnectivitySmiles(String connectivitySmiles) {
        this.connectivitySmiles = connectivitySmiles;
    }

    public String getInchi() {
        return inchi;
    }

    public void setInchi(String inchi) {
        if (inchi.length() > 32766) {
            this.inchi = "";
        } else {
            this.inchi = inchi;    
        }
    }

    public String getInchiKey() {
        return inchiKey;
    }

    public void setInchiKey(String inchiKey) {
        if (inchiKey.length() > 32766) {
            this.inchiKey = "";
        } else {
            this.inchiKey = inchiKey;    
        }
    }
}

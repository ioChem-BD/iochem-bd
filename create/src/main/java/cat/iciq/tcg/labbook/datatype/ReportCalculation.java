/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import cat.iciq.tcg.labbook.shell.utils.Paths;

@Entity
@Table(name="report_calculations", uniqueConstraints = {@UniqueConstraint(columnNames = "id", name = "id")})
public class ReportCalculation implements Serializable {
        
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "report_calculations_id_seq")
    @SequenceGenerator(name = "report_calculations_id_seq",  sequenceName = "report_calculations_id_seq", allocationSize = 1)
	private long id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "report_id", foreignKey = @ForeignKey(name = "report_calculations_report_id_fkey"), nullable = false)
	private Report report;
    
    @Column(name = "calc_order", nullable = false)
	private int calcOrder;
    
    @Column(name = "title", nullable = false)
	private String title;
    
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "calc_id", foreignKey = @ForeignKey(name = "report_calculations_calc_id_fk"), nullable = false)      
	private Calculation calculation;	
	    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public Report getReport() {
        return report;
    }
	public void setReport(Report report) {
        this.report = report;
    }

	public int getCalcOrder() {
		return calcOrder;
	}
	public void setCalcOrder(int calcOrder) {
		this.calcOrder = calcOrder;
	}	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getCalcPath(){
		return Paths.getFullPath(this.getCalculation().getParentPath(), this.getCalculation().getName());				
	}
	
    public Calculation getCalculation() {
        return calculation;
    }
    public void setCalculation(Calculation calculation) {
        this.calculation = calculation;
    }
	
}

package cat.iciq.tcg.labbook.datatype.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.xml.transform.TransformerException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.hibernate.Session;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.openscience.cdk.BondRef;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.config.AtomTypeFactory;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.fingerprint.ExtendedFingerprinter;
import org.openscience.cdk.fingerprint.IFingerprinter;
import org.openscience.cdk.graph.rebond.RebondTool;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IChemModel;
import org.openscience.cdk.interfaces.ICrystal;
import org.openscience.cdk.io.CMLReader;
import org.openscience.cdk.io.SMILESReader;
import org.openscience.cdk.silent.ChemFile;
import org.openscience.cdk.smarts.SmartsPattern;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.ChemFileManipulator;
import org.openscience.cdk.silent.Crystal;

import cat.iciq.tcg.labbook.datatype.MolecularFingerprint;
import cat.iciq.tcg.labbook.datatype.helper.fingerprint.InchiFormula;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;
import io.github.dan2097.jnainchi.InchiFlag;

public class FingerprintService {

    private static final Logger log = LogManager.getLogger(FingerprintService.class);

    private static final String[] ATOM_TYPES_PER_INDEX = { "X", "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne",
            "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni",
            "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd",
            "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd",
            "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb",
            "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm",
            "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn", "Nh", "Fl", "Mc", "Lv", "Ts",
            "Og" };
    private static ArrayList<String> adsorbateAtomTypes = new ArrayList<>(
            Arrays.asList("C", "O", "N", "H", "S", "P", "F", "Cl", "Br", "I", "K"));

    private static final int EXTENDED_FINGEPRINT_SIZE = 1024;
    private static final int EXTENDED_FINGEPRINT_DEPTH = 7;

    public enum FORMULA_TYPE {
        SMILES, INCHI, INCHIKEY
    };

    private static final String IS_CRYSTAL = "isCrystal";

    public FingerprintService() {
        throw new UnsupportedOperationException();
    }

    public static MolecularFingerprint getByCalculationId(Long calculationId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<MolecularFingerprint> criteriaQuery = builder.createQuery(MolecularFingerprint.class);
            Root<MolecularFingerprint> root = criteriaQuery.from(MolecularFingerprint.class);

            Predicate predicate = builder.equal(root.get("calculation").get("id"), calculationId);
            criteriaQuery.where(predicate);

            return session.createQuery(criteriaQuery).getSingleResult();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;

    }

    public static synchronized MolecularFingerprint getMolecularFingerprint(Long calculationId) throws Exception {
        MolecularFingerprint mf = new MolecularFingerprint();
        mf.setCalculation(CalculationService.getById(calculationId));

        String filePath = FileService.getCalculationPath(calculationId, "geometry.cml");
        if (!new File(filePath).exists())
            return mf;
        try {
            IAtomContainer container = loadAtomContainer(filePath);
            if (container == null)
                return mf;

            InchiFormula inchiFields = getInchi(container);
            mf.setInchi(inchiFields.getInchi());
            mf.setInchiKey(inchiFields.getInchiKey());
            mf.setAtomTypes(getAtomList(container));
            mf.setSmiles(getSmilesFromInchi(mf.getInchi()));
            mf.setConnectivitySmiles(getConnectivitySmilesFromInchi(mf.getInchi()));
            mf.setFeature(FingerprintService.getFeatures(mf.getSmiles()));
            mf.setConnectivityFeature(FingerprintService.getFeatures(mf.getConnectivitySmiles()));
        } catch (CDKException e) {
            throw e;
        } catch (Exception e) {
            log.error(String.format("Load error for id: %d   Cause: %s", calculationId, e.getMessage()));
        }
        return mf;
    }

    public static InchiFormula getInchi(IAtomContainer container) throws CDKException {
        InChIGeneratorFactory factory = null;
        InchiFormula inchiFormula = null;
        try {
            factory = InChIGeneratorFactory.getInstance();
            InChIGenerator gen = factory.getInChIGenerator(container, InchiFlag.RecMet, InchiFlag.DoNotAddH,
                    InchiFlag.OutErrInChI, InchiFlag.SNon);
            if (gen.getInchi() == null || gen.getInchi().isEmpty() || (Boolean) container.getProperty(IS_CRYSTAL)) {
                inchiFormula = alternativeInchiCalculation(container);
            } else {
                inchiFormula = new InchiFormula(gen.getInchi(), gen.getAuxInfo(), gen.getInchiKey());
            }
        } catch (Exception e) {
            log.error("Error raised while parsing container to InChI " + e.getMessage());
        }
        return inchiFormula;
    }

    private static IAtomContainer loadAtomContainer(String filePath) throws TransformerException {
        IAtomContainer container = null;
        try (CMLReader reader = new CMLReader(new FileInputStream(new File(filePath)))) {
            ChemFile chemfile = reader.read(new ChemFile());
            IChemModel model = ChemFileManipulator.getAllChemModels(chemfile).get(0);
            if (ChemFileManipulator.getAllChemModels(chemfile).isEmpty())
                return null;

            if (model.getCrystal() != null) {
                container = model.getCrystal();
                container.setProperty(IS_CRYSTAL, true);
            } else {
                container = ChemFileManipulator.getAllAtomContainers(chemfile).get(0);
                container.setProperty(IS_CRYSTAL, false);
            }
            rebondAndConfigure(container);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return container;
    }

    public static void rebondAndConfigure(IAtomContainer container) throws CDKException {
        if (container.getBondCount() == 0) {
            AtomTypeFactory factory = AtomTypeFactory.getInstance("org/openscience/cdk/config/data/jmol_atomtypes.txt",
                    container.getBuilder());
            for (IAtom atom : container.atoms())
                factory.configure(atom);
            container.setBonds(new IBond[0]);
            RebondTool rebonder = new RebondTool(2.0, 0.5, 0.5);
            rebonder.rebond(container);
        }
        AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(container);
    }

    public static String[] getAtomTypes(String[] userAtomTypes, String smiles) {
        try (SMILESReader reader = new SMILESReader(new ByteArrayInputStream(smiles.getBytes()))) {
            ChemFile chemfile = reader.read(new ChemFile());

            IAtomContainer container = null;
            IChemModel model = ChemFileManipulator.getAllChemModels(chemfile).get(0);
            if (model.getCrystal() != null)
                container = model.getCrystal();
            else
                container = ChemFileManipulator.getAllAtomContainers(chemfile).get(0);

            AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(container);

            CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
            adder.addImplicitHydrogens(container);
            // join atom list and userAtomTypes but removing duplicates
            List<String> list1 = Arrays.asList(userAtomTypes);
            List<String> list2 = Arrays.asList(getAtomList(container));

            return Stream.concat(list1.stream(), list2.stream()).distinct().toArray(String[]::new);
        } catch (Exception e) {
            log.error("Error retrieving features: %s", e.getMessage());
        }
        return userAtomTypes;
    }

    private static String[] getAtomList(IAtomContainer container) {
        Set<String> types = new HashSet<>();
        for (int inx = 0; inx < container.getAtomCount(); inx++)
            types.add(ATOM_TYPES_PER_INDEX[container.getAtom(inx).getAtomicNumber()]);
        if (types.isEmpty()) {
            return new String[0];
        } else {
            String[] arrTypes = new String[types.size()];
            return types.toArray(arrTypes);
        }
    }

    public static Integer[] getFeatures(String smiles) {
        IFingerprinter fingerprinter = new ExtendedFingerprinter(EXTENDED_FINGEPRINT_SIZE, EXTENDED_FINGEPRINT_DEPTH);
        if (smiles == null || smiles.isEmpty())
            return new Integer[0];
        try (SMILESReader reader = new SMILESReader(new ByteArrayInputStream(smiles.getBytes()))) {
            ChemFile chemfile = reader.read(new ChemFile());

            IAtomContainer container = null;
            IChemModel model = ChemFileManipulator.getAllChemModels(chemfile).get(0);
            if (model.getCrystal() != null)
                container = model.getCrystal();
            else
                container = ChemFileManipulator.getAllAtomContainers(chemfile).get(0);

            AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(container);
            return ArrayUtils.toObject(fingerprinter.getBitFingerprint(container).getSetbits());
        } catch (Exception e) {
            log.error("Error retrieving features: %s", e.getMessage());
        }
        return new Integer[0];
    }

    public static String getSmilesFromInchi(String formula) {
        try {
            InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
            IAtomContainer container = factory.getInChIToStructure(formula, DefaultChemObjectBuilder.getInstance())
                    .getAtomContainer();
            return getCanonicalSmiles(container);
        } catch (Exception e) {
            log.error("Error raised while parsing InChI into SMILES " + e.getMessage());
        }
        return "";
    }

    public static String getConnectivitySmilesFromInchi(String formula) {
        try {
            InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
            IAtomContainer container = factory.getInChIToStructure(formula, DefaultChemObjectBuilder.getInstance())
                    .getAtomContainer();
            Iterator<IBond> iter = container.bonds().iterator();
            while (iter.hasNext()) {
                ((BondRef) iter.next()).setOrder(Order.SINGLE);
            }
            return getCanonicalSmiles(container);
        } catch (Exception e) {
            log.error("Error raised while parsing InChI into SMILES " + e.getMessage());
        }
        return "";
    }

    public static String getCanonicalSmiles(String smiles) {
        try (SMILESReader reader = new SMILESReader(new ByteArrayInputStream(smiles.getBytes()))) {
            ChemFile chemfile = reader.read(new ChemFile());
            IAtomContainer container = ChemFileManipulator.getAllAtomContainers(chemfile).get(0);
            SmilesGenerator generator = new SmilesGenerator(SmiFlavor.Unique);
            return generator.create(container);
        } catch (Exception e) {
            return "";
        }
    }

    private static String getCanonicalSmiles(IAtomContainer container) {
        try {
            SmilesGenerator sg = new SmilesGenerator(SmiFlavor.Unique);
            return sg.create(container);
        } catch (Exception e) {
            log.error(String.format("Error raised generating SMILES for with message: %s", e.getMessage()));
            return "";
        }
    }

    public static Map<Long, String> searchCalculationsBySubstructure(String[] addedAtomTypes, String smiles,
            boolean isSimilarSearch) {
        String[] atomTypes = FingerprintService.getAtomTypes(addedAtomTypes, smiles);
        Integer[] features = FingerprintService.getFeatures(smiles);
        try {
            QueryBuilder qb = HibernateUtil.getQueryBuilder(MolecularFingerprint.class);
            BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
            for (Integer feature : features) {
                org.apache.lucene.search.Query featureQuery = qb.keyword()
                        .onField(isSimilarSearch ? "connectivity_feature" : "feature").matching(feature).createQuery();
                booleanQueryBuilder.add(featureQuery, Occur.MUST);
            }
            for (String atomType : atomTypes) {
                org.apache.lucene.search.Query atomTypeQuery = qb.keyword().onField("atom_type").matching(atomType)
                        .createQuery();
                booleanQueryBuilder.add(atomTypeQuery, Occur.MUST);
            }

            BooleanQuery booleanQuery = booleanQueryBuilder.build();
            javax.persistence.Query jpaQuery = HibernateUtil.createFullTextQuery(booleanQuery,
                    MolecularFingerprint.class);
            Map<Long, String> smilesPerCalcId = new HashMap<>();
            for (Object obj : jpaQuery.getResultList()) {
                MolecularFingerprint mf = (MolecularFingerprint) obj;
                String smilesStr = isSimilarSearch ? mf.getConnectivitySmiles() : mf.getSmiles();
                smilesPerCalcId.put(mf.getCalculation().getId(), smilesStr);
            }
            return smilesPerCalcId;
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    public static boolean contains(String moleculeSmiles, SmilesParser parser, SmartsPattern pattern) {        
        IAtomContainer molecule;
        try {
            molecule = parser.parseSmiles(moleculeSmiles);
            return pattern.matches(molecule);
        } catch (InvalidSmilesException e) {
            return false;
        }
    }

    public static Map<Long, String> searchCalculationsByExactFormula(String searchFormula, FORMULA_TYPE type) {
        try {
            String field = "";
            switch (type) {
                case INCHI:
                    field = "inchi";
                    break;
                case INCHIKEY:
                    field = "inchi_key";
                    break;
                case SMILES:
                    field = "canonical_smiles";
                    break;
                default:
                    throw new UnsupportedOperationException();
            }

            QueryBuilder qb = HibernateUtil.getQueryBuilder(MolecularFingerprint.class);
            org.apache.lucene.search.Query featureQuery = qb.keyword().onField(field).matching(searchFormula)
                    .createQuery();
            javax.persistence.Query jpaQuery = HibernateUtil.createFullTextQuery(featureQuery,
                    MolecularFingerprint.class);
            Map<Long, String> formulaPerCalcId = new HashMap<>();
            for (Object obj : jpaQuery.getResultList()) {
                MolecularFingerprint mf = (MolecularFingerprint) obj;
                formulaPerCalcId.put(mf.getCalculation().getId(), searchFormula);
            }
            return formulaPerCalcId;
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    private static InchiFormula alternativeInchiCalculation(IAtomContainer container) throws CDKException {
        ICrystal crystal = (Crystal) container;
        List<IAtom> adsorbateAtoms = new ArrayList<>();
        // Create a complete graph with all the adsorbate atoms of the crystal
        Graph<IAtom, DefaultEdge> completeGraph = new SimpleGraph<>(DefaultEdge.class);
        for (IAtom atom : crystal.atoms()) {
            completeGraph.addVertex(atom);
            if (adsorbateAtomTypes.contains(atom.getSymbol()))
                adsorbateAtoms.add(atom);
        }

        // Add the bonds to the graph and count the bonds to bulk atoms
        for (IAtom atom : crystal.atoms()) {
            int bondsToBulk = 0;
            for (IAtom neighbor : crystal.getConnectedAtomsList(atom)) {
                completeGraph.addEdge(atom, neighbor);
                if (!adsorbateAtoms.contains(neighbor))
                    bondsToBulk++;
            }
            atom.setProperty("bonds", bondsToBulk);
        }

        // Sort the atoms by the number of bonds to bulk atoms
        Map<Integer, List<IAtom>> map = new TreeMap<>();
        for (IAtom atom : crystal.atoms()) {
            if (!map.containsKey(atom.getProperty("bonds")))
                map.put(atom.getProperty("bonds"), new ArrayList<>());
            map.get(atom.getProperty("bonds")).add(atom);
        }

        // Remove from adsorbate atoms those that are heavily connected to bulk atoms:
        // more than two bonds
        Iterator<IAtom> it = adsorbateAtoms.iterator();
        while (it.hasNext()) {
            IAtom atom = it.next();
            if (atom.getProperty("bonds") != null && (int) atom.getProperty("bonds") > 4) {
                int metalNeighbors = 0;
                for (IAtom neighbor : crystal.getConnectedAtomsList(atom))
                    if (!adsorbateAtoms.contains(neighbor))
                        metalNeighbors++;
                if (metalNeighbors > 2)
                    it.remove();
            }
        }

        // Iterate all adsorbate atoms and remove the bonds to those that are connected
        // to the bulk
        for (IAtom atom : adsorbateAtoms) {
            for (IAtom neighbor : crystal.getConnectedAtomsList(atom))
                if (!adsorbateAtoms.contains(neighbor))
                    crystal.removeBond(atom, neighbor);
        }

        // Remove from the graph all the bulk atoms
        for (IAtom atom : crystal.atoms())
            if (!adsorbateAtoms.contains(atom))
                completeGraph.removeVertex(atom);

        // Get the connected sets of the graph
        ConnectivityInspector<IAtom, DefaultEdge> inspector = new ConnectivityInspector<>(completeGraph);
        List<Set<IAtom>> connectedSets = inspector.connectedSets();
        // Create subgraphs
        List<Graph<IAtom, DefaultEdge>> subGraphs = new ArrayList<>();
        for (Set<IAtom> connectedSet : connectedSets) {
            Graph<IAtom, DefaultEdge> subGraph = new SimpleGraph<>(DefaultEdge.class);
            connectedSet.forEach(subGraph::addVertex);
            for (IAtom vertex : connectedSet) {
                Set<DefaultEdge> edges = completeGraph.edgesOf(vertex);
                for (DefaultEdge edge : edges) {
                    IAtom source = completeGraph.getEdgeSource(edge);
                    IAtom target = completeGraph.getEdgeTarget(edge);
                    if (connectedSet.contains(source) && connectedSet.contains(target)) {
                        subGraph.addEdge(source, target);
                    }
                }
            }
            subGraphs.add(subGraph);
        }

        // Create an IAtomContainer container from the subgraphs, and get the SMILES
        HashSet<String> smiles = new HashSet<>();
        for (Graph<IAtom, DefaultEdge> subGraph : subGraphs) {
            IAtomContainer subContainer = new Crystal();
            for (IAtom atom : subGraph.vertexSet())
                subContainer.addAtom(atom);
            for (DefaultEdge edge : subGraph.edgeSet()) {
                IAtom source = subGraph.getEdgeSource(edge);
                IAtom target = subGraph.getEdgeTarget(edge);
                subContainer.addBond(getAtomIndex(subGraph.vertexSet(), source),
                        getAtomIndex(subGraph.vertexSet(), target), IBond.Order.SINGLE);
            }
            CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(subContainer.getBuilder());
            adder.addImplicitHydrogens(subContainer);
            AtomContainerManipulator.convertImplicitToExplicitHydrogens(subContainer);
            SmilesGenerator smilesGenerator = new SmilesGenerator(SmiFlavor.Unique);
            String subSmiles = smilesGenerator.create(subContainer);
            if (!subSmiles.matches("\\[H\\]\\[H\\]")) // Exclude hydrogen molecules
                smiles.add(subSmiles);
        }

        // Get the unique SMILES from each subgraph
        String finalSmiles = String.join(".", smiles);

        // Create an IAtomContainer container from the final SMILES and append bulk
        // single atoms
        // Also generate the InChI and InChIKey from the adsorbate part
        try (SMILESReader reader = new SMILESReader(new ByteArrayInputStream(finalSmiles.getBytes()));) {
            ChemFile chemfile = reader.read(new ChemFile());
            IAtomContainer subContainer = null;
            IChemModel model = ChemFileManipulator.getAllChemModels(chemfile).get(0);
            if (model.getCrystal() != null)
                subContainer = model.getCrystal();
            else
                subContainer = ChemFileManipulator.getAllAtomContainers(chemfile).get(0);

            InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
            InChIGenerator gen = factory.getInChIGenerator(subContainer, InchiFlag.RecMet, InchiFlag.DoNotAddH,
                    InchiFlag.OutErrInChI, InchiFlag.SNon);
            InchiFormula inchiFormula = new InchiFormula(gen.getInchi(), gen.getAuxInfo(), gen.getInchiKey());

            // Get atom symbol of the bulk atoms
            HashMap<String, IAtom> bulkAtoms = new HashMap<>();
            for (IAtom atom : crystal.atoms())
                if (!adsorbateAtoms.contains(atom))
                    bulkAtoms.put(atom.getSymbol(), atom);

            for (IAtom atom : bulkAtoms.values())
                subContainer.addAtom(atom);
            return inchiFormula;
        } catch (Exception e) {
            return new InchiFormula("", "", "");
        }
    }

    private static int getAtomIndex(Set<IAtom> vertexSet, IAtom source) {
        int index = 0;
        for (IAtom atom : vertexSet) {
            if (atom.equals(source))
                return index;
            index++;
        }
        return -1;
    }

    public static Map<Long, String> searchCalculationsByAtomType(String[] atomTypes) {
        try {
            QueryBuilder qb = HibernateUtil.getQueryBuilder(MolecularFingerprint.class);
            BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
            for (String atomType : atomTypes) {
                org.apache.lucene.search.Query atomTypeQuery = qb.keyword().onField("atom_type").matching(atomType)
                        .createQuery();
                booleanQueryBuilder.add(atomTypeQuery, Occur.MUST);
            }

            BooleanQuery booleanQuery = booleanQueryBuilder.build();
            javax.persistence.Query jpaQuery = HibernateUtil.createFullTextQuery(booleanQuery,
                    MolecularFingerprint.class);
            Map<Long, String> smilesPerCalcId = new HashMap<>();
            for (Object obj : jpaQuery.getResultList()) {
                MolecularFingerprint mf = (MolecularFingerprint) obj;
                smilesPerCalcId.put(mf.getCalculation().getId(), "");
            }
            return smilesPerCalcId;
        } catch (Exception e) {
            return new HashMap<>();
        }
    }
}
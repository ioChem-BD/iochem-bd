package cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile;

import xyz.avarel.aljava.TexElement;

public class Fraction implements TexElement {
    private final long numerator;
    private final long denominator;

    public Fraction(long n) {
        this(n, 1);
    }

    public Fraction(long numerator, long denominator) {
        if (denominator < 0) {
            denominator = -denominator;
            numerator = -numerator;
        }

        this.numerator = numerator;
        this.denominator = denominator;

        if (denominator == 0) {
            throw new ArithmeticException("Division by zero");
        }
    }

    public long getNumerator() {
        return numerator;
    }

    public long getDenominator() {
        return denominator;
    }

    public Fraction reduce() {
        long gcd = gcd(numerator, denominator);
        return new Fraction(numerator / gcd, denominator / gcd);
    }

    public Fraction reciprocal() {
        return new Fraction(denominator, numerator);
    }

    public Fraction abs() {
        return new Fraction(Math.abs(numerator), Math.abs(denominator));
    }



    public Fraction plus(long other) {
        return plus(other, true);
    }

    public Fraction plus(long other, boolean reduce) {
        return plus(new Fraction(other), reduce);
    }

    public Fraction plus(Fraction other) {
        return plus(other, true);
    }

    public Fraction plus(Fraction other, boolean reduce) {
        if (this.denominator == other.denominator) {
            Fraction result = new Fraction(this.numerator + other.numerator, denominator);
            return reduce ? result.reduce() : result;
        }

        long lcm = lcm(this.denominator, other.denominator);
        long a = lcm / this.denominator;
        long b = lcm / other.denominator;

        Fraction result = new Fraction(this.numerator * a + other.numerator * b, lcm);
        return reduce ? result.reduce() : result;
    }



    public Fraction minus(long other) {
        return minus(other, true);
    }

    public Fraction minus(long other, boolean reduce) {
        return minus(new Fraction(other), reduce);
    }

    public Fraction minus(Fraction other) {
        return minus(other, true);
    }

    public Fraction minus(Fraction other, boolean reduce) {
        return plus(new Fraction(-other.numerator, other.denominator), reduce);
    }



    public Fraction times(long other) {
        return times(other, true);
    }

    public Fraction times(long other, boolean reduce) {
        return times(new Fraction(other), reduce);
    }

    public Fraction times(Fraction other) {
        return times(other, true);
    }

    public Fraction times(Fraction other, boolean reduce) {
        Fraction result = new Fraction(this.numerator * other.numerator, this.denominator * other.denominator);
        return reduce ? result.reduce() : result;
    }



    public Fraction div(long other) {
        return div(other, true);
    }

    public Fraction div(long other, boolean reduce) {
        return div(new Fraction(other), reduce);
    }

    public Fraction div(Fraction other) {
        return div(other, true);
    }

    public Fraction div(Fraction other, boolean reduce) {
        return times(other.reciprocal(), reduce);
    }



    public Fraction pow(long n) {
        return pow(n, true);
    }

    public Fraction pow(long n, boolean reduce) {
        if (n >= 0) {
            Fraction result = new Fraction((long) Math.pow(numerator, n), (long) Math.pow(denominator, n));
            return reduce ? result.reduce() : result;
        } else {
            return pow(Math.abs(n)).reciprocal();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Fraction) {
            Fraction other = (Fraction) obj;
            return numerator == other.numerator
                    && denominator == other.denominator;
        }
        return obj.equals(numerator / denominator);
    }

    @Override
    public String toString() {
        if (numerator == 0) {
            return "0";
        } else if (denominator == 1) {
            return String.valueOf(numerator);
        } else if (denominator == -1) {
            return String.valueOf(-numerator);
        }
        return numerator + "/" + denominator;
    }

    @Override
    public String toTex() {
        if (numerator == 0) {
            return "0";
        } else if (denominator == 1) {
            return String.valueOf(numerator);
        } else if (denominator == -1) {
            return String.valueOf(-numerator);
        }

        if (numerator < 0 && denominator < 0) {
            return "\\frac{" + -numerator + "}{" + -denominator + "}";
        } else if (numerator < 0 || denominator < 0) {
            return "-\\frac{" + numerator + "}{" + denominator + "}";
        }

        return "\\frac{" + numerator + "}{" + denominator + "}";
    }

    public double toDouble() {
        return (double) numerator / denominator;
    }


    protected Fraction sqrt() {
        if (!sqrtIsRational()) {
            throw new IllegalStateException("Internal error");
        }

        return new Fraction((long) Math.sqrt(numerator), (long) Math.sqrt(denominator));
    }

    public boolean sqrtIsRational() {
        double num = Math.sqrt(numerator);
        double den = Math.sqrt(denominator);

        return num % 1 == 0 && den % 1 == 0;
    }

    private static long gcd(long a, long b)  {
        while (a != 0 && b != 0) {
            long c = b;
            b = a % b;
            a = c;
        }
        return a + b;
    }

    private static long lcm(long a, long b) {
        return a * b / gcd(a, b);
    }
}

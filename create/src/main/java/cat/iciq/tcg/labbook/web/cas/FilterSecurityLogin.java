package cat.iciq.tcg.labbook.web.cas;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Base64;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;

import cat.iciq.tcg.labbook.web.controllers.ErrorResponse;
import io.github.bucket4j.Bucket;

public class FilterSecurityLogin implements Filter {

  private static Cache<String, Bucket> cache;
  private static CacheManager cacheManager;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {    cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
        .withCache("rateLimiterCache",
            CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Bucket.class,
                org.ehcache.config.builders.ResourcePoolsBuilder.heap(1000))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofMinutes(3))))
        .build();
    cacheManager.init();
    cache = cacheManager.getCache("rateLimiterCache", String.class, Bucket.class);
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) req;
    HttpServletResponse httpResponse = (HttpServletResponse) resp;

    String userEmail = null;
    String authHeader = httpRequest.getHeader("Authorization");
    if (authHeader != null && authHeader.startsWith("Basic ")) {
        String base64Credentials = authHeader.substring("Basic ".length());
        String credentials = new String(Base64.getDecoder().decode(base64Credentials), StandardCharsets.UTF_8);
        String[] values = credentials.split(":", 2);
        if (values.length == 2) {
            userEmail = values[0];
        }
    }
    if (userEmail == null || userEmail.isEmpty()) {
      new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Unauthorized", "Email is required").sendErrorResponse(httpResponse);
    }

    Bucket bucket = cache.get(userEmail);
    if (bucket == null) {
      bucket = createNewBucket();
      cache.put(userEmail, bucket);
    }

    if (!bucket.tryConsume(1)) {
      new ErrorResponse( 429, "Unauthorized", "Too many requests, please wait 2 minute to re-login").sendErrorResponse(httpResponse);
    }
    chain.doFilter(req, resp);
  }

  private Bucket createNewBucket() {
    return Bucket.builder().addLimit(limit -> limit.capacity(10).refillIntervally(10, Duration.ofMinutes(2))).build();
  }

  @Override
  public void destroy() {
    // Clean up Ehcache resources by using the CacheManager to close the cache
    if (cacheManager != null) {
      cacheManager.close();
    }
  }

}

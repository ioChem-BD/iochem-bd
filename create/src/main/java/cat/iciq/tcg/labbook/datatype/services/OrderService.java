/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.util.HashMap;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class OrderService {
	
	private static final int PROJECT_ORDER_START_INDEX = 1;
	private static final int CALCULATION_ORDER_START_INDEX = 10000;
	
	private static final Logger log = LogManager.getLogger(OrderService.class);
	
	public static void moveProject(long projectId, String oldParentPath, String newParentPath) {	    
	    try {
			Project project = ProjectService.getById(projectId);
			int order = project.getElementOrder();						
			reduceProjectSiblingsOrder(order, oldParentPath);			
			int newOrder = getLastProjectOrderNumberExcludingId(newParentPath, projectId) + 1;
			setProjectOrder(project.getId(), newOrder);			
			sendMoveProjectNavigationEvent(projectId, order, newOrder, oldParentPath, newParentPath);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public static void reduceProjectSiblingsOrder(int projectOrder, String parentProjectPath) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            // update projects set element_order = element_order-1 where element_order > ? and path = ?
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaUpdate<Project> updateCriteria = builder.createCriteriaUpdate(Project.class);
            Root<Project> root = updateCriteria.from(Project.class);
            
            Path<Integer> order = root.get("elementOrder");
            Predicate elementOrder = builder.greaterThan(order, projectOrder);
            Predicate path = builder.equal(root.get("path"), parentProjectPath);
            
            updateCriteria
                .set(order, builder.diff(order, 1))
                .where(builder.and(elementOrder, path));

            session.createQuery(updateCriteria).executeUpdate();
        }catch(Exception e) {
            log.error(e.getMessage());
        }
	}

	private static void sendMoveProjectNavigationEvent(long projectId, int oldOrder, int newOrder, String oldParentPath, String newParentPath) {
		HashMap<String, Object> params = new HashMap<>();		
		params.put("id", projectId);
		params.put("oldOrder", oldOrder);
		params.put("newOrder", newOrder);
		params.put("oldParentPath", oldParentPath);
		params.put("newParentPath", newParentPath);
		TreeEvent.sendEventToUserQueue(new Event("projectReorder", null, params));
	}

	private static void setProjectOrder(long projectId, int order) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
		    CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaUpdate<Project> updateCriteria = builder.createCriteriaUpdate(Project.class);
            Root<Project> root = updateCriteria.from(Project.class);

            updateCriteria
                .set(root.get("elementOrder"), order)
                .where(builder.equal(root.get("id"), projectId));

            Query<Project> query = session.createQuery(updateCriteria);
            query.executeUpdate();
		}catch(Exception e) {
			log.error(e.getMessage());
		}
	}

	public static int getNewProjectOrderIndex(String parentPath) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();        
        try {
            // select coalesce(max(element_order), " + (PROJECT_ORDER_START_INDEX - 1) + ") from projects where path = ?
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);

            Root<Project> root = query.from(Project.class);
            Expression<Integer> maxElementOrder = criteriaBuilder.max(root.get("elementOrder"));
                        
            Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), parentPath);
            query.select(criteriaBuilder.coalesce(maxElementOrder, PROJECT_ORDER_START_INDEX - 1))
                 .where(pathPredicate);

            return session.createQuery(query).uniqueResult() + 1;    
        }catch(Exception e) {
            log.error(e.getMessage());
            return PROJECT_ORDER_START_INDEX;
        }
	}

	public static int getNewCalculationOrderIndex(String parentPath) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();	    
		try {
		    // select coalesce(max(element_order), " + (CALCULATION_ORDER_START_INDEX - 1) + ") from calculations where path = ?
		    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		    CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);
		    Root<Calculation> root = query.from(Calculation.class);

		    Expression<Integer> maxElementOrder = criteriaBuilder.max(root.get("elementOrder"));
		    Expression<Integer> defaultValue = criteriaBuilder.literal(CALCULATION_ORDER_START_INDEX - 1);

		    Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), parentPath);

		    query.select(criteriaBuilder.coalesce(maxElementOrder, defaultValue))
		         .where(pathPredicate);

		    return session.createQuery(query).getSingleResult() + 1;
		}catch (Exception e) {
			log.error(e.getMessage());
			return CALCULATION_ORDER_START_INDEX;
		}
	}
	
	private static int getLastProjectOrderNumberExcludingId(String projectPath, long id) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            // select coalesce(max(element_order), " + (PROJECT_ORDER_START_INDEX - 1) + ") from projects where path = ? and id != ?
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);

            Root<Project> root = query.from(Project.class);
            Expression<Integer> maxElementOrder = criteriaBuilder.max(root.get("elementOrder"));

            Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), projectPath);
            Predicate idNotEqualPredicate = criteriaBuilder.notEqual(root.get("id"), id);

            query.select(criteriaBuilder.coalesce(maxElementOrder, PROJECT_ORDER_START_INDEX - 1))
                 .where(criteriaBuilder.and(pathPredicate, idNotEqualPredicate));
            
            return session.createQuery(query).uniqueResult();    
        }catch(Exception e) {
            log.error(e.getMessage());
            return PROJECT_ORDER_START_INDEX;
        }
	}
	
	public static void moveCalculation(Calculation calculation, Calculation targetCalculation, String oldParentPath, String newParentPath) {
		int oldOrder = calculation.getElementOrder();
		int newOrder = 0;
		try {
			if(targetCalculation == null) {		// Dropped from multiple selection, no targetCalculationId => move to last position
				reduceCalculationSiblingsOrder( calculation.getElementOrder(), oldParentPath);
				newOrder = getLastCalculationOrderNumberExcludingId(calculation.getId(), newParentPath) + 1;
				setCalculationOrder(calculation.getId(), newOrder);				
			}else {
				if(oldParentPath.equals(newParentPath)) {	// Dropped on same project, insert before targetCalculation
					moveCalculationWithinProject(oldParentPath, calculation.getElementOrder(), targetCalculation.getElementOrder());									
				}else {										// Dropped on different project, insert before targetCalculation
					reduceCalculationSiblingsOrder(calculation.getElementOrder(), oldParentPath);
					moveCalculationIntoProject(newParentPath, targetCalculation.getElementOrder());
				}
				newOrder = targetCalculation.getElementOrder();
				setCalculationOrder(calculation.getId(), newOrder);
			}
			sendMoveCalculationNavigationEvent(calculation.getId(), targetCalculation == null? -1: targetCalculation.getId(), newOrder, oldOrder, oldParentPath, newParentPath);
		}catch (Exception e) {
			log.error(e.getMessage());
		}			
	}
	
	private static void sendMoveCalculationNavigationEvent(long calculationId, long targetCalculationId,  int newOrder, int oldOrder, String oldParentPath, String newParentPath) {
		HashMap<String, Object> params = new HashMap<>();		
		params.put("id", calculationId);
		params.put("targetId", targetCalculationId);
		params.put("oldOrder", oldOrder);
		params.put("newOrder", newOrder);
		params.put("oldParentPath", oldParentPath);
		params.put("newParentPath", newParentPath);
		TreeEvent.sendEventToUserQueue(new Event("calculationReorder", null, params));		
	}

	protected static void reduceCalculationSiblingsOrder(int order, String path) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
		    // update calculations set element_order = element_order-1 where element_order > ? and path = ?
		    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		    CriteriaUpdate<Calculation> update = criteriaBuilder.createCriteriaUpdate(Calculation.class);
		    Root<Calculation> root = update.from(Calculation.class);

		    Expression<Integer> newOrder = criteriaBuilder.diff(root.get("elementOrder"), 1);

		    Predicate elementOrderPredicate = criteriaBuilder.greaterThan(root.get("elementOrder"), order);
		    Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), path);

		    update.set("elementOrder", newOrder)
		          .where(criteriaBuilder.and(elementOrderPredicate, pathPredicate));

		    session.createQuery(update).executeUpdate();
		}catch(Exception e) {
			log.error(e.getMessage());
		}
	}

	private static int getLastCalculationOrderNumberExcludingId(long calculationId, String projectPath) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();	   
		try {
		    // select coalesce(max(element_order)," + (CALCULATION_ORDER_START_INDEX - 1) + ") from calculations where path = ? and id != ?		    
		    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		    CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);
		    Root<Calculation> root = query.from(Calculation.class);

		    Expression<Integer> maxElementOrder = criteriaBuilder.max(root.get("elementOrder"));
		    Expression<Integer> defaultValue = criteriaBuilder.literal(CALCULATION_ORDER_START_INDEX - 1);

		    Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), projectPath);
		    Predicate idNotEqualPredicate = criteriaBuilder.notEqual(root.get("id"), calculationId);

		    query.select(criteriaBuilder.coalesce(maxElementOrder, defaultValue))
		         .where(criteriaBuilder.and(pathPredicate, idNotEqualPredicate));
		    return session.createQuery(query).getSingleResult();
		}catch(Exception e) {
			log.error(e.getMessage());
			return CALCULATION_ORDER_START_INDEX;          //Calculation order indexes start from 10000
		}		
	}
	
	private static void setCalculationOrder(long calculationId, int order) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    try {
	        // update calculations set element_order = ? where id = ?
	        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
	        CriteriaUpdate<Calculation> update = criteriaBuilder.createCriteriaUpdate(Calculation.class);
	        Root<Calculation> root = update.from(Calculation.class);
	        update.set(root.get("elementOrder"), order)
	              .where(criteriaBuilder.equal(root.get("id"), calculationId));

	        session.createQuery(update).executeUpdate();		
		}catch(Exception e) {
			log.error(e.getMessage());
		}
	}
	
	private static void moveCalculationWithinProject(String parentPath, int calculationOrder, int targetCalculationOrder) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		if(calculationOrder > targetCalculationOrder) {
			try {
			    // update calculations set element_order = element_order + 1 where element_order >= ? and element_order <= ? and path = ?
			    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			    CriteriaUpdate<Calculation> update = criteriaBuilder.createCriteriaUpdate(Calculation.class);
			    Root<Calculation> root = update.from(Calculation.class);

			    Expression<Integer> newOrder = criteriaBuilder.sum(root.get("elementOrder"), 1);
			    Predicate elementOrderPredicate = criteriaBuilder.between(root.get("elementOrder"), targetCalculationOrder, calculationOrder);
			    Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), parentPath);

			    update.set("elementOrder", newOrder)
			          .where(criteriaBuilder.and(elementOrderPredicate, pathPredicate));

			    session.createQuery(update).executeUpdate();			   
			}catch(Exception e) {
				log.error(e.getMessage());
			}		
		}else if(calculationOrder < targetCalculationOrder) {
			try {
			    // update calculations set element_order = element_order - 1 where element_order >= ? and element_order <= ? and path = ?
			    CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			    CriteriaUpdate<Calculation> update = criteriaBuilder.createCriteriaUpdate(Calculation.class);
			    Root<Calculation> root = update.from(Calculation.class);

			    Expression<Integer> newOrder = criteriaBuilder.diff(root.get("elementOrder"), 1);
			    Predicate elementOrderPredicate = criteriaBuilder.between(root.get("elementOrder"), calculationOrder, targetCalculationOrder);
			    Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), parentPath);

			    update.set("elementOrder", newOrder)
			          .where(criteriaBuilder.and(elementOrderPredicate, pathPredicate));

			    session.createQuery(update).executeUpdate();
			}catch(Exception e) {
				log.error(e.getMessage());
			}			
		}else {
			log.error("Can't swap calculations with the same order field value.");
		}
	}

	private static void moveCalculationIntoProject(String parentPath, int calculationOrder) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    try {
	        // update calculations set element_order = element_order + 1 where element_order >= ? and path = ?
	        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
	        CriteriaUpdate<Calculation> update = criteriaBuilder.createCriteriaUpdate(Calculation.class);
	        Root<Calculation> root = update.from(Calculation.class);

	        Expression<Integer> newOrder = criteriaBuilder.sum(root.get("elementOrder"), 1);

	        Predicate elementOrderPredicate = criteriaBuilder.greaterThanOrEqualTo(root.get("elementOrder"), calculationOrder);
	        Predicate pathPredicate = criteriaBuilder.equal(root.get("path"), parentPath);

	        update.set("elementOrder", newOrder)
	              .where(criteriaBuilder.and(elementOrderPredicate, pathPredicate));

	        session.createQuery(update).executeUpdate();
	    }catch(Exception e) {
            log.error(e.getMessage());
        }
	}
	
}

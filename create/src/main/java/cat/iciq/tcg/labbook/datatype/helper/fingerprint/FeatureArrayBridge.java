package cat.iciq.tcg.labbook.datatype.helper.fingerprint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.hibernate.search.bridge.LuceneOptions;
import org.hibernate.search.bridge.StringBridge;
import org.hibernate.search.bridge.TwoWayFieldBridge;

// Define the custom field bridge for the feature field
public class FeatureArrayBridge implements TwoWayFieldBridge, StringBridge {

    @Override
    public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
        Integer[] values = (Integer[]) value;
        if (values != null) {
            for (Integer val : values) {
                luceneOptions.addNumericFieldToDocument(name, val, document);
            }
        }
    }

    @Override
    public Object get(String name, Document document) {
        List<Integer> values = new ArrayList<>();
        for (IndexableField field : document.getFields(name)) {
            values.add(Integer.parseInt(field.stringValue()));
        }
        return values.toArray(new Integer[values.size()]);
    }

    @Override
    public String objectToString(Object object) {
        if (object instanceof Integer)
            return Integer.toString((Integer) object);
        else
            return Arrays.toString((Integer[]) object);
    }

    public Object stringToObject(String stringValue) {
        if (stringValue == null || stringValue.isEmpty()) {
            return null;
        }
        String[] stringValues = stringValue.substring(1, stringValue.length() - 1).split(", ");
        Integer[] values = new Integer[stringValues.length];
        for (int i = 0; i < stringValues.length; i++) {
            values[i] = Integer.parseInt(stringValues[i]);
        }
        return values;
    }

}
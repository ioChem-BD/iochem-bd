/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.OrderService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class MCalc extends HibernateService {

	private static final long serialVersionUID = 1L;
    
    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try {
            String path = params.get(PARAMETER_NEWPATH);   
            String destinationPath = params.get(PARAMETER_NEWPARENTPATH); 

            if(PermissionService.hasPermissionOnProject(Paths.getParent(path), Permissions.WRITE)) {
                Calculation modifiedCalculation = getModifiedCalculation(path, destinationPath, params);
                if (destinationPath != null) {
                    if(isValidMovePath(modifiedCalculation.getParentPath()) && PermissionService.hasPermissionOnProject(modifiedCalculation.getParentPath(), Permissions.WRITE)) {
        				CalculationService.moveOverProject(modifiedCalculation, ProjectService.getByPath(destinationPath));
					}                        
                    else {
                        returnKO(response, PERMISSION_ERROR, WRITE_PERMISSION_ERROR);
                        return;
                    }
                } else 
                    CalculationService.update(modifiedCalculation);
                returnOK(response);
            } else {
                returnKO(response, PERMISSION_ERROR, WRITE_PERMISSION_ERROR);
            }
        } catch(Exception e) {
            returnKO(response, ACCESS_ERROR, e.getMessage());            
        }
    }

    private Calculation getModifiedCalculation(String path, String destinationPath, Map<String, String> params) throws Exception {
        Calculation calculation = CalculationService.getByPath(path);
        if(calculation == null)
            throw new Exception("Calculation not found.");
        if (destinationPath != null) {
            if(destinationPath.length()> 256)
                throw new Exception("Calculation path exceeds maximum length (256)");
            calculation.setParentPath(destinationPath);
        }
        if (params.get(PARAMETER_NEWDESC) != null) 
            calculation.setDescription(truncate(params.get(PARAMETER_NEWDESC),304));
        if (params.get(PARAMETER_NEWNAME) != null)  
            calculation.setName(truncate(Main.normalizeField(params.get(PARAMETER_NEWNAME)),64));
        return calculation;
    }

    private boolean isValidMovePath(String destinationPath) throws BrowseCredentialsException {
        return  destinationPath.startsWith(GeneralConstants.DB_ROOT + UserService.getUserName() + GeneralConstants.PARENT_SEPARATOR);
    }
}
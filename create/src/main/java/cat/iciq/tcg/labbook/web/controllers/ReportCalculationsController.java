package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;


import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.services.ReportService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class ReportCalculationsController extends HibernateService {
    @Override

    public void executePutService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        if(params.size() == 0){
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST,"Invalid JSON","Invalid JSON.").sendErrorResponse(response);
            return;
        }
        try {          
            int id = Integer.parseInt(params.get(PARAMETER_ID));
            Report report = ReportService.getReport(id);
            if (report == null) {
                new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Not found", "Could not find the report.")
                        .sendErrorResponse(response);
                return;
            }
            if (report.getOwnerId() != UserService.getUserId()) {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "Unauthorized",
                        "You don't have permission to delete this report.").sendErrorResponse(response);
                return;
            }

            List<String> listCalculations = params.containsKey("listCalculations")
            ? new ArrayList<>(Arrays.asList(params.get("listCalculations").split(",")))
            : new ArrayList<>();

            ArrayList<ReportCalculation> reportCalculations = (ArrayList<ReportCalculation>) ReportController.setCalculationsReport(listCalculations, report);
            report.getCalculations().clear();
            report.setCalculations(reportCalculations);
            ReportService.updateReport(report);
            returnOK(response);
        } catch (SecurityException u) {
            new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "Unauthorized", u.getMessage())
                    .sendErrorResponse(response);
        }
         catch (NumberFormatException e) {
            new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Id incorrect", "Invalid report id.")
                    .sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, LOAD_ERROR, "Check URL or parameters")
                    .sendErrorResponse(response);
        }
    }
}
/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.manager.actions;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Textbox;

/**
 * This class will allow displaying calculation files inside a textbox, it will be
 * used on formats that don't display as XSLT transformation.
 * TODO: Maybe in future developments will implement HtmlViewAction to display content inside a web browser  
 * 
 * @author malvarez
 *
 */
public class TextViewAction extends Action {

	private long maxPermittedFileSize 	= 524288;		//Only files with size < 0,5 Mb are allowed to be displayed inside our textbox,
	private String FILE_TOO_BIG_MESSAGE = "This file is too big to display, please download it.";
	
	@Override
	public void init() {		
		this.setInternalOrder(4);
		this.setTabID("downloadActionTab");
		this.setTabLabel("RAW CML");
		
		tab = new Tab();
	    tab.setId(this.tabID);
	    tab.setLabel(this.tabLabel);
	    tabpanel = new Tabpanel();		    
		//Maximize functionality			
		tab.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {		
				actionQueue.publish(new Event("maximizeRequested",null,null));						
			}			
		});
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void render() {
        if(filesByFormat.size() == 0)
            return;
	    

	    //Children tabbox
	    Tabbox tabbox = new Tabbox();
	    tabbox.setId("textViewActionTabbox");
	    tabbox.setHeight("100%");
	    Tabs childrenTabs = new Tabs();
	    Tabpanels childrenTabPanels = new Tabpanels();	    
	    tabbox.appendChild(childrenTabs);
	    tabbox.appendChild(childrenTabPanels);	    
	    int inx = 1;
	    for(String key : filesByFormat.keySet()){	    		
	            for(String fileName : filesByFormat.get(key)){	            		 
	            		try {	            			
							String filePath = findFileRealPathByName(fileName);	
							long fileSize = new File(filePath).length();
							//Add new tab 
							Tab childrenTab = new Tab();
		            		childrenTab.setId("textViewActionTabboxTab" + inx);
		            		childrenTab.setLabel(fileName);			            		
		            		childrenTab.setSclass("view-file-tab");
		            		childrenTab.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener(){
		            			@Override
		            			public void onEvent(Event event) throws Exception {		
		            				actionQueue.publish(new Event("maximizeRequested",null,null));						
		            			}			
		            		});
		            		
		            		Tabpanel childrenTabPanel = new Tabpanel();
		            		childrenTabPanel.setId("textViewActionTabboxTabPanel" + inx);		            		
		            				  		            		
		            				            	
		            		Textbox textbox = new Textbox();
		            		textbox.setId("textViewActionTabboxTextbox" + inx);
		            		textbox.setWidth("100%");
		            		textbox.setHeight("100%");
		            		textbox.setMultiline(true);	
		            		textbox.setSclass("text-view-action-content");
		            		if(fileSize > maxPermittedFileSize)
		            		{		            			
		            			textbox.setValue(FILE_TOO_BIG_MESSAGE);
		            		}
		            		else
		            		{
		            			String fileContent = FileUtils.readFileToString(new File(filePath));
		            			textbox.setValue(fileContent);
		            		}		            		
		            		childrenTabPanel.appendChild(textbox);
		            		childrenTabs.appendChild(childrenTab);
		            		childrenTabPanels.appendChild(childrenTabPanel);
						} catch (IOException e) {						
							continue;
						} catch (Exception e) {
							continue;
						}
	                 inx++;
	            }
	    }	    
	    tabpanel.appendChild(tabbox);
	    tab.setSelected(false);
	    setVisible(true);
	}

	@Override
	public void resizeContent(boolean maximize) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void resetZkControls() {
		while(!tabpanel.getChildren().isEmpty())
			tabpanel.getFirstChild().detach();		
	}
}

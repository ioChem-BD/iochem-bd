/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class LSPro extends HibernateService {
	
	private static final long serialVersionUID = 1L;

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName, Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
            
        String path = params.get(PARAMETER_NEWPATH);
    
        if(!PermissionService.hasPermissionOnProject(path, Permissions.READ)) {
            returnKO(response, PERMISSION_ERROR, READ_PERMISSION_ERROR);
            return;
        }
        
        
        List<Project> projects = ProjectService.getChildProjects(path);
        List<Calculation> calculations = ProjectService.getChildCalculations(path);
        List<EntityDTO> results = new ArrayList<>();

        String orderby = params.get(PARAMETER_ORDERBY);
        if (orderby != null) {
            sortProjects(projects, orderby);
            sortCalculations(calculations, orderby);
        }
        
        for(Project p: projects) { 
            EntityDTO entity = fromProject(p);
            if(entity != null)
                results.add(entity);
        }
                
        if(!calculations.isEmpty()) {
            Project p = ProjectService.getByPath(path); 
            String permissions = p.getLinuxPermissions();
            String owner = UserService.getUserNameById(p.getOwner());
            String group = UserService.getGroupNameById(p.getGroup());
            String conceptGroup = p.getConceptGroup();
            for(Calculation c: calculations) {
                EntityDTO entity = fromCalculation(c, permissions, owner, group, conceptGroup);
                if(entity != null)
                    results.add(entity);                
            }
        }

        sendObj(response, results);
    }
    
    private void sortCalculations(List<Calculation> calculations, String order) {
        Comparator<Calculation> comparator = null;

        switch(order) {
            case "t":   comparator = (Calculation c1, Calculation c2) -> c1.getCreationDate().compareTo(c2.getCreationDate());
                        break;
            case "n":
            default:    comparator = (Calculation c1, Calculation c2) -> c1.getName().compareTo(c2.getName());
                        break;
        }
        Collections.sort(calculations, comparator);
    }

    private void sortProjects(List<Project> projects, String order) {
        Comparator<Project> comparator = null;
        switch(order) {
            case "t":   comparator = (Project p1, Project p2) -> p1.getCreationDate().compareTo(p2.getCreationDate());
                        break;
            case "o":   comparator = (Project p1, Project p2) -> p1.getOwner() - p2.getOwner();
                        break;  
            case "g":   comparator = (Project p1, Project p2) -> p1.getGroup() - p2.getGroup();
                        break;
            case "c":   comparator = (Project p1, Project p2) -> p1.getConceptGroup().compareTo(p2.getConceptGroup());
                        break;
            case "s":   comparator = (Project p1, Project p2) -> p1.getState().name().compareTo(p2.getState().name());
                        break;                      
            case "n":
            default:    comparator = (Project p1, Project p2) -> p1.getName().compareTo(p2.getName());
                        break;
        }
        Collections.sort(projects, comparator);       
    }
}
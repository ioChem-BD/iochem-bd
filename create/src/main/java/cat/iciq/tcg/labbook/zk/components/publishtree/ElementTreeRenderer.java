/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree;

import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Window;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class ElementTreeRenderer implements TreeitemRenderer<ElementTreeNode> {
	
	private PublishTreeModel elementTreeModel;
	
	public ElementTreeRenderer(Window parentWindow, PublishTreeModel elementTreeModel){
		super();
		this.elementTreeModel 	= elementTreeModel;
	}
	
	public void render(final Treeitem treeItem, ElementTreeNode treeNode, int index) throws Exception {	
		ElementTreeNode etn = treeNode;
		Element element = (Element) etn.getData();
		Treerow dataRow = new Treerow();
		dataRow.setParent(treeItem);
		treeItem.setValue(etn);
		treeItem.setOpen(etn.isOpen());
		
		Hlayout hl 		= new Hlayout();	
		Label lblName  	= new Label(element.getPath()); 
		hl.appendChild(lblName);
		hl.setSclass("h-inline-block");
		Treecell treeCell = new Treecell();
		treeCell.appendChild(hl);
		if (element.isItem()) {			
			dataRow.setDraggable("false");
			dataRow.setDroppable("false");
		} else if(element.isCollection()) {			
			dataRow.setDraggable(String.valueOf(!element.isPublished()));
			dataRow.setDroppable(String.valueOf(!element.isPublished()));
			if(!element.isPublished())
				dataRow.addEventListener(Events.ON_DROP, new ElementDropEventListener(elementTreeModel, treeItem));			
		} else if(element.isCommunity())  {			
			lblName.setStyle("font-weight:bold");
			dataRow.setDraggable("false");
			dataRow.setDroppable("true");			
			dataRow.addEventListener(Events.ON_DROP, new ElementDropEventListener(elementTreeModel, treeItem));
		} else {
			throw new Exception("Unhandled Create element type");
		}		
		dataRow.appendChild(treeCell);		
		dataRow.appendChild(buildPublishTreecell(element));
		dataRow.appendChild(buildHandleTreecell(element));
		dataRow.appendChild(buildOrderTreecell(element));
		
		if(!element.isCommunity() && !element.isPublished())
			dataRow.addEventListener(Events.ON_CLICK, new ElementClickEventListener());
	}	
	
	private Treecell buildPublishTreecell(Element element) {
		Treecell treeCell = new Treecell();
		if(element.isPublished()){
			Image imgPublished = new Image("../images/published.png");			
			treeCell.appendChild(imgPublished);
		}			
		return treeCell;		
	}
	
	private Treecell buildHandleTreecell(Element element) {
		Treecell handleCell = new Treecell();    	
    	A a = new A();
    	String handleHref = Main.getHandleBaseUrl() + "/" + element.getHandle();
    	a.setHref(handleHref);
    	a.setLabel(element.getHandle());
    	a.setTarget("_blank");
    	handleCell.getChildren().add(a);    		
		return handleCell;
	}
	
	private Treecell buildOrderTreecell(Element element) {
		return new Treecell(String.valueOf(element.getOrder()));
	}
}


class ElementClickEventListener implements EventListener<Event> {
	
	public void onEvent(Event event) throws Exception {
		ElementTreeNode clickedNodeValue = (ElementTreeNode) ((Treeitem) event.getTarget().getParent()).getValue();		
		Treerow treerow	 	= (Treerow)event.getTarget();
		Treecell cell 		= (Treecell)treerow.getChildren().get(0);
		if(cell.getChildren().size()== 0){									//Editing collection name
						
		}else{
			if(cell.getChildren().get(0).getClass().equals(Textbox.class))	//Editing item name
				return;
			cell.removeChild(cell.getChildren().get(0));
		}
				
		Textbox txtEdit = new Textbox();
		txtEdit.addEventListener(Events.ON_BLUR, new onBlurEventListener(cell, clickedNodeValue, txtEdit));
		txtEdit.addEventListener(Events.ON_OK, 	 new onBlurEventListener(cell, clickedNodeValue, txtEdit));
		txtEdit.setText(clickedNodeValue.getData().getPath());
		cell.appendChild(txtEdit);		
		clickedNodeValue.getData().toString();	
		txtEdit.setFocus(true);
		txtEdit.setSelectionRange(txtEdit.getText().length(), txtEdit.getText().length());
	}

	private class onBlurEventListener implements EventListener<Event>{
		private ElementTreeNode treeNode;
		private Textbox txtEdit;
		private Treecell cell;
		
		public onBlurEventListener(Treecell cell, ElementTreeNode treeNode, Textbox txtEdit){
			this.treeNode 	= treeNode;
			this.txtEdit	= txtEdit;
			this.cell 		= cell;
		}
		
		public void onEvent(Event event) throws Exception {			
					treeNode.getData().setPath(txtEdit.getText());
					cell.removeChild(cell.getChildren().get(0));
					cell.appendChild(new Label(txtEdit.getText()));					
		}
	}
	
}


class ElementDropEventListener implements EventListener<Event> {
	
	private Treeitem treeItem 				  = null;
	private PublishTreeModel elementTreeModel = null;

	public ElementDropEventListener(PublishTreeModel elementTreeModel, Treeitem treeItem){
		super();
		this.treeItem = treeItem;
		this.elementTreeModel = elementTreeModel;
	}	
	
	public void onEvent(Event event) throws Exception {
		Treeitem draggedItem = (Treeitem) ((DropEvent) event).getDragged().getParent();
		ElementTreeNode draggedValue = (ElementTreeNode) draggedItem.getValue();
		Treeitem parentItem = treeItem.getParentItem();				
		elementTreeModel.remove(draggedValue);
		elementTreeModel.add((ElementTreeNode) treeItem.getValue(),	new DefaultTreeNode[] { draggedValue });
	}

}


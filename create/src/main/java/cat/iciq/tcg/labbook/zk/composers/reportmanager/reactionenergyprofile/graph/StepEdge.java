package cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph;

import org.jgrapht.graph.DefaultEdge;

public class StepEdge extends DefaultEdge {
	
	private Step step;

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="calculation_types", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CalculationType implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)        
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "calculation_types_id_seq")
    @SequenceGenerator(name = "calculation_types_id_seq", sequenceName = "calculation_types_id_seq", allocationSize = 1)
	private int id;
    
    @Column(name = "creation_time")
	private Timestamp creationTime;
    
    @Column(name = "abr", nullable = false)
	private String abbreviation;
    
    @Column(name = "name", nullable = false)
	private String name;
    
    @Column(name = "description", nullable = false)
	private String description;
    
    @Column(name = "metadata_template", nullable = false)
	private String metadataTemplate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMetadataTemplate() {
		return metadataTemplate;
	}
	public void setMetadataTemplate(String metadataTemplate) {
		this.metadataTemplate = metadataTemplate;
	}
	

}

package cat.iciq.tcg.labbook.datatype.helper;

import java.util.BitSet;

import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.java.ImmutableMutabilityPlan;

import cat.iciq.tcg.labbook.datatype.Project;

public class BitSetJavaDescriptor extends AbstractTypeDescriptor<BitSet> {

    public static final BitSetJavaDescriptor INSTANCE = new BitSetJavaDescriptor();

    public BitSetJavaDescriptor() {
        super(BitSet.class, ImmutableMutabilityPlan.INSTANCE);
    }

    @Override
    public BitSet fromString(String string) {
        BitSet bitSet = new BitSet();
        for(int i = 0; i < string.length(); i++)
            if(string.charAt(i) == '1')
                bitSet.set(i);
        return bitSet;
    }
    
    
    public String toString(BitSet bitSet) {
        StringBuilder sb = new StringBuilder(); 
        for(int i = 0; i < Project.PERMISSIONS_LENGHT; i++) {
            sb.append(bitSet.get(i)? "1": "0");            
        }
      return sb.toString();   
    }

    @Override
    public <X> X unwrap(BitSet value, Class<X> type, WrapperOptions options) {
        if (value == null)
            return null;
        if (BitSet.class.isAssignableFrom(type))
            return (X) toString(value);
        throw unknownUnwrap(type);
    }

    @Override
    public <X> BitSet wrap(X value, WrapperOptions options) {
        if (value == null)
            return null;
        if (String.class.isInstance(value))
            return fromString((String) value);
        throw unknownWrap(value.getClass());
    }

}
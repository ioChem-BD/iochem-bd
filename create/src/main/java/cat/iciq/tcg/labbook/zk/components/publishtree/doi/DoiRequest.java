/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree.doi;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

public class DoiRequest {
	private String doiBatchId;
	private String depositorEmailAddress;
	private String depositorName;
	private String title; 
	
	private List<Author> authors;

	public String getDoiBatchId() {
		return doiBatchId;
	}

	public void setDoiBatchId(String doiBatchId) {
		this.doiBatchId = doiBatchId;
	}

	public String getDepositorEmailAddress() {
		return depositorEmailAddress;
	}

	public void setDepositorEmailAddress(String depositorEmailAddress) {
		this.depositorEmailAddress = depositorEmailAddress;
	}

	public String getDepositorName() {
		return depositorName;
	}

	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"doiBatchId\":");
		sb.append("\""+ escapeJson(this.getDoiBatchId()) +"\"");
		sb.append(",");
		
		sb.append("\"depositorEmailAddress\":");
		sb.append("\""+ escapeJson(this.getDepositorEmailAddress()) +"\"");
		sb.append(",");
		
		sb.append("\"depositorName\":");
		sb.append("\""+ escapeJson(this.getDepositorName()) +"\"");
		sb.append(",");
		
		sb.append("\"title\":");
		sb.append("\""+ escapeJson(this.getTitle()) +"\"");
		sb.append(",");
		//Build author array
		sb.append("\"authors\": [");
		Author lastAuthor = getAuthors().get(getAuthors().size()-1);
		for(Author author : getAuthors()){			
			sb.append(author.getJson());
			if(!author.equals(lastAuthor))
				sb.append(",");
		}			
		sb.append("]");		
		sb.append("}");		
		return sb.toString();
	}
	
	public static String escapeJson(String input){
		return input.replace("\"", "\\\"");
		
	}
	
}



package cat.iciq.tcg.labbook.web.cas;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.zkoss.zk.ui.WebApps;

public class FilterMaintenance implements Filter {

  private static boolean isUnderMaintenance = false;
  
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {       
    isUnderMaintenance = (boolean)WebApps.getCurrent().getAttribute("isUnderMaintenance");    
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)  throws IOException, ServletException {    
    HttpServletRequest httpRequest = (HttpServletRequest) req;
    HttpServletResponse httpResponse = (HttpServletResponse) resp;
    String path = httpRequest.getRequestURI();

    if (path.startsWith("/create/public")) {
      chain.doFilter(req, resp);
      return;
    }
    
    if(!isUnderMaintenance) {
      chain.doFilter(req, resp);
    } else {
      httpResponse.sendRedirect("/create/public/errors/maintenance.html");
    }
  }

  @Override
  public void destroy() {   
    
  }

}
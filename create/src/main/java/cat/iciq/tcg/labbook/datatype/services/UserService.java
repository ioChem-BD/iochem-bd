package cat.iciq.tcg.labbook.datatype.services;

import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pac4j.cas.profile.CasProfile;
import org.pac4j.cas.profile.CasRestProfile;
import org.pac4j.core.profile.CommonProfile;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.http.SimpleSession;

import cat.iciq.tcg.labbook.shell.exceptions.ServerConnectionException;
import cat.iciq.tcg.labbook.web.controllers.ApiJWT;
import cat.iciq.tcg.labbook.web.utils.Rest50ApiManager;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;

public class UserService {

    private static final ThreadLocal<HttpSession> userContext = new ThreadLocal<>();

    private static final Logger log = LogManager.getLogger(UserService.class);
    public static final String CAS_USER_PATH = "user_path";
    public static final String CAS_USER_EMAIL = "user_email";
    public static final String CAS_USER_ID = "user_id";
    public static final String CAS_USER_FULL_NAME = "user_fullname";

    public static final String CAS_USER_MAIN_GROUP_ID = "main_group_id";
    public static final String CAS_USER_ASSOCIATED_GROUPS_ID = "group_id";
    private static final String CAS_FIELD_DELIMITER = "/";

    public static final String BROWSE_USERNAMES = "userNames";
    public static final String BROWSE_USER_GROUPS = "userGroups";

    public static String getUserName() {
        isUserInfoLoaded();
        if (userContext.get() == null)
            return (String) Executions.getCurrent().getSession().getAttribute(CAS_USER_PATH);
        else
            return (String) userContext.get().getAttribute(CAS_USER_PATH);
    }

    public static String getUserEmail() {
        isUserInfoLoaded();
        if (userContext.get() == null)
            return (String) Executions.getCurrent().getSession().getAttribute(CAS_USER_EMAIL);
        else
            return (String) userContext.get().getAttribute(CAS_USER_EMAIL);
    }

    public static int getUserId() {
        isUserInfoLoaded();
        if (userContext.get() == null)
            return (Integer) Executions.getCurrent().getSession().getAttribute(CAS_USER_ID);
        else
            return (Integer) userContext.get().getAttribute(CAS_USER_ID);
    }

    public static String[] getUserGroups() {
        isUserInfoLoaded();
        if (userContext.get() == null)
            return (String[]) Executions.getCurrent().getSession().getAttribute(CAS_USER_ASSOCIATED_GROUPS_ID);
        else
            return (String[]) userContext.get().getAttribute(CAS_USER_ASSOCIATED_GROUPS_ID);
    }

    public static String getUserFullName() {
        isUserInfoLoaded();
        if (userContext.get() == null)
            return (String) Executions.getCurrent().getSession().getAttribute(CAS_USER_FULL_NAME);
        else
            return (String) userContext.get().getAttribute(CAS_USER_FULL_NAME);
    }

    public static int getMainGroupId() {
        isUserInfoLoaded();
        if (userContext.get() == null)
            return (Integer) Executions.getCurrent().getSession().getAttribute(CAS_USER_MAIN_GROUP_ID);
        else
            return (Integer) userContext.get().getAttribute(CAS_USER_MAIN_GROUP_ID);
    }

    private static void isUserInfoLoaded() {
        if (userContext.get() != null) // Already loaded from servlet HttpRequest
            return;
        if (Executions.getCurrent().getSession().getAttribute(CAS_USER_PATH) == null) { // Not loaded on zk session
            try {
                loadUserInfoFromPac4j();
            } catch (Exception e) {
                log.error("Error loading user info from Browse", e);
            }
        }
    }

    public static void setUserInfoFromRequest(HttpServletRequest req, Map<String, Object> attributes) {
        HttpSession session = req.getSession();
        CommonProfile casProfile = getCasProfile(session.getAttribute("pac4jUserProfiles"));
        casProfile.addAttributes(attributes);
        casProfile.getAttribute(CAS_USER_FULL_NAME);
        casProfile.getAttribute(CAS_USER_FULL_NAME);
    }

    public static void loadUserInfoFromRequest(HttpServletRequest req) {
        // TODO: Refactor this code to better handle user info retrieval from shell
        // client services and from REST API calls
        if (userContext.get() != null && userContext.get().getAttribute(CAS_USER_PATH) != null)
            return;
        HttpSession session = req.getSession();
        CommonProfile casProfile = getCasProfile(session.getAttribute("pac4jUserProfiles"));
        session.setAttribute(CAS_USER_PATH, casProfile.getAttribute(CAS_USER_PATH));
        session.setAttribute(CAS_USER_EMAIL, casProfile.getAttribute(CAS_USER_EMAIL));
        session.setAttribute(CAS_USER_ID, Integer.valueOf((String) casProfile.getAttribute(CAS_USER_ID)));
        session.setAttribute(CAS_USER_FULL_NAME, casProfile.getAttribute(CAS_USER_FULL_NAME));
        session.setAttribute(CAS_USER_MAIN_GROUP_ID,
                Integer.valueOf((String) casProfile.getAttribute(CAS_USER_MAIN_GROUP_ID)));
        session.setAttribute(CAS_USER_ASSOCIATED_GROUPS_ID,
                ((String) casProfile.getAttribute(CAS_USER_ASSOCIATED_GROUPS_ID)).split(CAS_FIELD_DELIMITER));
        userContext.set(session);
    }

    private static void loadUserInfoFromPac4j() {
        SimpleSession session = (SimpleSession) Executions.getCurrent().getSession();
        CommonProfile casProfile = getCasProfile(session.getAttribute("pac4jUserProfiles"));
        session.setAttribute(CAS_USER_PATH, casProfile.getAttribute(CAS_USER_PATH));
        session.setAttribute(CAS_USER_EMAIL, casProfile.getAttribute(CAS_USER_EMAIL));
        session.setAttribute(CAS_USER_ID, Integer.valueOf((String) casProfile.getAttribute(CAS_USER_ID)));
        session.setAttribute(CAS_USER_FULL_NAME, casProfile.getAttribute(CAS_USER_FULL_NAME));
        session.setAttribute(CAS_USER_MAIN_GROUP_ID,
                Integer.valueOf((String) casProfile.getAttribute(CAS_USER_MAIN_GROUP_ID)));
        session.setAttribute(CAS_USER_ASSOCIATED_GROUPS_ID,
                ((String) casProfile.getAttribute(CAS_USER_ASSOCIATED_GROUPS_ID)).split(CAS_FIELD_DELIMITER));
    }

    public static void loadUserInfoFromApi(HttpServletRequest req, String token) throws ParseException {
        HttpSession session = req.getSession();

        String key = (String) WebApps.getCurrent().getAttribute("keyJWT");

        HashMap<String, Object> attr = ApiJWT.extractDataJWT(token, key);
        session.setAttribute(CAS_USER_PATH, attr.get(CAS_USER_PATH));
        session.setAttribute(CAS_USER_EMAIL, attr.get(CAS_USER_EMAIL));
        session.setAttribute(CAS_USER_ID, Integer.valueOf((String) attr.get(CAS_USER_ID)));
        session.setAttribute(CAS_USER_FULL_NAME, attr.get(CAS_USER_FULL_NAME));
        session.setAttribute(CAS_USER_MAIN_GROUP_ID, Integer.valueOf((String) attr.get(CAS_USER_MAIN_GROUP_ID)));
        session.setAttribute(CAS_USER_ASSOCIATED_GROUPS_ID,
                ((String) attr.get(CAS_USER_ASSOCIATED_GROUPS_ID)).split(CAS_FIELD_DELIMITER));
        userContext.set(session);
    }

    private static CommonProfile getCasProfile(Object profilesObj) {
        try {
            if (profilesObj instanceof LinkedHashMap) {
                Map<String, Object> profiles = (LinkedHashMap<String, Object>) profilesObj;
                if (profiles.containsKey("CasClient"))
                    return (CasProfile) profiles.get("CasClient");
                else if (profiles.containsKey("CasRestClient"))
                    return (CasRestProfile) profiles.get("CasRestClient");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        throw new UnsupportedOperationException("Error raised while retrieving user profile");
    }

    public static boolean isValidSubject() {
        return !getUserName().isEmpty() && getMainGroupId() > 0;
    }

    /*
     * The following fields are retrieved from Browse module via REST API because
     * it can be too much large to be retrieved along with user login information
     */

    public static int getGroupIdByName(String groupName) {
        try {
            checkUsersAndGroupsInfoLoaded();
            return (int) ((DualHashBidiMap) WebApps.getCurrent().getAttribute(BROWSE_USER_GROUPS)).getKey(groupName);
        } catch (Exception e) {
            log.error(
                    String.format("Error raised while retrieving group id for name %s. %s", groupName, e.getMessage()));
        }
        return 0;
    }

    public static String getGroupNameById(int groupId) {
        try {
            checkUsersAndGroupsInfoLoaded();
            return (String) ((DualHashBidiMap) WebApps.getCurrent().getAttribute(BROWSE_USER_GROUPS)).get(groupId);

        } catch (Exception e) {
            log.error(String.format("Error raised while retrieving group name for id %d. %s", groupId, e.getMessage()));
        }
        return "";
    }

    public static int getUserIdByName(String userName) {
        try {
            checkUsersAndGroupsInfoLoaded();
            return (int) ((DualHashBidiMap) WebApps.getCurrent().getAttribute(BROWSE_USERNAMES)).getKey(userName);
        } catch (Exception e) {
            log.error(String.format("Error raised while retrieving user id for name %s. %s", userName, e.getMessage()));
        }
        return 0;
    }

    public static String getUserNameById(int userId) {
        try {
            checkUsersAndGroupsInfoLoaded();
            return (String) ((DualHashBidiMap) WebApps.getCurrent().getAttribute(BROWSE_USERNAMES)).get(userId);
        } catch (Exception e) {
            log.error(
                    String.format("Error raised while retrieving username name for id %d. %s", userId, e.getMessage()));
        }
        return "";
    }

    public static int getNumberOfUsers() {
        try {
            checkUsersAndGroupsInfoLoaded();
            return ((DualHashBidiMap) WebApps.getCurrent().getAttribute(BROWSE_USERNAMES)).size();
        } catch (Exception e) {
            log.error(String.format("Error raised while retrieving the number of users %s", e.getMessage()));
        }
        return 0;
    }

    public static int getNumberOfGroups() {
        try {
            checkUsersAndGroupsInfoLoaded();
            return ((DualHashBidiMap) WebApps.getCurrent().getAttribute(BROWSE_USER_GROUPS)).size();
        } catch (Exception e) {
            log.error(String.format("Error raised while retrieving the number of user groups %s", e.getMessage()));
        }
        return 0;
    }

    public static Map<Integer, String> getUsers() throws ServerConnectionException {
        try {
            checkUsersAndGroupsInfoLoaded();
            return (Map<Integer, String>) WebApps.getCurrent().getAttribute(BROWSE_USERNAMES);
        } catch (Exception e) {
            log.error(String.format("Error raised while retrieving the users info from session. %s", e.getMessage()));
            throw new ServerConnectionException();
        }
    }

    public static Map<Integer, String> getGroups() throws ServerConnectionException {
        try {
            checkUsersAndGroupsInfoLoaded();
            return (Map) WebApps.getCurrent().getAttribute(BROWSE_USER_GROUPS);

        } catch (Exception e) {
            log.error(String.format("Error raised while retrieving the user groups info from session. %s",
                    e.getMessage()));
            throw new ServerConnectionException();
        }
    }

    private static void checkUsersAndGroupsInfoLoaded() throws Exception {
        if (!WebApps.getCurrent().hasAttribute(BROWSE_USERNAMES))
            loadUsersInfoFromBrowse();
    }

    private static void loadUsersInfoFromBrowse() throws Exception {
        Rest50ApiManager restManager = initRestManager(getUserEmail());
        try {
            WebApps.getCurrent().setAttribute(BROWSE_USERNAMES, loadUsers(restManager));
            WebApps.getCurrent().setAttribute(BROWSE_USER_GROUPS, loadUsergroups(restManager));
        } finally {
            closeRestManager(restManager);
        }
    }

    /**
     * User information is stored on Browse module.
     * Will use REST API calls to retrieve users and usergroups.
     * 
     * @return
     * @throws Exception
     */
    private static Rest50ApiManager initRestManager(String email) throws Exception {
        try {
            return new Rest50ApiManager(email, 30, 30, 30);
        } catch (Exception e) {
            log.error("Error creating RestManager for publication notification");
            log.error(e.getMessage());
            throw e;
        }
    }

    private static Map loadUsers(Rest50ApiManager restManager) throws Exception {
        for (int retries = 0; retries < 10; retries++) {
            Map users = new DualHashBidiMap();
            try {
                XMLFileManager xml = restManager.getPlatformUsers();
                NodeList userList = xml.getItemIteratorQuery("/users/user");
                for (int inx = 0; inx < userList.getLength(); inx++) {
                    Element user = (Element) userList.item(inx);
                    String username = user.getAttribute("username");
                    int id = Integer.parseInt(user.getAttribute("id"));
                    users.put(id, username);
                }
                return users;
            } catch (Exception e) {
                log.error("Error loading users from Browse module, retrying.");
                log.error(e.getMessage());
            }
        }
        throw new Exception("Cannot retrieve users from Browse module");
    }

    private static Map loadUsergroups(Rest50ApiManager restManager) throws Exception {
        for (int retries = 0; retries < 10; retries++) {
            Map groups = new DualHashBidiMap();
            try {
                XMLFileManager xml = restManager.getPlatformUsergroup();
                NodeList userList = xml.getItemIteratorQuery("/groups/group");
                for (int inx = 0; inx < userList.getLength(); inx++) {
                    Element user = (Element) userList.item(inx);
                    String username = user.getAttribute("name");
                    int id = Integer.parseInt(user.getAttribute("id"));
                    groups.put(id, username);
                }
                return groups;
            } catch (Exception e) {
                log.error("Error loading user groups from Browse module, retrying.");
                log.error(e.getMessage());
            }
        }
        throw new Exception("Cannot retrieve user groups from Browse module");
    }

    private static void closeRestManager(Rest50ApiManager restManager) {
        try {
            restManager.close();
        } catch (Exception e) {
            log.error("Error closing RestManager after publication process.");
            log.error(e.getMessage());
        }
    }

    public static void clearUserInfoFromRequest() {
        userContext.remove();
    }
}

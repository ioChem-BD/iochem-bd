/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.SerializationUtils;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class MPro extends HibernateService {
    
	private static final long serialVersionUID = 1L;

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try { 
            String username = UserService.getUserName();
            String path = params.get(PARAMETER_NEWPATH);
            String destinationPath = params.get(PARAMETER_NEWPARENTPATH);
            if(PermissionService.hasPermissionOnProject(path, Permissions.WRITE)) {
                Project project = (Project)SerializationUtils.clone(ProjectService.getByPath(path));
                if (params.get(PARAMETER_NEWGROUP) != null) {
                    String group = params.get(PARAMETER_NEWGROUP);
                    if(belongsToGroup(group))
                        project.setGroup(UserService.getGroupIdByName(group));
                    else
                        throw new Exception("Current user doesn't belong to the provided user group.");
                }
                if (params.get(PARAMETER_NEWPERM) != null) {
                    String permissions = params.get(PARAMETER_NEWPERM).trim();
                    if(!Pattern.matches("rw(----|r---|rw--|rwr-|rwrw)", permissions))
                        throw new Exception("Invalid permissions, please use a permission string made of six r, w or - characters, similar to the Linux ones. Hyphen avoids permissions. Start always with rw, ex. rw---- , rwr---, rwrw--");
                    project.setLinuxPermissions(permissions);
                }
                if (params.get(PARAMETER_NEWDESC) != null) 
                    project.setDescription(truncate(params.get(PARAMETER_NEWDESC), 304));
                if (params.get(PARAMETER_NEWCG) != null) 
                    project.setConceptGroup(truncate(params.get(PARAMETER_NEWCG), 64));
                if (params.get(PARAMETER_NEWNAME) != null) {
                    project.setName(truncate(Main.normalizeField(params.get(PARAMETER_NEWNAME)), 64));
                    if(destinationPath == null && ProjectService.projectExists(project.getParentPath(), project.getName()))
                        throw new Exception(PROJ_ALREADY_EXIST_ERROR);
                }
                if (destinationPath != null) {
                    if(destinationPath.length()> 512)
                        throw new Exception("Project path exceeds maximum length (256)");
                    if(isValidMovePath(username, destinationPath) && PermissionService.hasPermissionOnProject(destinationPath, Permissions.WRITE)) {                        
                        if(isValidMove(project, destinationPath)) {
                            project.setParentPath(destinationPath);
                            ProjectService.update(project, destinationPath);
                        } else {
                            returnKO(response, PRO_MOD_ERROR, ACCESS_ERROR);
                            return;
                        }
                    } else {
                        returnKO(response, PRO_MOD_ERROR, ACCESS_ERROR);
                        return;
                    }
                } else
                    ProjectService.update(project);
                returnOK(response);
            }else {
                returnKO(response, PRO_MOD_ERROR, ACCESS_ERROR);
            }
        } catch(Exception e) {
            returnKO(response, PRO_MOD_ERROR, e.getMessage());
        }
    }

	private boolean belongsToGroup(String group) throws Exception {
		if(group.equals(""))
			return false;		
		try {			
			int groupId = UserService.getGroupIdByName(group);
			for(String userGroup: UserService.getUserGroups()) 
				if(Integer.parseInt(userGroup) == groupId)
					return true;	
		}catch(Exception e) {
			throw new Exception(GROUP_DOESNT_EXIST_ERROR);
		}
		return false;
	}

	private boolean isValidMovePath(String username, String destinationPath) {
		return destinationPath.startsWith(GeneralConstants.DB_ROOT + username) ;
	}

	private boolean isValidMove(Project source, String destinationPath){
		return !(isMovingToSameParentOrItself(source, destinationPath) ||
 				isDestinationChildOfSelection(source, destinationPath) ||
 				existsCollisionsOnDestinationPath(source, destinationPath));
	}

	private boolean isMovingToSameParentOrItself(Project source, String destinationPath){
		return source.getPath().equals(destinationPath) || 
				source.getPath().equals(Paths.getFullPath(destinationPath, source.getName()));
	}
 
	private boolean isDestinationChildOfSelection(Project source, String destinationPath){		
		return Paths.isDescendant(source.getPath(), destinationPath);			
	}

	private boolean existsCollisionsOnDestinationPath(Project source, String destinationPath){		
		try {
			return ProjectService.projectExists(destinationPath, source.getName());
		} catch (Exception e) {
			return true;
		}
	}
}
/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.SearchService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class FindPro extends HibernateService {

	private static final long serialVersionUID = 1L;

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
        CriteriaBuilder criteriaBuilder = SearchService.getBuilder();
        CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
        try {                          
            Root<Project> root = criteriaQuery.from(Project.class);
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(SearchService.addUserPermissionFilters(criteriaBuilder, criteriaQuery));
            
            // Adding name search condition
            if (params.get(PARAMETER_NAMESEARCH) != null) {
                Predicate nameSearchCondition = criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + params.get(PARAMETER_NAMESEARCH).toLowerCase() + "%");
                predicates.add(nameSearchCondition);
            }

            // Adding path search condition
            if (params.get(PARAMETER_PATHSEARCH) != null) {
                Predicate pathSearchCondition = criteriaBuilder.like(criteriaBuilder.lower(root.get("path")), "%" + params.get(PARAMETER_PATHSEARCH).toLowerCase() + "%");
                predicates.add(pathSearchCondition);
            } else {
                Predicate currentPathCondition = criteriaBuilder.like(criteriaBuilder.lower(root.get("path")), params.get(PARAMETER_PATH) + "%");
                predicates.add(currentPathCondition);
            }

            // Adding description search condition
            if (params.get(PARAMETER_DESCSEARCH) != null) {
                Predicate descSearchCondition = criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + params.get(PARAMETER_DESCSEARCH).toLowerCase() + "%");
                predicates.add(descSearchCondition);
            }

            criteriaQuery.multiselect(root.get("name"), root.get("path"));
            criteriaQuery.where(predicates.toArray(new Predicate[0]));            
            
            
            if (predicates.size() > 1) {
                List<Object[]> results = SearchService.launchQuery(criteriaQuery);                
                List<String[]> values = new ArrayList<>();
                for(Object[] result: results) 
                    values.add(new String[] {(String) result[0], (String) result[1]});                    
                sendObj(response, values);            
            } else {
                returnOK(response);
            }           
        }catch(Exception e) {
            returnKO(response, PROJECT_ERROR, READ_PERMISSION_ERROR);
        }
    }
}
/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class JCampDX extends HibernateService {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(JCampDX.class.getName());
		
	private static final String XSLT_PATH 				= "html/xslt/cml2jcampdx.xsl";
	private static final String JCAMPDX_MIMETYPE 		= "chemical/x-jcamp-dx";
	private static final String JCAMPDX_FILENAME		= "cml2jcampdx.jdx";
	private static TransformerFactory tFactory 			= null; 
	private static Templates template					= null;
	
	static {
		tFactory	=  new net.sf.saxon.TransformerFactoryImpl();
	}
	
	public static void flushXsltTemplates(){
		template = null;		
	}

	private String convertOutputToJCampDX(int calculationId, String index) {		
        try {
        	String realFilePath = CalculationService.getOutputFilePath(calculationId);
    		StreamSource xml = new StreamSource(new File(realFilePath));	    
    	    StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            Transformer transformer = template.newTransformer();
            transformer.setParameter(PARAMETER_INDEX, index);
            transformer.transform(xml, result);            
            return writer.toString();
        } catch (Exception e) {
            return "";
        }
	}

	@Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
		if(template == null){		//First call to servlet will load conversion template
			StreamSource xslSource =  new StreamSource(new File(this.getServletContext().getRealPath("/") + XSLT_PATH));
			try {
				template = tFactory.newTemplates(xslSource);
			} catch (TransformerConfigurationException e) {
				
			}
		}
		
		try	{
			int calculationId = Integer.parseInt(params.get(PARAMETER_ID));
			String index = params.get(PARAMETER_INDEX) == null ? "" : params.get(PARAMETER_INDEX);
	
			if(PermissionService.hasPermissionOnCalculation(calculationId, Permissions.READ)){
				String jcampFileContent = convertOutputToJCampDX(calculationId, index);								    
				response.setContentType(JCAMPDX_MIMETYPE);
				response.setContentLength(jcampFileContent.length());
				response.setHeader("Content-Disposition", "attachment; filename=\"" + JCAMPDX_FILENAME + "\"");
				response.getOutputStream().write(jcampFileContent.getBytes());							
			}else{
				response.sendError(HttpServletResponse.SC_FORBIDDEN);	
			}		
		}catch (Exception e) {
			logger.error(e.getMessage());		
			returnSystemError(response, SERVER_ERROR,e.getMessage());
		}
	}
}

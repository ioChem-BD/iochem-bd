package cat.iciq.tcg.labbook.datatype.helper;

import java.util.BitSet;

import org.hibernate.dialect.Dialect;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

public class BitSetType extends AbstractSingleColumnStandardBasicType<BitSet> {

    public static final BitSetType INSTANCE = new BitSetType();

    public BitSetType() {
        super(BitSetSqlDescriptor.INSTANCE, BitSetJavaDescriptor.INSTANCE);
    }

    @Override
    public String getName() {
        return "BitSet";
    }

    public BitSet stringToObject(String xml) {
        return fromString(xml);
    }

    public String objectToSQLString(BitSet value, Dialect dialect) {
        return '\'' + BitSetJavaDescriptor.INSTANCE.toString(value) + '\'';
    }
    
}
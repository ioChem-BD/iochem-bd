/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class ProjectService {

	private static final Logger log = LogManager.getLogger(ProjectService.class);

	// TODO: Valid
	public static long add(Project project) throws Exception {

		if (!PermissionService.isOwner(project, UserService.getUserName()))
			throw new Exception("Can't create project, invalid path or not enough permissions.");
		if (!PermissionService.hasPermissionOnProject(project.getParentPath(), Permissions.WRITE))
			throw new Exception("Can't create project, invalid path or not enough permissions.");
		if (projectExists(project.getParentPath(), project.getName()))
			throw new Exception("A project with this name already exists.");
		if (project.getParentPath().length() > 512)
			throw new SQLException("Project path exceeds maximum length (256)");
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			project.setElementOrder(OrderService.getNewProjectOrderIndex(project.getParentPath()));
			project.setHandle("");
			project.setPublishedName("");
			project.setPublished(false);
			project.setState(Project.State.created);
			project.setCreationDate(new Timestamp(System.currentTimeMillis()));
			session.persist(project);
			session.flush();
			TreeEvent.sendEventToUserQueue(new Event("projectAdded", null, String.valueOf(project.getId())));
			return project.getId();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	// TODO: Valid
	public static boolean projectExists(String path, String name) throws Exception {
		return getByPath(Paths.getFullPath(path, name)) != null;
	}

	// TODO: Valid
	public static Project getById(long projectId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			return session.get(Project.class, projectId);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}

	// TODO: Valid
	public static Project getByPath(String projectPath) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);
			Root<Project> root = criteriaQuery.from(Project.class);

			Predicate path = builder.equal(root.get("path"), Paths.getParent(projectPath));
			Predicate name = builder.equal(root.get("name"), Paths.getTail(projectPath));
			criteriaQuery.select(root).where(builder.and(path, name));

			Query<Project> query = session.createQuery(criteriaQuery);
			return query.getSingleResult();
		} catch (NoResultException e1) {

		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	public static List<Project> getDescendantProjects(String projectPath) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);
			Root<Project> root = criteriaQuery.from(Project.class);
			Predicate pathRegex = builder.like(root.get("path"), projectPath + "/%");
			Predicate pathEquals = builder.equal(root.get("path"), projectPath);
			Order orderByPath = builder.asc(root.get("path"));
			Order orderByElementOrder = builder.asc(root.get("elementOrder"));
			criteriaQuery.orderBy(orderByPath, orderByElementOrder);
			criteriaQuery.select(root).where(builder.or(pathRegex, pathEquals));
			Query<Project> query = session.createQuery(criteriaQuery);
			return query.getResultList();

		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return new ArrayList<>();
	}

	public static List<Calculation> getDescendantCalculations(String projectPath) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Calculation> criteriaQuery = builder.createQuery(Calculation.class);
			Root<Calculation> root = criteriaQuery.from(Calculation.class);
			Predicate pathRegex = builder.like(root.get("path"), projectPath + "/%");
			Predicate pathEquals = builder.equal(root.get("path"), projectPath);
			Order orderByPath = builder.asc(root.get("path"));
			Order orderByElementOrder = builder.asc(root.get("elementOrder"));
			criteriaQuery.orderBy(orderByPath, orderByElementOrder);
			criteriaQuery.select(root).where(builder.or(pathRegex, pathEquals));
			Query<Calculation> query = session.createQuery(criteriaQuery);
			return query.getResultList();

		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return new ArrayList<>();
	}

	// TODO: Valid
	public static List<Project> getChildProjects(String parentPath) {
		return getChildProjects(parentPath, true);
	}

	// TODO: Valid
	public static List<Project> getChildProjects(String parentPath, boolean ordered) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		List<Project> results = new ArrayList<>();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Project> criteriaQuery = builder.createQuery(Project.class);
			Root<Project> root = criteriaQuery.from(Project.class);

			Predicate predicate = builder.equal(root.get("path"), parentPath);
			criteriaQuery.where(predicate);
			if (ordered)
				criteriaQuery.orderBy(builder.asc(root.get("elementOrder")));

			Query<Project> query = session.createQuery(criteriaQuery);
			for (Project project : query.getResultList())
				results.add((Project) SerializationUtils.clone(project));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return results;
	}

	// TODO: Valid
	public static List<Calculation> getChildCalculations(String parentPath) {
		return getChildCalculations(parentPath, true);
	}

	// TODO: Valid
	public static List<Calculation> getChildCalculations(String parentPath, boolean ordered) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		List<Calculation> results = new ArrayList<>();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Calculation> criteriaQuery = builder.createQuery(Calculation.class);
			Root<Calculation> root = criteriaQuery.from(Calculation.class);

			Predicate predicate = builder.equal(root.get("path"), parentPath);
			criteriaQuery.where(predicate);
			if (ordered)
				criteriaQuery.orderBy(builder.asc(root.get("elementOrder")));

			Query<Calculation> query = session.createQuery(criteriaQuery);
			for (Calculation calculation : query.getResultList())
				results.add((Calculation) SerializationUtils.clone(calculation));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return results;
	}

	public static void deleteProject(long id) throws Exception {
		deleteProject(id, true);
	}

	// TODO: Check deleteChildCalculations(project);
	public static void deleteProject(long id, boolean checkPermissions) throws Exception {
		if (checkPermissions && !PermissionService.hasPermissionOnProject(id, Permissions.DELETE))
			throw new Exception("Not enough permissions to delete project.");
		Project project = ProjectService.getById(id);
		if (project == null)
			return;

		int removedCalculations = deleteChildCalculations(project);
		int removedProjects = deleteChildProjectsAndItself(project);
		// Reduce sibling projects order on database
		OrderService.reduceProjectSiblingsOrder(project.getElementOrder(), project.getParentPath());
		TreeEvent.sendEventToUserQueue(new Event("projectDeleted", null, project.getId()));
	}

	private static int deleteChildCalculations(Project project) {
		// Remove all calculations from the assetstore
		getDescendantCalculations(project.getPath()).forEach(calculation -> {
			try {
				AssetstoreService.deleteCalculation(calculation.getId());
			} catch (Exception e) {
				log.error("Error raised while removing calculation: " + calculation.toString());
			}
		});
		// Remove all project child calculations from the database
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaDelete<Calculation> deleteCriteria = criteriaBuilder.createCriteriaDelete(Calculation.class);
			Root<Calculation> root = deleteCriteria.from(Calculation.class);
			Predicate pathPredicate = criteriaBuilder.or(
					criteriaBuilder.equal(root.get("path"), project.getPath()),
					criteriaBuilder.like(root.get("path"), project.getPath() + "/%"));
			deleteCriteria.where(pathPredicate);
			Query query = session.createQuery(deleteCriteria);
			return query.executeUpdate();
		} catch (Exception e) {
			log.error("Error raised while removing child calculations from : " + project.getPath());
			log.error(e.getMessage());
		}
		return 0;
	}

	private static int deleteChildProjectsAndItself(Project project) {
		int totalRemoved = 0;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaDelete<Project> deleteCriteria = criteriaBuilder.createCriteriaDelete(Project.class);
			Root<Project> root = deleteCriteria.from(Project.class);
			Predicate pathPredicate = criteriaBuilder.or(
					criteriaBuilder.equal(root.get("path"), project.getPath()),
					criteriaBuilder.like(root.get("path"), project.getPath() + "/%"));
			deleteCriteria.where(pathPredicate);
			Query query = session.createQuery(deleteCriteria);
			totalRemoved = query.executeUpdate();
			session.delete(project);
			totalRemoved++;
			return totalRemoved;
		} catch (Exception e) {
			log.error("Error raised while removing child projects from : " + project.getPath());
			log.error(e.getMessage());
		}
		return 0;
	}

	public static void moveOverProject(Project source, Project destination)
			throws BrowseCredentialsException, Exception {
		update(source, destination.getPath());
	}

	public static void update(Project project) throws BrowseCredentialsException, Exception {
		update(project, project.getParentPath());
	}

	public static void update(Project project, String newParentPath) throws BrowseCredentialsException, Exception {
		Project modifiedProject = (Project) SerializationUtils.clone(project);

		String newPath = Paths.getFullPath(newParentPath, modifiedProject.getName());
		boolean pathUpdated = false;

		// Retrieve database project to compare changes
		Project originalProject = getById(modifiedProject.getId());
		String originalPath = originalProject.getPath();
		String originalName = originalProject.getName();

		if (ProjectService.getByPath(modifiedProject.getPath()) != null
				&& ProjectService.getByPath(modifiedProject.getPath()).getId() != modifiedProject.getId())
			throw new Exception(
					"A project with name " + modifiedProject.getName() + " already exists at the current path.");

		if (!isValidPath(newPath)
				|| !PermissionService.hasPermissionOnProject(modifiedProject.getParentPath(), Permissions.WRITE))
			throw new Exception("Can't move project, not a valid destination path");

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		modifiedProject.setParentPath(newParentPath);
		session.merge(modifiedProject);

		if (!modifiedProject.getName().equals(originalName) ||
				!newParentPath.equals(Paths.getParent(originalPath))) {
			String escapedPath = escape(originalPath);
			// Update child projects
			// update projects set path=regexp_replace(path,? ,?) where (path like ?) or
			// (path=?)"
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaUpdate<Project> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(Project.class);

			Root<Project> projRoot = criteriaUpdate.from(Project.class);
			Path<String> path = projRoot.get("path");

			criteriaUpdate.set(path,
					criteriaBuilder.function("regexp_replace", String.class, path,
							criteriaBuilder.parameter(String.class, "escapedPath"),
							criteriaBuilder.parameter(String.class, "newPath")));
			criteriaUpdate.where(criteriaBuilder.or(
					criteriaBuilder.like(path, criteriaBuilder.parameter(String.class, "originalPathLike")),
					criteriaBuilder.equal(path, criteriaBuilder.parameter(String.class, "originalPath"))));

			Query query = session.createQuery(criteriaUpdate);
			query.setParameter("escapedPath", "^" + escapedPath);
			query.setParameter("newPath", newPath);
			query.setParameter("originalPathLike", originalPath + "/%");
			query.setParameter("originalPath", originalPath);
			query.executeUpdate();

			// Update child calculations
			// update calculations set path=regexp_replace(path, ?, ?) where (path like ?)
			// or (path=?)
			criteriaBuilder = session.getCriteriaBuilder();
			CriteriaUpdate<Calculation> calculationUpdate = criteriaBuilder.createCriteriaUpdate(Calculation.class);

			Root<Calculation> calcRoot = calculationUpdate.from(Calculation.class);
			path = calcRoot.get("path");

			calculationUpdate.set(path,
					criteriaBuilder.function("regexp_replace", String.class, path,
							criteriaBuilder.parameter(String.class, "escapedPath"),
							criteriaBuilder.parameter(String.class, "newPath")));
			calculationUpdate.where(criteriaBuilder.or(
					criteriaBuilder.like(path, criteriaBuilder.parameter(String.class, "originalPathLike")),
					criteriaBuilder.equal(path, criteriaBuilder.parameter(String.class, "originalPath"))));
			query = session.createQuery(calculationUpdate);
			query.setParameter("escapedPath", "^" + escapedPath);
			query.setParameter("newPath", newPath);
			query.setParameter("originalPathLike", originalPath + "/%");
			query.setParameter("originalPath", originalPath);
			query.executeUpdate();

			pathUpdated = true;
		}
		// Send modification events to the rest of the UI components
		HashMap<String, Object> params = new HashMap<>();
		params.put("id", Long.toString(originalProject.getId()));
		params.put("oldPath", originalPath);
		params.put("newPath", newPath);
		TreeEvent.sendEventToUserQueue(new Event("projectModified", null, params));
		// Reorder element according to the new path
		if (pathUpdated)
			OrderService.moveProject(modifiedProject.getId(), Paths.getParent(originalPath), newParentPath);
	}

	private static boolean isValidPath(String destinationPath) throws BrowseCredentialsException {
		return destinationPath.startsWith(GeneralConstants.DB_ROOT + UserService.getUserName());
	}

	private static String escape(String inString) {
		StringBuilder builder = new StringBuilder(inString.length() * 2);
		String toBeEscaped = "\\{}()[]*+?.|^$";
		for (int i = 0; i < inString.length(); i++) {
			char c = inString.charAt(i);
			if (toBeEscaped.contains(Character.toString(c))) {
				builder.append('\\');
			}
			builder.append(c);
		}
		return builder.toString();
	}

	public static void publishProject(Long projectId, String publishedName, String handle) throws Exception {
		Project project = getById(projectId);
		if (project == null)
			throw new Exception("Project to be marked as published not found.");
		project.setPublished(true);
		project.setPublishedName(publishedName);
		project.setHandle(handle);
		project.setPublicationDate(new Timestamp(System.currentTimeMillis()));
		project.setState(Project.State.published);

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.saveOrUpdate(project);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	public static void cascadePermissionsToChildren(Project project) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaUpdate<Project> updateCriteria = builder.createCriteriaUpdate(Project.class);
			Root<Project> root = updateCriteria.from(Project.class);

			Predicate childPath = builder.equal(root.get("path"), project.getPath());
			Predicate siblingsPath = builder.like(root.get("path"), Paths.getFullPath(project.getPath(), "%"));

			updateCriteria
					.set(root.get("group"), project.getGroup())
					.set(root.get("permissions"), project.getPermissions())
					.where(builder.or(childPath, siblingsPath));

			Query<Project> query = session.createQuery(updateCriteria);
			query.executeUpdate();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
}

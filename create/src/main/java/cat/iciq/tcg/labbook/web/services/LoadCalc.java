/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.UploadService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.CalculationInsertion;

@MultipartConfig()
public class LoadCalc extends HibernateService {
	
	private static final long   serialVersionUID 	= 1L;
	private static final Logger logger = LogManager.getLogger(LoadCalc.class.getName());

    @Override 
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {      
        try {
            UserService.loadUserInfoFromRequest(req);                    
            executePostService(req, resp, buildParameterMap(req), UserService.getUserName(), UserService.getUserId());            
        } catch (Exception e) {
            logger.error(e.getMessage());
            returnSystemError(resp, "Server error", e.getMessage());
        } finally {        
            UserService.clearUserInfoFromRequest();
        }
    }
    
    public void executePostService(HttpServletRequest req, HttpServletResponse response, Map<String,String> params, String username, int userId)  {
		try {
            params.put(Constants.LOCAL_APP_DIR, FileService.getCreatePath() + File.separatorChar);
            params.put(CalculationInsertion.PARAM_USERNAME, UserService.getUserName());       
            params.put(CalculationInsertion.PARAM_USERID, String.valueOf(UserService.getUserId()));
            
		    String id = UploadService.loadCalculationsViaShell(req, params);
			response.getOutputStream().write(id.getBytes());
		} catch (Exception e) {
			logger.error(e.getMessage());
			returnKO(response, LOAD_ERROR, e.getMessage());
		}
    }

}

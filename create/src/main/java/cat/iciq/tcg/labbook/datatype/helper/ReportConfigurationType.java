package cat.iciq.tcg.labbook.datatype.helper;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.xml.sax.SAXException;

import cat.iciq.tcg.labbook.zk.composers.reportmanager.ReportConfiguration;

public class ReportConfigurationType implements UserType {

    @Override
    public int[] sqlTypes() {
        return new int[]{Types.VARCHAR};
    }
    
    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
            throws SQLException {
        String configurationJson = rs.getString(names[0]);
        try {
            return new ReportConfiguration(configurationJson);
        } catch (Exception e) {
            try {
                return new ReportConfiguration("");
            } catch (Exception e1) {
                return null;
            }
        }
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
            throws SQLException {
        if (value == null) {
            st.setNull(index, Types.VARCHAR);
        } else {
            String configurationJson = ((ReportConfiguration) value).toString();
            st.setString(index, configurationJson);
        }
    }

    @Override
    public Class<ReportConfiguration> returnedClass() {
        return ReportConfiguration.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y) {
            return true;
        }
        if (x == null || y == null) {
            return false;
        }
        return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if (value == null) {
            return null;
        }       
        try {
            return new ReportConfiguration(((ReportConfiguration) value).toString());
        } catch (SAXException | IOException e) {
            return null;    
        }
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}

package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ReportService;
import cat.iciq.tcg.labbook.datatype.services.ReportTypeService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.zk.composers.Main;
import cat.iciq.tcg.labbook.zk.composers.reportmanager.ReportConfiguration;

public class ReportController extends HibernateService {

    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try {

            if (params.get(PARAMETER_ID) == null || !params.get(PARAMETER_ID).matches("\\d+")) {
                new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, PROJECT_ERROR, BAD_REQUEST_ERROR)
                        .sendErrorResponse(response);
                return;
            }

            Report report = ReportService.getReport(Integer.parseInt(params.get(PARAMETER_ID)));
            if (report == null) {
                new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Not found", NOT_FOUND_ERROR)
                        .sendErrorResponse(response);
                return;
            }

            if (report.getOwnerId() != UserService.getUserId()) {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "Unauthorized", PERMISSION_ERROR)
                        .sendErrorResponse(response);
                return;
            }
            sendObj(response, serializeReport(report));
        } catch (NumberFormatException e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Invalid id", BAD_REQUEST_ERROR)
                    .sendErrorResponse(response);
        } catch (NullPointerException e) {
            new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Not found", NOT_FOUND_ERROR)
                    .sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Error raised retrieving report",
                    "Error raised while retrieving report.").sendErrorResponse(response);
        }
    }

    private ObjectNode serializeReport(Report report) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode reportJson = mapper.createObjectNode();
        ArrayList<Long> listIdCalculations = new ArrayList<>();
        List<ReportCalculation> calculations = report.getCalculations();
        for (ReportCalculation reportCalculation : calculations) {
            listIdCalculations.add(reportCalculation.getCalculation().getId());
        }
        reportJson.put("id", report.getId());
        reportJson.put("reportType", report.getType().getId());
        reportJson.put("title", report.getTitle());
        reportJson.put("name", report.getName());
        reportJson.put("description", report.getDescription());
        reportJson.put("creationDate", report.getCreationDate().toString());
        reportJson.put("calculationIds", listIdCalculations.toString());
        reportJson.put("configuration", report.getReportConfiguration().toString());
        return reportJson;
    }

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        if(params.size() == 0){
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST,"Invalid JSON","Invalid JSON.").sendErrorResponse(response);
            return;
        }
        try {    
            int reportTypeId = Integer.parseInt(params.get("reportType"));
            ReportType type = ReportTypeService.getReportTypeById(reportTypeId);
            if(type == null) {
                new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Invalid report type", "Provided report type is not valid");
                return;
            }

            Report report = new Report();
            report.setName(params.get("name"));
            report.setTitle(params.get("title"));
            report.setDescription(params.get("description"));
            report.setType(type);
            report.setCreationDate(new Date(System.currentTimeMillis()));
		    report.setOwnerId(UserService.getUserId());
            report.setReportConfiguration(getReportDefaultConfiguration(params.get("configuration")));
            assignCalculationsToReport(report);

            int id = ReportService.createReport(report);
            sendMsg(response, String.valueOf(id));
        } catch (IllegalArgumentException e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Invalid report type",
                    "Provided report type is not valid.").sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error raised storing report",
                    "Error raised while storing report.").sendErrorResponse(response);
        }
    }

    private void assignCalculationsToReport(Report report) throws Exception {
        int reactionGraphType = ReportTypeService.getReportTypeByName("Reaction Graph").getId();
        if(report.getType().getId() == reactionGraphType) {            
            report.setCalculations(setCalculationsReport(getCalculationsFromReactionGraph(report), report));
        }
    }

    @Override
    public void executePutService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        if(params.size() == 0){
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST,"Invalid JSON","Invalid JSON.").sendErrorResponse(response);
            return;
        }
        try {
            Report report = ReportService.getReport(Integer.parseInt(params.get(PARAMETER_ID)));

            if (report == null) {
                sendMsgError(response, NOT_FOUND_ERROR, 404);
                return;
            }
            if (report.getOwnerId() != UserService.getUserId()) {
                sendMsgError(response, PERMISSION_ERROR, 403);
                return;
            }

            if (params.get(PARAMETER_CONFIGURATION) != null && !params.get(PARAMETER_CONFIGURATION).isBlank()) {
                String configuration = params.get(PARAMETER_CONFIGURATION);
                if (!isConfigurationValid(configuration)) {
                    sendMsgError(response, "Provided report configuration is not valid.", 400);
                    return;
                }
                report.setReportConfiguration(configuration);
            }

            String name = ((params.get(PARAMETER_NAME) == null || params.get(PARAMETER_NAME).isBlank())
                    ? report.getName()
                    : truncate(Main.normalizeField(params.get(PARAMETER_NAME)), 64));
            String description = ((params.get(PARAMETER_DESCRIPTION) == null
                    || params.get(PARAMETER_DESCRIPTION).isBlank()) ? report.getName()
                            : truncate(params.get(PARAMETER_DESCRIPTION), 512));
            String title = ((params.get(PARAMETER_TITLE) == null || params.get(PARAMETER_TITLE).isBlank())
                    ? report.getName()
                    : truncate(params.get(PARAMETER_TITLE), 256));

            report.setName(name);
            report.setDescription(description);
            report.setTitle(title);

            ReportService.updateReport(report);
            sendMsg(response, String.valueOf(report.getId()));
        } catch (NumberFormatException e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Invalid id", BAD_REQUEST_ERROR)
                    .sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error raised updating report",
                    REPO_MOD_ERROR).sendErrorResponse(response);
        }
    }

    @Override
    public void executeDeleteService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try {
            if (params.get(PARAMETER_ID) == null || !params.get(PARAMETER_ID).matches("\\d+")) {
                new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, PROJECT_ERROR, BAD_REQUEST_ERROR)
                        .sendErrorResponse(response);
                return;
            }
            if (ReportService.getReport(Integer.parseInt(params.get(PARAMETER_ID))) == null) {
                new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, "Not found", NOT_FOUND_ERROR)
                        .sendErrorResponse(response);
                return;
            }
            if (ReportService.getReport(Integer.parseInt(params.get(PARAMETER_ID))).getOwnerId() != UserService
                    .getUserId()) {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "Unauthorized", PERMISSION_ERROR)
                        .sendErrorResponse(response);
                return;
            }
            ReportService.deleteReport(Integer.parseInt(params.get(PARAMETER_ID)));
            returnOK(response);
        } catch (NumberFormatException e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, "Id incorrect", BAD_REQUEST_ERROR)
                    .sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, REPORT_ERROR, REPORT_ERROR);
        }
    }

    public static List<ReportCalculation> setCalculationsReport(List<String> listCalculations, Report report)
            throws SecurityException {
        ArrayList<ReportCalculation> reportCalculations = new ArrayList<>();
        int order = 1;
        for (String calcId : listCalculations) {
            Calculation calc = CalculationService.getById(Long.parseLong(calcId));
            if (calc == null || !PermissionService.hasPermissionOnCalculation(calc, Permissions.READ)) {
                throw new SecurityException("You don't have permission to access this calculation.");
            }
            ReportCalculation rp = new ReportCalculation();
            rp.setTitle("Calculation " + calc.getName());
            rp.setReport(report);
            rp.setCalculation(calc);
            rp.setCalcOrder(order);
            reportCalculations.add(rp);
            order++;
        }
        return reportCalculations;
    }

    private String getReportDefaultConfiguration(String configuration) throws Exception {
        if (!configuration.isBlank() && isConfigurationValid(configuration))
            return configuration;
        return "";
    }

    private boolean isConfigurationValid(String configuration) {
        ReportConfiguration config;
        try {
            config = new ReportConfiguration(configuration.replace("\t", "\t").replace("\n", "\n"));
            return config.isValid();
        } catch (SAXException | IOException e) {
            return false;
        }
    }

    public static List<JsonNode> findKeys(JsonNode node, String keyName) {
        List<JsonNode> matchingNodes = new ArrayList<>();
        if (node.isObject()) {
            node.fields().forEachRemaining(entry -> {
                if (entry.getKey().equals(keyName)) {
                    matchingNodes.add(entry.getValue());
                }
                matchingNodes.addAll(findKeys(entry.getValue(), keyName));
            });
        } else if (node.isArray()) {
            node.forEach(element -> matchingNodes.addAll(findKeys(element, keyName)));
        }
        return matchingNodes;
    }

    private ArrayList<String> getCalculationsFromReactionGraph(Report report) throws SAXException, IOException {
        Node node = report.getReportConfiguration().query("/configuration/parameters/graph").item(0);
        String jsonGraph = node.getTextContent().trim();        
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(jsonGraph);
        List<JsonNode> nodes = findKeys(rootNode, "cid_formula");
        HashSet<String> calculationIds = new HashSet<>();
        nodes.forEach((JsonNode cids) -> {
            String[] ids = cids.toString().trim().split("\s*[+-]\s*");
            for (String id : ids) {
                id = id.replaceAll("\"", "");
                if (!id.isBlank()){
                    calculationIds.add(id.trim());
                }
            }
        });
        return new ArrayList<>(calculationIds);
    }
}
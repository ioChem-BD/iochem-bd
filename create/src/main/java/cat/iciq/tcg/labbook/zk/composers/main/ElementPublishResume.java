/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.main;

import java.util.ArrayList;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Window;

import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.TokenManager;
import cat.iciq.tcg.labbook.zk.components.publishtree.Element;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class ElementPublishResume extends SelectorComposer<Window> {
		
	@Wire Window elementPublishResume;
	@Wire Label totalProjectsLbl;
	@Wire Label totalCalculationsLbl;
	@Wire Label totalDOIsLbl;

	@Wire Grid publicationEditLinksGrid;
	@Wire Grid publicationReviewLinksGrid;

	@Listen("onClick=#closeBtn")
	public void onCloseBtnClick(){
		elementPublishResume.detach();
	}
	
	
	public void setStatistics(int projectCount, int calculationCount, int doiRequestCount){
		totalProjectsLbl.setValue(String.valueOf(projectCount));
		totalCalculationsLbl.setValue(String.valueOf(calculationCount));
		totalDOIsLbl.setValue(String.valueOf(doiRequestCount));		
	}
	
	public void setPublishedElements(ArrayList<Element> elements){		
		Rows editRows = (Rows)publicationEditLinksGrid.query("rows");
		Rows reviewRows = (Rows)publicationReviewLinksGrid.query("rows");
		for(Element element : elements){
			editRows.appendChild(buildEditRow(element));
			reviewRows.appendChild(buildReviewRow(element));			
		}		
	}	
	
	private Row buildEditRow(Element element){
		Row row = new Row();
		row.setZclass("none");
		Cell nameCell = new Cell();		
		Label nameLbl = new Label(element.getName());
		nameLbl.setMaxlength(40);
		nameCell.appendChild(nameLbl);
		
		Cell aCell = new Cell();
		String href = buildBrowseEditLink(element);
		A editA = new A(href);
		editA.setHref(href);
		editA.setTarget("_blank");
		aCell.appendChild(editA);
		
		row.appendChild(nameCell);
		row.appendChild(aCell);
		return row;		
	}
	
	private Row buildReviewRow(Element element){
		Row row = new Row();
		row.setZclass("none");
		Cell nameCell = new Cell();
		Label nameLbl = new Label(element.getName());
		nameLbl.setMaxlength(40);		
		nameCell.appendChild(nameLbl);	
		
		Cell aCell = new Cell();
		String href = buildBrowseReviewLink(element);
		A editA = new A(href);
		editA.setHref(href);
		editA.setTarget("_blank");
		aCell.appendChild(editA);
		
		row.appendChild(nameCell);
		row.appendChild(aCell);
		return row;		
	}	
	
	
	private String buildBrowseEditLink(Element element){
		String salt = CustomProperties.getProperty("module.communication.secret");
		String secret = Main.getBrowseBaseUrl() + "#"+ element.getHandle() + "#" + "edit-collection";
		
		String calculatedToken = TokenManager.encode(salt, secret);
		calculatedToken = calculatedToken.substring(calculatedToken.length() -24 ,calculatedToken.length());	//Kept last 24 characters, enough to be safe
		StringBuilder sb = new StringBuilder();
		sb.append(Main.getBrowseBaseUrl());
		sb.append(Constants.BROWSE_HANDLE_EDIT_ENDPOINT);
		sb.append("/" + element.getHandle());
		sb.append("/" + calculatedToken);		
		return sb.toString();
	}
	
	private String buildBrowseReviewLink(Element element){
		String salt = CustomProperties.getProperty("module.communication.secret");
		String secret = Main.getBrowseBaseUrl() + "#" + element.getHandle() + "#" + "review-collection";
		String calculatedToken = TokenManager.encode(salt, secret);
		calculatedToken = calculatedToken.substring(calculatedToken.length() -24 ,calculatedToken.length());	//Kept last 24 characters, enough to be safe
		StringBuilder sb = new StringBuilder();
		sb.append(Main.getBrowseBaseUrl());
		sb.append(Constants.BROWSE_HANDLE_REVIEW_ENDPOINT);
		sb.append("/" + element.getHandle());		
		sb.append("/" + calculatedToken);		
		return sb.toString();
	}
}

package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class ProjectHierarchyController extends HibernateService {
    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {

        Project project = ProjectService.getById(Long.parseLong(params.get(PARAMETER_ID)));
        String type = params.get("type");
        String depth = params.get("depth");

        try{
            if (project == null) {
                returnKO(response, "Not found", "The requested element is not accessible.");
                return;
            }
            if(!PermissionService.hasPermissionOnProject(project, Permissions.READ) || !PermissionService.hasPermissionOnAllDescendants(project, Permissions.READ)){
                returnKO(response, "Not enough permissions", "You can't read the entire hierarchy of the current project.");
                return;
            }

            if (type == null || type.equalsIgnoreCase("calculation")) {
                List<EntityDTO> calcs = new ArrayList<>();
                if(depth == null || depth.equalsIgnoreCase("1")){
                    for (Calculation c : ProjectService.getChildCalculations(project.getPath()))                       
                        calcs.add(HibernateService.fromCalculation(c, project.getLinuxPermissions(), String.valueOf(project.getOwner()), String.valueOf(project.getGroup()), project.getConceptGroup()));                    
                    sendObj(response, calcs);
                }else if(depth.equalsIgnoreCase("all")){                    
                    for (Calculation c : ProjectService.getDescendantCalculations(project.getPath()))                       
                        calcs.add(HibernateService.fromCalculation(c, project.getLinuxPermissions(), String.valueOf(project.getOwner()), String.valueOf(project.getGroup()), project.getConceptGroup()));                    
                    sendObj(response, calcs);            
                } else{
                    int depthInt = 1;
                    try{
                        depthInt = Integer.parseInt(depth);
                    }catch(NumberFormatException e){                        
                    }
                    for (Calculation c : ProjectService.getDescendantCalculations(project.getPath())) {
                        if (Paths.pathDepth(project.getPath(), c.getPath()) <= depthInt) {
                            calcs.add(HibernateService.fromCalculation(c, project.getLinuxPermissions(), String.valueOf(project.getOwner()), String.valueOf(project.getGroup()), project.getConceptGroup()));                    
                        }
                    }
                    sendObj(response, calcs);
                }              
            } else if (type.equalsIgnoreCase("project")) {                
              
                if(depth == null || depth.equalsIgnoreCase("1")){
                    sendObj(response, ProjectService.getChildProjects(project.getPath(), false));
                } else if (depth.equalsIgnoreCase("all")) {
                    sendObj(response, ProjectService.getDescendantProjects(project.getPath()));
                } else {
                    int depthInt = 1;
                    try{
                        depthInt = Integer.parseInt(depth);
                    }catch(NumberFormatException e){

                    }
                    List<Project> projects = new ArrayList<>();
                      for (Project p : ProjectService.getDescendantProjects(project.getPath())) {
                        if (Paths.pathDepth(project.getPath(), p.getPath()) <= depthInt) {
                            projects.add(p);                    
                        }
                    }
                    sendObj(response, projects);                    
                }
            } else {
                returnKO(response, "Bad request", "Invalid type.");        
            }
            
    }catch(Exception e){
        returnKO(response, "Permissions Error", "Your user is not allowed to read the requested path.");
    }
    }

}

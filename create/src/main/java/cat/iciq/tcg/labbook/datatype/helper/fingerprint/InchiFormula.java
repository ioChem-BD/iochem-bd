package cat.iciq.tcg.labbook.datatype.helper.fingerprint;

public class InchiFormula {
    
    private String inchi;
    private String auxInfo;
    private String inchiKey;    

    public InchiFormula(String inchi, String auxInfo, String inchiKey) {
        this.inchi = inchi;
        this.auxInfo = auxInfo;
        this.inchiKey = inchiKey;
    }

    public String getInchi() {
        return inchi;
    }

    public String getAuxInfo() {
        return auxInfo;
    }

    public String getInchiKey() {
        return inchiKey;
    }
}

package cat.iciq.tcg.labbook.web.controllers;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.*;
import com.nimbusds.jwt.*;

import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.web.cas.FilterConfigFactory;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

public class ApiJWT {
    private static final long ONE_DAY_MILLIS = 24 * 60 * 60 * 1000L;

    private static final String CAS_ISSUER_URL =  FilterConfigFactory.getCasLoginUrl();

    public static String createJWT(String sharedSecret, String subject, String path, String email, String userId, String fullname, String mainGroup, String groupId) throws JOSEException {
        JWSSigner signer = new MACSigner(sharedSecret);
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(subject)
                .issuer(CAS_ISSUER_URL)
                .expirationTime(new Date(new Date().getTime() + ONE_DAY_MILLIS)) 
                .claim(UserService.CAS_USER_PATH, path)
                .claim(UserService.CAS_USER_EMAIL, email)
                .claim(UserService.CAS_USER_ID, userId)
                .claim(UserService.CAS_USER_FULL_NAME, fullname)
                .claim(UserService.CAS_USER_ASSOCIATED_GROUPS_ID, groupId)
                .claim(UserService.CAS_USER_MAIN_GROUP_ID, mainGroup)
                .build();
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS512), claimsSet);
        signedJWT.sign(signer);
        return signedJWT.serialize();
    }

    public static boolean verifyJWT(String token, String sharedSecret) throws JOSEException, ParseException, IllegalStateException {
        SignedJWT signedJWT = SignedJWT.parse(token);
        JWSVerifier verifier = new MACVerifier(sharedSecret);
        return signedJWT.verify(verifier);
    }

    public static HashMap<String, Object> extractDataJWT(String token, String sharedSecret) throws ParseException{
        SignedJWT signedJWT = SignedJWT.parse(token);
        signedJWT.getPayload().toString();
        JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();

        HashMap<String, Object> attr = new HashMap<>();
        attr.put(UserService.CAS_USER_PATH, claimsSet.getStringClaim(UserService.CAS_USER_PATH));
        attr.put(UserService.CAS_USER_EMAIL , claimsSet.getStringClaim(UserService.CAS_USER_EMAIL));
        attr.put(UserService.CAS_USER_ID, claimsSet.getStringClaim(UserService.CAS_USER_ID));
        attr.put(UserService.CAS_USER_FULL_NAME, claimsSet.getStringClaim(UserService.CAS_USER_FULL_NAME));
        attr.put(UserService.CAS_USER_MAIN_GROUP_ID, claimsSet.getStringClaim(UserService.CAS_USER_MAIN_GROUP_ID));
        attr.put(UserService.CAS_USER_ASSOCIATED_GROUPS_ID, claimsSet.getStringClaim(UserService.CAS_USER_ASSOCIATED_GROUPS_ID));
        return attr;
    }
}
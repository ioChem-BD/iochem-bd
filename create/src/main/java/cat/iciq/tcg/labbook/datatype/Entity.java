/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

import org.hibernate.search.annotations.Field;

import cat.iciq.tcg.labbook.shell.utils.Paths;

@MappedSuperclass
public abstract class Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "path", nullable = false)
    private String path;
    
    @Column(name = "name", nullable = false)
    @Field    
	private String name;
    
    @Column(name = "description", nullable = false)
    @Field
	private String description;
    
    @Column(name = "element_order", nullable = false)
	private int elementOrder;
    
    @Column(name = "creation_time")
	private Timestamp creationDate;

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentPath() {
		return path;
	}

	public void setParentPath(String path) {
		this.path = path;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getElementOrder() {
		return elementOrder;
	}

	public void setElementOrder(int elementOrder) {
		this.elementOrder = elementOrder;
	}

	public boolean isProject() {
		return this instanceof Project;
	}
	
	public boolean isCalculation() {
		return this instanceof Calculation;
	}

	public String getPath() {
		return Paths.getFullPath(this.getParentPath(), this.getName());
	}

    public long getId() {
        if(isProject())
            return ((Project)this).getId();
        else
            return ((Calculation)this).getId();        
    }
    
    public void setId(long id) {
        if(isProject())
            ((Project)this).setId(id);
        else
            ((Calculation)this).setId(id);        
    }   
}

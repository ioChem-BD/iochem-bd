package cat.iciq.tcg.labbook.zk.ui;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.openscience.cdk.exception.CDKException;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.MolecularFingerprint;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FingerprintService;

public class FingerprintDaemon implements Runnable{

    private static final Logger log = LogManager.getLogger(FingerprintDaemon.class.getName());
    
    public enum STATUS {ERROR, INDEXING, OK};
    public static final String FP_DAEMON_STATUS = "fingerprintDaemonStatus";
    public static final String FP_DAEMON_ATTRIBUTE = "fingerprintDaemonMessage";

    private static final String INCHI_ERROR = "¹A valid InChI library could not be detected.\n Please contact the platform administrator to solve this issue.";
    private static final String INDEXING_MESSAGE = "¹The indexing of molecules is ongoing.\n The last uploaded results may not yet be displayed.";

    @Override
    public void run() {       
        try {
            addMolecularFingerprints();
        } catch (CDKException e) {
            setStatus(STATUS.ERROR, INCHI_ERROR);
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error("FingerprintDaemon: Error raised trying to index molecules.");
        }
    }
    
    private void addMolecularFingerprints() throws Exception {
        int pageSize = 500;
        int pageNumber = 1;
        int batchSize = 50;
        while (true) {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();
                List<Long> ids = CalculationService.getCalculationsWithoutFingerprint(pageSize, pageNumber);
                if (ids.isEmpty()) {
                    transaction.commit();
                    break;
                }
                setStatus(STATUS.INDEXING, INDEXING_MESSAGE);
                for (int i = 0 ; i< ids.size(); i++) {
                    if(i > 0 && i % batchSize == 0) {
                        session.flush();
                        session.clear();
                    }
                    MolecularFingerprint mf = FingerprintService.getMolecularFingerprint(ids.get(i));                    
                    session.saveOrUpdate(mf);
                }               
                transaction.commit();                
                pageNumber++;
            } catch (Exception e) {
                log.error("An error raised adding molecular fingerprints.");
                if (transaction != null)
                    transaction.rollback();
                throw e;
            }
        }
        setStatus(STATUS.OK, "");        
    }
    
    public static void setStatus(STATUS status, String message) {
        WebApps.getCurrent().setAttribute(FP_DAEMON_STATUS, status);
        WebApps.getCurrent().setAttribute(FP_DAEMON_ATTRIBUTE, message);
    }
}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;

public 	class DummyServletOutputStream extends ServletOutputStream{
	ByteArrayOutputStream inner = null;

	public DummyServletOutputStream(){
		inner = new ByteArrayOutputStream ();
	}
	
	public byte[] toByteArray(){
		return inner.toByteArray();
	}
	
	@Override
	public void write(byte[] b) throws IOException {
		inner.write(b);
	}
	
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		inner.write(b, off, len);
	}

	@Override
	public void write(int b) throws IOException {
		inner.write(b);		
	}	

	@Override
	public void flush() throws IOException {
		inner.flush();
	}

	@Override
	public void close() throws IOException {
		inner.close();
	}
	
	@Override
	public void print(String s) throws IOException {}

	@Override
	public void print(boolean b) throws IOException {}

	@Override
	public void print(char c) throws IOException {}

	@Override
	public void print(int i) throws IOException {}

	@Override
	public void print(long l) throws IOException {}

	@Override
	public void print(float f) throws IOException {}

	@Override
	public void print(double d) throws IOException {}

	@Override
	public void println() throws IOException {}

	@Override
	public void println(String s) throws IOException {}

	@Override
	public void println(boolean b) throws IOException {}

	@Override
	public void println(char c) throws IOException {}

	@Override
	public void println(int i) throws IOException {}

	@Override
	public void println(long l) throws IOException {}

	@Override
	public void println(float f) throws IOException {}

	@Override
	public void println(double d) throws IOException {}
	
}
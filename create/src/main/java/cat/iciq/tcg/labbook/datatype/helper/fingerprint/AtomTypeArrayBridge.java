package cat.iciq.tcg.labbook.datatype.helper.fingerprint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.hibernate.search.bridge.LuceneOptions;
import org.hibernate.search.bridge.StringBridge;
import org.hibernate.search.bridge.TwoWayFieldBridge;

// Define the custom field bridge for the feature field
public class AtomTypeArrayBridge implements TwoWayFieldBridge, StringBridge {

    @Override
    public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
        String[] values = (String[]) value;
        if (values != null) {
            for (String val : values) {
                luceneOptions.addFieldToDocument(name, val, document);
            }
        }
    }

    @Override
    public Object get(String name, Document document) {
        List<String> values = new ArrayList<>();
        for (IndexableField field : document.getFields(name)) {
            values.add(field.stringValue());
        }
        return values.toArray(new String[values.size()]);
    }

    @Override
    public String objectToString(Object object) {
        if (object instanceof String)
            return (String) object;
        else
            return Arrays.toString((String[]) object);
    }

    public Object stringToObject(String stringValue) {
        if (stringValue == null || stringValue.isEmpty()) {
            return null;
        }
        return stringValue.substring(1, stringValue.length() - 1).split(", ");
    }

}
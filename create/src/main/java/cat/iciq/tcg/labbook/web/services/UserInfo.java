package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pac4j.cas.profile.CasProfile;

public class UserInfo extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(UserInfo.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        // Assuming "pac4jUserProfiles" is correctly named and contains the profiles
        Object profilesObj = session.getAttribute("pac4jUserProfiles");
        try(OutputStream ostream = response.getOutputStream()){
            if (profilesObj instanceof LinkedHashMap) {
                Map<String, Object> profiles = (LinkedHashMap<String, Object>) profilesObj;
                CasProfile casProfile = (CasProfile) profiles.get("CasClient");
                response.setContentType("application/json");
                String userInfoJson = mapToJson(casProfile.getAttributes());
                ostream.write(userInfoJson.getBytes(StandardCharsets.UTF_8));                
                response.setStatus(HttpServletResponse.SC_OK);
                return;
            }
        }catch(Exception e){
            logger.error(e.getMessage());
        }
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.setContentType("text/plain");
        response.getWriter().write("Could not retrieve user information.");
    }

    private String mapToJson(Map<String, Object> map) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(map);
        } catch (Exception e) {
            throw e;
        }
    } 
}
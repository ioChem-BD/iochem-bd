/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class ViewCalc extends HibernateService {

	private static final long serialVersionUID = 1L;

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName, Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
        String path = params.get(PARAMETER_NEWPATH);
        String fullView = params.get(PARAMETER_FULL);
    
        Calculation c = CalculationService.getByPath(path);
        if(c == null) {
            returnKO(response, CALCULATION_ERROR, BAD_PATH_ERROR);
            return;
        } else if(!PermissionService.hasPermissionOnCalculation(c.getId(), Permissions.READ)){
            returnKO(response, CALCULATION_ERROR, CALC_READ_ERROR);
            return;
        }
        
        Project p = ProjectService.getByPath(c.getParentPath());
         
        String permissions = p.getLinuxPermissions();
        String owner = UserService.getUserNameById(p.getOwner());
        String group = UserService.getGroupNameById(p.getGroup());
        String conceptGroup = p.getConceptGroup();        
        EntityDTO entity = fromCalculation(c, permissions, owner, group, conceptGroup);
                
        if (fullView != null) {
            List<String> filePaths = new ArrayList<>();
            List<CalculationFile> files = CalculationService.getCalculationFiles(c.getId());
            for(CalculationFile f : files) {
                filePaths.add("assetstore" + File.separatorChar + c.getId()+ File.separatorChar + f.getName());
            }
            entity.setExtraData(filePaths);
        }       
        sendObj(response, entity);
    }
}

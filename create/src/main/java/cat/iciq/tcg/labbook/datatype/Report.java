/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cat.iciq.tcg.labbook.zk.composers.reportmanager.ReportConfiguration;

@Entity
@Table(name="reports", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
public class Report implements Serializable{
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "reports_id_seq")
    @SequenceGenerator(name = "reports_id_seq", sequenceName = "reports_id_seq", allocationSize = 1)
    private int id;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "title", nullable = false)
    private String title;
    
    @Column(name = "description", nullable = false)
    private String description;
    
    @Column(name = "creation_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @Column(name = "owner_id", nullable = false)
    private int ownerId;
    
    @Column(name = "published", nullable = false)
    private boolean published;
    
    @ManyToOne
    @JoinColumn(name = "type", foreignKey = @ForeignKey(name = "report_type_id_fk"), nullable = false)
    private ReportType type;
    
    @OneToMany(mappedBy = "report", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @OrderBy("calcOrder")
    private List<ReportCalculation> calculations = new ArrayList<>();    
    
    @Column(name = "configuration")
    @Type(type = "cat.iciq.tcg.labbook.datatype.helper.ReportConfigurationType")
    private ReportConfiguration configuration;
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    
    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public void setReportConfiguration(String xml) throws SAXException, IOException{
        this.configuration = new ReportConfiguration(xml);      
    }

    public ReportConfiguration getReportConfiguration(){
        return this.configuration;
    }

    public NodeList queryConfiguration(String xpath) throws SAXException, IOException{
        return this.configuration.query(xpath);
    }

    public List<ReportCalculation> getCalculations() {        
        return calculations;
    }
    
    public void setCalculations(List<ReportCalculation> calculations) {
        this.calculations = calculations;
    }   

    public ReportCalculation getReportCalculationByCalculationId(long calculationId){
        for(ReportCalculation reportCalculation : this.getCalculations())
            if(reportCalculation.getId() == calculationId)
                return reportCalculation;
        return null;
    }
   
    public boolean containsCalculation(Long id){
        if(calculations == null || calculations.isEmpty())     
            return false;
        for(ReportCalculation calculation : calculations)
            if(calculation.getCalculation().getId() == id)
                return true;
        return false;
    }
        
    public void reorderCalculations(){
        Collections.sort(calculations, new Comparator<ReportCalculation>(){
            @Override
            public int compare(ReportCalculation o1, ReportCalculation o2) {
                return o1.getCalcOrder() - o2.getCalcOrder();
            }           
        });
        
    }   
    
    public boolean hasNoConfiguration() {
        return configuration == null || configuration.toString() == null || configuration.toString().isBlank();
    }

    public boolean isConfigurationValid() {
        return configuration.isValid();
    }

}

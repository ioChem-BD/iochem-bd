/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import jumbo2.util.Constants;
import jumbo2.util.UniversalNamespaceCache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IChemModel;
import org.openscience.cdk.io.CMLReader;
import org.openscience.cdk.silent.ChemFile;
import org.openscience.cdk.tools.manipulator.ChemFileManipulator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cat.iciq.tcg.labbook.datatype.helper.fingerprint.InchiFormula;
import cat.iciq.tcg.labbook.datatype.services.FingerprintService;

public class InchiGenerator {
	    
	public static final String CONVENTION = "iupac:inchi";
	private static final Logger log = LogManager.getLogger(InchiGenerator.class.getName());
	private static DocumentBuilderFactory dbFactory = null;
	private static XPathFactory xpFactory = null;
	private static DocumentBuilder dBuilder = null;	
	private XPath xPath;
	
	static{		
		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setNamespaceAware(true);
			dbFactory.setXIncludeAware(true);
			dBuilder = dbFactory.newDocumentBuilder();
			xpFactory = new net.sf.saxon.xpath.XPathFactoryImpl();
		} catch (ParserConfigurationException e) {
			dbFactory = null;
			xpFactory = null;
			dBuilder = null;
			log.error(e.getMessage());
		}							
	}
	
	public void addInchi(File outputFile){		 
		if(dbFactory == null)
			return;
		try {
			Document doc = dBuilder.parse(outputFile);			
			xPath = xpFactory.newXPath();
			xPath.setNamespaceContext(new UniversalNamespaceCache(doc, true));
			
			if(!hasInchi(doc)){
				NodeList moleculeNodes = (NodeList)xPath.evaluate(".//*:molecule", doc, XPathConstants.NODESET);
				for(int inx = 0; inx < moleculeNodes.getLength(); inx++){					
					Node moleculeNode = moleculeNodes.item(inx);					
					Node inchi =  buildInchiNode(outputFile.getAbsolutePath(), moleculeNode);
					moleculeNode.appendChild(inchi);					
				}				
				writeOutputFile(outputFile, doc);	
			}
		} catch (Exception e) {
			log.error(e.getMessage());			
		}		
	}

	private Node buildInchiNode(String path, Node moleculeNode) {
		IAtomContainer container = null;
		String molecule = convertDocumentToString(createDocumentFromNode(moleculeNode));
		try (CMLReader reader = new CMLReader(new ByteArrayInputStream(molecule.getBytes(StandardCharsets.UTF_8)))) {
            ChemFile chemfile = reader.read(new ChemFile());
            IChemModel model = ChemFileManipulator.getAllChemModels(chemfile).get(0);
            if (ChemFileManipulator.getAllChemModels(chemfile).isEmpty())
                return null;

            if (model.getCrystal() != null){
                container = model.getCrystal();
                container.setProperty("isCrystal", true);
            }
            else{
                container = ChemFileManipulator.getAllAtomContainers(chemfile).get(0);
                container.setProperty("isCrystal", false);
            }
        	
			InchiFormula inchiFormula = FingerprintService.getInchi(container);				
			return createInchiNode(moleculeNode.getOwnerDocument(), inchiFormula.getInchi(), inchiFormula.getAuxInfo());
        } catch (Exception e) {		
			log.error("Exception raised while generating INCHI formula for calculation file: " + path);			
            log.error(e.getMessage());
        }   
		return null;     
	}
	
	public Document createDocumentFromNode(Node node) {
		if(node == null)
			return null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document newDocument = builder.newDocument();
			Node importedNode = newDocument.importNode(node, true);
			newDocument.appendChild(importedNode);	
			return newDocument;
		} catch (ParserConfigurationException e) {
			log.error(e.getMessage());
			return null;
		}		
	}

	public String convertDocumentToString(Document doc) {
		if(doc == null)
			return null;

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			return writer.getBuffer().toString();
		} catch (TransformerException e) {
			log.error(e.getMessage());
			return null;
		}	
	}
	
	private Node createInchiNode(Document doc, String inchiStr, String auxInfoStr) {
		Element formula = doc.createElementNS(Constants.cmlNamespace, "formula");
		Element auxInfo = doc.createElementNS(Constants.cmlNamespace, "scalar");
		formula.setAttribute("convention", CONVENTION);

		formula.setAttribute("inline", inchiStr);
		auxInfo.setAttribute("id", "auxInfo");
		auxInfo.setAttribute("dataType", "xsd:integer");
		auxInfo.setTextContent(auxInfoStr);
		formula.appendChild(auxInfo);
		return formula;
	}

	
	private boolean hasInchi(Document doc) throws XPathExpressionException{
		String hasInchi ="count(//*:molecule/*:formula[@convention='iupac:inchi']) > 0";
		return (Boolean)xPath.evaluate(hasInchi, doc, XPathConstants.BOOLEAN);
	}
	
	private void writeOutputFile(File file, Document doc){
		try {
			StreamResult result = new StreamResult(file);
		    // Use a Transformer for output
		    TransformerFactory tFactory = TransformerFactory.newInstance();
		    Transformer transformer;
			transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    DOMSource source = new DOMSource(doc);		  
		    transformer.transform(source, result);	
		} catch (TransformerConfigurationException e) {		
			log.error(e.getMessage());
		} catch (TransformerException e) {	
			log.error(e.getMessage());
		}		
	}
}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cat.iciq.tcg.labbook.datatype.CalculationType;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class CalculationTypeService {
    
    private static final Logger log = LogManager.getLogger(CalculationTypeService.class);
    
    private static HashMap<Integer, CalculationType> calculationTypeById;
    private static HashMap<String, CalculationType> calculationTypeByName;
    
    static {
        calculationTypeById = new HashMap<>();
        calculationTypeByName = new HashMap<>();      
        loadCalculationTypes();
    }
    
    private static void loadCalculationTypes() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CalculationType> criteriaQuery = builder.createQuery(CalculationType.class);
            Root<CalculationType> root = criteriaQuery.from(CalculationType.class);
            criteriaQuery.select(root);

            List<CalculationType> results = session.createQuery(criteriaQuery).getResultList();
            for(CalculationType type : results) {
                calculationTypeById.put(type.getId(), type);
                calculationTypeByName.put(type.getName(), type);   
            }
        } catch (Exception e) {
            log.error("Exception raised loading calculation types.");
            log.error(e.getMessage());
        }                    
    }
    
    public static CalculationType getCalculationTypeByName(String name) throws SQLException{
        if(name == null || name.equals(""))
            return null;
        if(calculationTypeByName.containsKey(name))
            return calculationTypeByName.get(name);        
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();        
        //Not cached. retrieve it from database        
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CalculationType> criteriaQuery = builder.createQuery(CalculationType.class);
            Root<CalculationType> root = criteriaQuery.from(CalculationType.class);

            criteriaQuery.select(root).where(builder.equal(root.get("name"), name));
            CalculationType type = session.createQuery(criteriaQuery).getSingleResult();
            //Load cache of calculationTypes
            calculationTypeByName.put(type.getName(), type);
            calculationTypeById.put(type.getId(), type);
            return type;
        } catch (Exception e) {
            throw new SQLException(e);          
        }               
    }

    public static CalculationType getCalculationTypeById(int id) throws SQLException {
        if(calculationTypeById.containsKey(id))
            return calculationTypeById.get(id);     
        //Not cached. retrieve it from database
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();        
        //Not cached. retrieve it from database        
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CalculationType> criteriaQuery = builder.createQuery(CalculationType.class);
            Root<CalculationType> root = criteriaQuery.from(CalculationType.class);

            criteriaQuery.select(root).where(builder.equal(root.get("id"), id));
            CalculationType type = session.createQuery(criteriaQuery).getSingleResult();
            //Load cache of calculationTypes
            calculationTypeByName.put(type.getName(), type);
            calculationTypeById.put(type.getId(), type);
            return type;
        } catch (Exception e) {
            throw new SQLException(e);          
        }
    }

    public static List<CalculationType> getAll() {
        List<CalculationType> calculationTypes = new ArrayList<CalculationType>(calculationTypeById.values()); 
        calculationTypes.sort(new CalculationTypeSorter());
        return calculationTypes;
    }
}

class CalculationTypeSorter implements Comparator<CalculationType> {

    @Override
    public int compare(CalculationType o1, CalculationType o2) {
        return o1.getName().compareTo(o2.getName());
    }
}

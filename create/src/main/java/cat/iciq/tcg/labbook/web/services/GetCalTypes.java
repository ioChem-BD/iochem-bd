/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.CalculationTypeFile;
import cat.iciq.tcg.labbook.datatype.services.CalculationTypeFileService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class GetCalTypes extends HibernateService {
    
    private static final long serialVersionUID = 1L;

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {

        List<CalculationTypeFile> calcTypes = CalculationTypeFileService.getCalcTypeFiles();
        sendObj(response, CalculationTypeFile.convertListToDTO(calcTypes));
    }
}

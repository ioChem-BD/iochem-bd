/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class ToXYZ extends HibernateService {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(ToXYZ.class.getName());
	
	private static final String XSLT_PATH 		= "html/xslt/cml2xyz.xsl";
	private static final String XYZ_MIMETYPE 	= "chemical/x-xyz";
	private static final String XYZ_FILENAME	= "data.xyz";
	private static TransformerFactory tFactory 	= null; 
	private static Templates template			= null;
	
	static {
		tFactory	=  new net.sf.saxon.TransformerFactoryImpl();
	}

	public static void flushXsltTemplates(){
		template = null;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Session session = null;
	    Transaction transaction = null;
    	loadXsltTemplate();
    	String mode = request.getParameter(PARAMETER_MODE);
		try{
			UserService.loadUserInfoFromRequest(request);
		    session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
		    OutputStream ostream = response.getOutputStream();
			int calculationId = Integer.parseInt(request.getParameter(PARAMETER_ID));
			if(PermissionService.hasPermissionOnCalculation(calculationId, Permissions.READ)){
				String xyzFileContent = getXyzFileContent(ostream, calculationId, mode);						
				String mimetype = XYZ_MIMETYPE;		    
				response.setContentType(mimetype);
		        response.setContentLength(xyzFileContent.length());
		        // sets HTTP header
		        response.setHeader("Content-Disposition", "attachment; filename=\"" + XYZ_FILENAME + "\"");
		        ostream.write(xyzFileContent.getBytes());						
			}else{
				response.sendError(HttpServletResponse.SC_FORBIDDEN);
			}
			transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error(e.getMessage());       
            returnSystemError(response, SERVER_ERROR,e.getMessage());
        } finally {
            if (session != null)
                session.close();
			UserService.clearUserInfoFromRequest();
        }
	}

	private void loadXsltTemplate(){		
		if(template == null){			
			StreamSource xslSource =  new StreamSource(new File(this.getServletContext().getRealPath("/") + XSLT_PATH));
			try {
				template = tFactory.newTemplates(xslSource);
			} catch (TransformerConfigurationException e) {				
			}
		}
	}
	
	private String getXyzFileContent(OutputStream ostream, int calculationId, String mode) throws Exception {		        	      
        try {
        	String realFilePath = CalculationService.getOutputFilePath(calculationId);
    		StreamSource xml = new StreamSource(new File(realFilePath));	    
    	    StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            
            Transformer transformer = template.newTransformer();
            if(mode != null) 
            	transformer.setParameter(PARAMETER_MODE, mode);
            transformer.transform(xml, result);            
            return writer.toString();
        }
        catch (TransformerException e){
            return "";
        }
	}

    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
        throw new UnsupportedOperationException();
    }
}

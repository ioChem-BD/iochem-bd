
package cat.iciq.tcg.labbook.zk.composers.main.navigation;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.EventListener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;

import cat.iciq.tcg.labbook.web.definitions.Constants.ScreenSize;
import cat.iciq.tcg.labbook.web.utils.NavigationDTO;

import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.ReportTypeService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class TreeNavigation extends SelectorComposer<Window> {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(TreeNavigation.class.getName());

	private transient EventQueue<Event> userEventsQueue = null;
	private transient EventQueue<Event> navigationQueue = null;
	private transient EventQueue<Event> elementPublishQueue = null;
	private transient EventQueue<Event> reportManagementQueue = null;
	private transient EventQueue<Event> displayQueue = null;

	@WireVariable("desktopScope")
	private transient Map<String, Object> _desktopScope;

	@Wire
	Window treeWindow;

	@Listen("onClick=#refreshBtn")
	public void onRefreshBtnClick() {
		refresh();
	}

	@Listen("onRequestRefresh=#treeWindow")
	public void onTreeNavigationRequestRefresh() {
		refresh();
	}

	@Listen("onTreeNodesMoveToProject = #treeWindow")
	public void onTreeNavigationTreeNodesSelected(Event e) {
		TreeNodeIdList results = null;
		ObjectMapper mapper = new ObjectMapper();
		Set<Entity> selectedElements = new HashSet<>();
		try {
			results = mapper.readValue((String) e.getData(), TreeNodeIdList.class);
			Long destId = Long.parseLong(results.getTarget());

			if (destId != -1) {
				Entity destination = ProjectService.getById(destId);
				if (destination == null || !PermissionService.hasPermissionOnProject(destId, Permissions.WRITE))
					return;
			} else if (!results.getCalculationIds().isEmpty()) {
				Messagebox.show("Can't move calculations to the root", "Error while moving elements to project.",
						Messagebox.OK, Messagebox.INFORMATION);
				return;
			}
			// Check permission for moved elements
			for (long id : results.getProjectIds()) {
				Project p = ProjectService.getById(id);
				if (p == null || !PermissionService.hasPermissionOnProject(id, Permissions.WRITE))
					return;
				selectedElements.add(p);
			}
			for (long id : results.getCalculationIds()) {
				Calculation c = CalculationService.getById(id);
				if (c == null || !PermissionService.hasPermissionOnCalculation(id, Permissions.WRITE) || destId == -1) // -1 is the root project
					return;
				selectedElements.add(c);
			}
			moveMultipleElementsToProject(destId, selectedElements);
		} catch (Exception e1) {

		}
	}

	@Listen("onTreeNodesMoveToCalculation = #treeWindow")
	public void onTreeNodesMoveToCalculation(Event e) {
		TreeNodeIdList results = null;
		ObjectMapper mapper = new ObjectMapper();
		Set<Entity> selectedElements = new HashSet<>();
		try {
			results = mapper.readValue((String) e.getData(), TreeNodeIdList.class);
			Long destId = Long.parseLong(results.getTarget());

			if (destId == -1 || !results.getProjectIds().isEmpty())
				return;

			Calculation destCalc = CalculationService.getById(destId);
			if (destCalc == null || !PermissionService.hasPermissionOnCalculation(destId, Permissions.WRITE)) {
				return;
			}

			for (long id : results.getCalculationIds()) {
				Calculation c = CalculationService.getById(id);
				if (c == null || !PermissionService.hasPermissionOnCalculation(id, Permissions.WRITE))
					return;
				selectedElements.add(c);
			}

			for (Entity element : selectedElements) {
				CalculationService.moveOverCalculation((Calculation) element, (Calculation) destCalc);
			}
		} catch (Exception e1) {

		}
	}

	@Listen("onProjectSelected = #treeWindow")
	public void onTreeNavigationProjectSelected(Event e) {
		long projectId = Long.parseLong((String) e.getData());
		if (!PermissionService.hasPermissionOnProject(projectId, Permissions.WRITE))
			return;

		Project p = ProjectService.getById(projectId);
		Set<Project> data = new HashSet<>();
		data.add(p);
		_desktopScope.put("selectedElements", data);
		navigationQueue.publish(new Event("displayNavigationElement"));
	}

	@Listen("onCalculationSelected = #treeWindow")
	public void onTreeNavigationCalculationSelected(Event e) {
		long calcId = Long.parseLong((String) e.getData());
		if (!PermissionService.hasPermissionOnCalculation(calcId, Permissions.WRITE))
			return;

		Calculation calc = CalculationService.getById(calcId);
		Set<Calculation> data = new HashSet<>();
		data.add(calc);
		_desktopScope.put("selectedElements", data);
		navigationQueue.publish(new Event("displayNavigationElement"));
	}

	@Listen("onMultipleSelection = #treeWindow")
	public void onTreeNavigationMultipleSelection(Event e){
		navigationQueue.publish(new Event("clearSelection"));
		ObjectMapper mapper = new ObjectMapper();
		try {
			TreeNodeIdList results = mapper.readValue((String) e.getData(), TreeNodeIdList.class);
			Set<Calculation> data = new HashSet<>();
			for (long id : results.getProjectIds()) {
				if (!PermissionService.hasPermissionOnProject(id, Permissions.WRITE))
					return;
				Project p = ProjectService.getById(id);
				ProjectService.getDescendantCalculations(p.getPath()).stream().forEach(c -> data.add(c));
			}
			for (long id : results.getCalculationIds()) {
				if (!PermissionService.hasPermissionOnCalculation(id, ((Permissions.WRITE))))
					return;
				Calculation calc = CalculationService.getById(id);
				data.add(calc);
			}
			_desktopScope.put("selectedElements", data);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@Listen("onPublishSelection = #treeWindow")
	public void onTreeNavigationPublishSelection(Event e) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			TreeNodeIdList results = mapper.readValue((String) e.getData(), TreeNodeIdList.class);
			Set<Entity> data = new HashSet<>();
			if(results.getProjectIds().size() == 1 && results.getCalculationIds().isEmpty()){
				long projectId = results.getProjectIds().get(0);
				if (!PermissionService.hasPermissionOnProject(projectId, Permissions.WRITE))
					return;
				Project p = ProjectService.getById(projectId);
				data.add(p);
				ProjectService.getDescendantProjects(p.getPath()).stream().forEach(pr -> {
					data.add(pr);}
				);
				ProjectService.getDescendantCalculations(p.getPath()).stream().forEach(c -> {
					if(!c.isPublished()) data.add(c);}
				);
				if(data.size() == 1){
					Messagebox.show("There is no new content to publish", "Project publication ", Messagebox.OK, null, null);
					return;
				} 	
			
				_desktopScope.put("selectedElements", data);
				 elementPublishQueue.publish(new Event("show"));
			}
		} catch (Exception e1) {
			logger.error(e1.getMessage());
		}
	}

	@Listen("onCreateReportFromSelection = #treeWindow")
	public void onTreeNavigationCreateReportFromSelection(Event e) {
		TreeNodeIdList results = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			results = mapper.readValue((String) e.getData(), TreeNodeIdList.class);
			Set<Calculation> data = new HashSet<>();
			for (long id : results.getProjectIds()) {
				if (!PermissionService.hasPermissionOnProject(id, Permissions.WRITE))
					return;
				Project p = ProjectService.getById(id);
				ProjectService.getDescendantCalculations(p.getPath()).stream().forEach(c -> data.add(c));
			}
			for (long id : results.getCalculationIds()) {
				if (!PermissionService.hasPermissionOnCalculation(id, ((Permissions.WRITE))))
					return;
				Calculation calc = CalculationService.getById(id);
				data.add(calc);
			}
			int reportTypeId = Integer.parseInt(results.getTarget());
			if (ReportTypeService.getReportTypeById(reportTypeId) == null)
				return;

			_desktopScope.put("selectedElements", data);
			reportManagementQueue.publish(new Event("createreportfromselection", null, reportTypeId));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void moveMultipleElementsToProject(Long projectId, Set<Entity> selectedElements)
			throws InterruptedException {
		String destination = projectId != -1 ? ProjectService.getById(projectId).getPath() : Main.getUserPath();
		if (!selectedElements.isEmpty() && isMoveValid(destination, selectedElements))
	
			Messagebox.show("Move selected items to " + destination + "?", "Move elements",
					Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new OnMoveListener(projectId, selectedElements, userEventsQueue));
	}

	private boolean isMoveValid(String destinationPath, Set<Entity> selectedElements){
		for(Entity element: selectedElements) {			
			boolean moveInvalid = isMovingToSameParentOrItself(element, destinationPath) ||
									 isDestinationChildOfSelection(element, destinationPath) ||
									 existsCollisionsOnDestinationPath(element, destinationPath);
			if(moveInvalid)
				return false;
		}
		return true;
	}

	private boolean isMovingToSameParentOrItself(Entity source, String destinationPath){		
		return (source.getPath().equals(destinationPath) && source.isProject());			
	}

	//This function checks that among selected elements there is no parent of destination element, otherwise it will cause to break path hierarchy and result on orphaned elements.  
	private boolean isDestinationChildOfSelection(Entity source, String destinationPath){			
		if(Paths.isDescendant(source.getPath(), destinationPath) && source.isProject() ){
			Messagebox.show("Can't move a parent project inside its child, please check your selection", "Move elements error", Messagebox.OK, Messagebox.ERROR, null);
			return true;
		}	
		return false;		
	}	
	
	private boolean existsCollisionsOnDestinationPath(Entity source, String destinationPath){
		try{
			if((source.isProject() && ProjectService.projectExists(destinationPath, source.getName())) ||					
					(source.isCalculation() && CalculationService.calculationExists(destinationPath, source.getName()))){
				Messagebox.show("A project/calculation with the same name already exists on destination project. \nPlease rename colliding elements before moving them.", "Move elements error", Messagebox.OK, Messagebox.ERROR, null);					
				return true;				
			}
		}catch(Exception e){
			logger.error(e.getMessage());
			Messagebox.show("An error raised moving selection", "Move elements error", Messagebox.OK, Messagebox.ERROR, null);
			return true;
		}		
		return false;
	}

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		initActionQueues();
		initTree();
		userEventsQueue.publish(new Event("clearSelection"));		// To activate ItemDetails component
	}

	@Listen("onSearchFromHere=#treeWindow")
	public void onTreeNavigationSearchFromHere(Event e) {
		long projectId = Long.parseLong((String) e.getData());
		if (!PermissionService.hasPermissionOnProject(projectId, Permissions.WRITE))
			return;
		Project p = ProjectService.getById(projectId);
		if (p == null)
			return;
		String path = p.getPath();
		navigationQueue.publish(new Event("searchFromHere", null, path));
	}

	public void initTree() {
		Clients.evalJavaScript("baseUrl='"  + Main.getBaseUrl() + "'");
		Clients.evalJavaScript("browseUrl='" + Main.getBrowseBaseUrl() + "'");
		Clients.evalJavaScript("reportTypes=" + getReportTypesJSON());
		Clients.evalJavaScript("initNavigationTree()");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initActionQueues() {
		userEventsQueue = EventQueues.lookup(getUsername() + "userevents", WebApps.getCurrent(), true);
		userEventsQueue.subscribe(new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				switch (event.getName()) {
					case "projectAdded":
						appendProject((String) event.getData());
						break;
					case "calculationAdded":
						appendCalculation((String) event.getData());
						break;
					case "projectModified":
						updateProject((HashMap<String, String>) event.getData());
						break;
					case "calculationModified":
						updateCalculation((HashMap<String, Object>) event.getData());
						break;
					case "projectDeleted":
						deleteProject((Long) event.getData());
						break;
					case "calculationDeleted":
						deleteCalculation((Long) event.getData());
						break;
					case "projectReorder":
						reorderProjects((HashMap<String, Object>) event.getData());
						break;
					case "calculationReorder":
						reorderCalculation((HashMap<String, Object>) event.getData());
						break;
					case "resetHome":
						navigationQueue.publish(new Event("resetHome"));
						break;
					case "clearSelection":
						navigationQueue.publish(new Event("clearSelection"));
						break;
					case "refresh":
						refresh();
						break;
					case "fileRetrieved":
						String[] values = ((String) event.getData()).split("#");
						notifyFileRetrieved(values[0], values[1]);
						break;
				}
			}
		});

		navigationQueue = EventQueues.lookup("navigation", EventQueues.DESKTOP, true);
		navigationQueue.subscribe(new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				switch (event.getName()) {
					case "resetHome":
						initTree();
						break;
					case "clearNavigationSelection":
						Clients.evalJavaScript("clearNavigationSelection()");
						break;
				}
			}
		});
		elementPublishQueue = EventQueues.lookup("elementpublish", EventQueues.DESKTOP, true);
		reportManagementQueue = EventQueues.lookup("reportmanagement", EventQueues.DESKTOP, true);
		displayQueue = EventQueues.lookup("display", EventQueues.DESKTOP, true);
		displayQueue.subscribe(new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				switch (event.getName()) {
					case "sizeChanged":
						ScreenSize size = (ScreenSize) event.getData();
						setupLayout(size);
				}
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String getReportTypesJSON() {
		StringBuilder reportTypesJSON = new StringBuilder();
		reportTypesJSON.append("{\"reports\":[");

		List<ReportType> reportTypes = ReportTypeService.getActiveReportTypes();
		Iterator<ReportType> iter = reportTypes.iterator();
		while (iter.hasNext()) {
			ReportType reportType = iter.next();
			reportTypesJSON
					.append("{\"name\":" + "\"" + reportType.getName() + "\", \"id\":" + reportType.getId() + "}");
			if (iter.hasNext())
				reportTypesJSON.append(",");
		}
		reportTypesJSON.append("]}");
		return reportTypesJSON.toString();
	}

	private String getUsername() {
		try {
			return UserService.getUserName();
		} catch (Exception e) {
			return UUID.randomUUID().toString();
		}
	}

	private void notifyFileRetrieved(String calcId, String calcFileId) {
		Component window = org.zkoss.zk.ui.Path.getComponent("/mainWindow");
		Component component = window.query("div#download-spin-" + calcFileId);
		if (component != null) {
			Div div = (Div) component;
			div.setClass("mx-auto fas fa-cloud-download-alt");
			div.setTooltiptext("File available");
		}

		Calculation calc = CalculationService.getById(Long.parseLong(calcId));
		String message = "Files from calculation:<br/><b>" + calc.getPath() + "</b><br/>Are now available to download.";
		Clients.evalJavaScript("setupToast('" + message + "');");
	}

	private void setupLayout(ScreenSize size) {
		boolean isSmallLayout = (size == ScreenSize.SMALL || size == ScreenSize.X_SMALL);
		// Clients.evalJavaScript("setupNavigationLayout(" + isSmallLayout + ")");
	}

	private void refresh() {
		navigationQueue.publish(new Event("resetHome"));
	}

	private void appendProject(String projIdStr) {
		if (projIdStr == null || projIdStr.isEmpty())
			return;
		long projId = Long.parseLong(projIdStr);
		Project p = ProjectService.getById(projId);
		try {
			String jsonArray = new ObjectMapper().writeValueAsString(NavigationDTO.fromEntity(p, new HashMap<>()));
			long parentId = p.getParentPath().equals(Main.getUserPath()) ? -1
					: ProjectService.getByPath(p.getParentPath()).getId();
			Clients.evalJavaScript("addProject(" + parentId + ", " + jsonArray + ")");
		} catch (Exception e) {
			refresh();
		}
	}

	private void appendCalculation(String calcIdStr) {
		if (calcIdStr == null || calcIdStr.isEmpty())
			return;
		long calcId = Long.parseLong(calcIdStr);
		Calculation c = CalculationService.getById(calcId);
		long parentId = ProjectService.getByPath(c.getParentPath()).getId();
		try {
			String jsonArray = new ObjectMapper().writeValueAsString(NavigationDTO.fromEntity(c, null));
			Clients.evalJavaScript("addCalculation(" + parentId + ", " + jsonArray + ")");
		} catch (Exception e) {
			refresh();
		}
	}

	private void updateProject(HashMap<String, String> data) {
		Project p = ProjectService.getById(Long.parseLong(data.get("id")));
		long parentId = p.getParentPath().equals(Main.getUserPath()) ? -1
				: ProjectService.getByPath(p.getParentPath()).getId();
		try {
			String jsonArray = new ObjectMapper().writeValueAsString(NavigationDTO.fromEntity(p, new HashMap<>()));
			Clients.evalJavaScript("updateProject(" + parentId + ", " + data.get("id") + "," + jsonArray + ")");
		} catch (JsonProcessingException e) {
			refresh();
		}
	}

	private void updateCalculation(HashMap<String, Object> data) {
		Calculation c = CalculationService.getById((Long) data.get("id"));
		if (c == null)
			return;
		Project p = ProjectService.getByPath(c.getParentPath());
		if (p == null)
			return;
		long parentId = p.getId();
		NavigationDTO navDTO = NavigationDTO.fromEntity(c, null);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonArray;
		try {
			jsonArray = objectMapper.writeValueAsString(navDTO);
			Clients.evalJavaScript("updateCalculation(" + parentId + ", " + c.getId() + "," + jsonArray + ")");
		} catch (JsonProcessingException e) {
			refresh();
		}
	}

	private void reorderCalculation(HashMap<String, Object> data) {
		long id = (Long) data.get("id");
		String oldPath = (String) data.get("oldParentPath");
		String newPath = (String) data.get("newParentPath");
		int oldOrder = (Integer) data.get("oldOrder");
		int newOrder = (Integer) data.get("newOrder");
		Clients.evalJavaScript("updateCalculationOrder(" + id + ",'" + oldPath + "', '" + newPath + "', " + oldOrder
				+ ", " + newOrder + ")");
	}

	private void reorderProjects(HashMap<String, Object> data) {
		long id = (Long) data.get("id");
		int oldOrder = (Integer) data.get("oldOrder");
		int newOrder = (Integer) data.get("newOrder");
		String oldPath = (String) data.get("oldParentPath");
		String newPath = (String) data.get("newParentPath");
		Clients.evalJavaScript("updateProjectOrder(" + id + ",'" + oldPath + "', '" + newPath + "', " + oldOrder + ", "
				+ newOrder + ")");
	}

	private void deleteProject(Long projIdStr) {
		Clients.evalJavaScript("deleteProject(" + projIdStr + ")");
	}

	private void deleteCalculation(Long calcIdStr) {
		Clients.evalJavaScript("deleteCalculation(" + calcIdStr + ")");
	}
}

class TreeNodeIdList {

	@JsonProperty("projects")
	private List<Long> projectIds;

	@JsonProperty("calculations")
	private List<Long> calculationIds;

	@JsonProperty("target")
	private String target;

	public TreeNodeIdList() {

	}

	public TreeNodeIdList(List<Long> projectIds, List<Long> calculationIds, String target) {
		this.projectIds = projectIds;
		this.calculationIds = calculationIds;
		this.target = target;
	}

	public void setProjectIds(List<Long> projectIds) {
		this.projectIds = projectIds;
	}

	public void setCalculationIds(List<Long> calculationIds) {
		this.calculationIds = calculationIds;
	}

	public List<Long> getProjectIds() {
		return projectIds;
	}

	public List<Long> getCalculationIds() {
		return calculationIds;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
}

class OnMoveListener implements EventListener {
	private Long projectId;
	private Set<Entity> selectedElements;
	private EventQueue<Event> userEventsQueue;

	public OnMoveListener(Long projectId, Set<Entity> selectedElements, EventQueue<Event> userEventsQueue) {
		this.projectId = projectId;
		this.selectedElements = selectedElements;
		this.userEventsQueue = userEventsQueue;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (Messagebox.ON_YES.equals(event.getName())) {
			for (Entity element : selectedElements) { // If move action accepted send move commands and last one to
														// check whether tree needs to be reloaded
				moveToProject(projectId, element);
			}
			userEventsQueue.publish(new Event("clearSelection"));
		}
	}

	private void moveToProject(Long projectId, Entity element) {
		try {
			if (element.isProject()) {
				if (projectId == -1)
					ProjectService.update((Project) element, Main.getUserPath());
				else
					ProjectService.update((Project) element, ProjectService.getById(projectId).getPath());
			} else
				CalculationService.moveOverProject((Calculation) element, ProjectService.getById(projectId));
		} catch (Exception e) {
			Messagebox.show(e.getMessage(), "Error while moving elements to project.", Messagebox.OK,
					Messagebox.INFORMATION);
		}
	}
}

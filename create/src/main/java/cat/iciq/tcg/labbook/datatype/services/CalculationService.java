/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.MolecularFingerprint;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class CalculationService {

    private static final Logger log = LogManager.getLogger(CalculationService.class);

    public static long add(String path, String name, String description, int calculationType) throws Exception {
        if (getByPath(Paths.getFullPath(path, name)) != null) {
            throw new Exception("A calculation with such name already exists on destination project.");
        }

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            Calculation calc = new Calculation();
            calc.setType(CalculationTypeService.getCalculationTypeById(calculationType));
            calc.setName(name);
            calc.setParentPath(path);
            calc.setDescription(description);
            calc.setHandle("");
            calc.setPublishedName("");
            calc.setCreationDate(new Timestamp(System.currentTimeMillis()));
            calc.setPublished(false);
            calc.setElementOrder(OrderService.getNewCalculationOrderIndex(path));
            session.save(calc);
            session.flush();
            return calc.getId();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    public static boolean calculationExists(String path, String name) throws Exception {
        return getByPath(Paths.getFullPath(path, name)) != null;
    }

    public static Calculation getById(long calculationId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            return (Calculation) SerializationUtils.clone(session.get(Calculation.class, calculationId));
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static Calculation getByPath(String calculationPath) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Calculation> criteriaQuery = builder.createQuery(Calculation.class);
            Root<Calculation> root = criteriaQuery.from(Calculation.class);

            Predicate path = builder.equal(root.get("path"), Paths.getParent(calculationPath));
            Predicate name = builder.equal(root.get("name"), Paths.getTail(calculationPath));
            criteriaQuery.select(root).where(builder.and(path, name));

            Query<Calculation> query = session.createQuery(criteriaQuery);
            return (Calculation) SerializationUtils.clone(query.getSingleResult());
        } catch (NoResultException e1) {

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static long addCalculationFile(long calcId, String filename) throws Exception {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            Calculation calculation = getById(calcId);
            CalculationFile calcFile = new CalculationFile();
            calcFile.setCalculation(calculation);
            calcFile.setName(filename);
            calcFile.setFile(AssetstoreService.getCalculationFilePartialPath(calcId, filename));
            session.persist(calcFile);
            session.flush();
            return calcFile.getId();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    public static void delete(long id) throws Exception {
        delete(id, true);
    }

    public static void delete(long id, boolean checkPermissions) throws Exception {
        Calculation calculation = getById(id);
        if (calculation == null)
            return;
        if (checkPermissions && !PermissionService.hasPermissionOnCalculation(id, Permissions.DELETE))
            throw new Exception("Not enough permissions to delete calculation.");

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            // Delete calculation files, folder and from database
            deleteCalculationFiles(calculation.getId());
            AssetstoreService.deleteCalculation(id);
            Calculation mergedCalculation = (Calculation) session.merge(calculation);
            session.delete(mergedCalculation);
            // Reorder calculation orders
            OrderService.reduceCalculationSiblingsOrder(calculation.getElementOrder(), calculation.getParentPath());
            TreeEvent.sendEventToUserQueue(new Event("calculationDeleted", null, calculation.getId()));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    private static void deleteCalculationFiles(long calculationId) throws Exception {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaDelete<CalculationFile> criteriaDelete = builder.createCriteriaDelete(CalculationFile.class);
            Root<CalculationFile> root = criteriaDelete.from(CalculationFile.class);

            Predicate predicate = builder.equal(root.get("calculation").get("id"), calculationId);
            criteriaDelete.where(predicate);

            session.createQuery(criteriaDelete).executeUpdate();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    public static List<CalculationFile> getCalculationFiles(long id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List<CalculationFile> results = new ArrayList<>();
        try {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<CalculationFile> criteriaQuery = builder.createQuery(CalculationFile.class);
            Root<CalculationFile> root = criteriaQuery.from(CalculationFile.class);

            Predicate predicate = builder.equal(root.get("calculation").get("id"), id);
            criteriaQuery.where(predicate);

            results = session.createQuery(criteriaQuery).getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return results;
    }

    public static void moveOverProject(Calculation calculation, Project project) throws Exception {
        update(calculation, project.getPath());
        // Path has been changed, reorder its surrounding siblings
        OrderService.moveCalculation(calculation, null, calculation.getParentPath(), project.getPath());
    }

    public static void moveOverCalculation(Calculation sourceCalculation, Calculation destinationCalculation)
            throws Exception {
        update(sourceCalculation, destinationCalculation.getParentPath());
        // Path has been changed, reorder its surrounding siblings
        OrderService.moveCalculation(sourceCalculation, destinationCalculation, sourceCalculation.getParentPath(),
                destinationCalculation.getParentPath());
    }

    public static void update(Calculation calculation) throws Exception {
        update(calculation, calculation.getParentPath());
    }

    private static void update(Calculation calculation, String newPath) throws Exception {
        update(true, UserService.getUserName(), calculation, newPath);
    }

    public static void update(boolean checkPermissions, String username, Calculation calculation, String newPath) throws Exception {
        Calculation modifiedCalculation = (Calculation) SerializationUtils.clone(calculation);
        Calculation originalCalculation = getById(modifiedCalculation.getId());

        if (!isValidPath(username, modifiedCalculation.getParentPath()))
            throw new Exception("Can't set calculation, not a valid destination path");

        if(checkPermissions && !PermissionService.hasPermissionOnProject(modifiedCalculation.getParentPath(), Permissions.WRITE))
                    throw new Exception("Can't update calculation, not a valid destination path");
        

        if (!modifiedCalculation.getPath().equals(originalCalculation.getPath())
            && getByPath(modifiedCalculation.getPath()) != null)
            throw new Exception("A calculation with name " + modifiedCalculation.getName() + " already exists at the current path.");


        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            modifiedCalculation.setParentPath(newPath);
            session.merge(modifiedCalculation);            
            session.flush();
        } catch (Exception e) {
            throw new Exception("An error raised while updating the calculation.");
        }
        // Send refresh UI event
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", originalCalculation.getId());
        params.put("oldPath", originalCalculation.getPath());
        TreeEvent.sendEventToUserQueue(new Event("calculationModified", null, params));
    }

    private static boolean isValidPath(String username, String destinationPath) throws BrowseCredentialsException {
        String userRoot = GeneralConstants.DB_ROOT + username;
        return !destinationPath.equals(userRoot) && destinationPath.startsWith(userRoot + "/") ; // Can't work with calculation on users' root
    } 

    public static void publishCalculation(String username, long id, String publishedName, String handle) throws Exception {
        Calculation calculation = getById(id);
        if (calculation == null)
            throw new Exception("Calculation to be marked as published not found.");
        calculation.setPublished(true);
        calculation.setHandle(handle);
        calculation.setPublishedName(publishedName);
        calculation.setPublicationDate(new Timestamp(System.currentTimeMillis()));
        update(false, username, calculation, calculation.getParentPath());
    }

    public static String getOutputFilePath(long calculationId) {
        try {
            Calculation calculation = getById(calculationId);
            if (calculation == null)
                return null;
            XMLFileManager xmlFileManager = new XMLFileManager(MetsFileHandler.METS_NAMESPACE,
                    MetsFileHandler.METS_ADDITIONAL_NAMESPACES, calculation.getMetsXml());
            String outputFileName = xmlFileManager
                    .getSingleAttributeValueQuery(XpathQueries.GET_CALCULATION_OUTPUT_FILENAME);
            return FileService.getCalculationPath(calculationId, outputFileName);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static List<Long> getCalculationsWithoutFingerprint(int pageSize, int pageNumber) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Calculation> calculationRoot = criteriaQuery.from(Calculation.class);

        Subquery<Long> subquery = criteriaQuery.subquery(Long.class);
        Root<MolecularFingerprint> fingerprintRoot = subquery.from(MolecularFingerprint.class);
        subquery.select(fingerprintRoot.get("calculation").get("id"));

        criteriaQuery.select(calculationRoot.get("id"))
                .where(criteriaBuilder.not(criteriaBuilder.in(calculationRoot.get("id")).value(subquery)));

        Query<Long> query = session.createQuery(criteriaQuery);
        int offset = (pageNumber - 1) * pageSize;      
        query.setMaxResults(pageSize);
        query.setFirstResult(offset);
        return query.getResultList();
    }
}

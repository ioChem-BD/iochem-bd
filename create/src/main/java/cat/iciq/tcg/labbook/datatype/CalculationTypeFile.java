/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import cat.iciq.tcg.labbook.shell.datatype.CalculationTypeFileDTO;

@Entity
@Table(name="cutting_area_definitions", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CalculationTypeFile {

	public static enum Use {INPUT, OUTPUT, ADDITIONAL, APPEND};
    
	@Id
    @Column(name = "id", updatable = false, nullable = false)        
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "cutting_area_definitions_id_seq")
    @SequenceGenerator(name = "cutting_area_definitions_id_seq", sequenceName = "cutting_area_definitions_id_seq", allocationSize = 1)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "calculation_type_id", foreignKey = @ForeignKey(name = "cutting_area_definitions_calculation_type_id_fkey"), nullable = false)
	private CalculationType calculationType;
	
	@Column(name = "default_type_sel", nullable = false)
	private boolean defaultTypeSelection;
	
	@Column(name = "abbreviation", nullable = false)
	private String abbreviation; 
	
	@Column(name = "file_name", nullable = false)
	private String fileName;
	
	@Column(name = "url", nullable = false)
	private String url;
	
	@Column(name = "description", nullable = false)
	private String description;
	
	@Column(name = "jumbo_converter_class", nullable = false)
	private String jumboConverterClass;
	
	@Column(name = "jumbo_converter_in_type", nullable = false)
	private String jumboConverterInType;
	
	@Column(name = "jumbo_converter_out_type", nullable = false)
	private String jumboConverterOutType;
	
	@Column(name = "mimetype", nullable = false)
	private String mimetype;
	
	@Column(name = "use", nullable = false)
	private String use;
	
	@Column(name = "label", nullable = false)
	private String label;
	
	@Column(name = "behaviour")
	private String behaviour;
	
	@Column(name = "requires")
    private String requires;
	
	@Column(name = "renameto")
    private String renameTo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public CalculationType getCalculationType() {
        return calculationType;
    }
    public void setCalculationType(CalculationType calculationType) {
        this.calculationType = calculationType;
    }
    
	public boolean isDefaultTypeSelection() {
		return defaultTypeSelection;
	}
	public void setDefaultTypeSelection(boolean defaultTypeSelection) {
		this.defaultTypeSelection = defaultTypeSelection;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getJumboConverterClass() {
		return jumboConverterClass;
	}
	public void setJumboConverterClass(String jumboConverterClass) {
		this.jumboConverterClass = jumboConverterClass;
	}
	public String getJumboConverterInType() {
		return jumboConverterInType;
	}
	public void setJumboConverterInType(String jumboConverterInType) {
		this.jumboConverterInType = jumboConverterInType;
	}
	public String getJumboConverterOutType() {
		return jumboConverterOutType;
	}
	public void setJumboConverterOutType(String jumboConverterOutType) {
		this.jumboConverterOutType = jumboConverterOutType;
	}
	public String getMimetype() {
		return mimetype;
	}
	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}
	public String getUse() {
		return use;
	}
	public void setUse(String use) {
		this.use = use;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getBehaviour() {
		return behaviour;
	}
	public void setBehaviour(String behaviour) {
		this.behaviour = behaviour;
	}
    public String getRequires() {
        return requires;
    }
    public void setRequires(String requires) {
        this.requires = requires;
    }
    public String getRenameTo() {
        return renameTo;
    }
    public void setRenameTo(String renameTo) {
        this.renameTo = renameTo;
    }

    public static CalculationTypeFileDTO convertToDTO(CalculationTypeFile c) {
        CalculationTypeFileDTO cDTO = new CalculationTypeFileDTO();
        cDTO.setId(c.getId());
        cDTO.setCalculationTypeName(c.getCalculationType().getName());
        cDTO.setDefaultTypeSelection(c.isDefaultTypeSelection());
        cDTO.setAbbreviation(c.getAbbreviation());
        cDTO.setFileName(c.getFileName());
        cDTO.setUrl(c.getUrl());
        cDTO.setDescription(c.getDescription());
        cDTO.setJumboConverterClass(c.getJumboConverterClass());
        cDTO.setJumboConverterInType(c.getJumboConverterInType());
        cDTO.setJumboConverterOutType(c.getJumboConverterOutType());
        cDTO.setMimetype(c.getMimetype());
        cDTO.setUse(c.getUse());
        cDTO.setLabel(c.getLabel());
        cDTO.setBehaviour(c.getBehaviour());
        cDTO.setRequires(c.getRequires());
        cDTO.setRenameTo(c.getRenameTo());
        return cDTO;
    }

    public static List<CalculationTypeFileDTO> convertListToDTO(List<CalculationTypeFile> calcTypeFiles) {
        List<CalculationTypeFileDTO> results = new ArrayList<>();
        for(CalculationTypeFile c : calcTypeFiles)
            results.add(convertToDTO(c));
        return results;
    }
}

package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.PublicableEntity;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.utils.NavigationDTO;

import java.util.HashMap;

public class Navigation extends HibernateService {

    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {

        String userRoot = GeneralConstants.DB_ROOT + UserService.getUserName();     

        List<Project> projects = ProjectService.getDescendantProjects(userRoot);
        List<Calculation> calculations = ProjectService.getDescendantCalculations(userRoot);
        
        List<NavigationDTO> rootEntities = new ArrayList<>();
        HashMap<String, NavigationDTO> entitiesMap = new HashMap<>();
    
        Map<String, String> editHashCache = new HashMap<>();
        for(PublicableEntity e : projects) {
            NavigationDTO dto = NavigationDTO.fromEntity(e, editHashCache);
            entitiesMap.put(e.getPath(), dto);
            if(e.getParentPath().equals(userRoot)){
                rootEntities.add(dto);                
            }else{
                entitiesMap.get(e.getParentPath()).addChild(dto);
                entitiesMap.put(e.getHandle(), dto);
            }
        }
        for(PublicableEntity e : calculations) {
            NavigationDTO dto = NavigationDTO.fromEntity(e, null);
            if(entitiesMap.get(e.getParentPath()) != null)   // If not hierarchy error exist
                entitiesMap.get(e.getParentPath()).addChild(dto);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonArray = objectMapper.writeValueAsString(rootEntities);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonArray);
    }
}

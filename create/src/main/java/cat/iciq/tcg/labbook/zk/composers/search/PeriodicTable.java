package cat.iciq.tcg.labbook.zk.composers.search;

import java.util.Map;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.impl.DesktopEventQueue;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

public class PeriodicTable extends SelectorComposer<Window> {
    
    @Wire Window table;
    
    @Listen ("onClick=#acceptBtn")
    public void onAcceptClick() {        
        Clients.evalJavaScript("getSelectedAtoms()");
    }
    
    @Listen("onAtomsSelected=#table")
    public void onIframeResponse(Event event) {
        Map<?, ?> data = (Map<?, ?>) event.getData();
        if (data.containsKey("returnValue")) {
            String returnValue = (String) data.get("returnValue");
            ((DesktopEventQueue<Event>) EventQueues.lookup("navigation", EventQueues.DESKTOP, true)).publish(new Event("searchAtomsSelected", null, returnValue));
            table.detach();        
        }
    }
    
    @Listen ("onClick=#cancelBtn")
    public void onCancelClick() {
        table.detach(); 
    }
}

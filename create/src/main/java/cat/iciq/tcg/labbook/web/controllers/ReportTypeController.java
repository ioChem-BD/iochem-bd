package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.datatype.services.ReportTypeService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class ReportTypeController extends HibernateService {

    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try {
            List<ReportType> reportTypes = ReportTypeService.getActiveReportTypes();
         
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode reportTypesJson = mapper.createArrayNode();

            for (ReportType reportType : reportTypes) {
                ObjectNode reportTypeJson = mapper.createObjectNode();
                reportTypeJson.put("id", reportType.getId());
                reportTypeJson.put("name", reportType.getName());
                reportTypeJson.put("enabled", reportType.isEnabled());
                reportTypesJson.add(reportTypeJson);
            }
            sendObj(response, reportTypesJson);
        } catch (Exception e) {
            returnKO(response, "Error retrieving report types", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnKO(response, "Method not supported", "POST method is not supported for ReportTypeController");
    }

    @Override
    public void executePutService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnKO(response, "Method not supported", "PUT method is not supported for ReportTypeController");
    }

    @Override
    public void executeDeleteService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnKO(response, "Method not supported", "DELETE method is not supported for ReportTypeController");
    }
}
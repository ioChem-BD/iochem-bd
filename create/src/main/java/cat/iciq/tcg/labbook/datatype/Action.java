/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="actions", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Action {

    @Id
    @Column(name = "id", updatable = false, nullable = false)        
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "actions_id_seq")
    @SequenceGenerator(name = "actions_id_seq", sequenceName = "actions_id_seq", allocationSize = 1)
    private int id;
    
    @Column(name = "mimetype", nullable = false)       
    private String mimetype;
    
    @Column(name = "jumbo_format", nullable = false)
    private String jumboFormat;
    
    @Column(name = "action", nullable = false)
    private String action;
    
    @Column(name = "parameters", nullable = false)
    private String parameters;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMimetype() {
		return mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public String getJumboFormat() {
		return jumboFormat;
	}

	public void setJumboFormat(String jumboFormat) {
		this.jumboFormat = jumboFormat;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
    
    
	
}

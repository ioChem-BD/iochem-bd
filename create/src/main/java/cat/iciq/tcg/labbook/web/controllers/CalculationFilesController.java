package cat.iciq.tcg.labbook.web.controllers;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;
import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;

public class CalculationFilesController extends HibernateService {

    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {

        String type = params.get("type");
        Calculation calculation = getCalculation(params);
        if (!PermissionService.hasPermissionOnCalculation(calculation, Permissions.READ)) {
            new ErrorResponse(HttpServletResponse.SC_FORBIDDEN,"Unauthorized",  "You don't have permission to access this calculation.").sendErrorResponse(response);
            return;
        }
        if (type.equalsIgnoreCase("files")) {
            String id = params.get(PARAMETER_ID);
            List<String> listFileNames = new ArrayList<String>();
            List<CalculationFile> files = CalculationService.getCalculationFiles(Long.parseLong(id));
            for (CalculationFile c : files) {
                c.getCalculation();
                String[] pathFile = c.getFile().split("/");
                listFileNames.add(pathFile[(pathFile.length - 1)]);
            }
            sendObj(response, listFileNames);
        } else if (type.equalsIgnoreCase("cml")) {
            try {
                if (calculation == null)
                    returnKO(response, userName, "Can't acces to calculation");
                XMLFileManager xmlFileManager = new XMLFileManager(MetsFileHandler.METS_NAMESPACE,
                        MetsFileHandler.METS_ADDITIONAL_NAMESPACES, calculation.getMetsXml());
                String outputFileName = xmlFileManager
                        .getSingleAttributeValueQuery(XpathQueries.GET_CALCULATION_OUTPUT_FILENAME);

                File xmlOut = new File(FileService.getCalculationPath(calculation.getId(), outputFileName));

                sendFile(response, xmlOut);

            } catch (Exception e) {
                returnKO(response, userName, e.getMessage());
            }
        } else if (type.equalsIgnoreCase("bitstream")) {

        } else {
            returnKO(response, userName, "there is no type engine or it does not exist.");
        }
    }

    protected Calculation getCalculation(Map<String, String> params) {
        String searchType = params.get(PARAMETER_SEARCHTYPE);
        String path = params.get(PARAMETER_PATH);
        String id = params.get(PARAMETER_ID);

        if (searchType == null || searchType.equalsIgnoreCase("id")) {
            return CalculationService.getById(Long.parseLong(id));
        }
        if (searchType.equalsIgnoreCase("path")) {
            return CalculationService.getByPath(path);
        }
        return null;
    }
}
/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.Vector;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.smarts.Smarts;
import org.openscience.cdk.smarts.SmartsPattern;
import org.openscience.cdk.smiles.SmilesParser;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.AfterSizeEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.event.impl.DesktopEventQueue;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.XulElement;

import com.google.re2j.Pattern;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationType;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.CalculationTypeService;
import cat.iciq.tcg.labbook.datatype.services.FingerprintService;
import cat.iciq.tcg.labbook.datatype.services.FingerprintService.FORMULA_TYPE;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.SearchService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.datatype.services.SearchService.Filters;
import cat.iciq.tcg.labbook.datatype.services.SearchService.SearchType;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.CalculationInsertion;
import cat.iciq.tcg.labbook.zk.ui.FingerprintDaemon;
import cat.iciq.tcg.labbook.zk.ui.FingerprintDaemon.STATUS;

public class Search extends SelectorComposer<Window>{

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LogManager.getLogger(Search.class.getName());
	private DesktopEventQueue<Event> navigationQueue = null;
	
	private static final int OFFSET = 150;  
	private static final float HEIGHT_PER_RESULT = 24.0f;  
	
	private static final String MOLECULAR_EDITOR_ZUL = "/zul/main/search/molecularEditor.zul";
	private static final String PERIODIC_TABLE_ZUL = "/zul/main/search/periodicTable.zul";

	private static final String SELECT_USER_ZUL = "/zul/main/search/selectUser.zul";
	private static final String SELECT_GROUP_ZUL = "/zul/main/search/selectGroup.zul";
		
	private String formula = "";
	private String formulaSearchType = "";
	private FORMULA_TYPE formulaType = FORMULA_TYPE.SMILES;
	
	@WireVariable("desktopScope")
	private Map<String, Object> _desktopScope;
	
	//Search form
	@Wire 
	Div searchDiv;
	@Wire
	Button searchBtn;
    @Wire
    Listbox type;
	@Wire
	Groupbox element;
	@Wire 
	Groupbox owner;
	@Wire 
	Groupbox group;
	@Wire 
	Radiogroup formulaSearchTypeRg;
	@Wire
	Div geometrySearchDiv;
	//Results form
	@Wire
	Button resetSearchBtn;
	@Wire
	Div resultsDiv;
	@Wire 
	Tree searchTree;
	@Wire
    Treechildren searchTreeChildren;
	@Wire
	Menupopup treePopup;
	@Wire 
	Menupopup emptyTreePopup;
    @Wire 
    Popup thumbnailPopup;    
    @Wire 
    Textbox formulaTxt;
    @Wire 
    Textbox path;
    @Wire 
    Label fingerprintStatusLbl;
    @Wire
    Button drawBtn;    
    @Wire 
    Listbox smilesCond;
    @Wire
    Label geometryLbl;

	@Listen("onAfterSize=#searchWindow")
	public void afterSize(AfterSizeEvent e) {
	    int totalHeight = e.getHeight();
	    int availableHeight = totalHeight - OFFSET;
	    int resultsPerPage = Math.round(availableHeight / HEIGHT_PER_RESULT);

		if(resultsPerPage < 5) 
			resultsPerPage = 30;			
		searchTree.setPageSize(resultsPerPage);
		searchTree.setRows(resultsPerPage);
	}

	@Listen("onClick=#searchBtn")
	public void onSearchBtnClick() throws Exception {
		List<Object[]> results = runSearchQuery();
		if(!results.isEmpty())
		    displayResults(results);
	}

	@Listen("onClick=#resetSearchBtn")
	public void onResetSearchBtnClick(){
		resetSearch();
	}

    @Listen("onSelect=#searchTree")    
    public void onTreePopupOpen() {        
        searchTree.setContext(isProjectSelected()? treePopup : emptyTreePopup);
    }

    @Listen("onClick=#treeDivSearchFromHere")
    public void onTreeDivSearchFromHereClick() {
        Treeitem  item = searchTree.getSelectedItem();
        String path = ((Entity) item.getAttribute("properties")).getPath();        
        navigationQueue.publish(new Event("searchFromHere", null, path));        
    }
    
    private boolean isProjectSelected() {
        if(searchTree.getSelectedItems().size() == 1) {
            Treeitem  item = searchTree.getSelectedItem();            
            return ((Entity) item.getAttribute("properties")).isProject();
        }
        return false;
    }
	
	@Listen("onOpen=#thumbnailPopup")
	public void showThumbnail(Event e) throws MalformedURLException{		
		URL reconstructedURL = new URL(Executions.getCurrent().getScheme(),Executions.getCurrent().getServerName(),Executions.getCurrent().getServerPort(),Executions.getCurrent().getContextPath());
		Treerow row = (Treerow)((OpenEvent) e).getReference();
		if(row != null){
			Entity dc = (Entity) row.getAttribute("properties");										
			((Html)thumbnailPopup.getChildren().get(0)).setContent("<img style='width:200px;height:200px' src='" + reconstructedURL.toString() +"/innerServices/getfile?id=" + dc.getId() + "&file=" + CalculationInsertion.THUMBNAIL_FILE_NAME + "' alt='No image available'></image>");				
		}
	}
	
	@Listen("onClick=#drawBtn")
    public void onDBtnClick(){
        Window window = (Window)Executions.createComponents(MOLECULAR_EDITOR_ZUL, null, null);
        window.doModal();
        Events.postEvent(Events.ON_OPEN, window, null);
    }
	
	@Listen("onClick=#elementBtn")
	public void onElementBtnClick(){
		Window window = (Window)Executions.createComponents(PERIODIC_TABLE_ZUL, null, null);
		window.doModal();
		Events.postEvent(Events.ON_OPEN, window, null);
	}
	
	@Listen("onClick=#ownerBtn")
	public void onOwnerBtnClick(){
		Window window = (Window)Executions.createComponents(SELECT_USER_ZUL, null, null);
        window.doModal();
        Events.postEvent(Events.ON_OPEN, window, null);
	}

	@Listen("onClick=#groupBtn")
	public void onGroupBtnClick(){
		Window window = (Window)Executions.createComponents(SELECT_GROUP_ZUL, null, null);
        window.doModal();
        Events.postEvent(Events.ON_OPEN, window, null);
	}
	
    @SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Window comp) throws Exception {	
		super.doAfterCompose(comp);
		initActionQueues();
		fillTypeListbox();
		setFingerprintStatus();
		searchTree.addEventListener("onSelect", new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {				
				Treeitem item = ((Tree)event.getTarget()).getSelectedItem();
				Entity dc = (Entity)item.getAttribute("properties");
				HashSet<Entity> selectedElements = new HashSet<Entity>();
				selectedElements.add(dc.isProject()? 
							ProjectService.getById(dc.getId()): 
								CalculationService.getById(dc.getId()));				
				_desktopScope.put("selectedSearchElements", selectedElements); 
				navigationQueue.publish(new Event("displaySearchElement"));				
			}			
		});
	}
     
    private void setFingerprintStatus() {
        STATUS status = WebApps.getCurrent().hasAttribute(FingerprintDaemon.FP_DAEMON_STATUS) ? 
                            (STATUS)(WebApps.getCurrent().getAttribute(FingerprintDaemon.FP_DAEMON_STATUS)): STATUS.OK;
        String message = WebApps.getCurrent().hasAttribute(FingerprintDaemon.FP_DAEMON_ATTRIBUTE) ? (String)(WebApps.getCurrent().getAttribute(FingerprintDaemon.FP_DAEMON_ATTRIBUTE)): "";

        switch(status) {        
            case OK: 
            case INDEXING:  
                        drawBtn.setDisabled(false);
                        formulaTxt.setDisabled(false);
                        smilesCond.setDisabled(false);
                        break;           
            case ERROR: formulaTxt.setValue("");                       
                        drawBtn.setDisabled(true);
                        formulaTxt.setDisabled(true);
                        smilesCond.setDisabled(true);        
                        break;
        }
        geometryLbl.setValue(message.isEmpty()? "Geometry":"Geometry¹");
        fingerprintStatusLbl.setValue(message);        
    }

    @SuppressWarnings("unchecked")
	private void initActionQueues() {    	
    	navigationQueue = (DesktopEventQueue<Event>) EventQueues.lookup("navigation", 	EventQueues.DESKTOP, true);    	
     	navigationQueue.subscribe(new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if(event.getName().equals("resetSearch")) {
					resetSearch();
				}else if(event.getName().equals("smilesProvided")) {
				    formulaTxt.setValue((String)event.getData());
				}else if(event.getName().equals("searchAtomsSelected")) {
					String newElements = (String)event.getData();
					element.setVisible(true);
					addElements(newElements);
				}else if(event.getName().equals("searchFromHere")) {
				    resetSearch();
				    path.setValue((String)event.getData());
				    setFingerprintStatus();
				}else if(event.getName().equals("searchTabFocused")) {
				    setFingerprintStatus();
				}
			}
     	});

     	EventQueue<Event> addUsersQueue = EventQueues.lookup("searchaddusers",     EventQueues.DESKTOP, true);        
     	addUsersQueue.subscribe(new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {
				String newOwner = (String)event.getData();
				owner.setVisible(true);
				addOwner(newOwner, UserService.getUserIdByName(newOwner));				
			}     	
     	});
     	
     	EventQueue<Event> addGroupsQueue = EventQueues.lookup("searchaddgroups",   EventQueues.DESKTOP, true);        
     	addGroupsQueue.subscribe(new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {
				String newGroup = (String)event.getData();
				group.setVisible(true);
				addGroup(newGroup, UserService.getGroupIdByName(newGroup));
			}
     		
     	});
	}

    private void addElements(String data) {
		String[] elements = data.trim().split("[\\s+]");
		for(String newElement: elements)
			addElement(newElement);
	}

	private void addOwner(String newOwner, int newOwnerId){
    	if(optionIsAlreadySelected(owner, newOwner))
    		return;    	
    	Button button = new Button(newOwner);
    	button.setAttribute("value", newOwnerId);
    	button.setZclass("btn btn-outline-secondary ml-1");
    	button.addEventListener("onClick", new EventListener<Event>(){
			@Override
			public void onEvent(Event event) throws Exception {	
				Component button = event.getTarget();
				button.detach();
				if(owner.getChildren().isEmpty())
					owner.setVisible(false);
			}    		
    	});
    	owner.appendChild(button);
    }
    
    private void addGroup(String newGroup, int newGroupId){
    	if(optionIsAlreadySelected(group, newGroup))
    		return;    	
    	Button button = new Button(newGroup);
    	button.setAttribute("value", newGroupId);
    	button.setZclass("btn btn-outline-secondary ml-1");
    	button.addEventListener("onClick", new EventListener<Event>(){
			@Override
			public void onEvent(Event event) throws Exception {	
				Component button = event.getTarget();
				button.detach();
				if(group.getChildren().size() == 0)
					group.setVisible(false);
			}    		
    	});
    	group.appendChild(button);
    }

	private void addElement(String newElement){
    	if(optionIsAlreadySelected(element, newElement))
    		return;    	
    	Button button = new Button(newElement);
    	button.setAttribute("value", newElement);
    	button.setZclass("btn btn-outline-secondary ml-1");
    	button.addEventListener("onClick", new EventListener<Event>(){
			@Override
			public void onEvent(Event event) throws Exception {	
				Component button = event.getTarget();
				button.detach();
				if(element.getChildren().isEmpty())
					element.setVisible(false);
			}    		
    	});
    	element.appendChild(button);
    }
    
    
    private boolean optionIsAlreadySelected(Groupbox groupbox, String option){
    	Vector<String> options = groupbox2Vector(groupbox);
    	return options.contains(option);    	    	
    }

    private void fillTypeListbox(){
    	type.appendItem("ALL", "");    	
    	type.appendItem("Project", "");
    	type.appendItem("Calculation", "");    	
    	fillCalculationTypesFromDatabase();
    	type.setSelectedIndex(0);
    }
    
    private void fillCalculationTypesFromDatabase(){
    	for(CalculationType calcType : CalculationTypeService.getAll()) 
    		type.appendItem(calcType.getName(), String.valueOf(calcType.getId()));
    }

    @SuppressWarnings("rawtypes")
	public List<Object[]> runSearchQuery() throws Exception{        
		if(!hasSelectedCriteria()){
			Messagebox.show("Must select criteria to perform search", "Missing criteria", Messagebox.OK, "fas fa-exclamation-circle fa-3x z-div messagebox", null);
			return new ArrayList<>();
		}	
		
		String formulaField = formulaTxt.getValue().trim();
		if(!formulaField.isEmpty()) {
            formulaType = detectFormulaType(formulaTxt.getValue().trim());
            formulaSearchType = formulaSearchTypeRg.getSelectedItem().getValue().toString();
            formula = getFormula(formulaType, formulaSearchType, formulaField);
            if(formula.isEmpty()) {
                Messagebox.show("Please use a valid SMILES, InChI or InChIkey", "Invalid formula", Messagebox.OK, "fas fa-exclamation-circle fa-3x z-div messagebox", null);
                return new ArrayList<>();   
            }
		}
		
		CriteriaBuilder criteriaBuilder = SearchService.getBuilder();
		List<Object> results = SearchService.setupSearch(criteriaBuilder);		
		CriteriaQuery criteriaQuery = (CriteriaQuery)results.get(0);

		//Group filters by AND/OR condition type
        List<Predicate> andFilters = new ArrayList<>();
        List<Predicate> orFilters = new ArrayList<>();
                    
        andFilters.add((Predicate)results.get(1));  // Added join predicate        
        andFilters.add(SearchService.addUserPermissionFilters(criteriaBuilder, criteriaQuery)); // Added access permissions
        
        for(SearchService.Filters filter : SearchService.Filters.values()) {
            Predicate predicate = buildFilter(criteriaBuilder, criteriaQuery, filter);
            if(predicate != null) {
                Listbox element = (Listbox)searchDiv.query("#" + filter + "Cond");
                if(element.getSelectedItem().getLabel().equals("AND"))
                    andFilters.add(predicate);
                else
                    orFilters.add(predicate);
            }
        }
        
        Predicate partialFilters = criteriaBuilder.and(andFilters.toArray(new Predicate[andFilters.size()]));
        Predicate totalFilters = partialFilters;  
        if(!orFilters.isEmpty())
            totalFilters = criteriaBuilder.and(partialFilters, criteriaBuilder.or(orFilters.toArray(new Predicate[orFilters.size()])));
                
        criteriaQuery.where(totalFilters);
        return SearchService.launchQuery(criteriaQuery);
    }	

	private String getFormula(FORMULA_TYPE formulaType, String formulaSearchType, String formula) {
		if(formulaType == FORMULA_TYPE.INCHIKEY) {
	        return formula;
		} else if(formulaType == FORMULA_TYPE.INCHI) {
			if("contains".equals(formulaSearchType)) 
			    return FingerprintService.getSmilesFromInchi(formula);
			else if("similar".equals(formulaSearchType))
			    return FingerprintService.getConnectivitySmilesFromInchi(formula);
	        else if("exact".equals(formulaSearchType))
	        	return formula;
	    } else if(formulaType == FORMULA_TYPE.SMILES) {	        
			return FingerprintService.getCanonicalSmiles(formula);
	    }	
		return "";
	}

	private FORMULA_TYPE detectFormulaType(String formula) {
	    Pattern inchiPattern = Pattern.compile("^InChI\\=1S?\\/[^\\s]+(\\s|$)"); 
	    Pattern inchiKeyPattern = Pattern.compile("^[A-Z]{14}-[A-Z]{10}-[A-Z]{1}$");
	    if(inchiPattern.matches(formula))
	        return FORMULA_TYPE.INCHI;
	    else if(inchiKeyPattern.matches(formula))
	        return FORMULA_TYPE.INCHIKEY;
	    
	    return FORMULA_TYPE.SMILES;       
    }

    private boolean hasSelectedCriteria(){		
		Iterator<Component> textboxes = searchDiv.queryAll("textbox").iterator();
		while(textboxes.hasNext()){
			Textbox child = (Textbox) textboxes.next();
			if(!child.getValue().equals(""))
				return true;
		}			
		
		Iterator<Component> groupboxes = searchDiv.queryAll("groupbox").iterator();
		while(groupboxes.hasNext()){
			Groupbox child = (Groupbox) groupboxes.next();
			if(!child.getChildren().isEmpty())
				return true;
		}
		
		Iterator<Component> dateboxes = searchDiv.queryAll("datebox").iterator();
		while(dateboxes.hasNext()){
			Datebox child = (Datebox) dateboxes.next();
			if(child.getValue() != null)
				return true;
		}
		return false;
	}
	
	private SearchType getSearchType() {
		String type = this.type.getSelectedItem().getLabel(); 
		switch(type) {
			case "ALL":			return SearchType.PROJECT_AND_CALCULATIONS;	 
			case "Project": 	return SearchType.PROJECTS;
			case "Calculation": 
			default:			return SearchType.CALCULATIONS;		
		}			
	}
	
	private Predicate buildFilter(CriteriaBuilder builder, CriteriaQuery query, Filters field) throws Exception {	    
		XulElement element = (XulElement) searchDiv.query("#" + field);		
		if(element instanceof Textbox) { 			
			return getTextboxQuery(builder, query, field, ((Textbox)element).getText());
		} else if(element instanceof Listbox) {						
			return getTextboxQuery(builder, query, field, (String)((Listbox)element).getSelectedItem().getValue());
		} else if(element instanceof Groupbox) {			
			List<Integer> values = new ArrayList<>();
			Groupbox groupBox = ((Groupbox)element);
			groupBox.getChildren().stream().forEach(children -> values.add((Integer)((Button)children).getAttribute("value")));
			return getGroupboxQuery(builder, query, field, values);					
		} else if(element instanceof Datebox) {					
			String start = ((Datebox)element).getText();			
			String end = ((Datebox)searchDiv.query("#" + field + "End")).getText();
			return getDateQuery(builder, query, field, start, end);
		}
		return null;
	}
	
	private Predicate getTextboxQuery(CriteriaBuilder builder, CriteriaQuery query, Filters field, String value) throws Exception {
		if(value == null || value.equals(""))
			return null;
		return SearchService.addFilter(builder, query, getSearchType(), field, value.toLowerCase()); //All textual searches use lowercase
	}
	
	private Predicate getGroupboxQuery(CriteriaBuilder builder, CriteriaQuery query, Filters field, List<Integer> values) throws Exception {
		if(values == null || values.isEmpty())
			return null;
		return SearchService.addFilter(builder, query, field, values);
	}

	private Predicate getDateQuery(CriteriaBuilder builder, CriteriaQuery query, Filters field, String start, String end) throws Exception {	    		
		if((start == null || start.isEmpty()) && (end == null|| end.isEmpty())) 
		    return null;		
		return SearchService.addDateFilter(builder, query, getSearchType(), field, start, end);
	}

	public void displayResults(List<Object[]> results) throws InterruptedException, BrowseCredentialsException{    	
    	List<TreeMap<String, Entity>> entities = loadValues(results);
    	TreeMap<String, Entity> projects = entities.get(0);
    	TreeMap<String, Entity> calculations = entities.get(1);
    	
    	if(!projects.isEmpty() || !calculations.isEmpty()){
    		fillResultTree(projects, calculations);
    		navigationQueue.publish(new Event("resetHome"));
			searchDiv.setVisible(false);
			resultsDiv.setVisible(true);
    	}else
    		Messagebox.show("Current criteria did not match any result.", "No results", Messagebox.OK, Messagebox.INFORMATION);
    }

	public List<TreeMap<String, Entity>> loadValues(List<Object[]> results){
	    List<TreeMap<String, Entity>> matchingResults = new ArrayList<>();
        TreeMap<String, Entity> projects = new TreeMap<>();
        TreeMap<String, Entity> calculations = new TreeMap<>();
        matchingResults.add(projects);
        matchingResults.add(calculations);
        
        // Formula-filtering related variables
        Map<Long,String> formulaPerCalculationId = new HashMap<>();
		SmilesParser parser = null;
        SmartsPattern pattern = null;

        if(!formula.isEmpty()) {	// Filter by formula and/or element type
            try {            
                formulaPerCalculationId = getMatchingCalculations(formula, formulaType); 
				if(!"exact".equals(formulaSearchType)){
					parser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
					pattern = SmartsPattern.create(formula);
				}
            }catch(InvalidSearchOptionException e) {
                formulaTxt.setValue("");
                Messagebox.show(e.getMessage(), "Invalid setup.", Messagebox.OK, Messagebox.INFORMATION);                
                return matchingResults;
            }catch(Exception e1) {
                formulaTxt.setValue("");
                Messagebox.show("Could not process provided formula, please check its validity.", "Invalid setup", Messagebox.OK, Messagebox.INFORMATION);
                return matchingResults;
            }
        }else if(element.getChildren().size() > 0) {	// Filter by element type		
			formulaPerCalculationId = getMatchingCalculations(getElementTypes());
		}        
        try {                   
            for(Object[] result: results) {
                Entity entity;                
                long id = (long)result[0];
                String projectPath = (String)result[1];
                String projectName = (String)result[2];
                String projectDesc = (String)result[3];                
                long calcId = result.length > 4? (long)result[4]: 0;                
                String calculationPath = result.length > 4? (String)result[5]: "";
                String calculationName = result.length > 4? (String)result[6]: "";
                String calculationDesc = result.length > 4? (String)result[7]: "";
                
                if(calcId == 0) {       //Search only projects or empty projects
                    entity = new Project();
                    entity.setId(id);
                    entity.setParentPath(projectPath);
                    entity.setName(projectName);
                    entity.setDescription(projectDesc);
                    projects.put(entity.getPath(), entity);
                }else{				  //Search calculations
                    if(!isValid(calcId, formulaPerCalculationId, parser, pattern))	// Substructure search is performed here when possible
                        continue;
                    entity = new Calculation();
                    entity.setId(calcId);
                    entity.setParentPath(calculationPath);                  
                    entity.setName(calculationName);
                    entity.setDescription(calculationDesc);
                    calculations.put(entity.getPath(), entity);
                    if(!projects.containsKey(entity.getParentPath())) {
                        entity = new Project();
                        entity.setId(id);
                        entity.setParentPath(projectPath);
                        entity.setName(projectName);
                        entity.setDescription(projectDesc);
                        projects.put(entity.getPath(), entity);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return matchingResults;   
    }
	
    private Map<Long, String> getMatchingCalculations(String[] elementTypes) {		
		return FingerprintService.searchCalculationsByAtomType(elementTypes);
	}

	private Map<Long, String> getMatchingCalculations(String formula, FORMULA_TYPE type) throws Exception{
        Map<Long, String> formulaPerCalculationId;
        
        if("contains".equals(formulaSearchType) || "similar".equals(formulaSearchType)) {
                if(type == FORMULA_TYPE.INCHIKEY) 
                    throw new InvalidSearchOptionException("InChIKey must be set only with the Exact option.");   
                formulaPerCalculationId = FingerprintService.searchCalculationsBySubstructure(getElementTypes(), formula, "similar".equals(formulaSearchType));
        } else {
                formulaPerCalculationId = FingerprintService.searchCalculationsByExactFormula(formula, type);
        }
        return formulaPerCalculationId;
    }
    
    private String[] getElementTypes() {
		List<String> elements = new ArrayList<>();
		for(Component child: element.getChildren())
			elements.add((String)((Button)child).getAttribute("value"));
		return elements.toArray(new String[elements.size()]);	
	}

	private boolean isValid(long calcId,  Map<Long,String> formulaPerCalculationId, SmilesParser parser, SmartsPattern pattern) {
		String formulaForCalcId = formulaPerCalculationId.get(calcId);
        if(!formula.isEmpty()) {                           
            if("exact".equals(formulaSearchType))
                return formulaPerCalculationId.containsKey(calcId);
            else if("contains".equals(formulaSearchType) || "similar".equals(formulaSearchType)){
				if(formula.equals(formulaForCalcId))
					return true;
                if(!formulaPerCalculationId.containsKey(calcId) || !FingerprintService.contains(formulaForCalcId, parser, pattern))                            
            	    return false;
			}
        } else if(element.getChildren().size() > 0 && !formulaPerCalculationId.containsKey(calcId)){	// Filter only by element type
			return false;
		}
		return true;
    }
    
    private void fillResultTree(TreeMap<String, Entity> projects, TreeMap<String, Entity> calculations) {
    	HashMap<String, Treeitem> ownerPaths = new HashMap<>();
    	HashMap<String, Treeitem> insertedPaths = new HashMap<>();
    	
    	for(String path: projects.keySet()) {    		    		
    		Entity entity = projects.get(path);
    		String owner = entity.getParentPath().replaceAll("^/db/", "").replaceAll("/.*", "");
    		if(!ownerPaths.containsKey(owner)) { 
    			ownerPaths.put(owner, buildOwnerTreeitem(owner));
    			searchTreeChildren.appendChild(ownerPaths.get(owner));
    			
    		}    		
    		Treeitem item = buildTreeitem(entity, false);
    		item.setOpen(false);
    		insertedPaths.put(entity.getPath(), item);
    		ownerPaths.get(owner).getLastChild().appendChild(item);
    		
    	}
    	
    	for(String path: calculations.keySet()) {
    		Entity entity = calculations.get(path);
    		Treeitem item = buildTreeitem(entity, true);    		
    		if(insertedPaths.containsKey(entity.getParentPath()))
    			insertedPaths.get(entity.getParentPath()).getLastChild().appendChild(item);    		
    		else 
    			log.error("Missing parent for calculation " + path + "on search result");    		    
    	}
    }
    
    private Treeitem buildOwnerTreeitem(String owner) {
    	Treerow row = new Treerow();
		Treecell name = new Treecell(owner);
		name.setSclass("searchOwnerTreeitem");
		row.appendChild(name);
		Treeitem item = new Treeitem();
		item.appendChild(row);		
		item.appendChild(new Treechildren());
		item.setSelectable(false);		
		return item;    	
    }

    private Treeitem buildTreeitem(Entity entity, boolean isCalculation) {
        Treerow row = new Treerow();
		Treecell name = new Treecell(isCalculation ? entity.getPath().replaceAll(entity.getParentPath(), ""): entity.getPath());
		row.appendChild(name);
		row.setAttribute("properties", entity);
		Treeitem item = new Treeitem();
		item.appendChild(row);
		item.setAttribute("properties", entity);
		if(entity.isCalculation())
			row.setTooltip("thumbnailPopup, position=after_center, delay=700");
		
		if(!isCalculation)
			item.appendChild(new Treechildren());
		
		return item;
    }

    private Vector<String> groupbox2Vector(Groupbox g){
    	Vector<String> ret = new Vector<>();
    	for(Component child : g.getChildren()){
    		String username = ((Button)child).getLabel();
    		ret.add(username);
    	}
    	return ret;
    } 

    private void resetSearch(){
		formula = "";
		formulaSearchType = "";
		formulaType = FORMULA_TYPE.SMILES;

    	clearSearchForm();
    	resultsDiv.setVisible(false);
    	while(searchTreeChildren.getChildren().size() > 0)    	
    		searchTreeChildren.removeChild(searchTreeChildren.getFirstChild());
    	searchDiv.setVisible(true);
    	navigationQueue.publish(new Event("resetHome"));
    }

	private void clearSearchForm() {
		clearComponentValues(searchDiv.queryAll("textbox"), "");
		clearComponentValues(searchDiv.queryAll("datebox"), null);
		clearComponentValues(searchDiv.queryAll("listbox"), null);

		clearGroupBox(element);
		clearGroupBox(owner);
		clearGroupBox(group);
	}
	
	private void clearGroupBox(Groupbox groupBox) {
		while(groupBox.getFirstChild() != null)
    		groupBox.getFirstChild().detach();
		groupBox.setVisible(false);
	}

	private <T extends Component> void clearComponentValues(Iterable<T> components, Object value) {
		for (T component : components) {
			if (component instanceof Textbox) {
				((Textbox) component).setValue((String) value);
			} else if (component instanceof Datebox) {
				((Datebox) component).setValue((Date) value);
			} else if (component instanceof Listbox) {
				((Listbox) component).setSelectedIndex(0);
			}
		}
	}
}

class InvalidSearchOptionException extends Exception {

    private static final long serialVersionUID = 1L;
    
	public InvalidSearchOptionException(String message) {
        super(Objects.requireNonNull(message, "Message cannot be null"));
	} 
}

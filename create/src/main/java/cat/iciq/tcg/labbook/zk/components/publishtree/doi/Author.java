/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree.doi;

public class Author {
	private String givenName;
	private String surname;
	private String role;
	private String sequence;
	
	
	public Author(String givenName, String surname, String role, String sequence){
		this.givenName = givenName;
		this.surname = surname;
		this.role = role;
		this.sequence = sequence;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"givenName\" : \"" + DoiRequest.escapeJson(getGivenName())  +"\",");
		sb.append("\"surname\" : \"" + DoiRequest.escapeJson(getSurname())  +"\",");
		sb.append("\"role\" : \"" + DoiRequest.escapeJson(getRole())  +"\",");
		sb.append("\"sequence\" : \"" + DoiRequest.escapeJson(getSequence())  +"\"");
		sb.append("}");
		return sb.toString();
	}	
	
}
/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;

import java.util.List;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;


import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationType;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class SearchService {
	
	private static final Logger log = LogManager.getLogger(SearchService.class);
	
	public enum SearchType { PROJECT_AND_CALCULATIONS, PROJECTS, CALCULATIONS}; 
	    
	public enum Filters {name, description, type, path, owner, group, creationDate, conceptGroup}; // Smiles filter is processed after the initial search to improve performance 
	
    public static final boolean SEARCH_ON_OTHERS_PERMISSION = Boolean.parseBoolean(CustomProperties.getProperty("search.display.others", "true"));

	
	public static CriteriaBuilder getBuilder() {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        return session.getCriteriaBuilder();
	}
	
	public static List<Object> setupSearch(CriteriaBuilder criteriaBuilder) throws Exception {
	    List<Object> result = new ArrayList<>();
	    CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);	    
	    	  
        Root<Project> projectRoot = criteriaQuery.from(Project.class);
        Root<Calculation> calculationRoot = criteriaQuery.from(Calculation.class);

        Expression<String> projectPath = projectRoot.get("path");
        Expression<String> delimiter = criteriaBuilder.literal("/");
        Expression<String> projectName = projectRoot.get("name");
        
        Expression<String> projectFullPath = criteriaBuilder.function(
                "concat",
                String.class,
                projectPath,
                delimiter,
                projectName
        );
        Expression<String> calculationPath = calculationRoot.get("path");
        
        Predicate joinCondition = criteriaBuilder.equal(projectFullPath, calculationPath);
        // Define the select clause
        criteriaQuery.multiselect(
            projectRoot.get("id"),
            projectRoot.get("path"),
            projectRoot.get("name"),
            projectRoot.get("description"),
            calculationRoot.get("id"),
            calculationRoot.get("path"),
            calculationRoot.get("name"),
            calculationRoot.get("description")
        );
        result.add(criteriaQuery);
        result.add(joinCondition);
	   
	    return result;
	}
	
	public static Predicate addUserPermissionFilters(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery) throws BrowseCredentialsException {	   
	    Root<Project> root = (Root<Project>) criteriaQuery.getRoots().iterator().next();	    
	    // Constructing the initial part of the query: permission filters
        // User level permissions
	    
        Predicate readPermissionOnOwner = criteriaBuilder.equal(root.get("owner"),UserService.getUserId());
	    
	    // Group level permissions
        Expression<List<Integer>> userGroups = criteriaBuilder.literal(getBelongedUserGroupIds());
        Expression<List<BitSet>> userGroupExpr = criteriaBuilder.literal(getGroupMask());
        Predicate readPermissionOnGroup = criteriaBuilder.and(
                criteriaBuilder.in(root.get("group")).value(userGroups),
                criteriaBuilder.in(root.get("permissions")).value(userGroupExpr));
        
        // Others level permissions 
        if(SEARCH_ON_OTHERS_PERMISSION) {
            Expression<List<BitSet>> userOtherExpr = criteriaBuilder.literal(getOthersMask());
            Predicate readPermissionOnOther = criteriaBuilder.in(root.get("permissions")).value(userOtherExpr);            
            return criteriaBuilder.or(readPermissionOnOwner, readPermissionOnGroup, readPermissionOnOther);    
        }
        return criteriaBuilder.or(readPermissionOnOwner, readPermissionOnGroup);
	}
		
	public static Predicate addFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, SearchType type, Filters field, String value) throws Exception {
	    Root[] roots = (Root[]) criteriaQuery.getRoots().toArray(new Root[criteriaQuery.getRoots().size()]);	  
        Predicate predicate = null;
        if(field == Filters.name || field == Filters.description || field == Filters.path) {
            Predicate projectPredicate = criteriaBuilder.like(criteriaBuilder.lower(roots[0].get(field.name())), "%" + value.toLowerCase() + "%" );
            Predicate calculationPredicate = criteriaBuilder.like(criteriaBuilder.lower(roots[1].get(field.name())), "%" + value.toLowerCase() + "%" );            
            if(SearchType.PROJECT_AND_CALCULATIONS == type) {                      
                predicate = criteriaBuilder.or(projectPredicate, calculationPredicate);
            }else if(SearchType.PROJECTS == type) {
                predicate = projectPredicate;
            }else {
                predicate = calculationPredicate;
            }        
	    } else if(field == Filters.type && SearchType.PROJECTS != type) {
	        CalculationType calcType = CalculationTypeService.getCalculationTypeById(Integer.parseInt(value));
	        predicate = criteriaBuilder.equal(roots[1].get(field.name()), calcType);
	    }else if(field == Filters.conceptGroup && (SearchType.PROJECT_AND_CALCULATIONS == type || SearchType.PROJECTS == type)) {	        	    
	        predicate = criteriaBuilder.equal(criteriaBuilder.lower(roots[0].get(field.name())),value.toLowerCase());
	    }
        return predicate;
    }
	
	public static Predicate addFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Filters field, List<Integer>values) throws Exception { 
	    Root[] roots = (Root[]) criteriaQuery.getRoots().toArray(new Root[criteriaQuery.getRoots().size()]);
	    Expression<List<Integer>> ids = criteriaBuilder.literal(values);
        return criteriaBuilder.in(roots[0].get(field.name())).value(ids);        
	}
	
	public static Predicate addDateFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, SearchType type, Filters field, String start, String end) throws Exception {	    
	    Root[] roots = (Root[]) criteriaQuery.getRoots().toArray(new Root[criteriaQuery.getRoots().size()]);
	    Predicate predicateProjects = null;
	    Predicate predicateCalculations = null;
	    
	    String pattern = "yyyy/MM/dd HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
	       
	    if(start != null && !start.equals("")) {	        
	        Date startDate = dateFormat.parse(start);
	        Timestamp startTimestamp = new Timestamp(startDate.getTime());
            if(end != null && !end.equals("")) {                
                Date endDate = dateFormat.parse(end);                
                Timestamp endTimestamp = new Timestamp(endDate.getTime());
                predicateProjects = criteriaBuilder.between(roots[0].get(field.name()), startTimestamp, endTimestamp);                            
                predicateCalculations = criteriaBuilder.between(roots[1].get(field.name()), startTimestamp, endTimestamp);
            }else{
                predicateProjects = criteriaBuilder.greaterThanOrEqualTo(roots[0].get(field.name()), startTimestamp);                            
                predicateCalculations = criteriaBuilder.greaterThanOrEqualTo(roots[1].get(field.name()), startTimestamp);
            }
        }else if(end != null && !end.equals("")){
            Date endDate = dateFormat.parse(end);                
            Timestamp endTimestamp = new Timestamp(endDate.getTime());
            predicateProjects = criteriaBuilder.lessThanOrEqualTo(roots[0].get(field.name()), endTimestamp);
            predicateCalculations = criteriaBuilder.lessThanOrEqualTo(roots[1].get(field.name()), endTimestamp);
        }
	    
	    return getDatePredicate(criteriaBuilder, type, predicateProjects, predicateCalculations);
    }

    private static Predicate getDatePredicate(CriteriaBuilder criteriaBuilder, SearchType type, Predicate projectPredicate, Predicate calculationPredicate) {
        Predicate predicate = null;
        if(SearchType.PROJECT_AND_CALCULATIONS == type) {                      
            predicate = criteriaBuilder.or(projectPredicate, calculationPredicate);
        }else if(SearchType.PROJECTS == type) {
            predicate = projectPredicate;
        }else {
            predicate = calculationPredicate;
        }
        return predicate;
    }

    public static List<BitSet> getUserMask() {
        List<BitSet> groupIds = new ArrayList<>();
        groupIds.add(BitSet.valueOf(new long[] {1L}));
        groupIds.add(BitSet.valueOf(new long[] {3L}));        
        return groupIds;
    }
    
    public static List<BitSet> getGroupMask() {
        List<BitSet> groupIds = new ArrayList<>();
        groupIds.add(BitSet.valueOf(new long[] {7L}));
        groupIds.add(BitSet.valueOf(new long[] {15L}));
        return groupIds;
    }
    
    public static List<BitSet> getOthersMask() {
        List<BitSet> groupIds = new ArrayList<>();
        groupIds.add(BitSet.valueOf(new long[] {31L}));
        groupIds.add(BitSet.valueOf(new long[] {63L}));        
        return groupIds;
    }

    public static List<Integer> getBelongedUserGroupIds() throws BrowseCredentialsException {
        List<Integer> groupIds = new ArrayList<>();
        for(String id: UserService.getUserGroups())
            groupIds.add(Integer.parseInt(id));
        return groupIds;
    }

    public static List<Object[]> launchQuery(CriteriaQuery criteriaQuery) throws BrowseCredentialsException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query<Object[]> query = session.createQuery(criteriaQuery);
        return query.getResultList();
    }

    public static List<Integer> getGroupUsers() {
        if(SEARCH_ON_OTHERS_PERMISSION)
            return getGroupAndOtherUsers();
        else
            return getGroupUsersOnly();    
    }
    
    private static List<Integer> getGroupUsersOnly() {
        List<Integer> ownerUserIds = new ArrayList<>();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Integer> criteriaQuery = criteriaBuilder.createQuery(Integer.class);
            Root<Project> projectRoot = criteriaQuery.from(Project.class);

            Expression<Integer> ownerUserIdExpression = projectRoot.get("owner");
            Expression<Integer> ownerGroupIdExpression = projectRoot.get("group");
            Expression<BitSet> permissionsExpression = projectRoot.get("permissions");

            Predicate groupPredicate = ownerGroupIdExpression.in(getUserGroupIds());
            Predicate permissionsPredicate = permissionsExpression.in(getGroupMask());
            criteriaQuery.select(ownerUserIdExpression)
                    .distinct(true)
                    .where(criteriaBuilder.and(groupPredicate, permissionsPredicate));
    
            ownerUserIds = session.createQuery(criteriaQuery).getResultList();
        }catch(Exception e) {
            log.error(String.format("An error raised while retrieving user groups. %s", e.getMessage()));
        }
        return ownerUserIds;
    }

    private static List<Integer> getGroupAndOtherUsers() {
        List<Integer> ownerUserIds = new ArrayList<>();

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Integer> criteriaQuery = criteriaBuilder.createQuery(Integer.class);
            Root<Project> projectRoot = criteriaQuery.from(Project.class);

            Expression<Integer> ownerUserIdExpression = projectRoot.get("owner");
            Expression<Integer> ownerGroupIdExpression = projectRoot.get("group");
            Expression<BitSet> permissionsExpression = projectRoot.get("permissions");

            Predicate groupPredicate = ownerGroupIdExpression.in(getUserGroupIds());
            Predicate permissionsPredicate = permissionsExpression.in(getGroupMask());
            Predicate condition1 = criteriaBuilder.and(groupPredicate, permissionsPredicate);            
            Predicate condition2 =  permissionsExpression.in(getOthersMask());           
            Predicate finalCondition = criteriaBuilder.or(condition1, condition2);
           
            criteriaQuery.select(ownerUserIdExpression)
                    .distinct(true)
                    .where(finalCondition);
    
            ownerUserIds = session.createQuery(criteriaQuery).getResultList();
        }catch(Exception e) {
            log.error(String.format("An error raised while retrieving user groups. %s", e.getMessage()));
        }
        return ownerUserIds;        
    }

    private static List<Integer> getUserGroupIds() throws NumberFormatException, BrowseCredentialsException{
        List<Integer> groupIds = new ArrayList<>();       
        for(String groupId: UserService.getUserGroups())
            groupIds.add(Integer.parseInt(groupId));    
        return groupIds;
    }
}
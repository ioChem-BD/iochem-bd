package cat.iciq.tcg.labbook.web.cas;

import org.pac4j.core.context.CallContext;
import org.pac4j.core.matching.matcher.Matcher;

public class CustomMatcher implements Matcher {

    @Override
    public boolean matches(CallContext ctx) {
        ctx.webContext().setResponseHeader("X-Frame-Options", "SAMEORIGIN");
        ctx.webContext().setResponseHeader("X-Content-Type-Options", "");
        return true;        
    }
}
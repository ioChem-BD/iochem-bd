/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.Constants;

public class DCalc extends HibernateService {
    
	private static final long serialVersionUID = 1L;
	
    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try {
            params.put(Constants.LOCAL_APP_DIR, FileService.getCreatePath() + File.separatorChar);
            String path = params.get(PARAMETER_NEWPATH);
            Calculation calculation = CalculationService.getByPath(path);			
            if(calculation != null) { 
                CalculationService.delete(calculation.getId(), true); 
                returnOK(response);
            } else {
                returnKO(response, CALCULATION_ERROR, BAD_PATH_ERROR);
            }		
        } catch(Exception e) {		
            returnKO(response, CALCULATION_ERROR, e.getMessage());			
        }        
    }
}

package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ErrorResponse {

    private int status;
    private String error;
    private String message;

    public ErrorResponse(int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void sendErrorResponse(HttpServletResponse httpResponse) throws IOException {
        httpResponse.setStatus(status);
        httpResponse.setContentType("application/json");

        PrintWriter writer = httpResponse.getWriter();
        writer.write(new ObjectMapper().writeValueAsString(this));
        writer.flush();
    }
}

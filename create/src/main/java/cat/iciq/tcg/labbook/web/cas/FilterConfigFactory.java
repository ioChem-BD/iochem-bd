package cat.iciq.tcg.labbook.web.cas;

import java.util.Optional;

import org.pac4j.cas.client.CasClient;
import org.pac4j.cas.client.rest.CasRestBasicAuthClient;
import org.pac4j.cas.config.CasConfiguration;
import org.pac4j.cas.config.CasProtocol;
import org.pac4j.core.authorization.authorizer.RequireAnyRoleAuthorizer;
import org.pac4j.core.client.Clients;
import org.pac4j.core.client.direct.AnonymousClient;
import org.pac4j.core.config.Config;
import org.pac4j.core.config.ConfigFactory;
import org.pac4j.core.context.HttpConstants;

import cat.iciq.tcg.labbook.web.utils.CustomProperties;

public class FilterConfigFactory implements ConfigFactory {

    private static final String URL_SCHEME = "https://";
    private static final String LOGIN_PATH = "/login";
    private static final String CALLBACK_PATH = "/callback";

    private static final long TIME_TOLERANCE = 3600000;

    @Override
    public Config build(Object... parameters) {
        CasConfiguration casConfig = new CasConfiguration();
        casConfig.setLoginUrl(getCasLoginUrl());
        casConfig.setProtocol(CasProtocol.CAS30);
        casConfig.setTimeTolerance(TIME_TOLERANCE);

        CasRestBasicAuthClient casBasicAuthClient = new CasRestBasicAuthClient(
            casConfig, 
            HttpConstants.AUTHORIZATION_HEADER, 
            HttpConstants.BASIC_HEADER_PREFIX);

        casBasicAuthClient.setName("CasRestClient");
        casBasicAuthClient.setAuthorizationGenerator((ctx, profile) -> { profile.addRole("ROLE_USER"); return Optional.of(profile); });
        casBasicAuthClient.setSaveProfileInSession(false);

        CasClient casClient = new CasClient(casConfig);
        casClient.setName("CasClient");
        casClient.setAuthorizationGenerator((ctx, profile) -> { profile.addRole("ROLE_USER"); return Optional.of(profile); });
        
        AnonymousClient anonymousClient = new AnonymousClient();
        anonymousClient.setName("AnonymousClient");
        
        Clients clients = new Clients(getCreateCallbackUrl(), casBasicAuthClient, casClient, anonymousClient);

        Config config = new Config(clients);
        config.addAuthorizer("user", new RequireAnyRoleAuthorizer("ROLE_USER"));

        CustomMatcher customMatcher = new CustomMatcher();
        config.addMatcher("customMatcher", customMatcher);
        return config;
    }

    public static String getCasLoginUrl() { 
        StringBuilder casLoginUrl = new StringBuilder();
        casLoginUrl.append(URL_SCHEME);
        casLoginUrl.append(CustomProperties.getProperty("cas.server.hostname", "localhost"));                
        String basePort = CustomProperties.getProperty("cas.server.port", "");
        String portDelimiter = basePort.isEmpty() ? "" : ":";
        casLoginUrl.append(portDelimiter);
        casLoginUrl.append(basePort);
        casLoginUrl.append("/");
        casLoginUrl.append(CustomProperties.getProperty("cas.server.app", "cas"));
        casLoginUrl.append(LOGIN_PATH);
        return casLoginUrl.toString();
    }

    private String getCreateCallbackUrl() {
        StringBuilder callbackUrl = new StringBuilder();
        callbackUrl.append(URL_SCHEME);
        callbackUrl.append(CustomProperties.getProperty("create.server.hostname", "localhost"));
        String basePort = CustomProperties.getProperty("create.server.port", "");
        String portDelimiter = basePort.isEmpty() ? "" : ":";
        callbackUrl.append(portDelimiter);
        callbackUrl.append(basePort);
        callbackUrl.append("/");
        callbackUrl.append(CustomProperties.getProperty("create.server.app", "create"));
        callbackUrl.append(CALLBACK_PATH);
        return callbackUrl.toString();
    }
}
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.util.HashMap;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;

public class ReportReactionGraphIframe extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;

	private EventQueue<Event> reportCommunicationQueue = null;
	protected static String uuid;

    @Wire
    Label childrenLbl;


    @Listen("onNotifyGraphChanged=#customReportWindowIframe")
    public void notifyGraphChanged(Event event) {
    	String jsonGraph = (String)event.getData();
    	
    	reportCommunicationQueue.publish(new Event("graphChanged", null, jsonGraph));
    }

    @Listen("onDeleteReportCalculation=#customReportWindowIframe")
    public void deleteReportCalculation(Event event) {
    	long calculationId = Long.parseLong((String)event.getData());
    	reportCommunicationQueue.publish(new Event("deleteReportCalculation", null, calculationId));
    }

    @Listen("onClearGraphJson=#customReportWindowIframe")
    public void clearGraphJson(Event event) {
    	reportCommunicationQueue.publish(new Event("clearGraphJson", null, 1));
    }
    
    @Listen("onSetupReport=#customReportWindowIframe")
    public void setupReport(Event event) {

    	String uuid = (String) event.getData();

        reportCommunicationQueue = EventQueues.lookup("navigation-" + uuid, EventQueues.SESSION, true);
        reportCommunicationQueue.publish(new Event("requestListCalculationsReport", null, null));
        reportCommunicationQueue.subscribe(new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (event.getName().equals("isCalculationRemovable")) {
                    long calculationId = (long) event.getData();
                	Clients.evalJavaScript("isCalculationRemovable("+calculationId+")");
                }
                if(event.getName().equals("addCalculationToReactionGraph")) {
               	 ReportCalculation calculationReport = (ReportCalculation)event.getData();
                 Calculation calculation = CalculationService.getByPath(calculationReport.getCalcPath());
                    Clients.evalJavaScript("\t\t\t\torderedFormulas = []\n" + //
                            "\t\t\t\tfillArrayFormulasOrdered()");
                    Clients.evalJavaScript("if(formulaIsUnique('+"+calculationReport.getCalculation().getId()+"')){ "
                    +"nodeCY.addNode('"+calculation.getName()+"', '"+calculationReport.getCalculation().getId()+"', 300, 300);"
                    +"applyStylesJson();"                    
                    +"}");
                }
                if(event.getName().equals("selectAllNodesWithThisCalculationId")) {
               	 ReportCalculation calculationReport = (ReportCalculation)event.getData();
                 Calculation calculation = CalculationService.getByPath(calculationReport.getCalcPath());
                 Clients.evalJavaScript("selectAllNodesWithThisCalculationId("+calculation.getId()+")");
                }
                if(event.getName().equals("downloadJSON")) {
                	Clients.evalJavaScript("downloadJSON();"); 
                }
                if(event.getName().equals("focusGraph")) {
            		Clients.evalJavaScript("focusViewCanva();");
                }
                if(event.getName().equals("clearGraph")) {
           		 Clients.evalJavaScript("clearGraphCanva();");
                }
                if(event.getName().equals("showEdgesName")) {
           		 Clients.evalJavaScript("showFormulas('true')");
                }
                if(event.getName().equals("confCorrectBtn")) {
                    Object[] values = (Object[]) event.getData();
                    String energyType = (String) values[0];
                    HashMap<String, Double> energiesFiltred = (HashMap<String, Double>) values[1];
                    String energies = energiesFiltred.toString();
                    Clients.evalJavaScript("calculateTotalEnergies('"+energies+ "TYPE::" + energyType+"')");                   
                    Clients.evalJavaScript("confCorrectEnergy()");
                }
                if(event.getName().equals("loadJsonGraph")) {
                    Clients.evalJavaScript("cy.json(sanitizeJsonData("+event.getData()+"));");
                    Clients.evalJavaScript("changeStyleOfThegraph();"); 
                }
                if(event.getName().equals("setCalculationsList")) {
                    Clients.evalJavaScript("setCalculationsList('"+event.getData()+"')");
                }
                if(event.getName().equals("setTSCalculations")) {
                    Clients.evalJavaScript("setTSCalculations('"+event.getData()+"')");
                }
                if(event.getName().equals("showFormulas")) {
                    Clients.evalJavaScript("showFormulas('"+event.getData()+"')");
                }
                if(event.getName().equals("showEdges")) {                   
                    Clients.evalJavaScript("showEdges = "+event.getData());
                    Clients.evalJavaScript("showFormulas()");
                }
            }
        });

    }
    
    @Listen("onClick=#messageParentBtn")
    public void onMessageParentBtnClick() {
        reportCommunicationQueue.publish(new Event("getFromChildren"));        
    }
    
    @Override
    public void doAfterCompose(Window window) {
        try {
            super.doAfterCompose(window);    
            Clients.evalJavaScript("setupReport()");
            Clients.evalJavaScript("setupCytoscape()");
        } catch (Exception e) {
            e.printStackTrace();
        }       
                      
    }    
}

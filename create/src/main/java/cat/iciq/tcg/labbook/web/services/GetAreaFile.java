/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;

public class GetAreaFile extends HibernateService {

    private static final long serialVersionUID = 1L;

    private static final Long THRESHOLD = (Long) WebApps.getCurrent().getAttribute("deferredContentThreshold");
    private static final int BUFFER_SIZE = 1024;

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        String path = params.get(PARAMETER_NEWPATH);
        Calculation calculation = CalculationService.getByPath(path);
        if (calculation == null) {
            returnKO(response, CALCULATION_ERROR, CALC_NON_EXIST_ERROR);
            return;
        }
        if (!PermissionService.hasPermissionOnCalculation(calculation.getId(), Permissions.READ)) {
            returnKO(response, PERMISSION_ERROR, CALC_READ_ERROR);
            return;
        }

        List<CalculationFile> files = CalculationService.getCalculationFiles(calculation.getId());
        String calcFile = params.get(PARAMETER_CALCFILE).replaceAll("assetstore/[0-9]+/", "");
        try {
            for (CalculationFile file : files) {
                if (file.getName().equals(calcFile)) {
                    sendFile(response, FileService.getCreatePath("/" + file.getFile()));
                    break;
                }
            }
        } catch (IOException e) {
            returnKO(response, "title",
                    "File exceeds automated download size. Please download current file using the web interface.");
        } catch (Exception e) {
            returnKO(response, "title", "Could not retrieve calculation file.");
        }
    }

    private void sendFile(HttpServletResponse response, String file) throws IOException {
        if (THRESHOLD > -1L && new File(file).length() > THRESHOLD)
            throw new IOException("Maximum file size reached. This file must be retrieved using the web interface.");

        try (FileInputStream instream = new FileInputStream(file);
                BufferedInputStream reader = new BufferedInputStream(instream);
                BufferedOutputStream writer = new BufferedOutputStream(response.getOutputStream());) {

            int read = 0;
            byte[] buffer = new byte[BUFFER_SIZE];
            while (-1 != (read = reader.read(buffer, 0, BUFFER_SIZE))) {
                writer.write(buffer, 0, read);
            }
        } finally {
            response.getOutputStream().close();
        }
    }
}

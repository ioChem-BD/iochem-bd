/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.uploadtoolbar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationType;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.AssetstoreService;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.CalculationTypeService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.exceptions.ExtractionException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.web.utils.InchiGenerator;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class CalculationInsertion implements Callable<Map<String, String>> {

	private static final Logger logger = LogManager.getLogger(CalculationInsertion.class.getName());

	public static Templates geometryExtractionTemplate;
	
	private Map<String,String> params = null;
	private List<File> fileItems = null;	
	
	public static final String VASP_CALCULATION_TYPE= "Vasp"; 
	public static final String METS_FILE_NAME		= "mets.xml";
	public static final String THUMBNAIL_FILE_NAME 	= "thumbnail.jpeg";
	public static final String GEOMETRY_FILE_NAME 	= "geometry.cml";
	public static final String NO_THUMBNAIL_LOCAL_PATH 	="/images/thumbnail.jpeg";
	public static final String JMOLDATA_LOCAL_PATH 	="/html/xslt/jmol/JmolData.jar";	
	public static final String THUMBNAIL_SCRIPT_LOCAL_PATH 	= "/html/xslt/jmol/thumbnail.spt";
	public static final String GEOMETRY_SCRIPT_LOCAL_PATH 	= "/html/xslt/jmol/geometry.spt";	
	
	public static final String PARAM_CALC_ID = "calcId";
	public static final String PARAM_NAME = "name";
    public static final String PARAM_DESC = "description";
    public static final String PARAM_PATH = "path";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_AUTOPUBLISH = "autopublish";
    public static final String PARAM_USERNAME = "userLogin";
    public static final String PARAM_USERID = "userID";
    public static final String PARAM_UUID = "UUID";
    public static final String PARAM_EXCEPTION_TITLE = "exceptionTitle";
    public static final String PARAM_EXCEPTION_MESSAGE = "exceptionMessage";

    
    private static final String PROJECT_ERROR = "Project Operation Error";
    private static final String WRITE_PERMISSION_ERROR = "User doesn't have write permission to the resource. Operation aborted.";
    private static final String BAD_PATH_ERROR = "Calculation path error. Operation aborted.";
    private static final String CALCULATION_ERROR = "Error on the calculation operation.";
    private static final String CALC_LONG_PATH_ERROR = "Project path exceeds maximum length (256)";
    private static final String CALC_ALREADY_EXIST_ERROR = "Calculation already exist. Calculation not inserted.";
    private static final String CALC_IO_ERROR= "There was an Input/Output error. Calculation not inserted.";
    
	static{		
		try {
			FileInputStream fis = new FileInputStream(WebApps.getCurrent().getRealPath("/WEB-INF/extract-geometry.xsl"));
			geometryExtractionTemplate = new net.sf.saxon.TransformerFactoryImpl().newTemplates(new StreamSource(fis));
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
    

	
	public CalculationInsertion(Map<String,String> params, List<File> fileItems){	
		this.params = params;
		this.fileItems = fileItems;
	}
	
	@Override
	public Map<String, String> call() throws Exception {
		insertCalculation();					
		return params;
	}

	/* Can't verify the user uploading the content by the current session, so username parameter will be used */
    private void insertCalculation() throws Exception {
    	String userName = params.get(PARAM_USERNAME);
    	String path = params.get(PARAM_PATH);
    	long calcId = 0;
    	String directoryPath = null;
		Session session = null;		
		Transaction transaction = null;		
    	try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();

			if(canWriteInPath(path, userName)){				
				calcId = saveCalculation(params);
				params.put(PARAM_CALC_ID, String.valueOf(calcId));
				directoryPath = createDirectory(calcId);
				saveFiles(userName, fileItems, directoryPath, calcId);
				setInchiFormulaOnOutputFile(calcId);
				extractGeometry(calcId);
				buildThumbnail(calcId);
				transaction.commit();
			}else{				
				throw new Exception("Invalid destination path.");
			}			
        }catch(Exception e){			
			if (transaction != null) transaction.rollback();
			AssetstoreService.deleteCalculation(calcId);
            FileUtils.deleteDirectory(new File(params.get(Constants.LOAD_CALC_TMP_DIR)));
        	if(params.containsKey(Constants.LOAD_CALC_WEB)){   // Is web upload
            	ExtractionException extExc = new ExtractionException();
    			extExc.setMessage(e.getMessage());
    			extExc.setThreadUUID(params.get("UUID"));
    			extExc.setUser(params.get("userLogin"));
    			throw extExc;
        	}else {
        	    throw e;
        	}
    	} finally {
			if (session != null) session.close(); 
		}
    	if(params.containsKey(Constants.LOAD_CALC_TMP_DIR))
    		FileUtils.deleteDirectory(new File(params.get(Constants.LOAD_CALC_TMP_DIR)));
    }

	private boolean canWriteInPath(String path, String userName) throws IOException, SQLException, BrowseCredentialsException {
	    if(path.length() > 256){
            logger.error(CALCULATION_ERROR + ": " + CALC_LONG_PATH_ERROR);
            return false;
        }
	    // Check valid destination
	    Project project = ProjectService.getByPath(path);
	    if(project == null) {
	        logger.error(CALCULATION_ERROR + ": " + BAD_PATH_ERROR);
            return false;
	    }
	    // Check valid permissions on target project			
	    if(!PermissionService.isOwner(project, userName)) {
	        logger.error(PROJECT_ERROR + " " + WRITE_PERMISSION_ERROR);
            return false;
	    }
		return true;
    }


	private void setInchiFormulaOnOutputFile(long id) {    	    
    	File outputFile = new File(CalculationService.getOutputFilePath(id));
    	new InchiGenerator().addInchi(outputFile);
    }
    
	public void extractGeometry(long calcId) throws Exception {				
		String geometryFile = FileService.getCalculationPath(calcId, CalculationInsertion.GEOMETRY_FILE_NAME);		
		Transformer transformer;
		try {
			transformer = geometryExtractionTemplate.newTransformer();
			StreamSource xml = new StreamSource(new File(CalculationService.getOutputFilePath(calcId)));
			StringWriter writer = new StringWriter();
	        StreamResult result = new StreamResult(writer);
	        transformer.transform(xml, result);
	        FileUtils.writeStringToFile(new File(geometryFile), writer.toString());
		} catch (TransformerConfigurationException e) {
			throw new Exception("Bad geometry extraction configuration setup. Please contact your ioChem-BD administrator.");			
		} catch (IOException e) {
			throw new Exception("Could not store extracted geometry file. Please contact your ioChem-BD administrator.");
		} catch (TransformerException e) {
			throw new Exception("Could not extract geometry from uploaded calculation.");
		}		
	}
        
    private void buildThumbnail(long id) throws Exception  {    	
      	String jmolDataJar = FileService.getCreateWebapp(JMOLDATA_LOCAL_PATH);
    	String thumbnailFile = FileService.getCalculationPath(id, THUMBNAIL_FILE_NAME);    	    	
    	String geometryFile = FileService.getCalculationPath(id, GEOMETRY_FILE_NAME);    	
    	String thumbnailScriptFilePath 	= FileService.getCreateWebapp(THUMBNAIL_SCRIPT_LOCAL_PATH);  
    	
        ArrayList<String> commandList = new ArrayList<>();
    	String[] commands;	    	           
		commandList.add("java");
		commandList.add("-Djava.awt.headless=true");          
		commandList.add("-jar");
		commandList.add(jmolDataJar);
		commandList.add("-i");		  
		commandList.add("-L");
		commandList.add("-n");   
		commandList.add("-s");
		commandList.add(thumbnailScriptFilePath);
		commandList.add("-g500x500");
		commandList.add(geometryFile);
		commandList.add("-w");
		commandList.add("JPG:" + thumbnailFile);
		commands = new String[commandList.size()];
		commandList.toArray(commands);
		try{
		    Process child = Runtime.getRuntime().exec(commands);
		    InputStream is = child.getInputStream();
		    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    String aux = br.readLine();
		    logger.info("CML thumbnail creation command:" + commandList.toString());              
		    while (aux != null) 			                  
		        aux = br.readLine();			
		}catch (Exception e){
			logger.error("Error parsing CML document to extract thumbnail." + e.getMessage());
		}   
		//If thumbnail was not properly generated, we'll use "thumbnail not available" placeholder
		File thumbnail = new File(thumbnailFile);
		if(!thumbnail.exists())
			try {			    
				FileUtils.copyFile(new File(FileService.getCreateWebapp(NO_THUMBNAIL_LOCAL_PATH)), thumbnail);
			} catch (IOException e) {
				logger.error("Error copying noThumbnail image into calculation folder. " + e.getMessage());
    	}    	    
    }

    private long saveCalculation(Map<String,String> vars) throws Exception{
    	String name = HibernateService.truncate(vars.get(PARAM_NAME),64);
    	String path = vars.get(PARAM_PATH);
    	String description = HibernateService.truncate(vars.get(PARAM_DESC),304);    	
    	CalculationType calcType = CalculationTypeService.getCalculationTypeByName(vars.get(PARAM_TYPE));    	
    	return CalculationService.add(path, name, description, calcType.getId());    
    }
    
    private String createDirectory(long calcId) throws Exception {        
        String path = FileService.getCalculationPath(calcId);
		new File(path).mkdir();
		return path;
    }

    private void saveFiles(String username, List<File> fileItems, String filesStorage, long calcId) throws Exception {        
		try {
		    Iterator<File> i = fileItems.iterator();
		    while(i.hasNext()){              
	            saveFile(username, calcId, i.next());
	        }   
		}catch(SQLException e){
		    logger.error(e.getMessage());
            throw new Exception(CALC_ALREADY_EXIST_ERROR);          
		}catch(Exception e) {
		    logger.error(e.getMessage());
            throw new Exception(CALC_IO_ERROR);
		}
    }
    
    private void saveFile(String username, long calcId, File file) throws Exception {                       
        String filename = file.getName(); 
        if(filename != null) {
            if(filename.equals(METS_FILE_NAME))
                storeMetsMetadata(username, calcId, file);
            else
                storeCalculationFile(calcId, filename, file);
        }
    }

    /**
     * METS metadata file is store in database after assigning calculation ID inside its content
     * @param obj Object that holds the METS file, can be a File or a FileItem either
     * @throws Exception 
     */
    private void storeMetsMetadata(String username, long calcId, File file) throws Exception {        
        MetsFileHandler metsFile = new MetsFileHandler(Files.readAllBytes(java.nio.file.Paths.get(file.getAbsolutePath())));
        metsFile.setCalcID(calcId);

        Calculation calculation = CalculationService.getById(calcId);
        calculation.setMetsXml(metsFile.toString());
        CalculationService.update(false, username, calculation, calculation.getParentPath());
    }

    private void storeCalculationFile(long calcId, String filename, File file) throws Exception {
        File outFile = AssetstoreService.getCalculationFileByName(calcId, filename);
        Files.move(file.toPath(), outFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        CalculationService.addCalculationFile(calcId, filename);
    }
    
    /**
         private void appendItemMetadata(HashMap<String,String> publishParams) throws BrowseCredentialsException{
        publishParams.put("metadata1#dc:type", "dataset");
        publishParams.put("metadata2#dc:contributor.dcterms:author", UserService.getUserFullName());      
        publishParams.put("metadata3#dc:publisher", CustomProperties.getProperty("mets.institution.name"));             
        publishParams.put("metadata4#dc:rights", "CC BY 4.0 (c) " +  CustomProperties.getProperty("mets.institution.name") + ", " +   Calendar.getInstance().get(Calendar.YEAR));
        publishParams.put("metadata5#dc:rights.dcterms:URI", "http://creativecommons.org/licenses/by/4.0/");
        publishParams.put("metadata6#dcterms:accessRights", "info:eu-repo/semantics/openAccess");

        if(COVERAGE_SPATIAL_LOCATION == null)
            COVERAGE_SPATIAL_LOCATION = getLocationFromHttpsCertificate();
        
        if(!COVERAGE_SPATIAL_LOCATION.equals(""))
            publishParams.put("metadata" + publishParams.size() +  "#dcterms:spatial", COVERAGE_SPATIAL_LOCATION);      
    }
    
    private String getLocationFromHttpsCertificate(){
        StringBuilder sb = new StringBuilder();
        try {
            X509Certificate cert = (X509Certificate) RestManager.getCurrentCertificate();       
            String dn = cert.getSubjectX500Principal().getName();
            LdapName ldapDN = new LdapName(dn);
            for(Rdn rdn: ldapDN.getRdns()) {
                if(rdn.getType().matches("L|ST|C") && !((String)rdn.getValue()).trim().equals("") && !((String)rdn.getValue()).trim().equals("Unknown"))
                    sb.append((String)rdn.getValue() + " ");
            }           
        } catch (InvalidNameException e) {
            logger.error(e.getMessage());           
        }       
        return sb.toString();
    }  
    */
}

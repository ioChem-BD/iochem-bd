package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.SerializationUtils;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;

public class ProjectController extends HibernateService {

    @Override
    public void executeGetService(HttpServletResponse resp, Map<String, String> params,
            String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {

        if (params.get(PARAMETER_ID) == null || !params.get(PARAMETER_ID).matches("\\d+")) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, PROJECT_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(resp);
            return;
        }

        Project project = ProjectService.getById(Long.parseLong(params.get(PARAMETER_ID)));
        if (project == null) {
            returnKO(resp, "Not found", "The requested element is not accessible.");
        } else if (PermissionService.hasPermissionOnProject(project, Permissions.READ)) {
            sendObj(resp, fromProject(project));
        } else {
            returnKO(resp, "Permissions Error", "Your user is not allowed to read the requested path.");
        }
    }

    @Override
    public void executePostService(HttpServletResponse resp, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        if(params.size() == 0){
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST,"Invalid JSON","Invalid JSON.").sendErrorResponse(resp);
            return;
        }
        String path = params.get(PARAMETER_PATH);
        String name = params.get(PARAMETER_NAME);
        String description = params.get(PARAMETER_DESCRIPTION);
        String cg = params.get(PARAMETER_CG) != null ? params.get(PARAMETER_CG) : "";
        if (path == null || name == null || description == null) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, PRO_MOD_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(resp);
        } else {
            try {
                if (path.length() > 512)
                    throw new Exception("Project path exceeds maximum length (512)");

                if (!PermissionService.hasPermissionOnProject(path, Permissions.WRITE)) {
                    new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, PRO_MOD_ERROR, PERMISSION_ERROR).sendErrorResponse(resp);
                    return;
                }

                Project p = new Project();
                p.setParentPath(path);
                p.setName(truncate(Main.normalizeField(name), 64));
                p.setDescription(truncate(description, 304));
                p.setConceptGroup(truncate(cg, 64));
                p.setLinuxPermissions(buildPermissions(path));
                p.setOwner(UserService.getUserId());
                p.setGroup(UserService.getMainGroupId());
                ProjectService.add(p);
                sendMsg(resp, String.valueOf(p.getId()));
            } catch (Exception e) {
                new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, PRO_MOD_ERROR, e.getMessage()).sendErrorResponse(resp);
            }
        }
    }

    @Override
    public void executePutService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        if(params.size() == 0){
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST,"Invalid JSON","Invalid JSON.").sendErrorResponse(response);
            return;
        }
        try {
            Project p = ProjectService.getById(Long.parseLong(params.get(PARAMETER_ID)));
            if (p == null)
                throw new Exception("Invalid project id.");

            String path = p.getPath();

            if (PermissionService.hasPermissionOnProject(path, Permissions.WRITE)) {

                Project project = (Project) SerializationUtils.clone(p);
                String username = UserService.getUserName();

                String name = ((params.get(PARAMETER_NAME) == null || params.get(PARAMETER_NAME).isBlank())
                        ? p.getName()
                        : truncate(Main.normalizeField(params.get(PARAMETER_NAME)), 64));
                String cg = ((params.get(PARAMETER_CG) == null) ? p.getConceptGroup() : params.get(PARAMETER_CG));
                int group = ((params.get(PARAMETER_GROUP) == null) ? p.getGroup()
                        : Integer.parseInt(params.get(PARAMETER_GROUP)));
                String permissions = ((params.get(PARAMETER_PERM) == null) ? p.getLinuxPermissions()
                        : params.get(PARAMETER_PERM));
                String destinationPath = ((params.get(PARAMETER_PATH) == null) ? null : params.get(PARAMETER_PATH));
                String description = ((params.get(PARAMETER_DESCRIPTION) == null
                        || params.get(PARAMETER_NAME).isBlank()) ? p.getDescription()
                                : params.get(PARAMETER_DESCRIPTION));

                if (!belongsToGroup(group))
                    throw new Exception("Current user doesn't belong to the provided user group.");

                if (!Pattern.matches("rw(----|r---|rw--|rwr-|rwrw)", permissions))
                    throw new Exception(
                            "Invalid permissions, please use a permission string made of six r, w or - characters, similar to the Linux ones. Hyphen avoids permissions. Start always with rw, ex. rw---- , rwr---, rwrw--");

                if (destinationPath != null) {
                    if (destinationPath.isBlank())
                        throw new Exception("Invalid project path");

                    if (destinationPath.length() > 512)
                        throw new Exception("Project path exceeds maximum length (512)");

                    if (!isValidMovePath(username, destinationPath) ||
                            !PermissionService.hasPermissionOnProject(destinationPath, Permissions.WRITE)) {
                        new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, PRO_MOD_ERROR, INVALID_PATH).sendErrorResponse(response);
                        return;

                    }
                    if (!isValidMove(project, destinationPath, name)) {
                        new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, PRO_MOD_ERROR, INVALID_PATH).sendErrorResponse(response);
                        return;
                    }
                    project.setParentPath(destinationPath);
                }

                project.setGroup(group);
                project.setLinuxPermissions(permissions);
                project.setConceptGroup(truncate(cg, 64));
                project.setDescription(truncate(description, 304));
                project.setName(name);
                ProjectService.update(project);
                returnOK(response);
            } else {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN ,PRO_MOD_ERROR, PERMISSION_ERROR).sendErrorResponse(response);
            }
        } catch (NullPointerException n) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, PRO_MOD_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, PRO_MOD_ERROR, e.getMessage()).sendErrorResponse(response);
        }
    }

    @Override
    public void executeDeleteService(HttpServletResponse resp, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {

        if (params.get(PARAMETER_ID) == null || !params.get(PARAMETER_ID).matches("\\d+")) {
            new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, PROJECT_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(resp);
            return;
        }

        Project project = ProjectService.getById(Long.parseLong(params.get(PARAMETER_ID)));
        if (project == null) {
            new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, PROJECT_ERROR, NOT_FOUND_ERROR).sendErrorResponse(resp);
        } else {
            if (PermissionService.hasPermissionOnProject(project, Permissions.READ)) {
                try {
                    ProjectService.deleteProject(project.getId(), true);
                    returnOK(resp);
                } catch (Exception e) {
                    new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, PROJECT_ERROR, PERMISSION_ERROR).sendErrorResponse(resp);
                }
            } else {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, PROJECT_ERROR, PERMISSION_ERROR).sendErrorResponse(resp);
            }
        }
    }

    private String buildPermissions(String path) throws BrowseCredentialsException {
        if (path.equals(Paths.getFullPath(Constants.BASE_PATH, UserService.getUserName()))) // Base path
            return "rw----";
        else {
            return ProjectService.getByPath(path).getLinuxPermissions();
        }
    }

    private boolean belongsToGroup(int group) throws Exception {
        try {
            int groupId = group;
            for (String userGroup : UserService.getUserGroups())
                if (Integer.parseInt(userGroup) == groupId)
                    return true;
        } catch (Exception e) {
            throw new Exception(GROUP_DOESNT_EXIST_ERROR);
        }
        return false;
    }

    private boolean isValidMovePath(String username, String destinationPath) {
        return destinationPath.startsWith(GeneralConstants.DB_ROOT + username);
    }

    private boolean isValidMove(Project source, String destinationPath, String name) {
        return !(isMovingToSameParentOrItself(source, destinationPath) ||
                isDestinationChildOfSelection(source, destinationPath) ||
                existsCollisionsOnDestinationPath(destinationPath, name));
    }

    private boolean isMovingToSameParentOrItself(Project source, String destinationPath) {
        return source.getPath().equals(destinationPath) ||
                source.getPath().equals(Paths.getFullPath(destinationPath, source.getName()));
    }

    private boolean isDestinationChildOfSelection(Project source, String destinationPath) {
        return Paths.isDescendant(source.getPath(), destinationPath);
    }

    private boolean existsCollisionsOnDestinationPath(String destinationPath, String name) {
        try {
            return ProjectService.projectExists(destinationPath, name);
        } catch (Exception e) {
            return true;
        }
    }
}

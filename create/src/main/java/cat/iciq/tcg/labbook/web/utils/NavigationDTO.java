package cat.iciq.tcg.labbook.web.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.PublicableEntity;

public class NavigationDTO {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");

    private Long id;
    private String title;
    private String type;
    private String description;
    private String creationDate;
    private String handle;
    private boolean published;
    private String edit;
    private int elementOrder;    
    private List<NavigationDTO> children;

    public NavigationDTO() {
        this.children = new ArrayList<>();
    }
    
    public NavigationDTO(Long id, String title, String type, String description, String creationDate, String handle,
                 boolean published, String edit, int elementOrder, List<NavigationDTO> children) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.description = description;
        this.creationDate = creationDate;
        this.handle = handle;
        this.published = published;
        this.edit = edit;
        this.elementOrder = elementOrder;
        this.children = children;
    }

    public NavigationDTO(Long id, String title, String type, String description, String creationDate, String handle,
                 boolean published, String edit, int elementOrder) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.description = description;
        this.creationDate = creationDate;
        this.handle = handle;
        this.published = published;
        this.edit = edit;
        this.elementOrder = elementOrder;
        this.children = new ArrayList<>();
    }

    public static NavigationDTO fromEntity(PublicableEntity entity, Map<String, String> editHashCache){
        NavigationDTO dto = null;        
        if(entity == null)
            throw new IllegalArgumentException("Entity cannot be null");
        LocalDateTime creationDate = entity.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();              
        if(entity instanceof Project){
            String editHash = null;
            if(editHashCache.containsKey(entity.getHandle())) {
                editHash = editHashCache.get(entity.getHandle());
            }else{
                editHash = ((Project)entity).getEditHash();
                editHashCache.put(entity.getHandle(), editHash);
            }
            dto = new NavigationDTO(entity.getId(), entity.getName(), "PRO", entity.getDescription(), creationDate.format(formatter), entity.getHandle(), entity.isPublished(), editHash, entity.getElementOrder());            
        }else if(entity instanceof Calculation){
            dto = new NavigationDTO(entity.getId(), entity.getName(), ((Calculation)entity).getType().getAbbreviation(), entity.getDescription(), creationDate.format(formatter), entity.getHandle(), entity.isPublished(), "", entity.getElementOrder());
        }else {
            throw new IllegalArgumentException("Entity type not supported");
        }
        return dto;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }

    public int getElementOrder() {
        return elementOrder;
    }

    public void setElementOrder(int elementOrder) {
        this.elementOrder = elementOrder;
    }

    public List<NavigationDTO> getChildren() {
        return children;
    }

    public void setChildren(List<NavigationDTO> children) {
        this.children = children;
    }    

    public void addChild(NavigationDTO child) {
        this.children.add(child);
    }
}

package cat.iciq.tcg.labbook.web.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.controllers.ErrorResponse;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class HibernateService extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LogManager.getLogger(HibernateService.class.getName());

    public static final String SERVER_ERROR = "System error on server";

    private static final String MIMETYPE_APPLICATION_JSON = "application/json";
    private static final String MIMETYPE_TEXT_PLAIN = "text/plain";

    protected static final String PARAMETER_CALCFILE = "calcFile";
    protected static final String PARAMETER_CG = "cg";
    protected static final String PARAMETER_DESCRIPTION = "description";
    protected static final String PARAMETER_CONFIGURATION = "configuration";
    protected static final String PARAMETER_DESCSEARCH = "descSearch";
    protected static final String PARAMETER_FILE = "file";
    protected static final String PARAMETER_FULL = "full";
    protected static final String PARAMETER_ID = "id";
    protected static final String PARAMETER_PATH = "path";
    protected static final String PARAMETER_INDEX = "index";
    protected static final String PARAMETER_LABEL = "label";
    protected static final String PARAMETER_MODE = "mode";
    protected static final String PARAMETER_NAME = "name";
    protected static final String PARAMETER_TITLE = "title";
    protected static final String PARAMETER_NAMESEARCH = "nameSearch";
    protected static final String PARAMETER_NEWCG = "newCG";
    protected static final String PARAMETER_NEWDESC = "newDesc";
    protected static final String PARAMETER_NEWGROUP = "newGroup";
    protected static final String PARAMETER_NEWNAME = "newName";
    protected static final String PARAMETER_NEWPARENTPATH = "newParentPath";
    protected static final String PARAMETER_NEWPATH = "newPath";
    protected static final String PARAMETER_NEWPERM = "newPerm";
    protected static final String PARAMETER_DESC = "description";
    protected static final String PARAMETER_GROUP = "group";
    protected static final String PARAMETER_PARENTPATH = "parentPath";
    protected static final String PARAMETER_PERM = "permissions";
    protected static final String PARAMETER_ORDERBY = "orderby";
    protected static final String PARAMETER_SEARCHTYPE = "searchType";
    protected static final String PARAMETER_PATHSEARCH = "pathSearch";
    protected static final String PARAMETER_RANGE = "range";
    protected static final String PARAMETER_FILE_ID = "fileId";

    protected static final String LOAD_ERROR = "Problems ocurred during the execution of the query:";
    protected static final String BAD_REQUEST_ERROR = "Bad request";
    protected static final String PROJECT_ERROR = "Project Operation Error";
    protected static final String REPORT_ERROR = "Report Operation Error";
    protected static final String PERMISSION_ERROR = "Not enough permissions";
    protected static final String NOT_FOUND_ERROR = "Element not found";
    protected static final String READ_PERMISSION_ERROR = "User doesn't have read permission to the resource. Operation aborted.";
    protected static final String WRITE_PERMISSION_ERROR = "User doesn't have write permission to the resource. Operation aborted.";
    protected static final String ACCESS_ERROR = "It's not possible to access the specified resource.";
    protected static final String PROJ_ALREADY_EXIST_ERROR = "The project name specified already exist. Please, choose another one.";
    protected static final String PRO_MOD_ERROR = "Error modifying the project";
    protected static final String REPO_MOD_ERROR = "Error modifying the report";
    protected static final String GROUP_DOESNT_EXIST_ERROR = "The specified group it's not valid.";
    protected static final String INVALID_PATH = "The provided path is invalid";
    protected static final String CALCULATION_ERROR = "Error on the calculation operation.";
    protected static final String CALC_NON_EXIST_ERROR = "Requested calculation doesn't exist.";
    protected static final String CALC_READ_ERROR = "User not allowed to read this project.";
    protected static final String BAD_PATH_ERROR = "Calculation path error. Operation aborted.";
    protected static final String CALC_LONG_PATH_ERROR = "Project path exceeds maximum length (256)";

    protected HibernateService() {
        super();
    }

    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnSystemError(response, "Server error on execute GET action", "Unsupported operation");
    }

    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnSystemError(response, "Server error on execute POST action", "Unsupported operation");
    }

    public void executePutService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnSystemError(response, "Server error on execute PUT action", "Unsupported operation");
    }

    public void executeDeleteService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        returnSystemError(response, "Server error on execute DELETE action", "Unsupported operation");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Session session = null;
        Transaction transaction = null;
        try {
            UserService.loadUserInfoFromRequest(req);
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            // Call the abstract method to handle the specific logic for each servlet
            executeGetService(resp, buildParameterMap(req), UserService.getUserName(), UserService.getUserId());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error(e.getMessage());
            returnSystemError(resp, "Server error", e.getMessage());
        } finally {
            if (session != null)
                session.close();
            UserService.clearUserInfoFromRequest();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Session session = null;
        Transaction transaction = null;
        try {            
            UserService.loadUserInfoFromRequest(req);
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            // Call the abstract method to handle the specific logic for each servlet
            executePostService(resp, buildParameterMap(req) , UserService.getUserName(), UserService.getUserId());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error(e.getMessage());
            returnSystemError(resp, "Server error", e.getMessage());
        } finally {
            if (session != null)
                session.close();
            UserService.clearUserInfoFromRequest();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Session session = null;
        Transaction transaction = null;
        try {
            UserService.loadUserInfoFromRequest(req);
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            // Call the abstract method to handle the specific logic for each servlet
            executeDeleteService(resp, buildParameterMap(req), UserService.getUserName(), UserService.getUserId());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error(e.getMessage());
            returnSystemError(resp, "Server error", e.getMessage());
        } finally {
            if (session != null)
                session.close();
            UserService.clearUserInfoFromRequest();
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Session session = null;
        Transaction transaction = null;
        try {
            UserService.loadUserInfoFromRequest(req);
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            executePutService(resp, buildParameterMap(req), UserService.getUserName(), UserService.getUserId());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error(e.getMessage());
            returnSystemError(resp, "Server error", e.getMessage());
        } finally {
            if (session != null)
                session.close();
            UserService.clearUserInfoFromRequest();
        }
    }

    public static Map<String, String> buildParameterMap(HttpServletRequest request) throws UnsupportedEncodingException {
        request.setCharacterEncoding(StandardCharsets.UTF_8.name());
        Map<String, String> params = new HashMap<>();
        // Retrieve url parameters
        Enumeration<String> parameters = request.getParameterNames();        
        while (parameters.hasMoreElements()) {
            String key = parameters.nextElement();
            String value = request.getParameter(key);
            params.put(key, value);
        }
        //Retrieve body-defined parameters
        try{
            String body = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
            params.putAll(mapJsonBodyToMap(body));          
        }catch(IOException e){
            logger.error(e.getMessage());
        }
        return params;
    }

    private static Map<String, String> mapJsonBodyToMap(String jsonBody) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<>();
        if(jsonBody == null || jsonBody.isBlank())
            return map;
        try {               
            map = mapper.readValue(jsonBody, new TypeReference<Map<String, String>>() {});
        } catch (IOException e) {
            logger.error("Error parsing JSON body."); 
        }
        return map;
    }

    protected static void returnSystemError(HttpServletResponse response, String title, String message,
            boolean useJson) {
        try {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            if (useJson)
                response.setContentType(MIMETYPE_TEXT_PLAIN);
            response.getWriter().write(String.format("%s: %s", title, message));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    protected static void returnSystemError(HttpServletResponse response, String title, String message) {
        returnSystemError(response, title, message, false);
    }

    public static void sendObj(HttpServletResponse response, Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(obj);
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType(MIMETYPE_APPLICATION_JSON);
            response.getWriter().write(json);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void sendMsg(HttpServletResponse response, String msg) {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType(MIMETYPE_TEXT_PLAIN);
            response.getWriter().write(msg);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void sendMsgError(HttpServletResponse response, String msg, Integer statusCode) {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setStatus(statusCode);
            response.setContentType(MIMETYPE_TEXT_PLAIN);
            response.getWriter().write(msg);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void sendBitstream(HttpServletResponse response, File file, String mimetype) {
        if (file == null || !file.exists()) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType(mimetype);
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

        try (FileInputStream fileInputStream = new FileInputStream(file);
                OutputStream responseOutputStream = response.getOutputStream()) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                responseOutputStream.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    public static void sendFile(HttpServletResponse response, File file) {
        if (file == null || !file.exists()) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/xml");
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

        try (FileInputStream fileInputStream = new FileInputStream(file);
                OutputStream responseOutputStream = response.getOutputStream()) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                responseOutputStream.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    public static void returnOK(HttpServletResponse response) {
        try {
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType(MIMETYPE_TEXT_PLAIN);
            response.getWriter().write("OK");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void returnKO(HttpServletResponse response, String title, String message) {
        try {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.setContentType(MIMETYPE_TEXT_PLAIN);
            response.getWriter().write(String.format("%s: %s", title, message));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static EntityDTO fromProject(Project p) {
        EntityDTO entity = new EntityDTO();
        try {
            entity.setId(p.getId());
            entity.setPath(p.getPath());
            entity.setType("PRO");
            entity.setName(p.getName());
            entity.setDescription(p.getDescription());
            entity.setPermissions(p.getLinuxPermissions());
            entity.setOwner(UserService.getUserNameById(p.getOwner()));
            entity.setGroup(UserService.getGroupNameById(p.getGroup()));
            entity.setCreationDate(p.getCreationDate());
            entity.setConceptGroup(p.getConceptGroup());
            entity.setState(p.getState().name());
            entity.setPublished(p.isPublished());
            entity.setPublishedName(p.getPublishedName());
            entity.setPublicationDate(p.getPublicationDate());
            entity.setHandle(p.getHandle());
            return entity;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public static EntityDTO fromCalculation(Calculation c, String permissions, String owner, String group,
            String conceptGroup) {
        EntityDTO entity = new EntityDTO();
        entity.setId(c.getId());
        entity.setPath(c.getPath());
        entity.setType(c.getType().getAbbreviation());
        entity.setName(c.getName());
        entity.setDescription(c.getDescription());
        entity.setPermissions(permissions);
        entity.setOwner(owner);
        entity.setGroup(group);
        entity.setCreationDate(c.getCreationDate());
        entity.setConceptGroup(conceptGroup);
        entity.setState(c.isPublished() ? "published" : "created");
        entity.setPublished(c.isPublished());
        entity.setPublishedName(c.getPublishedName());
        entity.setPublicationDate(c.getPublicationDate());
        entity.setHandle(c.getHandle());
        return entity;
    }

    public static String truncate(String value, int length) {
        if (value != null && value.length() > length)
            value = value.substring(0, length);
        return value;
    }
}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cat.iciq.tcg.labbook.datatype.Action;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class ActionService {
	
	private static final Logger log = LogManager.getLogger(ActionService.class);
	
	private static HashMap<Integer, Action> actionById;
	
	static {
	    loadActions();        
	}
	
	private static void loadActions() {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		actionById = new HashMap<>();
		try {
		    CriteriaBuilder builder = session.getCriteriaBuilder();
		    CriteriaQuery<Action> criteriaQuery = builder.createQuery(Action.class);
		    Root<Action> root = criteriaQuery.from(Action.class);
		    criteriaQuery.select(root);

		    List<Action> results = session.createQuery(criteriaQuery).getResultList();

		    for(Action action: results)
	        	actionById.put(action.getId(), action);
		} catch (Exception e) {
			log.error("Exception raised loading actions.");
			log.error(e.getMessage());
		}
	}
	
	public static Action getById(int id) {
		return actionById.get(id);
	}	

	public static List<Action> getAll(){
		return new ArrayList<Action>(actionById.values());
	}
	
}

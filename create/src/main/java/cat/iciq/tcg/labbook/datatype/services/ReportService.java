/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class ReportService {
	
	private static final Logger log = LogManager.getLogger(ReportService.class);

	public static List<Report> getUserReports(int userId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		List<Report> results = new ArrayList<>();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Report> criteriaQuery = builder.createQuery(Report.class);
			Root<Report> root = criteriaQuery.from(Report.class);

			Predicate owner = builder.equal(root.get("ownerId"), userId);
			criteriaQuery.orderBy(builder.asc(root.get("creationDate")));
			criteriaQuery.select(root).where(owner);

			results = session.createQuery(criteriaQuery).getResultList();
			// Evict from current session all the results
			for (Report report : results)
				session.evict(report);
			return results;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return results;
	}

	public static Report getReport(int id) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Report report = session.get(Report.class, id);
		session.evict(report);
		return report;
	}

	public static void deleteReport(int id) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.delete(getReport(id));
	}

	public static void saveReport(Report report) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.update(report);
	}

	public static int createReport(int reportType, int ownerId) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Report report = new Report();
		report.setCreationDate(new Date(System.currentTimeMillis()));
		report.setOwnerId(ownerId);
		report.setName("");
		report.setTitle("");
		report.setDescription("");
		report.setReportConfiguration(null);
		report.setType(ReportTypeService.getReportTypeById(reportType));
		report.setPublished(false);
		session.save(report);
		session.flush();
		return report.getId();
	}

	public static int createReport(Report newReport) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.save(newReport);
		session.flush();
		return newReport.getId();
	}

	public static int updateReport(Report report) throws Exception {
		// Retrieve the original report from the database
		Report originalReport = ReportService.getReport(report.getId());

		if (originalReport == null) {
			throw new Exception("Report not found");
		}

		// Update the report fields that have changed
		if (!report.getDescription().equals(originalReport.getDescription())) {
			originalReport.setDescription(report.getDescription());
		}
		if (!report.getName().equals(originalReport.getName())) {
			originalReport.setName(report.getName());
		}
		if (!report.getTitle().equals(originalReport.getTitle())) {
			originalReport.setTitle(report.getTitle());
		}

		// Update the report configuration
		if (report.getReportConfiguration() != null
				&& !report.getReportConfiguration().equals(originalReport.getReportConfiguration())) {
			originalReport.setReportConfiguration(report.getReportConfiguration().toString());
		}

		originalReport.getCalculations().clear();
		originalReport.setCalculations(report.getCalculations());

		// Save the updated report to the database
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.merge(originalReport);

		return report.getId();
	}
}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;


import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Window;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.zk.composers.main.ErrorDialog;

public class ReportReactionGraph extends ReportCustomBase {

	public static final long serialVersionUID = 1L;

	public static final String GET_CALCULATION_ISTS_XSLT_TEMPLATE_PATH = "/html/reports/ReportReactionGraph/reactionGraph.xsl";
	public static final String AVAILABLE_ENERGIES_XSLT_TEMPLATE_PATH   = "/html/reports/ReportEnergyReactionProfile/getAvailableEnergies.xsl";
	public static final String DATA_EXTRACTION_XSLT_TEMPLATE_PATH      = "/html/reports/ReportReactionGraph/dataExtraction.xsl";
	private String CALCULS_ARRAY_STRINGIFY = "NOT CALCULS";

	private String jsonGraph;
	
	protected double frequencyThreshold = -5.0;
	private Map<Long, Map<String, Double>> energiesCalculated = new HashMap<>();
	private String calculationsTS = "";


	@WireVariable("desktopScope")
	private Map<String, Object> _desktopScope;
	
	@Wire
	Iframe containerIframe;

	@Wire
	Window customReportWindow;

	@Wire
	Radiogroup energyTypeRdg;
	
	@Wire
	Label unavailableEnergyLbl;

	@Wire
	Radio potentialEnergyRad;

	@Wire
	Radio freeEnergyRad;

	@Wire
	Radio zeroPointEnergyRad;

	@Wire
	Radio enthalpyEnergyRad;

	@Wire
	Radio bondingEnergyRad;

	@Wire
	Radio nucRepulsionEnergyRad;

	@Wire
	Button clearGraphBtn;

	@Wire
	Button focusGraphBtn;

	@Wire
	Button showEdgesBtn;

	@Wire
	Button downloadGraphBtn;

	@Wire
	Button applyStylesBtn;

	@Wire
	Button uploadGraphBtn;

	@Wire
	Button confCorrectBtn;

	@Wire
  Checkbox showFormula;

	@Wire
	Checkbox showEdges;

	@Wire
	Listbox graphStyleListBox;

	@Listen("onClick=#clearGraphBtn")
	public void onClearGraphBtnClick() {
		reportCommunicationQueue.publish(new Event("clearGraph", null, null));
	}

	@Listen("onClick=#focusGraphBtn")
	public void onFocusGraphBtnClick() {
		reportCommunicationQueue.publish(new Event("focusGraph", null, null));
	}

	@Listen("onClick=#showEdgesBtn")
	public void onShowEdgesBtnClick() {
		reportCommunicationQueue.publish(new Event("showEdgesName", null, null));
	}

	@Listen("onClick=#confCorrectBtn")
	public void onConfCorrectBtnClick(){
		try {
			HashMap<String, Double> filteredEnergies = new HashMap<>();
			String typeEnergy = energyTypeRdg.getSelectedItem().getValue().toString();
			for (Long calculationId : energiesCalculated.keySet()) {
				Double energyValue = energiesCalculated.get(calculationId).get(typeEnergy);
				filteredEnergies.put(String.valueOf(calculationId), energyValue);
			}
			Object[] values = new Object[2];
			values[0] = energyTypeRdg.getSelectedItem().getLabel();
			values[1] = filteredEnergies;

			reportCommunicationQueue.publish(new Event("confCorrectBtn", null, values));			
			
			
		} catch (Exception e) {
			showError("Missing parameter", null, "Please select a type of energy");
		}

	}

	@Listen("onCheck=#energyTypeRdg")
	public void onEnergyTypeRdgSelected() {
			confCorrectBtn.setDisabled(false);
	}

	@Listen("onCheck = #showFormula")
	public void formulaCheck(){
		reportCommunicationQueue.publish(new Event("showFormulas",null, showFormula.isChecked()));
	}

	@Listen("onCheck = #showEdges")
	public void edgesCheck(){
		reportCommunicationQueue.publish(new Event("showEdges",null, showEdges.isChecked()));
	}



	@Listen("onUpload=#uploadGraphBtn")
	public void onUploadGraphBtn(UploadEvent event) throws IOException {
		try {
			Media media = event.getMedia();
			
			if (media != null) {
	
				byte[] jsonData = media.getByteData();
				TreeSet<Long> calculationIds = new TreeSet<>();
	
				String jsonStr = new String(jsonData, StandardCharsets.UTF_8);
	
				JsonNode jsonNode = new ObjectMapper().readTree(jsonStr);
	
				JsonNode nodes = jsonNode.path("elements").path("nodes");
				for (JsonNode node : nodes) {
					String nodeId = node.path("data").path("cid_formula").asText();
					String[] calculs = nodeId.trim().split("[\\+\\-\\s]+");
					calculationIds.addAll(Arrays.asList(convertToLongArray(calculs)));
				}
	
				JsonNode edges = jsonNode.path("elements").path("edges");
				for (JsonNode edge : edges) {
					String edgeId = edge.path("data").path("cid_formula").asText();

					if(edgeId.length() > 1){
						String[] calculs = edgeId.trim().split("[\\+\\-\\s]+");
	
						calculationIds.addAll(Arrays.asList(convertToLongArray(calculs)));
					}
				}
				
				Set<Calculation> allowedCalculationIds = filterCalculations(calculationIds);
				displayElement(allowedCalculationIds); 
				reportCommunicationQueue.publish(new Event("loadJsonGraph", null, jsonStr));
				
				
			} else {
				showError("No file provided", "Please provide a valid Json file");
			}
		}
		catch(JsonProcessingException e) {
			showError("Invalid JSON", e.getMessage());
		}
		catch(NumberFormatException e1){
			showError("Invalid Calculation ID", e1.getMessage());
		}
		catch(NotEnoughPermissions nep){
			showError("Not enough permissions", nep.getMessage());
		}
		catch(CalculationIsNull cn) {
			showError("Calculation is null", cn.getMessage());
		}
	}

	public Set<Calculation> filterCalculations(Set<Long> calculationIds ) throws NotEnoughPermissions, CalculationIsNull{
		
		Set<Calculation> allowedCalculationIds = new LinkedHashSet<>();
		Long idPointed;
		for (Long calculationId : calculationIds) {
			idPointed = calculationId;

			if(CalculationService.getById(calculationId) != null) {
				 Calculation calculation = CalculationService.getById(calculationId);		
				try {
					Boolean hasPermission = PermissionService.hasPermissionOnCalculation(calculationId, Permissions.READ);
					if(Boolean.TRUE.equals(hasPermission)) {
						allowedCalculationIds.add(calculation);
					}
				} catch(Exception e) {
					 throw new NotEnoughPermissions("You do not have sufficient permissions in the calculation with ID "+idPointed+" for the requested operation:");	
				}
			} else {
				 throw new CalculationIsNull("No calculation exists with id: "+idPointed);
			}
		}
		
		return allowedCalculationIds;
	}

	private void displayElement(Set<Calculation> allowedCalculationIds) {
		HashMap<String, Object> data = new HashMap<>();
		data.put("reportId", report.getId());
		data.put("calculationsList", allowedCalculationIds);
		reportManagementQueue.publish(new Event("loadCalculationsGraph", null, data));
	}
	

	public void showError(String caption, String error, String cause) {
		Window window = (Window) Executions.createComponents("errors/errorDialog.zul", null, null);
		window.setWidth("500px");
		ErrorDialog errorDialog = (ErrorDialog) window.getAttribute("$composer");        
		errorDialog.setTitle(caption);
		errorDialog.setError(error);
		errorDialog.setCause(new Html(cause));        
		window.doModal();
	}

	public void showError(String error, String cause) {
		showError("Graph upload error", error, cause);
	}
	
	public static Long[] convertToLongArray(String[] stringArray) throws NumberFormatException {
		
		if (stringArray == null || stringArray.length == 0) {
			return new Long[0];
		}
		Long[] longArray = new Long[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			longArray[i] = Long.valueOf(stringArray[i]);
		}
		return longArray;
	}

	private void getCalculationsEnergyAndTS() throws Exception {
		
		energiesCalculated = new HashMap<>();
		List<ReportCalculation> calculations = report.getCalculations();	
		try {
			setStrOfCalculationsEnergies(calculations);
			enableEnergyRadioButtons();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			Clients.clearBusy();
		}
	}

	private void setStrOfCalculationsEnergies(List<ReportCalculation> calculations) throws Exception{
		StringBuilder calcTs = new StringBuilder();	
		for (ReportCalculation reportCalc : calculations) {			
			List<String> energyTypes = getEnergyTypesAvaibleOfCalculation(reportCalc);
			Pair<Boolean, Map<String, Double>> data = extractCalculationData(reportCalc, energyTypes);
			long calcId = reportCalc.getCalculation().getId();
			if(data.getLeft().booleanValue()) {			
				String type = reportCalc.getCalculation().getType().getAbbreviation();
				calcTs.append(String.valueOf(calcId) + "/" + type + " ");
			}
			energiesCalculated.put(calcId, data.getRight());
		}
		calculationsTS = calcTs.toString().trim();

	}

	private void enableEnergyRadioButtons() {
		int totalCalculations = report.getCalculations().size();
		potentialEnergyRad.setVisible(countEnergy(CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL) == totalCalculations && totalCalculations != 0);
		freeEnergyRad.setVisible(countEnergy(CONFIG_PARAMETER_ENERGY_TYPE_FREE) == totalCalculations && totalCalculations != 0);		
		zeroPointEnergyRad.setVisible(countEnergy(CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT) == totalCalculations && totalCalculations != 0);
		enthalpyEnergyRad.setVisible(countEnergy(CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY) == totalCalculations && totalCalculations != 0);
		bondingEnergyRad.setVisible(countEnergy(CONFIG_PARAMETER_ENERGY_TYPE_BONDING) == totalCalculations && totalCalculations != 0);
		nucRepulsionEnergyRad.setVisible(countEnergy(CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION) == totalCalculations && totalCalculations != 0);
	}

	private long countEnergy(String type) {		
		return energiesCalculated.values().stream().filter( w -> w.keySet().contains(type)).count();
	}

	private List<String> getEnergyTypesAvaibleOfCalculation(ReportCalculation calculation){
		try{
			Templates template = loadedXsltTemplates.get(AVAILABLE_ENERGIES_XSLT_TEMPLATE_PATH);
			Transformer transformer = template.newTransformer();
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
					StringWriter writer = new StringWriter();
					StreamResult result = new StreamResult(writer);                  	    
    
			transformer.transform(xml, result);
			return Arrays.asList(writer.getBuffer().toString().split("\\|"));
		}catch (TransformerConfigurationException e){
			logger.error(e.getMessage());			
		}catch (TransformerException e){
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return new ArrayList<>();
	}

	// private boolean isCalculationTS(Transformer transformer, ReportCalculation reportCalc, int negativeFreqThreshold) {		
	// 	try {
	// 		String fileContent = getCalculationContent(reportCalc.getCalcPath());
	// 		StreamSource xml = new StreamSource(new StringReader(fileContent));
	// 		StringWriter writer = new StringWriter();
	// 		StreamResult result = new StreamResult(writer);
	// 		transformer.setParameter("negativeFrequencyThreshold", String.valueOf(negativeFreqThreshold));
	// 		transformer.transform(xml, result);
	// 		if(writer.toString().trim().matches("true"))
	// 			return true;
	// 	} catch (Exception e) {
	// 		logger.error(e.getMessage());
	// 	}
	// 	return false;
	// }

	@Override
	protected void loadReport() throws Exception {

		try {			
			if (report.hasNoConfiguration())
				setDefaultConfiguration();
			Node node = report.getReportConfiguration().query("/configuration/parameters/graph").item(0);
			jsonGraph = node.getTextContent();

			containerIframe.setClientDataAttribute("uuid", communicationUuid.toString());
			containerIframe.setClientDataAttribute("jsongraph", jsonGraph);
			containerIframe.setSrc("./reports/reportReactionGraphIframe.zul");
			Clients.showBusy("Loading report...");

			reportCommunicationQueue.subscribe(new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					if (event.getName().equals("graphChanged")) {
						jsonGraph = (String) event.getData();
						saveConfiguration();
					}
					if (event.getName().equals("deleteReportCalculation")) {
						long calculationId = (long) event.getData();
						reportManagementQueue.publish(new Event("deleteReportCalculation", null, calculationId));
					}
					if (event.getName().equals("requestListCalculationsReport")) {
						List<ReportCalculation> calculations = report.getCalculations();
						StringBuilder calculationsString = new StringBuilder();
						
						for (int i = 0; i < calculations.size(); i++) {
							ReportCalculation reportCalculation = calculations.get(i);
					
							calculationsString.append(reportCalculation.getCalculation().getId())
																.append(" >>> ")
																.append(reportCalculation.getCalcPath());
					
							if (i < calculations.size() - 1) {
									calculationsString.append(",");
							}

						}
						reportCommunicationQueue.publish(new Event("setCalculationsList", null, calculationsString.toString()));
						getCalculationsEnergyAndTS();
						reportCommunicationQueue.publish(new Event("setTSCalculations",null, calculationsTS));
					}					
				}
			});
		} catch (Exception e) {
			logger.error(e.getMessage());			
		} 
	}

	protected boolean matchStringsRegex(String keyWord, String phrase) {
		Pattern pattern = Pattern.compile(keyWord, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(phrase);
		return matcher.find();
	}

	@Override
	protected void generateReport() throws Exception {
		reportCommunicationQueue.publish(new Event("downloadJSON", null, null));
	}

	private void setDefaultConfiguration() throws SAXException, IOException {
		String defaultConfiguration = "<configuration>" + "<parameters>" + "<graph></graph>" + "</parameters>"
				+ "</configuration>";
		report.setReportConfiguration(defaultConfiguration);
	}

	@Override
	protected void saveConfiguration() throws SAXException, IOException {
		parentSelector.addChanges();
		StringBuilder configuration = new StringBuilder();
		configuration.append("<configuration>" + "	<parameters>" + "   <graph> " + jsonGraph + "</graph>");  
		configuration.append("	</parameters>" + "</configuration>");
		report.setReportConfiguration(configuration.toString());
	}


	@Override
	protected void onCalculationsChanged() {

		List<String> calculationsString = new ArrayList<>();
		
		report.getCalculations().forEach(
				reportCalculation -> calculationsString.add(reportCalculation.getCalculation().getId() + " >>> " + reportCalculation.getCalcPath())
		);
		reportCommunicationQueue.publish(new Event("setCalculationsList", null, String.join(",", calculationsString)));
		try {
			if(energiesCalculated.size()>report.getCalculations().size()){
				Set<Long> keysToRemove = new HashSet<>(energiesCalculated.keySet());
				List<ReportCalculation> newCalculations =  new ArrayList<>();
				for (ReportCalculation reportCalculation : report.getCalculations()) {
						Long key = reportCalculation.getCalculation().getId();
						if (!keysToRemove.contains(key)) {
							energiesCalculated.remove(key);
						} else {
							newCalculations.add(reportCalculation);
						}
				}

				setStrOfCalculationsEnergies(newCalculations);
				
				reportCommunicationQueue.publish(new Event("setTSCalculations",null, calculationsTS));

			} else {
				getCalculationsEnergyAndTS();
				reportCommunicationQueue.publish(new Event("setTSCalculations",null, calculationsTS));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected Pair<Boolean, Map<String, Double>> extractCalculationData(ReportCalculation calculation, List<String> energyTypes) throws Exception{
		Map<String, Double> energyValues = new HashMap<>();
		Transformer transformer = null;	
		Templates template = loadedXsltTemplates.get(DATA_EXTRACTION_XSLT_TEMPLATE_PATH);
		transformer = template.newTransformer();
		transformer.setParameter("title", calculation.getTitle());
		transformer.setParameter("energyTypes", String.join("|", energyTypes));
		transformer.setParameter("energyUnits", "nonsi:electronvolt");
		transformer.setParameter("negativeFrequencyThreshold", frequencyThreshold);

		String fileContent = getCalculationContent(calculation.getCalcPath());
		StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		boolean isTs = false;
		try {
				transformer.transform(xml, result);				
				String energyString = writer.getBuffer().toString();				
				for(String dataPair : energyString.split("\\|")){						
						if(dataPair.trim().isEmpty() || dataPair.endsWith("N/A"))
							continue;							
						String[] keyValue = dataPair.split("\\s+");
						if(keyValue[0].equals("TS"))
							isTs = Boolean.valueOf(keyValue[1]);
						else
							energyValues.put(keyValue[0], Double.valueOf(keyValue[1]));
				}
		} catch (TransformerException e) {				
				throw new Exception("Could not extract information from calculation c" + calculation.getCalcOrder());
		}	
		return new ImmutablePair<>(isTs, energyValues);		
	}
}

	class NotEnoughPermissions extends Exception {

	public NotEnoughPermissions(String message) {
		super(message);
		// Auto-generated constructor stub
	}
	private static final long serialVersionUID = 1L;
	
	}

	class CalculationIsNull extends Exception {

	public CalculationIsNull(String message) {
		super(message);
		// Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	}




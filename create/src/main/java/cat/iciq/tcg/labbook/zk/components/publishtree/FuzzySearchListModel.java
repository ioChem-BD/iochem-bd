/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.ListSubModel;
import org.zkoss.zul.event.ListDataListener;
import org.zkoss.zul.ext.Selectable;
import org.zkoss.zul.ext.SelectionControl;


@SuppressWarnings({ "unchecked", "serial" })
public class FuzzySearchListModel implements  ListModel<Comboitem>, ListSubModel<Comboitem>, Selectable<Comboitem>, Serializable{

	public static final Logger log = LogManager.getLogger(FuzzySearchListModel.class.getName());
	public static enum SearchMode { STARTS_WITH, CONTAINS, LEVENSHTEIN };
	public static enum SourceFields { VALUES_ONLY, LABEL_AND_VALUES;};
	private static final String FIELD_SEPARATOR = "\t";
	
	private ArrayList<Comboitem> proposals = new ArrayList<Comboitem>();;	
	private ListModelList<Comboitem> _model = new ListModelList(new ArrayList(), true);;
	private boolean _multiple = false;	
	private SearchMode searchMode = SearchMode.STARTS_WITH;	
	private int nRows = -1;
	
	
	public FuzzySearchListModel(String path){
		this(path, -1, SearchMode.STARTS_WITH, SourceFields.VALUES_ONLY);
	}
	
	public FuzzySearchListModel(String path, int nRows){
		this(path, nRows, SearchMode.STARTS_WITH, SourceFields.VALUES_ONLY);
	}
	
	public FuzzySearchListModel(String path, int nRows, SearchMode searchMode, SourceFields sourceFields){
		try {
			String line;
			Comboitem cboitem;
			LineNumberReader fr = new LineNumberReader(new FileReader(new File(path)));						
			while((line = fr.readLine()) != null){
				if(sourceFields == SourceFields.VALUES_ONLY){
					cboitem = new Comboitem(line);
					cboitem.setValue(line);
					proposals.add(cboitem);
				}
				else if(sourceFields == SourceFields.LABEL_AND_VALUES){
					String[] fields = line.split(FIELD_SEPARATOR);
					if(fields.length >= 2){
						cboitem = new Comboitem(fields[0]);
						cboitem.setValue(fields[1]);
						proposals.add(cboitem);
					} else{
						cboitem = new Comboitem(fields[0]);
						cboitem.setValue(fields[0]);
						proposals.add(cboitem);
					}						
				}
			}
			fr.close();
			this.nRows = nRows;
			this.searchMode = searchMode;			
		} catch (Exception e) {			
		}				
	}
	
	public static void main(String[] args){
		FuzzySearchListModel cm = new FuzzySearchListModel("/home/user/terms.txt");
		long startTime = System.currentTimeMillis();
		cm.getSubModel("sulfur", 5);
		long endtime = System.currentTimeMillis();
		long ellapsed = endtime - startTime;
		System.out.println("Ellapsed time : " + ellapsed);
	}
	
	@Override
	public ListModel<Comboitem> getSubModel(Object value, int nRows) {
		if(this.nRows != -1 && nRows == -1)
			nRows = this.nRows;
		switch(searchMode){		 
			case CONTAINS: 		return getViaContains(value, nRows);				
			case LEVENSHTEIN: 	return getViaLevenshtein(value, nRows);		
			case STARTS_WITH: 	
		    default: 			return getViaStartsWith(value, nRows);
		}		
	}
	
	private ListModel<Comboitem> getViaStartsWith(Object value, int nRows){
		String valueStr = ((String)value).toUpperCase();
		List<Comboitem> matches = new ArrayList<Comboitem>();
		
		for(Comboitem proposal : proposals)
			if(matches.size() == nRows)
				break;
			else if(proposal.getLabel().toUpperCase().startsWith(valueStr))
				matches.add(proposal);
		
		final Selectable<Comboitem> model =  new ListModelList<Comboitem>(matches, true);
        model.setMultiple(_multiple);
        _model = (ListModelList<Comboitem>)model;
        return (ListModel<Comboitem>)model;		     	
	}
	
	private  ListModel<Comboitem> getViaContains(Object value, int nRows){
		String valueStr = ((String)value).toUpperCase();		
		List<Comboitem> matches = new ArrayList<Comboitem>();
		
		for(Comboitem proposal : proposals)
			if(matches.size() == nRows)
				break;
			else if(proposal.getLabel().toUpperCase().indexOf(valueStr) >= 0)
				matches.add(proposal);
		
		
		final Selectable<Comboitem> model =  new ListModelList<Comboitem>(matches, true);
		
        model.setMultiple(_multiple);
        _model = (ListModelList<Comboitem>)model;        
        return (ListModel<Comboitem>)model;
	}
	
	private  ListModel<Comboitem> getViaLevenshtein(Object value, int nRows){
		List<Comboitem> matches = new ArrayList<Comboitem>();
	    Map<Integer, List<Comboitem>> levenshteinIndex = new HashMap<Integer, List<Comboitem>>();	    
		String valueStr = (String)value;
		
		for(int inx = 0; inx < proposals.size(); inx++){			
			int similarity = new Levenshtein(proposals.get(inx).getLabel().toUpperCase(), valueStr.toUpperCase()).getSimilarity();			
			List<Comboitem> l = levenshteinIndex.get(similarity);
			if (l == null)
				levenshteinIndex.put(similarity, l=new ArrayList<Comboitem>());
			l.add(proposals.get(inx));
		}
		
		//We will raise up Levenshtein level to 10 unless we don't reach nRows
		for(int inx = 0; inx < 10; inx++){
			if(matches.size() == nRows)
				break;
			ArrayList<Comboitem> values = (ArrayList<Comboitem>) levenshteinIndex.get(inx);
			if(values != null)
				for(Comboitem results : levenshteinIndex.get(inx)){
					matches.add(results);
					if(matches.size() == nRows)
						break;				
				}												
		}
     
        final Selectable<Comboitem> model =  new ListModelList<Comboitem>(matches, true);
        model.setMultiple(_multiple);
        _model = (ListModelList<Comboitem>)model;        
        return (ListModel<Comboitem>)model;		     
		
	}

	public Comboitem getElementAt(int index) {
		return _model.getElementAt(index);
	}

    public int getSize() {
        return _model.getSize();
    }

    public void addListDataListener(ListDataListener l) {
        _model.addListDataListener(l);
    }

    public void removeListDataListener(ListDataListener l) {
        _model.removeListDataListener(l);
    }

    @SuppressWarnings("unchecked")
    private Selectable<Comboitem> getSelectModel() {
        return (Selectable<Comboitem>) _model;
    }

    public Set<Comboitem> getSelection() {
        return getSelectModel().getSelection();
    }

    public boolean isSelected(Object obj) {
        return getSelectModel().isSelected(obj);
    }

    public boolean isSelectionEmpty() {
        return getSelectModel().isSelectionEmpty();
    }

    public boolean removeFromSelection(Object obj) {
        return getSelectModel().removeFromSelection(obj);
    }

    public void clearSelection() {
        getSelectModel().clearSelection();
    }
    
    public void setMultiple(boolean multiple) {
        getSelectModel().setMultiple(multiple);
    }

    public boolean isMultiple() {
        return getSelectModel().isMultiple();
    }
	@Override
	public void setSelection(Collection<? extends Comboitem> selection) {
		getSelectModel().setSelection(selection);	
	}
	
	@Override
	public boolean addToSelection(Comboitem obj) {		
		return false;			
	}

	@Override
	public SelectionControl getSelectionControl() {	
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSelectionControl(SelectionControl arg0) {
		// TODO Auto-generated method stub
		
	}	
}

class Levenshtein {
    private String compOne;
    private String compTwo;
    private int[][] matrix;
    private Boolean calculated = false;
 
    public Levenshtein(String one, String two){
        compOne = one;
        compTwo = two;
    }
 
    public int getSimilarity() {
        if (!calculated)
            setupMatrix();
        return matrix[compOne.length()][compTwo.length()];
    }
     
    public int[][] getMatrix(){
        setupMatrix();
        return matrix;
    }
 
    private void setupMatrix(){
        matrix = new int[compOne.length()+1][compTwo.length()+1];
 
        for (int i = 0; i <= compOne.length(); i++)
            matrix[i][0] = i;
        
        for (int j = 0; j <= compTwo.length(); j++)
            matrix[0][j] = j;
        
        for (int i = 1; i < matrix.length; i++)
            for (int j = 1; j < matrix[i].length; j++)
                if (compOne.charAt(i-1) == compTwo.charAt(j-1))
                    matrix[i][j] = matrix[i-1][j-1];
                else {
                    int minimum = Integer.MAX_VALUE;
                    if ((matrix[i-1][j])+1 < minimum)
                        minimum = (matrix[i-1][j])+1;                    
                    if ((matrix[i][j-1])+1 < minimum)
                        minimum = (matrix[i][j-1])+1;
                    
                    if ((matrix[i-1][j-1])+1 < minimum)
                        minimum = (matrix[i-1][j-1])+1;
                    matrix[i][j] = minimum;
                }        
        calculated = true;        
    }
}




/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.sql.Timestamp;
import java.util.BitSet;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import cat.iciq.tcg.labbook.datatype.helper.PostgreSQLEnumType;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.TokenManager;
import cat.iciq.tcg.labbook.zk.composers.Main;

@Entity
@Table(name="projects", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Project extends PublicableEntity {

    public static final int PERMISSIONS_LENGHT = 6;   

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projects_id_seq")
    @SequenceGenerator(name = "projects_id_seq",  sequenceName = "projects_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;
    
    @Column(name = "owner_user_id", nullable = false)    
	private int owner;

    @Column(name = "owner_group_id", nullable = false)
	private int group;

    @Type(type = "cat.iciq.tcg.labbook.datatype.helper.BitSetType")
    @Column(name = "permissions", columnDefinition = "bit(6)", nullable = false)    
	private BitSet permissions;

    @Column(name = "concept_group", nullable = false)
	private String conceptGroup;

    @Column(name = "modification_time")    
	private Timestamp modificationDate;

    @Column(name = "certification_time")
	private Timestamp certificationDate;

    @Column(name = "state", nullable = false)
    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
	private State state;

	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public BitSet getPermissions() {
		return permissions;
	}

	public void setPermissions(BitSet permissions) {	    
		this.permissions = permissions;
	}
	
	public String getConceptGroup() {
		return conceptGroup;
	}

	public void setConceptGroup(String conceptGroup) {
		this.conceptGroup = conceptGroup;
	}

	public Timestamp getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Timestamp modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Timestamp getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(Timestamp certificationDate) {
		this.certificationDate = certificationDate;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public boolean hasGroupReadPermission() {
		return permissions != null &&  permissions.get(2); //.matches("..r...");	
	}
	
	public boolean hasGroupWritePermission() {
		return permissions != null &&  permissions.get(3); //.matches("...w..");	
	}
	
	public boolean hasOthersReadPermission() {
		return permissions != null &&  permissions.get(4); //.matches("....r.");	
	}
	
	public boolean hasOthersWritePermission() {
		return permissions != null &&  permissions.get(5); //.matches(".....w");	
	}
	
	public String getEditHash() {
		if(isPublished() && getHandle() != null) {			
			String salt = CustomProperties.getProperty("module.communication.secret");
			String secret = Main.getBrowseBaseUrl() + "#" + this.getHandle() + "#" + "edit-collection";
			
			String editHash = TokenManager.encode(salt, secret);
			editHash = editHash.substring(editHash.length() -24, editHash.length());
			return editHash;			
		} else {
			return null;
		}
	}
		
	public enum State {
	    created, modified, certified, published
	}

    public String getLinuxPermissions() {
        StringBuilder result = new StringBuilder(); 
        for(int inx = 0; inx < PERMISSIONS_LENGHT; inx++)
            if(permissions.get(inx))
                result.append(inx%2 == 0? "r":"w");
            else
                result.append("-");        
        return result.toString();
    }

    public void setLinuxPermissions(String value) {
        if(permissions == null)
            permissions = new BitSet(PERMISSIONS_LENGHT);
        for(int inx = 0; inx < PERMISSIONS_LENGHT; inx++) {
            permissions.clear(inx);
            if(value.charAt(inx)!= '-')
                permissions.set(inx);
        }
    }
}





/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2020 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.util.HashMap;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Doublespinner;
import org.zkoss.zul.Window;

public class ReportEnergyReactionProfileGibbs extends SelectorComposer<Window> {

	private static final long serialVersionUID = 1L;

	@Wire
	Window gibbsEnergyParameters;
	
	@Wire
	Doublespinner pressureSpin;

	@Wire 
	Doublespinner temperatureSpin;

	@Listen("onClick=#resetBtn")
	public void onResetBtnClick() {
		pressureSpin.setValue(basePressure);
		temperatureSpin.setValue(baseTemperature);
	}

	protected Double baseTemperature;
	protected Double basePressure;

	@Listen("onClick=#closeBtn; onClick=#closeBtn2")
	public void onBtnClose() {
		HashMap<String, Double> params = new HashMap<String, Double>();
		params.put("pressure", pressureSpin.doubleValue());
		params.put("temperature", temperatureSpin.doubleValue());
		Events.postEvent("onClosing", gibbsEnergyParameters, params); 
		gibbsEnergyParameters.detach();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Window comp) throws Exception {	
		super.doAfterCompose(comp);	
		HashMap<String, Double> params = (HashMap<String, Double>) Executions.getCurrent().getArg();	
		baseTemperature = params.get("baseTemperature");
		basePressure = params.get("basePressure");
		pressureSpin.setValue(params.get("pressure") == -1.0 ? basePressure : params.get("pressure"));
		temperatureSpin.setValue(params.get("temperature") == -1.0 ? baseTemperature : params.get("temperature"));
	}

	public ReportEnergyReactionProfileGibbs() {
		super();
	}
}


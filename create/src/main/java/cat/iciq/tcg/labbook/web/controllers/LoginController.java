package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cat.iciq.tcg.labbook.zk.composers.Main;

import org.pac4j.cas.client.rest.CasRestBasicAuthClient;
import org.pac4j.cas.profile.CasProfile;
import org.pac4j.cas.profile.CasRestProfile;
import org.pac4j.core.config.Config;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.TokenCredentials;
import org.pac4j.jee.context.JEEContext;
import org.zkoss.zk.ui.WebApps;

import com.nimbusds.jose.JOSEException;

import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.cas.FilterConfigFactory;

public class LoginController extends HttpServlet {

    private static final String REST_URL= "/create/rest";

    private CasRestBasicAuthClient casRestClient;

    @Override
    public void init() throws ServletException {
        super.init();
        FilterConfigFactory configFactory = new FilterConfigFactory();
        Config config = configFactory.build();
        casRestClient = (CasRestBasicAuthClient) config.getClients().findClient("CasRestClient").get();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> atts = (Map<String, Object>) req.getAttribute("pac4jUserProfiles");
        if(atts == null){
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Wrong request, please clear session cookies and try again.");
            return;
        }
        CasRestProfile restProfile = (CasRestProfile) atts.get("CasRestClient");
        WebContext context = new JEEContext(req, resp);

        TokenCredentials serviceTicket = casRestClient.requestServiceTicket((Main.getBaseUrl() + REST_URL), restProfile, context);

        CasProfile profile = casRestClient.validateServiceTicket( (Main.getBaseUrl() + REST_URL) , serviceTicket, context);

        if (profile != null) {
            String key = (String) WebApps.getCurrent().getAttribute("keyJWT");
            try {
                String email = (String) profile.getAttribute("user_email");
                String mainGroupId = (String) profile.getAttribute("main_group_id");
                String groupId = (String) profile.getAttribute("group_id");
                String path = (String) profile.getAttribute("user_path");
                String fullname = (String) profile.getAttribute("user_fullname");
                String userId = (String) profile.getAttribute("user_id");

                String jwt = ApiJWT.createJWT(key, REST_URL, path, email, userId, fullname, mainGroupId, groupId);
                HibernateService.sendMsg(resp, jwt);

            } catch (JOSEException e) {
                e.printStackTrace();
            }
            resp.setStatus(HttpServletResponse.SC_OK);
        } else {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid service ticket.");
        }
        resp.flushBuffer();
    }
}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.services.UploadService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.TokenManager;
import cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction;
import cat.iciq.tcg.labbook.zk.ui.DeferredContentDaemon;
import cat.iciq.tcg.labbook.zk.ui.WebAppInitiator;

public class AdminTools extends HibernateService {

	private static final Logger logger = LogManager.getLogger(AdminTools.class.getName()); 	
	
	public static final String SERVLET_ENDPOINT = "/admintools";
	public static final String FLUSHXSLT_ENDPOINT = "/flushxslt";
	public static final String DOIDAEMONSTATUS_ENDPOINT = "/doidaemonrefresh";
	public static final String LOADLIMITS_ENDPOINT = "/uploadlimits";
	public static final String FILE_RETRIEVAL_ENDPOINT = "/fileretrieval";
	public static final String USERS_INFO_REFRESH_ENDPOINT = "/usersinforefresh";
	
	@Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
           
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		String endPoint = req.getRequestURI().substring(req.getContextPath().length());
		if(endPoint.equals(SERVLET_ENDPOINT + FLUSHXSLT_ENDPOINT )){
			flushXsltTemplates();
			resp.setContentType("text/html");
			resp.getOutputStream().write("<html><body>XSLT templates flushed!</body></html>".getBytes());
		}else if(endPoint.equals(SERVLET_ENDPOINT + DOIDAEMONSTATUS_ENDPOINT)){
			WebAppInitiator.setDoiDaemonStatus();
			String response = "Daemon status set to " + (int) WebApps.getCurrent().getAttribute("doiDaemonStatus"); 
			resp.getOutputStream().write(response.getBytes());
		}else if(endPoint.equals(SERVLET_ENDPOINT + LOADLIMITS_ENDPOINT)){
		    WebAppInitiator.setGlobalParameters();
		    UploadService.setupUploadRestrictions();
		    String response = "Global parameters updated successfully.";
		    resp.getOutputStream().write(response.getBytes());
		}else if(endPoint.equals(SERVLET_ENDPOINT + FILE_RETRIEVAL_ENDPOINT)) {
		    processFileRetrievalRequest(req);
		    resp.setCharacterEncoding("text/html");
		    resp.getOutputStream().write("File retrieval notified!".getBytes());		   
		}else if(endPoint.equals(SERVLET_ENDPOINT + USERS_INFO_REFRESH_ENDPOINT)){
			WebApps.getCurrent().removeAttribute(UserService.BROWSE_USERNAMES);
			WebApps.getCurrent().removeAttribute(UserService.BROWSE_USER_GROUPS);
		}
		resp.flushBuffer();
	}
	
	private void flushXsltTemplates(){
		XsltTransformAction.flushXsltTemplates();
		JCampDX.flushXsltTemplates();
		ToXYZ.flushXsltTemplates();		
	}
	
	private void processFileRetrievalRequest(HttpServletRequest request) {	     
	    Long requestMillis = Long.valueOf(request.getParameter("date"));
        String requestHash = request.getParameter("hash");
        String parentId = request.getParameter("parentId");
        String id = request.getParameter("id");
        String username = request.getParameter("username");
        if(isRetrievalRequestValid(requestMillis, requestHash, id)) {
            DeferredContentDaemon.addAccessibleFile(username, Integer.valueOf(parentId), Integer.valueOf(id), requestMillis);            
        }
    }
    

    private boolean isRetrievalRequestValid(Long requestMillis, String requestHash, String id) {
        if(requestMillis == null || requestHash == null)    // Bad request
            return false;
        
        String salt =  CustomProperties.getProperty("external.communication.secret");
        String calculatedHash = TokenManager.encode(requestMillis + "#" + id , salt).substring(0,10);
        if(!requestHash.equals(calculatedHash))
            return false;
        // If request is more than 24h old, file is deferred again, need to request it again.
        Long currentMillis = System.currentTimeMillis();
        if(currentMillis < requestMillis || currentMillis > requestMillis + (24*60*60*1000)  ) 
            return false;
        return true;
    }
}

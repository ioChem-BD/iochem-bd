/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cat.iciq.tcg.labbook.datatype.CalculationTypeFile;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class CalculationTypeFileService {

	private static final Logger log = LogManager.getLogger(CalculationTypeFileService.class);
	
	private static HashMap<Integer, CalculationTypeFile> calculationTypeFileById = null;
	private static HashMap<Integer, List<CalculationTypeFile>> calculationTypeFileByCalculationTypeId = null;

	static {
        loadCalculationTypeFiles();
	}
	
	public static List<CalculationTypeFile> getByCalculationTypeId(int calculationTypeId) {
		//Using lazy load, CalculationTypes must be loaded (static init) before loading this dependent table
		if(calculationTypeFileById  == null || calculationTypeFileByCalculationTypeId == null) {
			loadCalculationTypeFiles();
		}
		return calculationTypeFileByCalculationTypeId.get(calculationTypeId);		
	}
 	
	public static void loadCalculationTypeFiles() {
	    calculationTypeFileById = new HashMap<>();
        calculationTypeFileByCalculationTypeId = new HashMap<> ();
		try {					
			for(CalculationTypeFile type: getCalcTypeFiles()) {
			    calculationTypeFileById.put(type.getId(), type);
                if(!calculationTypeFileByCalculationTypeId.containsKey(type.getCalculationType().getId())) 
                    calculationTypeFileByCalculationTypeId.put(type.getCalculationType().getId(), new ArrayList<>());
                calculationTypeFileByCalculationTypeId.get(type.getCalculationType().getId()).add(type);
			}		
		}catch(Exception e) {
			log.error("Error loading CalculationTypeFiles values.");
			log.error(e.getMessage());
		}
	}
	
    public static List<CalculationTypeFile> getCalcTypeFiles() {
       Session session = HibernateUtil.getSessionFactory().getCurrentSession();
       List<CalculationTypeFile> results = new ArrayList<>();
       try {
           CriteriaBuilder builder = session.getCriteriaBuilder();
           CriteriaQuery<CalculationTypeFile> criteriaQuery = builder.createQuery(CalculationTypeFile.class);
           Root<CalculationTypeFile> root = criteriaQuery.from(CalculationTypeFile.class);
           criteriaQuery.select(root);
           
           results = session.createQuery(criteriaQuery).getResultList();
       }catch(NoResultException e1) {
           
       } catch(Exception e) {
           log.error(e.getMessage());
       }        
       return results;
   }
}

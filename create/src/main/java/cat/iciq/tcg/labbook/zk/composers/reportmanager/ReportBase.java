/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.logging.Log;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.ibm.icu.text.SimpleDateFormat;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.ReportService;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.web.definitions.Constants.ScreenSize;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.CalculationInsertion;

public class ReportBase extends SelectorComposer<Window> {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = LogManager.getLogger(ReportBase.class.getName());
	protected EventQueue<Event> reportManagementQueue = null;
	private EventQueue<Event> displayQueue = null;
	protected EventListener<Event> reportEventListener = null;
	private EventListener<Event> displayEventListener = null;
	protected EventQueue<Event> reportCommunicationQueue = null;

	private Report report;
	private List children;
	protected boolean hasChanges = false;

	@WireVariable("desktopScope")
	private Map<String, Object> _desktopScope;

	@Wire
	Window reportBaseWindow;

	@Wire
	Div baseReportDiv;

	@Wire
	Div baseSmallReportDiv;

	@Wire
	Div customReportDiv;

	@Wire
	Div customSmallReportDiv;

	@Wire
	Div reportSmall;

	@Wire
	Label reportIdLbl;

	@Wire
	Textbox reportNameTxt;

	@Wire
	Textbox reportTitleTxt;

	@Wire
	Textbox reportDescriptionTxt;

	@Wire
	Textbox searchBoxCalculations;

	@Wire
	Label reportCreationLbl;

	@Wire
	Checkbox reportPublishedChk;

	@Wire
	Column calcId;

	@Wire
	Column calcDrag;

	@Wire
	Column calcSelOrderCol;

	@Wire
	Column calcPath;

	@Wire
	Column calcTitle;

	@Wire
	Column calcBtn;

	@Wire
	Rows calcSelRows;

	@Wire
	Popup thumbnailPopup;

	@Wire
	Button sessionInReportBtn;

	@Wire
	Include reportTypeInc;

	@Wire
	Button saveBtn;

	@Wire
	Button saveAndCloseBtn;

	@Wire
	Button deleteBtn;

	@Wire
	Button generateBtn;

	@Wire
	Button publishBtn;

	@Wire
	Button findBtn;

	@Wire
	Button clearSearchBtn;

	@Listen("onCreate=#reportBaseWindow")
	public void onReportBaseWindowCreate(CreateEvent event) {
		int reportId = (Integer) event.getArg().get("reportId");
		boolean appendSessionElements = (Boolean) event.getArg().get("appendSessionElements");
		try {
			loadReport(reportId);
			loadReportCalculations();
			loadSpecificReportZulPage();
			initActionQueues();

			if (appendSessionElements)
				onSessionInReportBtnClick();
			
			setupLayout();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Listen("onClick=#sessionInReportBtn")
	public void onSessionInReportBtnClick() {
		Set<Entity> selectedElements = (Set<Entity>) _desktopScope.get("selectedElements");

		printListCalculations(selectedElements);

	}

	@Listen("onOpen=#thumbnailPopup")
	public void showThumbnail(Event e) throws MalformedURLException {
		Row calcRow = (Row) ((OpenEvent) e).getReference();
		if (calcRow != null) {
			URL reconstructedURL = new URL(Executions.getCurrent().getScheme(), Executions.getCurrent().getServerName(),
					Executions.getCurrent().getServerPort(), Executions.getCurrent().getContextPath());
			long id = (long) calcRow.getAttribute("calculationId");
			((Html) thumbnailPopup.getChildren().get(0)).setContent(
					"<img style='width:200px;height:200px' src=\"" + reconstructedURL.toString() + "/innerServices/getfile?id="
							+ id + "&file=" + CalculationInsertion.THUMBNAIL_FILE_NAME + "\" alt='No image available'></image>");
		}
	}

	@Listen("onClick=#setNameAsEntry")
	public void onSetNameAsEntryClick() {
		List<ReportCalculation> calculations = report.getCalculations();
		for (ReportCalculation calculation : calculations) {
			String name = Paths.getTail(calculation.getCalcPath());
			calculation.setTitle(name);
		}
		loadReportCalculations();
		addChanges();
	}

	@Listen("onClick=#setPathAsEntry")
	public void onSetPathAsEntryClick() {
		List<ReportCalculation> calculations = report.getCalculations();
		for (ReportCalculation calculation : calculations) {
			calculation.setTitle(calculation.getCalcPath());
		}
		loadReportCalculations();
		addChanges();
	}

	@Listen("onChange=#reportNameTxt")
	public void onReportNameTxtChange() {
		report.setName(reportNameTxt.getText());
		reportManagementQueue.publish(new Event("reportTextFieldChanged", reportBaseWindow, report.getId()));
	}

	@Listen("onChange=#reportTitleTxt")
	public void onReportTitleTextChange() {
		report.setTitle(reportTitleTxt.getText());
		reportManagementQueue.publish(new Event("reportTextFieldChanged", reportBaseWindow, report.getId()));
	}

	@Listen("onChange=#reportDescriptionTxt")
	public void onReportDescriptionTxtChange() {
		report.setDescription(reportDescriptionTxt.getValue());
		reportManagementQueue.publish(new Event("reportTextFieldChanged", reportBaseWindow, report.getId()));
	}

	@Listen("onClick=#saveBtn")
	public void onSaveBtnClick() throws Exception {
		saveReport();
		clearChanges();
	}

	@Listen("onClick=#saveAndCloseBtn")
	public void onSaveAndCloseBtnClick() throws Exception {
		saveReport();
		clearChanges();
		reportManagementQueue.publish(new Event("closeReport", null, report.getId()));
	}

	@Listen("onClick=#deleteBtn")
	public void onDeleteBtnClick() throws InterruptedException, Exception {
		Messagebox.show("Are you sure you want to remove this report?", "Remove report", Messagebox.YES + Messagebox.NO,
				Messagebox.QUESTION, new DeleteReportEventListener(report.getId())
				);
	}

	@Listen("onClick=#generateBtn")
	public void onGenerateBtnClick() {
		reportManagementQueue.publish(new Event("generateReport", null, report.getId()));
	}

	@Listen("onClick=#publishBtn")
	public void onPublishBtnClick() {
		// TODO:implement
	}

	@Listen("onOK=#searchBoxCalculations")
	public void onSearchBoxCalculations(){
		executeSearchLogic();
	}

	@Listen("onClick=#findBtn")
	public void onfindBtn() {
		executeSearchLogic();
	}

	public void executeSearchLogic() {
		String inputText = searchBoxCalculations.getValue();
		String reportType = report.getType().getName();
		
		for (int i = 0; i < calcSelRows.getChildren().size(); i++) {
			Row row = (Row) calcSelRows.getChildren().get(i);
    
			Cell cell1 = (Cell) row.getChildren().get(4);
			Label label = (Label) cell1.getFirstChild();
			String path = label.getValue();
			
			if(!reportType.equals("Reaction Graph")){
				row.setVisible(path.contains(inputText));

			}

			if(reportType.equals("Reaction Graph")){
				Cell cell2 = (Cell) row.getChildren().get(2);
				Label label2 = (Label) cell2.getFirstChild();
				String calculationId = label2.getValue();

				row.setVisible(path.contains(inputText) || calculationId.contains(inputText));

			}
		}

	}

	@Listen("onClick=#clearSearchBtn")
	public void onClearSearchBtn() {
		for (int i = 0; i < calcSelRows.getChildren().size(); i++) {
			Row row = (Row) calcSelRows.getChildren().get(i);
			row.setVisible(true);
    }
		
	}


	public void printListCalculations(Set<Entity> selectedElements){
		if (selectedElements != null && !selectedElements.isEmpty()) {
			boolean addedNewCalculations = false;
			for (Entity element : selectedElements)
				if (element.isCalculation() && !report.containsCalculation(element.getId())) {
					appendReportCalculation(element.getId());
					addedNewCalculations = true;
				}
			reindexCalculationsOrder();
			loadReportCalculations();
			calcSelOrderCol.sort(true, true);
			if (addedNewCalculations)
				reportManagementQueue.publish(new Event("reportChanged", reportBaseWindow, report.getId()));
		}
	}

	public void clearChanges() {
		hasChanges = false;
	}

	public void addChanges() {
		hasChanges = true;
	}

	public boolean hasChanges() {
		return hasChanges;
	}

	@SuppressWarnings("unchecked")
	private void initActionQueues() {
		reportManagementQueue = EventQueues.lookup("reportmanagement", EventQueues.DESKTOP, true);
		reportEventListener = new ReportEventListener();
		reportManagementQueue.subscribe(reportEventListener);


		displayQueue = EventQueues.lookup("display", EventQueues.DESKTOP, true);
		displayEventListener = new DisplayEventListener();
		displayQueue.subscribe(displayEventListener);
	}

	private void setupLayout() {
		if (isSmallLayout()) {
			baseReportDiv.setVisible(false);
			customReportDiv.setVisible(false);
			swapChild(baseReportDiv, baseSmallReportDiv);
			swapChild(customReportDiv, customSmallReportDiv);
			calcTitle.setVisible(false);
			reportSmall.setVisible(true);
		} else {
			reportSmall.setVisible(false);
			swapChild(baseSmallReportDiv, baseReportDiv);
			swapChild(customSmallReportDiv, customReportDiv);
			calcTitle.setVisible(true);
			baseReportDiv.setVisible(true);
			customReportDiv.setVisible(true);
		}
	}

	protected boolean isSmallLayout() {
		ScreenSize layout = getCurrentLayout();
		return layout == ScreenSize.SMALL || layout == ScreenSize.X_SMALL;
	}

	protected ScreenSize getCurrentLayout() {
		ScreenSize layout = (ScreenSize) Executions.getCurrent().getDesktop().getAttribute("display");
		if (layout == null)
			return ScreenSize.X_LARGE;
		else
			return layout;
	}

	private void swapChild(Component source, Component destination) {
		Component child = source.getFirstChild();
		if (child == null)
			return;
		child.detach();
		destination.appendChild(child);
	}

	public Report getReport() {
		return report;
	}

	private void saveReport() throws InterruptedException, Exception {
		if (reportNameTxt.getValue().equals("") || reportDescriptionTxt.getValue().equals("")) {
			Messagebox.show("Name and description fields are mandatory");
			return;
		}
		try {
			ReportService.saveReport(report);
			loadReportCalculations();
			Messagebox.show("Report " + report.getName() + " successfully saved.", "Report", Messagebox.OK,
					Messagebox.INFORMATION);
			reportManagementQueue.publish(new Event("refreshList"));
		} catch (Exception e) {
			Messagebox.show("An internal error raised while saving current report.", "Error on saving report",
					Messagebox.OK, Messagebox.INFORMATION);
			return;
		}
	}

	private void loadReport(int reportId) throws Exception {
		report = ReportService.getReport(reportId); // The report is detached from Hibernate control
		reportIdLbl.setValue(String.valueOf(reportId));
		reportNameTxt.setValue(this.report.getName());
		reportTitleTxt.setValue(this.report.getTitle());
		reportDescriptionTxt.setValue(this.report.getDescription());
		reportCreationLbl.setValue(new SimpleDateFormat("YYYY-MM-dd").format(report.getCreationDate()));
		reportPublishedChk.setChecked(this.report.isPublished());
		calcSelOrderCol.setSortAscending(new CalculationOrderComparator(true));
	}

	@SuppressWarnings("unchecked")
	private void loadReportCalculations() {
		// Clear calculation rows
		while (!calcSelRows.getChildren().isEmpty())
			calcSelRows.removeChild(calcSelRows.getFirstChild());

		// Iterate current calculations
		String reportType = report.getType().getName();
		for (ReportCalculation reportCalculation : report.getCalculations()) {
			Row row = new Row();
			row.setDroppable("true");
			row.setDraggable("true");
			row.addEventListener("onDrop", new EventListener() {
				public void onEvent(Event event) throws Exception {
					Component dragged = ((DropEvent) event).getDragged();
					Component droped = ((DropEvent) event).getTarget();
					int from = children.indexOf(dragged);
					int to = children.indexOf(droped);
					if (from > to)
						droped.getParent().insertBefore(dragged, droped);
					else {
						if (droped.getNextSibling() != null)
							droped.getParent().insertBefore(dragged, droped.getNextSibling());
						else
							droped.getParent().appendChild(dragged);
					}

					reindexCalculationsOrder();
					loadReportCalculations();
					calcSelOrderCol.sort(true, true);
					reportManagementQueue.publish(new Event("reportCalculationDragged", null, report.getId()));
				}
			});
			row.setTooltip("thumbnailPopup, position=after_center, delay=700");
			row.setAttribute("calculationId", reportCalculation.getCalculation().getId());
			row.setSclass("report-calculation-row");
			Cell cell = new Cell();
			cell.appendChild(new Label(String.valueOf(reportCalculation.getId())));
			row.appendChild(cell);
			cell = new Cell();
			Label dragLbl = new Label();
			dragLbl.setSclass("z-icon-drag");
			cell.appendChild(dragLbl);
			row.appendChild(cell);
			cell = new Cell();
			Label calcIdLbl = new Label(String.valueOf(reportCalculation.getCalculation().getId()));
			calcIdLbl.setSclass("report-calculation-id-label");
			cell.appendChild(calcIdLbl);
			row.appendChild(cell);
			cell = new Cell();
			Label orderLbl = new Label(String.valueOf(reportCalculation.getCalcOrder()));
			orderLbl.setSclass("report-calculation-order-label");
			cell.appendChild(orderLbl);
			row.appendChild(cell);
			cell = new Cell();
			Label pathLbl = new Label(reportCalculation.getCalcPath());
			pathLbl.setSclass("report-calculation-path-label");
			pathLbl.setTooltip(reportCalculation.getCalcPath());
			cell.appendChild(pathLbl);
			row.appendChild(cell);
			cell = new Cell();
			Textbox textbox = new Textbox(reportCalculation.getTitle());
			textbox.addEventListener("onChange", new EventListener() {
				@Override
				public void onEvent(Event event) throws Exception {
					Textbox textbox = (Textbox) event.getTarget();
					long calculationId = Long.parseLong(
							((Label) ((Row) event.getTarget().getParent().getParent()).getFirstChild().getFirstChild()).getValue());
					ReportCalculation reportCalculation = report.getReportCalculationByCalculationId(calculationId);
					reportCalculation.setTitle(textbox.getText());
					reportManagementQueue.publish(new Event("reportTextFieldChanged", null, report.getId()));
				}
			});
			textbox.setHflex("1");
			textbox.setSclass("form-control form-control-sm");
			cell.appendChild(textbox);
			row.appendChild(cell);
			
			cell = new Cell();
			Button removeCalculationBtn = new Button();
			removeCalculationBtn.setSclass("btn btn-sm btn-secondary btn-block z-icon-times");
			removeCalculationBtn.setTooltip("Remove from list");
			removeCalculationBtn.addEventListener("onClick", new EventListener() {
				@Override
				public void onEvent(Event event) throws Exception {
					if (!"Reaction Graph".equals(reportType)) {
						deleteCalculation(reportCalculation.getCalculation().getId());
					} else {
						reportManagementQueue.publish(new Event("isCalculationRemovable", null, reportCalculation));
					}
				}
			});
			cell.appendChild(removeCalculationBtn);

			if ("Reaction Graph".equals(reportType)) {
				generateBtn.setLabel("Export");
				row.addEventListener("onClick", new EventListener() {
					@Override
					public void onEvent(Event event) throws Exception {
						reportManagementQueue.publish(new Event("selectAllNodesWithThisCalculationId", null, reportCalculation));
					}
				});
				cell = applyReactionGraphRow(cell, reportCalculation);
				cell.setSclass("d-flex");
			}


			row.appendChild(cell);
			calcSelRows.appendChild(row);
		}
		children = calcSelRows.getChildren();
	}

	private Cell applyReactionGraphRow(Cell cell, ReportCalculation reportCalculation){
				Button addCalculToGraphBtn = new Button();
				addCalculToGraphBtn.setIconSclass("fas fa-long-arrow-alt-right");
				addCalculToGraphBtn.setSclass("btn btn-sm btn-secondary btn-block z-forward");
				addCalculToGraphBtn.setTooltiptext("Add the node to the graph");
				addCalculToGraphBtn.addEventListener("onClick", new EventListener() {
					@Override
					public void onEvent(Event event) throws Exception {
						String reportId = reportIdLbl.getValue();
						reportManagementQueue.publish(new Event("addCalculationToReactionGraph", null, reportCalculation));
					}
				});
				addCalculToGraphBtn.setStyle("margin:0em 0em 0em 1em; padding-top:0px; padding-bottom:0px;");


				calcPath.setHflex("90");
				calcTitle.setHflex("30");
				calcBtn.setHflex("30");

				cell.appendChild(addCalculToGraphBtn);
				calcId.setVisible(true);
				calcSelOrderCol.setVisible(false);
				return cell;
	}

	private void deleteCalculation(long calculationId) {
		removeCalculation(calculationId);
		reindexCalculationsOrder();
		loadReportCalculations();
		calcSelOrderCol.sort(true, true);
		reportManagementQueue.publish(new Event("reportChanged", null, report.getId()));
	}

	private void loadSpecificReportZulPage() {
		reportTypeInc.setDynamicProperty("parent", reportBaseWindow);
		reportTypeInc.setSrc(Constants.REPORT_TEMPLATES_FOLDER + report.getType().getAssociatedZul());
	}

	private void reindexCalculationsOrder() {
		HashMap<Long, Integer> calcIdToOrder = new HashMap<>();
		for (int i = 0; i < calcSelRows.getChildren().size(); i++) {
			Row row = (Row) calcSelRows.getChildren().get(i);
			Long id = (Long) row.getAttribute("calculationId");
			calcIdToOrder.put(id, i + 1);
		}

		for (ReportCalculation reportCalculation : report.getCalculations()) {
			if (!calcIdToOrder.containsKey(reportCalculation.getCalculation().getId()))
				calcIdToOrder.put(reportCalculation.getCalculation().getId(), calcIdToOrder.size() + 1);
			reportCalculation.setCalcOrder(calcIdToOrder.get(reportCalculation.getCalculation().getId()));
		}
		logger.error("ReportCalculations: " + report.getCalculations().size());
		report.reorderCalculations();
	}

	private void removeCalculation(long removeId) {
		for (ReportCalculation reportCalculation : report.getCalculations()) {
			if (reportCalculation.getCalculation().getId() == removeId) {
				report.getCalculations().remove(reportCalculation);
				break;
			}
		}
		for (int i = 0; i < calcSelRows.getChildren().size(); i++) {
			Row row = (Row) calcSelRows.getChildren().get(i);
			long id = (Long) row.getAttribute("calculationId");
			if (removeId == id) {
				calcSelRows.removeChild(row);
				break;
			}
		}
	}

	private void appendReportCalculation(long calculationId) {
		ReportCalculation newReportCalculation = new ReportCalculation();
		newReportCalculation.setCalcOrder(report.getCalculations().size() + 1);
		newReportCalculation.setTitle("-");
		newReportCalculation.setCalculation(CalculationService.getById(calculationId));
		newReportCalculation.setReport(report);
		report.getCalculations().add(newReportCalculation);
	}

	private class CalculationOrderComparator implements Comparator {

		private boolean isAscending = false;

		public CalculationOrderComparator(boolean ascending) {
			isAscending = ascending;
		}

		public int compare(Object o1, Object o2) {
			int leftValue = Integer
					.valueOf(((Label) ((Component) o1).query(".report-calculation-order-label")).getValue());
			int rightValue = Integer
					.valueOf(((Label) ((Component) o2).query(".report-calculation-order-label")).getValue());
			if (isAscending)
				return leftValue - rightValue;
			else
				return rightValue - leftValue;
		}
	}

	class ReportEventListener implements EventListener<Event> {
		@Override
		public void onEvent(Event event) throws Exception {
			logger.info("Event raised : " + event.getName());

			if (event.getName().equals("loadCalculationsGraph")) {
				Map<String, Object> data = (Map<String, Object>)event.getData();
				int reportId = (int) data.get("reportId");
				if (reportId != report.getId())
					return;
				Set<Entity> calculationsList =  (Set<Entity>) data.get("calculationsList");
				printListCalculations(calculationsList);
			}
			if (event.getName().equals("deleteReportCalculation")) {
				long calculationId = (long) event.getData();
				deleteCalculation(calculationId);
			}
			if (event.getName().equals("closeReport")) {
				int reportId = (int) event.getData();
				if (reportId == report.getId()) {
					reportManagementQueue.unsubscribe(reportEventListener);
					displayQueue.unsubscribe(displayEventListener);
				}
			} else if (event.getName().equals("saveAndCloseReport")) {
				logger.info("Event raised : " + event.getName());
				int reportId = (int) event.getData();
				if (reportId == report.getId())
					onSaveAndCloseBtnClick();
			}
		}
	}

	class DisplayEventListener implements EventListener<Event> {
		@Override
		public void onEvent(Event event) throws Exception {
			switch (event.getName()) {
				case "sizeChanged":
					setupLayout();
			}
		}
	}

	class DeleteReportEventListener implements EventListener<Event> {
		int reportId = 0;

		public DeleteReportEventListener(int reportId) {
			this.reportId = reportId;
		}

		@Override
		public void onEvent(Event e) {
			if (Messagebox.ON_YES.equals(e.getName()))
				try {
					ReportService.deleteReport(reportId);
					reportManagementQueue.publish(new Event("deleteReport", null, reportId));
				} catch (Exception e1) {
					logger.error(e1.getMessage());
				}
		}
	}

}

/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class CPro extends HibernateService {
		
	private static final long serialVersionUID = 1L;
    
    @Override
    public void executePostService(HttpServletResponse resp, Map<String, String> params, String userName,
            Integer userId)
            throws SQLException, IOException, BrowseCredentialsException {
        try {               
            String path = params.get(PARAMETER_PATH);           
            if(path.length() > 512)
                throw new Exception("Project path exceeds maximum length (256)");

            Project p = new Project();
            p.setParentPath(path);
            p.setName(truncate(Main.normalizeField(params.get(PARAMETER_NAME)),64));
            p.setDescription(truncate(params.get(PARAMETER_DESCRIPTION),304));
            p.setConceptGroup(truncate(params.get(PARAMETER_CG) + "",64));
            p.setLinuxPermissions(buildPermissions(path));          
            p.setOwner(UserService.getUserId());
            p.setGroup(UserService.getMainGroupId());
            ProjectService.add(p);
            returnOK(resp);                              
        }catch(Exception e) {
            returnKO(resp, PROJECT_ERROR, e.getMessage());
        }
    }

	private String buildPermissions(String path) throws BrowseCredentialsException {
		if(path.equals(Paths.getFullPath(Constants.BASE_PATH, UserService.getUserName())))	//Base path
			return "rw----";
		else {
			return ProjectService.getByPath(path).getLinuxPermissions();
		}
	}
}

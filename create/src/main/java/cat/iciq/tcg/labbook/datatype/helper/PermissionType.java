package cat.iciq.tcg.labbook.datatype.helper;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.BitSet;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.mapping.Array;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import com.google.common.primitives.Ints;

/**
 * Permission conversion helper class, turns a six bit array into a r/w/- string.
 * Ex: 110000 becomes rw---- and reverse.
 * @author malvarez
 *
 */
public class PermissionType  implements UserType, ParameterizedType {

    @Override
    public int[] sqlTypes() {
        return new int[]{Types.BINARY};
    }

    @Override
    public Class returnedClass() {
        return BitSet.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y)
            return true;
        if (x == null || y == null)
            return false;
        return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        byte[] bytes = rs.getBytes(names[0]);
        BitSet bitSet = new BitSet(bytes.length);
        bitSet.set(bytes.length, false);
        for (int i = 0; i < bytes.length; i++) {
            if((bytes[i] & (0b1)) == 1)
                bitSet.set(i);
        }
        return bitSet;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {        
        if (value == null) {
            st.setNull(index, Types.BINARY);
          } else {
              BitSet bitSet = (BitSet) value;
              
//              byte[] bytes = new byte[]{0};
//              for (int i = 0; i < 6; i++) {
//                  if (bitSet.get(i)) {
//                      bytes[0] |= (1 << 5-i);
//                  }
//              }
              st.setBytes(index, bitSet.toByteArray());              
          }
        
//        if (value == null) {
//            st.setNull(index, Types.INTEGER);
//        } else {            
//            BitSet bitSet = (BitSet) value;
//            bitSet.set(6);
//            int intValue = 0;
//            for (int i = 0; i < 6; i++) {                                
//                intValue += bitSet.get(i) ? (1L<< (5-i)): 0;
//            }
//            st.setInt(index, intValue);
//        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if (value == null)
            return null;
        BitSet original = (BitSet) value;
        BitSet copy = new BitSet(6);
        copy.or(original);
        return copy;
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }

    @Override
    public void setParameterValues(Properties parameters) {
        // Optional: If you need to set any additional parameters from the mapping
        // configuration, you can do it here.
        // You can access the parameters using the provided Properties object.
        // For example:
        // String additionalParameter = parameters.getProperty("additionalParameter");
        // Perform any necessary logic based on the additional parameter.
    }
}
  
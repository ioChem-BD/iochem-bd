package cat.iciq.tcg.labbook.datatype.helper.fingerprint;

import java.io.Serializable;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

public class Feature implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[]{Types.ARRAY};
    }

    @Override
    public Class returnedClass() {
        return Integer[].class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x instanceof Integer[] && y instanceof Integer[]) {
            List<Integer> lx = Arrays.asList((Integer[])x);
            List<Integer> ly = Arrays.asList((Integer[])y);
            return lx.size() == ly.size() && lx.containsAll(ly) && ly.containsAll(lx);            
        } else {
            return false;
        }
    }
 
    @Override
    public int hashCode(Object x) throws HibernateException {
        return Arrays.hashCode((int[])x);
    }
 
    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
            throws HibernateException, SQLException {
        Array array = rs.getArray(names[0]);
        return array != null ? array.getArray() : null;
    }
 
    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        if (value != null && st != null) {
            Integer[] aint = (Integer[])value;
            Integer[] aInt = new Integer[aint.length];
            Arrays.setAll(aInt, i -> aint[i]);            
            Array array = session.connection().createArrayOf("integer", aInt);
            st.setArray(index, array);
        } else {
            st.setNull(index, sqlTypes()[0]);
        }
    }
 
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        Integer[] a = (Integer[])value;
        return Arrays.copyOf(a, a.length);
    }
 
    @Override
    public boolean isMutable() {
        return false;
    }
 
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }
 
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }
 
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}

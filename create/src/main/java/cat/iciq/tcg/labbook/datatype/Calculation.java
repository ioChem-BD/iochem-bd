/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="calculations", uniqueConstraints = @UniqueConstraint(columnNames = "id", name = "id"))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Calculation extends PublicableEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "calculations_id_seq")
    @SequenceGenerator(name = "calculations_id_seq",   sequenceName = "calculations_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;
    
    @ManyToOne
    @JoinColumn(name = "type_id", foreignKey = @ForeignKey(name = "calculations_type_id_fkey"), nullable = false)
    private CalculationType type;
    
    @Column(name = "mets_xml")
    @Basic(fetch = FetchType.LAZY)
    private String metsXml;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CalculationType getType() {
        return type;
    }

    public void setType(CalculationType type) {
        this.type = type;
    }

    public String getMetsXml() {
        return metsXml;
    }

    public void setMetsXml(String metsXml) {
        this.metsXml = metsXml;
    }

}

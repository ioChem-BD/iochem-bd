/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.zk.ui.HibernateUtil;

public class ReportTypeService {

	private static final Logger log = LogManager.getLogger(ReportTypeService.class);	
	private static HashMap<Integer, ReportType> reportTypeById;

	static {
       loadReportTypes();          
	}

	private static void loadReportTypes() {	    
		reportTypeById = new HashMap<>();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<ReportType> criteriaQuery = criteriaBuilder.createQuery(ReportType.class);
            Root<ReportType> root = criteriaQuery.from(ReportType.class);
            criteriaQuery.select(root);
            List<ReportType> results = session.createQuery(criteriaQuery).getResultList();
            for(ReportType type : results) {
                reportTypeById.put(type.getId(), type);                  
            }
        } catch (Exception e) {
            log.error("Exception raised loading calculation types.");
            log.error(e.getMessage());
        }
	}
	
	public static List<ReportType> getActiveReportTypes() {		
		ArrayList<ReportType> activeReportTypes = new ArrayList<ReportType>();
		for(ReportType reportType: reportTypeById.values())
			if(reportType.isEnabled())
				activeReportTypes.add(reportType);			
		return activeReportTypes;		
	}
	
	public static List<ReportType> getAllReportTypes() {
		return new ArrayList<ReportType>(reportTypeById.values());				
	}
	
	public static ReportType getReportTypeById(int id) throws Exception {		
		if(reportTypeById.containsKey(id))
			return reportTypeById.get(id);
	    		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
		    CriteriaBuilder builder = session.getCriteriaBuilder();
	        CriteriaQuery<ReportType> criteriaQuery = builder.createQuery(ReportType.class);
	        Root<ReportType> root = criteriaQuery.from(ReportType.class);
	        
	        criteriaQuery.select(root).where(builder.equal(root.get("id"), id));
	        ReportType type = session.createQuery(criteriaQuery).getSingleResult();
	        reportTypeById.put(type.getId(), type);
	        return type;
		}catch(Exception e) {
		    log.error(e.getMessage());
		    return null;
		}
	}

	public static ReportType getReportTypeByName(String name) throws Exception {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ReportType> criteriaQuery = builder.createQuery(ReportType.class);
			Root<ReportType> root = criteriaQuery.from(ReportType.class);
			
			criteriaQuery.select(root).where(builder.equal(root.get("name"), name));
			ReportType type = session.createQuery(criteriaQuery).uniqueResult();
			
			if (type != null) {
				reportTypeById.put(type.getId(), type);
			}
			
			return type;
		} catch (Exception e) {
			log.error("Error searching for ReportType by name: " + name);
			log.error(e.getMessage());
			throw e;
		}
	}
}

package cat.iciq.tcg.labbook.zk.ui;

import java.io.File;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.Query;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.Action;
import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.CalculationType;
import cat.iciq.tcg.labbook.datatype.CalculationTypeFile;
import cat.iciq.tcg.labbook.datatype.MolecularFingerprint;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;

// Source: https://www.zkoss.org/wiki/ZK_Developer's_Reference/Integration/Persistence_Layer/Hibernate
public class HibernateUtil {
    private static final Logger log = LogManager.getLogger(HibernateUtil.class);

    private static SessionFactory sessionFactory = null;

    private static FullTextEntityManager fem;
    private static EntityManager em;

    private HibernateUtil() {
        throw new IllegalStateException("Utility class");
    }

    static {
      try {
          Configuration configuration = buildConfiguration();
          StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
          sessionFactory = configuration.buildSessionFactory(builder.build());
          startSearchIndexes();
      } catch(Throwable ex) {
          throw new ExceptionInInitializerError(ex);          
      }
    }

    private static Configuration buildConfiguration() {
        Configuration configuration = new Configuration();        
        configuration.setProperty("hibernate.connection.provider_class", "org.hibernate.hikaricp.internal.HikariCPConnectionProvider");
        configuration.setProperty("hibernate.hikari.dataSourceClassName","org.postgresql.ds.PGSimpleDataSource");
        configuration.setProperty("hibernate.hikari.dataSource.url", buildConnectionUrl());                                
        configuration.setProperty("hibernate.hikari.dataSource.user", CustomProperties.getProperty("create.database.user"));     
        configuration.setProperty("hibernate.hikari.dataSource.password", CustomProperties.getProperty("create.database.pwd"));
        
        configuration.setProperty("hibernate.hikari.maximumPoolSize", "10");
        configuration.setProperty("hibernate.hikari.idleTimeout", "30000");
        configuration.setProperty("hibernate.hikari.connectionTimeout", "20000");
        configuration.setProperty("hibernate.hikari.validationTimeout", "5000");
        
        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        configuration.setProperty("hibernate.connection.charSet", "UTF-8");
        configuration.setProperty("hibernate.hbm2ddl.auto", "validate");
        // Prevent lazy loading outside of a transaction
        configuration.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        configuration.setProperty("hibernate.current_session_context_class", "org.hibernate.context.internal.ThreadLocalSessionContext");
      
        configuration.setProperty("hibernate.jdbc.batch_size", "50");        
        configuration.setProperty("hibernate.cache.use_second_level_cache", "true");
        configuration.setProperty("hibernate.cache.region.factory_class",  "org.hibernate.cache.jcache.JCacheRegionFactory");
        configuration.setProperty("hibernate.javax.cache.provider", "org.ehcache.jsr107.EhcacheCachingProvider");
        configuration.setProperty("hibernate.javax.cache.uri", "file://" + WebApps.getCurrent().getRealPath("/WEB-INF/ehcache.xml"));
        // Uncomment to debug hibernate queries
        // configuration.setProperty("hibernate.show_sql", "true");          
        // configuration.setProperty("hibernate.format_sql", "true");

        // Add annotated classes
        configuration.addAnnotatedClass(Action.class);
        configuration.addAnnotatedClass(Calculation.class);
        configuration.addAnnotatedClass(CalculationFile.class);
        configuration.addAnnotatedClass(CalculationType.class);
        configuration.addAnnotatedClass(CalculationTypeFile.class);
        configuration.addAnnotatedClass(MolecularFingerprint.class);
        configuration.addAnnotatedClass(Project.class);
        configuration.addAnnotatedClass(Report.class);
        configuration.addAnnotatedClass(ReportCalculation.class);
        configuration.addAnnotatedClass(ReportType.class);
        
        String extraParameters = CustomProperties.getProperty("database.extra.parameters");
        if(extraParameters != null && !extraParameters.isEmpty())
            configuration.setProperty("hibernate.connection.requireSSL", "true");

        try {            
            String searchIndexPath = FileService.getCreatePath("/search");
            new File(searchIndexPath).mkdir();                
            configuration.setProperty("hibernate.search.default.indexBase", searchIndexPath);    
        }catch(Exception e) {
            log.error("Could not generate Hibernate search index folder.");
        }        
        
        return configuration;
    }
    
    private static String buildConnectionUrl() {
        StringBuilder url = new StringBuilder();
        url.append("jdbc:postgresql://");
        url.append(CustomProperties.getProperty("create.database.host").trim());
        url.append(":");
        url.append(CustomProperties.getProperty("create.database.port").trim());
        url.append("/");
        url.append(CustomProperties.getProperty("create.database.name").trim());        
        return url.toString();
    }
    
    public static SessionFactory getSessionFactory() {
       return sessionFactory;
    }
    
    public static QueryBuilder getQueryBuilder(Class entityType){
        return fem.getSearchFactory().buildQueryBuilder().forEntity(entityType).get();
    }
    
    public static FullTextQuery createFullTextQuery(Query query, Class entityType) {
        return fem.createFullTextQuery(query, entityType);
    }
    
    private static void startSearchIndexes() throws Exception {
        em = sessionFactory.createEntityManager();
        fem = org.hibernate.search.jpa.Search.getFullTextEntityManager(em);
        try {
            fem.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            log.error("Error starting search indexes.");
            log.error(e.getMessage());
        }
    }
 
    public static EntityManager getEntityManager() {
        return em;
    }
    
    public static void stopSearchIndexes() {
        em.close();
    }
 
}

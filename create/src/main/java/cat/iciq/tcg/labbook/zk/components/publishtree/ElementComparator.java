/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree;

import java.io.Serializable;
import java.util.Comparator;

import org.zkoss.zul.TreeNode;

public class ElementComparator implements Comparator, Serializable {

	private boolean asc = true;
 
    public ElementComparator(boolean asc) {
        this.asc = asc;      
    }
  
    public boolean isAscending() {
    	return this.asc;
    }

	@Override
	public int compare(Object o1, Object o2) {		
		Element element1 = ((TreeNode<Element>) o1).getData();
        Element element2 = ((TreeNode<Element>) o2).getData();
        int order1 = element1.getOrder() <= 0 ? 0 : element1.getOrder();
		int order2 = element1.getOrder() <= 0 ? 0 : element2.getOrder();			 
		return (order1 - order2) * (asc ? 1 : -1); 		
	}


}

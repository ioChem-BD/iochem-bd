package cat.iciq.tcg.labbook.web.cas;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import org.zkoss.zk.ui.WebApps;
import cat.iciq.tcg.labbook.web.controllers.ErrorResponse;
import com.ibm.icu.impl.InvalidFormatException;
import cat.iciq.tcg.labbook.web.controllers.ApiJWT;

public class FilterRestApi implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {      
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        if (httpRequest.getMethod().equalsIgnoreCase("OPTIONS")) {
            httpResponse.setStatus(HttpServletResponse.SC_OK);
            return; 
        }

        String authorizationHeader = httpRequest.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);
            try {
                String key = (String) WebApps.getCurrent().getAttribute("keyJWT");
                if(!ApiJWT.verifyJWT(token, key))
                    throw new InvalidFormatException("Provided JWT token is not valid.");
                UserService.loadUserInfoFromApi((HttpServletRequest)request, token);
                chain.doFilter(request, response);
            } catch (Exception e) {
                new ErrorResponse(HttpServletResponse.SC_UNAUTHORIZED,"Unauthorized",  "Invalid JWT token").sendErrorResponse(httpResponse);
            }
        } else {
            new ErrorResponse(HttpServletResponse.SC_UNAUTHORIZED,"Unauthorized",  "Token bearer is empty").sendErrorResponse(httpResponse);
        }  
    }      
    

    @Override
    public void destroy() {
        // Cleanup code, if needed
    }
}

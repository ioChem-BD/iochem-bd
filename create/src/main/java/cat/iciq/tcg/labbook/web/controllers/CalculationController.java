package cat.iciq.tcg.labbook.web.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.UserService;
import cat.iciq.tcg.labbook.shell.datatype.EntityDTO;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.core.HibernateService;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class CalculationController extends HibernateService {
    @Override
    public void executeGetService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {

        if (params.get(PARAMETER_ID) == null || !params.get(PARAMETER_ID).matches("\\d+")) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST ,PROJECT_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(response);
            return;      
        } 
        Calculation c = CalculationService.getById(Long.parseLong(params.get(PARAMETER_ID)));
        if (c == null) {
            new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, CALCULATION_ERROR, BAD_PATH_ERROR).sendErrorResponse(response);
            return;
        } else if (!PermissionService.hasPermissionOnCalculation(c.getId(), Permissions.READ)) {
            new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "Unauthorized", PERMISSION_ERROR).sendErrorResponse(response);
            return;
        }

        Project p = ProjectService.getByPath(c.getParentPath());
        String permissions = p.getLinuxPermissions();
        String owner = UserService.getUserNameById(p.getOwner());
        String group = UserService.getGroupNameById(p.getGroup());
        String conceptGroup = p.getConceptGroup();
        EntityDTO entity = fromCalculation(c, permissions, owner, group, conceptGroup);
        sendObj(response, entity);
    }

    @Override
    public void executePostService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, BAD_REQUEST_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(response);
    }

    @Override
    public void executePutService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        if(params.size() == 0){
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST,"Invalid JSON","Invalid JSON.").sendErrorResponse(response);
            return;
        }
        try {
            long calcId = Long.parseLong(params.get(PARAMETER_ID));
            Calculation calculation = CalculationService.getById(calcId);

            if (calculation == null) {
                new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, CALCULATION_ERROR, CALC_NON_EXIST_ERROR).sendErrorResponse(response);
                return;
            }
            String destinationPath = ((params.get(PARAMETER_PATH) == null) ? calculation.getParentPath() : params.get(PARAMETER_PATH));
            if (!isValidMovePath(UserService.getUserName(), destinationPath) ||
            !PermissionService.hasPermissionOnProject(destinationPath, Permissions.WRITE)) {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, PRO_MOD_ERROR, INVALID_PATH).sendErrorResponse(response); 
                return;
            }
            if (!PermissionService.hasPermissionOnProject(calculation.getParentPath(), Permissions.WRITE)) {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, PERMISSION_ERROR, WRITE_PERMISSION_ERROR).sendErrorResponse(response);
            }
            saveCalculation(response, params, calculation);
        } catch (NumberFormatException e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, BAD_REQUEST_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(response);
        } catch (Exception e) {
            new ErrorResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, LOAD_ERROR, e.getMessage()).sendErrorResponse(response);
        }
    }

    private void saveCalculation(HttpServletResponse response, Map<String, String> params, Calculation calculation)
            throws Exception {
        String name = ((params.get(PARAMETER_NAME) == null || params.get(PARAMETER_NAME).isBlank())
                ? calculation.getName()
                : truncate(Main.normalizeField(params.get(PARAMETER_NAME)), 64));
        String destinationPath = ((params.get(PARAMETER_PATH) == null) ? calculation.getParentPath() : params.get(PARAMETER_PATH));
        String description = ((params.get(PARAMETER_DESCRIPTION) == null) ? calculation.getDescription()
                : truncate(params.get(PARAMETER_DESCRIPTION), 304));

        calculation.setName(name);
        calculation.setDescription(description);
        calculation.setParentPath(destinationPath);

        if (destinationPath != null) {
            if (destinationPath.isBlank())
                throw new Exception("Invalid project path");

            if (destinationPath.length() > 512)
                throw new Exception("Project path exceeds maximum length (512)");

            if (!isPathOwnerByUser(destinationPath)
                    || !PermissionService.hasPermissionOnProject(destinationPath, Permissions.WRITE)) {
                returnKO(response, PRO_MOD_ERROR, INVALID_PATH);
                return;
            }
            CalculationService.moveOverProject(calculation, ProjectService.getByPath(destinationPath));
        } else {
            CalculationService.update(calculation);
        }
        returnOK(response);
    }

    @Override
    public void executeDeleteService(HttpServletResponse response, Map<String, String> params, String userName,
            Integer userId) throws SQLException, IOException, BrowseCredentialsException {
        try {

            if (params.get(PARAMETER_ID) == null || !params.get(PARAMETER_ID).matches("\\d+")) {
                new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, PROJECT_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(response);
                return;      
            } 

            Long calculationId = Long.parseLong(params.get(PARAMETER_ID));
            if (CalculationService.getById(calculationId) == null) {
                new ErrorResponse(HttpServletResponse.SC_NOT_FOUND, CALCULATION_ERROR, NOT_FOUND_ERROR).sendErrorResponse(response);
                return;               
            }
            if (PermissionService.hasPermissionOnCalculation(calculationId, Permissions.DELETE)) {
                CalculationService.delete(calculationId, true);
                returnOK(response);
            } else {
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "Unauthorized", PERMISSION_ERROR).sendErrorResponse(response);
            }
        } catch (NumberFormatException e) {
            new ErrorResponse(HttpServletResponse.SC_BAD_REQUEST, BAD_REQUEST_ERROR, BAD_REQUEST_ERROR).sendErrorResponse(response);
        } catch (Exception e) {
            returnKO(response, CALCULATION_ERROR, e.getMessage());
        }
    }

    private boolean isPathOwnerByUser(String destinationPath) {
        return destinationPath
                .startsWith(GeneralConstants.DB_ROOT + UserService.getUserName() + GeneralConstants.PARENT_SEPARATOR);
    }

    private boolean isValidMovePath(String username, String destinationPath) {
        return destinationPath.startsWith(GeneralConstants.DB_ROOT + username);
    }
    
}
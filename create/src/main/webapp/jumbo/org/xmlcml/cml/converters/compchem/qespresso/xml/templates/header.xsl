<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="HEADER" >
        <xsl:param name="isVerbose" tunnel="yes"/>

        <xsl:if test="$isVerbose">
            <comment class="example.input" id="XXXX">
                <HEADER>
                    <FORMAT NAME="QEXML" VERSION="1.4.0"/>
                    <CREATOR NAME="PWSCF" VERSION="6.1"/>
                </HEADER>                
            </comment>
        </xsl:if>  
     
        <module id="header" cmlx:templateRef="header">
            <scalar dataType="xsd:string" dictRef="cc:program">QuantumEspresso</scalar>
            <scalar dataType="xsd:string" dictRef="cc:programVersion"><xsl:value-of select="./CREATOR[@NAME='PWSCF']/@VERSION"/></scalar>            
            <scalar dataType="xsd:string" dictRef="qex:formatqexml"><xsl:value-of select="./FORMAT[@NAME='QEXML']/@VERSION"/></scalar>
        </module>
     
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="XXXX">                
                    <module id="header" cmlx:templateRef="header">
                        <scalar dataType="xsd:string" dictRef="cc:program">QuantumEspresso</scalar>
                        <scalar dataType="xsd:string" dictRef="cc:programVersion">6.1</scalar>
                        <scalar dataType="xsd:string" dictRef="qex:formatqexml">1.4.0</scalar>
                    </module>                                               
            </comment>
        </xsl:if>  
    </xsl:template>
</xsl:stylesheet>
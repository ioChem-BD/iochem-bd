<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<template id="zerofield" name="Zero Field Splitting Tensor" pattern="\s*\-+\s*$\s*ZERO-FIELD.SPLITTING\sTENSOR\s*$\s*\-+\s*" endPattern="\s+\*{5,}.*" endPattern2="\-+.*$\S+.*" endPattern3="\sTimings.*" endPattern4="~" endOffset="-1" repeat="*" >
	<comment class="example.input" id="zerofield">
---------------------------
ZERO-FIELD-SPLITTING TENSOR
---------------------------

raw-matrix : 
  -0.505591     1.149911     0.494847  
   1.149911     9.812988     2.559488  
   0.494847     2.559488    -0.004718  
diagonalized D matrix : 
  -0.843288    -0.431797    10.577764  

  -0.712387     0.692993     0.110749  
  -0.090285    -0.246999     0.964800  
   0.695955     0.677313     0.238526  


Direction X=1 Y=0 Z=2
D   =    11.215307  cm**-1
E/D =     0.018345

SPIN-SPIN-PART
                   0          1          2    
      0      -0.523192   0.162711   0.065838
      1       0.162711   0.970351   0.376727
      2       0.065838   0.376727  -0.447159
Exchange and Coulomb parts of SS (raw)
Coulomb
                   0          1          2    
      0      -0.467892   0.139989   0.058729
      1       0.139989   0.851749   0.321647
      2       0.058729   0.321647  -0.383857
Exchange
                   0          1          2    
      0      -0.055300   0.022722   0.007109
      1       0.022722   0.118602   0.055080
      2       0.007109   0.055080  -0.063302
One-, two-, three- and four-center SS contributions (raw)
One-Center            (AA|AA):
                   0          1          2    
      0      -0.528215   0.165657   0.067391
      1       0.165657   0.982845   0.383303
      2       0.067391   0.383303  -0.454630
Two-Center-Coulomb    (AA|BB):
                   0          1          2    
      0      -0.003301   0.002183  -0.000434
      1       0.002183   0.010516   0.004478
      2      -0.000434   0.004478  -0.007215
Two-Center-Exchange   (AB|BA):
                   0          1          2    
      0       0.000007  -0.000050  -0.000013
      1      -0.000050  -0.000203  -0.000142
      2      -0.000013  -0.000142   0.000196
Two-Center-hybrid     (AA|AB):
                   0          1          2    
      0       0.009208  -0.004959  -0.002683
      1      -0.004959  -0.023086  -0.010764
      2      -0.002683  -0.010764   0.013878
Three-Center-Coulomb  (AA|BC):
                   0          1          2    
      0      -0.000752  -0.000192   0.001525
      1      -0.000192   0.000051  -0.000314
      2       0.001525  -0.000314   0.000702
Three-Center-Exchange (AB|AC):
                   0          1          2    
      0      -0.000127   0.000073   0.000102
      1       0.000073   0.000293   0.000191
      2       0.000102   0.000191  -0.000166
Four-Center           (AB|CD):
                   0          1          2    
      0      -0.000011  -0.000000  -0.000051
      1      -0.000000  -0.000064  -0.000024
      2      -0.000051  -0.000024   0.000075
ALPHA-PART
                   0          1          2    
      0      -7.930785  -0.108817   0.035661
      1      -0.108817  -8.147987  -0.153174
      2       0.035661  -0.153174  -7.725891
BETA-PART
                   0          1          2    
      0     -14.615790   0.549274   0.188143
      1       0.549274  -9.894368   1.166510
      2       0.188143   1.166510 -14.284724
ALPHA->BETA-PART
                   0          1          2    
      0      19.143572   0.493076   0.221147
      1       0.493076  23.343588   1.093287
      2       0.221147   1.093287  19.124483
BETA->ALPHA-PART
                   0          1          2    
      0       3.420604   0.053666  -0.015942
      1       0.053666   3.541404   0.076137
      2      -0.015942   0.076137   3.328574

Individual contributions (in cm**-1)
                                   D          E
SPIN-SPIN                  :   1.62454    0.02437
  Exchange vs Coulomb
  Coulomb                  :   1.41949    0.02392
  Exchange                 :   0.20505    0.00046
  One through four center contributions:
  1-center                 :     1.647      0.025
  2-center-Coulomb         :     0.018     -0.001
  2-center-Exchange        :    -0.000      0.000
  2-center-Hybrid          :    -0.040     -0.001
  3-center-Coulomb         :    -0.000      0.002
  3-center_Exchange        :     0.001      0.000
  4-center                 :    -0.000     -0.000
SPIN-ORBIT                 :     9.591      0.181
SOMO->VMO  (alpha->alpha)  :    -0.417      0.068
DOMO->SOMO (beta ->beta )  :     5.091      0.051
SOMO->SOMO (alpha->beta )  :     4.703      0.094
DOMO->VMO  (beta ->alpha)  :     0.215     -0.032


Timings for individual modules:
	</comment>	
	<templateList>
		
		<template id="raw" pattern="raw-matrix.*" endPattern="\s*$\s*" endOffset="0">
			<record/>			
			<record repeat="*">{1_30F,o:raw}</record>
			<record repeat="*">([A-Za-z].*|\s*)</record>			
			<record>{1_30F,o:diagonalized}</record>
			<record/>
			<record repeat="*">{1_30F,o:diagonal}</record>
								


			<transform process="createMatrix" xpath="." from=".//cml:array[@dictRef='o:raw']" dictRef="o:raw"/>
			<transform process="createMatrix" xpath="." from=".//cml:array[@dictRef='o:diagonal']" dictRef="o:diagonal" />			
		</template>
		
		<template id="direction" pattern="\s*Direction.*" endPattern="\s*" endOffset="0">
			<record>\s*Direction\s*X={F,cc:x3}Y={F,cc:y3}Z={F,cc:z3}</record>
			<record>\s*D\s*={F,o:d}.*</record>
			<record>\s*E/D\s*={F,o:ed}</record>	
			
			<transform process="addChild" xpath="." elementName="cml:list" dictRef="direction"/>
			<transform process="move" xpath=".//cml:scalar" to=".//cml:list[@dictRef='direction']" />	
		</template>
				
		<template id="parts" pattern="\S.*$\s{10,}0.*" endPattern="\S+.*" endPattern2="\s*" endPattern3="~"  endOffset="0" repeat="*">
			<record>{X,o:part}</record>
			<record/>
			<record repeat="*">\s+\S+\s+{1_20F,o:value}</record>	
			<transform process="createMatrix" xpath="." from=".//cml:array" dictRef="values"/>
			<transform process="addChild" xpath="." elementName="cml:list" dictRef="parts"/>
			<transform process="move" xpath=".//cml:scalar" to=".//cml:list[@dictRef='parts']"/>
			<transform process="move" xpath=".//cml:matrix" to=".//cml:list[@dictRef='parts']"/>
			<transform process="delete" xpath=".//cml:list[not(@dictRef)]"/>
		</template>
		
		<template id="contributions" pattern="\s*Individual\scontributions.*" endPattern="\s*" endPattern2="~">
			<templateList>
				<template pattern=".*:.*" endPattern=".*" endPattern2="~" repeat="*">
					<record>{X,o:part}:{F,o:d}{F,o:e}</record>
				</template>				
			</templateList>
						
			<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='o:part']" delimiter="|" />
			<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='o:d']" />
			<transform process="createArray" xpath="." from=".//cml:scalar[@dictRef='o:e']" />
			<transform process="addChild" xpath="." elementName="cml:list" dictRef="contributions"/>
			<transform process="move" xpath=".//cml:array" to=".//cml:list[@dictRef='contributions']"/>
		</template>

	</templateList>
	
	<transform process="addUnits" xpath=".//cml:scalar[@dictRef='o:d']" value="nonsi:cm-1"/>
	<transform process="move" xpath=".//cml:module[@cmlx:templateRef='raw']//(cml:array|cml:matrix)" to="." />
	<transform process="move" xpath=".//cml:list[@dictRef='parts' or @dictRef='direction' or @dictRef='contributions']" to="."/>	
	<transform process="delete" xpath=".//cml:module[@cmlx:templateRef = 'raw' or @cmlx:templateRef= 'parts' or @cmlx:templateRef= 'direction' or @cmlx:templateRef='contributions']" />	
	<transform process="delete" xpath=".//cml:module[@cmlx:templateRef='parts']"/>	
	<transform process="delete" xpath=".//cml:module[@cmlx:templateRef='direction']"/>
	
	<comment class="example.output" id="zerofield">
      <module cmlx:templateRef="zerofield" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
         <matrix cols="3" dataType="xsd:double" dictRef="o:raw" rows="3">-0.505591 1.149911 0.494847 1.149911 9.812988 2.559488 0.494847 2.559488 -0.004718</matrix>
         <array dataType="xsd:double" dictRef="o:diagonalized" size="3">-0.843288 -0.431797 10.577764</array>
         <matrix cols="3" dataType="xsd:double" dictRef="o:diagonal" rows="3">-0.712387 0.692993 0.110749 -0.090285 -0.246999 0.964800 0.695955 0.677313 0.238526</matrix>
         <list dictRef="direction">
            <scalar dataType="xsd:double" dictRef="cc:x3">1</scalar>
            <scalar dataType="xsd:double" dictRef="cc:y3">0</scalar>
            <scalar dataType="xsd:double" dictRef="cc:z3">2</scalar>
            <scalar dataType="xsd:double" dictRef="o:d" units="nonsi:cm-1">11.215307</scalar>
            <scalar dataType="xsd:double" dictRef="o:ed">0.018345</scalar>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">SPIN-SPIN-PART</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.523192 0.162711 0.065838 0.162711 0.970351 0.376727 0.065838 0.376727 -0.447159</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Coulomb</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.467892 0.139989 0.058729 0.139989 0.851749 0.321647 0.058729 0.321647 -0.383857</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Exchange</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.055300 0.022722 0.007109 0.022722 0.118602 0.055080 0.007109 0.055080 -0.063302</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">One-Center            (AA|AA):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.528215 0.165657 0.067391 0.165657 0.982845 0.383303 0.067391 0.383303 -0.454630</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Two-Center-Coulomb    (AA|BB):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.003301 0.002183 -0.000434 0.002183 0.010516 0.004478 -0.000434 0.004478 -0.007215</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Two-Center-Exchange   (AB|BA):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">0.000007 -0.000050 -0.000013 -0.000050 -0.000203 -0.000142 -0.000013 -0.000142 0.000196</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Two-Center-hybrid     (AA|AB):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">0.009208 -0.004959 -0.002683 -0.004959 -0.023086 -0.010764 -0.002683 -0.010764 0.013878</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Three-Center-Coulomb  (AA|BC):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.000752 -0.000192 0.001525 -0.000192 0.000051 -0.000314 0.001525 -0.000314 0.000702</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Three-Center-Exchange (AB|AC):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.000127 0.000073 0.000102 0.000073 0.000293 0.000191 0.000102 0.000191 -0.000166</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">Four-Center           (AB|CD):</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-0.000011 -0.000000 -0.000051 -0.000000 -0.000064 -0.000024 -0.000051 -0.000024 0.000075</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">ALPHA-PART</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-7.930785 -0.108817 0.035661 -0.108817 -8.147987 -0.153174 0.035661 -0.153174 -7.725891</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">BETA-PART</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">-14.615790 0.549274 0.188143 0.549274 -9.894368 1.166510 0.188143 1.166510 -14.284724</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">ALPHA-&gt;BETA-PART</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">19.143572 0.493076 0.221147 0.493076 23.343588 1.093287 0.221147 1.093287 19.124483</matrix>
         </list>
         <list dictRef="parts">
            <scalar dataType="xsd:string" dictRef="o:part">BETA-&gt;ALPHA-PART</scalar>
            <matrix cols="3" dataType="xsd:double" dictRef="values" rows="3">3.420604 0.053666 -0.015942 0.053666 3.541404 0.076137 -0.015942 0.076137 3.328574</matrix>
         </list>
         <list dictRef="contributions">
            <array dataType="xsd:string" delimiter="|" dictRef="o:part" size="15">SPIN-SPIN|Coulomb|Exchange|1-center|2-center-Coulomb|2-center-Exchange|2-center-Hybrid|3-center-Coulomb|3-center_Exchange|4-center|SPIN-ORBIT|SOMO-&gt;VMO  (alpha-&gt;alpha)|DOMO-&gt;SOMO (beta -&gt;beta )|SOMO-&gt;SOMO (alpha-&gt;beta )|DOMO-&gt;VMO  (beta -&gt;alpha)</array>
            <array dataType="xsd:double" dictRef="o:d" size="15">1.62454 1.41949 0.20505 1.647 0.018 -0.000 -0.040 -0.000 0.001 -0.000 9.591 -0.417 5.091 4.703 0.215</array>
            <array dataType="xsd:double" dictRef="o:e" size="15">0.02437 0.02392 0.00046 0.025 -0.001 0.000 -0.001 0.002 0.000 -0.000 0.181 0.068 0.051 0.094 -0.032</array>
         </list>
      </module>	
	</comment>
	
</template>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <xsl:template match="./separator[@name='electronic exchange-correlation']" mode="parameters">
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="electronic.exchange.correlation">
              <separator name="electronic exchange-correlation" >
				   <i type="string" name="GGA">PE</i>
				   <i type="int" name="VOSKOWN">     0</i>
				   <i type="logical" name="LHFCALC"> F  </i>
				   <i type="string" name="PRECFOCK"></i>
				   <i type="logical" name="LSYMGRAD"> F  </i>
				   <i type="logical" name="LHFONE"> F  </i>
				   <i type="logical" name="LRHFCALC"> F  </i>
				   <i type="logical" name="LTHOMAS"> F  </i>
				   <i type="logical" name="LMODELHF"> F  </i>
				   <i name="ENCUT4O">     -1.00000000</i>
				   <i type="int" name="EXXOEP">     0</i>
				   <i type="int" name="FOURORBIT">     0</i>
				   <i name="AEXX">      0.00000000</i>
				   <i name="HFALPHA">      0.00000000</i>
				   <i name="MCALPHA">      0.00000000</i>
				   <i name="ALDAX">      1.00000000</i>
				   <i name="AGGAX">      1.00000000</i>
				   <i name="ALDAC">      1.00000000</i>
				   <i name="AGGAC">      1.00000000</i>
				   <i type="int" name="NKREDX">     1</i>
				   <i type="int" name="NKREDY">     1</i>
				   <i type="int" name="NKREDZ">     1</i>
				   <i type="logical" name="SHIFTRED"> F  </i>
				   <i type="logical" name="ODDONLY"> F  </i>
				   <i type="logical" name="EVENONLY"> F  </i>
				   <i type="int" name="LMAXFOCK">     0</i>
				   <i type="int" name="NMAXFOCKAE">     0</i>
				   <i type="logical" name="LFOCKAEDFT"> F  </i>
				   <i name="HFSCREEN">      0.00000000</i>
				   <i name="HFSCREENC">      0.00000000</i>
				   <i type="int" name="NBANDSGWLOW">     0</i>
			  </separator>
            </comment>            
        </xsl:if>
        
        <module id="electronic.exchange.correlation" cmlx:templateRef="electronic.exchange.correlation">
            <xsl:if test="exists(./i[@name='LHFCALC'])">
                <scalar dataType="xsd:string" dictRef="v:lhfcalc"><xsl:value-of select="helper:readBoolean(./i[@name='LHFCALC'])"/></scalar>                
            </xsl:if>
        	<xsl:if test="exists(./i[@name='AGGAC'])">
        		<scalar dataType="xsd:double" dictRef="v:aggac"><xsl:value-of select="helper:trim(./i[@name='AGGAC'])"/></scalar>                
        	</xsl:if>
            <xsl:if test="exists(./i[@name='HFSCREEN'])">
                <scalar dataType="xsd:double" dictRef="v:hfscreen"><xsl:value-of select="helper:trim(./i[@name='HFSCREEN'])"/></scalar>                
            </xsl:if>                  
        </module>
           
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="electronic.exchange.correlation">
            	<module id="electronic.exchange.correlation" cmlx:templateRef="electronic">
            		<scalar dataType="xsd:boolean" dictRef="v:lhfcalc">false</scalar>
            		<scalar dataType="xsd:double" dictRef="v:aggac">1.00000000</scalar>
            		<scalar dataType="xsd:double" dictRef="v:hfscreen">0.00000000</scalar>
            	</module>                    
            </comment>
        </xsl:if>
            
    </xsl:template>
    
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <xsl:template match="./separator[@name='vdW DFT']" mode="parameters">
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="vdw.dft">
                <separator name="vdW DFT" >
                    <i type="logical" name="LUSE_VDW"> F  </i>
                    <i name="Zab_VDW">     -0.84910000</i>
                    <i name="PARAM1">      0.12340000</i>
                    <i name="PARAM2">      1.00000000</i>
                    <i name="PARAM3">      0.00000000</i>
                </separator>
            </comment>            
        </xsl:if>
        
        <module id="vdW DFT" cmlx:templateRef="vdw.dft">
            <xsl:if test="exists(./i[@name='LUSE_VDW'])">
                <scalar dataType="xsd:string" dictRef="v:luseVdw"><xsl:value-of select="helper:readBoolean(./i[@name='LUSE_VDW'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='Zab_VDW'])">
                <scalar dataType="xsd:double" dictRef="v:zabVdw"><xsl:value-of select="helper:trim(./i[@name='Zab_VDW'])"/></scalar>                
            </xsl:if>                               
            <xsl:if test="exists(./i[@name='PARAM1'])">
                <scalar dataType="xsd:double" dictRef="v:param1"><xsl:value-of select="helper:trim(./i[@name='PARAM1'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='PARAM2'])">
                <scalar dataType="xsd:double" dictRef="v:param2"><xsl:value-of select="helper:trim(./i[@name='PARAM2'])"/></scalar>                
            </xsl:if>

        </module>
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="vdw.dft">
                <module id="vdW DFT" cmlx:templateRef="vdw.dft">
                    <scalar dataType="xsd:boolean" dictRef="v:luseVdw">false</scalar>
                    <scalar dataType="xsd:double" dictRef="v:zabVdw">-0.84910000</scalar>
                    <scalar dataType="xsd:double" dictRef="v:param1">0.12340000</scalar>
                    <scalar dataType="xsd:double" dictRef="v:param2">1.00000000</scalar>
                </module>                           
            </comment>
        </xsl:if>
        
    </xsl:template>
    
</xsl:stylesheet>
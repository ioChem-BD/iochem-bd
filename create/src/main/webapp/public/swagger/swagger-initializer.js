window.onload = function() {
  const ui = SwaggerUIBundle({
    url: "./swagger.yml", // URL del archivo OpenAPI
    dom_id: '#swagger-ui',
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    layout: "BaseLayout",
    plugins: [
      () => {
        return {
          statePlugins: {
            auth: {
              wrapActions: {
                authorize: (originalAuthAction, system) => async (authConfig) => {
                  if (authConfig.basicAuth) {
                    document.querySelectorAll('.auth-container')[1].hidden = true 

                    const passwordInput = document.querySelector('[aria-label="auth-basic-password"]');
                    const password = passwordInput ? passwordInput.value : ''; // Get the value of the input field
                    const usernameInput = document.querySelector('[aria-label="auth-basic-username"]');
                    const username = usernameInput ? usernameInput.value : ''; // Get the value of the 

                    const basicAuthHeader = "Basic " + btoa(`${username}:${password}`);          
                    // Perform the login to get the JWT
                    const response = await fetch("../../rest/login", {
                      method: "POST",
                      headers: {
                        Authorization: basicAuthHeader,
                        Accept: "application/json",
                      },
                      credentials: 'omit'
                    });
                    
                    if (response.ok) {
                      const jwt = await response.text();
                      //authBtn.addEventListener('click', copiarContenido(jwt))
                      ui.preauthorizeApiKey("bearerAuth", jwt);
                    } else {
                      alert("Login failed: " + response.statusText + "\nPlease check your credentials and, eventually, clear session cookies.");
                    }
                  }
                  return originalAuthAction(authConfig);
                },
                logout: (originalAuthAction, system) => async (authConfig) => {
                  ui.preauthorizeApiKey("bearerAuth", "");
                  return originalAuthAction(authConfig);          
                }
              },
            },
          },
        };
      },
    ],
    
  });
  window.ui = ui;
};

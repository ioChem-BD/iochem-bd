<?xml version='1.0' encoding='UTF-8'?> 
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:xs='http://www.w3.org/2001/XMLSchema' 																  
xmlns:cml='http://www.xml-cml.org/schema' 
xmlns:cmlx='http://www.xml-cml.org/schema/cmlx' 
xpath-default-namespace='http://www.xml-cml.org/schema' exclude-result-prefixes='xs cml' 
version='2.0'> 
<xsl:output method='xml' indent='yes'/> 
<xsl:strip-space elements='*'/> 																

    <xsl:template match='/'>
         <xsl:element name='cml'>
            <xsl:choose>
                <xsl:when test="exists(//molecule)">
                    <xsl:copy-of select="(//molecule)[last()]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message terminate="yes">Could not extract a molecular geometry from the output file, this error is associated to an unsupported format.</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
               </xsl:element>
    </xsl:template>      
</xsl:stylesheet>
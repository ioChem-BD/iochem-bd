<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:molcas="http://www.iochem-bd.org/dictionary/molcas/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>Jul 05, 2015</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/molcas.xsl"/>    
    <xsl:template match="/">     
        <xsl:variable name="environmentModule" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:environment']"/>
        <xsl:variable name="program" select="$environmentModule/cml:parameterList/cml:parameter[@dictRef='cc:program']/cml:scalar"/>        
        <xsl:variable name="programVersion" select="$environmentModule/cml:parameterList/cml:parameter[@dictRef='cc:programVersion']/cml:scalar"/>        
        <xsl:variable name="basis" select="molcas:getBasis(//cml:module[@cmlx:templateRef='basisset']//cml:array[@dictRef='m:basis'])"/>
        <xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>        
        <xsl:variable name="spin">
            <xsl:if test="exists((//cml:module[@cmlx:templateRef='wave.specs']/cml:scalar[@dictRef='m:spinquantumnum'])[last()])">
                <xsl:value-of select="number((//cml:module[@cmlx:templateRef='wave.specs']/cml:scalar[@dictRef='m:spinquantumnum'])[last()]) * 2 + 1 "/>
            </xsl:if>
            <xsl:if test="exists((//cml:module[@cmlx:templateRef='scf-ksdft']/cml:scalar[@dictRef='m:spin'])[last()])">
                <xsl:value-of select="number((//cml:module[@cmlx:templateRef='scf-ksdft']/cml:scalar[@dictRef='m:spin'])[last()]) * 2 + 1 "/>
            </xsl:if>                       
        </xsl:variable>               
        <xsl:variable name="multiplicity" select="substring-before(concat(string($spin),'.'),'.')"/>         
        <xsl:variable name="charge">
            <xsl:choose>
                <xsl:when test="exists((//cml:module[@id='job']/cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='m:charge'])[last()])">
                    <xsl:value-of select="(//cml:module[@id='job']/cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='m:charge'])[last()]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="(//cml:module[@cmlx:templateRef='mulliken']//cml:scalar[@dictRef='cc:charge'])[last()]"/>                    
                </xsl:otherwise>
            </xsl:choose>            
        </xsl:variable>
        
        <xsl:variable name="formula" select="(//cml:formula/@concise)[1]"/>
        
        <!-- Calculation type capture -->
        <xsl:variable name="inputLines" select="(//cml:module[@id='m:inputlines'])[1]/cml:scalar/text()"/>
        <xsl:variable name="isRestrictedOpt" select="exists(//cml:module[@cmlx:templateRef='constraint'])" />
        <xsl:variable name="isOptimization" select="molcas:isOptimization($inputLines)" />    
        <xsl:variable name="isTS" select="molcas:isTS($inputLines)"/>
        <xsl:variable name="hasLastEnergySection" select="exists(//cml:module[@id='calculation']//cml:module[@cmlx:templateRef='module' and @role='last_energy'])"/>            
        <xsl:variable name="isIncomplete" select="molcas:isIncomplete(//cml:module[@cmlx:templateRef='energy.statistics'], $isOptimization, $hasLastEnergySection )" />
        <xsl:variable name="calcType" select="molcas:getCalcType($isRestrictedOpt, $isOptimization, $isTS, $isIncomplete) "/>
        
        <!-- Method capture -->        
        <xsl:variable name="modules" select="//cml:module[@id='finalization']/cml:propertyList/cml:property[@dictRef='m:module']/cml:array/text()"/>
        <xsl:variable name="ksdft" select="(//cml:module[@cmlx:templateRef='scf-ksdft'])[last()]/cml:scalar[@dictRef='m:program']"/>
        <xsl:variable name="wavespecs" select="(//cml:module[@cmlx:templateRef='wave.specs'])[last()]"/>        
        <xsl:variable name="methods" select="molcas:getMethods($modules, $ksdft,$wavespecs)"/>
        
        <xsl:variable name="numberOfJobs" select="count(//cml:module[@dictRef='cc:jobList']/cml:module[@dictRef='cc:job'])"/>               
        <xsl:variable name="hasVibrations" select="exists(//cml:property[@dictRef='cc:frequencies'])"/>        
        <xsl:variable name="hasSolvent" select="exists((//cml:module[@cmlx:templateRef='pcm'])[last()]) or exists((//cml:module[@cmlx:templateRef='kirkwood'])[last()])"/>
        
        <!-- Print metadata fields -->
        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
        </xsl:if>  
        <xsl:for-each select="$basis">
            <xsl:if test=". != ''">
cml:basisset&#x9;<xsl:value-of select="."/>    
            </xsl:if>                                        
        </xsl:for-each>  
        <xsl:if test="exists($charge) and compare($charge, '') != 0">
cml:charge&#9;<xsl:value-of select="round($charge)"/>
        </xsl:if>
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity"/>
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation($formula)"/>
        <xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>        
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>              
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrations"/>
<xsl:for-each select="tokenize($methods, '\s+')">
cml:method&#9;<xsl:value-of select="."/>        
</xsl:for-each>                                
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>       
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
        <!-- Create provided metadata -->      
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>        
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>

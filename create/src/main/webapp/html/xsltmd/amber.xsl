<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"     
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:am="http://www.iochem-bd.org/dictionary/amber/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Nov 16, 2021</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/amber.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>    
    <xsl:template match="/">
        
        <xsl:variable name="program" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:program']/scalar)[last()]"/>
        <xsl:variable name="programVersion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programVersion']/scalar)[last()]"/>
        <xsl:variable name="initialization" select="//module[@dictRef='cc:initialization']"/>
        <xsl:variable name="formula" select="(//formula/@concise)[1]" />       
        <xsl:variable name="hasSolvent" select="helper:hasSolvent($initialization)"/>
        <xsl:variable name="hasOrbitals" select="'false'"/>
        <xsl:variable name="hasVibrations" select="'false'"/>
        <xsl:variable name="numberOfJobs" select="count(//module[@dictRef='cc:jobList']/module[@dictRef='cc:job'])"/>
        
        <xsl:variable name="properties" select="//module[@dictRef='cc:finalization']//propertyList"/>
        <xsl:variable name="energy" select="$properties/property[@dictRef= 'am:etot']" />
        
        <!-- Initialization -->
        <xsl:variable name="initialization" select="//module[@dictRef='cc:initialization']"/>
        <xsl:variable name="inputLines" select="$initialization//module[@cmlx:templateRef='input.file']/list[@cmlx:templateRef='lines']"/>
        <xsl:variable name="imin" select="$initialization/parameterList/parameter[@dictRef='am:imin']" />    
        <xsl:variable name="ntt" select="helper:readInputFileParameter($initialization, $inputLines,'ntt')" />
        <xsl:variable name="ntp" select="helper:readInputFileParameter($initialization, $inputLines,'ntp')" />    
        <xsl:variable name="barostatParam" select="helper:readInputFileParameter($initialization, $inputLines, 'barostat')" />    
        <xsl:variable name="method" select="helper:getMethod($imin, $ntp, $ntt)" />                
        <xsl:variable name="calcType" select="if($method = $am:GeometryOptimization) then
                                                    $am:GeometryOptimization
                                              else
                                                    'Molecular Dynamics'" />
        
        <!-- Print metadata fields -->
<xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
</xsl:if>     
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation($formula)"/>   
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>                                           
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:hasmolecularorbitals&#9;<xsl:value-of select="$hasOrbitals"/>
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrations"/>      
<xsl:if test="exists($energy)" >
cml:energy.value&#9;<xsl:value-of select="helper:convertEnergyUnits($energy/scalar, $unitsHartree)"/>
cml:energy.units&#9;Eh
</xsl:if>
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
<!-- Create provided metadata -->      
<xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
</xsl:if>
<xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
</xsl:if>
    </xsl:template>
    
    
    <xsl:function name="helper:readInputFileParameter">
        <xsl:param name="initialization" />
        <xsl:param name="inputLines"/>
        <xsl:param name="parameterName" />
        
        <!-- Will try reading from CONTROL section, otherwise from input lines section -->
        <xsl:choose>
            <xsl:when test="exists($initialization/parameterList/parameter[@dictRef= concat('am:', $parameterName)])">
                <xsl:value-of select="$initialization/parameterList/parameter[@dictRef= concat('am:', $parameterName)]"/>
            </xsl:when>
            <xsl:when test="exists($inputLines/scalar[@dictRef='cc:inputLine'][matches(lower-case(text()),concat('^\s*', $parameterName, '\s*[=:].*'))])">
                <xsl:variable name="parameter" select="$inputLines/scalar[@dictRef='cc:inputLine'][matches(lower-case(text()),concat('^\s*', $parameterName, '\s*[=:].*'))]"/>                                
                <xsl:value-of select="helper:trim(tokenize($parameter, '[=:,]')[2])"/>
            </xsl:when>                       
        </xsl:choose>
    </xsl:function>   
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:turbo="http://www.turbomole.com"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>Jan 26, 2015</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>

    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/turbomole.xsl"/>    
    <xsl:template match="/">     
        <xsl:variable name="environmentModule" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:environment']"/>
        <xsl:variable name="program" select="$environmentModule/cml:parameterList/cml:parameter[@dictRef='cc:program']/cml:scalar"/>        
        <xsl:variable name="programVersion" select="$environmentModule/cml:parameterList/cml:parameter[@dictRef='cc:programVersion']/cml:scalar"/>        
        <xsl:variable name="basis" select="distinct-values(tokenize((//cml:module[@cmlx:templateRef='basisset']//cml:array[@dictRef='t:basis'])[1],'\s+'))"/>

        <xsl:variable name="electrostaticmoments" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='electrostatic.moments'])[last()]"/>
        <xsl:variable name="charge" select="$electrostaticmoments/cml:scalar[@dictRef='t:charge']"/>
        <xsl:variable name="multiplicity" select="number($charge) + 1"/>
        
        <xsl:variable name="potentialEnergy" select="//cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='t:energy']/cml:scalar"/> 
        <xsl:variable name="formula" select="(//cml:formula/@concise)[1]"/>
        <xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>
        
        <!-- Method capture -->
        <xsl:variable name="parameters" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList"/>
        <xsl:variable name="soes" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:soes']/cml:scalar"/>    
        <xsl:variable name="methods" select="$parameters/cml:parameter[@dictRef='cc:method']/child::*"/>
        <xsl:variable name="calcMethod" select="turbo:getMethod($soes,$methods)"/>

        <xsl:variable name="isRestrictedOptimization" select="exists(//cml:module[@cmlx:templateRef='restrictions'])"/>
        <xsl:variable name="isOptimization" select="exists(//cml:module[@cmlx:templateRef='convergence.info'])"/>
        <xsl:variable name="isIncomplete" select="count(//cml:module[@cmlx:templateRef='convergence.info']//cml:scalar[@dictRef='cc:converged' and normalize-space(text())='no']) > 0"/>
        <xsl:variable name="vibrations" select="//cml:module[@cmlx:templateRef='vibrations']"/>
        <xsl:variable name="statpt" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:statpt']"/>
        <xsl:variable name="soes" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:soes']/cml:list"/>
        <xsl:variable name="calcType" select="helper:trim(turbo:getCalcType($isRestrictedOptimization, $isOptimization, $isIncomplete, $vibrations, $statpt, $soes))"/>     

        <xsl:variable name="hasSolvent" select="exists((//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='cosmo'])[last()])"/>
        <xsl:variable name="numberOfJobs" select="count(//module[@dictRef='cc:jobList']/module[@dictRef='cc:job'])"/>       
        <xsl:variable name="hasVibrations" select="exists(//cml:module[@dictRef='cc:finalization']//cml:module[cmlx:templateRef='vibrations'])"/>
        
        <!-- Print metadata fields -->
        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
        </xsl:if>  
        <xsl:for-each select="$basis">            
cml:basisset&#x9;<xsl:value-of select="."/>                
        </xsl:for-each>                               
        <xsl:if test="exists($electrostaticmoments)">
cml:charge&#9;<xsl:value-of select="$charge"/>
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity"/>    
        </xsl:if>                          
        <xsl:if test="exists($potentialEnergy)">
cml:energy.value&#9;<xsl:value-of select="$potentialEnergy/text()"/>
cml:energy.units&#9;<xsl:value-of select="helper:printUnitSymbol($potentialEnergy/@units)"/>
        </xsl:if>
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation($formula)"/>
        <xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>        
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>              
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrations"/>
        <xsl:for-each select="$calcMethod">
            <xsl:if test="compare(helper:trim(.),'') != 0">
cml:method&#9;<xsl:value-of select="."/>        
            </xsl:if>            
        </xsl:for-each>                                          
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>       
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
        <!-- Create provided metadata -->      
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>        
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>
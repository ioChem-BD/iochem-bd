<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:mp="http://www.iochem-bd.org/dictionary/mopac/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    exclude-result-prefixes="xs"    
    version="2.0">
    
    <!-- Calculation type related variables -->
    <xsl:variable name="mp:GeometryOptimization" select="'Geometry optimization'" />
    <xsl:variable name="mp:SinglePoint" select="'Single point'" />    
    <xsl:variable name="mp:TransitionState" select="'TS'" />
    <xsl:variable name="mp:Minimum" select="'Minimum'"/>
    
    
    <xsl:variable name="mp:methodsRegex" select="'^(HF|UHF|RHF|PM3|PM6|PM6-D3|PM6-DH\+|PM6-DH2|PM6-DH2X|PM6-D3H4|PM6-D3H4X|PMEP|PM7|PM7-TS|AM1|RM1|MNDO|MNDOD).*'"/>
    <xsl:variable name="mp:tsRegex" select="'^(TRANS|TRANS\s*=).*'" />
    <xsl:variable name="mp:chargeRegex" select="'^CHARGE\s*=\s*'" />
    <xsl:variable name="mp:spinRegex" select="'^(MS\s*=|SINGLET|DOUBLET|TRIPLET|QUARTET|QUINTET|SEXTET|SEPTET|OCTET|NONET).*'" />
    <xsl:variable name="mp:spinMsRegex" select="'MS\s*=\s*'" />
    
    <xsl:function name="mp:getMethods">
        <xsl:param name="section"/>        
        <xsl:for-each select="$section//cml:scalar[@dictRef='mp:inputline']">
            <xsl:variable name="line" select="./text()"/>        
            <xsl:for-each select="tokenize($line,'\s+')">
                <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $mp:methodsRegex)">
                        <xsl:value-of select="$command"/><xsl:text> </xsl:text>
                    </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:function>    

    <xsl:function name="mp:isTS">
        <xsl:param name="section"/>
        <xsl:variable name="return">
            <xsl:for-each select="$section//cml:scalar[@dictRef='mp:inputline']">
                <xsl:variable name="line" select="./text()"/>        
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $mp:tsRegex)">
                        <xsl:value-of select="$command"/><xsl:text> </xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="contains($return,'true')"/>
    </xsl:function>
    
    
    <xsl:function name="mp:getCalcType">
        <xsl:param name="isOptimization" as="xs:boolean"/>
        <xsl:param name="hasVibrations" as="xs:boolean"/>
        <xsl:param name="negativeFrequenciesCount" as="xs:integer"/>
        <xsl:param name="isTS" as="xs:boolean"/>
        
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="$isOptimization">
                    <xsl:value-of select="$mp:GeometryOptimization"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$mp:SinglePoint"/>
                </xsl:otherwise>
            </xsl:choose>     
        </xsl:variable>
        
        <xsl:variable name="type2">
            <xsl:if test="$hasVibrations">
                <xsl:choose>
                    <xsl:when test="$negativeFrequenciesCount = 1 and $isTS" >
                        <xsl:value-of select="$mp:TransitionState"/>
                    </xsl:when>
                    <xsl:when test="$negativeFrequenciesCount = 0">
                        <xsl:value-of select="$mp:Minimum"/>
                    </xsl:when >
                </xsl:choose>
            </xsl:if>	       
        </xsl:variable>        
        <xsl:value-of select="concat($type, ' ', $type2)"/>        
    </xsl:function>
    
    <xsl:function name="mp:getCharge">
        <xsl:param name="sections"/>


        <xsl:variable name="charge">
            <xsl:for-each select="$sections">
                <xsl:variable name="section" select="."/>
                <xsl:for-each select="$section//cml:scalar[@dictRef='mp:inputline']">
                    <xsl:variable name="line" select="replace(./text(),'\s*=\s*', '=')"/> <!-- Remove blank spaces inside the assignations, otherwise assignations will fail to be properly parsed -->        
                    <xsl:for-each select="tokenize($line,'\s+')">
                        <xsl:variable name="command" select="."/>
                        <xsl:if test="matches(upper-case($command), $mp:chargeRegex)">
                            <xsl:value-of select="replace(upper-case($command), $mp:chargeRegex, '')"/><xsl:text> </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>            
            </xsl:for-each>                
        </xsl:variable>
        <xsl:value-of select="replace($charge, '\s+$', '')" />               
    </xsl:function>
    
    
    <xsl:function name="mp:getMultiplicity">
        <xsl:param name="sections"/>
        <xsl:variable name="multiplicity">        
            <xsl:for-each select="$sections">
                <xsl:variable name="section" select="."/>
                <xsl:for-each select="$section//cml:scalar[@dictRef='mp:inputline']">
                    <xsl:variable name="line" select="replace(./text(),'\s*=\s*', '=')"/> <!-- Remove blank spaces inside the assignations, otherwise assignations will fail to be properly parsed -->        
                    <xsl:for-each select="tokenize($line,'\s+')">
                        <xsl:variable name="command" select="upper-case(.)"/>                    
                        <xsl:if test=" matches($command, $mp:spinRegex)">                       
                            <xsl:choose>
                                <xsl:when test="matches($command, $mp:spinMsRegex)">
                                    <xsl:value-of select="(number(replace($command, $mp:spinMsRegex, '')) * 2) + 1"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="$command = 'SINGLET'">1</xsl:when>                                    
                                        <xsl:when test="$command = 'DOUBLET'">2</xsl:when>
                                        <xsl:when test="$command = 'TRIPLET'">3</xsl:when>
                                        <xsl:when test="$command = 'QUARTET'">4</xsl:when>
                                        <xsl:when test="$command = 'QUINTET'">5</xsl:when>
                                        <xsl:when test="$command = 'SEXTET'">6</xsl:when>
                                        <xsl:when test="$command = 'SEPTET'">7</xsl:when>
                                        <xsl:when test="$command = 'OCTET'">8</xsl:when>
                                        <xsl:when test="$command = 'NONET'">9</xsl:when>                                                                        
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>                        
                            <xsl:text> </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>            
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="if($multiplicity = '') then '1' else $multiplicity"/>
    </xsl:function>    
</xsl:stylesheet>
    

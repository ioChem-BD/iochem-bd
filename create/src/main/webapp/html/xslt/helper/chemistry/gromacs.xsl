<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:gm="http://www.iochem-bd.org/dictionary/gromacs/"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:variable name="gm:npt">NPT</xsl:variable>
    <xsl:variable name="gm:nvt">NVT</xsl:variable>
    <xsl:variable name="gm:nve">NVE</xsl:variable>
    
   
    <xsl:function name="helper:getMethod">
        <xsl:param name="tcoupl"  />
        <xsl:param name="pcoupl" />
        
        <xsl:choose>
            <xsl:when test="not(exists($pcoupl)) or lower-case(gm:trim($pcoupl/cml:scalar/text())) = 'no'">
                <xsl:choose>
                    <xsl:when test="not(exists($tcoupl)) or lower-case(gm:trim($tcoupl/cml:scalar/text())) = 'no'">
                        <xsl:value-of select="$gm:nve"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$gm:nvt"/>                        
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$gm:npt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="helper:getCouplingMethods">
        <xsl:param name="tcoupl"  />
        <xsl:param name="pcoupl" />
        <xsl:choose>
            <xsl:when test="not(exists($pcoupl)) or lower-case(gm:trim($pcoupl/cml:scalar/text())) = 'no'">
                <xsl:choose>
                    <xsl:when test="not(exists($tcoupl)) or lower-case(gm:trim($tcoupl/cml:scalar/text())) = 'no'">
                        <xsl:value-of select="''"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('t=', $tcoupl)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>              
                <xsl:value-of select="concat('T=', $tcoupl, '  P=', $pcoupl)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="helper:hasSolvent">
        <xsl:param name="initialization" />        
        <xsl:variable name="solventParametersRegex" select="'(implicit.solvent|gb.algorithm|nstgbradii|rgbradii|gb.epsilon.solvent|gb.saltconc|gb.obc.alpha|gb.obc.beta|gb.obc.gamma|gb.dielectric.offset|sa.algorithm|sa.surface.tension)'"/>        
        <xsl:value-of select="count($initialization/cml:parameterList/cml:parameter[matches(@dictRef, $solventParametersRegex)]) &gt; 0"/>                
    </xsl:function>
    
    <!-- Trim whitespaces from string -->
    <xsl:function name="gm:trim" as="xs:string">
        <xsl:param name="arg" as="xs:string?"/>        
        <xsl:sequence select="replace(replace($arg,'\s+$',''),'^\s+','')"/>        
    </xsl:function>
    
</xsl:stylesheet>
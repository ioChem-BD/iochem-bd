// FUNCTIONS OF PANEL DRAW
let listCalcIdReport = [];
var arrayCalculationsReportAvailable = [];

/**
 * Get the json graph and print it in cytoscape
 * @param {String} calculs
 */
function addCalculsFromJava(calculs) {
  var jsonDataCalculs = JSON.parse(calculs);
  var jsonBase = {
    elements: {
      nodes: [],
      edges: [],
    },
  };
  jsonDataCalculs.calculations.forEach((dataCalcul) => {
    jsonBase.elements.nodes.push({ data: dataCalcul });
  });

  cy.json(sanitizeJsonData(jsonBase));
  setTimeout(() => {
    applyStylesJson();
  }, 500);
}

/**
 * Repair the json to avoid possible errors by applying the cytoscape framework
 * @param {Object} jsonObject - The JSON object to be sanitized.
 * @returns {Object} - The sanitized JSON object.
 */
function sanitizeJsonData(jsonObject) {
  if(jsonObject.elements.edges){
    jsonObject.elements.edges.forEach((edge) => {

      if (!edge.data.cid_formula) {
        edge.data.cid_formula = "";
      }
      
      if (!edge.data.id) {
        edge.data.id =
          edge.data.name || edge.data.source + "-" + edge.data.target;
      }
    });
  }
  return jsonObject;

}

/**
 * Delete a node of the calculs list
 * @param {String} nodeId - Id of node
 */
function deleteNodeOfGraph(nodeId) {
  const response = confirm("Are you sure you want to delete selected Node?");
  if (response) {
    cy.remove("#" + nodeId);
    notifyGraphChanged();
  }
}

/**
 * Remove the link and notify java that the graph has changed
 * @param {Object} edge - Object edge
 */
function deleteEdge(edge) {
  const response = confirm("Are you sure you want to delete selected Edge?");
  if (response) {
    cy.remove("edge[source='" + edge.source() + "']");
    cy.remove("edge[target='" + edge.target() + "']");
    cy.remove("#" + edge.id());
    notifyGraphChanged();
  }
}

/**
 * Change color of the node selected
 * @param {String} nodeID - Id of the node selected
 */
function checkNodeGraphSelected(nodeID) {
  cy.$('#'+nodeID).removeClass("nodes2");
  cy.$('#'+nodeID).addClass("elementSelected");
}

/**
 * Change the color of the selected Edge
 * @param {String} edgeID - the id of the edge
 */
function checkEdgeGraphSelected(edgeID) {
  if (selectedEdgeColor !== null) {
    cy.$("edges").removeClass("elementSelected");
    cy.$("nodes").removeClass("elementSelected");
    selectedEdgeColor = null;
  }
  cy.$(`#${edgeID}`).addClass("elementSelected");
  selectedEdgeColor = edgeID;
}


/**
 * Function to check how many nodes are in the graph with the id and change theri color
 * @param {String id}
 * @return
 */
function selectAllNodesWithThisCalculationId(id) {

  let matchingNodes = cy.elements().filter((e) => {
    let components = e._private.data.cid_formula.split(/[+-]/);
    let returValue = components.some((component) => component == id);
    return returValue;
  });

  cy.$("nodes").removeClass("elementSelected");
  cy.$("nodes").addClass("nodes2");

  cy.$("edges").removeClass("elementSelected");

  matchingNodes.forEach((e) => {
    selectedEdgeColor = e;
    e.removeClass("nodes2");
    e.addClass("elementSelected");
  });
}


/**
 * Check if a node is possibly to be deleted, the conditions are if the id node appears in more than one cid_formula cannot be deleted.
 * @param {Object} arr - array of strings
 */
function isCalculationRemovable(calculationId) {
  let matchingNodes = cy.elements().filter((e) => {
    let components = e._private.data.cid_formula.toString().split(/[+-]/);
    let returnValue = components.some((component) => component == calculationId);
    return returnValue;
  });
  if (matchingNodes.length == 1 && 
      !(
        matchingNodes[0]._private.data.cid_formula.toString().includes("+") ||
        matchingNodes[0]._private.data.cid_formula.toString().includes("-")
      )
  ) {
    zAu.send(
      new zk.Event(
        zk.Widget.$("$customReportWindowIframe"),
        "onDeleteReportCalculation",
        "" + calculationId + "",
        { toServer: true }
      )
    );
    cy.remove(matchingNodes[0]);
  } else if (matchingNodes.length == 0) {
    zAu.send(
      new zk.Event(
        zk.Widget.$("$customReportWindowIframe"),
        "onDeleteReportCalculation",
        "" + calculationId + "",
        { toServer: true }
      )
    );
  } else {
    printToast("To delete the calculation there can be no more than one node referring to it", 5000);
  }
  notifyGraphChanged();
}

/**
 * Get the value of the input, trim the value, insert the pill and clean the input field.
 * @param {Event} event
 */
function addPillCalculation(event) {
  event.preventDefault();
  const pillTextInput = document.getElementById("pillText");
  const text = pillTextInput.value.trim();
  createPill(text);
  pillTextInput.value = "";
}

/**
 * create the list for the autocomplete of the create formula input.
 * @param {String} listString - Estructure of the string is "CalculationID >>> CalculationPath"
 */
function setCalculationsList(listString) {
  listCalcIdReport = [];

  let listArrayStrings = listString.split(",");
  arrayPathCalculations.push(...listArrayStrings)
  listArrayStrings.forEach((e) => {
    let [calculationID, calculationPath] = e.split(">>>");
    listCalcIdReport.push(calculationID.trim());
  });

  if (listString.length > 0) {
    arrayCalculationsReportAvailable = []
    arrayCalculationsReportAvailable.push(...listString.split(","));
  }
}

/**
 * Separate the formula for get only the CalculationID
 * @param {String} cid_formula
 * @returns  {Object} cid_formula - array of CalculationID
 */
function splitCidFormula(cid_formula) {
  return cid_formula.split(/[\+\-]/);
}

/**
 * Check node is on report, if isn't in report return false and you cannot create the node and if it is true you can create the node.
 * @param {*} cid_formula
 * @returns {Boolean}
 */
function checkNodeIsOnReport(cid_formula) {
  if(showEdges && cid_formula.length < 1){
    return true
  }
  let ids = splitCidFormula(cid_formula);
  return ids.some((id) => listCalcIdReport.includes(id));
}

/**
 * Count all the cycles of the graph
 * @returns {number} cycles - number of cycles
 */
function countRings() {
  let ringCount = 0;
  let visitedNodes = new Set();

  function dfs(currentNode, parentNode) {
    let currentNodeId = currentNode.id();
    visitedNodes.add(currentNodeId);

    let neighbors = currentNode.neighborhood().nodes();
    for (const element of neighbors) {
      let neighborId = element.id();

      if (!visitedNodes.has(neighborId)) {
        dfs(element, currentNode);
      } else if (neighborId !== (parentNode ? parentNode.id() : undefined)) {
        ringCount++;
      }
    }
  }

  cy.nodes().forEach(function (node) {
    let nodeId = node.id();

    if (!visitedNodes.has(nodeId)) {
      dfs(node, null);
    }
  });

  let rings = ringCount / 2; 
  return rings;
}

function fillArrayFormulasOrdered() {
  let nodes = cy.elements();
  nodes.forEach((node) => {
    let formula = orderFormula(node._private.data.cid_formula);
    orderedFormulas.push(formula);
  });
}

/**
 * Order the formula with this formula +1-2+4-4 -> +1+4-2-4
 * @param {*} formula 
 * @returns 
 */
function orderFormula(formula) {
  let positive = [];
  let negative = [];
  let calculNumber = "";
  let operationType = true;

  if(formula != undefined || formula != null){

    if (formula[0] == "+" && formula[0] == "-") {
      formula = formula.slice(1);
      formula = formula.toString().trim();
    }
  
    for (let i = 0; i < formula.length; i++) {
      const char = formula[i];
  
      if (char !== "-" && char !== "+") {
        calculNumber += char;
        // Verificar si es el último número
        if (i === formula.length - 1) {
          if (operationType) positive.push(calculNumber);
          if (!operationType) negative.push(calculNumber);
        }
      } else {
        if (operationType) positive.push(calculNumber);
        if (!operationType) negative.push(calculNumber);
        calculNumber = "";
      }
  
      if (char === "+") {
        operationType = true;
      }
      if (char === "-") {
        operationType = false;
      }
    }
  
    let finalFormula = [...positive.sort(), "-", ...negative.sort()];
    return finalFormula.toString();
  }


}

/**
 * Check if the formula has been already been defined or not
 * @param {*} formula 
 * @returns 
 */
function formulaIsUnique(formula) {
  let formulaIsCorrect = false;



  if(showEdges && formula.length < 1){
    return true
  }
  if(formula.length == 0){
    return formulaIsCorrect
  }

  let formulaSliced = formula.slice(1);
  let formulaSorted = orderFormula(formulaSliced);

  if(orderedFormulas.indexOf(formulaSorted) < 0){ 
    formulaIsCorrect = true;
  }
  return formulaIsCorrect
}

function checkFormFormulaIsValid(){
  let saveBtn = document.getElementById("saveNodeFormBtn");
  let alertMsg = document.getElementById("alertNodeFormMsg");
  const formulaFromPills = formulaOfPills.toString().replace(/,/g, "");



  alertMsg.style.display = "none";
  if (currentEditedFormula === formulaFromPills)    // We are editing the same formula
    return;

  if (formulaIsUnique(formulaFromPills)) {          // Else check repeated
    saveBtn.disabled = false;
  } else {
    saveBtn.disabled = true;
    alertMsg.style.display = "block";
  }
}

function setTSCalculations(listTs){
  listCalculationsTs = listTs.split(" ")
}

/**
 * Show formulas of the elements.
 * @param {*} isChecked 
 */
function showFormulas(isChecked = showFormulasChecked){
  cy.style().resetToDefault();
  showFormulasChecked = isChecked


  if(isChecked == 'true'){
    let showNameFormula = (node) => {
      return `${node.data("name")} \n ${node.data("cid_formula")}`
    }
    labelElementGraph(showNameFormula)
  }

  if(isChecked == 'false') {
    let showName = (node) => {
      return `${node.data("name")}`
    }
    labelElementGraph(showName)
  }

  
  cy.nodes().removeClass("elementSelected");
  cy.nodes().addClass("nodes2");

    cy.style(`
      edge {width: 5%}
      .elementSelected {background-color: greenyellow ; line-color:yellow;}
      .nodes2 {background-color: orange;}
      curvsse-style: bezier;
    `);

}

function labelElementGraph(labelObjectGraph) {
  cy.$("nodes").style({
    'text-wrap': 'wrap',
    label: labelObjectGraph
  })
  if(showEdges){
    cy.$("edges").style({
      'text-wrap': 'wrap',
      'color' : "#0062cc",
      label: labelObjectGraph
    })  
  } else {
    cy.$("edges").style({
      'text-wrap': 'wrap',
      'color' : "#0062cc",
      label: null
    })      
  }

}

function calculateTotalEnergies(calcsTransformed) {
  let [energies, energyType] = calcsTransformed.split("TYPE::")
  cadena = energies.substring(1, energies.length - 1);
  energiesOfCalculs = cadena.split(", ");
  energiesOfCalculs.forEach(e => {
    let [calcId, energy] = e.split("=")
    let elements = cy.elements().filter((node)=>{
      let cid_formulas = node.data("cid_formula");
      return cid_formulas.includes(calcId)
    })

      elements.forEach((e)=>{
        let cid_formula = e.data("cid_formula");
        let numeros = cid_formula.match(/\d+/g); // [id1,id2,id3]
        let operadores = cid_formula.match(/[+-]/g); // ["+", "-", "+"]
        let energy_formula = "";
        let lengthNum = numeros.length - 1
        for (let i = 0; i < numeros.length; i++) {
          const found = energiesOfCalculs.find((element) => element.includes(`${numeros[i]}=`));
          let [calcId, energy] = found.split("=")

          if (lengthNum != i) {
            energy_formula += `${energy} ${operadores[i]} `
          }
          if (lengthNum == i) {
            energy_formula += energy
          }
        }
        e.data("energy_formula", energy_formula)
      })

  })
}

/**
 * Calculate total energy of the element with a other relative energy of element.
 */
function calculateRelativeEnergy(formulastr) {
  let ids = formulastr.split(/[+-]/)
  let energies = ""
  for (let id = 0; id < ids.length; id++) {
    const found = energiesOfCalculs.find((element) => element.includes(`${ids[id]}=`));
    let [calcId, energy] = found.split("=")
      energies += energy
  }
  cy.elements((e) => {
    let energy_formula = e.data("energy_formula");
    let energyTotal;
    if(energy_formula != undefined){
      
      energy_formula += energies
      energyTotal = eval(energy_formula);
      e.data("energy_total", energyTotal)
    }

  })
}
/**
 * Add node and validate if is correct
 */
function addNodeToCanva(name, formulaOfPills, x, y) {
  if(formulaIsUnique(formulaOfPills)){
    let nodeCreated = nodeCY.addNode(name, formulaOfPills, x, y);
    nodeCreated.addClass("nodes2")
    applyStylesJson();
    return nodeCreated;
  } else {
    return null
  }

}

/**
 * Clear the form of the create modal
 */
function clearCreateNodeModal() {
  document.getElementById("nameNodeInputCreate").value = ""
  document.getElementById("pillTextInputCreate").value = ""
  document.getElementById("pillContainerCreate").innerHTML = ""
  document.getElementById("energyNodeDiv").innerHTML = ""
  printSelectedOperation()
}

function insertEditNodeModal(element, typeElement) {
  document.getElementById("nameNodeInputCreate").value = element.name
  document.getElementById("idNodeInput").value = element.id
  let divEnergy = document.getElementById("energyNodeDiv")
  
  if (element.energy_total) {
    divEnergy.innerHTML = `<h5 class="mb-3">Energy</h5><span>${element.energy_total.toFixed(4)}</span>`  
  }
  
  if(typeElement == "edge" && element.cid_formula.trim().length == 0) return
  
  let calculations = ("+"+element.cid_formula).match(/[-+]? *\d+(?:\.\d+)?/g)
  if (calculations) {
    calculations.forEach((calculation) => {
      if(calculation != "") {
      createPill(calculation)
      }
    })
  }

  printSelectedOperation()
  document.getElementById("pillContainerCreate").value = element.id
  document.getElementById("pillTextInputCreate").value = ""
}

function printSelectedOperation() {
  let selectElement;
  if(typeContainerPills == "create"){
    selectElement = document.getElementById("operacionSelectCreate");
  }
  if(typeContainerPills == "correct"){
    selectElement = document.getElementById("operacionSelectCorrect");
  }
  for (const element of selectElement.options) {
    let option = element;
    if (option.value === "+") {
      option.selected = true;
    } else {
      option.selected = false;
    }
  }
}

/**
 * Print info of edge details in modalHtml
 * @param {Object} edge all data of the edge from the graph
 */
function printDetailsEdge(edge) {
  window.crudDialog.showModal();
  crudCalculDialog.innerHTML = "";

  crudCalculDialog.innerHTML = `

  <h3>Details Edge</h3>
  <div class="form-group">
  <h4>${edge.name}</h4>
  <h4><b>Source Node:</b> ${edge.source}</h4>
  <h4><b>Target Node:</b> ${edge.target}</h4>
  <p><b>cid formula</b> ${edge.cid_formula}</p>

  </div>
  <button onclick="window.crudDialog.close();" class="btn btn-danger">Cerrar</button>
  `;

}

/**
 * Clear all the inputs of the form passed.
 * @param {Object} form
 */
function clearForm(form) {
  for (const inputField of form) {
    inputField.value = "";
  }
}


function printToast(text, delay){

  let toast = document.createElement("div");
  toast.classList.add("toast");
  toast.classList.add("fade");
  toast.setAttribute("role", "alert");
  toast.setAttribute("aria-live", "assertive");
  toast.setAttribute("aria-atomic", "true");

  toast.innerHTML = `
    
      <button type="button" class="ml-2 mb-1 close" style="background-color:white" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
  
    <div class="toast-body">${text}</div>
  `;

  document.getElementById("toast-container").appendChild(toast);
  $(toast).toast({ delay: delay || 3000 }).toast('show');

  $(toast).on('hidden.bs.toast', function () {
    $(this).remove();
  });

}

/**
 * Print the info about the graph
 */
function printInfoGraph(){
  window.infoGraphModal.showModal();

  const nodes = cy.nodes().length;
  const edges = cy.edges().length;
  const averageDegree = edges/nodes;
  
  document.getElementById("nodesInfoSpan").innerText = nodes;
  document.getElementById("edgesInfoSpan").innerText = edges;
  
  // Verifica si la media es menor o igual a 3 antes de ejecutar countRings()
  if (averageDegree <= 3) {
    document.getElementById("cyclesInfoSpan").innerText = countRings();
  } else {
    document.getElementById("cyclesInfoSpan").innerText = "Is not available";
    document.getElementById("cyclesInfoSpan").style.color = "red";
  }

}

/**
 * Apply the substring or 2n title to each node
 */
function applyStylesJson(){
 
  cy.$("nodes").removeClass("elementSelected");
  cy.$("nodes").addClass("nodes2");
  cy.$("nodes").style({
    'text-wrap': 'wrap',
    label: function(node) {
      return `${node.data("name")} \n ${node.data("cid_formula")}`
    }
  })
     
}


/**
 * PRINT ONE PILL INTO THE DIV 
 * @param {*} text 
*/
var formulaOfPills = [];
function createPill(text) {
  text.trim();
  text = text.replace(/\s/g, "");

  if (text) {

    let [calculationID, calculationPath] = text.split(">>>")
    let featuresContainerCreate = null;
    let saveBtn = document.getElementById("saveNodeFormBtn");
    let alertMsg = document.getElementById("alertNodeFormMsg");
    formulaOfPills.push(calculationID)

    if(typeContainerPills == "create"){
      featuresContainerCreate = document.getElementById("pillContainerCreate");
    }
    if(typeContainerPills == "correct"){
      featuresContainerCreate = document.getElementById("pillContainerCorrect");
    }

    const pill = document.createElement("div");
    pill.classList.add("pill");

    const content = document.createElement("span");
    content.textContent = calculationID;

    const deleteIcon = document.createElement("span");
    deleteIcon.textContent = "X";
    deleteIcon.classList.add("delete-icon");

    deleteIcon.addEventListener("click", function () {
      let index = formulaOfPills.indexOf(calculationID);
      formulaOfPills.splice(index, 1);
      pill.remove();
      if(formIsForEdge === true && $( ".pill" ).length == 0){
        saveBtn.disabled = false;
        alertMsg.style.display = "none"
      } else if($( ".pill" ).length <1){
        alertMsg.textContent = "At least one calculation is required"
        checkFormFormulaIsValid()
      } else {
        document.getElementById("alertNodeFormMsg").textContent = "The formula already exist"
        checkFormFormulaIsValid()
      }
    });

    pill.appendChild(content);
    pill.appendChild(deleteIcon);

    featuresContainerCreate.appendChild(pill)

  }

}

/**
 * Add the function autocomplete in the input param.
 * @param {String} idInput - The id of the input search field
 */
function addAutocomplete(idInput) {

  $("#" + idInput).autocomplete({
    source: arrayCalculationsReportAvailable,
    select: function (event, ui) {
      let operationSelect;
      if(typeContainerPills == "create"){
        operationSelect = document.getElementById("operacionSelectCreate");
      }
      if(typeContainerPills == "correct"){
        operationSelect = document.getElementById("operacionSelectCorrect");
      }
      
      let selectedValue = ui.item.value;
      createPill(`${operationSelect.value}${selectedValue}`);
      if(typeContainerPills == "create"){
        checkFormFormulaIsValid()
      }
      setTimeout(function () {
        $("#" + idInput).val("");
      }, 0);
    },
  });
  $("#" + idInput).on("keydown", function (event) {
    if (event.keyCode === 13) {
      let autocompleteData =
        $(this).autocomplete("instance").menu.element[0].children;
      if (
        autocompleteData.length === 1 &&
        autocompleteData[0].textContent.length > 0
      ) {
        autocompleteData[0].textContent = "";
        $(this).val("");
      }
    }
  });
}

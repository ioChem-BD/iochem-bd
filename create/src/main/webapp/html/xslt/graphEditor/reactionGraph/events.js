/**
 * This function simply trigger all the functions and it works to order them
 */
function addElementsEvents() {
  onTapNode();
  onDoubleTap();
  onmouseupNode();
  onRightClickNode();
  onTapEdge();
  onRightClickEdge();
  onTapBackground();
  escapeControlGeometryViewer();
}


function escapeControlGeometryViewer() {
  document.addEventListener("keydown", (event)=>{
    if (event.key === "Escape") {
      manageDiv('', 'hide')
  }
  })  
}
/**
 * This function is the same as addElementsEvents()
 */
function addGraphControlEvents() {
  onTapGraph();
}

/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

function onTapEdge() {
  cy.on("tap", "edge", function (evt) {
    let edgeID = evt.target._private.data.id;
    checkEdgeGraphSelected(edgeID);
  });
}

function onTapBackground() {
  cy.on('tap', function(event){
    let evtTarget = event.target;
    cy.elements().removeClass("elementSelected");
    cy.nodes().addClass("nodes2")
    if( evtTarget === cy ){
      manageDiv("", "hide");
    }

  });
}

function onDoubleTap(){
  cy.on("dbltap", "node", (event) => {
    let nodeT = event.target._private.data;
    nodeSelectedRight = true;
    manageDiv(null,'clean')
    manageDiv(nodeT,'show')
  })
  cy.on("dbltap", "edge", (event) => {
    let nodeT = event.target._private.data;
    if(nodeT.cid_formula.length>0){
      manageDiv(null,'clean')
      manageDiv(nodeT,'show')
    } else {
      printToast("the edge has no formula", 3000);
    }

  })
}


/**
 * Change color like is selected and if the user goes to menu can select link node and this function will do 
 */
function onTapNode() {
  cy.on("tap", "node", function (evt) {
    let node = evt.target._private.data;

    if (
      nodeSelectedRight != null &&
      nodeSelectedRight != node.id &&
      !existConnectionBetweenNodes(node.id, nodeSelectedRight)
    ) {
      nodeSelectedRightData = cy.getElementById(nodeSelectedRight)._private.data;
      nodeCY.linkNode(node, nodeSelectedRightData, null);
      notifyGraphChanged();
    }
    nodeSelectedRight = null;
    checkNodeGraphSelected(node.id);
  });
}
/**
 * Check if the exist a connection between the two nodes
 * @param {String} nodoA 
 * @param {String} nodoB 
 * @returns {Boolean}
 */
function existConnectionBetweenNodes(nodoA, nodoB) {
  let edges = cy.edges(
    '[source="' +
      nodoA +
      '"][target="' +
      nodoB +
      '"], [source="' +
      nodoB +
      '"][target="' +
      nodoA +
      '"]'
  );
  return edges.length > 0;
}

/**
 * This function controls when the user moves a node, it sends a notification to the server so that it knows that there have been changes in the graph
 * and forces you to save or discard them.
 */
function onmouseupNode() {
  cy.on("mouseup", "node", (e) => {
    notifyGraphChanged();
  });
}

/**
 * When user do a right click on a node the node change his color like is selected
 */
function onRightClickNode() {

  cy.on("cxttap", "node", function (evt) {
    cy.elements().removeClass("elementSelected");
    cy.nodes().addClass("nodes2")
    let nodeID = evt.target._private.data.id;
    checkNodeGraphSelected(nodeID);
  });
}

/**
 * When user do a right click on a edge the node change his color like is selected
 */
function onRightClickEdge() {
  cy.on("cxttap", "edge", function (evt) {
    let edgeID = evt.target._private.data.id;
    checkEdgeGraphSelected(edgeID);
  });
}

/**
 * When user do a click in the canvas disselected all the things (nodes, edges)
 */
function onTapGraph() {
  cy.on("tap", function (evt) {
    // Verifica si ya hay un nodo seleccionado
    if (selectedNodeColor !== null) {
      // Restaura el color del nodo previamente seleccionado
      cy.$("nodes").removeClass("elementSelected");
      selectedNodeColor = null; // Limpia el nodo seleccionado

      cy.$(`nodes`).addClass("nodes2");
    }
    if (selectedEdgeColor !== null) {
      cy.$("edges").removeClass("elementSelected");
      selectedEdgeColor = null;
    }
  });
}

/**
 * Changes the style of the graph with one of the options specified
 * @param {Object} event 
 */
function changeStyleOfThegraph() {
  // Cambia el estilo del graph mediante el listbox de estilos
  
    let graphstyle = document.getElementById("graphStyle");
    if(graphstyle.value == 'cola') {
      cy.layout({ name: 'cola', animate:true }).run()   
    } else {
    cy.layout({ name: graphstyle.value }).run() || cytoscape.use(cise);
    }
    applyStylesJson();
    notifyGraphChanged();

}


/**
 * The java executes the function and launches an alert for you to name the file with the name you want it to have and then download it with a cytoscape function.
 */
function downloadJSON() {
  try {
    let originalJson = cy.json(false);

    const filteredNodes = originalJson.elements.nodes.map(node => ({
      position: node.position,
      data: node.data
    }));
    
    const filteredEdges = originalJson.elements.edges.map(edge => ({
      data: edge.data
    }));
    
    // Fusionar los arrays de nodos y aristas en un solo objeto JSON
    const jsonFinal = {
      elements: {
        nodes: filteredNodes,
        edges: filteredEdges
      }
    };
    const jsonString = JSON.stringify(jsonFinal, null, 2);

    const blob = new Blob([jsonString], { type: "application/json" });

    const url = window.URL.createObjectURL(blob);

    let name = prompt("Name of the file: ");
    if (name.trim() != "") {
      let a = document.createElement("a");
      a.href = url;
      a.download = name + ".json"; // Nombre del archivo JSON

      // Simular un clic en el enlace para descargar el archivo
      a.click();
    } 
  } catch (error) {
    console.log(error);
  }
}

/**
 * Sets the view of the canvas if you lost the graph
 */
function focusViewCanva() {
  cy.pan({ x: 50, y: 50 });
  cy.zoom(0.8);
}

/**
 * Clean the canvas, remove all the nodes and edges, set parameters and throw a request to the server
 */
function clearGraphCanva() {
  let confirm = window.confirm("Are you sure you want to clear all the graph elements?")
  if (confirm) {
    cy.$("nodes").remove();
    cy.$("edges").remove();
    cy.minZoom(0.35);
    cy.maxZoom(3.7);
    zAu.send(new zk.Event(zk.Widget.$('$customReportWindowIframe'), "onClearGraphJson", 1, {toServer:true}));	
  }
  notifyGraphChanged()
}

/**
 * This function create a node and link with the specified
 * 1 - Recover the data of the form ( input and div of pills)
 * 2 - Search if there are any nodes with the same chemical formula (cid_formula)
 * 3 - Trigger the function for create the node
 * @param {Object} event 
 */
function controlElementsForm(event) {

    event.preventDefault(); 
    let formE = document.getElementById("createNodeForm");
    window.modal.close();

    const formData = new FormData(formE);
		const name = formData.get("nameNodeInput");
    const id = document.getElementById("idNodeInput").value;

    const formulaOfPillsStr = formulaOfPills.toString().replace(/,/g, "");
		let foundNodes = cy.nodes().filter('[cid_formula="' + formulaOfPillsStr + '"]');

    formulaOfPills = [];
    if(typeFormatForm == 'create'){
      handleNodeCreationForm(name, formulaOfPillsStr, foundNodes);
    } else 
    if (formIsForEdge){
      handleEdgeEditForm(name, formulaOfPillsStr, id);
    } else 
    if(typeFormatForm == 'edit'){
      handleNodeEditForm(name, formulaOfPillsStr, id);
    } 

    clearForm(formE);
    notifyGraphChanged();

  };

/**
 * 1 - Check the node (cid_formula/checmical formula) doesn't exist
 * 2 - Check the id insert by User exist in their Report
 * 3 - If the previous steps work makes the node in the graph, if doesn't exist throw a toast with message error
 * @param {String} name
 * @param {String} formulaOfPillsStr -> the chemical formula made by calculations id
 * @param {Array} foundNodes -> the nodes founded in the graph
 */
function handleNodeCreationForm(name, formulaOfPillsStr, foundNodes){
  if(formulaOfPillsStr.length == 0) return
  if (linkStartNode !== null  && foundNodes.length == 0 ) {
    
    if(checkNodeIsOnReport(formulaOfPillsStr) && formulaIsUnique(formulaOfPillsStr)){
      let nodeRecover = cy.$("#" + linkStartNode);
      let positionNodeRecover = nodeRecover._private.eles[0]._private.position;
      let positionXNewNode = positionNodeRecover.x + 10;
      let positionYNewNode = positionNodeRecover.y + 10;
      formulaOfPillsStr = formulaOfPillsStr.slice(1)
      
      let nodeCreated = addNodeToCanva(name, formulaOfPillsStr, positionXNewNode, positionYNewNode);
    
      if(nodeCreated){
        nodeCY.linkNode(nodeCreated[0]._private.data, nodeRecover._private.eles[0]._private.data, null);  
      }
      linkStartNode = null;
      applyStylesJson();
      formulaOfPills = [];
    } else {
      formulaOfPills = [];
    }

  } else {
    formulaOfPills = [];
    printToast("The formula of the node already exists in the graph", 5000);
  }
}



function handleNodeEditForm(name, formulaOfPillsStr, id) {
  if(formulaOfPillsStr.length == 0) return

  let nodeToEdit = cy.$("#" + id);

  if (nodeToEdit.nonempty()) {
      nodeToEdit.data({ name: name });

    if (checkNodeIsOnReport(formulaOfPillsStr) && formulaIsUnique(formulaOfPillsStr)) {
      nodeToEdit.data({cid_formula: formulaOfPillsStr.slice(1) });
      formulaOfPills = [];
      applyStylesJson();
      nodeToEdit.addClass("elementSelected");

    } else {
      formulaOfPills = [];
    }
    formulaOfPills = [];
    applyStylesJson();
    nodeToEdit.addClass("elementSelected");
    cy.fit();
  }
}

function handleEdgeEditForm(name, formulaOfPillsStr, id) {
  let edgeToEdit = cy.$("#" + id);

  if (edgeToEdit.nonempty()) {
    let mvar = "";
    
      if(name != ""){
        edgeToEdit.data( 'name', name);
      }
      
    
    if (checkNodeIsOnReport(formulaOfPillsStr) && formulaIsUnique(formulaOfPillsStr)) {
      $('#pillContainerCreate .pill').each(function() {
        mvar += $(this).text();
      });
      mvar = mvar.slice(1)
      let formula = mvar.replace(/X/g, '');
      edgeToEdit.data({ cid_formula: formula });

      formulaOfPills = [];
      applyStylesJson();
      edgeToEdit.addClass("elementSelected");
    } else {
      formulaOfPills = [];
      applyStylesJson();
      edgeToEdit.addClass("elementSelected");
    }
    formulaOfPills = [];
    applyStylesJson();
    edgeToEdit.addClass("elementSelected");
    cy.resize();
  
    formIsForEdge = false
    
    if(showFormulasChecked == "true") {
      showFormulas("false");
      showFormulas("true")
    } 
    if(showFormulasChecked == "false") {
      showFormulas("true")
      showFormulas("false")
    }        

  }
}

function handleCorrectEnergy() {
  let formulaOfPills2 = [];
  let elementosPill = document.querySelectorAll("#pillContainerCorrect .pill span");
  elementosPill.forEach(function(element) {
      let value = element.textContent;
      if (value.match(/-\d+/)) {
        formulaOfPills2.push(value);
      }
  });

  let formulaOfPillsStr = formulaOfPills2.toString().replace(/,/g, "");
  formulaOfPillsStr = formulaOfPillsStr.substring(1)
  formulaOfPillsStr = formulaOfPillsStr.replace("+", "-")

  calculateRelativeEnergy(formulaOfPillsStr)
  window.correctEnergiesModal.close()
}

window.addEventListener('resize', function(event){
  cy.center();
  cy.fit();
});


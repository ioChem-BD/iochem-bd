let nodeCY = {
  linkNode(node, nodeSelectedRight, data) {
 
      let edge =  cy.add([
        {
          group: "edges",
          data: {
            id: `${nodeSelectedRight.id}_${node.id}`,
            source: nodeSelectedRight.id,
            target: node.id,
            cid_formula: "",
            originalData: data,
            name:`${nodeSelectedRight.name}_${node.name}`,
          },
        },
      ]);


      formIsForEdge = true;
      orderedFormulas = []
      fillArrayFormulasOrdered()
      setTimeout(() => {
        let edgeT = edge[0]._private.data;
        currentEditedFormula = edgeT.cid_formula
        addAutocomplete("pillTextInputCreate")
        typeFormatForm = "edit"
        document.getElementById("titleFormModal").innerHTML = "Edit Edge"
        clearCreateNodeModal()
        insertEditNodeModal(edgeT, "edge")
        window.modal.showModal();
      }, 200)
  },

  addNode(nameNode, formula, x, y){
    let node = cy.add([
      {
        group: "nodes",
        data: { name: nameNode, cid_formula: formula },
        position: { x: x, y: y},
      },
    ]);
    return node
  }
}

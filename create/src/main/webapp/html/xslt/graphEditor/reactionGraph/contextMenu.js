var optionsContextMenu = {
	evtType: "cxttap",
	menuItems: [
		{
			id: "linkAddNodeBtnMenu",
			content: "",
			tooltipText: "Create a node and link with the node selected",
			selector: "node",
			coreAsWell: false,
			onClickFunction: function(event) {
				typeFormatForm = "create"
				orderedFormulas = []
				formulaOfPills = [];
				fillArrayFormulasOrdered()
				let nodeT = event.target._private.data;
				linkStartNode = nodeT.id;
				document.getElementById("titleFormModal").innerHTML = "Create Node"
				clearCreateNodeModal()
				typeContainerPills = "create"
				addAutocomplete("pillTextInputCreate")
				window.modal.showModal();
			},
		},
		{
			id: "linkNodeBtnMenu",
			content: "",
			tooltipText: "link the node with other node",
			selector: "node",
			coreAsWell: false,
			onClickFunction: function(event) {
				let nodeT = event.target._private.data;

				printToast("Click other node for link both", 5000)
				if (nodeT.id) {
					nodeSelectedRight = nodeT.id;
				}
			},
		},
		{
			id: "showGeometryBtnMenu",
			content: "",
			tooltipText: "show the geometry of the node",
			selector: 'node, edge',
			coreAsWell: false,
			onClickFunction: function(event) {
				let nodeT = event.target._private.data;
				manageDiv(null,'clean')
				manageDiv(nodeT,'show')

			},
		},
		{
			id: "editNodeBtnMenu",
			content: "",
			tooltipText: "edit the node",
			selector: "node",
			coreAsWell: false,
			onClickFunction: function(event) {
				typeContainerPills = "create"
				orderedFormulas = []
				formulaOfPills = [];
				fillArrayFormulasOrdered()
				let nodeT = event.target._private.data;
				currentEditedFormula = nodeT.cid_formula
				addAutocomplete("pillTextInputCreate")
				typeFormatForm = "edit"
				document.getElementById("titleFormModal").innerHTML = "Edit Node"
				clearCreateNodeModal()
				insertEditNodeModal(nodeT, "node")
				window.modal.showModal();
			},
		},
		{
			id: "removeNodeBtnMenu",
			content: "",
			tooltipText: "remove the node",
			selector: "node",
			onClickFunction: function(event) {
				let nodeT = event.target._private.data;
				deleteNodeOfGraph(nodeT.id);
			},
			hasTrailingDivider: true,
			coreAsWell: false,
		},
		{
			id: "editEdgeBtnMenu",
			content: "",
			tooltipText: "edit the edge",
			selector: "edge",
			onClickFunction: function(event) {
				formIsForEdge = true;
				orderedFormulas = []
				formulaOfPills = [];
				fillArrayFormulasOrdered()
				let edgeT = event.target._private.data;
				currentEditedFormula = edgeT.cid_formula
				typeContainerPills = "create"
				addAutocomplete("pillTextInputCreate")
				typeFormatForm = "edit"
				document.getElementById("titleFormModal").innerHTML = "Edit Edge"
				clearCreateNodeModal()
				insertEditNodeModal(edgeT, "edge")
				window.modal.showModal();
			},
			coreAsWell: false,
		},
		{
			id: "removeEdgeBtnMenu",
			content: "",
			tooltipText: "remove the edge",
			selector: "edge",
			onClickFunction: function(event) {
				let edge = event.target;
				deleteEdge(edge);
			},
			coreAsWell: false,
		},
	],
	// css classes that menu items will have
	menuItemClasses: [],
	// css classes that context menu will have
	contextMenuClasses: [
		// add class names to this list
	],
	// Indicates that the menu item has a submenu. If not provided default one will be used
	submenuIndicator: {
		src: "assets/submenu-indicator-default.svg",
		width: 12,
		height: 12,
	},
};

// NODE

/**
 * Apply styles to the context menu
 */
function applyStylesContextMenu() {
	let heightIcon = 25;
	let widthIcon = 25;
	let removeNodeBtnMenu = document.getElementById("removeNodeBtnMenu");
	var svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
	svgObject.src = "../../html/xslt/graphEditor/images/recycle-bin-line-icon.svg";
	removeNodeBtnMenu.appendChild(svgObject)
	removeNodeBtnMenu.classList.add("btn");
	removeNodeBtnMenu.classList.add("btn-danger");

	let editNodeBtnMenu = document.getElementById("editNodeBtnMenu");
	svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
		svgObject.style.padding = 0;
	svgObject.src = "../../html/xslt/graphEditor/images/edit-document-icon.svg";
	editNodeBtnMenu.appendChild(svgObject)
	editNodeBtnMenu.classList.add("btn");
	editNodeBtnMenu.classList.add("btn-secondary");

	let showGeometryBtnMenu = document.getElementById("showGeometryBtnMenu");
	svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
		svgObject.style.padding = 0;
	svgObject.src = "../../html/xslt/graphEditor/images/atom.svg";
	showGeometryBtnMenu.appendChild(svgObject)
	showGeometryBtnMenu.classList.add("btn");
	showGeometryBtnMenu.classList.add("btn-secondary");

	let linkNodeBtnMenu = document.getElementById("linkNodeBtnMenu");
	svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
		svgObject.style.padding = "-10px";
	svgObject.src = "../../html/xslt/graphEditor/images/interactivity-icon.svg";
	linkNodeBtnMenu.appendChild(svgObject)
	linkNodeBtnMenu.classList.add("btn");
	linkNodeBtnMenu.classList.add("btn-secondary");

	let linkAddNodeBtnMenu = document.getElementById("linkAddNodeBtnMenu");
	svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
	svgObject.src = "../../html/xslt/graphEditor/images/stories-icon.svg";
	linkAddNodeBtnMenu.appendChild(svgObject)
	linkAddNodeBtnMenu.classList.add("btn");
	linkAddNodeBtnMenu.classList.add("btn-secondary");

	// EDGE

	let editEdgeBtnMenu = document.getElementById("editEdgeBtnMenu");
	svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
		svgObject.style.padding = 0;
	svgObject.src = "../../html/xslt/graphEditor/images/edit-document-icon.svg";
	editEdgeBtnMenu.appendChild(svgObject)
	editEdgeBtnMenu.classList.add("btn");
	editEdgeBtnMenu.classList.add("btn-secondary");

	let removeEdgeBtnMenu = document.getElementById("removeEdgeBtnMenu");
	var svgObject = document.createElement("img");
		svgObject.width = widthIcon
		svgObject.height = heightIcon
	svgObject.src = "../../html/xslt/graphEditor/images/recycle-bin-line-icon.svg";
	removeEdgeBtnMenu.appendChild(svgObject)
	removeEdgeBtnMenu.classList.add("btn");
	removeEdgeBtnMenu.classList.add("btn-danger");
}


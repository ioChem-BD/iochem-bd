var cy;
var createNodeForm;
var crudCalculDialog;
var styleOfGraphBtn;
var infoGraphDiv;
 
var orderedFormulas = [];
var uuid = window.frameElement.getAttribute('data-uuid');
var calculs = window.frameElement.getAttribute('data-calculs');
var jsonGraphXml = window.frameElement.getAttribute('data-jsongraph');
var nodeSelectedRight = null;
var linkStartNode = null;
var selectedNodeColor = null;
var selectedEdgeColor = null;
var typeFormatForm = "create" || "edit";
var formIsForEdge = false;
var currentEditedFormula = "";
var arrayPathCalculations = [];
var listCalculationsTs = [];
var showFormulasChecked = 'true'
var showEdges = true;
var energiesOfCalculs;
var typeContainerPills = "create"

function setupCytoscape(){
 createNodeForm = document.getElementById("createNodeForm");
 crudCalculDialog = document.getElementById("crudDialog");
 styleOfGraphBtn = document.getElementById("styleOfGraph");
 infoGraphDiv = document.getElementById("infoGraphDiv");

    initCytoscape("panelDraw");
    cy.$("node").addClass("nodes2");
    cy.minZoom(0.35);
    cy.maxZoom(3.5);
    
    // Events of panel draw

    addGraphControlEvents();
    addElementsEvents();
    
    cy.ready(()=>{
      if(jsonGraphXml.includes("elements")){
        cy.json(sanitizeJsonData(JSON.parse(jsonGraphXml)))
        applyStylesJson()
      }
      showFormulas('true');
      cy.contextMenus(optionsContextMenu);
      applyStylesContextMenu();
    })
}

function setupReport() { 
  zAu.send(new zk.Event(zk.Widget.$('$customReportWindowIframe'), "onSetupReport", uuid, {toServer:true}));	
}

function notifyGraphChanged(){
  let jsonRecovery = cy.json(false)
  zAu.send(new zk.Event(zk.Widget.$('$customReportWindowIframe'), "onNotifyGraphChanged", JSON.stringify(jsonRecovery), {toServer:true}));	
}	



/**
 * Init the graph of cytoscape and apply the settings
 * @param {String} divPanel - Id of the container (div)
 * @returns {Object} Cytoscape CANVA
 */
function initCytoscape(divPanel) {
  const panelDraw = document.getElementById(divPanel);
   cy = cytoscape({
    container: panelDraw, 
    group: "nodes",
    wheelSensitivity: 0.1,
    pan: { x: 0, y: 0 },
    minDistance: 150,
    style: `
        node {label:data(id)}
        edge {width: 5%}
        .elementSelected {background-color: greenyellow ; line-color:yellow;}
        .nodes2 {background-color: orange;}
        curve-style: bezier;
      `,
    grabDistance: 100,
  });
  
}

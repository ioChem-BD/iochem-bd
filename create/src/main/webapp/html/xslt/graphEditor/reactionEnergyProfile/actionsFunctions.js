// Functions of draw panel
let listCalcIdReport = [];
var arrayCalculationsReportAvailable = [];

/**
 * Repair the json to avoid possible errors by applying the cytoscape framework
 * @param {Object} jsonObject - The JSON object to be sanitized.
 * @returns {Object} - The sanitized JSON object.
 */
function sanitizeJsonData(jsonObject) {

  jsonObject.elements.edges.forEach((edge) => {
    edge.data = edge;
    
    if (!edge.data.id) {
      edge.data.id =
        edge.data.name || edge.data.source + "-" + edge.data.target;
    }
  });
  jsonObject.elements.nodes.forEach((node) => {
    node.data = node;
  });
  return jsonObject;
}

/** 
 * Function to check how many nodes are in the graph with the id and change their color
 * @param {String id}
 * @return
 */
function selectAllNodesWithThisCalculationId(id) {

  let matchingNodes = cy.elements().filter((e) => {
    // Split the node into components separated by "+" or "-".
    let components = e._private.data.cid_formula.split(/[+-]/);
    let returValue = components.some((component) => component == id);
    // Checks if the letter is present in at least one of the components
    return returValue;
  });

  cy.$("nodes").removeClass("elementSelected");
  cy.$("nodes").addClass("nodes2");

  cy.$("edges").removeClass("elementSelected");

  matchingNodes.forEach((e) => {
    selectedEdgeColor = e;
    e.removeClass("nodes2");
    e.addClass("elementSelected");
  });
}



function setTSCalculations(listTs){
listCalculationsTs = listTs.split(" ")
}

function showFormulas(isChecked = showFormulasChecked){
  cy.style().resetToDefault();
  showFormulasChecked = isChecked


  if(isChecked == 'true'){
    let showNameFormula = (node) => {
      let labelsplited = node.data("label").split("\\n")
      let calcSplited = node.data("calculations").split("<->")
      return `${labelsplited[0]} \n ${calcSplited[1]}`
    }
    labelElementGraph(showNameFormula)
  }

  if(isChecked == 'false') {
    let showName = (node) => {
      let labelsplited = node.data("label").split("\\n")
      return `${labelsplited[0]}`
    }
    labelElementGraph(showName)
  }

    cy.nodes().removeClass("elementSelected");
    cy.nodes().addClass("nodes2");

    cy.style(`
    edge {width: 5%}
    .elementSelected {background-color: greenyellow ; line-color:yellow;}
    .nodes2 {background-color: orange;}
    curve-style: bezier;
    `);

}

function labelElementGraph(labelObjectGraph) {
  cy.$("nodes").style({
    'text-wrap': 'wrap',
    label: labelObjectGraph
  })
  if(showEdges){
    cy.$("edges").style({
      'text-wrap': 'wrap',
      'color' : "#0062cc",
      label: labelObjectGraph
    })  
  } else {
    cy.$("edges").style({
      'text-wrap': 'wrap',
      'color' : "#0062cc",
      label: null
    })      
  }

}


function clearMinifedFormula(formula) {
  let finalFormula = formula
  let formulaSplited = formula.split(/[+-]/)
  formulaSplited.forEach(e => {
    e = e.trim()
    let index = (e.slice(1))-1
    finalFormula = finalFormula.replace(e, calculationsIds[index])
  })
  return finalFormula
}
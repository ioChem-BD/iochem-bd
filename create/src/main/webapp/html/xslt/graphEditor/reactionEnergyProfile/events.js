/**
 * This function simply trigger all the functions and it works to order them
 */

function addElementsEvents() {
  onDoubleTap();
  onTapBackground();
  escapeControlGeometryViewer();
}


function escapeControlGeometryViewer() {
  document.addEventListener("keydown", (event)=>{
    if (event.key === "Escape") {
      manageDiv('', 'hide')
  }
  })  
}
/**
 * This function is the same as addElementsEvents()
 */
function addGraphControlEvents() {
  onTapGraph();
  //changeStyleOfThegraph();
}

/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

function onTapEdge() {
  cy.on("tap", "edge", function (evt) {
    let edgeID = evt.target._private.data.id;
    checkEdgeGraphSelected(edgeID);
  });
}

function onTapBackground() {
  cy.on('tap', function(event){
    var evtTarget = event.target;
    cy.elements().removeClass("elementSelected");
    cy.nodes().addClass("nodes2")
    if( evtTarget === cy ){
      manageDiv("", "hide");
    }

  });
}

function onDoubleTap(){
  cy.on("dbltap", "node", (event) => {
    var nodeT = event.target._private.data;
    nodeSelectedRight = true;
    manageDiv(null,'clean')
    manageDiv(nodeT,'show')
  })
  cy.on("dbltap", "edge", (event) => {
    var nodeT = event.target._private.data;
    if(nodeT.calculations.length>0){
      manageDiv(null,'clean')
      manageDiv(nodeT,'show')
    } else {
      printToast("the edge has no formula", 3000);
    }

  })
}





/**
 * When user do a right click on a edge the node change his color like is selected
 */
function onRightClickEdge() {
  cy.on("cxttap", "edge", function (evt) {
    let edgeID = evt.target._private.data.id;
    checkEdgeGraphSelected(edgeID);
  });
}

/**
 * Changes the style of the graph with one of the options specified
 * @param {Object} event 
 */
function changeStyleOfThegraph() {
  // Cambia el estilo del graph mediante el listbox de estilos
  
    let graphstyle = document.getElementById("graphStyle");
    if(graphstyle.value == 'cola') {
      cy.layout({ name: 'cola', animate:true }).run()   
    } else {
    cy.layout({ name: graphstyle.value }).run() || cytoscape.use(cise);
    }
    applyStylesJson();

}


/**
 * TODO
 * Change c1 + c2 to cID1 + cID2
 * The java executes the function and launches an alert for you to name the file with the name you want it to have and then download it with a cytoscape function.
 */
function downloadJSON() {
  try {
    let originalJson = cy.json(false);

    const filteredNodes = originalJson.elements.nodes.map(node => (
      { 
      position: node.position,
      data: {
        "calculations": node.data.calculations,
        "cid_formula" : cleanFormula(node.data.cid_formula),
        "name": (() => {
          let labelsplited = node.data.label.split("\\n");
          return `${labelsplited[0]}`  || "undefined";
      })(),
        "id": node.data.id
      }
    }));
    
    const filteredEdges = originalJson.elements.edges.map(edge => (
      {
      data: {
        "calculations": edge.data.calculations,
        "cid_formula" : cleanFormula(edge.data.cid_formula),
        "name": (() => {
          let labelsplited = edge.data.label.split("\\n");
          return `${labelsplited[0]}` || "undefined";
      })(),
        "id": edge.data.id,
        "source": edge.data.source,
        "target": edge.data.target
      }
    }));
    
    // Merge the node and edge arrays into a single JSON object
    const jsonFinal = {
      reportID: reportID,
      reportTitle: reportTitle,
      elements: {
        nodes: filteredNodes,
        edges: filteredEdges
      }
    };
    

    const jsonString = JSON.stringify(jsonFinal, null, 2);

    const blob = new Blob([jsonString], { type: "application/json" });

    const url = window.URL.createObjectURL(blob);

    let name = prompt("Filename: ");
    if (name.trim() != "") {
      var a = document.createElement("a");
      a.href = url;
      a.download = name + ".json"; // JSON filename

      // Simulate a click on the link to download file
      a.click();
    } else {
    }
  } catch (error) {
    console.log(error);
  }
}


function cleanFormula(cid_formula) {
  try {
    let finalFormula = clearMinifedFormula(cid_formula)
    if(finalFormula == "undefined"){
      finalFormula = ""
    }
    return finalFormula    
  } 
  catch (error) {
    console.log(error)
  }
  }


/**
 * Sets the view of the canvas if you lost the graph
 */
function focusViewCanva() {
  cy.pan({ x: 50, y: 50 });
  cy.zoom(0.8);
}

window.addEventListener('resize', function(event){
  cy.center();
  cy.fit();
});


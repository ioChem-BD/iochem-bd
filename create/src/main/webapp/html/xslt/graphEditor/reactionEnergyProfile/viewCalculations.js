function manageDiv(node, operation) {
  let panelDraw = document.getElementById("panelDraw");
  let containerMolecular = document.getElementById("containerMolecular");
  switch (operation) {
    case "show":
      panelDraw.classList.remove("col-md-12");
      panelDraw.classList.add("col-md-8");
      panelDraw.classList.remove("col-sm-12");
      panelDraw.classList.add("col-sm-6");

      containerMolecular.classList.remove("d-none"); // Show container
      containerMolecular.classList.add("col-md-4");
      containerMolecular.classList.add("col-sm-6");

      let title = document.createElement("h1");
      let subtitle = document.createElement("h4");

      title.textContent = node.name;
      subtitle.textContent = node.cid_formula;

      createIframeGeometry(node);
      break;

    case "hide":
      containerMolecular.classList.add("d-none"); // Hide container
      panelDraw.classList.remove("col-md-8");
      panelDraw.classList.add("col-md-12");
      panelDraw.classList.remove("col-sm-6");
      panelDraw.classList.add("col-sm-12");

      break;

    case "clean":
      containerMolecular.innerHTML = ``;
      let closeBtn = document.createElement("button");
      closeBtn.style =
        "position:relative; margin-right: 2%; margin-top: 2%; color: black;";
      closeBtn.textContent = "X";

      closeBtn.addEventListener("click", function () {
        manageDiv("", "hide");
      });
      closeBtn.classList.add("btn");
      closeBtn.classList.add("btn-secondary");

      containerMolecular.appendChild(closeBtn);

      break;

    default:
      break;
  }
}

function createIframeGeometry(node) {
  let containerMolecular = document.getElementById("containerMolecular");
  containerMolecular.classList.add("nopadding");
  containerMolecular.classList.add("d-flex");
  containerMolecular.classList.add("flex-column");
  containerMolecular.classList.add("overflow-auto");
  containerMolecular.classList.add("container");

  let pathCalculation = "";
  let formulas = node.calculations.split("<->")[1].trim().split(/[+\-]/);


  containerMolecular.style.overflowY = "scroll";
  formulas = [... new Set(formulas)]
  formulas.forEach((formula) => {
    formula = formula.trim()
    let index = formula.slice(1)
    formula = calculationsIds[index]
 
    let rowDiv = document.createElement("div");
    rowDiv.classList.add("d-flex");
    rowDiv.classList.add("justify-content-center");
    rowDiv.style.borderLeft = "2px solid grey";
    let geometryIframe = document.createElement("iframe");
    geometryIframe.style.border = "0px solid black"
    geometryIframe.style.width = "300px"
    geometryIframe.style.minHeight = "400px";
    geometryIframe.classList.add("mb-2");

    printVisualizer(rowDiv, geometryIframe, formula, pathCalculation, transitionStates[index], originalTransitionStates[index], "GRRM");
    
  });
  nodeSelectedRight = null;
}


function printVisualizer(rowDiv, geometryIframe, formula, path = "", ts, tsOriginally, type) {
  let containerMolecular = document.getElementById("containerMolecular");
 
  geometryIframe.setAttribute("id", formula);
  geometryIframe.setAttribute("data-path", path);
  geometryIframe.setAttribute("data-is-ts", true);
  geometryIframe.setAttribute("data-is-ts-originally", false);
  geometryIframe.setAttribute("data-program", type);
  geometryIframe.setAttribute("data-calcid", formula);
  geometryIframe.setAttribute("data-file", "geometry.cml");
  geometryIframe.classList.add("report-viewer");
  geometryIframe.classList.add("testIframe");
  geometryIframe.setAttribute("src", "../../xslt/jsmol/jsmol-reaction-energy-report.html");
  rowDiv.appendChild(geometryIframe);
  containerMolecular.appendChild(rowDiv);

}


/**
 * Create and display a toast notification on the web page
 * @param {String} text - The message to be displayed in the toast
 * @param {number} delay - Duration (in milliseconds) for which the toast should remain visible before automatically disappearing
 */

function printToast(text, delay){

  let toast = document.createElement("div");
  toast.classList.add("toast");
  toast.classList.add("fade");
  toast.setAttribute("role", "alert");
  toast.setAttribute("aria-live", "assertive");
  toast.setAttribute("aria-atomic", "true");
  toast.innerHTML = `
    
      <button type="button" class="ml-2 mb-1 close" style="background-color:white" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
  
    <div class="toast-body">${text}</div>
  `;

  document.getElementById("toast-container").appendChild(toast);
  $(toast).toast({ delay: delay || 3000 }).toast('show');

  $(toast).on('hidden.bs.toast', function () {
    $(this).remove();
  });

}

/**
 * Apply the substring or 2n title to each node
 */
function applyStylesJson(){
 
  cy.$("nodes").removeClass("elementSelected");
  cy.$("nodes").addClass("nodes2");
  cy.$("nodes").style({
    'text-wrap': 'wrap',
    label: function(node) {
      let labelsplited = node.data("label").split("\\n")
      return `${labelsplited[0]} \n ${node.data("calculations")}`
    }
  })
  showFormulas('true');

}


var cy;
var createNodeForm;
var crudCalculDialog;
var styleOfGraphBtn;
var infoGraphDiv;
 
var orderedFormulas = [];

var nodeSelectedRight = null;
var linkStartNode = null;
var selectedNodeColor = null;
var selectedEdgeColor = null;
var typeFormatForm = "create" || "edit";
var formIsForEdge = false;
var currentEditedFormula = "";
var arrayPathCalculations = [];
var listCalculationsTs = [];
var showFormulasChecked = 'true'
var showEdges = true;

var jsonString = window.frameElement.getAttribute('data-dotgraph');
var jsonSrc = JSON.parse(jsonString);
var transitionStates = JSON.parse(window.frameElement.getAttribute('data-transition-states'));
var originalTransitionStates = JSON.parse(window.frameElement.getAttribute('data-original-transition-states'));
var programs = JSON.parse(window.frameElement.getAttribute('data-programs'));
var calculationsIds = JSON.parse(window.frameElement.getAttribute('data-calculationsIds'));
var reportID = window.frameElement.getAttribute('data-reportID');
var reportTitle = window.frameElement.getAttribute('data-reportTitle');
let jsondatanew2 = {"creator": jsonSrc.creator, "elements": {"nodes":jsonSrc.nodes, "edges":jsonSrc.edges}}


function setupCytoscape(){

 createNodeForm = document.getElementById("createNodeForm");
 crudCalculDialog = document.getElementById("crudDialog");
 styleOfGraphBtn = document.getElementById("styleOfGraph");
 infoGraphDiv = document.getElementById("infoGraphDiv");
    initCytoscape("panelDraw");
    cy.$("node").addClass("nodes2");
    cy.minZoom(0.35);
    cy.maxZoom(3.5);
       
    cy.ready(()=>{
        cy.json(sanitizeJsonData(jsondatanew2))
        applyStylesJson()
        addElementsEvents()
        showFormulas('true');
        cy.layout({ name: 'grid', animate:true }).run()   
    })
}

/**
 * Init the graph of cytoscape and apply the settings
 * @param {String} divPanel - Id of the container (div)
 * @returns {Object} Cytoscape CANVA
 */
function initCytoscape(divPanel) {
  const panelDraw = document.getElementById(divPanel);
   cy = cytoscape({
    container: panelDraw, // container to render in
    group: "nodes",
    wheelSensitivity: 0.1,
    pan: { x: 0, y: 0 },
    minDistance: 150,
    style: `
        node {label:data(id)}
        edge {width: 5%}
        .elementSelected {background-color: greenyellow ; line-color:yellow;}
        .nodes2 {background-color: orange;}
        curve-style: bezier;
      `,
    grabDistance: 100,
  });
  
}
$(document).ready(
  setTimeout(() => {  }, 2500)
)
window.addEventListener('load', function () {
  setupCytoscape()
})
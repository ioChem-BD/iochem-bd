const elementType =[['H','X','X','X','X','X','X','X','X','X','X','X','X','X','X','X','X','He'],
                    ['Li','Be','X','X','X','X','X','X','X','X','X','X','B','C','N','O','F','Ne'],
                    ['Na','Mg','X','X','X','X','X','X','X','X','X','X','Al','Si','P','S','Cl','Ar'],
                    ['K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr'],
                    ['Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te',' I ','Xe'],
                    ['Cs','Ba','X','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn'],
                    ['Fr','Ra','X','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg','Cn','Nh','Fl','Mc','Lv','Ts','Og'],
                    ['X','X','X','La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu'],
                    ['X','X','X','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr']];
const atomCategory =   [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
                        [2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 1, 1, 7, 8],
                        [2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 5, 1, 1, 7, 8],
                        [2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 6, 5, 5, 1, 7, 8],
                        [2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 6, 6, 5, 5, 7, 8],
                        [2, 3, 9, 4, 4, 4, 4, 4, 4, 4, 4, 4, 6, 6, 6, 5, 7, 8],
                        [2, 3,10, 4, 4, 4, 4, 4, 4, 4, 4, 4, 6, 6, 6, 6, 7, 8],
                        [0, 0, 0, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9],
                        [0, 0, 0, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]];


var selectedElements = [];

function buildPeriodicTable(containerSelector, callback){
    var periodicTable = jQuery(containerSelector);
    for(var inx = 0; inx < elementType.length; inx++){
        var period = jQuery("<div class='period'></div>");                
        for(var inx2 = 0; inx2 < elementType[inx].length; inx2++){
            period.append(buildAtomCell(inx,inx2));
        }
        periodicTable.append(period);
    }
    
    
	jQuery(".atom").click(function(event){
		jQuery(event.target.parentNode).toggleClass("atom-selected");		
		const atomType = $(event.target)[0].innerText;
		atomClicked(atomType);
		callback(getSelectedElements());
	});
    
}

function atomClicked(atomType){
	if(selectedElements.indexOf(atomType) !== -1){
		selectedElements.splice(selectedElements.indexOf(atomType), 1);		
	}else{
		selectedElements.push(atomType);
	}
}


function resetPeriodicTable(){
	selectedElements = [];
	jQuery('.atom').removeClass('atom-selected');
}

function getSelectedElements(){	
	return selectedElements.join(' ');
}

function buildAtomCell(period, group){
    const atomType = elementType[period][group];
    const category = atomCategory[period][group];
    var cell = jQuery("<div></div>");
    const catClass = 'cat' + category;
    cell.addClass(catClass);
    if(atomType !== 'X' && atomType !== 'H'){
        cell.addClass('atom');
        cell.html("<div class='innercell'>" + atomType + "</div>");                    
    }else{
        cell.addClass('empty');
        if(atomType === 'H')
        	cell.html("<div class='innercell'>" + atomType + "</div>");
        else	
        	cell.html('&nbsp;');
    }
    return cell;            
}

function displayPeriodicTable(tableId, visibility){	
	if(visibility === true){
		jQuery(tableId).css("display", "initial");		
	}else{
		jQuery(tableId).css("display", "none");
	}
}

<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet   
    xmlns="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:adf="http://www.scm.com/ADF/"
    xmlns:gaussian="http://www.gaussian.com/"  
    xmlns:v="http://www.iochem-bd.org/dictionary/vasp/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="#all"    
    version="3.0">
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/adf.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/gaussian.xsl"/>
    <xsl:include href="../../xslt/helper/math/fxsl/product.xsl"/>

    <xsl:output method="text" />
	<xsl:param name="negativeFrequencyThreshold"/>


    <xsl:template match='/module'>    
            <xsl:call-template name="isTransitionState" />    
    </xsl:template>


<xsl:template name="isTransitionState">
        <xsl:variable name="program" select="upper-case((//parameter[@dictRef='cc:program'])[last()])" />
        <xsl:variable name="threshold" select="number($negativeFrequencyThreshold)"/>
        <xsl:choose>
            <xsl:when test="exists(//module[@dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])">                
    		        <xsl:variable name="frequencyArray"> 
    		            <xsl:choose>
    		                <xsl:when test="matches($program, 'ADF|AMS')"> <!-- Specific ADF SCANFREQ correction -->
    		                    <xsl:variable name="correctedFrequencies" select="adf:getFrequencies(
                                                                                                        .//module[@id='finalization']/propertyList/property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations'],
                                                                                                        .//module[@id='finalization']//property/module[@cmlx:templateRef='scanfreq']
                        		                                                                      )"/>
    		                    <xsl:copy-of select="helper:trim(replace(($correctedFrequencies//array[@dictRef='cc:frequency'])[position() = last()], '^(\d+\.\d+(?:[Ee][\-\+]\d+)?)|(\s+\d+\.\d+(?:[Ee][\-\+]\d+)?)', ''))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="helper:trim(replace((//module[@dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])[position() = last()], '^(\d+\.\d+(?:[Ee][\-\+]\d+)?)|(\s+\d+\.\d+(?:[Ee][\-\+]\d+)?)', ''))"/>                                          
    		                </xsl:otherwise>
    		            </xsl:choose>
    		        </xsl:variable>
                  
                    
                    <xsl:variable name="negativeFrequencies">
                        <xsl:for-each select="tokenize($frequencyArray, '\s+')">
                            <xsl:variable name="negativeFrequency" select="."/>
                            <xsl:if test="number($negativeFrequency) &lt; number($threshold)">
                                <xsl:element  namespace="http://www.xml-cml.org/schema" name="frequency"><xsl:value-of select="$negativeFrequency"/></xsl:element>
                            </xsl:if>    
                        </xsl:for-each>    
                    </xsl:variable>                  
    		                           
    		        <xsl:value-of select="count($negativeFrequencies/frequency) = 1"/>
    		    </xsl:when>
    		    <xsl:otherwise>
    		        <xsl:value-of select="'false'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
  

<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    version="2.0">
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <xsl:param name="reportTitle"/>
    <xsl:param name="reportDescription"/>
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
     
    <xsl:variable name="CM-1_ENERGY" select="'nonsi:cm-1'"/>
    <xsl:variable name="KCALMOL_ENERGY" select="'nonsi2:kcal.mol-1'"/>
    <xsl:variable name="KJMOL_ENERGY" select="'nonsi:kj.mol-1'"/>
    <xsl:variable name="EV_ENERGY" select="'nonsi:electronvolt'"/>
    <xsl:variable name="EH_ENERGY" select="'nonsi:hartree'"/>
    
    
    <xsl:variable name="calculations" select="//template"/>

    <!-- Used to calculate all column headers and know in which one to place our calculated values -->
    <xsl:variable name="columnHeaders"> 
        <xsl:element name="columns"> 
            <xsl:variable name="allColumnHeaders" select="helper:getHeaders()"/>
            <xsl:for-each select="distinct-values($allColumnHeaders)">
                <xsl:variable name="columnName" select="."/>
                <xsl:if test="compare($columnName,'#') != 0">
                    <xsl:element name="column">
                        <xsl:attribute name="number" select="position()"/>
                        <xsl:attribute name="name" select="$columnName"/>
                        <xsl:attribute name="value" select="$columnName"/>
                    </xsl:element>                       
                </xsl:if>                
            </xsl:for-each>
        </xsl:element>
    </xsl:variable>
    
    <xsl:function name="helper:getHeaders">
        <xsl:for-each select="$calculations">
            <xsl:variable name="calculation" select="."/>
            <xsl:variable name="functional" select="$calculation/functional"/>
            <xsl:variable name="basis">
                <xsl:for-each select="distinct-values(tokenize(replace($calculation/basis,'(\w+#)',''),'\s+'))">
                    <xsl:value-of select="."/><xsl:text> </xsl:text>                    
                </xsl:for-each>
            </xsl:variable>
            <xsl:if test="($functional != '') and ($basis != '')">
                <xsl:value-of select="helper:trim(concat($functional, '#', $basis))"/>    
            </xsl:if>                                    
        </xsl:for-each>
    </xsl:function>
    
    <xsl:template match="/">        
        <fo:root>
            <fo:layout-master-set>    		
                <fo:simple-page-master master-name="calculations"
                    page-height="21cm" 
                    page-width="29.7cm"
                    margin-top="0in"
                    margin-bottom="0in"
                    margin-left = "0.75in"
                    margin-right="0.75in">	    			
                    <fo:region-body 				
                        margin-top="0.6in" 
                        margin-bottom="0.6in"/>    			
                    <fo:region-before 
                        extent="0.5in"/>    			
                    <fo:region-after  
                        extent="0.5in"/>			
                </fo:simple-page-master>                
            </fo:layout-master-set>
            
            <!-- Print main report data -->
            <fo:page-sequence master-reference="calculations" id="reportPage">	
                <!-- Header section -->
                <fo:static-content flow-name="xsl-region-before">
                    <fo:list-block provisional-distance-between-starts="5in" provisional-label-separation="0in">
                        <fo:list-item>
                            <fo:list-item-label>
                                <fo:block  text-align="start"><xsl:value-of select="$reportTitle"/></fo:block>
                            </fo:list-item-label>
                            <fo:list-item-body>
                                <fo:block  text-align="end"></fo:block>
                            </fo:list-item-body>
                        </fo:list-item>
                    </fo:list-block>
                </fo:static-content>
                <!-- Footer section -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:list-block provisional-distance-between-starts="5in" provisional-label-separation="0in">
                        <fo:list-item>
                            <fo:list-item-label>
                                <fo:block  text-align="start"></fo:block>
                            </fo:list-item-label>
                            <fo:list-item-body>
                                <fo:block  text-align="end">Page <fo:page-number/></fo:block>
                            </fo:list-item-body>
                        </fo:list-item>
                    </fo:list-block>
                </fo:static-content>	
                <!-- Body section -->
                <fo:flow flow-name="xsl-region-body" font-family="Courier">				    			
                    <fo:block break-before="page">                            
                        <fo:block><fo:inline font-size="1em">Report : Spin state energies for transition metal complexes (v2)</fo:inline></fo:block>                                                    
                        <xsl:if test="exists($reportDescription)">
                            <fo:block>Compound : <xsl:value-of select="$reportDescription"/></fo:block>
                        </xsl:if>
                        <fo:block><fo:inline font-family='Symbol'>Δ</fo:inline>E(HL)<xsl:value-of select="helper:printUnitSymbol(($calculations/template)[1]/finalenergy/@units)"/></fo:block>
                        
                        <fo:table>                            
                            <fo:table-column column-width="10%"/>
                            <fo:table-column column-width="3%"/>
                            <xsl:for-each select="$columnHeaders/columns/column">                                
                                <fo:table-column></fo:table-column>
                            </xsl:for-each> 
                            <fo:table-header>
                                <fo:table-row>
                                    <fo:table-cell border-style="none" border-width="0.1mm">
                                        <fo:block>-</fo:block>                                        
                                        <fo:block>Complex</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block>Functional</fo:block>
                                        <fo:block>Basis</fo:block>
                                    </fo:table-cell>                                    
                                    <xsl:for-each select="$columnHeaders/columns/column">
                                        <xsl:variable name="column" select="."/>
                                        <fo:table-cell border-style="none" border-width="0.1mm">
                                            <xsl:call-template name="printColumnHeader">
                                                <xsl:with-param name="columnName" select="$column/@name"></xsl:with-param>
                                            </xsl:call-template>
                                        </fo:table-cell>
                                    </xsl:for-each>                                             
                                </fo:table-row>
                            </fo:table-header>
                            <fo:table-body>
                                
                                <!--
                                <fo:table-row >
                                    <fo:table-cell number-columns-spanned="{count($columnHeaders/columns/column) + 1}">
                                        <fo:block font-size="0.8em"> </fo:block>
                                    </fo:table-cell>           
                                </fo:table-row>
                                -->
                                <xsl:call-template name="printEnergyRows"/>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>                   
                </fo:flow>
            </fo:page-sequence>
                   
            <!-- Print errors -->
            <fo:page-sequence master-reference="calculations" id="errorsPage">                
                <fo:flow flow-name="xsl-region-body" font-family="Courier">
                    <fo:block break-before="page"><fo:inline font-size="1em">Errors on grouping</fo:inline></fo:block>
                    <fo:table font-size="1em" border-style="solid" border-width="0.1mm">                            
                        <fo:table-column column-width="100%"/>                     
                        <fo:table-header>
                            <fo:table-row>
                                <fo:table-cell><fo:block>Error</fo:block></fo:table-cell>                                                                                                 
                            </fo:table-row>
                        </fo:table-header>
                        <fo:table-body>
                            <fo:table-row >
                                <fo:table-cell>
                                    <fo:block font-size="0.8em"> </fo:block>
                                </fo:table-cell>           
                            </fo:table-row>
                            <xsl:call-template name="printEnergyRowErrors"/>
                        </fo:table-body>
                    </fo:table>
                </fo:flow>
            </fo:page-sequence>          

            <!-- Data resume -->
            <fo:page-sequence master-reference="calculations" id="supportingInformationPage">                
                <fo:flow flow-name="xsl-region-body" font-family="Courier">
                    <fo:block break-before="page"><fo:inline font-size="1em">Source information</fo:inline></fo:block>
                    <fo:table font-size="0.5em" border-style="solid" border-width="0.1mm">                            
                        <fo:table-column column-width="10%"/>
                        <fo:table-column column-width="30%"/>                        
                        <fo:table-column column-width="4%"/>
                        <fo:table-column column-width="20%"/>
                        <fo:table-column column-width="6%"/>
                        <fo:table-column column-width="30%"/>
                        <fo:table-header>
                            <fo:table-row>
                                <fo:table-cell><fo:block>Name</fo:block></fo:table-cell>                                                                    
                                <fo:table-cell><fo:block>Inchi</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block>Mult.</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block>Energy</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block>Functional</fo:block></fo:table-cell>                                
                                <fo:table-cell><fo:block>Basis</fo:block></fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>
                        <fo:table-body>   
                            <xsl:call-template name="printSupportingInformationRows"/>                                
                        </fo:table-body>
                    </fo:table>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <!--  *****************************  Differential energies table generation template ************************************ -->

    <xsl:template name="printEnergyRows">    
        <xsl:variable name="inchiSet" select="distinct-values(//template/inchi)"/>
        <xsl:for-each select="$inchiSet">
            <xsl:variable name="inchi" select="."/>
            <!-- First, group by inchi -->
            <xsl:variable name="sameInchiCalculations" select="$calculations[compare(inchi,$inchi) = 0]"/>            
            <!-- All this calculations now go in the same row of the table, we must generate a variable with all matching values, if column matches (functional+basis) we print values, otherwise we print blanks -->
            <xsl:variable name="rowValues">
                <xsl:variable name="functionalSet" select="distinct-values($sameInchiCalculations/functional)"/>            
                <xsl:for-each select="$functionalSet">                                    
                    <xsl:variable name="functional" select="."/>
                    <xsl:variable name="sameFunctionalCalculations" select="$sameInchiCalculations[compare(functional,$functional) = 0]"/>
                    <xsl:variable name="basisSet" select="distinct-values($sameFunctionalCalculations/basis)"/>
                    <xsl:for-each select="$basisSet">
                        <xsl:variable name="basis" select="."/>
                        <xsl:variable name="sameBasisCalculations" select="$sameFunctionalCalculations[compare(basis,$basis) = 0]"/>                        
                        <xsl:element name="cellValue">
                            <xsl:attribute name="functionalSet" select="$functional"/>
                            <xsl:attribute name="basisSet" select="$basis"/>                            
                            <xsl:attribute name="name" select="helper:getCommonName($sameBasisCalculations)"/>
                            <xsl:attribute name="energyUnits" select="($sameBasisCalculations[1])/finalenergy/@units"/>
                            <xsl:attribute name="energy" select="helper:calculateDifferentialEnergy($sameBasisCalculations)"/>
                            
                            <!-- Set column header -->
                            <xsl:variable name="functional" select="$sameBasisCalculations[1]/functional"/>
                            <xsl:variable name="basis">
                                <xsl:for-each select="distinct-values(tokenize(replace($sameBasisCalculations[1]/basis,'(\w+#)',''),'\s+'))">
                                    <xsl:value-of select="."/><xsl:text> </xsl:text>                    
                                </xsl:for-each>
                            </xsl:variable>                                                       
                            <xsl:attribute name="column" select="helper:trim(concat($functional, '#',$basis ))"/>
                        </xsl:element>                        
                    </xsl:for-each>
                </xsl:for-each>                
            </xsl:variable>          
            <xsl:call-template name="printRow">
                <xsl:with-param name="rowValues" select="$rowValues"/>
            </xsl:call-template>
            
        </xsl:for-each>  
    </xsl:template>

    <xsl:function name="helper:calculateDifferentialEnergy">
        <xsl:param name="calculations"/>
        <xsl:if test="count($calculations) >= 2">
            <xsl:variable name="maxMultiplicity" select="max($calculations/multiplicity)"/>
            <xsl:variable name="minMultiplicity" select="min($calculations/multiplicity)"/>
            <xsl:if test="  (count($calculations[child::multiplicity = $maxMultiplicity]) = 1)
                            and
                            (count($calculations[child::multiplicity = $minMultiplicity]) = 1)">
                <xsl:variable name="minMultiplicityCalculation" select="$calculations[multiplicity = $minMultiplicity]"/>
                <xsl:variable name="maxMultiplicityCalculation" select="$calculations[multiplicity = $maxMultiplicity]"/>                
                <xsl:value-of select="number($minMultiplicityCalculation/finalenergy) - number($maxMultiplicityCalculation/finalenergy)" />                               
            </xsl:if>
        </xsl:if>
    </xsl:function>

    <xsl:template name="printRow">
        <xsl:param name="rowValues"/>
        <xsl:value-of select="$rowValues"/>

        <fo:table-row border-style="solid" border-width="0.1mm">            
            <fo:table-cell>
              <fo:block><xsl:value-of select="helper:getCommonNameFromArray($rowValues/cellValue/@name)"/></fo:block>
<!--
                <xsl:for-each select="distinct-values($rowValues/cellValue/@name)">
                    <fo:block><xsl:value-of select="."/></fo:block>    
                </xsl:for-each>
-->
            </fo:table-cell>
            <fo:table-cell>
                <fo:block></fo:block>
            </fo:table-cell>
            <xsl:for-each select="1 to count($columnHeaders/columns/column)">
                <xsl:variable name="outerIndex" select="number(.)"/>
             <xsl:variable name="currentColumnValue" select="($columnHeaders/columns/column)[$outerIndex]/@value"/>
                <xsl:variable name="hasMatchingValues" select="exists($rowValues/cellValue[@column = $currentColumnValue])"/>                
                <xsl:choose>
                    <xsl:when test="$hasMatchingValues">
                        <xsl:variable name="matchValue" select="$rowValues/cellValue[@column = $currentColumnValue]"/>
                        <fo:table-cell>
                            <fo:block text-align="right">
                                <xsl:variable name="energyUnits" select="$matchValue/@energyUnits"/>
                                <xsl:variable name="energy" select="$matchValue/@energy"/>
                                <xsl:value-of select="helper:formatEnergy($energy, $energyUnits)"/>
                            </fo:block>
                        </fo:table-cell>                        
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:table-cell>
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </fo:table-row>
    </xsl:template>

    <xsl:function name="helper:getCommonName">
        <xsl:param name="calculations"/>        
        <xsl:variable name="maxMultiplicity" select="max($calculations/multiplicity)"/>
        <xsl:variable name="minMultiplicity" select="min($calculations/multiplicity)"/>
        <xsl:if test="  (count($calculations[child::multiplicity = $maxMultiplicity]) = 1)
            and 
            (count($calculations[child::multiplicity = $minMultiplicity]) = 1)">            
            <xsl:variable name="maxName" select="$calculations[child::multiplicity = $maxMultiplicity]/@name"/>
            <xsl:variable name="minName" select="$calculations[child::multiplicity = $minMultiplicity]/@name"/>                        
            <xsl:variable name="commonName">
                <xsl:for-each select="1 to string-length($maxName)">
                    <xsl:variable name="outerIndex" select="."/>
                    <xsl:if test="compare(substring($maxName, $outerIndex, 1),substring($minName, $outerIndex, 1))= 0"><xsl:value-of select="substring($maxName, $outerIndex, 1)"/></xsl:if>            
                </xsl:for-each>
            </xsl:variable>
            <xsl:for-each select="1 to string-length($commonName)">
                <xsl:variable name="outerIndex" select="."/>
                <xsl:if test="compare(substring($commonName, $outerIndex, 1),substring($maxName, $outerIndex, 1))= 0"><xsl:value-of select="substring($commonName, $outerIndex, 1)"/></xsl:if>            
            </xsl:for-each>
        </xsl:if>
    </xsl:function>

    <xsl:function name="helper:formatEnergy">
        <xsl:param name="energy"/>
        <xsl:param name="energyUnits"/>
        <xsl:choose>
            <xsl:when test="compare($energyUnits, $CM-1_ENERGY) = 0"><xsl:value-of select="round(number($energy))"/></xsl:when>
            <xsl:when test="compare($energyUnits, $KCALMOL_ENERGY) = 0"><xsl:value-of select="round(number($energy) * 10) div 10"/></xsl:when>
            <xsl:when test="compare($energyUnits, $KJMOL_ENERGY) = 0"><xsl:value-of select="round(number($energy) * 100) div 100"/></xsl:when>
            <xsl:when test="compare($energyUnits, $EV_ENERGY) = 0"><xsl:value-of select="round(number($energy) * 100) div 100"/></xsl:when>
            <xsl:when test="compare($energyUnits, $EH_ENERGY) = 0"><xsl:value-of select="round(number($energy) * 1000000) div 1000000"/></xsl:when>
        </xsl:choose> 
    </xsl:function>

    <xsl:template name="printColumnHeader">
        <xsl:param name="columnName"/>
        <xsl:variable name="functionalBasis" select="tokenize($columnName,'/')"/>
        <fo:block text-align="right"><xsl:value-of select="tokenize($columnName,'#')[1]"/></fo:block>
        <fo:block text-align="right"><xsl:value-of select="tokenize($columnName,'#')[2]"/></fo:block>
        
        <xsl:variable name="basis" select="tokenize(normalize-space(replace($functionalBasis[2],'\w+#','')),'\s+')"/>
        <xsl:for-each select="distinct-values($basis)">
            <xsl:value-of select="."/>
            <xsl:text> </xsl:text>
        </xsl:for-each>
    </xsl:template>

    <xsl:function name="helper:getColumnHeader" as="xs:string">
        <xsl:param name="calculation" />        
        <xsl:variable name="functional" select="$calculation/functional"/>
        <xsl:variable name="basis">
            <xsl:for-each select="distinct-values(tokenize(replace($calculation/basis,'(\w+#)',''),'\s+'))">
                <xsl:value-of select="."/><xsl:text> </xsl:text>                    
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="helper:trim(concat($functional, '#', $basis))"/>        
    </xsl:function>

    <xsl:function name="helper:getCommonNameFromArray">
        <xsl:param name="candidates" as="xs:string*"/>
        <xsl:choose>            
            <xsl:when test="count($candidates) = 1">        <!-- Only one candidate -->
                <xsl:value-of select="$candidates"/>
            </xsl:when>
            <xsl:otherwise> 
                <xsl:variable name="result">
                    <xsl:for-each select="1 to string-length($candidates[1])">
                        <xsl:variable name="position" select="."/>
                        <xsl:value-of select="helper:charMatching( $position, $candidates,  substring(($candidates)[1], $position, 1))"/>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="result1" select="tokenize($result,'[$+]')"/>
                <xsl:choose>
                    <xsl:when test="string-length($result1[1]) > 0">
                        <xsl:value-of select="$result1[1]"/>                        
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:for-each select="$candidates">
                            <fo:block><xsl:value-of select="."/></fo:block>    
                        </xsl:for-each>
                    </xsl:otherwise>
                </xsl:choose>
                
            </xsl:otherwise>            
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="helper:charMatching">
        <xsl:param name="position"/>
        <xsl:param name="values"/>
        <xsl:param name="firstChar"/>
        
        <xsl:variable name="matches">
            <xsl:for-each select="$values">
                <xsl:variable name="currentCharr" select="substring(.,$position,1)"/>
                <xsl:choose>
                    <xsl:when test="not(matches($firstChar, $currentCharr))">$</xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                </xsl:choose>            
            </xsl:for-each>            
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="contains($matches,'$')">$</xsl:when>
            <xsl:otherwise><xsl:value-of select="$firstChar"/></xsl:otherwise>
        </xsl:choose>
        

    </xsl:function>


    <!--  *****************************  Differential energies error table generation template ************************************ -->

    <xsl:template name="printEnergyRowErrors">        
        <xsl:variable name="inchiSet" select="distinct-values(//template/inchi)"/>
        <xsl:for-each select="$inchiSet">
            <xsl:variable name="inchi" select="."/>
            <xsl:variable name="sameInchiCalculations" select="$calculations[compare(inchi,$inchi) = 0]"/>
            <xsl:variable name="functionalSet" select="distinct-values($sameInchiCalculations/functional)"/>            
            <xsl:for-each select="$functionalSet">
                <xsl:variable name="functional" select="."/>
                <xsl:variable name="sameFunctionalCalculations" select="$sameInchiCalculations[compare(functional,$functional) = 0]"/>
                <xsl:variable name="basisSet" select="distinct-values($sameFunctionalCalculations/basis)"/>
                <xsl:for-each select="$basisSet">
                    <xsl:variable name="basis" select="."/>
                    <xsl:variable name="sameBasisCalculations" select="$sameFunctionalCalculations[compare(basis,$basis) = 0]"/>
                    <xsl:call-template name="printErrorsOnCalculateDifferentialEnergy">
                        <xsl:with-param name="calculations" select="$sameBasisCalculations"/>
                    </xsl:call-template>                    
                </xsl:for-each>
            </xsl:for-each>
        </xsl:for-each>  
    </xsl:template>
 
    <xsl:template name="printErrorsOnCalculateDifferentialEnergy">
        <xsl:param name="calculations"/>
        <xsl:choose>
            <xsl:when test="count($calculations) = 1">
                <xsl:call-template name="printError">
                    <xsl:with-param name="message" select="concat('Error. Unpaired calculation on ', $calculations/@name)"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="maxMultiplicity" select="max($calculations/multiplicity)"/>
                <xsl:variable name="minMultiplicity" select="min($calculations/multiplicity)"/>
                <xsl:choose>
                    <xsl:when test="count($calculations[child::multiplicity = $maxMultiplicity]) > 1">
                        <xsl:call-template name="printError">
                            <xsl:with-param name="message" select="concat('Error. Two or more calculations matching max(multiplicity) for ', $calculations[1]/@name)"/>
                        </xsl:call-template>                              
                    </xsl:when>
                    <xsl:when test="count($calculations[child::multiplicity = $minMultiplicity]) > 1">
                        <xsl:call-template name="printError">                                                
                            <xsl:with-param name="message" select="concat('Error. Two or more calculations matching min(multiplicity) for ', $calculations[1]/@name)"/>
                        </xsl:call-template>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="printError">
        <xsl:param name="message"/>
        <fo:table-row >
            <fo:table-cell>
                <fo:block font-size="0.8em"><xsl:value-of select="$message"/></fo:block>
            </fo:table-cell>           
        </fo:table-row>     
    </xsl:template>
  
    <!--  *****************************  Supporting information table generation template ************************************ -->

    <xsl:template name="printSupportingInformationRows">
        <xsl:for-each select="$calculations">
            <xsl:sort select="inchi"></xsl:sort>
            <xsl:sort select="functional"></xsl:sort>
            <xsl:sort select="basis"></xsl:sort>
            <xsl:sort select="multiplicity" order="ascending"/>
            <xsl:variable name="calculation" select="."/>
            <fo:table-row border-style="solid" border-width="0.1mm">
                <fo:table-cell><fo:block wrap-option="wrap"><xsl:value-of select="$calculation/@name"/></fo:block></fo:table-cell>            
                <fo:table-cell><fo:block wrap-option="wrap"><xsl:value-of select="$calculation/inchi"/></fo:block></fo:table-cell>
                <fo:table-cell><fo:block wrap-option="wrap"><xsl:value-of select="$calculation/multiplicity"/></fo:block></fo:table-cell>
                <fo:table-cell><fo:block wrap-option="wrap"><xsl:value-of select="$calculation/finalenergy"/></fo:block></fo:table-cell>
                <fo:table-cell><fo:block wrap-option="wrap"><xsl:value-of select="$calculation/functional"/></fo:block></fo:table-cell>
                <fo:table-cell><fo:block wrap-option="wrap"><xsl:value-of select="$calculation/basis"/></fo:block></fo:table-cell>                               
            </fo:table-row>       
        </xsl:for-each>        
    </xsl:template>

    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>
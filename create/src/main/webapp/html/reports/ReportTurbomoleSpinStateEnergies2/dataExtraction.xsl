<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.xml-cml.org/schema" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"    
    xmlns:turbo="http://www.turbomole.com"
    xmlns:gaussian="http://www.gaussian.com/"
    xmlns:orca="https://orcaforum.cec.mpg.de/"
    xmlns:t="http://www.turbomole.com"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="#all"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:include href="../../xslt/helper/chemistry/turbomole.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/gaussian.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/orca.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
    
    <xsl:param name="title"/>
    <xsl:param name="energyUnits"/>
    
    <!-- We'll cut last section of Inchi to hit more matches -->
    <xsl:variable name="inchi">
        <xsl:variable name="fullInchi" select="(//module[@dictRef='cc:finalization']//molecule/formula[@convention='iupac:inchi']/@inline)[last()]"/>
        <xsl:choose>
            <xsl:when test="count(tokenize($fullInchi,'/')) &gt;= 5">
                <xsl:call-template name="substring-before-last">
                    <xsl:with-param name="list" select="normalize-space($fullInchi)"/>
                    <xsl:with-param name="delimiter" select="'/'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fullInchi"/>        
            </xsl:otherwise>
        </xsl:choose>                
    </xsl:variable>    
    
    <!-- Used for energy field -->    
    <xsl:template match="/">
        <template id="calculation" name="{$title}">            
            <xsl:element name="multiplicity">
                <xsl:choose>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'TURBOMOLE')">
                        <xsl:variable name="orbitalSpecsAlpha" select="//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='unrestrictedorbitals' and compare(child::scalar[@dictRef='t:spintype']/text(),'alpha') = 0]"/>                 
                        <xsl:variable name="orbitalSpecsBeta" select="//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='unrestrictedorbitals' and compare(child::scalar[@dictRef='t:spintype']/text(),'beta') = 0]"/>
                        <xsl:variable name="occupiedAlpha" select="tokenize(replace($orbitalSpecsAlpha/array[@dictRef='t:mosrange'],'\s*-\s*','-'),'\s+')"/>
                        <xsl:variable name="occupiedBeta" select="tokenize(replace($orbitalSpecsBeta/array[@dictRef='t:mosrange'],'\s*-\s*','-'), '\s+')"/>
                        <xsl:choose>                
                            <xsl:when test="exists($occupiedAlpha)"><xsl:value-of select="turbo:calculateMultiplicity($occupiedAlpha, $occupiedBeta)"/></xsl:when>
                            <xsl:otherwise><xsl:value-of select="1"/></xsl:otherwise>
                        </xsl:choose>                    
                    </xsl:when>  
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">
                        <xsl:value-of select="(//module[@id='initialization']/molecule/@spinMultiplicity)[1]"/>
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ORCA')">
                        <xsl:value-of select="(//module[@name='General Settings']//scalar[@dictRef='cc:parameter' and text() = 'Mult']/following-sibling::scalar[@dictRef='cc:value'])[1]"/>
                    </xsl:when>                                        
                </xsl:choose> 
                
            </xsl:element>
            
            <xsl:element name="finalenergy">
                <xsl:choose>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'TURBOMOLE')">
                        <xsl:variable name="energy" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:energy']/scalar)[last()]"/>
                        <xsl:variable name="energyresume" select="(//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@id='energy.resume'])[last()]"/>                                                    
                        <xsl:choose>
                            <xsl:when test="not(exists($energyresume/scalar[@dictRef='t:rhfEnergy']))">
                                <xsl:attribute name="units" select="$energyUnits"/>        
                                <xsl:value-of select="helper:convertEnergyUnits($energy,$energyUnits)"/>    
                            </xsl:when>
                            <xsl:when test="exists($energyresume/scalar[@dictRef='t:rhfEnergy'])">
                                <xsl:attribute name="units" select="$energyUnits"/>
                                <xsl:value-of select="helper:convertEnergyUnits($energyresume/scalar[@dictRef='t:rhfEnergy'],$energyUnits)"/>
                            </xsl:when>
                        </xsl:choose>        
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">
                        <xsl:attribute name="units" select="$energyUnits"/>
                        <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])[last()],$energyUnits)"/>                        
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ORCA')">
                        <xsl:attribute name="units" select="$energyUnits"/>
                        <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:totalener'])[last()],$energyUnits)"/>
                    </xsl:when>
                </xsl:choose>              
            </xsl:element>
            
            <xsl:element name="inchi">
                <xsl:value-of select="$inchi"/>
            </xsl:element>
            
            <xsl:element name="functional">
                <xsl:choose>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'TURBOMOLE')">
                        <xsl:value-of select="helper:normalizeFunctional(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:functional']/scalar)"/>
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">
                        <xsl:for-each select="distinct-values(tokenize(string-join(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:method']/scalar,' '), '[ /\(\):,]+'))">
                            <xsl:variable name="candidate" select="."/>
                            <xsl:if test="gaussian:isMethod($candidate)">
                                <xsl:value-of select="helper:normalizeFunctional(helper:filterFunctional($candidate))"/><xsl:text> </xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ORCA')">
                        <!-- Get functionals from all jos -->
                        <xsl:variable name="functionals">
                            <xsl:for-each select="(//module[@id='initialization'])[1]//module[@cmlx:templateRef='job']">
                                <xsl:variable name="commands" select="."/>
                                <xsl:variable name="position" select="position()" />

                                <xsl:variable name="scfsettings" select="(//module[@id='initialization'])[$position]//module[@cmlx:templateRef='scfsettings'][1]" />
                                <xsl:value-of select="helper:normalizeFunctional((orca:getFunctionals($commands, $scfsettings))[1])"/><xsl:text> </xsl:text>                        
                            </xsl:for-each>
                        </xsl:variable>
                        <xsl:variable name="functionalsStr">
                            <xsl:for-each select="distinct-values(tokenize($functionals, '\s+'))">
                                <xsl:value-of select="."/><xsl:text> </xsl:text>    
                            </xsl:for-each>     
                        </xsl:variable>
                       <xsl:value-of select="helper:trim($functionalsStr)"/>
                    </xsl:when>
                </xsl:choose>                
            </xsl:element>
            
            <xsl:element name="basis">  
                <xsl:choose>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'TURBOMOLE')">
                        <xsl:variable name="element" select="tokenize((//module[@dictRef='cc:calculation']//module[@cmlx:templateRef='basisset']/list/array[@dictRef='cc:atomType'])[1],'\s+')"/>
                        <xsl:variable name="basis"   select="tokenize((//module[@dictRef='cc:calculation']//module[@cmlx:templateRef='basisset']/list/array[@dictRef='t:basis'])[1],'\s+')"/>
                        <xsl:variable name="concatenatedBasis">
                            <xsl:element name="root" namespace="http://www.xml-cml.org/schema">
                                <xsl:for-each select="1 to count($element)">
                                    <xsl:variable name="outerIndex" select="."/>
                                    <xsl:element name="basis">
                                        <xsl:attribute name="value" select="concat($element[$outerIndex],'#',$basis[$outerIndex])"/>
                                    </xsl:element>
                                </xsl:for-each>          
                            </xsl:element>
                        </xsl:variable>                        
                        <xsl:for-each select="$concatenatedBasis/root/basis">
                            <xsl:sort select="@value"/>
                            <xsl:value-of select="@value"/><xsl:text> </xsl:text>                    
                        </xsl:for-each>                        
                    </xsl:when>                
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">
                        <xsl:variable name="initialMolecule" select="(//module[@dictRef='cc:initialization' and child::molecule])[1]/molecule"/>                           
                        <xsl:variable name="finalMolecule" select="(//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule[@id='mol9999']"/>
                        <xsl:variable name="molecule" select="
                            if(exists($finalMolecule)) 
                                then 
                                    $finalMolecule
                                else
                                    $initialMolecule
                            "/>
                        <xsl:variable name="centers" select="(//module[@id='job']//module[@cmlx:templateRef='l301.basis2'])[1]/module[@cmlx:templateRef='centers']"/>
                        <xsl:variable name="centers2" select="distinct-values(//module[@id='initialization']/parameterList/parameter[@dictRef='cc:basis']/scalar)"/>
                        <xsl:variable name="oniombasis" select="//module[@id='calculation']//module[@cmlx:templateRef='l120a']"/>                      
                        
                       <xsl:variable name="allAtomBasis">                           
                            <xsl:for-each select="$molecule/atomArray/atom">                            
                                <xsl:variable name="outerIndex" select="position()"/>
                                <xsl:variable name="id" select="@id"/>
                                <xsl:variable name="elementType" select="@elementType"/> 
                                <xsl:variable name="type">
                                    <xsl:choose>
                                        <xsl:when test="exists($centers)">
                                            <xsl:for-each select="$centers">
                                                <xsl:variable name="basis" select="./scalar[@dictRef='cc:basis']"/>
                                                <xsl:for-each select="tokenize(./array[@dictRef='cc:atomcount'],'(\s|\n)')">                               
                                                    <xsl:if test=".=string($outerIndex)">                                                                  
                                                        <xsl:value-of select="$basis"/><xsl:text>+</xsl:text>
                                                    </xsl:if>                                    
                                                </xsl:for-each>
                                            </xsl:for-each>                                                
                                        </xsl:when>
                                        <xsl:when test="exists($oniombasis)">
                                            <xsl:variable name="level" select="(//module[@cmlx:templateRef='l101.qmmm'])[1]/list[@cmlx:templateRef='isotope'][$outerIndex]/scalar[@dictRef='x:layer']"/>
                                            <xsl:choose>
                                                <xsl:when test="$level = 'H'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'high']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                <xsl:when test="$level = 'M'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'med']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                <xsl:when test="$level = 'L'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'low']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                            </xsl:choose>
                                        </xsl:when>                                           
                                        <xsl:otherwise>
                                            <xsl:value-of select="$centers2"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
    
                                <xsl:element name="basis">
                                    <xsl:attribute name="value" select="concat($elementType, '#', $type)"/>
                                </xsl:element>
                            </xsl:for-each>
                       </xsl:variable>                
           
                       <xsl:for-each select="distinct-values($allAtomBasis/basis/@value)">
                            <xsl:variable name="basis" select="."/>
                            <xsl:choose>
                                <xsl:when test="not(ends-with($basis,'+'))">
                                    <xsl:value-of select="concat($basis, ' ')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat(substring($basis,1,string-length($basis)-1),' ')"/>
                                </xsl:otherwise>
                            </xsl:choose>                                
                       </xsl:for-each>                     
                    </xsl:when>                    
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ORCA')">
                        <xsl:variable name="molecule" select="(//module[@dictRef='cc:finalization']//molecule)[last()]"/>
                        <xsl:variable name="basisecp" select="(//module[@id='initialization']/parameterList/module[@cmlx:templateRef='basisecp'])[last()]" />
                        <xsl:variable name="userdefbasis" select="(//module[@cmlx:templateRef='input']/module[@cmlx:templateRef='job'])[last()]/module[@cmlx:templateRef='basis']"/>                        
                    
                        <xsl:variable name="commands" select="((//module[@id='initialization'])[1]//module[@cmlx:templateRef='job'])[last()]"/>
                        <xsl:variable name="position" select="count(//module[@cmlx:templateRef='job'])" />
                        
                        <xsl:variable name="scfsettings" select="(//module[@id='initialization'])[$position]//module[@cmlx:templateRef='scfsettings'][1]" />
                        <xsl:variable name="basissets" select="orca:getBasis($commands)"/>
                        <xsl:variable name="functionals" select="orca:getFunctionals($commands, $scfsettings)"/>
                        
                        <xsl:variable name="isRestricted" select="exists((//module[@id='initialization'])[$position]//module[@cmlx:templateRef='optsetup'])"/>

                        <xsl:variable name="isCalculationLevel" select="boolean($basissets/@isCalculationLevel)"/>
                        <xsl:variable name="basis" select="
                            if(boolean($basissets/@isCalculationLevel) and compare($basissets/@definedBasis, ' ') = 0) then
                                $basissets/@calculationLevelBasis
                            else
                                $basissets/@definedBasis
                            "/>                            

                        <xsl:variable name="allAtomBasis">
                            <xsl:variable name="atoms" select="$molecule/atomArray/atom"/>
                            <xsl:for-each select="$atoms">                                                        
                                <xsl:variable name="outerIndex" select="position()"/>
                                <xsl:variable name="elementType" select="@elementType"/>
                                <xsl:variable name="elementBasis">
                                    <xsl:choose>
                                        <xsl:when test="exists(($molecule/atomArray/atom)[$outerIndex]/scalar[@dictRef='cc:basis']/text()) and compare(($molecule/atomArray/atom)[$outerIndex]/scalar[@dictRef='cc:basis']/text(),'N/A') !=0 ">
                                            <xsl:value-of select="($molecule/atomArray/atom)[$outerIndex]/scalar[@dictRef='cc:basis']/text()"/>        
                                        </xsl:when>
                                        <xsl:otherwise>
                                            
                                            <xsl:choose>
                                                <xsl:when test="boolean($isCalculationLevel) and starts-with($basis,'DefBas')">
                                                    <xsl:value-of select="orca:getDefaultBasisForAtomType($elementType, $basis)"/>                                                        
                                                </xsl:when>
                                                <xsl:when test="exists($userdefbasis[descendant::array[matches(upper-case(text()), concat('.*NEWGTO\s*', upper-case($elementType),'\s*.*')) ]])">
                                                    <xsl:variable name="basis" select="$userdefbasis//descendant::array[matches(upper-case(text()), concat('.*NEWGTO\s*', upper-case($elementType),'\s*.*'))]"/>                                                        
                                                    <xsl:value-of select="(tokenize($basis/text(),'&quot;'))[2]"/>
                                                </xsl:when>                                                       
                                                <xsl:otherwise>
                                                    <xsl:value-of select="(tokenize($basis, '[\s+]'))[1]"/>  <!--Display main basis, not secondary-->                                                         
                                                </xsl:otherwise>
                                            </xsl:choose>
                                    
                                        </xsl:otherwise>                                            
                                    </xsl:choose>
                                    <xsl:if test="exists($basisecp)">
                                        <xsl:value-of select="orca:getBasisECP($basisecp, $outerIndex -1)"/>
                                    </xsl:if>
                                </xsl:variable>
                                <xsl:element name="basis">
                                    <xsl:attribute name="value" select="concat($elementType, '#', $elementBasis)"/>
                                </xsl:element>
                            </xsl:for-each>                          
                        </xsl:variable>                                                
                       
                        <xsl:for-each select="distinct-values($allAtomBasis/basis/@value)">
                            <xsl:variable name="basis" select="."/>
                            <xsl:choose>
                                <xsl:when test="not(ends-with($basis,'+'))">
                                    <xsl:value-of select="concat($basis, ' ')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat(substring($basis,1,string-length($basis)-1),' ')"/>
                                </xsl:otherwise>
                            </xsl:choose>                                
                        </xsl:for-each>
                        
                    </xsl:when>
                </xsl:choose> 
            </xsl:element>
        </template>
    </xsl:template>
     
    <!-- Functional filtering, remove shell wave functions on gaussian-->
    <xsl:function name="helper:filterFunctional">
        <xsl:param name="functional"/>
                         
        <xsl:variable name="shellWaveFunctions" select="'U|R|'"/>
        <xsl:variable name="shellWaveFunctionsObligatory" select="'U|R'"/>
        <xsl:variable name="methodsRegex" select="upper-case('DFT|MM|Amber|Dreiding|UFF|AM1|PM3|PM3MM|PM6|PDDG|HF|HFS|XAlpha|HFB|VSXC|HCTH|HCTH93|HCTH147|HCTH407|tHCTH|M06L|B97D|LSDA|LC-wPBE|CAM-B3LYP|wB97XD|wB97|wB97X|LC-BLYP|B3LYP|B3P86|B3PW91|B1B95|mPW1PW91|mPW1LYP|mPW1PBE|mPW3PBE|B98|B971|B972|PBE1PBE|B1LYP|O3LYP|TPSSh|BMK|M06|M06HF|M062X|M05|M052X|X3LYP|BHandH|BHandHLYP|tHCTHhyb|HSEh1PBE|HSE2PBE|HSEhPBE|PBEh1PBE|CASSCF|CAS|MP2|MP3|MP4|MP5|B2PLYP|B2PLYPD|mPW2PLYP|mPW2PLYPD|QCISD|CCD|CCSD|CC|QCID|BD|EPT|CBS-4M|CBS-QB3|ROCBS-QB3|CBS-APNO|G1|G2|G2MP2|G3|G3MP2|G3B3|G3MP2B3|G4|G4MP2|W1U|W1BD|W1RO|CIS|CIS\(D\)|CID|CISD|TD|EOMCCSD|ZINDO|DFTB|DFTBA|GVB|CNDO|INDO|MINDO|MNDO|NMR|SAC-CI')"/>
        <xsl:variable name="multipleMethodRegex" select="'ONIOM|IRCMAX'"/>
        <xsl:variable name="exchangeFunctional" select="upper-case('S|XA|B|PW91|mPW|G96|PBE|OPBE|O|TPSS|RevTPSS|BRx|PKZB|wPBEh|PBEh|LC-')"/>
        <xsl:variable name="correlationFunctional" select="upper-case('VWN|VWN5|LYP|PL|P86|PW91|B95|PBE|TPSS|RevTPSS|KCIS|BRC|PKZB|VP86|V5LYP')"/>
        
        <xsl:choose>
            <xsl:when test="matches(upper-case($functional), concat('^(',$methodsRegex,')$'))">
                <xsl:value-of select="helper:normalizeFunctional($functional)"/>
            </xsl:when>
            <xsl:when test="matches(upper-case($functional),concat('^(', $shellWaveFunctions,')(',$methodsRegex ,')$'))">
                <xsl:value-of select="helper:normalizeFunctional(replace($functional,'^(U|R)',''))"/>
            </xsl:when>
            <xsl:when test="matches(upper-case($functional),concat('^(', $exchangeFunctional, ').*(', $correlationFunctional, ')$'))">
                <xsl:value-of select="helper:normalizeFunctional($functional)"/>
            </xsl:when>
            <xsl:when test="matches(upper-case($functional),concat('^(', $shellWaveFunctions, ')(', $exchangeFunctional, ').*(', $correlationFunctional, ')$'))">
                <xsl:value-of select="helper:normalizeFunctional(replace($functional,'^(U|R)',''))"/>
            </xsl:when>
            <xsl:otherwise>                
                <xsl:value-of select=" $functional"/>
            </xsl:otherwise>
        </xsl:choose>       
    </xsl:function>
    
    <!-- Normalize functional among it's different types of representation -->
    <xsl:variable name="functionalConversionRegex" select="'^(bp|bp86|b-p86|vwn|vwn5|s-vwn5|vwn3|s-vwn|blyp|b-lyp|pwldas-p81|b-vwn5|pbe|pbe-pbe|tpss|tpss-tpss|bhandhlyp|bhandhlyp|b3lyp|b3lyp/g|b3lyp|pbe0|pbe1pbe|tpssh|tpssh|b97d|b2plyp|opbe|o-pbe)$'"/>
    <xsl:variable name="funcionalConversionTable">
        <functional regex="^(bp|bp86|b-p86)$" value="b-p"/>
        <functional regex="^(vwn|vwn5|s-vwn5)$" value="s-vwn"/>
        <functional regex="^(vwn3|s-vwn)$" value="s-vwn_Gaussian"/>
        <functional regex="^(blyp|b-lyp)$" value="b-lyp"/>
        <functional regex="^(pwldas-p81)$" value="pwlda"/>
        <functional regex="^(b-vwn5)$" value="b-vwn"/>
        <functional regex="^(pbe|pbe-pbe)$" value="pbe"/>
        <functional regex="^(tpss|tpss-tpss)$" value="tpss"/>
        <functional regex="^(bhandhlyp|bhandhlyp)$" value="bh-lyp"/>
        <functional regex="^(b3lyp)$" value="b3-lyp"/>
        <functional regex="^(b3lyp/g|b3lyp)$" value="b3-lyp_Gaussian"/>
        <functional regex="^(pbe0|pbe1pbe)$" value="pbe0"/>
        <functional regex="^(tpssh|tpssh)$" value="tpssh"/>
        <functional regex="^(b97d)$" value="b97-d"/>
        <functional regex="^(b2plyp)$" value="b2-plyp"/>
        <functional regex="^(opbe|o-pbe)$" value="opbe"/>
    </xsl:variable>
    <xsl:function name="helper:normalizeFunctional">
        <xsl:param name="candidate"/>        
        <xsl:choose>        
            <xsl:when test="matches(lower-case($candidate),$functionalConversionRegex)">
                <xsl:variable name="results">
                    <xsl:for-each select="$funcionalConversionTable/functional ">
                        <xsl:variable name="functional" select="."/>
                        <xsl:if test="matches(lower-case($candidate), $functional/@regex)">
                            <xsl:value-of select="$functional/@value"/>|
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:value-of select="tokenize($results,'\|')[1]"/> <!-- Get first match only -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$candidate"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
     
      
    <xsl:function name="orca:getDefaultBasisForAtomType">
        <xsl:param name="elementType"/>
        <xsl:param name="basis"/>        
        <!-- Standard Basis Sets  and Computational Levels -->
        <xsl:variable name="basisNumber" select="number(substring-after($basis,'DefBas-'))"/>        
        <xsl:choose>
            <xsl:when test="$basisNumber >2">TZV</xsl:when>
            <xsl:otherwise>
                <xsl:variable name="elementNumber" select="helper:atomType2Number($elementType)"/>
                <xsl:choose>
                    <xsl:when test="$elementNumber = 1 or not(helper:isTransitionMetal($elementNumber))">SV</xsl:when>
                    <xsl:otherwise>TZ</xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>            
        </xsl:choose>
    </xsl:function>  
      
    <xsl:template name="substring-before-last">       
        <xsl:param name="list"/>     
        <xsl:param name="delimiter"/>        
        <xsl:choose>           
            <xsl:when test="contains($list, $delimiter)">
                <!-- get everything in front of the first delimiter -->
                <xsl:value-of select="substring-before($list,$delimiter)"/>
                <xsl:choose>
                    <xsl:when test="contains(substring-after($list,$delimiter),$delimiter)">
                        <xsl:value-of select="$delimiter"/>
                    </xsl:when>
                </xsl:choose>              
                <xsl:call-template name="substring-before-last">                  
                    <!-- store anything left in another variable -->                  
                    <xsl:with-param name="list" select="substring-after($list,$delimiter)"/>                  
                    <xsl:with-param name="delimiter" select="$delimiter"/>                  
                </xsl:call-template>              
            </xsl:when>         
        </xsl:choose>     
    </xsl:template>
      
</xsl:stylesheet>

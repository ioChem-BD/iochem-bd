<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:a="http://www.xml-cml.org/dictionary/adf/" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:gaussian="http://www.gaussian.com/"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="#all"
    version="2.0">
    <xsl:output method="text" encoding="UTF-8" indent="no"/>
    <xsl:include href="../../xslt/helper/chemistry/gaussian.xsl"/>
     
    <xsl:template match="/">
            <xsl:choose>
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">                      
                    <xsl:variable name="pressure" select="(//property[@dictRef='cc:thermochemistry'])[last()]//scalar[@dictRef='cc:press']"/>
                    <xsl:variable name="temperature" select="(//property[@dictRef='cc:thermochemistry'])[last()]//scalar[@dictRef='cc:temp']"/>
                    <xsl:if test="exists($pressure)">
                        <xsl:value-of select="$pressure"/>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="$pressure/@units"/>
                    </xsl:if>
                    <xsl:if test="exists($temperature)">
                        <xsl:text>
</xsl:text>             <xsl:value-of select="$temperature"/>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="$temperature/@units"/>
                    </xsl:if>
                </xsl:when>
                
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ADF')">
                    <xsl:variable name="pressure" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]/scalar[@dictRef='cc:press']"/>
                    <xsl:variable name="temperature" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]/scalar[@dictRef='cc:temp']"/>
                    <xsl:if test="exists($pressure)">
                        <xsl:value-of select="$pressure"/>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="$pressure/@units"/>
                    </xsl:if>         
                    <xsl:if test="exists($temperature)">
                        <xsl:text>
</xsl:text>             <xsl:value-of select="$temperature"/>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="$temperature/@units"/>
                    </xsl:if>
                </xsl:when>                                     
                <xsl:otherwise>
                    <xsl:text>Not implemented</xsl:text>
                    <!-- Not implemented -->
                </xsl:otherwise>
            </xsl:choose>                        
        
    </xsl:template>
</xsl:stylesheet>
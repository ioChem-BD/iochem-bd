<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.xml-cml.org/schema" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"    
    xmlns:turbo="http://www.turbomole.com"
    xmlns:t="http://www.turbomole.com"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="#all"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:include href="../../xslt/helper/chemistry/turbomole.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
    
    <xsl:param name="title"/>
    <xsl:param name="energyUnits"/>
    
    <!-- Turbomole report only (right now), other calculations can implement its own code -->
    <!-- Used for multiplicity -->
    <xsl:variable name="orbitalSpecsAlpha" select="//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='unrestrictedorbitals' and compare(child::scalar[@dictRef='t:spintype']/text(),'alpha') = 0]"/>
    <xsl:variable name="orbitalSpecsBeta" select="//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='unrestrictedorbitals' and compare(child::scalar[@dictRef='t:spintype']/text(),'beta') = 0]"/>
    <xsl:variable name="occupiedAlpha" select="tokenize(replace($orbitalSpecsAlpha/array[@dictRef='t:mosrange'],'\s*-\s*','-'),'\s+')"/>
    <xsl:variable name="occupiedBeta" select="tokenize(replace($orbitalSpecsBeta/array[@dictRef='t:mosrange'],'\s*-\s*','-'), '\s+')"/>
    <!-- Used for calculation type -->
    <xsl:variable name="isRestrictedOptimization" select="exists(//module[@cmlx:templateRef='restrictions'])"/>
    <xsl:variable name="isOptimization" select="exists(//module[@cmlx:templateRef='convergence.info'])"/>
    <xsl:variable name="isIncomplete" select="count(//module[@cmlx:templateRef='convergence.info']//scalar[@dictRef='cc:converged' and normalize-space(text())='no']) > 0"/>
    <xsl:variable name="vibrations" select="//module[@cmlx:templateRef='vibrations']"/>
    <xsl:variable name="statpt" select="//module[@cmlx:templateRef='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='t:statpt']"/>
    <xsl:variable name="soes" select="//module[@cmlx:templateRef='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='t:soes']/list"/>
    <!-- Used for method field-->
    <xsl:variable name="functional" select="//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:functional']/scalar"/>
    <xsl:variable name="scfinstab" select="//module[@cmlx:templateRef='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='t:scfinstab']/scalar"/>
    <xsl:variable name="dispersion" select="//module[@cmlx:templateRef='job'][1]/module[@dictRef='cc:initialization']//module[@cmlx:templateRef='dispersion']/scalar[@dictRef='t:correction']"/>   
    <xsl:variable name="gridsize" select="//module[@cmlx:templateRef='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='t:dftgridsize']/scalar"/>
    <xsl:variable name="parameterList" select="//module[@cmlx:templateRef='job'][1]/module[@dictRef='cc:initialization']/parameterList"/>        
    <xsl:variable name="parameters" select="$parameterList/parameter[matches(@dictRef,'(t:ri|t:ricc2|t:rir12)')]/child::*"/>    
    <xsl:variable name="methods" select="$parameterList/parameter[@dictRef='cc:method']/child::*"/>       
    <!-- Used for energy field -->
    <xsl:variable name="energy" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:energy']/scalar)[last()]"/>
    <xsl:variable name="energyresume" select="(//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@id='energy.resume'])[last()]"/>
    <!-- Used for zero point energy -->
    <xsl:variable name="zeroPoint" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:zeropoint']/scalar)[last()]"/>
    
    <xsl:template match="/">
        <xsl:variable name="multiplicity">
            <xsl:choose>                
                <xsl:when test="exists($occupiedAlpha)"><xsl:value-of select="turbo:calculateMultiplicity($occupiedAlpha, $occupiedBeta)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="1"/></xsl:otherwise>
            </xsl:choose>            
        </xsl:variable>
        
        <xsl:variable name="calcType">
            <xsl:value-of select="turbo:getCalcType($isRestrictedOptimization,$isOptimization,$isIncomplete,$vibrations, $statpt, $soes)"/>
        </xsl:variable>        
        
        <xsl:variable name="method">
            <xsl:value-of select="turbo:getMethod($soes, $methods)"/>       
            <xsl:if test="exists($functional) 
                or exists($dispersion) 
                or exists($parameters)
                or exists($gridsize)
                or exists($scfinstab)">
                <xsl:variable name="parameters" >
                    <xsl:value-of select="$functional"/><xsl:text>&#32;&#32;</xsl:text>
                    <xsl:value-of select="turbo:translateDispersion($dispersion)"/><xsltext>,&#32;&#32;</xsltext>
                    <xsl:value-of select="turbo:translateParameters($parameters)"/><xsl:text>,&#32;&#32;</xsl:text>
                    <xsl:value-of select="turbo:translateGridSize($gridsize)"/><xsl:text>,&#32;&#32;</xsl:text>
                    <xsl:value-of select="turbo:translateSCFinstab(upper-case($scfinstab))"/>                         
                </xsl:variable>
                (<xsl:value-of select="replace(replace($parameters, '[,\s]+', ', '),'(^([,\s*]+))|([,\s*]+$)','')"/>)
            </xsl:if>
        </xsl:variable>        
        
        <template id="calculation" name="{$title}">                           
            <xsl:element name="multiplicity"><xsl:value-of select="$multiplicity"/></xsl:element>
            <xsl:element name="calculationtype"><xsl:value-of select="helper:trim($calcType)"/></xsl:element>
            <xsl:element name="method"><xsl:value-of select="$methods"/></xsl:element>
            <xsl:element name="finalenergy">
                <xsl:choose>
                    <xsl:when test="not(exists($energyresume/scalar[@dictRef='t:rhfEnergy']))">
                        <xsl:attribute name="units" select="$energyUnits"/>        
                        <xsl:value-of select="helper:convertEnergyUnits($energy,$energyUnits)"/>    
                    </xsl:when>
                    <xsl:when test="exists($energyresume/scalar[@dictRef='t:rhfEnergy'])">
                        <xsl:attribute name="units" select="$energyUnits"/>
                        <xsl:value-of select="helper:convertEnergyUnits($energyresume/scalar[@dictRef='t:rhfEnergy'],$energyUnits)"/>
                    </xsl:when>
                </xsl:choose>        
                
            </xsl:element>
            <xsl:element name="zeropointenergy">
                <xsl:attribute name="units" select="$energyUnits"/>
                <xsl:value-of select="helper:convertEnergyUnits($zeroPoint,$energyUnits)"/>                    
            </xsl:element>
        </template>
    </xsl:template>
      
</xsl:stylesheet>

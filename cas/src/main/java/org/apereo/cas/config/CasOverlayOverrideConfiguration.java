package org.apereo.cas.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Configuration
@EnableConfigurationProperties(CustomConfigurationProperties.class)
public class CasOverlayOverrideConfiguration {

}
    
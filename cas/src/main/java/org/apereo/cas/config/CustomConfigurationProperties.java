package org.apereo.cas.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component(value = "customConfigurationProperties")
@ConfigurationProperties(prefix = "custom")
public class CustomConfigurationProperties {
    private String institution;

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }
}
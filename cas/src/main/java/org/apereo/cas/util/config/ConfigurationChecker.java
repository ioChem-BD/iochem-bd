package org.apereo.cas.util.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigurationChecker {

    public static void verifyOrUpdate() {
        try{
            String version = getConfigurationVersion();
            new ConfigurationUpdate10().run(version);               
        }catch(Exception e){
            LOGGER.error(e.getMessage());
        }        
    }

    private static String getConfigurationVersion() throws Exception {
        // Retrieve IOCHEMBD_DIR system variable parameter value
        String basePath = System.getenv("IOCHEMBD_DIR");
        if (basePath == null || basePath.isEmpty())
            throw new Exception("Undefined IOCHEMBD_DIR system variable.");

        Path casYamlPath = Paths.get(basePath, "cas", "cas.yml");
        Yaml yaml = new Yaml(new Constructor(Map.class));
        try (InputStream inputStream = new FileInputStream(casYamlPath.toFile())) {
            Map<String, Object> data = (Map<String, Object>) yaml.load(inputStream);
            return (String) ConfigurationUpdate.getNestedProperty(data, new String[] { "custom", "config", "version" });
        } catch (Exception e) {
            return null;
        }
    }
}

package org.apereo.cas.util.config;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ConfigurationUpdate {

    protected abstract boolean shouldUpdate(String version);

    protected abstract void run(String currentVersion) throws Exception;

    protected void replaceProperty(Properties conf, String oldKey, String newKey) throws Exception {
        try{
            if (conf.containsKey(oldKey)) {
                conf.setProperty(newKey, conf.getProperty(oldKey));
                conf.remove(oldKey);
            }
        }catch(Exception e){
            LOGGER.error("Could not replace property " + oldKey);
        }
    }

    public static Object getNestedProperty(Map<String, Object> data, String[] path) {
        Map<String, Object> currentMap = data;
        Object value = null;

        for (int i = 0; i < path.length; i++) {
            value = currentMap.get(path[i]);

            if (i < path.length - 1) {
                if (value instanceof Map) {
                    currentMap = (Map<String, Object>) value;
                } else {
                    return null; // Path does not exist
                }
            }
        }
        return value;
    }

    // Helper method to set a value in a nested map structure, including arrays
    protected static void setYamlValue(Map<String, Object> data, String path, Object newValue) {
        String[] keys = path.split("\\.");
        Object current = data;

        for (int i = 0; i < keys.length - 1; i++) {
            String key = keys[i];

            if (key.matches("\\S+\\[\\d+\\]")) {
                String regex = "(\\S+)\\[(\\d+)\\]";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(key);
                if (matcher.matches()) {
                    key = matcher.group(1);
                    int index = Integer.parseInt(matcher.group(2));

                    if (!(current instanceof Map)) {
                        throw new IllegalArgumentException(
                                "Expected a map at " + key + " but found " + current.getClass().getSimpleName());
                    }
                    Map<String, Object> map = (Map<String, Object>) current;
                    current = map.get(key);

                    if (!(current instanceof List)) {
                        throw new IllegalArgumentException(
                                "Expected a list at " + key + " but found " + current.getClass().getSimpleName());
                    }
                    List<Object> list = (List<Object>) current;
                    while (list.size() <= index) {
                        list.add(new java.util.HashMap<String, Object>());
                    }
                    current = list.get(index);
                }
            } else {
                if (!(current instanceof Map)) {
                    throw new IllegalArgumentException(
                            "Expected a map at " + key + " but found " + current.getClass().getSimpleName());
                }
                Map<String, Object> map = (Map<String, Object>) current;
                if (!map.containsKey(key) || !(map.get(key) instanceof Map)) {
                    map.put(key, new java.util.HashMap<String, Object>());
                }
                current = map.get(key);
            }
        }

        String lastKey = keys[keys.length - 1];
        if (lastKey.matches("\\d+")) {
            int index = Integer.parseInt(lastKey);
            if (!(current instanceof List)) {
                throw new IllegalArgumentException(
                        "Expected a list at " + lastKey + " but found " + current.getClass().getSimpleName());
            }
            List<Object> list = (List<Object>) current;
            while (list.size() <= index) {
                list.add(null);
            }
            list.set(index, newValue);
        } else {
            if (!(current instanceof Map)) {
                throw new IllegalArgumentException(
                        "Expected a map at " + lastKey + " but found " + current.getClass().getSimpleName());
            }
            Map<String, Object> map = (Map<String, Object>) current;
            map.put(lastKey, newValue);
        }
    }

    protected static void writeYaml(Map<String, Object> data, String filePath) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Yaml yaml = new Yaml(options);

        try (FileWriter writer = new FileWriter(filePath)) {
            yaml.dump(data, writer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

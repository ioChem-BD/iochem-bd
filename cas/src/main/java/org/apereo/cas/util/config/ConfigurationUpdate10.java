package org.apereo.cas.util.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class ConfigurationUpdate10 extends ConfigurationUpdate {

    @Override
    protected boolean shouldUpdate(String version) {
        return version == null || version.isEmpty();
    }

    @Override
    protected void run(String currentVersion) throws Exception {
        if (!shouldUpdate(currentVersion))
            return;

        String basePath = System.getenv("IOCHEMBD_DIR");

        Path createResourcesPath = Paths.get(basePath, "create", "resources.properties");
        String institutionName = getInstitutionName(createResourcesPath.toFile());

        // Update cas.yml properties file
        Path casYamlPath = Paths.get(basePath, "cas", "cas.yml");
        updateCasYaml(casYamlPath.toFile(), institutionName);
    }

    private String getInstitutionName(File file) {
        try {
            Properties conf = new Properties();            
            conf.load(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            return conf.getProperty("mets.institution.name", "");
        } catch (Exception e) {
            return "";
        }
    }

    private void updateCasYaml(File file, String institutionName) throws Exception {
        if (!file.exists())
            throw new FileNotFoundException("Could not access cas.yml file on " + file.getCanonicalPath());

        String url = "";
        String user = "";
        String password = "";
        String serverName = "";
        String serverPrefix = "";
        Yaml yaml = new Yaml(new Constructor(Map.class));
        // Load original cas.yml file
        try (InputStream inputStream = new FileInputStream(file)) {
            Map<String, Object> data = (Map<String, Object>) yaml.load(inputStream);
            // Retrieve jdbc connection properties
            ArrayList urlArray = (ArrayList) getNestedProperty(data,
                    new String[] { "cas", "authn", "jdbc", "encode" });
            url = (String) (((Map<String, Object>) urlArray.get(0)).get("url"));
            user = (String) (((Map<String, Object>) urlArray.get(0)).get("user"));
            password = (String) (((Map<String, Object>) urlArray.get(0)).get("password"));

            // Retrieve server properties
            serverName = (String) getNestedProperty(data, new String[] { "cas", "server", "name" });
            serverPrefix = (String) getNestedProperty(data, new String[] { "cas", "server", "prefix" });
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create the updated cas.yml file
        try (InputStream inputStream = new ByteArrayInputStream(newYaml.getBytes())) {
            Map<String, Object> data = (Map<String, Object>) yaml.load(inputStream);
            setYamlValue(data, "cas.authn.jdbc.encode[0].user", user);
            setYamlValue(data, "cas.authn.jdbc.encode[0].password", password);
            setYamlValue(data, "cas.authn.jdbc.encode[0].url", url);

            setYamlValue(data, "cas.server.name", serverName);
            setYamlValue(data, "cas.server.prefix", serverPrefix);

            setYamlValue(data, "custom.institution", institutionName);
            setYamlValue(data, "custom.config.version", "1.0");

            writeYaml(data, file.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String newYaml = "cas:\n" + //
            "  locale:\n" + //
            "    force-default-locale: true\n" + //
            "    default-value: en\n" + //
            "  theme:\n" + //
            "    default-theme-name: \"iochembd\"\n" + //
            "  authn:\n" + //
            "    accept:\n" + //
            "      users: ''\n" + //
            "    policy:\n" + //
            "      any:\n" + //
            "        enabled: \"true\"\n" + //
            "    throttle:\n" + //
            "      core: \n" + //
            "          username-parameter: username\n" + //
            "          app-code: CAS\n" + //
            "      failure:\n" + //
            "        threshold: 1\n" + //
            "        range-seconds: 3\n" + //
            "        code: AUTHENTICATION_FAILED\n" + //
            "    jdbc:\n" + //
            "      encode:\n" + //
            "        - dialect: org.hibernate.dialect.PostgreSQL95Dialect\n" + //
            "          driver-class: org.postgresql.Driver\n" + //
            "          order: 1\n" + //
            "          password: null\n" + //
            "          sql: SELECT * FROM eperson WHERE email=?\n" + //
            "          url: null\n" + //
            "          user: null\n" + //
            "          disabled-field-name: can_log_in\n" + //
            "          algorithm-name: SHA-512\n" + //
            "          salt-field-name: salt\n" + //
            "          number-of-iterations: 1024\n" + //
            "          password-encoder:\n" + //
            "            character-encoding: UTF-8\n" + //
            "            type: DEFAULT\n" + //
            "            encoding-algorithm: SHA-512\n" + //
            "    attribute-repository:\n" + //
            "        core:\n" + //
            "          merger: REPLACE\n" + //
            "  server:\n" + //
            "    name: null\n" + //
            "    prefix: null\n"
            + //
            "  service-registry:\n" + //
            "    core:\n" + //
            "      init-from-json: true\n" + //
            "    json:\n" + //
            "      location: file://${IOCHEMBD_DIR}/webapps/cas/WEB-INF/classes/services\n" + //
            "logging:\n" + //
            "  config: ${IOCHEMBD_DIR}/cas/log4j2.xml\n" + //
            "spring:\n" + //
            "  data:\n" + //
            "    rest:\n" + //
            "      hal:\n" + //
            "        enabled: false\n" + //
            "custom:\n" + //
            "  institution:\n" + //
            "  config:\n" + //
            "    version: 1.0\n";
}
/**
 * cas-overlay - ioChem-BD platform is a suite of web services aimed to manage Computational Chemistry and Material Science digital files and aid scientist in their research.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.apereo.cas.adaptors.jdbc;

import lombok.extern.slf4j.Slf4j;


import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.ConfigurableHashService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.util.ByteSource;
import org.apereo.cas.authentication.AuthenticationHandlerExecutionResult;
import org.apereo.cas.authentication.CoreAuthenticationUtils;
import org.apereo.cas.authentication.PreventedException;
import org.apereo.cas.authentication.credential.UsernamePasswordCredential;
import org.apereo.cas.authentication.exceptions.AccountDisabledException;
import org.apereo.cas.authentication.exceptions.AccountPasswordMustChangeException;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.configuration.model.support.jdbc.authn.QueryEncodeJdbcAuthenticationProperties;
import org.apereo.cas.services.ServicesManager;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import javax.security.auth.login.AccountNotFoundException;
import javax.security.auth.login.FailedLoginException;
import javax.sql.DataSource;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A JDBC querying handler that will pull back the password and
 * the private salt value for a user and validate the encoded
 * password using the public salt value. Assumes everything
 * is inside the same database table. Supports settings for
 * number of iterations as well as private salt.
 * <p>
 * This handler uses the hashing method defined by Apache Shiro's
 * {@link org.apache.shiro.crypto.hash.DefaultHashService}. Refer to the Javadocs
 * to learn more about the behavior. If the hashing behavior and/or configuration
 * of private and public salts does nto meet your needs, a extension can be developed
 * to specify alternative methods of encoding and digestion of the encoded password.
 * </p>
 *
 */
@Slf4j
public class DspaceQueryAndEncodeDatabaseAuthenticationHandler extends AbstractJdbcUsernamePasswordAuthenticationHandler {

	
	private final String[] DSPACE_EPERSON_PUBLIC_PRINCIPAL_FIELDS = {"eperson_id", "email", "netid", "username", "main_group_id"};
 
    private final QueryEncodeJdbcAuthenticationProperties properties;


    public DspaceQueryAndEncodeDatabaseAuthenticationHandler(final QueryEncodeJdbcAuthenticationProperties properties,
                                                             final ServicesManager servicesManager,
                                                             final PrincipalFactory principalFactory,
                                                             final DataSource dataSource) {
        super(properties.getName(), servicesManager, principalFactory, properties.getOrder(), dataSource);
        this.properties = properties;       
    }

    @Override
    protected AuthenticationHandlerExecutionResult authenticateUsernamePasswordInternal(
        final UsernamePasswordCredential transformedCredential,
        final String originalPassword)
        throws GeneralSecurityException, PreventedException {        
            
        if (StringUtils.isBlank(properties.getSql()) || StringUtils.isBlank(properties.getAlgorithmName()) || getJdbcTemplate() == null) {
            throw new GeneralSecurityException("Authentication handler is not configured correctly");
        }

        

        final String username = transformedCredential.getUsername();
        try {
            final Map<String, Object> values = getJdbcTemplate().queryForMap(properties.getSql(), username);
            final String digestedPassword = digestEncodedPassword(new String(transformedCredential.getPassword()), values);
            if (!values.get(properties.getPasswordFieldName()).equals(digestedPassword)) {
                throw new FailedLoginException("Password does not match value on record.");
            }
            if (StringUtils.isNotBlank(properties.getExpiredFieldName()) && values.containsKey(properties.getExpiredFieldName())) {
                final String dbExpired = values.get(properties.getExpiredFieldName()).toString();
                if (BooleanUtils.toBoolean(dbExpired) || "1".equals(dbExpired)) {
                    throw new AccountPasswordMustChangeException("Password has expired");
                }
            }
            
            // Disabled field inside Dspace is "can_log_in" column, must invert result
            if (StringUtils.isNotBlank(properties.getDisabledFieldName()) && values.containsKey(properties.getDisabledFieldName())) {            	
                final String canLogIn = values.get(properties.getDisabledFieldName()).toString();
                if (!BooleanUtils.toBoolean(canLogIn) || "0".equals(canLogIn)) {
                    throw new AccountDisabledException("Account has been disabled");
                }
            }
            Map<String, Object> attributes = loadPrincipalAttributes(username, values);            
            return createHandlerResult(transformedCredential, this.principalFactory.createPrincipal(username, CoreAuthenticationUtils.convertAttributeValuesToMultiValuedObjects(attributes)), new ArrayList<>(0));

        } catch (final IncorrectResultSizeDataAccessException e) {
            if (e.getActualSize() == 0) {
                throw new AccountNotFoundException(username + " not found with SQL query");
            }
            throw new FailedLoginException("Multiple records found for " + username);
        } catch (final DataAccessException e) {
            throw new PreventedException("SQL exception while executing query for " + username);
        } catch(final IllegalArgumentException e) {
        	throw new PreventedException("Bad parameters for " + username);
        } catch (final DecoderException e) {
        	throw new PreventedException("Bad decoder provided for " + username);
        }
    }

    
    private Map<String, Object> loadPrincipalAttributes(String username, Map<String, Object> userValues) {
    	Map<String, Object> attributes  = new HashMap<String, Object>();    	
    	loadUserAttributes(username, userValues, attributes);
    	loadUserGroups(username, attributes);
		return attributes;
	}
    
	private void loadUserAttributes(String username, Map<String, Object> values, Map<String, Object> attributes) {
		// Load common user fields		
		for(String key : DSPACE_EPERSON_PUBLIC_PRINCIPAL_FIELDS) 
			if(values.containsKey(key))
				attributes.put(key, values.get(key));					
		// Load additional user fields
    	String userAttributesSql = "select eperson_id as user_id, getuserfullname(eperson_id) as user_fullname,  username as user_path, main_group_id, email as user_email from eperson where email = ?";
    	Map<String, Object> userAttrs = getJdbcTemplate().queryForMap(userAttributesSql, username);     	  
		for(String key : userAttrs.keySet())
			attributes.put(key, userAttrs.get(key));
    	
    }
    
	private void loadUserGroups(String username, Map<String, Object> attributes) {
		String userGroupsIdSql = "SELECT array_to_string(array_agg(eperson_group_id), '/')  as group_id \n" + 
				"                                          FROM  (\n" + 
				"                                                    SELECT eperson_group_id \n" + 
				"                                                    FROM eperson \n" + 
				"                                                    NATURAL JOIN epersongroup2eperson \n" + 
				"                                                    NATURAL JOIN epersongroup \n" + 
				"                                                    WHERE email = ?\n" + 
				"                                                    ORDER BY eperson_group_id ASC) \n" + 
				"                                          AS x";
		Map<String, Object> userIds = getJdbcTemplate().queryForMap(userGroupsIdSql, username);
		for(String key : userIds.keySet())
			attributes.put(key, userIds.get(key));    	
	}   

	/**
     * Digest password.
     *
     * @param password        the password
     * @param values          the values retrieved from database
     * @return the digested password
     * @throws DecoderException 
     * @throws IllegalArgumentException 
     */
     
    protected String digestEncodedPassword(final String password, final Map<String, Object> values) throws IllegalArgumentException, DecoderException {
        final ConfigurableHashService hashService = new DefaultHashService();

        
        if (StringUtils.isNotBlank(properties.getStaticSalt())) {
            hashService.setPrivateSalt(ByteSource.Util.bytes(properties.getStaticSalt()));
        }
        
        hashService.setHashAlgorithmName(properties.getAlgorithmName());

        int numOfIterations = properties.getNumberOfIterations();
        if (values.containsKey(properties.getNumberOfIterationsFieldName())) {
            numOfIterations = Integer.valueOf(values.get(properties.getNumberOfIterationsFieldName()).toString());
        }

        hashService.setHashIterations(numOfIterations);
        if (!values.containsKey(properties.getSaltFieldName())) {
            throw new IllegalArgumentException("Specified field name for salt does not exist in the results");
        }

        final String dynaSalt = values.get(properties.getSaltFieldName()).toString();
        final HashRequest request = new HashRequest.Builder()
            .setSalt(Hex.decodeHex(dynaSalt.toCharArray()))
            .setSource(password)
            .build();
        return hashService.computeHash(request).toHex();
    }
}

